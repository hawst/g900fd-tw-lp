.class public Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private A:Z

.field private B:Z

.field private a:Landroid/widget/BaseAdapter;

.field private b:Landroid/database/DataSetObserver;

.field private c:Lljj;

.field private final d:Lljk;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:F

.field private k:F

.field private l:I

.field private final m:Landroid/view/VelocityTracker;

.field private final n:Lljq;

.field private final o:Lnh;

.field private final p:Lnh;

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 106
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance v0, Lljm;

    invoke-direct {v0, p0}, Lljm;-><init>(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b:Landroid/database/DataSetObserver;

    .line 55
    new-instance v0, Lljk;

    invoke-direct {v0, p0}, Lljk;-><init>(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    .line 65
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    .line 94
    iput-boolean v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->A:Z

    .line 108
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f:I

    .line 110
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->g:I

    .line 111
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->h:I

    .line 112
    invoke-static {p1}, Lljq;->a(Landroid/content/Context;)Lljq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    .line 114
    new-instance v0, Lnh;

    invoke-direct {v0, p1}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    .line 115
    new-instance v0, Lnh;

    invoke-direct {v0, p1}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    .line 117
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setWillNotDraw(Z)V

    .line 119
    sget-object v0, Llge;->c:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->B:Z

    .line 121
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->B:Z

    if-eqz v1, :cond_0

    .line 122
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v1, :cond_1

    .line 123
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->B:Z

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setHorizontalScrollBarEnabled(Z)V

    .line 128
    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 129
    return-void

    .line 125
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->B:Z

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setVerticalScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method private a(II)I
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 766
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingLeft()I

    move-result v0

    move v1, v0

    .line 767
    :goto_0
    neg-int v4, p2

    .line 770
    :goto_1
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    if-le v0, v4, :cond_7

    if-ltz p1, :cond_7

    .line 771
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c(I)Landroid/view/View;

    move-result-object v5

    .line 772
    if-nez v5, :cond_1

    .line 807
    :goto_2
    return v2

    .line 766
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingTop()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 776
    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llji;

    .line 778
    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eq v3, p0, :cond_2

    .line 779
    iget-boolean v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->y:Z

    if-eqz v3, :cond_3

    .line 780
    invoke-virtual {p0, v5, v2, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 786
    :cond_2
    :goto_3
    invoke-direct {p0, v5}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/view/View;)V

    .line 787
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 788
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 791
    iget-boolean v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v3, :cond_5

    .line 792
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    iget v8, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v8, v6

    sub-int/2addr v3, v8

    iput v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    .line 793
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    .line 794
    iget-boolean v0, v0, Llji;->a:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v7

    div-int/lit8 v0, v0, 0x2

    .line 800
    :goto_4
    add-int/2addr v6, v3

    .line 801
    add-int/2addr v7, v0

    .line 803
    invoke-virtual {v5, v3, v0, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 804
    add-int/lit8 v0, p1, -0x1

    iput p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    move p1, v0

    .line 805
    goto :goto_1

    .line 782
    :cond_3
    invoke-virtual {p0, v5, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_3

    :cond_4
    move v0, v2

    .line 794
    goto :goto_4

    .line 796
    :cond_5
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    iget v8, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v8, v7

    sub-int/2addr v3, v8

    iput v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    .line 797
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    .line 798
    iget-boolean v0, v0, Llji;->a:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v6

    div-int/lit8 v0, v0, 0x2

    :goto_5
    move v9, v3

    move v3, v0

    move v0, v9

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_5

    .line 807
    :cond_7
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    sub-int v2, v1, v0

    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;I)I
    .locals 0

    .prologue
    .line 43
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;)Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v5, -0x2

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 915
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llji;

    .line 918
    iget v1, v0, Llji;->width:I

    if-ne v1, v5, :cond_0

    .line 919
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 928
    :goto_0
    iget v2, v0, Llji;->height:I

    if-ne v2, v5, :cond_2

    .line 929
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 936
    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 937
    return-void

    .line 920
    :cond_0
    iget v1, v0, Llji;->width:I

    if-ne v1, v6, :cond_1

    .line 922
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 921
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0

    .line 924
    :cond_1
    iget v1, v0, Llji;->width:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0

    .line 930
    :cond_2
    iget v2, v0, Llji;->height:I

    if-ne v2, v6, :cond_3

    .line 932
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    .line 931
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_1

    .line 934
    :cond_3
    iget v0, v0, Llji;->height:I

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_1
.end method

.method private a(IZ)Z
    .locals 11

    .prologue
    const v10, 0x7fffffff

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 404
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e()Z

    move-result v8

    .line 405
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 409
    if-nez v8, :cond_14

    .line 412
    iput-boolean v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->z:Z

    .line 415
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v3

    add-int/2addr v0, v3

    invoke-direct {p0, v0, v7}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b(II)I

    move-result v0

    .line 416
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v3, v7}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(II)I

    move-result v3

    iget v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v3, v4

    .line 418
    if-lez p1, :cond_0

    move v0, v2

    .line 432
    :goto_0
    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 433
    if-eqz v4, :cond_17

    .line 434
    if-eqz v0, :cond_2

    move v0, v4

    .line 435
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b(I)V

    .line 436
    iget-boolean v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v4

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    move v6, v5

    :goto_3
    if-ltz v6, :cond_6

    invoke-virtual {p0, v6}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    iget-boolean v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v5, :cond_4

    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v5

    :goto_4
    if-le v5, v4, :cond_6

    iget-boolean v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->y:Z

    if-eqz v5, :cond_5

    invoke-virtual {p0, v6, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->removeViewsInLayout(II)V

    :goto_5
    iget-object v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    invoke-virtual {v5, v9}, Lljk;->a(Landroid/view/View;)V

    add-int/lit8 v5, v6, -0x1

    move v6, v5

    goto :goto_3

    .line 423
    :cond_0
    if-gez v0, :cond_1

    move v0, v1

    :cond_1
    move v3, v0

    move v0, v1

    .line 429
    goto :goto_0

    .line 434
    :cond_2
    neg-int v0, v4

    goto :goto_1

    .line 436
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v4

    goto :goto_2

    :cond_4
    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v5

    goto :goto_4

    :cond_5
    invoke-virtual {p0, v6}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->removeViewAt(I)V

    goto :goto_5

    :cond_6
    :goto_6
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    iget-boolean v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v4, :cond_7

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    :goto_7
    if-gez v4, :cond_9

    iget-boolean v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->y:Z

    if-eqz v4, :cond_8

    invoke-virtual {p0, v1, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->removeViewsInLayout(II)V

    :goto_8
    iget-object v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    invoke-virtual {v4, v5}, Lljk;->a(Landroid/view/View;)V

    iget v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    goto :goto_6

    :cond_7
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v4

    goto :goto_7

    :cond_8
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->removeViewAt(I)V

    goto :goto_8

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v4

    iput v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->r:I

    iget v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->r:I

    if-lez v4, :cond_d

    iput v10, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    const/high16 v4, -0x80000000

    iput v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    move v4, v1

    :goto_9
    iget v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->r:I

    if-ge v4, v5, :cond_c

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget-boolean v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v5, :cond_a

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v5

    :goto_a
    iget-boolean v9, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v9, :cond_b

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v6

    :goto_b
    iget v9, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v6, v9

    iget v9, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    invoke-static {v9, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    iget v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_a
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v5

    goto :goto_a

    :cond_b
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v6

    goto :goto_b

    :cond_c
    iget v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    if-ne v4, v10, :cond_e

    :cond_d
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f(I)V

    .line 438
    :cond_e
    :goto_c
    iput-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->z:Z

    .line 439
    sub-int v3, v7, v3

    move v4, v0

    move v0, v3

    .line 445
    :goto_d
    if-eqz p2, :cond_10

    .line 446
    invoke-static {p0}, Liu;->a(Landroid/view/View;)I

    move-result v3

    .line 448
    if-eqz v3, :cond_f

    if-ne v3, v2, :cond_10

    if-nez v8, :cond_10

    .line 451
    :cond_f
    if-lez v0, :cond_10

    .line 452
    if-lez p1, :cond_15

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    .line 454
    :goto_e
    iget-boolean v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v3, :cond_16

    .line 455
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v3

    .line 459
    :goto_f
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    int-to-float v3, v3

    div-float v3, v5, v3

    invoke-virtual {v0, v3}, Lnh;->a(F)Z

    .line 460
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 465
    :cond_10
    if-eqz v4, :cond_11

    .line 466
    invoke-direct {p0, v4}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e(I)V

    .line 469
    :cond_11
    if-eqz p1, :cond_12

    if-eqz v4, :cond_13

    :cond_12
    move v1, v2

    :cond_13
    return v1

    :cond_14
    move v4, v1

    move v0, v7

    .line 442
    goto :goto_d

    .line 452
    :cond_15
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    goto :goto_e

    .line 457
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v3

    goto :goto_f

    :cond_17
    move v0, v4

    goto :goto_c
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;Z)Z
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->x:Z

    return p1
.end method

.method private b(II)I
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 818
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    move v1, v0

    .line 820
    :goto_0
    add-int v4, v1, p2

    .line 823
    :goto_1
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    if-ge v0, v4, :cond_7

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    if-ge p1, v0, :cond_7

    .line 824
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c(I)Landroid/view/View;

    move-result-object v5

    .line 825
    if-nez v5, :cond_1

    .line 860
    :goto_2
    return v2

    .line 819
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 829
    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llji;

    .line 831
    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eq v3, p0, :cond_2

    .line 832
    iget-boolean v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->y:Z

    if-eqz v3, :cond_3

    .line 833
    const/4 v3, -0x1

    invoke-virtual {p0, v5, v3, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 839
    :cond_2
    :goto_3
    invoke-direct {p0, v5}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/view/View;)V

    .line 840
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 841
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 844
    iget-boolean v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v3, :cond_5

    .line 845
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    .line 846
    iget v8, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    iget v9, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v9, v6

    add-int/2addr v8, v9

    iput v8, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    .line 847
    iget-boolean v0, v0, Llji;->a:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v7

    div-int/lit8 v0, v0, 0x2

    .line 853
    :goto_4
    add-int/2addr v6, v3

    .line 854
    add-int/2addr v7, v0

    .line 856
    invoke-virtual {v5, v3, v0, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 857
    add-int/lit8 p1, p1, 0x1

    .line 858
    goto :goto_1

    .line 835
    :cond_3
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->addView(Landroid/view/View;)V

    goto :goto_3

    :cond_4
    move v0, v2

    .line 847
    goto :goto_4

    .line 849
    :cond_5
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    .line 850
    iget v8, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    iget v9, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v9, v7

    add-int/2addr v8, v9

    iput v8, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    .line 851
    iget-boolean v0, v0, Llji;->a:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v6

    div-int/lit8 v0, v0, 0x2

    :goto_5
    move v10, v3

    move v3, v0

    move v0, v10

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_5

    .line 860
    :cond_7
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    sub-int v2, v0, v1

    goto :goto_2
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;)Lljq;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    return-object v0
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 489
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e:I

    if-eq p1, v0, :cond_0

    .line 490
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e:I

    .line 491
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c:Lljj;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c:Lljj;

    invoke-interface {v0, p0, p1}, Lljj;->a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;I)V

    .line 495
    :cond_0
    return-void
.end method

.method private e(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 503
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c:Lljj;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c:Lljj;

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->r:I

    invoke-interface {v0, p0, v1, p1, v2}, Lljj;->a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;III)V

    .line 506
    :cond_0
    invoke-virtual {p0, v3, v3, v3, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->onScrollChanged(IIII)V

    .line 507
    return-void
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 586
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    move v0, v2

    .line 599
    :goto_0
    return v0

    .line 592
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_2

    .line 593
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingLeft()I

    move-result v1

    .line 594
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    .line 599
    :goto_1
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    if-lt v3, v1, :cond_3

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    if-gt v1, v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    .line 596
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingTop()I

    move-result v1

    .line 597
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v0, v3

    goto :goto_1

    :cond_3
    move v0, v2

    .line 599
    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 1030
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1031
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lljk;->a(Landroid/view/View;)V

    .line 1030
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1034
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->y:Z

    if-eqz v0, :cond_1

    .line 1035
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->removeAllViewsInLayout()V

    .line 1039
    :goto_1
    return-void

    .line 1037
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->removeAllViews()V

    goto :goto_1
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 1170
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingLeft()I

    move-result v0

    :goto_0
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    .line 1171
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    .line 1172
    return-void

    .line 1170
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingTop()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    if-eq v0, p1, :cond_0

    .line 159
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    .line 160
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->requestLayout()V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->invalidate()V

    .line 163
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/BaseAdapter;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 132
    invoke-virtual {p0, p1, v0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/widget/BaseAdapter;II)V

    .line 133
    return-void
.end method

.method public a(Landroid/widget/BaseAdapter;II)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 136
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    if-eqz v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 140
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f()V

    .line 141
    invoke-direct {p0, p3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f(I)V

    .line 142
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    invoke-virtual {v1}, Lljk;->a()V

    .line 143
    iput p2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    .line 144
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->r:I

    .line 145
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    .line 147
    iput-object p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    .line 148
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->x:Z

    .line 149
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    if-nez v1, :cond_2

    :goto_0
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lljk;->a(I)V

    .line 155
    :cond_1
    return-void

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public a(Lljj;)V
    .locals 1

    .prologue
    .line 478
    iput-object p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c:Lljj;

    .line 479
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e(I)V

    .line 480
    return-void
.end method

.method public a(Lljl;)V
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    iput-object p1, v0, Lljk;->a:Lljl;

    .line 511
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    .line 167
    return-void
.end method

.method public b()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 174
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    if-nez v1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return v0

    .line 178
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 179
    if-eqz v1, :cond_0

    .line 183
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llji;

    .line 184
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    iget v0, v0, Llji;->topMargin:I

    add-int/2addr v0, v2

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public b(I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 605
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_0

    move v0, v1

    move v2, p1

    .line 612
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v3

    .line 613
    :goto_1
    if-ge v1, v3, :cond_1

    .line 614
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 615
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v6

    add-int/2addr v6, v0

    .line 616
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v8

    add-int/2addr v8, v0

    .line 615
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 613
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, p1

    move v2, v1

    .line 610
    goto :goto_0

    .line 619
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    .line 620
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    .line 621
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 974
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->A:Z

    .line 975
    return-void
.end method

.method final c(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    .line 1001
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    invoke-virtual {v1, v0}, Lljk;->b(I)Landroid/view/View;

    move-result-object v0

    .line 1003
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p1, v0, p0}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1004
    if-eq v1, v0, :cond_0

    if-eqz v0, :cond_0

    .line 1006
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d:Lljk;

    invoke-virtual {v2, v0}, Lljk;->a(Landroid/view/View;)V

    .line 1009
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llji;

    .line 1011
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eq v2, p0, :cond_2

    .line 1012
    if-nez v0, :cond_3

    .line 1013
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d()Llji;

    move-result-object v0

    .line 1017
    :cond_1
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1020
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v2, p1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v2

    iput v2, v0, Llji;->b:I

    .line 1021
    return-object v1

    .line 1014
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1015
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llji;

    goto :goto_0
.end method

.method public c()V
    .locals 7

    .prologue
    .line 890
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v5

    .line 891
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v4

    .line 893
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    .line 895
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    .line 896
    :goto_0
    iget v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    if-ge v0, v2, :cond_1

    .line 897
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c(I)Landroid/view/View;

    move-result-object v3

    .line 899
    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/view/View;)V

    .line 900
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 901
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 903
    iget-boolean v6, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v6, :cond_0

    :goto_1
    add-int/2addr v1, v2

    .line 904
    iget v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v1, v2

    .line 905
    add-int/lit8 v0, v0, 0x1

    .line 906
    goto :goto_0

    :cond_0
    move v2, v3

    .line 903
    goto :goto_1

    .line 908
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    .line 909
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_2

    move v0, v4

    :goto_2
    sub-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 911
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(IZ)Z

    .line 912
    return-void

    :cond_2
    move v0, v5

    .line 909
    goto :goto_2
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1111
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v2

    .line 1112
    if-lez v2, :cond_2

    .line 1113
    mul-int/lit8 v0, v2, 0x64

    .line 1115
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1116
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1117
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1118
    if-lez v1, :cond_0

    .line 1119
    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    add-int/2addr v0, v1

    .line 1122
    :cond_0
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1123
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    .line 1124
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1125
    if-lez v1, :cond_1

    .line 1126
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int/2addr v0, v1

    .line 1131
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1140
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    .line 1141
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v2

    .line 1142
    if-ltz v1, :cond_0

    if-lez v2, :cond_0

    .line 1143
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1144
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1145
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 1146
    if-lez v2, :cond_0

    .line 1147
    mul-int/lit8 v1, v1, 0x64

    mul-int/lit8 v3, v3, 0x64

    div-int v2, v3, v2

    sub-int/2addr v1, v2

    .line 1148
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 1147
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1151
    :cond_0
    return v0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 3

    .prologue
    .line 1160
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    mul-int/lit8 v0, v0, 0x64

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1161
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getScrollX()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1163
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1166
    :cond_0
    return v0
.end method

.method public computeScroll()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 374
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v0}, Lljq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v0}, Lljq;->b()I

    move-result v0

    .line 376
    :goto_0
    int-to-float v2, v0

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 377
    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    .line 378
    invoke-direct {p0, v2, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 380
    :goto_1
    if-nez v0, :cond_3

    iget-object v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v3}, Lljq;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 381
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 396
    :cond_0
    :goto_2
    return-void

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v0}, Lljq;->c()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 378
    goto :goto_1

    .line 383
    :cond_3
    if-eqz v0, :cond_5

    .line 384
    invoke-static {p0}, Liu;->a(Landroid/view/View;)I

    move-result v0

    .line 385
    const/4 v3, 0x2

    if-eq v0, v3, :cond_4

    .line 386
    if-lez v2, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    .line 388
    :goto_3
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v2}, Lljq;->d()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lnh;->a(I)Z

    .line 389
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 391
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v0}, Lljq;->f()V

    .line 393
    :cond_5
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d(I)V

    goto :goto_2

    .line 386
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    goto :goto_3
.end method

.method protected computeVerticalScrollExtent()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1047
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v2

    .line 1048
    if-lez v2, :cond_2

    .line 1049
    mul-int/lit8 v0, v2, 0x64

    .line 1051
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1052
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 1053
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1054
    if-lez v1, :cond_0

    .line 1055
    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    add-int/2addr v0, v1

    .line 1058
    :cond_0
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1059
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 1060
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1061
    if-lez v1, :cond_1

    .line 1062
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int/2addr v0, v1

    .line 1067
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1076
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    .line 1077
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v2

    .line 1078
    if-ltz v1, :cond_0

    if-lez v2, :cond_0

    .line 1079
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1080
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    .line 1081
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1082
    if-lez v2, :cond_0

    .line 1083
    mul-int/lit8 v1, v1, 0x64

    mul-int/lit8 v3, v3, 0x64

    div-int v2, v3, v2

    sub-int/2addr v1, v2

    .line 1084
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 1083
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1087
    :cond_0
    return v0
.end method

.method protected computeVerticalScrollRange()I
    .locals 3

    .prologue
    .line 1096
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    mul-int/lit8 v0, v0, 0x64

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1097
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getScrollY()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1099
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1102
    :cond_0
    return v0
.end method

.method protected d()Llji;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1026
    new-instance v0, Llji;

    invoke-direct {v0, v1, v1}, Llji;-><init>(II)V

    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 625
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 627
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    .line 628
    const/4 v0, 0x0

    .line 629
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    invoke-virtual {v2}, Lnh;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 630
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_2

    .line 631
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 632
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 633
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v2, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 634
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    invoke-virtual {v2, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 635
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :goto_0
    move v0, v1

    .line 641
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    invoke-virtual {v2}, Lnh;->a()Z

    move-result v2

    if-nez v2, :cond_4

    .line 642
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_3

    .line 643
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 644
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 645
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v5, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 646
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    invoke-virtual {v2, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 647
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 659
    :goto_1
    if-eqz v1, :cond_1

    .line 660
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 663
    :cond_1
    return-void

    .line 637
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    invoke-virtual {v0, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    goto :goto_0

    .line 649
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 650
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v2

    .line 651
    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 652
    const/high16 v3, 0x43340000    # 180.0f

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 653
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    invoke-virtual {v2, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 654
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d()Llji;

    move-result-object v0

    return-object v0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/16 v4, 0x9

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 332
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xc

    if-lt v0, v3, :cond_0

    .line 333
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 358
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 338
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 339
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 345
    :goto_1
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 346
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->i:I

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Llii;->a(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->i:I

    :cond_1
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->i:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 347
    invoke-direct {p0, v0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 349
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 351
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->awakenScrollBars()Z

    move v0, v2

    .line 352
    goto :goto_0

    :cond_3
    move v0, v1

    .line 339
    goto :goto_1

    .line 341
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_5

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    goto :goto_1

    .line 342
    :cond_5
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    goto :goto_1

    .line 335
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 190
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 191
    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v3

    .line 235
    :goto_0
    return v0

    .line 193
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 194
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v0}, Lljq;->f()V

    .line 195
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    .line 196
    invoke-static {p1, v3}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->l:I

    .line 197
    iput v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->k:F

    .line 198
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 200
    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d(I)V

    move v0, v2

    .line 201
    goto :goto_0

    .line 195
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_1

    .line 207
    :pswitch_2
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->l:I

    invoke-static {p1, v0}, Lik;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 208
    if-gez v0, :cond_2

    .line 209
    const-string v0, "RecyclingViewGroup"

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->l:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x7f

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - did RecyclingViewGroup receive an inconsistent event stream?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 212
    goto :goto_0

    .line 214
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v1, :cond_4

    invoke-static {p1, v0}, Lik;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 216
    :goto_2
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->k:F

    add-float/2addr v1, v0

    .line 217
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_5

    move v4, v2

    .line 218
    :goto_3
    if-eqz v4, :cond_7

    .line 219
    cmpl-float v0, v1, v5

    if-lez v0, :cond_6

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f:I

    neg-int v0, v0

    int-to-float v0, v0

    :goto_4
    add-float/2addr v0, v1

    .line 221
    :goto_5
    float-to-int v1, v0

    .line 222
    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->k:F

    .line 224
    if-eqz v4, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_3

    .line 227
    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_3
    move v0, v2

    .line 229
    goto/16 :goto_0

    .line 215
    :cond_4
    invoke-static {p1, v0}, Lik;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    goto :goto_2

    :cond_5
    move v4, v3

    .line 217
    goto :goto_3

    .line 219
    :cond_6
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f:I

    int-to-float v0, v0

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_5

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 674
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->y:Z

    .line 675
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 676
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->y:Z

    .line 678
    sub-int v0, p4, p2

    .line 679
    sub-int v1, p5, p3

    .line 680
    iget-boolean v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v2, :cond_e

    .line 681
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    invoke-virtual {v2, v1, v0}, Lnh;->a(II)V

    .line 682
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    invoke-virtual {v2, v1, v0}, Lnh;->a(II)V

    .line 687
    :goto_1
    return-void

    .line 675
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->z:Z

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->x:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f()V

    :cond_2
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b(II)I

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    add-int/lit8 v1, v0, -0x1

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingLeft()I

    move-result v0

    :goto_2
    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(II)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->z:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->x:Z

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v0

    :goto_3
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    if-ge v1, v0, :cond_3

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    if-gtz v1, :cond_3

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    sub-int v3, v0, v1

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->u:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-lt v2, v3, :cond_b

    move v0, v3

    :goto_4
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(IZ)Z

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_5
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llji;

    invoke-virtual {v4}, Landroid/view/View;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, v4}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/view/View;)V

    :cond_5
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v1, :cond_7

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    iget v7, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    iget v8, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v8, v5

    add-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    iget-boolean v0, v0, Llji;->a:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v6

    div-int/lit8 v0, v0, 0x2

    :goto_6
    add-int/2addr v5, v1

    add-int/2addr v6, v0

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    goto :goto_6

    :cond_7
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    iget v7, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    iget v8, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v8, v6

    add-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->v:I

    iget-boolean v0, v0, Llji;->a:Z

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    :goto_7
    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_6

    :cond_8
    const/4 v0, 0x0

    goto :goto_7

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getPaddingTop()I

    move-result v0

    goto/16 :goto_2

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v0

    goto/16 :goto_3

    :cond_b
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->s:I

    if-lez v0, :cond_d

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->q:I

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_8
    if-ltz v4, :cond_d

    if-ge v2, v3, :cond_d

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_f

    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-boolean v5, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v5, :cond_c

    :goto_9
    add-int/2addr v0, v2

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->t:I

    add-int/2addr v0, v1

    :goto_a
    add-int/lit8 v1, v4, -0x1

    move v4, v1

    move v2, v0

    goto :goto_8

    :cond_c
    move v0, v1

    goto :goto_9

    :cond_d
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto/16 :goto_4

    .line 684
    :cond_e
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    invoke-virtual {v2, v0, v1}, Lnh;->a(II)V

    .line 685
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    invoke-virtual {v2, v0, v1}, Lnh;->a(II)V

    goto/16 :goto_1

    :cond_f
    move v0, v2

    goto :goto_a
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    .line 979
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->A:Z

    if-eqz v0, :cond_0

    .line 980
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v2

    .line 981
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getWidth()I

    move-result v0

    move v1, v0

    .line 982
    :goto_0
    if-lez v2, :cond_0

    .line 983
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 984
    iget-boolean v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 985
    :goto_1
    sub-int v0, v1, v0

    .line 986
    if-lez v0, :cond_0

    .line 987
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(IZ)Z

    .line 991
    :cond_0
    return-void

    .line 981
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getHeight()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 984
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->B:Z

    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->awakenScrollBars()Z

    .line 242
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 245
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 246
    packed-switch v0, :pswitch_data_0

    .line 325
    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 248
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v0}, Lljq;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 249
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_2

    .line 251
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 256
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    invoke-virtual {v0}, Lljq;->f()V

    .line 257
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_2
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    .line 258
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->l:I

    .line 259
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->k:F

    goto :goto_0

    .line 257
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_2

    .line 264
    :pswitch_1
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->l:I

    invoke-static {p1, v0}, Lik;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 265
    if-gez v0, :cond_4

    .line 266
    const-string v0, "RecyclingViewGroup"

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->l:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x7e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - did StaggeredGridView receive an inconsistent event stream?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const/4 v0, 0x0

    goto :goto_1

    .line 271
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v1, :cond_6

    invoke-static {p1, v0}, Lik;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 273
    :goto_3
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    sub-float v1, v0, v1

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->k:F

    add-float/2addr v2, v1

    .line 274
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e:I

    if-nez v1, :cond_b

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_b

    .line 275
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 276
    if-eqz v1, :cond_5

    .line 277
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 279
    :cond_5
    const/4 v1, 0x0

    cmpl-float v1, v2, v1

    if-lez v1, :cond_7

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f:I

    neg-int v1, v1

    int-to-float v1, v1

    :goto_4
    add-float/2addr v1, v2

    .line 280
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d(I)V

    .line 282
    :goto_5
    float-to-int v2, v1

    .line 283
    int-to-float v3, v2

    sub-float/2addr v1, v3

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->k:F

    .line 285
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->e:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 286
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    .line 288
    const/4 v0, 0x1

    invoke-direct {p0, v2, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_0

    .line 272
    :cond_6
    invoke-static {p1, v0}, Lik;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    goto :goto_3

    .line 279
    :cond_7
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->f:I

    int-to-float v1, v1

    goto :goto_4

    .line 297
    :pswitch_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d(I)V

    .line 321
    :goto_6
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->o:Lnh;

    invoke-virtual {v0}, Lnh;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->p:Lnh;

    invoke-virtual {v1}, Lnh;->c()Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_1

    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    goto/16 :goto_0

    .line 303
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->g:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 304
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->l:I

    .line 305
    invoke-static {v0, v1}, Liq;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    move v4, v0

    .line 307
    :goto_7
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->h:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_a

    .line 308
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d(I)V

    .line 309
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->w:Z

    if-eqz v0, :cond_9

    .line 310
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    const/4 v1, 0x0

    const/4 v2, 0x0

    float-to-int v3, v4

    const/4 v4, 0x0

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Lljq;->a(IIIIIIII)V

    .line 316
    :goto_8
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->j:F

    .line 317
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    goto :goto_6

    .line 305
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->m:Landroid/view/VelocityTracker;

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->l:I

    .line 306
    invoke-static {v0, v1}, Liq;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    move v4, v0

    goto :goto_7

    .line 313
    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->n:Lljq;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/high16 v7, -0x80000000

    const v8, 0x7fffffff

    invoke-virtual/range {v0 .. v8}, Lljq;->a(IIIIIIII)V

    goto :goto_8

    .line 319
    :cond_a
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->d(I)V

    goto :goto_6

    :cond_b
    move v1, v2

    goto/16 :goto_5

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 667
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->z:Z

    if-nez v0, :cond_0

    .line 668
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 670
    :cond_0
    return-void
.end method
