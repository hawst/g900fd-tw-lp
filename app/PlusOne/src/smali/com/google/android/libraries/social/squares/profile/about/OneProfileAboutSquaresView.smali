.class public Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;
.super Ljnl;
.source "PG"

# interfaces
.implements Lhmm;


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private o:Landroid/widget/TextView;

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/libraries/social/squares/list/SquareListItemView;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/text/NumberFormat;

.field private r:Lhmk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->p:Ljava/util/ArrayList;

    .line 50
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->q:Ljava/text/NumberFormat;

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->p:Ljava/util/ArrayList;

    .line 50
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->q:Ljava/text/NumberFormat;

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->p:Ljava/util/ArrayList;

    .line 50
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->q:Ljava/text/NumberFormat;

    .line 89
    return-void
.end method

.method private a(Lnts;)V
    .locals 10

    .prologue
    const/4 v8, 0x3

    const/4 v4, 0x0

    .line 165
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->n:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 167
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->o:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    .line 169
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 171
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lkvq;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkvq;

    move v5, v4

    .line 172
    :goto_0
    if-ge v5, v8, :cond_2

    iget-object v2, p1, Lnts;->a:[Lnsr;

    array-length v2, v2

    if-ge v5, v2, :cond_2

    .line 173
    const v2, 0x7f0401f7

    iget-object v3, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/squares/list/SquareListItemView;

    .line 175
    invoke-virtual {v2, v4}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->setBackgroundColor(I)V

    .line 176
    iget-object v3, p1, Lnts;->a:[Lnsr;

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Lnsr;)V

    .line 177
    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Lkvq;)V

    .line 178
    invoke-virtual {v2, v4, v4, v4, v4}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->setPadding(IIII)V

    const v3, 0x7f1001e6

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    sget v6, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->f:I

    sget v7, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->f:I

    invoke-virtual {v3, v6, v4, v7, v4}, Landroid/view/View;->setPadding(IIII)V

    const v3, 0x7f1005b9

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    sget v6, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->f:I

    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    sget v6, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->f:I

    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    :cond_0
    const v3, 0x7f1005b3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a(Landroid/widget/TextView;)V

    .line 179
    iget-object v3, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 180
    iget-object v3, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v2, p1, Lnts;->a:[Lnsr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v5, v2, :cond_1

    .line 182
    const v2, 0x7f040133

    iget-object v3, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 183
    sget v3, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->h:I

    sget v6, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->h:I

    invoke-virtual {v2, v4, v3, v4, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 172
    :cond_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_0

    .line 188
    :cond_2
    iget-object v0, p1, Lnts;->a:[Lnsr;

    array-length v0, v0

    if-le v0, v8, :cond_4

    .line 189
    iget-object v0, p1, Lnts;->a:[Lnsr;

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->d(Landroid/widget/TextView;)V

    const v3, 0x7f0a0353

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->q:Ljava/text/NumberFormat;

    int-to-long v8, v0

    invoke-virtual {v6, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v4

    invoke-virtual {p0, v3, v5}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b013d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    sget v0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->h:I

    sget v3, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->h:I

    invoke-virtual {v2, v0, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_3

    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextAlignment(I)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020416

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Lhmk;

    sget-object v3, Lomv;->ag:Lhmn;

    invoke-direct {v0, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v2, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    new-instance v0, Lhmi;

    new-instance v3, Lkwm;

    invoke-direct {v3, p0}, Lkwm;-><init>(Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;)V

    invoke-direct {v0, v3}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->f:I

    sget v3, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->f:I

    sget v5, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->f:I

    sget v6, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->f:I

    invoke-virtual {v2, v0, v3, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v0, v4

    .line 194
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4, v4, v4, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 195
    new-instance v0, Lhmk;

    sget-object v1, Lomv;->m:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->r:Lhmk;

    .line 196
    return-void

    .line 191
    :cond_4
    sget v0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->g:I

    goto :goto_1
.end method

.method public static a(ZLnts;Lnto;Lnso;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 72
    if-eqz p0, :cond_2

    .line 73
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 73
    goto :goto_0

    .line 75
    :cond_2
    if-eqz p1, :cond_3

    iget-object v2, p1, Lnts;->a:[Lnsr;

    invoke-static {v2}, Llrv;->a([Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;

    .line 102
    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a()V

    goto :goto_0

    .line 104
    :cond_0
    return-void
.end method

.method public a(Lnjt;)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public a(Lnts;Lnto;Lnso;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 113
    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->j:Z

    if-eqz v0, :cond_4

    .line 115
    iget-object v0, p2, Lnto;->a:[Lnrx;

    invoke-static {v0}, Llrv;->a([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401c1

    iget-object v2, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f10017d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->M:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    new-instance v2, Lhmi;

    new-instance v3, Lkwl;

    invoke-direct {v3, p0}, Lkwl;-><init>(Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;)V

    invoke-direct {v2, v3}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Lhmk;

    sget-object v1, Lomv;->r:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->r:Lhmk;

    .line 130
    :cond_0
    :goto_0
    invoke-static {p0}, Lhmc;->a(Landroid/view/View;)V

    .line 131
    return-void

    .line 118
    :cond_1
    iget v0, p3, Lnso;->a:I

    if-eq v0, v1, :cond_2

    iget v0, p3, Lnso;->b:I

    if-ne v0, v1, :cond_3

    iget-object v0, p3, Lnso;->c:[Ljava/lang/String;

    .line 120
    invoke-static {v0}, Llrv;->a([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v0, Lhmk;

    sget-object v1, Lomv;->p:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->r:Lhmk;

    goto :goto_0

    .line 124
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a(Lnts;)V

    goto :goto_0

    .line 126
    :cond_4
    iget-object v0, p1, Lnts;->a:[Lnsr;

    invoke-static {v0}, Llrv;->a([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a(Lnts;)V

    goto :goto_0
.end method

.method public ac_()Lhmk;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->r:Lhmk;

    return-object v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 219
    const/16 v0, 0x11

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 94
    const v0, 0x7f1001e9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a:Landroid/view/ViewGroup;

    .line 95
    const v0, 0x7f1003e5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->o:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->o:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->e(Landroid/widget/TextView;)V

    .line 97
    return-void
.end method
