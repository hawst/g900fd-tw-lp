.class public Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;
.super Lhgy;
.source "PG"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/view/ViewGroup;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/view/View;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:F

.field private q:F

.field private r:I

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->b()V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 82
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Landroid/util/AttributeSet;)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->b()V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    .line 95
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Landroid/util/AttributeSet;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->b()V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 109
    invoke-direct {p0, p1, p2, p3, p4}, Lhgy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    .line 48
    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->l:Z

    .line 51
    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->m:Z

    .line 52
    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->n:Z

    .line 53
    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->o:Z

    .line 54
    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->p:F

    .line 55
    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->q:F

    .line 56
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->r:I

    .line 57
    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->s:F

    .line 58
    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->t:F

    .line 59
    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->u:F

    .line 60
    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->v:F

    .line 61
    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->w:F

    .line 110
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Landroid/util/AttributeSet;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->b()V

    .line 112
    return-void
.end method

.method private a(FI)I
    .locals 1

    .prologue
    .line 475
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    :goto_0
    return p2

    :cond_0
    float-to-int p2, p1

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0x7f7fffff    # Float.MAX_VALUE

    .line 127
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lhht;->a:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 128
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->m:Z

    .line 129
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->n:Z

    .line 130
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->o:Z

    .line 131
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->q:F

    .line 132
    const/4 v1, 0x5

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->r:I

    .line 133
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->s:F

    .line 134
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->t:F

    .line 136
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->u:F

    .line 138
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->v:F

    .line 140
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->w:F

    .line 143
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->p:F

    .line 144
    iget-object v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x3

    const v3, 0x7f04004b

    .line 146
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 149
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 150
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 155
    const v0, 0x7f04004d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->addView(Landroid/view/View;)V

    .line 156
    const v0, 0x7f1001a8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    .line 158
    const v0, 0x7f1001a6

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    .line 159
    const v0, 0x7f1001aa

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h:Landroid/widget/ImageView;

    .line 160
    const v0, 0x7f1001a7

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->i:Landroid/view/ViewGroup;

    .line 161
    const v0, 0x7f1001a9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->k:Landroid/view/View;

    .line 162
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->i:Landroid/view/ViewGroup;

    const v1, 0x1020014

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 188
    return-void
.end method

.method protected a(IIILjava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 168
    return-void
.end method

.method protected b()V
    .locals 28

    .prologue
    .line 247
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v20

    .line 248
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    .line 249
    const v3, 0x7f0a0532

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 251
    const v3, 0x104000e

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 252
    const v3, 0x7f0a057b

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 253
    const v3, 0x7f0a0533

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 255
    const v3, 0x7f0a0528

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 256
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    .line 258
    if-nez v20, :cond_8

    .line 260
    const/4 v3, 0x0

    .line 261
    const/16 v4, 0x8

    .line 427
    :goto_0
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 428
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 429
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->invalidate()V

    .line 432
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 435
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->m:Z

    if-eqz v6, :cond_0

    const/16 v4, 0x8

    :cond_0
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 436
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 437
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->k:Landroid/view/View;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->n:Z

    if-eqz v3, :cond_2c

    const/16 v3, 0x8

    :goto_1
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 438
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->o:Z

    if-eqz v3, :cond_2d

    const/16 v3, 0x8

    :goto_2
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 440
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->q:F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_1

    .line 443
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->q:F

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 446
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->r:I

    const v4, 0x7fffffff

    if-eq v3, v4, :cond_2

    .line 447
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->r:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 450
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->s:F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_3

    .line 451
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->s:F

    float-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    .line 452
    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->j:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v7

    .line 451
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 455
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->p:F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_4

    .line 456
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->p:F

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 459
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->t:F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v4

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->u:F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v4

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->v:F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v4

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->w:F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_6

    .line 461
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->t:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    .line 462
    invoke-virtual {v5}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(FI)I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->v:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    .line 463
    invoke-virtual {v6}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(FI)I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->u:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    .line 464
    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(FI)I

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->w:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    .line 465
    invoke-virtual {v8}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(FI)I

    move-result v7

    .line 461
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 469
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->e:Ljava/lang/Runnable;

    if-eqz v3, :cond_7

    .line 470
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->e:Ljava/lang/Runnable;

    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    .line 472
    :cond_7
    return-void

    .line 264
    :cond_8
    const/4 v9, 0x0

    .line 265
    const/4 v8, 0x0

    .line 266
    const/16 v18, 0x0

    .line 267
    const/16 v17, 0x0

    .line 268
    const/16 v16, 0x0

    .line 269
    const/4 v15, 0x0

    .line 270
    const/4 v7, 0x0

    .line 271
    const/4 v6, 0x0

    .line 272
    const/4 v5, 0x0

    .line 273
    const/4 v4, 0x0

    .line 275
    const/4 v3, 0x0

    move/from16 v19, v3

    :goto_3
    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_18

    .line 276
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->b:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhgw;

    .line 279
    invoke-virtual {v3}, Lhgw;->h()I

    move-result v11

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v11, v0, :cond_f

    .line 281
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lhgw;->b(I)Lhxc;

    move-result-object v11

    .line 282
    invoke-virtual {v11}, Lhxc;->c()I

    move-result v3

    .line 284
    const/16 v24, 0x9

    move/from16 v0, v24

    if-ne v3, v0, :cond_a

    .line 285
    const/4 v4, 0x1

    .line 288
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->getContext()Landroid/content/Context;

    move-result-object v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->d:I

    move/from16 v24, v0

    .line 287
    move/from16 v0, v24

    invoke-static {v9, v0, v3}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v3

    move/from16 v27, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v4

    move/from16 v4, v27

    .line 302
    :goto_4
    invoke-virtual {v11}, Lhxc;->b()Ljava/lang/String;

    move-result-object v9

    .line 303
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_e

    :goto_5
    move/from16 v11, v17

    move/from16 v17, v8

    move v8, v15

    move/from16 v15, v18

    move/from16 v27, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move-object v3, v9

    move/from16 v9, v16

    move/from16 v16, v7

    move/from16 v7, v27

    .line 341
    :goto_6
    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    add-int/lit8 v3, v20, -0x1

    move/from16 v0, v19

    if-ge v0, v3, :cond_9

    .line 343
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    :cond_9
    add-int/lit8 v3, v19, 0x1

    move/from16 v19, v3

    move/from16 v18, v15

    move v15, v8

    move/from16 v8, v16

    move/from16 v16, v9

    move/from16 v9, v17

    move/from16 v17, v11

    goto/16 :goto_3

    .line 289
    :cond_a
    const/16 v24, 0x8

    move/from16 v0, v24

    if-ne v3, v0, :cond_b

    .line 290
    const/4 v3, 0x1

    move v8, v9

    move/from16 v27, v5

    move v5, v6

    move v6, v7

    move v7, v3

    move v3, v4

    move/from16 v4, v27

    goto :goto_4

    .line 291
    :cond_b
    const/16 v24, 0x7

    move/from16 v0, v24

    if-ne v3, v0, :cond_c

    .line 292
    const/4 v4, 0x1

    .line 295
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->getContext()Landroid/content/Context;

    move-result-object v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->d:I

    move/from16 v24, v0

    .line 294
    move/from16 v0, v24

    invoke-static {v7, v0, v3}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v3

    move v7, v8

    move v8, v9

    move/from16 v27, v4

    move v4, v5

    move v5, v6

    move/from16 v6, v27

    goto :goto_4

    .line 296
    :cond_c
    const/16 v24, 0x5

    move/from16 v0, v24

    if-ne v3, v0, :cond_d

    .line 297
    const/4 v3, 0x1

    move v6, v7

    move v7, v8

    move v8, v9

    move/from16 v27, v4

    move v4, v5

    move v5, v3

    move/from16 v3, v27

    goto/16 :goto_4

    .line 298
    :cond_d
    const/16 v24, 0x65

    move/from16 v0, v24

    if-ne v3, v0, :cond_2e

    .line 299
    const/4 v3, 0x1

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move/from16 v27, v4

    move v4, v3

    move/from16 v3, v27

    goto/16 :goto_4

    :cond_e
    move-object v9, v10

    .line 303
    goto/16 :goto_5

    .line 304
    :cond_f
    invoke-virtual {v3}, Lhgw;->g()I

    move-result v11

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v11, v0, :cond_12

    .line 306
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lhgw;->a(I)Ljqs;

    move-result-object v11

    .line 307
    invoke-virtual {v11}, Ljqs;->b()Ljava/lang/String;

    move-result-object v3

    .line 308
    invoke-virtual {v11}, Ljqs;->c()Ljava/lang/String;

    move-result-object v11

    .line 309
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_10

    :goto_7
    move/from16 v11, v17

    move/from16 v17, v9

    move/from16 v9, v16

    move/from16 v16, v8

    move v8, v15

    move/from16 v15, v18

    .line 311
    goto/16 :goto_6

    .line 310
    :cond_10
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_11

    move-object v3, v11

    goto :goto_7

    :cond_11
    move-object v3, v12

    goto :goto_7

    .line 311
    :cond_12
    invoke-virtual {v3}, Lhgw;->i()I

    move-result v11

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v11, v0, :cond_15

    .line 313
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lhgw;->c(I)Lkxr;

    move-result-object v11

    .line 314
    invoke-virtual {v11}, Lkxr;->b()Ljava/lang/String;

    move-result-object v3

    .line 315
    invoke-virtual {v11}, Lkxr;->d()Ljava/lang/String;

    move-result-object v18

    .line 316
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_13

    move-object v3, v13

    .line 319
    :cond_13
    const/16 v17, 0x1

    .line 320
    invoke-virtual {v11}, Lkxr;->e()Z

    move-result v11

    .line 321
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_14

    move/from16 v27, v15

    move/from16 v15, v17

    move/from16 v17, v9

    move/from16 v9, v16

    move/from16 v16, v8

    move/from16 v8, v27

    .line 322
    goto/16 :goto_6

    .line 324
    :cond_14
    const v24, 0x7f0a0534

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object v3, v25, v26

    const/4 v3, 0x1

    aput-object v18, v25, v3

    move-object/from16 v0, v21

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move/from16 v27, v15

    move/from16 v15, v17

    move/from16 v17, v9

    move/from16 v9, v16

    move/from16 v16, v8

    move/from16 v8, v27

    .line 326
    goto/16 :goto_6

    :cond_15
    invoke-virtual {v3}, Lhgw;->j()I

    move-result v11

    const/4 v15, 0x1

    if-ne v11, v15, :cond_17

    .line 328
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lhgw;->d(I)Lhxs;

    move-result-object v11

    .line 329
    invoke-virtual {v11}, Lhxs;->b()Ljava/lang/String;

    move-result-object v3

    .line 330
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_16

    move-object v3, v14

    .line 333
    :cond_16
    const/4 v15, 0x1

    .line 334
    invoke-virtual {v11}, Lhxs;->c()Z

    move-result v11

    move/from16 v16, v8

    move v8, v11

    move/from16 v11, v17

    move/from16 v17, v9

    move v9, v15

    move/from16 v15, v18

    .line 336
    goto/16 :goto_6

    .line 338
    :cond_17
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    .line 357
    :cond_18
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->g:Z

    if-eqz v3, :cond_1b

    .line 358
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->l:Z

    if-eqz v3, :cond_1a

    .line 359
    const v3, 0x7f0204fc

    .line 419
    :cond_19
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 422
    const/16 v3, 0x8

    .line 423
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 361
    :cond_1a
    const v3, 0x7f0201a9

    goto :goto_8

    .line 365
    :cond_1b
    if-eqz v9, :cond_1e

    .line 366
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->l:Z

    if-eqz v3, :cond_1c

    .line 367
    const v3, 0x7f02053e

    .line 380
    :goto_9
    if-eqz v18, :cond_23

    .line 381
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->l:Z

    if-eqz v3, :cond_21

    .line 382
    const v3, 0x7f0204f6

    goto :goto_8

    .line 369
    :cond_1c
    if-eqz v4, :cond_1d

    const v3, 0x7f020312

    goto :goto_9

    :cond_1d
    const v3, 0x7f02030e

    goto :goto_9

    .line 373
    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->l:Z

    if-eqz v3, :cond_1f

    .line 374
    const v3, 0x7f02052e

    goto :goto_9

    .line 376
    :cond_1f
    if-eqz v4, :cond_20

    const v3, 0x7f0202f4

    goto :goto_9

    :cond_20
    const v3, 0x7f0202f0

    goto :goto_9

    .line 384
    :cond_21
    if-eqz v17, :cond_22

    .line 385
    const v3, 0x7f0201a9

    goto :goto_8

    .line 387
    :cond_22
    const v3, 0x7f0202db

    goto :goto_8

    .line 390
    :cond_23
    if-eqz v16, :cond_25

    .line 391
    if-eqz v15, :cond_24

    .line 392
    const v3, 0x7f0201a9

    goto :goto_8

    .line 394
    :cond_24
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "iconic_ic_clx_24"

    const-string v5, "drawable"

    .line 395
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 394
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto :goto_8

    .line 397
    :cond_25
    const/4 v9, 0x1

    move/from16 v0, v20

    if-ne v0, v9, :cond_19

    .line 398
    if-eqz v8, :cond_26

    .line 399
    const v3, 0x7f020413

    goto :goto_8

    .line 400
    :cond_26
    if-eqz v7, :cond_29

    .line 401
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->l:Z

    if-eqz v3, :cond_27

    .line 402
    const v3, 0x7f0204dc

    goto/16 :goto_8

    .line 404
    :cond_27
    if-eqz v4, :cond_28

    const v3, 0x7f0201cc

    goto/16 :goto_8

    :cond_28
    const v3, 0x7f0201c9

    goto/16 :goto_8

    .line 407
    :cond_29
    if-eqz v6, :cond_2b

    .line 408
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->l:Z

    if-eqz v3, :cond_2a

    .line 409
    const v3, 0x7f0204df

    goto/16 :goto_8

    .line 411
    :cond_2a
    const v3, 0x7f02017f

    goto/16 :goto_8

    .line 413
    :cond_2b
    if-eqz v5, :cond_19

    .line 415
    const v3, 0x7f02030a

    goto/16 :goto_8

    .line 437
    :cond_2c
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 438
    :cond_2d
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_2e
    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_4
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 193
    sget-object v0, Lhhu;->a:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 208
    const v0, 0x7f0201ad

    .line 215
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 219
    :cond_0
    return-void

    .line 195
    :pswitch_0
    const v0, 0x7f020271

    .line 196
    goto :goto_0

    .line 199
    :pswitch_1
    const v0, 0x7f020327

    .line 200
    goto :goto_0

    .line 203
    :pswitch_2
    const v0, 0x7f0203b0

    .line 204
    goto :goto_0

    .line 193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->m:Z

    .line 116
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 237
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->l:Z

    .line 124
    return-void
.end method

.method protected e(I)V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method public f(I)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 229
    :cond_0
    return-void
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    return v0
.end method

.method protected j()V
    .locals 0

    .prologue
    .line 178
    return-void
.end method
