.class public Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;
.super Lu;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final N:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation
.end field

.field private O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

.field private P:Ljava/lang/CharSequence;

.field private Q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lu;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->N:Ljava/util/ArrayList;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->Q:Z

    return-void
.end method


# virtual methods
.method public U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public W()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a()V

    .line 133
    return-void
.end method

.method public X()V
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->Q:Z

    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->a(Lkjx;)V

    .line 159
    return-void
.end method

.method public Y()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->requestFocus()Z

    .line 167
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->b()V

    .line 168
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-static {v0}, Llsn;->a(Landroid/view/View;)V

    .line 169
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 36
    if-eqz p3, :cond_0

    .line 37
    const-string v0, "GENERATED_TEXT"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->P:Ljava/lang/CharSequence;

    .line 38
    const-string v0, "URL_CHECKING_ENABLED"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->Q:Z

    .line 40
    :cond_0
    const v0, 0x7f0401d6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 42
    const v0, 0x7f100561

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->a(Z)V

    .line 46
    return-object v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->setCursorVisible(Z)V

    .line 69
    return-void
.end method

.method public a(ILkjm;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, v1, p2}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->a(Lu;ILjava/lang/String;Lkjm;)V

    .line 57
    return-void
.end method

.method public a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 163
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->N:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->setText(Ljava/lang/CharSequence;)V

    .line 65
    return-void
.end method

.method public a(Lkjx;)V
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->Q:Z

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->a(Lkjx;)V

    .line 151
    :cond_0
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    .line 52
    invoke-super {p0}, Lu;->ae_()V

    .line 53
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->setCursorVisible(Z)V

    .line 73
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iput-object p1, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->P:Ljava/lang/CharSequence;

    .line 102
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(Ljava/lang/CharSequence;)V

    .line 104
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->P:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    .line 92
    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->P:Ljava/lang/CharSequence;

    .line 109
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(Ljava/lang/CharSequence;)V

    .line 111
    :cond_0
    return-void
.end method

.method public e()Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->O:Lcom/google/android/libraries/social/sharekit/comments/CommentBox;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/CommentBox;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, Lu;->e(Landroid/os/Bundle;)V

    .line 82
    const-string v0, "GENERATED_TEXT"

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->P:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 83
    const-string v0, "URL_CHECKING_ENABLED"

    iget-boolean v1, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->Q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 84
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    .line 174
    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 176
    :cond_0
    return-void
.end method
