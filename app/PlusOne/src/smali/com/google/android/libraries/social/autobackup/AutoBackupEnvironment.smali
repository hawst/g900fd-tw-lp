.class public final Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static e:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;Landroid/content/Intent;)Z

    .line 65
    invoke-static {p0, p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;)Z

    .line 66
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;
    .locals 2

    .prologue
    .line 35
    const-class v1, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->e:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->e:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 38
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->e:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 111
    const/4 v1, 0x0

    .line 113
    const-class v0, Ljgn;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    .line 115
    iget-boolean v3, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b:Z

    .line 116
    invoke-interface {v0}, Ljgn;->f()Z

    move-result v4

    .line 117
    if-eq v4, v3, :cond_0

    .line 119
    iput-boolean v4, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b:Z

    move v1, v2

    .line 121
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c:Z

    .line 122
    invoke-interface {v0}, Ljgn;->g()Z

    move-result v6

    .line 123
    if-eq v6, v5, :cond_1

    .line 125
    iput-boolean v6, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c:Z

    move v1, v2

    .line 127
    :cond_1
    iget-boolean v7, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d:Z

    .line 128
    invoke-interface {v0}, Ljgn;->h()Z

    move-result v8

    .line 129
    if-eq v8, v7, :cond_6

    .line 131
    iput-boolean v8, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d:Z

    .line 134
    :goto_0
    const-string v0, "iu.Environment"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135
    if-eq v3, v4, :cond_3

    const-string v0, "*"

    move-object v3, v0

    :goto_1
    if-eq v5, v6, :cond_4

    const-string v0, "*"

    move-object v1, v0

    :goto_2
    if-eq v7, v8, :cond_5

    const-string v0, "*"

    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x64

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v7, v9

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v7, v9

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "update connectivity state; isNetworkMetered? "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isRoaming? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", isBackgroundDataAllowed? "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_2
    return v2

    .line 135
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto :goto_1

    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto :goto_2

    :cond_5
    const-string v0, ""

    goto :goto_3

    :cond_6
    move v2, v1

    goto :goto_0
.end method

.method static a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    .line 72
    iget-boolean v3, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    .line 73
    if-nez p2, :cond_4

    .line 75
    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 78
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p1, v5, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ReceiverCallNotAllowedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 86
    :goto_0
    if-eqz v2, :cond_0

    .line 87
    const-string v4, "plugged"

    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 88
    if-lez v2, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    .line 89
    const-string v0, "iu.Environment"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v4, "starting battery state: "

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v5, 0x15

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "UNKNOWN ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 101
    :cond_0
    :goto_3
    const-string v0, "iu.Environment"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    iget-boolean v2, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    if-eq v3, v0, :cond_5

    const-string v0, "*"

    :goto_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "update battery state; isPlugged? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    :cond_1
    return v1

    :cond_2
    move v0, v1

    .line 88
    goto :goto_1

    .line 90
    :pswitch_1
    const-string v0, "BATTERY_PLUGGED_AC"

    goto :goto_2

    :pswitch_2
    const-string v0, "BATTERY_PLUGGED_USB"

    goto :goto_2

    :pswitch_3
    const-string v0, "BATTERY_PLUGGED_WIRELESS"

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 94
    :cond_4
    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 95
    if-eq v2, v3, :cond_0

    .line 97
    iput-boolean v2, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    move v1, v0

    goto :goto_3

    .line 102
    :cond_5
    const-string v0, ""

    goto :goto_4

    :catch_0
    move-exception v4

    goto/16 :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static e()Z
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 59
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mounted_ro"

    .line 60
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d:Z

    return v0
.end method
