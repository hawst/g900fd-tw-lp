.class public Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;
.super Llol;
.source "PG"

# interfaces
.implements Lkkk;
.implements Llgs;


# instance fields
.field private N:Landroid/view/View;

.field private O:Landroid/view/View;

.field private P:Landroid/widget/TextView;

.field private Q:Llae;

.field private R:Z

.field private final S:Lhkd;

.field private final T:Lhke;

.field private final U:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkkv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0}, Llol;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->Q:Llae;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->R:Z

    .line 51
    new-instance v0, Lkks;

    invoke-direct {v0, p0}, Lkks;-><init>(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->S:Lhkd;

    .line 62
    new-instance v0, Lhke;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->av:Llqm;

    invoke-direct {v0, v1}, Lhke;-><init>(Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->au:Llnh;

    .line 64
    invoke-virtual {v0, v1}, Lhke;->a(Llnh;)Lhke;

    move-result-object v0

    const v1, 0x7f10004b

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->S:Lhkd;

    .line 65
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->T:Lhke;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->U:Ljava/util/ArrayList;

    .line 72
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;Llae;)Llae;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->Q:Llae;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)Llnh;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->au:Llnh;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->R:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->R:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->U:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->au:Llnh;

    const-class v1, Lilw;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilw;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->n()Lz;

    move-result-object v1

    invoke-interface {v0, v1}, Lilw;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->T:Lhke;

    const v2, 0x7f10004b

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 86
    const v0, 0x7f0401db

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 88
    const v0, 0x7f100259

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->N:Landroid/view/View;

    .line 89
    const v0, 0x7f10057f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->O:Landroid/view/View;

    .line 90
    const v0, 0x1020016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->P:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->au:Llnh;

    const-class v2, Lkki;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkki;

    .line 93
    const v2, 0x7f100258

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 94
    invoke-virtual {v0}, Lkki;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 95
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 134
    :goto_0
    return-object v1

    .line 97
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->O:Landroid/view/View;

    new-instance v4, Lkkt;

    invoke-direct {v4, v0}, Lkkt;-><init>(Lkki;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    new-instance v0, Lhmk;

    sget-object v3, Lonj;->b:Lhmn;

    invoke-direct {v0, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v2, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 105
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 106
    new-instance v3, Lhmi;

    new-instance v4, Lkku;

    invoke-direct {v4, p0, v0}, Lkku;-><init>(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;Landroid/content/res/Resources;)V

    invoke-direct {v3, v4}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 202
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->au:Llnh;

    const-class v1, Lkki;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkki;

    .line 203
    invoke-virtual {v0}, Lkki;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    invoke-virtual {v0}, Lkki;->h()Llae;

    move-result-object v1

    .line 208
    if-eqz v1, :cond_3

    .line 209
    invoke-virtual {v1}, Llae;->c()Ljava/lang/String;

    move-result-object v0

    .line 210
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 211
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v1, v0}, Llae;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 214
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->P:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->P:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0141

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 217
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->O:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 221
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->n()Lz;

    move-result-object v0

    .line 222
    instance-of v2, v0, Lkmw;

    if-eqz v2, :cond_0

    .line 223
    check-cast v0, Lkmw;

    .line 224
    invoke-interface {v0, v1}, Lkmw;->a(Llae;)V

    goto :goto_0

    .line 227
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->P:Landroid/widget/TextView;

    const v1, 0x7f0a026c

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->P:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b013d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 230
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->O:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 184
    const-string v0, "dialog-loc-settings"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->n()Lz;

    move-result-object v0

    const-class v1, Limn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limn;

    .line 186
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->n()Lz;

    move-result-object v1

    invoke-interface {v0, v1}, Limn;->a(Landroid/content/Context;)V

    .line 188
    :cond_0
    return-void
.end method

.method public a(Lkkv;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->U:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Llol;->aO_()V

    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->R:Z

    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->au:Llnh;

    const-class v1, Lkki;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkki;

    .line 159
    invoke-virtual {v0, p0}, Lkki;->a(Lkkk;)V

    .line 161
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->Q:Llae;

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->Q:Llae;

    invoke-virtual {v0, v1}, Lkki;->a(Llae;)V

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->Q:Llae;

    .line 170
    :goto_0
    return-void

    .line 168
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->a()V

    goto :goto_0
.end method

.method public ae_()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 139
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->N:Landroid/view/View;

    .line 140
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->O:Landroid/view/View;

    .line 141
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->P:Landroid/widget/TextView;

    .line 143
    invoke-super {p0}, Llol;->ae_()V

    .line 144
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 190
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->au:Llnh;

    const-class v1, Lkki;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkki;

    .line 175
    invoke-virtual {v0, p0}, Lkki;->b(Lkkk;)V

    .line 177
    invoke-super {p0}, Llol;->z()V

    .line 178
    return-void
.end method
