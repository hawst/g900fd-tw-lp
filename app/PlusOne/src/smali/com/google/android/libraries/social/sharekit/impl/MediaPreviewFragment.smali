.class public Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;
.super Llol;
.source "PG"

# interfaces
.implements Ljby;


# static fields
.field private static Q:I


# instance fields
.field private N:Ljbx;

.field private O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

.field private P:Landroid/widget/BaseAdapter;

.field private final R:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Llol;-><init>()V

    .line 35
    new-instance v0, Lkkw;

    invoke-direct {v0, p0}, Lkkw;-><init>(Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->R:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static a(Landroid/view/WindowManager;Landroid/content/res/Resources;Z)I
    .locals 3

    .prologue
    .line 102
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    .line 103
    const v1, 0x7f0d011a

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 104
    const v2, 0x7f0d0119

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 105
    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 106
    if-eqz p2, :cond_0

    sub-int/2addr v0, v2

    .line 107
    :cond_0
    sget v1, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->Q:I

    if-nez v1, :cond_1

    .line 108
    const v1, 0x7f0d0122

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->Q:I

    .line 110
    :cond_1
    sget v1, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->Q:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;)Ljbx;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->N:Ljbx;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 89
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->N:Ljbx;

    invoke-virtual {v2}, Ljbx;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setVisibility(I)V

    .line 98
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->P:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 99
    return-void

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setVisibility(I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->o()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->N:Ljbx;

    invoke-virtual {v4}, Ljbx;->k()I

    move-result v4

    if-le v4, v0, :cond_1

    .line 93
    :goto_1
    invoke-static {v2, v3, v0}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->a(Landroid/view/WindowManager;Landroid/content/res/Resources;Z)I

    move-result v0

    .line 95
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 94
    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;)Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->P:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->R:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 50
    const v0, 0x7f0401dc

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 52
    const v0, 0x7f100580

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Z)V

    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    new-instance v2, Ljbc;

    invoke-direct {v2}, Ljbc;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Lljl;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->N:Ljbx;

    invoke-virtual {v0}, Ljbx;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->n()Lz;

    move-result-object v0

    check-cast v0, Lkmw;

    invoke-interface {v0}, Lkmw;->l()Landroid/widget/BaseAdapter;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->P:Landroid/widget/BaseAdapter;

    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->P:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/widget/BaseAdapter;)V

    .line 58
    return-object v1

    .line 55
    :cond_0
    new-instance v0, Lkkx;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->n()Lz;

    move-result-object v2

    const v3, 0x1090003

    iget-object v4, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->N:Ljbx;

    iget-object v4, v4, Ljbx;->a:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v2, v3, v4}, Lkkx;-><init>(Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;Landroid/content/Context;ILjava/util/List;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->au:Llnh;

    const-class v1, Ljbx;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbx;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->N:Ljbx;

    .line 46
    return-void
.end method

.method public a(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->a()V

    .line 86
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Llol;->aO_()V

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->N:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->a(Ljby;)V

    .line 71
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->a()V

    .line 72
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->O:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 77
    invoke-super {p0}, Llol;->ae_()V

    .line 78
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->N:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->b(Ljby;)V

    .line 64
    invoke-super {p0}, Llol;->z()V

    .line 65
    return-void
.end method
