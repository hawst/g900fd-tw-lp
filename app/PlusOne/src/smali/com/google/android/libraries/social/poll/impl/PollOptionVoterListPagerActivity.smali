.class public Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lor;


# static fields
.field private static f:Z

.field private static g:Ljava/text/NumberFormat;

.field private static h:Ljava/text/NumberFormat;


# instance fields
.field private e:I

.field private i:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lloa;-><init>()V

    .line 53
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 54
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 250
    return-void
.end method

.method public static synthetic k()Ljava/text/NumberFormat;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->h:Ljava/text/NumberFormat;

    return-object v0
.end method

.method public static synthetic l()Ljava/text/NumberFormat;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->g:Ljava/text/NumberFormat;

    return-object v0
.end method


# virtual methods
.method public a(Loq;)V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1}, Loq;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 153
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->requestWindowFeature(I)Z

    .line 74
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 76
    if-nez v3, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    invoke-static {p0}, Llsc;->b(Landroid/content/Context;)Z

    move-result v4

    .line 80
    if-eqz v4, :cond_3

    .line 81
    invoke-virtual {p0}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 82
    invoke-virtual {v5, v11, v11}, Landroid/view/Window;->setFlags(II)V

    .line 84
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 85
    iget v6, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v6, v6, 0x400

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 86
    const/high16 v6, 0x3f000000    # 0.5f

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 87
    invoke-virtual {v5, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 88
    const-string v0, "card_width"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iget v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->e:I

    if-nez v0, :cond_2

    invoke-static {p0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->e:I

    invoke-static {p0}, Lley;->a(Landroid/content/Context;)I

    move-result v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v8, "status_bar_height"

    const-string v9, "dimen"

    const-string v10, "android"

    invoke-virtual {v0, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_8

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_1
    iget v8, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->e:I

    shl-int/lit8 v7, v7, 0x1

    sub-int v7, v8, v7

    sub-int v0, v7, v0

    iput v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->e:I

    :cond_2
    iget v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->e:I

    invoke-virtual {v5, v6, v0}, Landroid/view/Window;->setLayout(II)V

    .line 90
    :cond_3
    sget-boolean v0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->f:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v5}, Ljava/text/NumberFormat;->getIntegerInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v5

    sput-object v5, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->g:Ljava/text/NumberFormat;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->h:Ljava/text/NumberFormat;

    sput-boolean v2, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->f:Z

    .line 91
    :cond_4
    const v0, 0x7f0401a2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->setContentView(I)V

    .line 94
    const-string v0, "poll_option_voter_models"

    .line 95
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 97
    new-instance v5, Ljzn;

    .line 98
    invoke-virtual {p0}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->f()Lae;

    move-result-object v6

    invoke-direct {v5, p0, v6, v0}, Ljzn;-><init>(Landroid/content/Context;Lae;Ljava/util/ArrayList;)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->h()Loo;

    move-result-object v6

    .line 100
    const v0, 0x7f0a0172

    invoke-virtual {v6, v0}, Loo;->c(I)V

    .line 102
    if-nez v4, :cond_5

    move v0, v2

    :goto_2
    invoke-virtual {v6, v0}, Loo;->c(Z)V

    .line 103
    if-nez v4, :cond_6

    move v0, v2

    :goto_3
    invoke-virtual {v6, v0}, Loo;->b(Z)V

    .line 105
    const v0, 0x7f100320

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->i:Landroid/support/v4/view/ViewPager;

    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->i:Landroid/support/v4/view/ViewPager;

    new-instance v4, Ljzl;

    invoke-direct {v4}, Ljzl;-><init>()V

    invoke-virtual {v0, v2, v4}, Landroid/support/v4/view/ViewPager;->a(ZLkd;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->a(Lip;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->i:Landroid/support/v4/view/ViewPager;

    new-instance v2, Ljzk;

    invoke-direct {v2, v6}, Ljzk;-><init>(Loo;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->a(Lkc;)V

    .line 115
    invoke-virtual {v5}, Ljzn;->b()I

    move-result v2

    move v0, v1

    :goto_4
    if-ge v0, v2, :cond_7

    .line 116
    invoke-virtual {v6}, Loo;->c()Loq;

    move-result-object v4

    .line 117
    invoke-virtual {v5, v0}, Ljzn;->c(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Loq;->a(Ljava/lang/CharSequence;)Loq;

    move-result-object v4

    .line 118
    invoke-virtual {v4, p0}, Loq;->a(Lor;)Loq;

    move-result-object v4

    .line 116
    invoke-virtual {v6, v4}, Loo;->a(Loq;)V

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    move v0, v1

    .line 102
    goto :goto_2

    :cond_6
    move v0, v1

    .line 103
    goto :goto_3

    .line 120
    :cond_7
    invoke-virtual {v6, v11}, Loo;->e(I)V

    .line 122
    const-string v0, "poll_option_voters_count"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 123
    const-string v2, "poll_number_of_votes"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 125
    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 126
    const v0, 0x7f1004ef

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method
