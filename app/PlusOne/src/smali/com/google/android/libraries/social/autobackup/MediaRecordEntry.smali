.class public Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
.super Lifj;
.source "PG"


# annotations
.annotation runtime Lifl;
    a = "media_record"
.end annotation


# static fields
.field public static final a:Lifm;


# instance fields
.field private mAlbumId:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "album_id"
    .end annotation
.end field

.field private mAllowFullRes:Z
    .annotation runtime Lifk;
        a = "allow_full_res"
        d = false
        e = "1"
    .end annotation
.end field

.field private mBucketId:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "bucket_id"
    .end annotation
.end field

.field private mBytesTotal:J
    .annotation runtime Lifk;
        a = "bytes_total"
        d = false
        e = "-1"
    .end annotation
.end field

.field private mBytesUploaded:J
    .annotation runtime Lifk;
        a = "bytes_uploaded"
    .end annotation
.end field

.field private mEventId:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "event_id"
    .end annotation
.end field

.field private mFingerprint:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "fingerprint"
    .end annotation
.end field

.field private mFromCamera:Z
    .annotation runtime Lifk;
        a = "from_camera"
        d = false
        e = "0"
    .end annotation
.end field

.field private mIsImage:Z
    .annotation runtime Lifk;
        a = "is_image"
        d = false
        e = "1"
    .end annotation
.end field

.field private mMediaHash:J
    .annotation runtime Lifk;
        a = "media_hash"
        d = false
    .end annotation
.end field

.field private mMediaId:J
    .annotation runtime Lifk;
        a = "media_id"
        b = true
        d = false
    .end annotation
.end field

.field private mMediaTime:J
    .annotation runtime Lifk;
        a = "media_time"
        d = false
    .end annotation
.end field

.field private mMediaUrl:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "media_url"
        d = false
    .end annotation
.end field

.field private mMimeType:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "mime_type"
    .end annotation
.end field

.field private mRawComponentName:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "component_name"
    .end annotation
.end field

.field private mResumeToken:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "resume_token"
    .end annotation
.end field

.field private mRetryEndTime:J
    .annotation runtime Lifk;
        a = "retry_end_time"
        d = false
        e = "0"
    .end annotation
.end field

.field private mUploadAccountId:I
    .annotation runtime Lifk;
        a = "upload_account_id"
        d = false
        e = "-1"
    .end annotation
.end field

.field private mUploadError:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "upload_error"
    .end annotation
.end field

.field private mUploadFinishTime:J
    .annotation runtime Lifk;
        a = "upload_finish_time"
        d = false
        e = "0"
    .end annotation
.end field

.field private mUploadId:J
    .annotation runtime Lifk;
        a = "upload_id"
    .end annotation
.end field

.field private mUploadReason:I
    .annotation runtime Lifk;
        a = "upload_reason"
        d = false
        e = "0"
    .end annotation
.end field

.field private mUploadState:I
    .annotation runtime Lifk;
        a = "upload_state"
        d = false
        e = "500"
    .end annotation
.end field

.field private mUploadStatus:I
    .annotation runtime Lifk;
        a = "upload_status"
        d = false
        e = "0"
    .end annotation
.end field

.field private mUploadTaskState:I
    .annotation runtime Lifk;
        a = "upload_task_state"
    .end annotation
.end field

.field private mUploadTime:J
    .annotation runtime Lifk;
        a = "upload_time"
    .end annotation
.end field

.field private mUploadUrl:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "upload_url"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lifm;

    const-class v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v0, v1}, Lifm;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 355
    invoke-direct {p0}, Lifj;-><init>()V

    .line 306
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadAccountId:I

    .line 355
    return-void
.end method

.method public static a(Landroid/content/ContentValues;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 2

    .prologue
    .line 260
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, p0, v1}, Lifm;->a(Landroid/content/ContentValues;Lifj;)Lifj;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 2

    .prologue
    .line 229
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, p0, v1}, Lifm;->a(Landroid/database/Cursor;Lifj;)Lifj;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    return-object v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 3

    .prologue
    .line 237
    new-instance v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;-><init>()V

    .line 238
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    invoke-virtual {v1, p0, p1, p2, v0}, Lifm;->a(Landroid/database/sqlite/SQLiteDatabase;JLifj;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 246
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    invoke-virtual {v0}, Lifm;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    invoke-virtual {v0}, Lifm;->b()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "media_url = ? AND upload_account_id = -1"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 249
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    new-instance v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, v1, v2}, Lifm;->a(Landroid/database/Cursor;Lifj;)Lifj;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v0

    .line 252
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v5

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public A()I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 597
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesTotal:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 598
    :cond_0
    const/4 v0, 0x0

    .line 602
    :goto_0
    return v0

    .line 601
    :cond_1
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    long-to-float v0, v0

    iget-wide v2, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesTotal:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 602
    const/16 v1, 0x64

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 640
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadReason:I

    sparse-switch v0, :sswitch_data_0

    .line 650
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 642
    :sswitch_0
    const-string v0, "InstantUpload"

    goto :goto_0

    .line 644
    :sswitch_1
    const-string v0, "InstantShare"

    goto :goto_0

    .line 646
    :sswitch_2
    const-string v0, "UploadAll"

    goto :goto_0

    .line 648
    :sswitch_3
    const-string v0, "Manual"

    goto :goto_0

    .line 640
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_3
        0x14 -> :sswitch_1
        0x1e -> :sswitch_0
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method public C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 654
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    packed-switch v0, :pswitch_data_0

    .line 678
    :pswitch_0
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 656
    :pswitch_1
    const-string v0, "uploading"

    goto :goto_0

    .line 658
    :pswitch_2
    const-string v0, "queued"

    goto :goto_0

    .line 660
    :pswitch_3
    const-string v0, "completed"

    goto :goto_0

    .line 662
    :pswitch_4
    const-string v0, "failed"

    goto :goto_0

    .line 664
    :pswitch_5
    const-string v0, "being stalled"

    goto :goto_0

    .line 666
    :pswitch_6
    const-string v0, "being cancelled"

    goto :goto_0

    .line 668
    :pswitch_7
    const-string v0, "cancelled"

    goto :goto_0

    .line 670
    :pswitch_8
    const-string v0, "unauthorized"

    goto :goto_0

    .line 672
    :pswitch_9
    const-string v0, "quota exceeded"

    goto :goto_0

    .line 674
    :pswitch_a
    const-string v0, "skipped"

    goto :goto_0

    .line 676
    :pswitch_b
    const-string v0, "duplicate"

    goto :goto_0

    .line 654
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public D()Ljava/lang/String;
    .locals 5

    .prologue
    .line 683
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->F()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 687
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadState:I

    sparse-switch v0, :sswitch_data_0

    .line 699
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 689
    :sswitch_0
    const-string v0, "queued"

    goto :goto_0

    .line 691
    :sswitch_1
    const-string v0, "pending"

    goto :goto_0

    .line 693
    :sswitch_2
    const-string v0, "failed"

    goto :goto_0

    .line 695
    :sswitch_3
    const-string v0, "done"

    goto :goto_0

    .line 697
    :sswitch_4
    const-string v0, "don\'t upload"

    goto :goto_0

    .line 687
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch
.end method

.method public F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 704
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadStatus:I

    packed-switch v0, :pswitch_data_0

    .line 758
    :pswitch_0
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 706
    :pswitch_1
    const-string v0, "ok"

    goto :goto_0

    .line 708
    :pswitch_2
    const-string v0, "in progress"

    goto :goto_0

    .line 710
    :pswitch_3
    const-string v0, "stalled"

    goto :goto_0

    .line 712
    :pswitch_4
    const-string v0, "no wifi"

    goto :goto_0

    .line 714
    :pswitch_5
    const-string v0, "roaming"

    goto :goto_0

    .line 716
    :pswitch_6
    const-string v0, "no power"

    goto :goto_0

    .line 718
    :pswitch_7
    const-string v0, "upsync disabled"

    goto :goto_0

    .line 720
    :pswitch_8
    const-string v0, "downsync disabled"

    goto :goto_0

    .line 722
    :pswitch_9
    const-string v0, "background disabled"

    goto :goto_0

    .line 724
    :pswitch_a
    const-string v0, "yielded"

    goto :goto_0

    .line 726
    :pswitch_b
    const-string v0, "user auth"

    goto :goto_0

    .line 728
    :pswitch_c
    const-string v0, "no storage"

    goto :goto_0

    .line 730
    :pswitch_d
    const-string v0, "no network"

    goto :goto_0

    .line 732
    :pswitch_e
    const-string v0, "network exception"

    goto :goto_0

    .line 734
    :pswitch_f
    const-string v0, "FAIL quota"

    goto :goto_0

    .line 736
    :pswitch_10
    const-string v0, "FAIL user auth"

    goto :goto_0

    .line 738
    :pswitch_11
    const-string v0, "FAIL no storage"

    goto :goto_0

    .line 740
    :pswitch_12
    const-string v0, "FAIL invalid metadata"

    goto :goto_0

    .line 742
    :pswitch_13
    const-string v0, "FAIL duplicate"

    goto :goto_0

    .line 744
    :pswitch_14
    const-string v0, "FAIL no fingerprint"

    goto :goto_0

    .line 746
    :pswitch_15
    const-string v0, "FAIL disabled"

    goto :goto_0

    .line 748
    :pswitch_16
    const-string v0, "FAIL google exif"

    goto :goto_0

    .line 750
    :pswitch_17
    const-string v0, "FAIL skipped"

    goto :goto_0

    .line 752
    :pswitch_18
    const-string v0, "FAIL cancelled"

    goto :goto_0

    .line 754
    :pswitch_19
    const-string v0, "FAIL exceed retry time"

    goto :goto_0

    .line 756
    :pswitch_1a
    const-string v0, "FAIL media gone"

    goto :goto_0

    .line 704
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method public a()J
    .locals 2

    .prologue
    .line 358
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMediaId:J

    return-wide v0
.end method

.method public a(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 488
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadReason:I

    .line 489
    return-object p0
.end method

.method public a(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 513
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTime:J

    .line 514
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mAlbumId:Ljava/lang/String;

    .line 479
    return-object p0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 362
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMediaTime:J

    return-wide v0
.end method

.method public b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 493
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadState:I

    .line 494
    return-object p0
.end method

.method public b(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 533
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesTotal:J

    .line 534
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mFingerprint:Ljava/lang/String;

    .line 529
    return-object p0
.end method

.method public b(Landroid/content/ContentValues;)V
    .locals 1

    .prologue
    .line 613
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    invoke-virtual {v0, p1, p0}, Lifm;->a(Landroid/content/ContentValues;Lifj;)Lifj;

    .line 614
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 366
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMediaHash:J

    return-wide v0
.end method

.method public c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 498
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadStatus:I

    .line 499
    return-object p0
.end method

.method public c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 538
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    .line 539
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadUrl:Ljava/lang/String;

    .line 549
    return-object p0
.end method

.method public d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 508
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    .line 509
    return-object p0
.end method

.method public d(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 543
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadId:J

    .line 544
    return-object p0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMediaUrl:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mResumeToken:Ljava/lang/String;

    .line 564
    return-void
.end method

.method public e(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 523
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadAccountId:I

    .line 524
    return-object p0
.end method

.method public e(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 553
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mRetryEndTime:J

    .line 554
    return-object p0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMimeType:Ljava/lang/String;

    .line 568
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mIsImage:Z

    return v0
.end method

.method public f(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 558
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadFinishTime:J

    .line 559
    return-object p0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 571
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMediaUrl:Ljava/lang/String;

    .line 572
    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBucketId:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mResumeToken:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadReason:I

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 414
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    return v0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 418
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTime:J

    return-wide v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 426
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadAccountId:I

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 430
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mAllowFullRes:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mFingerprint:Ljava/lang/String;

    return-object v0
.end method

.method public p()J
    .locals 2

    .prologue
    .line 438
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesTotal:J

    return-wide v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 442
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 446
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadId:J

    return-wide v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 454
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mRetryEndTime:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 618
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "media_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "album_id"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "event_id"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "upload_account_id"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "upload_url"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "bytes_total"

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Lifm;->a(Lifj;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 620
    const-string v0, " {"

    iget v2, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadReason:I

    if-eqz v2, :cond_0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->B()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->D()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget v2, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    if-eqz v2, :cond_1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->C()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->A()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 458
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadFinishTime:J

    return-wide v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public w()Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 503
    return-object p0
.end method

.method public x()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 579
    iget v1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 584
    iget v1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Z
    .locals 4

    .prologue
    .line 593
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
