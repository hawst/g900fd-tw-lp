.class public Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;
.implements Lksv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lloa;-><init>()V

    .line 32
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 33
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lhmw;->aa:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->x:Llnh;

    const-class v1, Lksv;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 40
    return-void
.end method

.method public a(Lkxr;)V
    .locals 3

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 60
    const-string v1, "square_target"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 63
    new-instance v1, Lhgw;

    invoke-direct {v1, p1}, Lhgw;-><init>(Lkxr;)V

    .line 64
    const-string v2, "extra_acl"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 65
    const-string v2, "extra_acl_label"

    invoke-virtual {v1, p0}, Lhgw;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->finish()V

    .line 69
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->finish()V

    .line 74
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_target"

    .line 47
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkxr;

    .line 48
    if-eqz v0, :cond_0

    .line 49
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->x:Llnh;

    const-class v2, Lksu;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lksu;

    invoke-interface {v1, v0}, Lksu;->a(Lkxr;)Lt;

    move-result-object v0

    .line 50
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->f()Lae;

    move-result-object v1

    const-string v2, "square_category_picker"

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/categories/impl/SquareCategoryPickerActivity;->finish()V

    goto :goto_0
.end method
