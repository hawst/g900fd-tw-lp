.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;
.super Ljnl;
.source "PG"


# static fields
.field private static a:I

.field private static o:I


# instance fields
.field private p:Ljava/text/NumberFormat;

.field private q:Ljava/lang/String;

.field private r:Landroid/view/ViewGroup;

.field private s:Ljni;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 188
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 182
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->p:Ljava/text/NumberFormat;

    .line 200
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a:I

    if-nez v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 202
    const v1, 0x7f0d0126

    .line 203
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a:I

    .line 204
    const v1, 0x7f0d0125

    .line 205
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->o:I

    .line 189
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 192
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 182
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->p:Ljava/text/NumberFormat;

    .line 200
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a:I

    if-nez v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 202
    const v1, 0x7f0d0126

    .line 203
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a:I

    .line 204
    const v1, 0x7f0d0125

    .line 205
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->o:I

    .line 193
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 196
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 182
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->p:Ljava/text/NumberFormat;

    .line 200
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a:I

    if-nez v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 202
    const v1, 0x7f0d0126

    .line 203
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a:I

    .line 204
    const v1, 0x7f0d0125

    .line 205
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->o:I

    .line 197
    :cond_0
    return-void
.end method

.method private a(IILjava/lang/String;)Landroid/widget/TextView;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 225
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 226
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 227
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->d(Landroid/widget/TextView;)V

    .line 228
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setId(I)V

    .line 229
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 230
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->p:Ljava/text/NumberFormat;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    const v2, 0x7f0b034a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 232
    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    .line 233
    const v2, 0x7f020416

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 234
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->o:I

    sget v2, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->o:I

    invoke-virtual {v1, v0, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 235
    new-instance v0, Ljnf;

    invoke-direct {v0, p0}, Ljnf;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    return-object v1
.end method

.method private a(Landroid/widget/TextView;Landroid/widget/TextView;Z)Landroid/widget/TextView;
    .locals 4

    .prologue
    .line 303
    new-instance v1, Ljnh;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Ljnh;-><init>(Landroid/content/Context;)V

    .line 304
    invoke-virtual {v1, p1, p2}, Ljnh;->a(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 305
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->o:I

    invoke-virtual {v1, v0}, Ljnh;->a(I)V

    .line 307
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v3, -0x2

    invoke-direct {v2, v0, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 309
    if-eqz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 310
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a:I

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 311
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 313
    return-object p1

    .line 309
    :cond_0
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->f:I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 247
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 248
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 250
    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 251
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 252
    return-object v0
.end method

.method private a(Loez;)Ljnc;
    .locals 3

    .prologue
    .line 256
    new-instance v0, Ljnc;

    .line 257
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ljnc;-><init>(Landroid/content/Context;)V

    .line 258
    iget-object v1, p1, Loez;->b:[Loey;

    new-instance v2, Ljng;

    invoke-direct {v2, p0}, Ljng;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;)V

    invoke-virtual {v0, v1, v2}, Ljnc;->a([Loey;Ljne;)V

    .line 268
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;)Ljni;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->s:Ljni;

    return-object v0
.end method

.method private a(Ljava/lang/String;IIZ)V
    .locals 4

    .prologue
    .line 273
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v1

    .line 274
    if-eqz p2, :cond_1

    .line 275
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Landroid/widget/TextView;)V

    .line 280
    :goto_0
    const/4 v0, 0x0

    .line 281
    if-lez p2, :cond_0

    .line 282
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(IILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    .line 283
    const/4 v2, 0x0

    sget v3, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->b:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 286
    :cond_0
    invoke-direct {p0, v1, v0, p4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Landroid/widget/TextView;Landroid/widget/TextView;Z)Landroid/widget/TextView;

    .line 287
    return-void

    .line 277
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->b(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljnc;)V
    .locals 3

    .prologue
    .line 318
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 320
    if-eqz p2, :cond_0

    .line 321
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1, p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 329
    :goto_0
    return-void

    .line 323
    :cond_0
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 324
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->e(Landroid/widget/TextView;)V

    .line 326
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 327
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;IIZ)V
    .locals 2

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v1

    .line 292
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->c(Landroid/widget/TextView;)V

    .line 294
    const/4 v0, 0x0

    .line 295
    if-lez p2, :cond_0

    .line 296
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(IILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    .line 299
    :cond_0
    invoke-direct {p0, v1, v0, p4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Landroid/widget/TextView;Landroid/widget/TextView;Z)Landroid/widget/TextView;

    .line 300
    return-void
.end method

.method public static b(Lofz;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 466
    if-eqz p0, :cond_3

    .line 468
    iget-object v1, p0, Lofz;->c:Loez;

    .line 469
    if-eqz v1, :cond_1

    .line 470
    iget-object v1, v1, Loez;->b:[Loey;

    .line 471
    if-eqz v1, :cond_1

    array-length v1, v1

    if-eqz v1, :cond_1

    .line 491
    :cond_0
    :goto_0
    return v0

    .line 475
    :cond_1
    iget-object v1, p0, Lofz;->b:Loez;

    .line 476
    if-eqz v1, :cond_2

    .line 477
    iget-object v1, v1, Loez;->b:[Loey;

    .line 478
    if-eqz v1, :cond_2

    array-length v1, v1

    if-nez v1, :cond_0

    .line 482
    :cond_2
    iget-object v1, p0, Lofz;->a:Loez;

    .line 483
    if-eqz v1, :cond_3

    .line 484
    iget-object v1, v1, Loez;->b:[Loey;

    .line 485
    if-eqz v1, :cond_3

    array-length v1, v1

    if-nez v1, :cond_0

    .line 491
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljni;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->s:Ljni;

    .line 211
    return-void
.end method

.method public a(Lnjt;)V
    .locals 2

    .prologue
    .line 423
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->q:Ljava/lang/String;

    .line 424
    if-eqz p1, :cond_1

    .line 425
    iget-object v0, p1, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p1, Lnjt;->e:Lnkd;

    .line 427
    iget-object v1, v0, Lnkd;->a:Lnjb;

    if-eqz v1, :cond_0

    .line 428
    iget-object v0, v0, Lnkd;->a:Lnjb;

    .line 430
    iget-object v1, v0, Lnjb;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 431
    iget-object v0, v0, Lnjb;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->q:Ljava/lang/String;

    .line 438
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    iget-object v0, p1, Lnjt;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->q:Ljava/lang/String;

    .line 443
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 444
    return-void

    .line 433
    :cond_2
    iget-object v0, v0, Lnjb;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lofz;)V
    .locals 14

    .prologue
    const/16 v12, 0xa

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 450
    if-nez p1, :cond_7

    move-object v8, v4

    move-object v9, v4

    move-object v10, v4

    .line 460
    :goto_0
    if-eqz v9, :cond_8

    iget-object v0, v9, Loez;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    iget-object v0, v9, Loez;->b:[Loey;

    if-eqz v0, :cond_8

    iget-object v0, v9, Loez;->b:[Loey;

    array-length v0, v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->j:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_9

    :cond_0
    move v7, v1

    :goto_2
    if-eqz v7, :cond_1

    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->j:Z

    if-eqz v3, :cond_a

    const v3, 0x7f0a02f1

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->e(I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    const v6, 0x7f0a02f2

    invoke-virtual {p0, v6}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->e(I)Ljava/lang/String;

    move-result-object v11

    if-eqz v9, :cond_b

    iget-object v6, v9, Loez;->a:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    :goto_4
    if-eqz v0, :cond_c

    invoke-direct {p0, v9}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Loez;)Ljnc;

    move-result-object v0

    :goto_5
    const/16 v9, 0x9

    invoke-direct {p0, v3, v6, v9, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljava/lang/String;IIZ)V

    invoke-direct {p0, v11, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljava/lang/String;Ljnc;)V

    :cond_1
    if-nez v7, :cond_d

    move v7, v1

    .line 462
    :goto_6
    if-eqz v10, :cond_e

    iget-object v0, v10, Loez;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    iget-object v0, v10, Loez;->b:[Loey;

    if-eqz v0, :cond_e

    iget-object v0, v10, Loez;->b:[Loey;

    array-length v0, v0

    if-eqz v0, :cond_e

    move v6, v1

    :goto_7
    if-eqz v8, :cond_f

    iget-object v0, v8, Loez;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    iget-object v0, v8, Loez;->b:[Loey;

    if-eqz v0, :cond_f

    iget-object v0, v8, Loez;->b:[Loey;

    array-length v0, v0

    if-eqz v0, :cond_f

    move v0, v1

    :goto_8
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->j:Z

    if-nez v3, :cond_2

    if-nez v6, :cond_2

    if-eqz v0, :cond_10

    :cond_2
    move v3, v1

    :goto_9
    if-eqz v3, :cond_6

    if-nez v7, :cond_3

    new-instance v3, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0200f5

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0d0134

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sget v7, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->f:I

    iput v7, v9, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iput v7, v9, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v7, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v7, v3, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->j:Z

    if-eqz v3, :cond_12

    const v3, 0x7f0a02f6

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->e(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v8, :cond_11

    iget-object v3, v8, Loez;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :goto_a
    const v7, 0x7f0a02f7

    invoke-virtual {p0, v7}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->e(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v0, :cond_13

    invoke-direct {p0, v8}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Loez;)Ljnc;

    move-result-object v0

    :goto_b
    invoke-direct {p0, v5, v3, v12, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljava/lang/String;IIZ)V

    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->j:Z

    if-nez v3, :cond_14

    if-eqz v10, :cond_5

    iget-object v3, v10, Loez;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_5

    const v3, 0x7f0a02fd

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->e(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, v10, Loez;->a:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v9, 0x8

    invoke-direct {p0, v3, v5, v9, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->b(Ljava/lang/String;IIZ)V

    if-eqz v6, :cond_4

    invoke-direct {p0, v10}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Loez;)Ljnc;

    move-result-object v4

    :cond_4
    invoke-direct {p0, v7, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljava/lang/String;Ljnc;)V

    move v1, v2

    :cond_5
    if-eqz v8, :cond_6

    iget-object v2, v8, Loez;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_6

    const v2, 0x7f0a02fe

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->e(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v8, Loez;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v2, v3, v12, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->b(Ljava/lang/String;IIZ)V

    invoke-direct {p0, v7, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljava/lang/String;Ljnc;)V

    .line 463
    :cond_6
    :goto_c
    return-void

    .line 455
    :cond_7
    iget-object v6, p1, Lofz;->b:Loez;

    .line 456
    iget-object v3, p1, Lofz;->a:Loez;

    .line 457
    iget-object v0, p1, Lofz;->c:Loez;

    move-object v8, v0

    move-object v9, v3

    move-object v10, v6

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 460
    goto/16 :goto_1

    :cond_9
    move v7, v2

    goto/16 :goto_2

    :cond_a
    const v3, 0x7f0a02fb

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->q:Ljava/lang/String;

    aput-object v11, v6, v2

    invoke-virtual {p0, v3, v6}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    :cond_b
    move v6, v2

    goto/16 :goto_4

    :cond_c
    move-object v0, v4

    goto/16 :goto_5

    :cond_d
    move v7, v2

    goto/16 :goto_6

    :cond_e
    move v6, v2

    .line 462
    goto/16 :goto_7

    :cond_f
    move v0, v2

    goto/16 :goto_8

    :cond_10
    move v3, v2

    goto/16 :goto_9

    :cond_11
    move v3, v2

    goto/16 :goto_a

    :cond_12
    const v3, 0x7f0a02fc

    new-array v7, v1, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->q:Ljava/lang/String;

    aput-object v9, v7, v2

    invoke-virtual {p0, v3, v7}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move v13, v5

    move-object v5, v3

    move v3, v13

    goto/16 :goto_a

    :cond_13
    move-object v0, v4

    goto/16 :goto_b

    :cond_14
    invoke-direct {p0, v7, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljava/lang/String;Ljnc;)V

    goto :goto_c
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 216
    const v0, 0x7f1001e9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->r:Landroid/view/ViewGroup;

    .line 217
    return-void
.end method
