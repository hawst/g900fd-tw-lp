.class public Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# static fields
.field private static a:I

.field private static b:I


# instance fields
.field private final c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private final d:Lhis;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 52
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d()V

    .line 53
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 54
    const/16 v1, 0xd

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 55
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d:Lhis;

    .line 59
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 61
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c()I

    move-result v2

    invoke-virtual {v0, v4, v1, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 62
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 63
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 64
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 52
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d()V

    .line 53
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 54
    const/16 v1, 0xd

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 55
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d:Lhis;

    .line 59
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 61
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c()I

    move-result v2

    invoke-virtual {v0, v4, v1, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 62
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 63
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 64
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 52
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d()V

    .line 53
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 54
    const/16 v1, 0xd

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 55
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d:Lhis;

    .line 59
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 61
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c()I

    move-result v2

    invoke-virtual {v0, v4, v1, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 62
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 63
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 64
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    .line 90
    sget v0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->a:I

    if-nez v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01fb

    .line 92
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->a:I

    .line 94
    :cond_0
    sget v0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->a:I

    return v0
.end method

.method private c()I
    .locals 2

    .prologue
    .line 102
    sget v0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b:I

    if-nez v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01fc

    .line 104
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b:I

    .line 106
    :cond_0
    sget v0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->b:I

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 119
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;
    .locals 2

    .prologue
    .line 73
    if-eqz p1, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->c:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    return-object p0
.end method

.method public a()Lhis;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->d:Lhis;

    return-object v0
.end method
