.class public Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static a:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private b:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "Fingerprint Scanner"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->c(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 32
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 34
    return-void
.end method

.method public static declared-synchronized b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 43
    const-class v1, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->c(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :cond_0
    monitor-exit v1

    return-void

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized c(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;
    .locals 4

    .prologue
    .line 50
    const-class v1, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->a:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 51
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 52
    const/4 v2, 0x1

    const-string v3, "fingerprint_scanner_static"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->a:Landroid/os/PowerManager$WakeLock;

    .line 54
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->a:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 60
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 61
    const/4 v1, 0x1

    const-string v2, "fingerprint_scanner_local"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b:Landroid/os/PowerManager$WakeLock;

    .line 62
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 74
    .line 77
    :try_start_0
    invoke-static {p0}, Lhpx;->a(Landroid/content/Context;)Lhpx;

    move-result-object v0

    invoke-virtual {v0}, Lhpx;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 81
    return-void

    .line 80
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 67
    invoke-super {p0, p1, p2}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    .line 68
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b(Landroid/content/Context;)V

    .line 69
    return-void
.end method
