.class public Lcom/google/android/libraries/social/notifications/impl/GunsModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    const-class v0, Ljio;

    if-ne p2, v0, :cond_1

    .line 23
    const-class v0, Ljio;

    new-instance v1, Ljjk;

    invoke-direct {v1, p1}, Ljjk;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-class v0, Ljjn;

    if-ne p2, v0, :cond_2

    .line 25
    const-class v0, Ljjn;

    new-instance v1, Ljjn;

    invoke-direct {v1, p1}, Ljjn;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 26
    :cond_2
    const-class v0, Ljjm;

    if-ne p2, v0, :cond_3

    .line 27
    const-class v0, Ljjm;

    const/16 v1, 0xc

    new-array v1, v1, [Ljjm;

    const/4 v2, 0x0

    new-instance v3, Ljjy;

    invoke-direct {v3}, Ljjy;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljkj;

    invoke-direct {v3}, Ljkj;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Ljjd;

    invoke-direct {v3}, Ljjd;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Ljkg;

    invoke-direct {v3}, Ljkg;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, Ljks;

    invoke-direct {v3}, Ljks;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Ljkc;

    invoke-direct {v3}, Ljkc;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Ljka;

    invoke-direct {v3}, Ljka;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Ljkl;

    invoke-direct {v3}, Ljkl;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Ljkr;

    invoke-direct {v3}, Ljkr;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-instance v3, Ljjx;

    invoke-direct {v3}, Ljjx;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-instance v3, Ljkm;

    invoke-direct {v3}, Ljkm;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-instance v3, Ljjc;

    invoke-direct {v3}, Ljjc;-><init>()V

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 41
    :cond_3
    const-class v0, Liwq;

    if-ne p2, v0, :cond_4

    .line 42
    const-class v0, Liwq;

    new-instance v1, Ljjb;

    invoke-direct {v1, p1}, Ljjb;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 43
    :cond_4
    const-class v0, Ljji;

    if-ne p2, v0, :cond_5

    .line 44
    const-class v0, Ljji;

    new-instance v1, Ljjj;

    invoke-direct {v1}, Ljjj;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 45
    :cond_5
    const-class v0, Ljjq;

    if-ne p2, v0, :cond_6

    .line 46
    const-class v0, Ljjq;

    new-instance v1, Ljjq;

    invoke-direct {v1, p1}, Ljjq;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 47
    :cond_6
    const-class v0, Ljjo;

    if-ne p2, v0, :cond_7

    .line 48
    const-class v0, Ljjo;

    new-instance v1, Ljjo;

    invoke-direct {v1}, Ljjo;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 49
    :cond_7
    const-class v0, Ljip;

    if-ne p2, v0, :cond_0

    .line 50
    const-class v0, Ljip;

    new-instance v1, Ljjt;

    invoke-direct {v1, p1}, Ljjt;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0
.end method
