.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/google/android/libraries/social/media/ui/MediaView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method


# virtual methods
.method public a(Lpvl;)V
    .locals 7

    .prologue
    .line 48
    iget-object v0, p1, Lpvl;->c:Lpvm;

    .line 49
    iget-object v1, p1, Lpvl;->d:Lpvn;

    .line 50
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110012

    iget-object v4, v0, Lpvm;->a:Ljava/lang/Integer;

    .line 52
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, v0, Lpvm;->a:Ljava/lang/Integer;

    aput-object v0, v5, v6

    .line 51
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 54
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b00e7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 57
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, v1, Lpvn;->a:Ljava/lang/String;

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v1, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 60
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 42
    const v0, 0x7f100118

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->a:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f100288

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 44
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->setPadding(IIII)V

    .line 45
    return-void
.end method
