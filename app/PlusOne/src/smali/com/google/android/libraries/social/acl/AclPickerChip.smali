.class public Lcom/google/android/libraries/social/acl/AclPickerChip;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public a(Lhfv;Lhfw;)V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v4, 0x0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/AclPickerChip;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v2, p0, Lcom/google/android/libraries/social/acl/AclPickerChip;->c:Landroid/widget/RelativeLayout;

    const v0, 0x7f100152

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const v1, 0x7f100153

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v3, 0x7f10014d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0, v9}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    invoke-interface {p1, p2}, Lhfv;->a(Lhfw;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    invoke-virtual {p2}, Lhfw;->d()Lhxc;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {p2}, Lhfw;->d()Lhxc;

    move-result-object v3

    invoke-virtual {v3}, Lhxc;->c()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f020179

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 48
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 47
    :pswitch_2
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-interface {p1}, Lhfv;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f020314

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f020310

    goto :goto_1

    :pswitch_3
    invoke-interface {p1, p2}, Lhfv;->b(Lhfw;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Lhfw;->d()Lhxc;

    move-result-object v6

    invoke-virtual {v6}, Lhxc;->d()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v8, :cond_2

    invoke-interface {v3, v4, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    :goto_2
    new-array v3, v8, [Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const v0, 0x7f10014e

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aput-object v0, v3, v4

    const/4 v7, 0x1

    const v0, 0x7f100151

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aput-object v0, v3, v7

    const v0, 0x7f100150

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aput-object v0, v3, v10

    const v0, 0x7f10014f

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aput-object v0, v3, v11

    aget-object v0, v3, v4

    invoke-static {v5, v0, v10}, Lhfu;->a(Landroid/content/Context;Lcom/google/android/libraries/social/avatars/ui/AvatarView;I)V

    move v0, v4

    :goto_3
    if-ge v0, v8, :cond_3

    aget-object v2, v3, v0

    invoke-virtual {v2, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_4
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f02017b

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    invoke-virtual {p2}, Lhfw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2}, Lhfv;->b(Lhfw;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    move-object v1, v3

    goto :goto_2

    :cond_3
    packed-switch v6, :pswitch_data_2

    move v2, v4

    :goto_4
    if-ge v2, v8, :cond_0

    aget-object v4, v3, v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const v6, 0x7f0d023b

    invoke-static {v5, v4, v0, v6}, Lhfu;->a(Landroid/content/Context;Lcom/google/android/libraries/social/avatars/ui/AvatarView;Ljava/lang/String;I)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :goto_5
    :pswitch_6
    if-ge v4, v10, :cond_0

    aget-object v2, v3, v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const v6, 0x7f0d023a

    invoke-static {v5, v2, v0, v6}, Lhfu;->a(Landroid/content/Context;Lcom/google/android/libraries/social/avatars/ui/AvatarView;Ljava/lang/String;I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :pswitch_7
    move v2, v4

    :goto_6
    if-ge v2, v11, :cond_4

    aget-object v6, v3, v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const v7, 0x7f0d023b

    invoke-static {v5, v6, v0, v7}, Lhfu;->a(Landroid/content/Context;Lcom/google/android/libraries/social/avatars/ui/AvatarView;Ljava/lang/String;I)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_4
    aget-object v0, v3, v11

    invoke-virtual {v0, v9}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    aget-object v0, v3, v4

    const/4 v1, 0x1

    invoke-static {v5, v0, v1}, Lhfu;->a(Landroid/content/Context;Lcom/google/android/libraries/social/avatars/ui/AvatarView;I)V

    goto/16 :goto_0

    :pswitch_8
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-interface {p1}, Lhfv;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0201c7

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_5
    const v0, 0x7f0201c6

    goto :goto_7

    :pswitch_9
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-interface {p1}, Lhfv;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f0201ab

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_6
    const v0, 0x7f0201a6

    goto :goto_8

    :pswitch_a
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f020179

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p2}, Lhfw;->g()Lhhe;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p2}, Lhfw;->g()Lhhe;

    move-result-object v0

    invoke-virtual {v0}, Lhhe;->d()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    invoke-virtual {p2}, Lhfw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2}, Lhfv;->c(Lhfw;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerChip;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerChip;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerChip;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p2, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 40
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 32
    const v0, 0x7f100157

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/AclPickerChip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerChip;->a:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f100158

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/AclPickerChip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerChip;->b:Landroid/widget/TextView;

    .line 34
    const v0, 0x7f10014c

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/AclPickerChip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerChip;->c:Landroid/widget/RelativeLayout;

    .line 35
    return-void
.end method
