.class public Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljbb;
.implements Llcs;
.implements Lljh;
.implements Lljj;


# static fields
.field public static a:Llct;


# instance fields
.field public b:Lkzr;

.field public c:[Lppr;

.field public d:Lkzv;

.field public e:Licj;

.field public f:I

.field public g:I

.field public h:[Lizu;

.field public i:[I

.field public j:I

.field public k:I

.field public l:Z

.field public m:Z

.field private n:Lldm;

.field private o:Landroid/view/View;

.field private p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

.field private q:Landroid/widget/BaseAdapter;

.field private r:I

.field private s:I

.field private t:Landroid/animation/Animator$AnimatorListener;

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    iput-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->l:Z

    .line 92
    iput-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    .line 93
    iput-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->u:Z

    .line 108
    sget-object v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    if-nez v0, :cond_0

    .line 109
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    .line 112
    :cond_0
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->r:I

    .line 113
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    new-instance v0, Lldn;

    invoke-direct {v0, p0}, Lldn;-><init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->t:Landroid/animation/Animator$AnimatorListener;

    .line 148
    :cond_1
    new-instance v0, Lldo;

    invoke-direct {v0, p0}, Lldo;-><init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->q:Landroid/widget/BaseAdapter;

    .line 289
    new-instance v0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 290
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-static {v0}, Llii;->g(Landroid/view/View;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Lljj;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Z)V

    .line 294
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const v1, 0x7f0b0111

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setBackgroundResource(I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    new-instance v1, Lldp;

    invoke-direct {v1}, Lldp;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Lljl;)V

    .line 304
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    .line 305
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    const v1, 0x7f020416

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 307
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 309
    new-instance v0, Lldm;

    invoke-direct {v0, p1}, Lldm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    .line 310
    return-void
.end method

.method private a(II)V
    .locals 10

    .prologue
    .line 363
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    new-array v0, v0, [Lizu;

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    .line 364
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    .line 366
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 367
    int-to-float v0, p1

    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    float-to-int p1, v0

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->e:Licj;

    if-eqz v0, :cond_3

    .line 374
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->e:Licj;

    invoke-virtual {v3}, Licj;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljac;->d:Ljac;

    invoke-static {v2, v3, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    aput-object v2, v0, v1

    .line 376
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 377
    int-to-float v0, p1

    const v1, 0x3fb9999a    # 1.45f

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    .line 481
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->removeView(Landroid/view/View;)V

    .line 482
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->q:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/widget/BaseAdapter;)V

    .line 483
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->addView(Landroid/view/View;)V

    .line 485
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->removeView(Landroid/view/View;)V

    .line 486
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->removeView(Landroid/view/View;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    .line 489
    invoke-virtual {v0}, Lkzv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 490
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    if-eqz v0, :cond_15

    .line 491
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->addView(Landroid/view/View;)V

    .line 492
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    invoke-virtual {v0, v1}, Lldm;->a(Lkzr;)V

    .line 496
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lldm;->a(Z)V

    .line 497
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->addView(Landroid/view/View;)V

    .line 498
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    .line 502
    :goto_2
    return-void

    .line 378
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    if-eqz v0, :cond_7

    .line 379
    iget-object v7, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    invoke-virtual {v1}, Lkzv;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    .line 380
    invoke-virtual {v2}, Lkzv;->h()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    .line 381
    invoke-virtual {v4}, Lkzv;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    .line 382
    invoke-virtual {v5}, Lkzv;->n()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    invoke-virtual {v5}, Lkzv;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    :goto_3
    iget-object v6, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    .line 383
    invoke-virtual {v6}, Lkzv;->m()Ljac;

    move-result-object v6

    .line 379
    invoke-static/range {v0 .. v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    aput-object v0, v7, v8

    .line 384
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 385
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    invoke-virtual {v0}, Lkzv;->i()S

    move-result v0

    .line 386
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    invoke-virtual {v1}, Lkzv;->j()S

    move-result v1

    .line 388
    if-eqz v0, :cond_4

    if-nez v1, :cond_6

    .line 389
    :cond_4
    iput p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    .line 399
    :goto_4
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(I)V

    goto/16 :goto_0

    .line 382
    :cond_5
    const/4 v5, 0x0

    goto :goto_3

    .line 391
    :cond_6
    const/high16 v2, 0x3f800000    # 1.0f

    int-to-float v1, v1

    mul-float/2addr v1, v2

    int-to-float v0, v0

    div-float/2addr v1, v0

    .line 392
    int-to-float v0, p1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 393
    if-le v0, p2, :cond_17

    .line 395
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    const/4 v2, 0x0

    int-to-float v3, p2

    div-float v1, v3, v1

    float-to-int v1, v1

    aput v1, v0, v2

    .line 397
    :goto_5
    iput p2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    goto :goto_4

    .line 400
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    if-eqz v0, :cond_a

    .line 401
    const/4 v0, 0x0

    move v7, v0

    :goto_6
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    if-ge v7, v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    aget-object v0, v0, v7

    .line 403
    iget-object v2, v0, Lppr;->b:Lppt;

    .line 404
    const/4 v4, 0x0

    .line 406
    iget-object v1, v0, Lppr;->c:[Lpps;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lppr;->c:[Lpps;

    array-length v1, v1

    if-eqz v1, :cond_8

    .line 407
    iget-object v0, v0, Lppr;->c:[Lpps;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v4, v0, Lpps;->b:Ljava/lang/String;

    .line 411
    :cond_8
    iget-object v8, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, v2, Lppt;->b:Lpgx;

    iget-object v1, v1, Lpgx;->d:Ljava/lang/String;

    iget-object v2, v2, Lppt;->a:Ljava/lang/String;

    .line 412
    invoke-static {v2}, Llsl;->a(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v5, 0x0

    sget-object v6, Ljac;->a:Ljac;

    .line 411
    invoke-static/range {v0 .. v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    aput-object v0, v8, v7

    .line 416
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    .line 417
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aput p1, v0, v7

    .line 418
    iput p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    .line 401
    :goto_7
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_6

    .line 420
    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aput p2, v0, v7

    .line 421
    iput p2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    goto :goto_7

    .line 425
    :cond_a
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    new-array v8, v0, [I

    .line 427
    const/4 v0, 0x0

    move v7, v0

    :goto_8
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    if-ge v7, v0, :cond_f

    .line 429
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    invoke-virtual {v0, v7}, Lkzr;->a(I)Lkzv;

    move-result-object v6

    .line 430
    invoke-virtual {v6}, Lkzv;->m()Ljac;

    move-result-object v0

    .line 431
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    invoke-virtual {v6}, Lkzv;->i()S

    move-result v2

    aput v2, v1, v7

    .line 432
    invoke-virtual {v6}, Lkzv;->j()S

    move-result v1

    aput v1, v8, v7

    .line 433
    const/high16 v1, 0x3f800000    # 1.0f

    aget v2, v8, v7

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v2, v2, v7

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 434
    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v2, v2, v7

    if-gt v2, p1, :cond_c

    :cond_b
    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_c

    sget-object v2, Ljac;->c:Ljac;

    if-ne v0, v2, :cond_d

    .line 438
    :cond_c
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aput p1, v0, v7

    .line 440
    :cond_d
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v0, v0, v7

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    aput v0, v8, v7

    .line 443
    iget-object v9, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v6}, Lkzv;->f()Ljava/lang/String;

    move-result-object v1

    .line 444
    invoke-virtual {v6}, Lkzv;->h()J

    move-result-wide v2

    .line 445
    invoke-virtual {v6}, Lkzv;->e()Ljava/lang/String;

    move-result-object v4

    .line 446
    invoke-virtual {v6}, Lkzv;->n()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-virtual {v6}, Lkzv;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 447
    :goto_9
    invoke-virtual {v6}, Lkzv;->m()Ljac;

    move-result-object v6

    .line 443
    invoke-static/range {v0 .. v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    aput-object v0, v9, v7

    .line 427
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_8

    .line 446
    :cond_e
    const/4 v5, 0x0

    goto :goto_9

    .line 450
    :cond_f
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    .line 451
    const/4 v0, 0x0

    :goto_a
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    if-ge v0, v1, :cond_13

    .line 454
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v1, v1, v0

    sget-object v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget v2, v2, Llct;->ac:I

    if-ge v1, v2, :cond_10

    .line 455
    const/high16 v1, 0x3f800000    # 1.0f

    sget-object v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget v2, v2, Llct;->ac:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v2, v2, v0

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 456
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v3, v2, v0

    int-to-float v3, v3

    mul-float/2addr v3, v1

    float-to-int v3, v3

    aput v3, v2, v0

    .line 457
    aget v2, v8, v0

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v8, v0

    .line 460
    :cond_10
    aget v1, v8, v0

    sget-object v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget v2, v2, Llct;->ac:I

    if-ge v1, v2, :cond_11

    .line 461
    const/high16 v1, 0x3f800000    # 1.0f

    sget-object v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget v2, v2, Llct;->ac:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    aget v2, v8, v0

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 462
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v3, v2, v0

    int-to-float v3, v3

    mul-float/2addr v3, v1

    float-to-int v3, v3

    aput v3, v2, v0

    .line 463
    aget v2, v8, v0

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v8, v0

    .line 466
    :cond_11
    aget v1, v8, v0

    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    if-ge v1, v2, :cond_12

    .line 467
    aget v1, v8, v0

    iput v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    .line 451
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 471
    :cond_13
    const/4 v0, 0x0

    :goto_b
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    if-ge v0, v1, :cond_1

    .line 473
    aget v1, v8, v0

    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    if-le v1, v2, :cond_14

    .line 474
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    aget v2, v8, v0

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 475
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v3, v2, v0

    int-to-float v3, v3

    mul-float/2addr v1, v3

    float-to-int v1, v1

    aput v1, v2, v0

    .line 476
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    aput v1, v8, v0

    .line 471
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 494
    :cond_15
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    invoke-virtual {v0, v1}, Lldm;->a(Lkzv;)V

    goto/16 :goto_1

    .line 500
    :cond_16
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_17
    move p2, v0

    goto/16 :goto_5
.end method

.method private a(ZZ)V
    .locals 4

    .prologue
    .line 798
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 799
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 800
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 801
    instance-of v3, v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v3, :cond_0

    .line 802
    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 803
    const/4 v3, 0x4

    invoke-virtual {v0, v3, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(IZ)V

    .line 804
    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->l(Z)V

    .line 799
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 808
    :cond_1
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;)Z
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d()Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;Lkzv;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 55
    invoke-virtual {p1}, Lkzv;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lkzv;->h()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lkzv;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lkzv;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 772
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 773
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a()I

    move-result v1

    if-nez v1, :cond_0

    .line 774
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 775
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 779
    :cond_0
    return v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 793
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 603
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->removeAllViews()V

    .line 605
    iput-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    .line 606
    iput-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    .line 607
    iput-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->e:Licj;

    .line 608
    iput v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    .line 610
    iput-boolean v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    .line 611
    iput-boolean v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->u:Z

    .line 613
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 614
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0}, Lldm;->a()V

    .line 616
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/widget/BaseAdapter;)V

    .line 617
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(I)V

    .line 618
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const v1, 0x7f0b0111

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setBackgroundResource(I)V

    .line 620
    iput-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    .line 621
    iput-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    .line 622
    iput-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    .line 623
    iput v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    .line 624
    iput v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    .line 628
    iput v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    .line 630
    iput-boolean v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->l:Z

    .line 631
    return-void
.end method

.method public a(I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x96

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 701
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    if-nez v0, :cond_1

    .line 769
    :cond_0
    :goto_0
    return-void

    .line 704
    :cond_1
    iput p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    .line 706
    invoke-static {}, Llsj;->c()Z

    move-result v0

    .line 708
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 710
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0, v3}, Lldm;->setVisibility(I)V

    .line 711
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 712
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 717
    :pswitch_1
    if-eqz v0, :cond_4

    .line 718
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0}, Lldm;->getAlpha()F

    move-result v0

    const v1, 0x3f7fbe77    # 0.999f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 719
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0, v3}, Lldm;->setVisibility(I)V

    .line 720
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 721
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0}, Lldm;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 722
    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget-object v1, v1, Llct;->c:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 723
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->t:Landroid/animation/Animator$AnimatorListener;

    .line 724
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 725
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 726
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 728
    :cond_2
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 734
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 735
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 731
    :cond_4
    iput v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    .line 732
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0, v3}, Lldm;->setVisibility(I)V

    goto :goto_1

    .line 740
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0, v4}, Lldm;->setVisibility(I)V

    .line 764
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 765
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_0

    .line 747
    :pswitch_3
    if-eqz v0, :cond_7

    .line 748
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0}, Lldm;->getAlpha()F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 749
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 750
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0, v3}, Lldm;->setVisibility(I)V

    .line 751
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0}, Lldm;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 752
    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget-object v1, v1, Llct;->c:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    .line 753
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->t:Landroid/animation/Animator$AnimatorListener;

    .line 754
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 755
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 756
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 758
    :cond_6
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_2

    .line 761
    :cond_7
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    .line 762
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0, v4}, Lldm;->setVisibility(I)V

    goto :goto_2

    .line 708
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 2

    .prologue
    .line 784
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lldm;->a(Z)V

    .line 785
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 549
    if-eq p2, v4, :cond_1

    .line 550
    iput v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    .line 551
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 552
    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    if-eq v2, v4, :cond_0

    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 554
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(I)V

    .line 558
    :cond_1
    if-eq p2, v4, :cond_3

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->l:Z

    .line 559
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->l:Z

    if-nez v0, :cond_2

    .line 560
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(ZZ)V

    .line 563
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 558
    goto :goto_0
.end method

.method public a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;III)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 568
    if-nez p3, :cond_1

    .line 599
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    if-eqz v0, :cond_5

    .line 573
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    if-gez v0, :cond_4

    .line 575
    if-ltz p3, :cond_5

    .line 576
    iput p3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    .line 588
    :goto_1
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->r:I

    if-gt v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 589
    :cond_2
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    if-ne v0, v3, :cond_0

    .line 591
    :cond_3
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(I)V

    goto :goto_0

    .line 581
    :cond_4
    if-gez p3, :cond_5

    .line 582
    iput p3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    goto :goto_1

    .line 584
    :cond_5
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    goto :goto_1

    .line 593
    :cond_6
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->s:I

    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->r:I

    neg-int v1, v1

    if-ge v0, v1, :cond_0

    .line 594
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->k:I

    if-ne v0, v2, :cond_0

    .line 596
    :cond_7
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(I)V

    goto :goto_0
.end method

.method public a(Licj;IIZLjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 338
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->e:Licj;

    .line 339
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    .line 340
    iput-boolean p4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    .line 341
    iput-object p5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->v:Ljava/lang/String;

    .line 342
    iput-boolean p6, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->w:Z

    .line 344
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljgn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    .line 345
    invoke-interface {v0}, Ljgn;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->u:Z

    .line 347
    invoke-direct {p0, p2, p3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(II)V

    .line 348
    return-void
.end method

.method public a(Lkzr;IIIZZLjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 314
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    .line 315
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    invoke-virtual {v0}, Lkzr;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    .line 316
    iput p4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->g:I

    .line 317
    iput-boolean p5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    .line 318
    iput-boolean p6, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->u:Z

    .line 319
    iput-object p7, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->v:Ljava/lang/String;

    .line 320
    iput-boolean p8, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->w:Z

    .line 321
    invoke-direct {p0, p2, p3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(II)V

    .line 322
    return-void
.end method

.method public a(Lkzv;IIIZZLjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 326
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    .line 327
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    .line 328
    iput p4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->g:I

    .line 329
    iput-boolean p5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    .line 330
    iput-boolean p6, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->u:Z

    .line 331
    iput-object p7, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->v:Ljava/lang/String;

    .line 332
    iput-boolean p8, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->w:Z

    .line 333
    invoke-direct {p0, p2, p3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(II)V

    .line 334
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 788
    iput-boolean p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    .line 789
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(ZZ)V

    .line 790
    return-void
.end method

.method public a([Lppr;II)V
    .locals 1

    .prologue
    .line 351
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    .line 352
    array-length v0, p1

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    .line 353
    invoke-direct {p0, p2, p3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(II)V

    .line 354
    return-void
.end method

.method public aE_()V
    .locals 2

    .prologue
    .line 812
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(ZZ)V

    .line 813
    return-void
.end method

.method public aF_()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 817
    invoke-direct {p0, v0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(ZZ)V

    .line 819
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 697
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 635
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    if-nez v0, :cond_1

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 638
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 639
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    invoke-virtual {v0, v3}, Lkzr;->a(I)Lkzv;

    move-result-object v1

    .line 641
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lhte;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhte;

    .line 643
    invoke-virtual {v1}, Lkzv;->g()Ljava/lang/String;

    move-result-object v2

    .line 644
    invoke-virtual {v1}, Lkzv;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->v:Ljava/lang/String;

    .line 643
    invoke-interface {v0, v2, v1, v3}, Lhte;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 647
    :cond_2
    check-cast p1, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 648
    invoke-virtual {p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 649
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    if-eqz v1, :cond_3

    .line 650
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    aget-object v1, v1, v0

    iget-object v0, v1, Lppr;->c:[Lpps;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lppr;->c:[Lpps;

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lppr;->c:[Lpps;

    aget-object v0, v0, v3

    iget-object v0, v0, Lpps;->c:Lppf;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lhtk;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtk;

    iget-object v1, v1, Lppr;->c:[Lpps;

    aget-object v1, v1, v3

    iget-object v1, v1, Lpps;->c:Lppf;

    iget-object v1, v1, Lppf;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lhtk;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 652
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    aget-object v2, v1, v0

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    invoke-virtual {v1}, Lkzr;->a()I

    move-result v1

    if-le v1, v0, :cond_5

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    invoke-virtual {v1, v0}, Lkzr;->a(I)Lkzv;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lkzv;->n()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lkzv;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, Lkzv;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v2}, Lizu;->c()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_6

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhtq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtq;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->v:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lhtq;->a(Lizu;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    move-object v1, v0

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lhtj;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtj;

    invoke-virtual {v1}, Lkzv;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lkzv;->f()Ljava/lang/String;

    move-result-object v1

    iget-boolean v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->w:Z

    invoke-interface {v0, v2, v3, v1, v4}, Lhtj;->a(Lizu;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 528
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 529
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getMeasuredWidth()I

    move-result v0

    .line 530
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getMeasuredHeight()I

    move-result v1

    .line 532
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v2, v4, v4, v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->layout(IIII)V

    .line 534
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v2}, Lldm;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_0

    .line 535
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v2}, Lldm;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    .line 536
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v3, v4, v2, v0, v1}, Lldm;->layout(IIII)V

    .line 538
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v0}, Lldm;->b()Landroid/graphics/Rect;

    move-result-object v0

    .line 540
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v2

    iget v5, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/view/View;->layout(IIII)V

    .line 545
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 506
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 507
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 508
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->p:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 509
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 508
    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->measure(II)V

    .line 511
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v2}, Lldm;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_0

    .line 512
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 513
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 512
    invoke-virtual {v2, v3, v4}, Lldm;->measure(II)V

    .line 515
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_0

    .line 516
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->n:Lldm;

    invoke-virtual {v2}, Lldm;->b()Landroid/graphics/Rect;

    move-result-object v2

    .line 517
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->o:Landroid/view/View;

    .line 518
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 519
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 517
    invoke-virtual {v3, v4, v2}, Landroid/view/View;->measure(II)V

    .line 523
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->setMeasuredDimension(II)V

    .line 524
    return-void
.end method
