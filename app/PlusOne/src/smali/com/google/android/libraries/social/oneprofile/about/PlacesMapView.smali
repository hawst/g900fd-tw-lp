.class public Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"


# static fields
.field private static a:Ljava/util/regex/Pattern;


# instance fields
.field private b:F

.field private c:I

.field private d:I

.field private e:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, ".*size=(\\d+)x(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b(I)V

    .line 41
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 43
    const v1, 0x7f0c0042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b:F

    .line 45
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b:F

    iput v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->e:F

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b(I)V

    .line 41
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 43
    const v1, 0x7f0c0042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b:F

    .line 45
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b:F

    iput v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->e:F

    .line 37
    return-void
.end method


# virtual methods
.method public a(Lizu;)V
    .locals 2

    .prologue
    .line 50
    if-eqz p1, :cond_1

    .line 51
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->a:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->c:I

    .line 54
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->d:I

    .line 55
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->c:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->e:F

    .line 60
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 61
    return-void

    .line 58
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->b:F

    iput v0, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->e:F

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 66
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->getPaddingLeft()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 68
    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->e:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 69
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 70
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->setMeasuredDimension(II)V

    .line 71
    return-void
.end method
