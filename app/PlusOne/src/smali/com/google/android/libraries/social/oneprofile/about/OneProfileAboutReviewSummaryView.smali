.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;
.super Ljnl;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;

.field private o:Landroid/view/View;

.field private p:Landroid/widget/TextView;

.field private q:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Ljnj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method public static b(Lnjt;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 195
    iget-object v1, p0, Lnjt;->f:Lnjg;

    iget-object v1, v1, Lnjg;->b:Lnix;

    iget-object v1, v1, Lnix;->d:Lpmm;

    .line 197
    iget-object v2, v1, Lpmm;->k:Lptg;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lpmm;->k:Lptg;

    iget-object v2, v2, Lptg;->a:Lpgg;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lpmm;->k:Lptg;

    iget-object v2, v2, Lptg;->a:Lpgg;

    iget-object v2, v2, Lpgg;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lpmm;->k:Lptg;

    iget-object v2, v2, Lptg;->a:Lpgg;

    iget-object v2, v2, Lpgg;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 204
    :cond_1
    iget-object v1, v1, Lpmm;->s:Lpsx;

    .line 205
    if-eqz v1, :cond_2

    iget-object v1, v1, Lpsx;->a:Lpsz;

    if-nez v1, :cond_0

    .line 216
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljnj;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->t:Ljnj;

    .line 192
    return-void
.end method

.method public a(Lnjt;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v7, 0x0

    const/16 v12, 0x8

    const/4 v11, 0x0

    .line 86
    iget-object v0, p1, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v10, v0, Lnix;->d:Lpmm;

    .line 92
    iget-object v0, v10, Lpmm;->t:Lpse;

    .line 93
    if-eqz v0, :cond_6

    .line 94
    iget-object v0, v0, Lpse;->a:Lpsd;

    move-object v6, v0

    .line 97
    :goto_0
    iget-object v0, v10, Lpmm;->k:Lptg;

    .line 98
    if-eqz v0, :cond_5

    .line 99
    iget-object v0, v0, Lptg;->a:Lpgg;

    .line 100
    if-eqz v0, :cond_5

    .line 101
    iget-object v1, v0, Lpgg;->a:Ljava/lang/Integer;

    .line 102
    iget-object v0, v0, Lpgg;->b:Ljava/lang/Integer;

    move-object v8, v0

    move-object v9, v1

    .line 106
    :goto_1
    if-eqz v6, :cond_1

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->a:Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;

    iget-object v1, v6, Lpsd;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, v6, Lpsd;->d:Ljava/lang/Integer;

    .line 108
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, v6, Lpsd;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, v6, Lpsd;->b:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, v6, Lpsd;->a:Ljava/lang/Integer;

    .line 109
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 107
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->a(IIIII)V

    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->a:Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;

    invoke-virtual {v0, v11}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->setVisibility(I)V

    .line 115
    :goto_2
    if-eqz v9, :cond_3

    if-eqz v8, :cond_3

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->p:Landroid/widget/TextView;

    const-string v1, "%.1f"

    new-array v2, v13, [Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->q:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a(I)V

    .line 119
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110012

    .line 121
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-array v5, v13, [Ljava/lang/Object;

    .line 122
    invoke-virtual {v0, v8}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v11

    .line 120
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->q:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    invoke-virtual {v0, v11}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 129
    if-eqz v6, :cond_2

    .line 130
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 142
    :goto_3
    iget-object v0, v10, Lpmm;->s:Lpsx;

    .line 143
    if-eqz v0, :cond_0

    iget-object v1, v0, Lpsx;->a:Lpsz;

    if-eqz v1, :cond_0

    .line 144
    iget-object v7, v0, Lpsx;->a:Lpsz;

    .line 147
    :cond_0
    if-eqz v7, :cond_4

    .line 148
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 149
    iget-object v1, v7, Lpsz;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " \u2013 "

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    .line 153
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090045

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 149
    invoke-static {v0, v1, v2}, Llhv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 154
    const-string v1, "Google"

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    .line 158
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090046

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 154
    invoke-static {v0, v1, v2}, Llhv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    :goto_4
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->a:Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;

    invoke-virtual {v0, v12}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->setVisibility(I)V

    goto/16 :goto_2

    .line 132
    :cond_2
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_3

    .line 136
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->q:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    invoke-virtual {v0, v12}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->setVisibility(I)V

    goto :goto_3

    .line 162
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_5
    move-object v8, v7

    move-object v9, v7

    goto/16 :goto_1

    :cond_6
    move-object v6, v7

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100412

    if-ne v0, v1, :cond_0

    .line 222
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->t:Ljnj;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljnj;->f(Ljava/lang/String;)V

    .line 224
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 69
    const v0, 0x7f10041e

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->a:Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;

    .line 70
    const v0, 0x7f10041f

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->o:Landroid/view/View;

    .line 71
    const v0, 0x7f100420

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->p:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f100421

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->q:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    .line 73
    const v0, 0x7f100194

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->r:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f100422

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->s:Landroid/widget/TextView;

    .line 82
    return-void
.end method
