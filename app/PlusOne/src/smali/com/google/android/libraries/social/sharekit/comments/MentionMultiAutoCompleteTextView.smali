.class public Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;
.super Landroid/widget/MultiAutoCompleteTextView;
.source "PG"


# instance fields
.field private a:Ljpj;

.field private b:Lkjm;

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 127
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;Z)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->b(Z)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkjq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->b:Lkjm;

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a()Ljava/util/List;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->b:Lkjm;

    invoke-interface {v1, p1, v0}, Lkjm;->a(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method private a([Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V
    .locals 7

    .prologue
    .line 413
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_8

    .line 414
    aget-object v1, p1, v2

    .line 415
    invoke-virtual {p2, v1}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    .line 416
    invoke-virtual {p2, v1}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    .line 418
    instance-of v0, v1, Landroid/text/style/StyleSpan;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 419
    check-cast v0, Landroid/text/style/StyleSpan;

    invoke-virtual {v0}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v0

    .line 420
    const/4 v5, 0x1

    if-ne v0, v5, :cond_2

    .line 421
    const-string v0, "*"

    invoke-virtual {p2, v4, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 422
    const-string v0, "*"

    invoke-virtual {p2, v3, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 460
    :cond_0
    :goto_1
    invoke-virtual {p2, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 413
    :cond_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 423
    :cond_2
    const/4 v5, 0x2

    if-ne v0, v5, :cond_3

    .line 424
    const-string v0, "_"

    invoke-virtual {p2, v4, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 425
    const-string v0, "_"

    invoke-virtual {p2, v3, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 426
    :cond_3
    const/4 v5, 0x3

    if-ne v0, v5, :cond_0

    .line 427
    const-string v0, "*_"

    invoke-virtual {p2, v4, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 428
    const-string v0, "_*"

    invoke-virtual {p2, v3, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 430
    :cond_4
    instance-of v0, v1, Landroid/text/style/StrikethroughSpan;

    if-eqz v0, :cond_5

    .line 431
    const-string v0, "-"

    invoke-virtual {p2, v4, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 432
    const-string v0, "-"

    invoke-virtual {p2, v3, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 433
    :cond_5
    instance-of v0, v1, Lljd;

    if-nez v0, :cond_1

    .line 434
    instance-of v0, v1, Landroid/text/style/URLSpan;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 436
    check-cast v0, Landroid/text/style/URLSpan;

    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_6

    invoke-static {v0}, Ljpf;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 438
    if-eqz v3, :cond_1

    .line 439
    add-int/lit8 v5, v3, -0x1

    invoke-virtual {p2, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    .line 443
    const/16 v6, 0x2b

    if-ne v5, v6, :cond_1

    .line 444
    invoke-static {v0}, Ljpf;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 448
    if-eqz v0, :cond_0

    .line 449
    new-instance v5, Lljd;

    invoke-direct {v5, v0}, Lljd;-><init>(Ljava/lang/String;)V

    .line 450
    add-int/lit8 v0, v3, -0x1

    const/4 v3, 0x0

    invoke-virtual {p2, v5, v0, v4, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 451
    const-string v0, "\u200b"

    invoke-virtual {p2, v4, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 453
    :cond_6
    if-eqz v0, :cond_7

    const-string v5, "https://plus.google.com/s/%23"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 456
    :cond_7
    invoke-virtual {p2, v3, v4, v0}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 463
    :cond_8
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 464
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->c:Z

    return v0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getInputType()I

    move-result v1

    .line 318
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 322
    const/high16 v0, 0x10000

    or-int/2addr v0, v1

    .line 327
    :goto_0
    if-eq v1, v0, :cond_0

    .line 328
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setRawInputType(I)V

    .line 329
    invoke-static {p0}, Llsn;->c(Landroid/view/View;)V

    .line 331
    :cond_0
    return-void

    .line 324
    :cond_1
    const v0, -0x10001

    and-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkjq;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->b:Lkjm;

    if-nez v0, :cond_0

    .line 171
    const/4 v0, 0x0

    .line 203
    :goto_0
    return-object v0

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 174
    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v5

    .line 175
    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v2, Lljd;

    invoke-interface {v4, v1, v0, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lljd;

    .line 177
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 179
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 184
    array-length v7, v0

    move v3, v1

    :goto_1
    if-ge v3, v7, :cond_3

    .line 185
    aget-object v1, v0, v3

    invoke-virtual {v1}, Lljd;->a()Ljava/lang/String;

    move-result-object v8

    .line 187
    invoke-virtual {v6, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 188
    invoke-virtual {v6, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 192
    aget-object v1, v0, v3

    invoke-interface {v4, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 193
    aget-object v9, v0, v3

    invoke-interface {v4, v9}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v9

    .line 194
    add-int/lit8 v9, v9, 0x1

    invoke-static {v5, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-interface {v4, v1, v9}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 196
    sget-object v9, Lljd;->a:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 197
    const/4 v9, 0x1

    invoke-virtual {v1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 200
    :cond_1
    new-instance v9, Lkjq;

    invoke-direct {v9, v8, v1}, Lkjq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 203
    goto :goto_0
.end method

.method public a(Landroid/text/SpannableStringBuilder;)V
    .locals 3

    .prologue
    .line 388
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const-class v2, Ljava/lang/Object;

    invoke-virtual {p1, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a([Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V

    .line 389
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 371
    invoke-static {p1}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 372
    const/4 v1, 0x0

    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    .line 373
    if-nez v1, :cond_0

    .line 374
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :goto_0
    return-void

    .line 378
    :cond_0
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 379
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 381
    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a([Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V

    goto :goto_0
.end method

.method public a(Lkjm;Ljpj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 234
    iput-object p2, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    .line 235
    iput-object p1, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->b:Lkjm;

    .line 238
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0}, Llsc;->b(Landroid/util/DisplayMetrics;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d:Z

    .line 241
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d:Z

    if-eqz v0, :cond_1

    const-string v0, " "

    .line 244
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 245
    new-instance v3, Lkjv;

    invoke-direct {v3, v0, v2}, Lkjv;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 248
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    .line 249
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setThreshold(I)V

    .line 251
    new-instance v0, Lkjs;

    invoke-direct {v0, p0, v3}, Lkjs;-><init>(Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;Lkjv;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 298
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->b(Z)V

    .line 299
    return-void

    :cond_0
    move v0, v1

    .line 239
    goto :goto_0

    .line 241
    :cond_1
    const-string v0, "\u200b"

    goto :goto_1
.end method

.method public a(Lu;ILjava/lang/String;Lkjm;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 214
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljpk;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpk;

    .line 217
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lu;->p()Lae;

    move-result-object v2

    invoke-virtual {p1}, Lu;->w()Lbb;

    move-result-object v3

    move v4, p2

    .line 216
    invoke-interface/range {v0 .. v5}, Ljpk;->a(Landroid/content/Context;Lae;Lbb;II)Ljpj;

    move-result-object v0

    .line 218
    invoke-interface {v0, v5}, Ljpj;->e(Z)V

    .line 219
    invoke-interface {v0, v5}, Ljpj;->g(Z)V

    .line 220
    invoke-interface {v0, p3}, Ljpj;->a(Ljava/lang/String;)V

    .line 221
    invoke-virtual {p0, p4, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lkjm;Ljpj;)V

    .line 222
    iput-boolean v5, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e:Z

    .line 223
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 353
    iput-boolean p1, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->c:Z

    .line 354
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 303
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 305
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 360
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    invoke-interface {v0}, Ljpj;->au()V

    .line 362
    iput-object v1, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    .line 364
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 365
    return-void
.end method

.method protected convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 131
    move-object v0, p1

    check-cast v0, Landroid/database/Cursor;

    .line 132
    new-instance v1, Landroid/text/SpannableString;

    sget-object v2, Lljd;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 133
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 134
    const-string v2, "person_id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 135
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 136
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    new-instance v2, Lljd;

    invoke-direct {v2, v0}, Lljd;-><init>(Ljava/lang/String;)V

    .line 138
    const/4 v0, 0x0

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-interface {v1, v2, v0, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 140
    :cond_0
    return-object v1
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lkje;->a(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 335
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onAttachedToWindow()V

    .line 336
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    invoke-interface {v0}, Ljpj;->f()V

    .line 339
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 343
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onDetachedFromWindow()V

    .line 344
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    invoke-interface {v0}, Ljpj;->g()V

    .line 347
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 70
    check-cast p1, Lkjt;

    .line 71
    invoke-virtual {p1}, Lkjt;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/MultiAutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    iget-object v2, p1, Lkjt;->a:Landroid/os/Bundle;

    invoke-interface {v0, v2}, Ljpj;->a(Landroid/os/Bundle;)V

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    .line 81
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    .line 83
    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 84
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    .line 85
    invoke-static {v4}, Lljd;->a(Landroid/text/style/URLSpan;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 86
    new-instance v5, Lljd;

    invoke-direct {v5, v4}, Lljd;-><init>(Landroid/text/style/URLSpan;)V

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v8

    invoke-interface {v2, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    invoke-interface {v2, v5, v6, v7, v8}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 84
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    :cond_2
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 60
    const/4 v0, 0x0

    .line 61
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    if-eqz v2, :cond_0

    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a:Ljpj;

    invoke-interface {v2, v0}, Ljpj;->b(Landroid/os/Bundle;)V

    .line 65
    :cond_0
    new-instance v2, Lkjt;

    invoke-direct {v2, v1, v0}, Lkjt;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    return-object v2
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a()Ljava/util/List;

    move-result-object v0

    .line 147
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->replaceText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Ljava/util/List;)V

    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->b(Z)V

    .line 151
    return-void
.end method
