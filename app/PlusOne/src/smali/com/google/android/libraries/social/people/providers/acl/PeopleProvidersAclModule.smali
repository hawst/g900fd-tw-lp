.class public Lcom/google/android/libraries/social/people/providers/acl/PeopleProvidersAclModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    const-class v0, Lhij;

    if-ne p2, v0, :cond_1

    .line 90
    const-class v0, Lhij;

    const/4 v1, 0x2

    new-array v1, v1, [Lhij;

    const/4 v2, 0x0

    new-instance v3, Ljsu;

    invoke-direct {v3}, Ljsu;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljsj;

    invoke-direct {v3}, Ljsj;-><init>()V

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    const-class v0, Lhzd;

    if-ne p2, v0, :cond_0

    .line 96
    const-class v0, Lhzd;

    new-instance v1, Ljsq;

    const-string v2, "CirclesAndPeople"

    const v3, 0x7f0a04f1

    invoke-direct {v1, v2, v3}, Ljsq;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 106
    const-class v0, Lhzd;

    new-instance v1, Ljsr;

    const-string v2, "Circles"

    const v3, 0x7f0a04ee

    invoke-direct {v1, v2, v3}, Ljsr;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 115
    const-class v0, Lhzd;

    new-instance v1, Ljss;

    const-string v2, "People"

    const v3, 0x7f0a04ef

    invoke-direct {v1, v2, v3}, Ljss;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
