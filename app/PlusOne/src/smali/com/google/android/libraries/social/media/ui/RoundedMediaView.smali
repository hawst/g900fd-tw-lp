.class public Lcom/google/android/libraries/social/media/ui/RoundedMediaView;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->e(Z)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->e(Z)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->e(Z)V

    .line 28
    return-void
.end method


# virtual methods
.method protected b(Lkda;)V
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p1}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v1, v0}, Llho;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 40
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 42
    :cond_0
    return-void
.end method
