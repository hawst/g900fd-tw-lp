.class public Lcom/google/android/libraries/social/media/ui/MediaView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;
.implements Lljx;


# static fields
.field private static a:Lizs;

.field private static b:Landroid/view/animation/Interpolator;

.field private static c:Landroid/graphics/Bitmap;

.field private static d:Landroid/graphics/Bitmap;

.field private static e:Landroid/graphics/Bitmap;

.field private static f:Landroid/graphics/Bitmap;

.field private static final g:Landroid/graphics/Paint;

.field private static final h:Landroid/graphics/Paint;

.field private static i:Landroid/graphics/RectF;


# instance fields
.field private A:Z

.field private B:Z

.field private C:I

.field private D:Landroid/graphics/Matrix;

.field private E:Landroid/graphics/Matrix;

.field private F:Landroid/graphics/Rect;

.field private G:Z

.field private H:Landroid/graphics/Rect;

.field private I:Landroid/graphics/RectF;

.field private J:Landroid/graphics/RectF;

.field private K:Landroid/graphics/Matrix;

.field private L:Landroid/graphics/Matrix;

.field private M:Landroid/graphics/drawable/Drawable;

.field private N:Landroid/graphics/Bitmap;

.field private O:I

.field private P:I

.field private Q:Landroid/graphics/Bitmap;

.field private R:Landroid/graphics/drawable/Drawable;

.field private S:Landroid/graphics/drawable/Drawable;

.field private T:I

.field private U:I

.field private V:I

.field private W:Ljbb;

.field private aa:Z

.field private ab:Z

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:I

.field private ag:I

.field private ah:I

.field private ai:I

.field private aj:I

.field private ak:F

.field private al:Z

.field private am:Z

.field private an:I

.field private ao:Z

.field private ap:I

.field private aq:I

.field private ar:Z

.field private as:Ljaz;

.field private j:Lcom/google/android/libraries/social/media/MediaResource;

.field private k:Ljay;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:Landroid/graphics/drawable/Drawable;

.field private t:J

.field private u:I

.field private v:Landroid/graphics/RectF;

.field private w:Lizu;

.field private x:Lizo;

.field private y:Z

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 93
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 94
    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->g:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 100
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 101
    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->h:Landroid/graphics/Paint;

    const v1, 0x66ffffff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->i:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 248
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 112
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->p:Z

    .line 117
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    .line 125
    iput v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    .line 128
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    .line 130
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    .line 131
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->I:Landroid/graphics/RectF;

    .line 132
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->J:Landroid/graphics/RectF;

    .line 133
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->K:Landroid/graphics/Matrix;

    .line 134
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->L:Landroid/graphics/Matrix;

    .line 144
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->V:I

    .line 147
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ab:Z

    .line 158
    iput-boolean v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    .line 159
    iput-boolean v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->am:Z

    .line 162
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->an:I

    .line 164
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ao:Z

    .line 168
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ap:I

    .line 169
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aq:I

    .line 172
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ar:Z

    .line 249
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 250
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 253
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 112
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->p:Z

    .line 117
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    .line 125
    iput v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    .line 128
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    .line 130
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    .line 131
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->I:Landroid/graphics/RectF;

    .line 132
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->J:Landroid/graphics/RectF;

    .line 133
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->K:Landroid/graphics/Matrix;

    .line 134
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->L:Landroid/graphics/Matrix;

    .line 144
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->V:I

    .line 147
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ab:Z

    .line 158
    iput-boolean v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    .line 159
    iput-boolean v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->am:Z

    .line 162
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->an:I

    .line 164
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ao:Z

    .line 168
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ap:I

    .line 169
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aq:I

    .line 172
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ar:Z

    .line 254
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 255
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 258
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 112
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->p:Z

    .line 117
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    .line 125
    iput v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    .line 128
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    .line 130
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    .line 131
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->I:Landroid/graphics/RectF;

    .line 132
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->J:Landroid/graphics/RectF;

    .line 133
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->K:Landroid/graphics/Matrix;

    .line 134
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->L:Landroid/graphics/Matrix;

    .line 144
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->V:I

    .line 147
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ab:Z

    .line 158
    iput-boolean v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    .line 159
    iput-boolean v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->am:Z

    .line 162
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->an:I

    .line 164
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ao:Z

    .line 168
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ap:I

    .line 169
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aq:I

    .line 172
    iput-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ar:Z

    .line 259
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 260
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/media/ui/MediaView;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->z:I

    return v0
.end method

.method private a(I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 474
    if-nez p1, :cond_0

    .line 475
    const/4 v0, 0x0

    .line 477
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/media/ui/MediaView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method private a(ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 10

    .prologue
    .line 713
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    if-nez v0, :cond_4

    .line 716
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->T:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->U:I

    if-eqz v0, :cond_2

    .line 717
    :cond_0
    iget v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->T:I

    .line 718
    iget v4, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->U:I

    .line 723
    :goto_0
    if-nez v3, :cond_1

    if-eqz v4, :cond_3

    .line 724
    :cond_1
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->a:Lizs;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    const/4 v2, 0x0

    iget v5, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->V:I

    iget-object v6, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->v:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->x:Lizo;

    move v8, p1

    move-object v9, p2

    invoke-virtual/range {v0 .. v9}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    .line 730
    :goto_1
    return-object v0

    .line 720
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v3

    .line 721
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v4

    goto :goto_0

    .line 727
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 730
    :cond_4
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->a:Lizs;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    iget v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v4

    const/4 v5, -0x1

    iget-object v6, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->v:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->x:Lizo;

    move v8, p1

    move-object v9, p2

    invoke-virtual/range {v0 .. v9}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/media/ui/MediaView;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 271
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 272
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->a:Lizs;

    if-nez v0, :cond_0

    .line 273
    const-class v0, Lizs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->a:Lizs;

    .line 274
    const v0, 0x7f02053a

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->c:Landroid/graphics/Bitmap;

    .line 276
    const v0, 0x7f02046e

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->d:Landroid/graphics/Bitmap;

    .line 277
    const v0, 0x7f02046b

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->e:Landroid/graphics/Bitmap;

    .line 278
    const v0, 0x7f0202a7

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->f:Landroid/graphics/Bitmap;

    .line 281
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->f:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->Q:Landroid/graphics/Bitmap;

    .line 283
    const v0, 0x7f020416

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    .line 284
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 285
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ak:F

    .line 287
    if-eqz p2, :cond_2

    .line 288
    const-string v0, "size"

    invoke-interface {p2, v2, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_1

    .line 290
    const-string v1, "custom"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 291
    iput v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    .line 305
    :cond_1
    :goto_0
    const-string v0, "scale"

    invoke-interface {p2, v2, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 306
    if-eqz v0, :cond_2

    .line 307
    const-string v1, "zoom"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 308
    iput v4, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    .line 316
    :cond_2
    :goto_1
    invoke-static {p0}, Llii;->g(Landroid/view/View;)V

    .line 318
    const-class v0, Lizp;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizp;

    .line 320
    if-eqz v0, :cond_d

    invoke-interface {v0}, Lizp;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 321
    const-class v0, Ljba;

    .line 322
    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljba;

    .line 323
    if-eqz v0, :cond_3

    .line 324
    invoke-interface {v0, p1}, Ljba;->a(Landroid/content/Context;)Ljaz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->as:Ljaz;

    .line 329
    :cond_3
    :goto_2
    return-void

    .line 292
    :cond_4
    const-string v1, "thumbnail"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 293
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    goto :goto_0

    .line 294
    :cond_5
    const-string v1, "large"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 295
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    goto :goto_0

    .line 296
    :cond_6
    const-string v1, "full"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 297
    iput v4, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    goto :goto_0

    .line 298
    :cond_7
    const-string v1, "original"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 299
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    goto :goto_0

    .line 301
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid size category: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 309
    :cond_a
    const-string v1, "fit"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 310
    iput v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    goto :goto_1

    .line 312
    :cond_b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid scale mode: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_c
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 327
    :cond_d
    iput-object v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->as:Ljaz;

    goto :goto_2
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;II)V
    .locals 3

    .prologue
    .line 1125
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1141
    :cond_0
    :goto_0
    return-void

    .line 1132
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->G:Z

    if-eqz v0, :cond_3

    .line 1133
    :cond_2
    invoke-direct {p0, p3, p4}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(II)V

    .line 1136
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->D:Landroid/graphics/Matrix;

    if-eqz v0, :cond_4

    .line 1137
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->D:Landroid/graphics/Matrix;

    sget-object v1, Lcom/google/android/libraries/social/media/ui/MediaView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 1139
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/libraries/social/media/ui/MediaView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1081
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1082
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->o:Z

    if-eqz v0, :cond_0

    .line 1083
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->Q:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ai:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aj:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1107
    :goto_0
    return-void

    .line 1085
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->s:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1090
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1091
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1092
    iget-object v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->G:Z

    if-eqz v2, :cond_3

    .line 1093
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(II)V

    .line 1094
    iput-boolean v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->G:Z

    .line 1097
    :cond_3
    invoke-virtual {p2, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1098
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->D:Landroid/graphics/Matrix;

    if-eqz v0, :cond_4

    .line 1099
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->D:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1100
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1101
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->E:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 1103
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->K:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1104
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1105
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->L:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/media/ui/MediaView;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    return p1
.end method

.method private b(II)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1170
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingTop()I

    move-result v0

    .line 1171
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingRight()I

    move-result v1

    .line 1172
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingBottom()I

    move-result v2

    .line 1173
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingLeft()I

    move-result v3

    .line 1174
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v4

    sub-int/2addr v4, v3

    sub-int v1, v4, v1

    .line 1175
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v4

    sub-int/2addr v4, v0

    sub-int v2, v4, v2

    .line 1177
    int-to-float v4, p1

    int-to-float v5, p2

    div-float/2addr v4, v5

    .line 1178
    int-to-float v5, v1

    int-to-float v6, v2

    div-float/2addr v5, v6

    .line 1180
    iget v6, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    packed-switch v6, :pswitch_data_0

    .line 1217
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->J:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1218
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->I:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1219
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->K:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->J:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->I:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1220
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->K:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->L:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1221
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->L:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1223
    :cond_0
    return-void

    .line 1182
    :pswitch_0
    iget-object v6, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v6, v7, v7, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1183
    cmpl-float v5, v4, v5

    if-lez v5, :cond_1

    .line 1184
    int-to-float v5, v1

    div-float v4, v5, v4

    float-to-int v4, v4

    sub-int v4, v2, v4

    div-int/lit8 v4, v4, 0x2

    .line 1185
    iget-object v5, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    add-int v6, v0, v4

    add-int/2addr v1, v3

    add-int/2addr v0, v2

    sub-int/2addr v0, v4

    invoke-virtual {v5, v3, v6, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 1188
    :cond_1
    int-to-float v5, v2

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int v4, v1, v4

    div-int/lit8 v4, v4, 0x2

    .line 1189
    iget-object v5, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    add-int v6, v3, v4

    add-int/2addr v1, v3

    sub-int/2addr v1, v4

    add-int/2addr v2, v0

    invoke-virtual {v5, v6, v0, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 1196
    :pswitch_1
    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    .line 1197
    int-to-float v4, p2

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int v4, p1, v4

    div-int/lit8 v4, v4, 0x2

    .line 1198
    iget-object v5, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    sub-int v6, p1, v4

    invoke-virtual {v5, v4, v7, v6, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1206
    :goto_1
    iget-object v4, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    add-int/2addr v1, v3

    add-int/2addr v2, v0

    invoke-virtual {v4, v3, v0, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 1200
    :cond_2
    int-to-float v4, p1

    div-float/2addr v4, v5

    float-to-int v4, v4

    .line 1202
    int-to-float v5, p2

    iget v6, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ak:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 1203
    div-int/lit8 v6, v4, 0x2

    sub-int/2addr v5, v6

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1204
    iget-object v6, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    add-int/2addr v4, v5

    invoke-virtual {v6, v7, v5, p1, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    .line 1212
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0, v7, v7, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1213
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->H:Landroid/graphics/Rect;

    invoke-virtual {v0, v7, v7, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 1180
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    .line 1144
    if-eqz p2, :cond_0

    .line 1145
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingTop()I

    move-result v1

    .line 1146
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1145
    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1147
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1149
    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/media/ui/MediaView;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/media/ui/MediaView;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->G:Z

    return p1
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->d()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/media/ui/MediaView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 761
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 770
    :goto_0
    return-void

    .line 765
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 766
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lljh;

    if-eqz v0, :cond_1

    .line 767
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lljh;

    invoke-interface {v0}, Lljh;->a()V

    .line 769
    :cond_1
    iput-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 860
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->am:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    if-eqz v0, :cond_1

    .line 870
    :cond_0
    :goto_0
    return-void

    .line 863
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->h()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 864
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->k:Ljay;

    if-nez v0, :cond_3

    .line 865
    new-instance v0, Ljay;

    invoke-direct {v0, p0}, Ljay;-><init>(Lcom/google/android/libraries/social/media/ui/MediaView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->k:Ljay;

    .line 867
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->k:Ljay;

    iget-boolean v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->l:Z

    invoke-virtual {v0, v1}, Ljay;->a(Z)V

    .line 868
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->k:Ljay;

    invoke-virtual {v0}, Ljay;->b()V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/media/ui/MediaView;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ao:Z

    return v0
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/media/ui/MediaView;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->an:I

    return v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 873
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->k:Ljay;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->k:Ljay;

    invoke-virtual {v0}, Ljay;->c()V

    .line 876
    :cond_0
    return-void
.end method

.method public static synthetic v()Lizs;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->a:Lizs;

    return-object v0
.end method

.method private w()Z
    .locals 1

    .prologue
    .line 1034
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->z:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 810
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 811
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 813
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->V:I

    .line 814
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->l:Z

    .line 816
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->as:Ljaz;

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->as:Ljaz;

    invoke-interface {v0}, Ljaz;->a()V

    .line 819
    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 1261
    iput p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ak:F

    .line 1262
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->T:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->U:I

    if-eq v0, p2, :cond_1

    .line 405
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 406
    iput p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->T:I

    .line 407
    iput p2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->U:I

    .line 408
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b()V

    .line 410
    :cond_1
    return-void
.end method

.method public a(IZ)V
    .locals 2

    .prologue
    .line 518
    if-eqz p2, :cond_0

    .line 519
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->z:I

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    .line 523
    :goto_0
    return-void

    .line 521
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->z:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 488
    if-nez p1, :cond_0

    sget-object p1, Lcom/google/android/libraries/social/media/ui/MediaView;->f:Landroid/graphics/Bitmap;

    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->Q:Landroid/graphics/Bitmap;

    .line 489
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1039
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 1050
    :cond_0
    :goto_0
    return-void

    .line 1041
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1042
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/MediaResource;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    .line 1043
    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/MediaResource;->getHeight()I

    move-result v2

    .line 1042
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;II)V

    goto :goto_0

    .line 1044
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->N:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 1045
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->N:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->O:I

    iget v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->P:I

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;II)V

    goto :goto_0

    .line 1046
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1047
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 393
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    .line 394
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->D:Landroid/graphics/Matrix;

    .line 395
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->E:Landroid/graphics/Matrix;

    .line 396
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->D:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->E:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 397
    return-void
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->v:Landroid/graphics/RectF;

    .line 345
    return-void
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 352
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    .line 353
    return-void
.end method

.method public a(Lizu;)V
    .locals 2

    .prologue
    .line 552
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;Lizo;Z)V

    .line 553
    return-void
.end method

.method public a(Lizu;Lizo;)V
    .locals 1

    .prologue
    .line 562
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;Lizo;Z)V

    .line 563
    return-void
.end method

.method public a(Lizu;Lizo;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 606
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    invoke-virtual {v0, p1}, Lizu;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->x:Lizo;

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_4

    .line 622
    :goto_1
    return-void

    .line 606
    :cond_0
    if-nez v0, :cond_1

    if-nez p2, :cond_2

    :cond_1
    if-eqz v0, :cond_3

    if-nez p2, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {v0, p2}, Lizo;->a(Lizo;)Z

    move-result v0

    goto :goto_0

    .line 610
    :cond_4
    iput-boolean p3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->y:Z

    .line 611
    iput-object p2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->x:Lizo;

    .line 613
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 615
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    .line 616
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    if-eqz v0, :cond_5

    .line 617
    iput-boolean v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->m:Z

    .line 619
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b()V

    .line 621
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    goto :goto_1
.end method

.method public a(Lizu;Z)V
    .locals 1

    .prologue
    .line 592
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;Lizo;Z)V

    .line 593
    return-void
.end method

.method public a(Ljbb;)V
    .locals 0

    .prologue
    .line 1247
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->W:Ljbb;

    .line 1248
    return-void
.end method

.method public a(Lkda;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 832
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 852
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->W:Ljbb;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->am:Z

    if-eqz v0, :cond_0

    .line 853
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->W:Ljbb;

    invoke-interface {v0, p0}, Ljbb;->a(Lcom/google/android/libraries/social/media/ui/MediaView;)V

    .line 856
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 857
    return-void

    .line 834
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 835
    iget-wide v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->t:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 836
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->t:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 837
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->n:Z

    if-eqz v0, :cond_2

    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->b:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->b:Landroid/view/animation/Interpolator;

    :cond_1
    const v0, 0x3c23d70a    # 0.01f

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setAlpha(F)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/media/ui/MediaView;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 839
    :cond_2
    iput-wide v6, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->t:J

    .line 840
    iput-boolean v4, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->am:Z

    .line 841
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->e()V

    .line 842
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Lkda;)V

    goto :goto_0

    .line 847
    :sswitch_1
    iput-boolean v4, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->am:Z

    goto :goto_0

    .line 832
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 664
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->B:Z

    if-nez v0, :cond_0

    .line 665
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->n()V

    .line 667
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 335
    iput p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    .line 336
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->q:Landroid/graphics/drawable/Drawable;

    .line 445
    return-void
.end method

.method public b(Lkda;)V
    .locals 0

    .prologue
    .line 268
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 671
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->o()V

    .line 672
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 356
    iput p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->an:I

    .line 357
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->an:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Liro;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    check-cast v0, Liro;

    iget v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->an:I

    invoke-virtual {v0, v1}, Liro;->a(I)V

    .line 360
    :cond_0
    return-void
.end method

.method public c(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    .line 456
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 382
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    if-eq p1, v0, :cond_0

    .line 383
    iput p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->C:I

    .line 384
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 385
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 387
    :cond_0
    return-void
.end method

.method public d(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 466
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->s:Landroid/graphics/drawable/Drawable;

    .line 467
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 951
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 953
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 954
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 955
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 956
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 963
    :cond_1
    :goto_0
    return-void

    .line 957
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 958
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingTop()I

    move-result v2

    .line 959
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    .line 958
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 960
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1162
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1163
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 1166
    :cond_0
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 1167
    return-void
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 416
    iput p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->V:I

    .line 417
    return-void
.end method

.method public e(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 576
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    .line 577
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 580
    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 348
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ar:Z

    .line 349
    return-void
.end method

.method public f(I)V
    .locals 1

    .prologue
    .line 448
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->q:Landroid/graphics/drawable/Drawable;

    .line 449
    return-void
.end method

.method public f(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->S:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 803
    iput-object p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->S:Landroid/graphics/drawable/Drawable;

    .line 804
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 806
    :cond_0
    return-void
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 366
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ao:Z

    .line 367
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 913
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    if-eqz v0, :cond_0

    sget-object v0, Ljac;->b:Ljac;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    invoke-virtual {v1}, Lizu;->g()Ljac;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 459
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    .line 460
    return-void
.end method

.method public g(Z)V
    .locals 1

    .prologue
    .line 423
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->o:Z

    if-eq p1, v0, :cond_0

    .line 424
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->o:Z

    .line 425
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 427
    :cond_0
    return-void
.end method

.method public g()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 897
    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    if-eqz v1, :cond_1

    sget-object v1, Ljac;->c:Ljac;

    iget-object v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    invoke-virtual {v2}, Lizu;->g()Ljac;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 905
    :cond_0
    :goto_0
    return v0

    .line 901
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/MediaResource;->getResourceType()I

    move-result v1

    invoke-static {v1}, Llsd;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 905
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1113
    if-nez p1, :cond_0

    .line 1114
    const/4 v0, 0x0

    .line 1121
    :goto_0
    return v0

    .line 1117
    :cond_0
    instance-of v0, p1, Liro;

    if-nez v0, :cond_1

    .line 1118
    const/4 v0, 0x1

    goto :goto_0

    .line 1121
    :cond_1
    check-cast p1, Liro;

    invoke-virtual {p1}, Liro;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public h(I)V
    .locals 1

    .prologue
    .line 498
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/graphics/Bitmap;)V

    .line 499
    return-void
.end method

.method public h(Z)V
    .locals 0

    .prologue
    .line 433
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->p:Z

    .line 434
    return-void
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    if-eqz v0, :cond_0

    sget-object v0, Ljac;->d:Ljac;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    invoke-virtual {v1}, Lizu;->g()Ljac;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(I)V
    .locals 2

    .prologue
    .line 505
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->w()Z

    move-result v1

    .line 506
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 507
    :goto_0
    iput p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->z:I

    .line 508
    if-eq v1, v0, :cond_0

    .line 509
    if-eqz v0, :cond_2

    .line 510
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->e()V

    .line 515
    :cond_0
    :goto_1
    return-void

    .line 506
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 511
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    if-nez v0, :cond_0

    .line 512
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->i()V

    goto :goto_1
.end method

.method public i(Z)V
    .locals 0

    .prologue
    .line 437
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->l:Z

    .line 438
    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    .line 890
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 894
    :goto_0
    return-void

    .line 892
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public j(Z)V
    .locals 0

    .prologue
    .line 532
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->A:Z

    .line 533
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 1029
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ab:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 881
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 882
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 883
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 885
    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Liro;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    check-cast v0, Liro;

    invoke-virtual {v0}, Liro;->b()V

    .line 376
    :cond_0
    return-void
.end method

.method public k(Z)V
    .locals 0

    .prologue
    .line 569
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->m:Z

    .line 570
    return-void
.end method

.method public l()Lizu;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    return-object v0
.end method

.method public l(Z)V
    .locals 0

    .prologue
    .line 642
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->n:Z

    .line 643
    return-void
.end method

.method public m(Z)V
    .locals 0

    .prologue
    .line 649
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aa:Z

    .line 650
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 651
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 659
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ab:Z

    return v0
.end method

.method public n()V
    .locals 4

    .prologue
    .line 678
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 679
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Size category is not set: "

    .line 680
    invoke-static {p0}, Llii;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 683
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v0

    if-nez v0, :cond_3

    .line 684
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v0

    if-nez v0, :cond_3

    .line 710
    :cond_2
    :goto_1
    return-void

    .line 688
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->t:J

    .line 689
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 690
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->w:Lizu;

    if-eqz v0, :cond_5

    .line 694
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->z:I

    or-int/lit8 v0, v0, 0x40

    and-int/lit8 v0, v0, -0x5

    .line 696
    iget-boolean v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ar:Z

    if-nez v1, :cond_4

    .line 697
    and-int/lit8 v0, v0, -0x41

    .line 700
    :cond_4
    invoke-direct {p0, v0, p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    .line 702
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->as:Ljaz;

    if-eqz v0, :cond_2

    .line 703
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->as:Ljaz;

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-interface {v0, v1}, Ljaz;->a(Lcom/google/android/libraries/social/media/MediaResource;)V

    goto :goto_1

    .line 707
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->N:Landroid/graphics/Bitmap;

    .line 708
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->d()V

    goto :goto_1
.end method

.method public n(Z)V
    .locals 0

    .prologue
    .line 654
    iput-boolean p1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ab:Z

    .line 655
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 656
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 739
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->y:Z

    if-nez v0, :cond_0

    .line 740
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 741
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->N:Landroid/graphics/Bitmap;

    .line 742
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->O:I

    .line 743
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->P:I

    .line 747
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_1

    .line 748
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 749
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    .line 752
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->i()V

    .line 754
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->d()V

    .line 755
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 756
    iput-boolean v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->am:Z

    .line 757
    iput-boolean v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    .line 758
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 774
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 775
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->B:Z

    .line 776
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b()V

    .line 777
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 781
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 782
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 783
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 967
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 969
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aa:Z

    if-eqz v0, :cond_0

    .line 970
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->i:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 971
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->i:Landroid/graphics/RectF;

    const/16 v2, 0x69

    const/16 v3, 0x1f

    invoke-virtual {p1, v0, v2, v3}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    .line 974
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->s()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->N:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->m:Z

    if-eqz v0, :cond_9

    .line 975
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/graphics/Canvas;)V

    .line 977
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->p:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->m:Z

    if-nez v0, :cond_2

    .line 978
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 979
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->c:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ac:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ad:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1009
    :cond_2
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aa:Z

    if-eqz v0, :cond_3

    .line 1010
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1013
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->S:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    .line 1014
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v3

    invoke-virtual {v0, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1015
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->S:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 1016
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1019
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->j()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1020
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/libraries/social/media/ui/MediaView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1023
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->as:Ljaz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_6

    .line 1024
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->as:Ljaz;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getHeight()I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Ljaz;->a(Landroid/graphics/Canvas;II)V

    .line 1026
    :cond_6
    return-void

    .line 980
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 981
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->d:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ae:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->af:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 982
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->al:Z

    if-nez v0, :cond_2

    .line 983
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->e:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ag:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ah:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 986
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->o:Z

    if-eqz v0, :cond_a

    .line 987
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->Q:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ai:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aj:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 988
    :cond_a
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_b

    .line 989
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 992
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->q:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 996
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1002
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->s:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1006
    :cond_b
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->q:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 989
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 918
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 919
    sub-int v1, p4, p2

    .line 920
    sub-int v2, p5, p3

    .line 921
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ac:I

    .line 922
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ad:I

    .line 923
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ae:I

    .line 924
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->af:I

    .line 925
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ag:I

    .line 926
    sget-object v0, Lcom/google/android/libraries/social/media/ui/MediaView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ah:I

    .line 927
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->Q:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ai:I

    .line 928
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->Q:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aj:I

    .line 932
    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aq:I

    if-eq v1, v0, :cond_1

    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ap:I

    if-eq v2, v0, :cond_1

    .line 933
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    if-nez v0, :cond_3

    .line 934
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->T:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->U:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 935
    :goto_0
    if-eqz v0, :cond_0

    .line 936
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 937
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b()V

    .line 943
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 945
    :cond_1
    iput v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->aq:I

    .line 946
    iput v2, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->ap:I

    .line 947
    return-void

    .line 934
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 939
    :cond_3
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->u:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_0

    .line 940
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 941
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b()V

    goto :goto_1
.end method

.method public p()V
    .locals 1

    .prologue
    .line 787
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->A:Z

    if-eqz v0, :cond_0

    .line 788
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->B:Z

    .line 789
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 791
    :cond_0
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 795
    iget-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->A:Z

    if-eqz v0, :cond_0

    .line 796
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->B:Z

    .line 797
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b()V

    .line 799
    :cond_0
    return-void
.end method

.method public r()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 822
    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/MediaResource;->getStatus()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 826
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelected(Z)V
    .locals 1

    .prologue
    .line 1238
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->isSelected()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 1239
    invoke-super {p0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 1240
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1241
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 1244
    :cond_0
    return-void
.end method

.method public t()I
    .locals 1

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1063
    :goto_0
    return v0

    .line 1055
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1056
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getWidth()I

    move-result v0

    goto :goto_0

    .line 1057
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->N:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 1058
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->O:I

    goto :goto_0

    .line 1059
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 1060
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0

    .line 1063
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1068
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 1077
    :goto_0
    return v0

    .line 1069
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1070
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->j:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getHeight()I

    move-result v0

    goto :goto_0

    .line 1071
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->N:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 1072
    iget v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->P:I

    goto :goto_0

    .line 1073
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 1074
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0

    .line 1077
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->M:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/media/ui/MediaView;->R:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    .line 1154
    :cond_0
    const/4 v0, 0x1

    .line 1156
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
