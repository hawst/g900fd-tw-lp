.class public Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private N:Lhee;

.field private O:Landroid/widget/ListView;

.field private P:Ljiz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Llol;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 36
    const v0, 0x7f0400c5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 37
    const v0, 0x7f1002f9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->O:Landroid/widget/ListView;

    .line 38
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->N:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 55
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 56
    new-instance v0, Ljjs;

    iget-object v2, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->at:Llnl;

    invoke-direct {v0, v2, v1, p1}, Ljjs;-><init>(Landroid/content/Context;II)V

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->P:Ljiz;

    invoke-virtual {p1}, Ldo;->o()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Ljiz;->b(ILandroid/database/Cursor;)V

    .line 64
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->N:Lhee;

    .line 32
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-super {p0, p1}, Llol;->d(Landroid/os/Bundle;)V

    .line 44
    new-instance v0, Ljiz;

    iget-object v1, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->at:Llnl;

    invoke-direct {v0, v1}, Ljiz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->P:Ljiz;

    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->P:Ljiz;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 49
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/debug/GunsStatisticsFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 50
    return-void
.end method
