.class public final Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# static fields
.field private static a:I


# instance fields
.field private b:Ljsh;

.field private final c:Lhis;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 51
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 63
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    new-instance v0, Ljsh;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ljsh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    const-string v1, "avatar_pile_tag"

    invoke-virtual {v0, v1}, Ljsh;->setTag(Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->addView(Landroid/view/View;)V

    .line 71
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c:Lhis;

    .line 72
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 74
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 75
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 76
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    new-instance v0, Ljsh;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ljsh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    const-string v1, "avatar_pile_tag"

    invoke-virtual {v0, v1}, Ljsh;->setTag(Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->addView(Landroid/view/View;)V

    .line 71
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c:Lhis;

    .line 72
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 74
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 75
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 76
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    new-instance v0, Ljsh;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ljsh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    const-string v1, "avatar_pile_tag"

    invoke-virtual {v0, v1}, Ljsh;->setTag(Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->addView(Landroid/view/View;)V

    .line 71
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c:Lhis;

    .line 72
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 74
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 75
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 76
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    .line 120
    sget v0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a:I

    if-nez v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01fe

    .line 122
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a:I

    .line 124
    :cond_0
    sget v0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a:I

    return v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 131
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 132
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 133
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "item_check_view_tag"

    if-eq v2, v3, :cond_0

    .line 134
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 131
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_1
    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a(IZ)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    move-result-object v0

    return-object v0
.end method

.method public a(IZ)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c()V

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    invoke-virtual {v0, p1, p2}, Ljsh;->a(IZ)V

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljsh;->setVisibility(I)V

    .line 102
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;)",
            "Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c()V

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    invoke-virtual {v0, p1}, Ljsh;->a(Ljava/util/List;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->b:Ljsh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljsh;->setVisibility(I)V

    .line 89
    return-object p0
.end method

.method public a()Lhis;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->c:Lhis;

    return-object v0
.end method
