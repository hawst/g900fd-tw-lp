.class public Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;
.super Lcom/google/android/libraries/social/ui/views/EsScrollView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhmm;
.implements Lljh;


# static fields
.field private static a:Z

.field private static b:I


# instance fields
.field private final c:Landroid/view/LayoutInflater;

.field private d:Lkyl;

.field private e:Lkyk;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkyf;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:I

.field private j:Z

.field private k:Landroid/widget/TextView;

.field private l:I

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 194
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/ui/views/EsScrollView;-><init>(Landroid/content/Context;)V

    .line 125
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g:Z

    .line 126
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    .line 127
    iput v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->i:I

    .line 128
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->j:Z

    .line 212
    sget-boolean v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a:Z

    if-nez v0, :cond_0

    .line 213
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a:Z

    .line 215
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 216
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b:I

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c:Landroid/view/LayoutInflater;

    .line 195
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 201
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/ui/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 125
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g:Z

    .line 126
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    .line 127
    iput v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->i:I

    .line 128
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->j:Z

    .line 212
    sget-boolean v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a:Z

    if-nez v0, :cond_0

    .line 213
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a:Z

    .line 215
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 216
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b:I

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c:Landroid/view/LayoutInflater;

    .line 202
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 208
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/ui/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 125
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g:Z

    .line 126
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    .line 127
    iput v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->i:I

    .line 128
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->j:Z

    .line 212
    sget-boolean v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a:Z

    if-nez v0, :cond_0

    .line 213
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a:Z

    .line 215
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 216
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b:I

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c:Landroid/view/LayoutInflater;

    .line 209
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->n:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;I)I
    .locals 0

    .prologue
    .line 55
    iput p1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->i:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->j:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Lkyk;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->j:Z

    return v0
.end method

.method private d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 626
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lkyl;

    invoke-direct {v0, p0}, Lkyl;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    .line 224
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->m:Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->a(Z)V

    .line 225
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->l:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->f:Landroid/widget/TextView;

    sget v1, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->f:Landroid/widget/TextView;

    .line 230
    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v1

    .line 229
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->e(Landroid/graphics/drawable/Drawable;)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v1, Lkyl;->h:Landroid/widget/ImageButton;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v3, v3, Lkyl;->h:Landroid/widget/ImageButton;

    .line 235
    invoke-virtual {v3}, Landroid/widget/ImageButton;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v4, v4, Lkyl;->h:Landroid/widget/ImageButton;

    .line 236
    invoke-virtual {v4}, Landroid/widget/ImageButton;->getPaddingBottom()I

    move-result v4

    .line 233
    invoke-static {v0, v1, v2, v3, v4}, Lkcr;->a(Landroid/content/Context;Landroid/widget/ImageButton;III)V

    .line 238
    :cond_0
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 302
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    .line 305
    iget v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->l:I

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 322
    :goto_0
    iget v3, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->m:I

    if-eqz v3, :cond_0

    .line 323
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v3

    iget v4, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->m:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 324
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f11001d

    iget v6, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->m:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v3, v7, v1

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 326
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    if-eqz v0, :cond_0

    .line 329
    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    :cond_0
    if-eqz v0, :cond_1

    .line 334
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    :cond_1
    invoke-static {v2}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    .line 338
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 339
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 344
    :goto_1
    return-void

    .line 307
    :pswitch_0
    const v0, 0x7f0a0465

    .line 308
    goto :goto_0

    .line 312
    :pswitch_1
    const v0, 0x7f0a0466

    .line 313
    goto :goto_0

    .line 341
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v2, v2, Lkyl;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v1, Lkyl;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 305
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private g()V
    .locals 2

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v0, Lkyl;->g:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 631
    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g:Z

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v0, Lkyl;->g:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    if-eqz v0, :cond_2

    const v0, 0x7f02050a

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 635
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v0, Lkyl;->g:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0a0226

    .line 636
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d(I)Ljava/lang/String;

    move-result-object v0

    .line 635
    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 639
    :cond_0
    return-void

    .line 630
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 632
    :cond_2
    const v0, 0x7f02050d

    goto :goto_1

    .line 636
    :cond_3
    const v0, 0x7f0a0225

    .line 637
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 599
    iput-object v2, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    .line 601
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 604
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 605
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->l:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 606
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->o:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 607
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->n:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 609
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 611
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Z)V

    .line 612
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d()V

    .line 615
    :cond_0
    iput-object v2, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    .line 616
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 289
    iput p1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->l:I

    .line 290
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->f()V

    .line 291
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    return-void
.end method

.method public a(Ljava/util/List;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkyf;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 437
    iput-object p1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->f:Ljava/util/List;

    .line 438
    iput p2, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->i:I

    .line 439
    if-nez p1, :cond_1

    .line 445
    :cond_0
    return-void

    .line 443
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 444
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v2, v3

    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->j:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->i:I

    if-ne v2, v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0401f0

    iget-object v4, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v4, v4, Lkyl;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->f:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkyf;

    invoke-virtual {v1}, Lkyf;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v1, Lkyl;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->i:I

    if-ne v2, v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->j:Z

    if-eqz v1, :cond_5

    const v1, 0x7f0204d4

    :goto_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v4, v5, :cond_6

    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    :cond_3
    :goto_2
    new-instance v1, Lkyi;

    invoke-direct {v1, p0, v2}, Lkyi;-><init>(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    const v1, 0x7f0204c0

    goto :goto_1

    :cond_6
    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_2
.end method

.method public a(Lkyk;)V
    .locals 0

    .prologue
    .line 619
    iput-object p1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    .line 620
    return-void
.end method

.method public a(Lnse;)V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->m:Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->a(Lnse;)V

    .line 380
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b()V

    .line 381
    return-void
.end method

.method public a(Lpdt;)V
    .locals 4

    .prologue
    .line 387
    if-nez p1, :cond_1

    .line 388
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->n:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 405
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b()V

    .line 406
    return-void

    .line 390
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->n:Landroid/widget/TextView;

    iget-object v1, p1, Lpdt;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->n:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 393
    iget-object v0, p1, Lpdt;->b:Ljava/lang/String;

    .line 394
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 395
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v1, Lkyl;->n:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 396
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v1, Lkyl;->n:Landroid/widget/TextView;

    new-instance v2, Lkyg;

    invoke-direct {v2, p0, v0}, Lkyg;-><init>(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 414
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 415
    if-eqz p1, :cond_0

    iget v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->m:I

    if-lez v1, :cond_0

    .line 416
    new-instance v1, Llju;

    const/4 v2, 0x0

    new-instance v3, Lkyh;

    invoke-direct {v3, p0}, Lkyh;-><init>(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)V

    invoke-direct {v1, v2, v4, v3}, Llju;-><init>(Ljava/lang/String;ZLljv;)V

    .line 427
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 428
    const-string v3, " - "

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/16 v3, 0x21

    invoke-virtual {v2, v1, v4, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 429
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    :goto_0
    return-void

    .line 431
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v1, Lkyl;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(ZIZ)V
    .locals 2

    .prologue
    .line 247
    iput p2, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->n:I

    .line 248
    iput-boolean p3, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->j:Z

    .line 249
    iput-boolean p1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    .line 250
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e()V

    .line 252
    const v0, 0x7f1005c0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->k:Landroid/widget/TextView;

    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v0, Lkyl;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->requestLayout()V

    .line 256
    return-void

    .line 253
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 643
    new-instance v0, Lhmk;

    sget-object v1, Lomv;->C:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->m:Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;

    .line 512
    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->n:Landroid/widget/TextView;

    .line 513
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->l:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 514
    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 515
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g:Z

    .line 516
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g()V

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v0, Lkyl;->j:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 519
    return-void

    .line 518
    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 297
    iput p1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->m:I

    .line 298
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->f()V

    .line 299
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 277
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 279
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v1, p1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 283
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->k(Z)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v0, Lkyl;->h:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 507
    return-void

    .line 506
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 527
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "plus_privacy_block"

    const-string v2, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v1, v2}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 528
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0224

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 529
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    .line 528
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 530
    invoke-static {v0}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 531
    invoke-interface {v1}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v2, Landroid/text/style/URLSpan;

    invoke-interface {v1, v5, v0, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 532
    array-length v2, v0

    if-lez v2, :cond_0

    .line 533
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 534
    aget-object v0, v0, v5

    .line 535
    invoke-interface {v1, v0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    .line 536
    invoke-interface {v1, v0}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 537
    new-instance v4, Lkyj;

    invoke-direct {v4, p0, v0}, Lkyj;-><init>(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;Landroid/text/style/URLSpan;)V

    const/16 v0, 0x21

    invoke-virtual {v2, v4, v3, v1, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 551
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 552
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->p:Landroid/widget/TextView;

    .line 553
    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v1

    .line 552
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 554
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 556
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->l:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(I)V

    .line 502
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b()V

    .line 503
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 352
    if-nez p1, :cond_0

    .line 353
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 358
    :goto_0
    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v0, Lkyl;->o:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 523
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->o:Landroid/widget/Button;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 524
    return-void

    .line 522
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 523
    :cond_1
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 559
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 560
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 561
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->p:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 562
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 364
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->k:Landroid/widget/TextView;

    const/16 v1, 0x9

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 367
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b()V

    .line 373
    return-void

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x4

    .line 566
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 568
    const v0, 0x7f1001e6

    if-eq v2, v0, :cond_0

    const v0, 0x7f1005c3

    if-ne v2, v0, :cond_2

    .line 569
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    .line 570
    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g:Z

    if-eqz v0, :cond_1

    .line 571
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    if-eqz v0, :cond_5

    .line 572
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    iget-boolean v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    invoke-interface {v0, v1}, Lkyk;->c(Z)V

    .line 578
    :cond_1
    :goto_1
    invoke-static {p0, v4}, Lhly;->a(Landroid/view/View;I)V

    .line 581
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    if-nez v0, :cond_7

    .line 595
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v1

    .line 569
    goto :goto_0

    .line 574
    :cond_5
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->g()V

    .line 575
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->b:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->h:Z

    if-eqz v3, :cond_6

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_6
    const/16 v1, 0x8

    goto :goto_3

    .line 585
    :cond_7
    const v0, 0x7f1005b9

    if-ne v2, v0, :cond_8

    .line 586
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v1, v1, Lkyl;->l:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c()I

    move-result v1

    invoke-interface {v0, v1}, Lkyk;->d(I)V

    .line 587
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->l:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-static {v0, v4}, Lhly;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 588
    :cond_8
    const v0, 0x7f100406

    if-ne v2, v0, :cond_9

    .line 589
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    invoke-interface {v0}, Lkyk;->ae()V

    .line 590
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->h:Landroid/widget/ImageButton;

    invoke-static {v0, v4}, Lhly;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 591
    :cond_9
    const v0, 0x7f1005ba

    if-ne v2, v0, :cond_3

    .line 592
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e:Lkyk;

    invoke-interface {v0}, Lkyk;->af()V

    .line 593
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d:Lkyl;

    iget-object v0, v0, Lkyl;->o:Landroid/widget/Button;

    invoke-static {v0, v4}, Lhly;->a(Landroid/view/View;I)V

    goto :goto_2
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 260
    invoke-super {p0}, Lcom/google/android/libraries/social/ui/views/EsScrollView;->onFinishInflate()V

    .line 261
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->e()V

    .line 262
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->setVerticalFadingEdgeEnabled(Z)V

    .line 263
    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->setFadingEdgeLength(I)V

    .line 264
    return-void
.end method
