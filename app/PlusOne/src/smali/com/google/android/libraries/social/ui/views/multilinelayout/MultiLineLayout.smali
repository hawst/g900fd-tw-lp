.class public Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Llle;

.field private d:Lllf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a:I

    .line 17
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->b:I

    .line 19
    new-instance v0, Llle;

    invoke-direct {v0, p0}, Llle;-><init>(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->c:Llle;

    .line 21
    new-instance v0, Llld;

    invoke-direct {v0, p0}, Llld;-><init>(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->d:Lllf;

    .line 30
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;)I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->b:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;I)I
    .locals 0

    .prologue
    .line 14
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;II)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->measureChild(Landroid/view/View;II)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;)I
    .locals 2

    .prologue
    .line 14
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a:I

    return v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;I)I
    .locals 0

    .prologue
    .line 14
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->b:I

    return p1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a:I

    return v0
.end method

.method public a(I)I
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getPaddingTop()I

    move-result v0

    .line 49
    add-int/lit8 v1, p1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->b:I

    mul-int/2addr v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->d:Lllf;

    sub-int v1, p4, p2

    invoke-virtual {v0, v1}, Lllf;->a(I)V

    .line 41
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->c:Llle;

    invoke-virtual {v0, p1, p2}, Llle;->a(II)V

    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->c:Llle;

    const v1, 0x7fffffff

    invoke-static {v1, p1}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->resolveSize(II)I

    move-result v1

    invoke-virtual {v0, v1}, Llle;->a(I)V

    .line 36
    return-void
.end method
