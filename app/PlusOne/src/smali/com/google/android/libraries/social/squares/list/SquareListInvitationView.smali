.class public Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;
.super Lcom/google/android/libraries/social/squares/list/SquareListItemView;
.source "PG"


# instance fields
.field private c:Ljava/lang/String;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private f:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    :cond_1
    return-void
.end method

.method public a(Landroid/database/Cursor;Lkut;ZZ)V
    .locals 6

    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Landroid/database/Cursor;Lkut;ZZ)V

    .line 74
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->c:Ljava/lang/String;

    .line 75
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    const/16 v1, 0xa

    .line 77
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0458

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 81
    iget-object v2, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->d:Landroid/widget/TextView;

    invoke-static {v0}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v2, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->c:Ljava/lang/String;

    .line 88
    invoke-static {v1}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-void

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a0459

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    iget-object v2, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->b()V

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->d:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 58
    const v0, 0x7f1005c6

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->d:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f1005c5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 60
    const v0, 0x7f1005c7

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->f:Landroid/widget/ImageView;

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    new-instance v1, Lhmk;

    sget-object v2, Long;->i:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->f:Landroid/widget/ImageView;

    new-instance v1, Lhmk;

    sget-object v2, Lomv;->F:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 67
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->b:Lkut;

    if-eqz v0, :cond_1

    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 98
    const v1, 0x7f1005c5

    if-ne v0, v1, :cond_3

    .line 99
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const-string v2, "square_list_avatar_"

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setTransitionName(Ljava/lang/String;)V

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhkr;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lhkr;->a(Landroid/view/View;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->b:Lkut;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lkut;->g(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->e:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-static {v0, v4}, Lhly;->a(Landroid/view/View;I)V

    .line 112
    :cond_1
    :goto_1
    return-void

    .line 100
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_3
    const v1, 0x7f1005c7

    if-ne v0, v1, :cond_4

    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->b:Lkut;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lkut;->h(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListInvitationView;->f:Landroid/widget/ImageView;

    invoke-static {v0, v4}, Lhly;->a(Landroid/view/View;I)V

    goto :goto_1

    .line 109
    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->onClick(Landroid/view/View;)V

    goto :goto_1
.end method
