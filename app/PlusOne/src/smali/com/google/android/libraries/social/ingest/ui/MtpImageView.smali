.class public Lcom/google/android/libraries/social/ingest/ui/MtpImageView;
.super Landroid/widget/ImageView;
.source "PG"


# static fields
.field private static final i:Lisw;

.field private static final j:Lisx;


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/libraries/social/ingest/ui/MtpImageView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Object;

.field private e:Z

.field private f:Lisj;

.field private g:Landroid/mtp/MtpDevice;

.field private h:Ljava/lang/Object;

.field private k:F

.field private l:F

.field private m:I

.field private n:Landroid/graphics/Matrix;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lisw;->a()Lisw;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->i:Lisw;

    .line 59
    new-instance v0, Lisx;

    invoke-direct {v0}, Lisx;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->j:Lisx;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 49
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c:Ljava/lang/ref/WeakReference;

    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->d:Ljava/lang/Object;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->e:Z

    .line 123
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    .line 67
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setImageResource(I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c:Ljava/lang/ref/WeakReference;

    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->d:Ljava/lang/Object;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->e:Z

    .line 123
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    .line 72
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setImageResource(I)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c:Ljava/lang/ref/WeakReference;

    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->d:Ljava/lang/Object;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->e:Z

    .line 123
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    .line 77
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setImageResource(I)V

    .line 78
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;Landroid/mtp/MtpDevice;)Landroid/mtp/MtpDevice;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->g:Landroid/mtp/MtpDevice;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;Lisj;)Lisj;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->f:Lisj;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->h:Ljava/lang/Object;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->e:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Landroid/mtp/MtpDevice;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->g:Landroid/mtp/MtpDevice;

    return-object v0
.end method

.method public static synthetic b()Lisx;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->j:Lisx;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Lisj;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->f:Lisj;

    return-object v0
.end method

.method private c()V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v9, 0x3f000000    # 0.5f

    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 129
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    .line 130
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->getWidth()I

    move-result v0

    int-to-float v5, v0

    .line 132
    iget v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->m:I

    rem-int/lit16 v0, v0, 0xb4

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v3, v0

    .line 133
    :goto_0
    if-eqz v3, :cond_3

    .line 134
    iget v1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->l:F

    .line 135
    iget v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->k:F

    move v2, v1

    move v1, v0

    .line 140
    :goto_1
    cmpg-float v0, v2, v5

    if-gtz v0, :cond_4

    cmpg-float v0, v1, v4

    if-gtz v0, :cond_4

    .line 141
    const/high16 v0, 0x3f800000    # 1.0f

    .line 145
    :goto_2
    iget-object v6, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    invoke-virtual {v6, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 146
    if-eqz v3, :cond_0

    .line 147
    iget-object v6, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    neg-float v7, v1

    mul-float/2addr v7, v0

    mul-float/2addr v7, v9

    neg-float v8, v2

    mul-float/2addr v8, v0

    mul-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 149
    iget-object v6, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    iget v7, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->m:I

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 150
    iget-object v6, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    mul-float v7, v2, v0

    mul-float/2addr v7, v9

    mul-float v8, v1, v0

    mul-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 153
    :cond_0
    iget-object v6, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    mul-float/2addr v2, v0

    sub-float v2, v5, v2

    mul-float/2addr v2, v9

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    mul-float/2addr v0, v9

    invoke-virtual {v6, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 155
    if-nez v3, :cond_1

    iget v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->m:I

    if-lez v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->m:I

    int-to-float v1, v1

    div-float v2, v5, v10

    div-float v3, v4, v10

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->n:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 160
    return-void

    .line 132
    :cond_2
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    .line 137
    :cond_3
    iget v1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->k:F

    .line 138
    iget v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->l:F

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 143
    :cond_4
    div-float v0, v5, v2

    div-float v6, v4, v1

    invoke-static {v0, v6}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_2
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->h:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/mtp/MtpDevice;Lisj;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 112
    invoke-virtual {p2}, Lisj;->a()I

    move-result v0

    const/high16 v1, 0x800000

    if-gt v0, v1, :cond_0

    sget-object v0, Liso;->a:Ljava/util/Set;

    .line 113
    invoke-virtual {p2}, Lisj;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-static {p1, p2}, Lisk;->b(Landroid/mtp/MtpDevice;Lisj;)Lisf;

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lisf;

    invoke-static {p1, p2}, Lisk;->a(Landroid/mtp/MtpDevice;Lisj;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lisf;-><init>(Landroid/graphics/Bitmap;I)V

    goto :goto_0
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 220
    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 221
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->g:Landroid/mtp/MtpDevice;

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->f:Lisj;

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->h:Ljava/lang/Object;

    .line 224
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 226
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setImageResource(I)V

    .line 227
    return-void

    .line 224
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Landroid/mtp/MtpDevice;Lisj;I)V
    .locals 5

    .prologue
    .line 85
    invoke-virtual {p2}, Lisj;->d()I

    move-result v0

    .line 86
    iget v1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a:I

    if-ne v0, v1, :cond_0

    iget v1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->b:I

    if-ne p3, v1, :cond_0

    .line 108
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a()V

    .line 90
    const v1, 0x106000d

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setImageResource(I)V

    .line 91
    iput p3, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->b:I

    .line 92
    iput v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a:I

    .line 99
    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    :try_start_0
    iput-object p2, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->f:Lisj;

    .line 101
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->g:Landroid/mtp/MtpDevice;

    .line 102
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->e:Z

    if-eqz v0, :cond_1

    .line 103
    monitor-exit v1

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 105
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->e:Z

    .line 106
    sget-object v0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->i:Lisw;

    sget-object v2, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->i:Lisw;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c:Ljava/lang/ref/WeakReference;

    .line 107
    invoke-virtual {v2, v3, v4}, Lisw;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 106
    invoke-virtual {v0, v2}, Lisw;->sendMessage(Landroid/os/Message;)Z

    .line 108
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 205
    check-cast p1, Lisf;

    .line 206
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v0, v1, :cond_0

    .line 207
    iget-object v0, p1, Lisf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->l:F

    .line 208
    iget-object v0, p1, Lisf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->k:F

    .line 209
    iget v0, p1, Lisf;->b:I

    iput v0, p0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->m:I

    .line 210
    invoke-direct {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c()V

    .line 214
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setAlpha(F)V

    .line 215
    iget-object v0, p1, Lisf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 217
    return-void

    .line 212
    :cond_0
    iget v0, p1, Lisf;->b:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->setRotation(F)V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a()V

    .line 232
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 233
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 198
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 199
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 187
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 188
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v0, v1, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c()V

    .line 191
    :cond_0
    return-void
.end method
