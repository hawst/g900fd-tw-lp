.class public final Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhie;


# static fields
.field private static final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static j:Z


# instance fields
.field private final k:Lhee;

.field private l:Lhzl;

.field private m:Lhin;

.field private n:Lllg;

.field private o:Lhiv;

.field private p:Lhhx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lhhk;

    invoke-direct {v0}, Lhhk;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->e:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Lhhl;

    invoke-direct {v0}, Lhhl;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->f:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Lhhm;

    invoke-direct {v0}, Lhhm;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->g:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Lhhn;

    invoke-direct {v0}, Lhhn;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->h:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Lhho;

    invoke-direct {v0}, Lhho;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->i:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Lhhp;

    invoke-direct {v0}, Lhhp;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lloa;-><init>()V

    .line 137
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    .line 138
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->k:Lhee;

    .line 249
    new-instance v0, Lhzj;

    iget-object v1, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->y:Llqc;

    invoke-direct {v0, v1}, Lhzj;-><init>(Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->l:Lhzl;

    .line 250
    new-instance v0, Lhin;

    invoke-direct {v0}, Lhin;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->m:Lhin;

    .line 251
    new-instance v0, Lllg;

    invoke-direct {v0}, Lllg;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->n:Lllg;

    .line 252
    new-instance v0, Lhiv;

    invoke-direct {v0}, Lhiv;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->o:Lhiv;

    .line 253
    new-instance v0, Lhhx;

    iget-object v1, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhhx;-><init>(Los;Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->p:Lhhx;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;)Lhiv;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->o:Lhiv;

    return-object v0
.end method

.method public static a(Lhgw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Lhgw;->i()I

    move-result v0

    if-lez v0, :cond_0

    .line 241
    const-string v0, "Squares"

    .line 245
    :goto_0
    return-object v0

    .line 242
    :cond_0
    invoke-virtual {p0}, Lhgw;->j()I

    move-result v0

    if-lez v0, :cond_1

    .line 243
    const-string v0, "Clx"

    goto :goto_0

    .line 245
    :cond_1
    const-string v0, "CirclesAndPeople"

    goto :goto_0
.end method

.method public static synthetic k()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic l()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic m()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic n()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->e:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 387
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 388
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 389
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->c(Landroid/content/Intent;)V

    .line 390
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->f()Lae;

    move-result-object v1

    const-string v2, "ACL_PICKER_FRAGMENT_TAG"

    .line 391
    invoke-virtual {v1, v2}, Lae;->a(Ljava/lang/String;)Lu;

    .line 392
    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 394
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->finish()V

    .line 395
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 258
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-string v1, "acl.PlusAclPickerActivity.RETURN_AUDIENCE_DATA"

    invoke-virtual {v0, v1, v4}, Llnh;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->j:Z

    .line 262
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->p:Lhhx;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "acl.PlusAclPickerActivity.ALLOW_EMPTY_AUDIENCE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhhx;->b(Z)V

    .line 263
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Lhhx;

    iget-object v2, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->p:Lhhx;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Lllg;

    iget-object v2, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->n:Lllg;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 265
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->n:Lllg;

    invoke-virtual {v0, v4}, Lllg;->a(Z)V

    .line 268
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Lhie;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Lhin;

    iget-object v2, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->m:Lhin;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 270
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Lhiv;

    iget-object v2, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->o:Lhiv;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 271
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Lhzl;

    iget-object v2, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->l:Lhzl;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 272
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Ljpr;

    new-instance v2, Ljqy;

    iget-object v3, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Ljqy;-><init>(Landroid/content/Context;Llqr;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 274
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Ljsb;

    invoke-direct {v2}, Ljsb;-><init>()V

    const-string v0, "circle_usage_type"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "circle_usage_type"

    const/4 v3, 0x5

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid CircleUsageType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    sget-object v0, Ljof;->b:Ljqc;

    :goto_0
    iput-object v0, v2, Ljsb;->a:Ljqc;

    :cond_0
    const-string v0, "acl.PlusAclPickerActivity.FILTER_NULL_GAIA_IDS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "acl.PlusAclPickerActivity.FILTER_NULL_GAIA_IDS"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v2, Ljsb;->b:Z

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Ljsb;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 280
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljqe;

    invoke-direct {v1}, Ljqe;-><init>()V

    const-string v2, "acl.PlusAclPickerActivity.INCLUDE_PLUS_PAGES"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v1, v0}, Ljqe;->a(Z)Ljqe;

    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v2, Ljqe;

    invoke-virtual {v0, v2, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 282
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v1, Lhid;

    new-instance v2, Lhhq;

    invoke-direct {v2, p0}, Lhhq;-><init>(Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 304
    return-void

    .line 274
    :pswitch_2
    sget-object v0, Ljof;->d:Ljqc;

    goto :goto_0

    :pswitch_3
    sget-object v0, Ljof;->c:Ljqc;

    goto :goto_0

    :pswitch_4
    sget-object v0, Ljof;->e:Ljqc;

    goto :goto_0

    :pswitch_5
    sget-object v0, Ljof;->h:Ljqc;

    goto :goto_0

    :pswitch_6
    sget-object v0, Ljof;->f:Ljqc;

    goto :goto_0

    :pswitch_7
    sget-object v0, Ljof;->g:Ljqc;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method c(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 398
    sget-boolean v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->j:Z

    if-eqz v0, :cond_0

    .line 399
    const-string v0, "extra_acl"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->l:Lhzl;

    .line 400
    invoke-interface {v2}, Lhzl;->c()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Lhgw;->a(Ljava/util/List;)Lhgw;

    move-result-object v1

    .line 399
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 401
    const-string v0, "restrict_to_domain"

    iget-object v1, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->m:Lhin;

    invoke-virtual {v1}, Lhin;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 407
    :goto_0
    return-void

    .line 403
    :cond_0
    const-string v0, "acl.PlusAclPickerActivity.SELECTION"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->l:Lhzl;

    .line 404
    invoke-interface {v2}, Lhzl;->c()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 405
    const-string v0, "acl.PlusAclPickerActivity.RESTRICT_TO_DOMAIN"

    iget-object v1, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->m:Lhin;

    invoke-virtual {v1}, Lhin;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->o:Lhiv;

    invoke-virtual {v0}, Lhiv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->o:Lhiv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhiv;->a(Z)V

    .line 379
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->p:Lhhx;

    invoke-virtual {v0}, Lhhx;->b()V

    .line 383
    :goto_0
    return-void

    .line 382
    :cond_0
    invoke-super {p0}, Lloa;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 356
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 357
    if-nez p1, :cond_3

    .line 358
    new-instance v2, Lhif;

    invoke-direct {v2}, Lhif;-><init>()V

    .line 359
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    new-instance v3, Lhig;

    invoke-direct {v3}, Lhig;-><init>()V

    const-string v0, "acl.PlusAclPickerActivity.SHAREOUSEL_ORDER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "acl.PlusAclPickerActivity.SHAREOUSEL_ORDER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v3, v0}, Lhig;->a(Ljava/util/ArrayList;)Lhig;

    :goto_0
    const-string v0, "acl.PlusAclPickerActivity.RESTRICT_TO_DOMAIN"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "acl.PlusAclPickerActivity.RESTRICT_TO_DOMAIN"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v3, v0}, Lhig;->a(Z)Lhig;

    :cond_0
    const-string v0, "acl.PlusAclPickerActivity.DISABLE_DOMAIN_RESTRICTION_TOGGLE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "acl.PlusAclPickerActivity.DISABLE_DOMAIN_RESTRICTION_TOGGLE"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v3, v0}, Lhig;->b(Z)Lhig;

    :cond_1
    const/4 v0, 0x0

    const-string v4, "acl.PlusAclPickerActivity.SELECTION"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v0, "acl.PlusAclPickerActivity.SELECTION"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    :cond_2
    const-string v4, "acl.PlusAclPickerActivity.SELECTION_SLIDE"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "acl.PlusAclPickerActivity.SELECTION_SLIDE"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "acl.PlusAclPickerActivity.SELECTION_SLIDE"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, v0, v1}, Lhig;->a(Ljava/util/ArrayList;Ljava/lang/String;)Lhig;

    invoke-virtual {v3}, Lhig;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v0}, Lhif;->f(Landroid/os/Bundle;)V

    .line 361
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->f()Lae;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    const v1, 0x7f100143

    const-string v3, "ACL_PICKER_FRAGMENT_TAG"

    .line 363
    invoke-virtual {v0, v1, v2, v3}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    move-result-object v0

    .line 364
    invoke-virtual {v0}, Lat;->b()I

    .line 366
    :cond_3
    const v0, 0x7f04001e

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->setContentView(I)V

    .line 367
    return-void

    .line 359
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->x:Llnh;

    const-class v4, Lieh;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v4, Lhxo;->a:Lief;

    iget-object v5, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->k:Lhee;

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    invoke-interface {v0, v4, v5}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->f:Ljava/util/ArrayList;

    :goto_2
    invoke-virtual {v3, v0}, Lhig;->a(Ljava/util/ArrayList;)Lhig;

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->e:Ljava/util/ArrayList;

    goto :goto_2

    :cond_6
    const-string v1, ""

    goto :goto_1
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 371
    invoke-super {p0, p1}, Lloa;->onPostCreate(Landroid/os/Bundle;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->n:Lllg;

    iget-object v1, p0, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->p:Lhhx;

    invoke-virtual {v1}, Lhhx;->a()Landroid/support/v7/widget/SearchView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lllg;->a(Landroid/support/v7/widget/SearchView;)V

    .line 373
    return-void
.end method
