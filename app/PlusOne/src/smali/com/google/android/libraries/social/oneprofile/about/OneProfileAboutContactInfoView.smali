.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;
.super Ljnl;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/view/View;

.field private C:Landroid/view/LayoutInflater;

.field private a:Ljms;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/view/ViewGroup;

.field private u:Landroid/view/View;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/view/View;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/view/ViewGroup;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->C:Landroid/view/LayoutInflater;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->C:Landroid/view/LayoutInflater;

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->C:Landroid/view/LayoutInflater;

    .line 77
    return-void
.end method

.method private a(ILandroid/view/ViewGroup;)Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->C:Landroid/view/LayoutInflater;

    const v1, 0x7f040132

    .line 130
    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;

    .line 131
    const v1, 0x7f1001e9

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 132
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a:Ljms;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->a(ILjms;)V

    .line 133
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 134
    return-object v0
.end method

.method private a(Lnif;ILandroid/view/ViewGroup;)V
    .locals 10

    .prologue
    const v2, 0x7f0a0509

    const v5, 0x7f0a0507

    const v4, 0x7f0a04f8

    const v3, 0x7f0a04f5

    const/4 v7, 0x0

    .line 168
    .line 169
    const/4 v0, 0x0

    .line 171
    new-instance v9, Ljmr;

    invoke-direct {v9, p1, p2}, Ljmr;-><init>(Lnif;I)V

    move-object v1, v0

    move v6, v7

    .line 172
    :goto_0
    invoke-virtual {v9}, Ljmr;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 173
    invoke-virtual {v9}, Ljmr;->next()Ljava/lang/Object;

    move-result-object v0

    .line 174
    instance-of v8, v0, Lnka;

    if-eqz v8, :cond_1

    .line 175
    check-cast v0, Lnka;

    .line 177
    const/16 v1, 0x3e8

    invoke-direct {p0, v1, p3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(ILandroid/view/ViewGroup;)Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;

    move-result-object v8

    .line 179
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 180
    iget v1, v0, Lnka;->e:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v6

    .line 217
    :goto_1
    iget-object v0, v0, Lnka;->d:Ljava/lang/String;

    invoke-virtual {v8, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->b(Ljava/lang/String;)V

    move-object v0, v8

    .line 241
    :goto_2
    if-eq v1, v6, :cond_5

    .line 242
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->a(I)V

    move v6, v1

    move-object v1, v0

    .line 243
    goto :goto_0

    :pswitch_1
    move v1, v2

    .line 183
    goto :goto_1

    :pswitch_2
    move v1, v3

    .line 186
    goto :goto_1

    :pswitch_3
    move v1, v4

    .line 189
    goto :goto_1

    :pswitch_4
    move v1, v5

    .line 192
    goto :goto_1

    .line 198
    :cond_0
    iget v1, v0, Lnka;->e:I

    sparse-switch v1, :sswitch_data_0

    move v1, v6

    .line 212
    goto :goto_1

    :sswitch_0
    move v1, v2

    .line 201
    goto :goto_1

    :sswitch_1
    move v1, v3

    .line 204
    goto :goto_1

    :sswitch_2
    move v1, v4

    .line 207
    goto :goto_1

    :sswitch_3
    move v1, v5

    .line 210
    goto :goto_1

    .line 219
    :cond_1
    instance-of v8, v0, Lnjy;

    if-eqz v8, :cond_2

    .line 220
    check-cast v0, Lnjy;

    .line 221
    const/16 v1, 0x3e9

    invoke-direct {p0, v1, p3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(ILandroid/view/ViewGroup;)Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;

    move-result-object v1

    .line 222
    const v8, 0x7f0a0508

    .line 223
    iget-object v0, v0, Lnjy;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->b(Ljava/lang/String;)V

    move-object v0, v1

    move v1, v8

    .line 225
    goto :goto_2

    :cond_2
    instance-of v8, v0, Lnjz;

    if-eqz v8, :cond_3

    .line 226
    check-cast v0, Lnjz;

    .line 227
    invoke-direct {p0, v7, p3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(ILandroid/view/ViewGroup;)Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;

    move-result-object v8

    .line 228
    iget v1, v0, Lnjz;->e:I

    packed-switch v1, :pswitch_data_1

    const v1, 0x7f0a050f

    .line 229
    :goto_3
    iget-object v0, v0, Lnjz;->d:Ljava/lang/String;

    invoke-virtual {v8, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->b(Ljava/lang/String;)V

    move-object v0, v8

    .line 231
    goto :goto_2

    .line 228
    :pswitch_5
    const v1, 0x7f0a0510

    goto :goto_3

    :pswitch_6
    const v1, 0x7f0a0511

    goto :goto_3

    :pswitch_7
    const v1, 0x7f0a0512

    goto :goto_3

    :pswitch_8
    const v1, 0x7f0a0513

    goto :goto_3

    :pswitch_9
    const v1, 0x7f0a0514

    goto :goto_3

    :pswitch_a
    const v1, 0x7f0a0515

    goto :goto_3

    :pswitch_b
    const v1, 0x7f0a0516

    goto :goto_3

    :pswitch_c
    const v1, 0x7f0a0517

    goto :goto_3

    :pswitch_d
    const v1, 0x7f0a0518

    goto :goto_3

    .line 231
    :cond_3
    instance-of v8, v0, Lnjx;

    if-eqz v8, :cond_4

    .line 232
    check-cast v0, Lnjx;

    .line 233
    const/16 v1, 0x3ea

    invoke-direct {p0, v1, p3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(ILandroid/view/ViewGroup;)Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;

    move-result-object v1

    .line 234
    const v8, 0x7f0a050c

    .line 235
    iget-object v0, v0, Lnjx;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->b(Ljava/lang/String;)V

    move-object v0, v1

    move v1, v8

    .line 237
    goto/16 :goto_2

    :cond_4
    move-object v0, v1

    move v1, v6

    .line 238
    goto/16 :goto_2

    .line 245
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->b()V

    move-object v1, v0

    .line 247
    goto/16 :goto_0

    .line 248
    :cond_6
    return-void

    .line 180
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 198
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x6 -> :sswitch_2
        0x12 -> :sswitch_1
        0x13 -> :sswitch_3
    .end sparse-switch

    .line 228
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_5
        :pswitch_9
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
    .end packed-switch
.end method

.method public static b(Lnjt;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 545
    if-eqz p0, :cond_4

    .line 546
    iget-object v1, p0, Lnjt;->d:Lnib;

    .line 547
    if-eqz v1, :cond_4

    .line 548
    iget-object v1, v1, Lnib;->c:Lnif;

    .line 549
    if-eqz v1, :cond_4

    .line 550
    iget-object v2, v1, Lnif;->c:[Lnjx;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lnif;->c:[Lnjx;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 565
    :cond_0
    :goto_0
    return v0

    .line 553
    :cond_1
    iget-object v2, v1, Lnif;->b:[Lnjy;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lnif;->b:[Lnjy;

    array-length v2, v2

    if-gtz v2, :cond_0

    .line 556
    :cond_2
    iget-object v2, v1, Lnif;->d:[Lnjz;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lnif;->d:[Lnjz;

    array-length v2, v2

    if-gtz v2, :cond_0

    .line 559
    :cond_3
    iget-object v2, v1, Lnif;->a:[Lnka;

    if-eqz v2, :cond_4

    iget-object v1, v1, Lnif;->a:[Lnka;

    array-length v1, v1

    if-gtz v1, :cond_0

    .line 565
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v2, v1

    .line 107
    :goto_0
    if-ge v2, v3, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    .line 109
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;

    .line 110
    invoke-virtual {v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->a()V

    .line 107
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 116
    :goto_1
    if-ge v1, v2, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    .line 118
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;

    .line 119
    invoke-virtual {v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->a()V

    .line 116
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->u:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->u:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->o:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->o:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    return-void
.end method

.method public a(Ljms;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a:Ljms;

    .line 139
    return-void
.end method

.method public a(Lnjt;)V
    .locals 10

    .prologue
    const v9, 0x7f020416

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 252
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 255
    if-eqz p1, :cond_0

    iget-object v0, p1, Lnjt;->d:Lnib;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p1, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(Lnif;ILandroid/view/ViewGroup;)V

    .line 257
    iget-object v0, p1, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(Lnif;ILandroid/view/ViewGroup;)V

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->o:Landroid/view/View;

    sget v4, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->f:I

    sget v5, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->f:I

    sget v6, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->f:I

    sget v7, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->g:I

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 263
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_3

    move v4, v3

    .line 264
    :goto_0
    if-nez v4, :cond_1

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->j:Z

    if-eqz v0, :cond_a

    .line 265
    :cond_1
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->p:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->k:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 266
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->r:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->k:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 267
    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->j:Z

    if-eqz v0, :cond_7

    .line 268
    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->k:Z

    if-eqz v0, :cond_6

    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->o:Landroid/view/View;

    .line 270
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 269
    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->o:Landroid/view/View;

    new-instance v5, Ljmo;

    invoke-direct {v5, p0}, Ljmo;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->o:Landroid/view/View;

    sget v5, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->f:I

    sget v6, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->f:I

    sget v7, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->f:I

    sget v8, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->f:I

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 282
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->z:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 304
    :goto_3
    if-eqz v4, :cond_8

    .line 305
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 323
    :goto_4
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_b

    .line 324
    :goto_5
    if-nez v3, :cond_2

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->j:Z

    if-eqz v0, :cond_11

    .line 325
    :cond_2
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->u:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->k:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_6
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 326
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->w:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->k:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_7
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 327
    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->j:Z

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->k:Z

    if-nez v0, :cond_e

    .line 328
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->u:Landroid/view/View;

    .line 329
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 328
    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->u:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(Landroid/view/View;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->u:Landroid/view/View;

    new-instance v4, Ljmq;

    invoke-direct {v4, p0}, Ljmq;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 344
    :goto_8
    if-eqz v3, :cond_f

    .line 345
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 361
    :goto_9
    return-void

    :cond_3
    move v4, v2

    .line 263
    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 265
    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 266
    goto/16 :goto_2

    .line 285
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->p:Landroid/view/View;

    .line 286
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 285
    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->p:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(Landroid/view/View;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->p:Landroid/view/View;

    new-instance v5, Ljmp;

    invoke-direct {v5, p0}, Ljmp;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 301
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->z:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 302
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 308
    :cond_8
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->s:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->k:Z

    if-eqz v0, :cond_9

    const v0, 0x7f0a0343

    :goto_a
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    .line 311
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->s:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->e(Landroid/widget/TextView;)V

    .line 313
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_4

    .line 308
    :cond_9
    const v0, 0x7f0a033e

    goto :goto_a

    .line 316
    :cond_a
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_b
    move v3, v2

    .line 323
    goto/16 :goto_5

    :cond_c
    move v0, v2

    .line 325
    goto/16 :goto_6

    :cond_d
    move v0, v2

    .line 326
    goto/16 :goto_7

    .line 342
    :cond_e
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8

    .line 347
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->k:Z

    if-nez v0, :cond_10

    .line 348
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->x:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->e(Landroid/widget/TextView;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_9

    .line 352
    :cond_10
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_9

    .line 356
    :cond_11
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->w:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 358
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_9
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 86
    const v0, 0x7f1003d5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->o:Landroid/view/View;

    .line 87
    const v0, 0x7f1003d7

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->p:Landroid/view/View;

    .line 88
    const v0, 0x7f1003d8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->q:Landroid/widget/TextView;

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(Landroid/widget/TextView;)V

    .line 90
    const v0, 0x7f1003da

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->r:Landroid/view/View;

    .line 91
    const v0, 0x7f1003db

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->s:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f1003dc

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->t:Landroid/view/ViewGroup;

    .line 93
    const v0, 0x7f1003dd

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->u:Landroid/view/View;

    .line 94
    const v0, 0x7f1003de

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->v:Landroid/widget/TextView;

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->v:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(Landroid/widget/TextView;)V

    .line 96
    const v0, 0x7f1003e0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->w:Landroid/view/View;

    .line 97
    const v0, 0x7f1003e1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->x:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f1003e2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->y:Landroid/view/ViewGroup;

    .line 99
    const v0, 0x7f1003d9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->z:Landroid/view/View;

    .line 100
    const v0, 0x7f1003df

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->A:Landroid/view/View;

    .line 101
    const v0, 0x7f1003d6

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->B:Landroid/view/View;

    .line 102
    return-void
.end method
