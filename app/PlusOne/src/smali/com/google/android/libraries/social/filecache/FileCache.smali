.class public Lcom/google/android/libraries/social/filecache/FileCache;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mCacheDir:Ljava/io/File;

.field private final mCachePath:Ljava/lang/String;

.field private mCapacity:J

.field private final mMaxCapacity:J

.field private final mMaxPortionOfFreeStorage:F

.field private final mMaxPortionOfTotalStorage:F

.field private final mMinCapacity:J

.field private final mStatFsProvider:Life;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;JJFF)V
    .locals 11

    .prologue
    .line 42
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    new-instance v10, Liff;

    invoke-direct {v10}, Liff;-><init>()V

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lcom/google/android/libraries/social/filecache/FileCache;-><init>(Landroid/content/Context;Ljava/lang/String;JJFFLife;)V

    .line 45
    return-void

    .line 42
    :cond_0
    new-instance v10, Lifd;

    invoke-direct {v10}, Lifd;-><init>()V

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;JJFFLife;)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCacheDir:Ljava/io/File;

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCachePath:Ljava/lang/String;

    .line 52
    iput-wide p3, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMinCapacity:J

    .line 53
    iput-wide p5, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMaxCapacity:J

    .line 54
    iput p7, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMaxPortionOfTotalStorage:F

    .line 55
    iput p8, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMaxPortionOfFreeStorage:F

    .line 56
    iput-object p9, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mStatFsProvider:Life;

    .line 57
    return-void
.end method

.method public static synthetic access$200()Landroid/os/StatFs;
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcom/google/android/libraries/social/filecache/FileCache;->getStatFs()Landroid/os/StatFs;

    move-result-object v0

    return-object v0
.end method

.method private collectCacheFiles(Ljava/io/File;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 254
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 255
    if-eqz v1, :cond_1

    .line 256
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 257
    aget-object v2, v1, v0

    .line 258
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 259
    invoke-direct {p0, v2, p2}, Lcom/google/android/libraries/social/filecache/FileCache;->collectCacheFiles(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 256
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 261
    :cond_0
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 265
    :cond_1
    return-void
.end method

.method private static createDirIfNotFound(Ljava/lang/String;Ljava/io/FileNotFoundException;)V
    .locals 5

    .prologue
    .line 60
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v1

    .line 65
    const-string v2, "FileCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot create cache directory: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 66
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Cannot create cache directory"

    invoke-direct {v0, v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 69
    :cond_0
    const-string v1, "FileCache"

    const-string v2, "Cannot write file to cache: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static getStatFs()Landroid/os/StatFs;
    .locals 2

    .prologue
    .line 383
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getUsedSpace(Ljava/io/File;)J
    .locals 6

    .prologue
    .line 313
    const-wide/16 v2, 0x0

    .line 314
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 315
    if-eqz v1, :cond_1

    .line 316
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 317
    aget-object v4, v1, v0

    .line 318
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/libraries/social/filecache/FileCache;->getUsedSpace(Ljava/io/File;)J

    move-result-wide v4

    :goto_1
    add-long/2addr v2, v4

    .line 316
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    goto :goto_1

    .line 321
    :cond_1
    return-wide v2
.end method


# virtual methods
.method public computeCapacity()J
    .locals 7

    .prologue
    .line 282
    iget-wide v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMaxCapacity:J

    .line 284
    iget-object v2, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mStatFsProvider:Life;

    invoke-interface {v2}, Life;->a()J

    move-result-wide v2

    .line 285
    long-to-float v4, v0

    long-to-float v5, v2

    iget v6, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMaxPortionOfTotalStorage:F

    mul-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 286
    long-to-float v0, v2

    iget v1, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMaxPortionOfTotalStorage:F

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 289
    :cond_0
    iget-wide v2, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMinCapacity:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 290
    iget-wide v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMinCapacity:J

    .line 293
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mStatFsProvider:Life;

    invoke-interface {v2}, Life;->b()J

    move-result-wide v2

    .line 294
    long-to-float v4, v0

    long-to-float v5, v2

    iget v6, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMaxPortionOfFreeStorage:F

    mul-float/2addr v5, v6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    .line 302
    :goto_0
    return-wide v0

    .line 299
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/filecache/FileCache;->getUsedSpace()J

    move-result-wide v0

    add-long/2addr v0, v2

    .line 302
    long-to-float v0, v0

    iget v1, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mMaxPortionOfFreeStorage:F

    mul-float/2addr v0, v1

    float-to-long v0, v0

    goto :goto_0
.end method

.method public getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCachePath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x3

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 175
    iget-object v1, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCachePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 177
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 178
    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 179
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCacheFiles()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 188
    iget-object v1, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCacheDir:Ljava/io/File;

    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/social/filecache/FileCache;->collectCacheFiles(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 192
    :cond_0
    return-object v0
.end method

.method public getCachedFile(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getCapacity()J
    .locals 4

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCapacity:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 272
    invoke-virtual {p0}, Lcom/google/android/libraries/social/filecache/FileCache;->computeCapacity()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCapacity:J

    .line 274
    :cond_0
    iget-wide v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCapacity:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getUsedSpace()J
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCacheDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/filecache/FileCache;->getUsedSpace(Ljava/io/File;)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public purgeOldFiles()I
    .locals 4

    .prologue
    .line 199
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x1b7740

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/libraries/social/filecache/FileCache;->purgeOldFiles(JJ)I

    move-result v0

    return v0
.end method

.method purgeOldFiles(JJ)I
    .locals 13

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 211
    const/4 v1, 0x0

    .line 250
    :cond_0
    :goto_0
    return v1

    .line 214
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 215
    iget-object v0, p0, Lcom/google/android/libraries/social/filecache/FileCache;->mCacheDir:Ljava/io/File;

    invoke-direct {p0, v0, v2}, Lcom/google/android/libraries/social/filecache/FileCache;->collectCacheFiles(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 216
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    const/4 v1, 0x0

    goto :goto_0

    .line 220
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 221
    const-wide/16 v0, 0x0

    .line 222
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 223
    new-instance v6, Lifc;

    invoke-direct {v6, v0}, Lifc;-><init>(Ljava/io/File;)V

    .line 224
    iget-wide v0, v6, Lifc;->b:J

    sub-long v0, p1, v0

    cmp-long v0, v0, p3

    if-gez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, v6, Lifc;->d:Z

    .line 225
    iget-wide v0, v6, Lifc;->c:J

    add-long/2addr v0, v2

    .line 226
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-wide v2, v0

    .line 227
    goto :goto_1

    .line 224
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 229
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/filecache/FileCache;->getCapacity()J

    move-result-wide v6

    .line 230
    cmp-long v0, v2, v6

    if-gtz v0, :cond_5

    .line 231
    const/4 v1, 0x0

    goto :goto_0

    .line 234
    :cond_5
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 236
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 237
    const/4 v1, 0x0

    .line 238
    const/4 v0, 0x0

    move v4, v0

    :goto_3
    if-ge v4, v8, :cond_0

    .line 239
    cmp-long v0, v2, v6

    if-lez v0, :cond_0

    .line 240
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifc;

    .line 244
    iget-object v9, v0, Lifc;->a:Ljava/io/File;

    .line 245
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 246
    iget-wide v10, v0, Lifc;->c:J

    sub-long/2addr v2, v10

    .line 247
    add-int/lit8 v0, v1, 0x1

    .line 238
    :goto_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_4
.end method

.method public write(Ljava/lang/String;Ljava/nio/ByteBuffer;)V
    .locals 5

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    :try_start_0
    invoke-static {p2, v0}, Llrx;->a(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 93
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v1

    .line 83
    invoke-static {v0, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->createDirIfNotFound(Ljava/lang/String;Ljava/io/FileNotFoundException;)V

    .line 86
    :try_start_1
    invoke-static {p2, v0}, Llrx;->a(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 88
    :catch_1
    move-exception v2

    const-string v2, "FileCache"

    const-string v3, "Cannot write file to cache: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 90
    :catch_2
    move-exception v1

    .line 91
    const-string v2, "FileCache"

    const-string v3, "Cannot write file to cache: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public write(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 74
    invoke-static {p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/filecache/FileCache;->write(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    .line 75
    return-void
.end method
