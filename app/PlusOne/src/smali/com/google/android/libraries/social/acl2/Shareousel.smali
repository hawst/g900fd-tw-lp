.class public final Lcom/google/android/libraries/social/acl2/Shareousel;
.super Landroid/support/v4/view/ViewPager;
.source "PG"

# interfaces
.implements Lhiw;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/Shareousel;->a:Z

    .line 30
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/Shareousel;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhiv;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhiv;

    .line 31
    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0, p0}, Lhiv;->a(Lhiw;)V

    .line 22
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/Shareousel;->a:Z

    .line 30
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/Shareousel;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhiv;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhiv;

    .line 31
    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0, p0}, Lhiv;->a(Lhiw;)V

    .line 26
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 49
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/Shareousel;->a:Z

    .line 50
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/libraries/social/acl2/Shareousel;->a:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/libraries/social/acl2/Shareousel;->a:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
