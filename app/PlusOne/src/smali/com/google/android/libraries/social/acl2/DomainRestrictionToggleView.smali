.class public final Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/CompoundButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 92
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 52
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a052b

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    .line 54
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a052c

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    .line 56
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 71
    :goto_0
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->b(Z)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->b(Z)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    .line 85
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 43
    const v0, 0x7f10015c

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->a:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f10015d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->b:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f10015b

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->c:Landroid/widget/CompoundButton;

    .line 46
    return-void
.end method
