.class public Lcom/google/android/libraries/social/squares/list/SquareListItemView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhmm;
.implements Libe;
.implements Lljh;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lkut;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:I

.field private i:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Lcom/google/android/libraries/social/squares/membership/JoinButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    return-void
.end method

.method private d()V
    .locals 9

    .prologue
    const v1, 0x7f0a0466

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 239
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 242
    iget v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->g:I

    if-ne v0, v8, :cond_3

    move v0, v1

    .line 246
    :goto_0
    iget v4, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->h:I

    if-eqz v4, :cond_0

    .line 247
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v4

    iget v5, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->h:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 248
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f11001d

    iget v7, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->h:I

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v4, v8, v2

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 250
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    if-eqz v0, :cond_0

    .line 253
    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_0
    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    :cond_1
    invoke-static {v3}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    .line 262
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 263
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    :goto_1
    return-void

    .line 265
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->m:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 280
    :goto_0
    return-void

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Libd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 321
    invoke-static {p0, p1}, Llii;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    new-instance v2, Libd;

    const-string v3, "g:"

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v3, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->d:Ljava/lang/String;

    const/16 v4, 0x6f

    invoke-direct {v2, v0, v3, v4}, Libd;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    :cond_0
    return-object v1

    .line 322
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 315
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 226
    iput p1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->g:I

    .line 227
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->d()V

    .line 228
    return-void
.end method

.method public a(Landroid/database/Cursor;Lkut;ZZ)V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v2, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 132
    iput-object p2, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->b:Lkut;

    .line 134
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a:Ljava/lang/String;

    .line 135
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->c:Ljava/lang/String;

    .line 136
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->f:I

    .line 137
    iget v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->f:I

    if-ne v0, v5, :cond_1

    const/16 v0, 0xe

    .line 138
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->d:Ljava/lang/String;

    .line 139
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->e:Ljava/lang/String;

    .line 140
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 143
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 144
    iget-object v3, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Ljac;->a:Ljac;

    invoke-static {v4, v0, v5}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 149
    :goto_1
    if-eqz p3, :cond_3

    const/4 v0, 0x4

    .line 150
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 149
    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(I)V

    .line 152
    if-eqz p4, :cond_4

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_3
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 153
    :goto_4
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->b(I)V

    .line 154
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->e()V

    .line 156
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->n:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    if-eqz v0, :cond_0

    .line 158
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x10

    .line 159
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 161
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x10

    .line 162
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 160
    invoke-static {v0, v1}, Lkvu;->a(II)I

    move-result v0

    .line 164
    :goto_5
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->n:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    iget-object v2, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(Ljava/lang/String;I)V

    .line 167
    :cond_0
    invoke-virtual {p0, p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    return-void

    .line 138
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->k(Z)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 150
    goto :goto_2

    :cond_4
    move v0, v1

    .line 152
    goto :goto_3

    :cond_5
    iget-object v3, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v1, 0x63

    if-le v0, v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0593

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_6
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_7
    move v0, v2

    goto :goto_5
.end method

.method public a(Lkvq;)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->n:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->n:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {p1, v0}, Lkvq;->a(Lkvv;)V

    .line 214
    :cond_0
    return-void
.end method

.method public a(Lnsr;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 171
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lkut;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkut;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->b:Lkut;

    .line 173
    iget-object v0, p1, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a:Ljava/lang/String;

    .line 174
    iget-object v0, p1, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->b:Lnsc;

    iget-object v0, v0, Lnsc;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->c:Ljava/lang/String;

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->d:Ljava/lang/String;

    .line 176
    iget-object v0, p1, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->b:Lnsc;

    iget-object v0, v0, Lnsc;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->e:Ljava/lang/String;

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p1, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->b:Lnsc;

    iget-object v0, v0, Lnsc;->c:Ljava/lang/String;

    .line 180
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 181
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v0, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 186
    :goto_0
    iget-object v0, p1, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->c:Lnsf;

    if-eqz v0, :cond_3

    .line 187
    iget-object v0, p1, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->c:Lnsf;

    iget v0, v0, Lnsf;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(I)V

    .line 191
    :goto_1
    iget-object v0, p1, Lnsr;->d:Lnsv;

    iget-object v0, v0, Lnsv;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p1, Lnsr;->d:Lnsv;

    iget-object v0, v0, Lnsv;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->b(I)V

    .line 194
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->e()V

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->n:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    if-eqz v0, :cond_1

    .line 197
    iget v0, p1, Lnsr;->f:I

    iget-object v1, p1, Lnsr;->b:Lnsb;

    iget v1, v1, Lnsb;->d:I

    invoke-static {v0, v1}, Lkvu;->a(II)I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->n:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    iget-object v2, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(Ljava/lang/String;I)V

    .line 203
    :cond_1
    invoke-virtual {p0, p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    return-void

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->k(Z)V

    goto :goto_0

    .line 189
    :cond_3
    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(I)V

    goto :goto_1
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 330
    new-instance v0, Lkqw;

    sget-object v1, Lomv;->t:Lhmn;

    iget-object v2, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    const v1, 0x7f020197

    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->j:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 110
    const v0, 0x7f1005b3

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->j:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f1005bc

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 112
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->f(I)V

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(I)V

    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->e(Landroid/graphics/drawable/Drawable;)V

    .line 115
    const v0, 0x7f1005bf

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->k:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f1005bd

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->l:Landroid/widget/TextView;

    .line 117
    const v0, 0x7f1005c0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->m:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f1005b9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/squares/membership/JoinButton;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->n:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 120
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 234
    iput p1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->h:I

    .line 235
    invoke-direct {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->d()V

    .line 236
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->f:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->b:Lkut;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->b:Lkut;

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lkut;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :cond_0
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lhly;->a(Landroid/view/View;I)V

    .line 308
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 218
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 219
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->b()V

    .line 220
    return-void
.end method
