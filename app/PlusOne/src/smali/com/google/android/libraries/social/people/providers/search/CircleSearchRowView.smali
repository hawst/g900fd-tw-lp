.class public Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private a:Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

.field private b:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public a(Ljod;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljod;",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 44
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;->a:Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    invoke-interface {p1}, Ljod;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a(I)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    .line 50
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;->b:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    invoke-interface {p1}, Ljod;->b()Ljava/lang/String;

    move-result-object v1

    move-object v4, v3

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Ljava/lang/String;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    .line 53
    return-void

    .line 46
    :cond_2
    invoke-interface {p1}, Ljod;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;->a:Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a(Ljava/util/List;)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 39
    const v0, 0x7f1001f7

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;->a:Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    .line 40
    const v0, 0x7f1001f8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;->b:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    .line 41
    return-void
.end method
