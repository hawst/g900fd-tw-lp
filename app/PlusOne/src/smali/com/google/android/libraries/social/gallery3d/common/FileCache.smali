.class public final Lcom/google/android/libraries/social/gallery3d/common/FileCache;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:Ljava/lang/String;


# instance fields
.field private final e:Lifr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lifr",
            "<",
            "Ljava/lang/String;",
            "Lifo;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/io/File;

.field private g:J

.field private h:Z

.field private i:J

.field private j:Lifp;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 38
    sget-object v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->a:Lifm;

    invoke-virtual {v0}, Lifm;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a:Ljava/lang/String;

    .line 45
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "sum(%s)"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "size"

    aput-object v3, v2, v4

    .line 46
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->b:[Ljava/lang/String;

    .line 47
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "filename"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string v2, "content_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "size"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->c:[Ljava/lang/String;

    .line 50
    const-string v0, "%s ASC"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "last_access"

    aput-object v2, v1, v4

    .line 51
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->d:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lifr;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lifr;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->e:Lifr;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->h:Z

    .line 91
    invoke-static {p2}, Llsk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    .line 92
    iput-wide p4, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->g:J

    .line 93
    new-instance v0, Lifp;

    invoke-direct {v0, p0, p1, p3}, Lifp;-><init>(Lcom/google/android/libraries/social/gallery3d/common/FileCache;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    .line 94
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/gallery3d/common/FileCache;)Ljava/io/File;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    return-object v0
.end method

.method private a(I)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 236
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    invoke-virtual {v0}, Lifp;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->c:[Ljava/lang/String;

    sget-object v7, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->d:Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 240
    :goto_0
    if-lez p1, :cond_3

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J

    iget-wide v4, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->g:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 241
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 242
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 243
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 244
    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 245
    const/4 v5, 0x3

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 247
    iget-object v5, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->e:Lifr;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 249
    :try_start_1
    iget-object v8, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->e:Lifr;

    invoke-virtual {v8, v4}, Lifr;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    monitor-exit v5

    goto :goto_0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 262
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 250
    :cond_0
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 252
    add-int/lit8 p1, p1, -0x1

    .line 253
    :try_start_4
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    invoke-direct {v4, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 254
    iget-wide v4, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J

    .line 255
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    invoke-virtual {v0}, Lifp;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a:Ljava/lang/String;

    const-string v5, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 256
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    .line 255
    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 258
    :cond_1
    const-string v2, "unable to delete file: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 262
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 263
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 200
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 201
    const-string v1, "last_access"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 202
    iget-object v1, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    invoke-virtual {v1}, Lifp;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a:Ljava/lang/String;

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 203
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 202
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 204
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 77
    :try_start_0
    invoke-virtual {p0, p2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 78
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 79
    if-nez v0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 81
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 82
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "download"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, ".tmp"

    .line 83
    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 86
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/io/File;)Z
    .locals 3

    .prologue
    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    .line 135
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 140
    :goto_1
    return v0

    .line 136
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 183
    invoke-static {p1}, Lifu;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 184
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    const/4 v0, 0x1

    aput-object p1, v4, v0

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    invoke-virtual {v0}, Lifp;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->a:Lifm;

    .line 186
    invoke-virtual {v2}, Lifm;->b()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "hash_code=? AND content_url=?"

    move-object v6, v5

    move-object v7, v5

    .line 185
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 189
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 195
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v5

    .line 190
    :cond_0
    :try_start_1
    new-instance v5, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;

    invoke-direct {v5}, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;-><init>()V

    .line 191
    sget-object v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->a:Lifm;

    invoke-virtual {v0, v1, v5}, Lifm;->a(Landroid/database/Cursor;Lifj;)Lifj;

    .line 192
    iget-wide v2, v5, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->id:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 8

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 233
    :goto_0
    monitor-exit p0

    return-void

    .line 213
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    .line 214
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 215
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    .line 216
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "cannot create: "

    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 216
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 220
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    invoke-virtual {v0}, Lifp;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 224
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 226
    :cond_3
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 228
    iget-wide v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J

    iget-wide v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(I)V

    .line 232
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->h:Z

    goto :goto_0

    .line 226
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lifo;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 145
    iget-boolean v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->h:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->b()V

    .line 147
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->e:Lifr;

    monitor-enter v2

    .line 148
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->e:Lifr;

    invoke-virtual {v0, p1}, Lifr;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifo;

    .line 149
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    if-eqz v0, :cond_1

    .line 152
    monitor-enter p0

    .line 153
    :try_start_1
    iget-wide v2, v0, Lifo;->a:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(J)V

    .line 154
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 178
    :goto_0
    return-object v0

    .line 149
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 154
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 158
    :cond_1
    monitor-enter p0

    .line 159
    :try_start_4
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;

    move-result-object v2

    .line 160
    if-nez v2, :cond_2

    .line 161
    monitor-exit p0

    move-object v0, v1

    goto :goto_0

    .line 163
    :cond_2
    new-instance v0, Lifo;

    iget-wide v4, v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->id:J

    new-instance v3, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    iget-object v7, v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->filename:Ljava/lang/String;

    invoke-direct {v3, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v4, v5, v3}, Lifo;-><init>(JLjava/io/File;)V

    .line 164
    iget-object v3, v0, Lifo;->b:Ljava/io/File;

    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(Ljava/io/File;)Z

    move-result v3

    invoke-static {v3}, Lifu;->a(Z)V

    .line 165
    iget-object v3, v0, Lifo;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v3

    if-nez v3, :cond_4

    .line 167
    :try_start_5
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    invoke-virtual {v0}, Lifp;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a:Ljava/lang/String;

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-wide v8, v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->id:J

    .line 168
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 167
    invoke-virtual {v0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 169
    iget-wide v4, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J

    iget-wide v6, v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->size:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 173
    :goto_1
    :try_start_6
    monitor-exit p0

    move-object v0, v1

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    const-string v0, "cannot delete entry: "

    iget-object v2, v2, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->filename:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 179
    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 171
    :cond_3
    :try_start_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 175
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->e:Lifr;

    monitor-enter v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 176
    :try_start_8
    iget-object v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->e:Lifr;

    invoke-virtual {v2, p1, v0}, Lifr;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 178
    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_0

    .line 177
    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2
.end method

.method public a()Ljava/io/File;
    .locals 3

    .prologue
    .line 207
    const-string v0, "download"

    const-string v1, ".tmp"

    iget-object v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->f:Ljava/io/File;

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/io/File;)V
    .locals 6

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->h:Z

    if-nez v0, :cond_0

    .line 103
    invoke-direct {p0}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->b()V

    .line 106
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(Ljava/io/File;)Z

    move-result v0

    invoke-static {v0}, Lifu;->a(Z)V

    .line 107
    new-instance v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;

    invoke-direct {v0}, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;-><init>()V

    .line 108
    invoke-static {p1}, Lifu;->a(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->hashCode:J

    .line 109
    iput-object p1, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->contentUrl:Ljava/lang/String;

    .line 110
    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->filename:Ljava/lang/String;

    .line 111
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->size:J

    .line 112
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->lastAccess:J

    .line 113
    iget-wide v2, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->size:J

    iget-wide v4, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->g:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 114
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    .line 115
    new-instance v1, Ljava/lang/IllegalArgumentException;

    iget-wide v2, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->size:J

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v4, 0x24

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "file too large: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 117
    :cond_1
    monitor-enter p0

    .line 118
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;

    move-result-object v1

    .line 119
    if-eqz v1, :cond_3

    .line 120
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    .line 121
    iget-object v2, v1, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->filename:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->filename:Ljava/lang/String;

    .line 122
    iget-wide v2, v1, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->size:J

    iput-wide v2, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->size:J

    .line 126
    :goto_0
    sget-object v1, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->a:Lifm;

    iget-object v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    .line 127
    invoke-virtual {v2}, Lifp;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 126
    invoke-virtual {v1, v2, v0}, Lifm;->a(Landroid/database/sqlite/SQLiteDatabase;Lifj;)J

    .line 128
    iget-wide v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J

    iget-wide v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(I)V

    .line 129
    :cond_2
    monitor-exit p0

    return-void

    .line 124
    :cond_3
    iget-wide v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J

    iget-wide v4, v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->size:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->i:J

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->j:Lifp;

    invoke-virtual {v0}, Lifp;->close()V

    .line 99
    return-void
.end method
