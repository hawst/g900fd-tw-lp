.class public Lcom/google/android/libraries/social/ui/tabbar/TabBar;
.super Landroid/widget/HorizontalScrollView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# static fields
.field private static a:I


# instance fields
.field private b:Llhl;

.field private c:Landroid/widget/LinearLayout;

.field private d:I

.field private e:I

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llhm;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->f:Ljava/util/ArrayList;

    .line 64
    sget v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a:I

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 66
    const v1, 0x7f0d00f6

    .line 67
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a:I

    .line 53
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->f:Ljava/util/ArrayList;

    .line 64
    sget v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a:I

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 66
    const v1, 0x7f0d00f6

    .line 67
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a:I

    .line 57
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->f:Ljava/util/ArrayList;

    .line 64
    sget v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a:I

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 66
    const v1, 0x7f0d00f6

    .line 67
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a:I

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 75
    iput-object v1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->b:Llhl;

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->e:I

    iput v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->d:I

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->b(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V

    .line 79
    iput-object v1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    .line 81
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 149
    :goto_0
    if-ge v1, v3, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhm;

    .line 151
    iget v4, v0, Llhm;->a:I

    if-ne v4, p1, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v0, Llhm;->b:Landroid/widget/TextView;

    const/16 v6, 0x2b

    invoke-static {v4, v5, v6}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v4, v0, Llhm;->b:Landroid/widget/TextView;

    iget-object v5, v0, Llhm;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, v0, Llhm;->b:Landroid/widget/TextView;

    iget v4, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->d:I

    iget v5, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->e:I

    invoke-virtual {v0, v4, v2, v5, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 149
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v0, Llhm;->b:Landroid/widget/TextView;

    const/16 v6, 0x2a

    invoke-static {v4, v5, v6}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v4, v0, Llhm;->b:Landroid/widget/TextView;

    iget-object v5, v0, Llhm;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, v0, Llhm;->b:Landroid/widget/TextView;

    iget v4, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->d:I

    iget v5, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->e:I

    invoke-virtual {v0, v4, v2, v5, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 157
    :cond_1
    return-void
.end method

.method public a(ILjava/lang/String;I)V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;ILhmn;)V

    .line 114
    return-void
.end method

.method public a(ILjava/lang/String;ILhmn;)V
    .locals 4

    .prologue
    .line 117
    new-instance v1, Llhm;

    invoke-direct {v1}, Llhm;-><init>()V

    .line 118
    iput p3, v1, Llhm;->a:I

    .line 119
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, p1, v2}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Llhm;->b:Landroid/widget/TextView;

    .line 120
    iget-object v0, v1, Llhm;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, v1, Llhm;->b:Landroid/widget/TextView;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 123
    if-eqz p4, :cond_1

    .line 124
    iget-object v0, v1, Llhm;->b:Landroid/widget/TextView;

    new-instance v2, Lhmk;

    invoke-direct {v2, p4}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 125
    iget-object v0, v1, Llhm;->b:Landroid/widget/TextView;

    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    :goto_0
    iget v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->d:I

    if-nez v0, :cond_0

    .line 133
    iget-object v0, v1, Llhm;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->d:I

    .line 134
    iget-object v0, v1, Llhm;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->e:I

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 138
    const v2, 0x7f0205b8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v1, Llhm;->c:Landroid/graphics/drawable/Drawable;

    .line 139
    const v2, 0x7f0205b9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Llhm;->d:Landroid/graphics/drawable/Drawable;

    .line 140
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 143
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 144
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->c:Landroid/widget/LinearLayout;

    iget-object v1, v1, Llhm;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    return-void

    .line 127
    :cond_1
    iget-object v0, v1, Llhm;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V
    .locals 2

    .prologue
    .line 182
    iput-object p1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    .line 183
    if-eqz p1, :cond_0

    .line 184
    invoke-virtual {p1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getScrollX()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->scrollTo(II)V

    .line 186
    :cond_0
    return-void
.end method

.method public a(Llhl;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->b:Llhl;

    .line 109
    return-void
.end method

.method public b()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 93
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->setHorizontalScrollBarEnabled(Z)V

    .line 94
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->setHorizontalFadingEdgeEnabled(Z)V

    .line 95
    sget v1, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a:I

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->setFadingEdgeLength(I)V

    .line 96
    const v1, 0x7f0205ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    const v1, 0x7f0d00f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 100
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->c:Landroid/widget/LinearLayout;

    .line 101
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 102
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, v3, v0, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    return-void
.end method

.method public b(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    if-ne v0, p1, :cond_0

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    .line 192
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->b:Llhl;

    if-nez v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 201
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->b:Llhl;

    invoke-interface {v1, v0}, Llhl;->d(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 85
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->b()V

    .line 87
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->scrollTo(II)V

    .line 179
    :cond_0
    return-void
.end method
