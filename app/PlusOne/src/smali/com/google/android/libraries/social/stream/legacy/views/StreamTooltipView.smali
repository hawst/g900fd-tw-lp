.class public Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lhmm;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Landroid/graphics/Rect;

.field private c:Landroid/text/Layout$Alignment;

.field private d:I

.field private e:Landroid/text/StaticLayout;

.field private f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    .line 37
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->c:Landroid/text/Layout$Alignment;

    .line 40
    new-instance v0, Lleg;

    invoke-direct {v0, p0}, Lleg;-><init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->f:Ljava/lang/Runnable;

    .line 57
    sget-object v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    if-nez v0, :cond_0

    .line 58
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->d:I

    .line 64
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->requestLayout()V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->invalidate()V

    .line 66
    return-void
.end method

.method public a(IIII)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->requestLayout()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->invalidate()V

    .line 72
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->f:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b()V

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->f:Ljava/lang/Runnable;

    invoke-static {v0, p1, p2}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 178
    return-void
.end method

.method public a(Landroid/text/Layout$Alignment;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->c:Landroid/text/Layout$Alignment;

    .line 76
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 182
    new-instance v0, Lhmk;

    sget-object v1, Lonm;->l:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 128
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setVisibility(I)V

    .line 129
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setClickable(Z)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->invalidate()V

    .line 131
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 133
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setAlpha(F)V

    .line 134
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 138
    :cond_0
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setClickable(Z)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->invalidate()V

    .line 143
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 145
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 148
    :cond_0
    new-instance v1, Lleh;

    invoke-direct {v1, p0}, Lleh;-><init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->getWidth()I

    move-result v0

    .line 108
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->getHeight()I

    move-result v1

    .line 109
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v2, v0, v2

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    .line 110
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int v3, v1, v3

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    .line 112
    sget-object v4, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    iget-object v4, v4, Llct;->H:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v7

    iget-object v7, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v7

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 114
    sget-object v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    iget-object v0, v0, Llct;->ak:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    iget-object v1, v1, Llct;->H:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 115
    sget-object v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    iget-object v0, v0, Llct;->ak:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->e:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    .line 119
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    .line 121
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 122
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 123
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 125
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 81
    const/16 v1, 0x25

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    .line 84
    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->d:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v3

    sget-object v3, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    iget v3, v3, Llct;->m:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 87
    invoke-static {v1, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v0

    .line 88
    if-gtz v3, :cond_0

    .line 93
    :goto_0
    const v3, 0x7fffffff

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->c:Landroid/text/Layout$Alignment;

    invoke-static {v1, v2, v0, v3, v4}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->e:Landroid/text/StaticLayout;

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 98
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->e:Landroid/text/StaticLayout;

    .line 99
    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a:Llct;

    iget v3, v3, Llct;->aA:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 100
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setMeasuredDimension(II)V

    .line 101
    return-void

    .line 91
    :cond_0
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method
