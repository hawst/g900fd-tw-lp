.class public Lcom/google/android/apps/moviemaker/MovieMakerActivity;
.super Lahk;
.source "PG"

# interfaces
.implements Lapg;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private final f:Lchh;

.field private final g:Lhoc;

.field private h:Lasy;

.field private i:Lanh;

.field private j:Lcdu;

.field private k:Lavj;

.field private l:Lbjf;

.field private m:Lbqo;

.field private n:Lbvy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbvy",
            "<",
            "Laii;",
            ">;"
        }
    .end annotation
.end field

.field private o:Laol;

.field private p:Lavo;

.field private q:Lbgf;

.field private r:Lbte;

.field private s:Lcgk;

.field private t:Lcgk;

.field private u:Lahh;

.field private v:Laph;

.field private w:Ljfb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const-class v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 160
    invoke-direct {p0}, Lahk;-><init>()V

    .line 161
    new-instance v0, Lchh;

    invoke-direct {v0}, Lchh;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->f:Lchh;

    .line 162
    new-instance v0, Lhoc;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->O()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Landroid/app/Activity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->g:Lhoc;

    .line 163
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)Lbgf;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)Lbjf;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->l:Lbjf;

    return-object v0
.end method

.method public static b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lu;->n()Lz;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)Lbte;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->r:Lbte;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)Lcgk;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->s:Lcgk;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)Lcgk;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->t:Lcgk;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)Laph;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->v:Laph;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)Lcdu;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    return-object v0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 536
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 537
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v1}, Lavo;->bc()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 538
    const-string v1, "extra_result_message"

    const v2, 0x7f0a0163

    .line 540
    invoke-virtual {p0, v2}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 538
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 542
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->setResult(ILandroid/content/Intent;)V

    .line 543
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->w:Ljfb;

    invoke-virtual {v0}, Ljfb;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->w:Ljfb;

    invoke-virtual {v0, p1}, Ljfb;->a(I)Landroid/content/Intent;

    move-result-object v0

    .line 564
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->startActivity(Landroid/content/Intent;)V

    .line 565
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->finish()V

    .line 567
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 3

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->h()Loo;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f0205e3

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Loo;->b(Landroid/graphics/drawable/Drawable;)V

    .line 518
    return-void

    .line 515
    :cond_0
    const v0, 0x7f0205e4

    goto :goto_0
.end method

.method public c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->v:Laph;

    invoke-virtual {v0}, Laph;->j()V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    sget-object v1, Lcdz;->b:Lcdz;

    invoke-virtual {v0, v1}, Lcdu;->a(Lcdz;)V

    .line 550
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 551
    return-void
.end method

.method public d(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 493
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 494
    return-void
.end method

.method public d(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 571
    invoke-virtual {p0, p1}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->startActivity(Landroid/content/Intent;)V

    .line 572
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->h:Lasy;

    const-string v1, "loggable"

    invoke-static {v0, v1}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livo;

    new-instance v1, Livp;

    invoke-direct {v1, p3, p1}, Livp;-><init>(Ljava/io/PrintWriter;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Livo;->a(Livm;)V

    .line 523
    invoke-super {p0, p1, p2, p3, p4}, Lahk;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 524
    return-void
.end method

.method protected k()V
    .locals 3

    .prologue
    .line 401
    invoke-super {p0}, Lahk;->k()V

    .line 402
    invoke-static {}, Lapm;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->i:Lanh;

    invoke-virtual {v0}, Lanh;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 405
    sget-object v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    .line 406
    new-instance v0, Lbfo;

    invoke-direct {v0}, Lbfo;-><init>()V

    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "UnsupportedVersionDialogFragment"

    invoke-virtual {v0, v1, v2}, Lbfo;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 421
    :goto_0
    return-void

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->w:Ljfb;

    invoke-virtual {v0}, Ljfb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 413
    sget-object v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    .line 414
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->finish()V

    goto :goto_0

    .line 419
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    invoke-virtual {v0}, Laol;->a()V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->i:Lanh;

    invoke-virtual {v1}, Lanh;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdu;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 498
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->invalidateOptionsMenu()V

    .line 499
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    sget-object v1, Lcdz;->f:Lcdz;

    invoke-virtual {v0, v1}, Lcdu;->a(Lcdz;)V

    .line 556
    new-instance v0, Landroid/content/Intent;

    const-class v1, Laho;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 558
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 625
    const v0, 0x7f0a011d

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 626
    return-void
.end method

.method public o()Laol;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x1

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->v:Laph;

    invoke-virtual {v0}, Laph;->o()V

    .line 578
    const/4 v0, 0x2

    if-ne p1, v0, :cond_6

    if-ne p2, v1, :cond_6

    .line 579
    const/4 v0, 0x0

    .line 581
    invoke-static {}, Lapm;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 582
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljfb;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 586
    :cond_0
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 587
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 588
    if-nez v0, :cond_1

    .line 589
    sget-object v1, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x33

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "got null uri from G+ intent. Not adding new media: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    :cond_1
    :goto_0
    if-eqz v0, :cond_5

    .line 598
    sget-object v1, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GET_CONTENT returned URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v1, v0}, Lavo;->d(Landroid/net/Uri;)V

    .line 616
    :cond_2
    :goto_1
    if-ne p1, v4, :cond_3

    if-nez p2, :cond_3

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v0}, Lavo;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v0}, Lavo;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 618
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->finish()V

    .line 621
    :cond_3
    return-void

    .line 592
    :cond_4
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 593
    if-nez v0, :cond_1

    .line 594
    sget-object v1, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x30

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "got null uri from intent. Not adding new media: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 606
    :cond_5
    const v0, 0x7f0a0112

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 607
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 610
    :cond_6
    if-ne p1, v4, :cond_2

    if-ne p2, v1, :cond_2

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    sget-object v1, Lcdz;->a:Lcdz;

    invoke-virtual {v0, v1}, Lcdu;->a(Lcdz;)V

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v0}, Lavo;->Z()V

    .line 613
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v0, p3}, Lavo;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 528
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q()V

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->v:Laph;

    invoke-virtual {v0}, Laph;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    invoke-super {p0}, Lahk;->onBackPressed()V

    .line 533
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 60

    .prologue
    .line 167
    invoke-super/range {p0 .. p1}, Lahk;->onCreate(Landroid/os/Bundle;)V

    .line 168
    const v4, 0x7f040038

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->setContentView(I)V

    .line 172
    const v4, 0x7f100143

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    new-instance v5, Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->requestTransparentRegion(Landroid/view/View;)V

    .line 175
    invoke-static/range {p0 .. p0}, Lapm;->a(Landroid/content/Context;)Lapm;

    move-result-object v4

    invoke-virtual {v4}, Lapm;->a()Lamm;

    move-result-object v59

    .line 180
    invoke-interface/range {v59 .. v59}, Lamm;->g()Ljava/util/concurrent/Executor;

    move-result-object v4

    .line 181
    invoke-interface/range {v59 .. v59}, Lamm;->o()Laqx;

    move-result-object v5

    .line 178
    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lapr;->a(Landroid/content/Context;Ljava/util/concurrent/Executor;Laqx;)V

    .line 183
    invoke-interface/range {v59 .. v59}, Lamm;->c()Lasy;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->h:Lasy;

    .line 184
    invoke-interface/range {v59 .. v59}, Lamm;->p()Lanh;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->i:Lanh;

    .line 185
    invoke-interface/range {v59 .. v59}, Lamm;->k()Lcdu;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    .line 186
    invoke-interface/range {v59 .. v59}, Lamm;->l()Lavj;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->k:Lavj;

    .line 187
    invoke-interface/range {v59 .. v59}, Lamm;->Q()Lbjf;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->l:Lbjf;

    .line 188
    invoke-interface/range {v59 .. v59}, Lamm;->u()Lbqo;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->m:Lbqo;

    .line 189
    invoke-interface/range {v59 .. v59}, Lamm;->L()Ljfb;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->w:Ljfb;

    .line 191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->f:Lchh;

    invoke-virtual {v4}, Lchh;->a()V

    .line 192
    new-instance v31, Lchm;

    .line 193
    invoke-interface/range {v59 .. v59}, Lamm;->e()Lchk;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->f:Lchh;

    move-object/from16 v0, v31

    invoke-direct {v0, v4, v5}, Lchm;-><init>(Lchk;Lchh;)V

    .line 196
    invoke-interface/range {v59 .. v59}, Lamm;->t()Lbgf;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    .line 200
    new-instance v4, Lbtj;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lbtj;-><init>(Landroid/content/Context;Lbgf;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->r:Lbte;

    .line 203
    invoke-interface/range {v59 .. v59}, Lamm;->x()Lbjp;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->s:Lcgk;

    .line 204
    invoke-interface/range {v59 .. v59}, Lamm;->y()Lbjp;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->t:Lcgk;

    .line 206
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->a(Landroid/content/Context;)Lbvy;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->n:Lbvy;

    .line 207
    new-instance v13, Laim;

    .line 208
    invoke-interface/range {v59 .. v59}, Lamm;->g()Ljava/util/concurrent/Executor;

    move-result-object v10

    .line 209
    invoke-interface/range {v59 .. v59}, Lamm;->f()Ljava/util/concurrent/Executor;

    move-result-object v11

    new-instance v4, Lajd;

    .line 211
    invoke-interface/range {v59 .. v59}, Lamm;->s()Lbks;

    move-result-object v5

    .line 212
    invoke-interface/range {v59 .. v59}, Lamm;->r()Lajl;

    move-result-object v6

    new-instance v7, Lajh;

    new-instance v8, Lajx;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->n:Lbvy;

    invoke-direct {v8, v9}, Lajx;-><init>(Lbvy;)V

    .line 215
    invoke-interface/range {v59 .. v59}, Lamm;->t()Lbgf;

    move-result-object v9

    .line 216
    invoke-interface/range {v59 .. v59}, Lamm;->y()Lbjp;

    move-result-object v12

    invoke-direct {v7, v8, v9, v12}, Lajh;-><init>(Lajv;Lbgf;Lbjp;)V

    new-instance v8, Lbit;

    .line 217
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-direct {v8, v9}, Lbit;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->w:Ljfb;

    invoke-direct/range {v4 .. v9}, Lajd;-><init>(Lbks;Lajl;Lajv;Lbit;Ljfb;)V

    invoke-direct {v13, v10, v11, v4}, Laim;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lahx;)V

    .line 219
    new-instance v14, Laiw;

    invoke-direct {v14, v13}, Laiw;-><init>(Lahx;)V

    .line 226
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    sget-object v5, Lbbu;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    check-cast v4, Lbbu;

    .line 227
    if-nez v4, :cond_0

    .line 228
    new-instance v4, Lbbu;

    invoke-direct {v4}, Lbbu;-><init>()V

    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    sget-object v6, Lbbu;->a:Ljava/lang/String;

    .line 230
    invoke-virtual {v5, v4, v6}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 231
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I

    .line 233
    :cond_0
    invoke-virtual {v4}, Lbbu;->a()Lavo;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    .line 236
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    if-nez v5, :cond_1

    .line 237
    new-instance v5, Lavo;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lavo;-><init>(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    .line 238
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v4, v5}, Lbbu;->a(Lavo;)V

    .line 241
    :cond_1
    new-instance v4, Lahh;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lahh;-><init>(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->u:Lahh;

    .line 244
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4}, Lbqd;->a(Landroid/content/ContentResolver;)Lbqg;

    move-result-object v10

    .line 247
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->f()Lae;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v6}, Lavo;->af()Lavx;

    move-result-object v6

    .line 246
    move-object/from16 v0, p0

    invoke-static {v0, v4, v5, v6}, Laph;->a(Los;Lae;Lcdu;Lavx;)Laph;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->v:Laph;

    .line 248
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "android.hardware.camera"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v46

    .line 250
    const-class v4, Lkfd;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    move-object v9, v4

    check-cast v9, Lkfd;

    .line 251
    const-class v4, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    .line 252
    new-instance v50, Laxz;

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9, v4}, Laxz;-><init>(Landroid/content/Context;Lkfd;Lhei;)V

    .line 255
    new-instance v4, Laol;

    .line 256
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    .line 257
    invoke-virtual {v6}, Lavo;->ag()Lawc;

    move-result-object v6

    .line 258
    invoke-interface/range {v59 .. v59}, Lamm;->D()Laqa;

    move-result-object v7

    .line 259
    invoke-interface/range {v59 .. v59}, Lamm;->E()Laxu;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->v:Laph;

    .line 263
    invoke-interface/range {v59 .. v59}, Lamm;->C()Laro;

    move-result-object v12

    .line 266
    invoke-interface/range {v59 .. v59}, Lamm;->u()Lbqo;

    move-result-object v15

    .line 267
    invoke-interface/range {v59 .. v59}, Lamm;->v()Lbig;

    move-result-object v16

    .line 268
    invoke-interface/range {v59 .. v59}, Lamm;->x()Lbjp;

    move-result-object v17

    .line 269
    invoke-interface/range {v59 .. v59}, Lamm;->y()Lbjp;

    move-result-object v18

    .line 270
    invoke-interface/range {v59 .. v59}, Lamm;->z()Lbjl;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->r:Lbte;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->l:Lbjf;

    move-object/from16 v22, v0

    .line 274
    invoke-interface/range {v59 .. v59}, Lamm;->i()Lalg;

    move-result-object v23

    .line 275
    invoke-interface/range {v59 .. v59}, Lamm;->j()Lalg;

    move-result-object v24

    .line 278
    invoke-interface/range {v59 .. v59}, Lamm;->v()Lbig;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->i:Lanh;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    .line 276
    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-static {v10, v0, v1, v2}, Lalm;->a(Lbqg;Lbig;Lani;Z)Lalo;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->w:Ljfb;

    move-object/from16 v26, v0

    .line 283
    invoke-interface/range {v59 .. v59}, Lamm;->v()Lbig;

    move-result-object v27

    const-class v28, Lanu;

    const-string v29, "metadata-extractor"

    .line 285
    move-object/from16 v0, v31

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-interface {v0, v1, v2}, Lchk;->b(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    move-object/from16 v29, v0

    .line 281
    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v29

    invoke-static {v0, v1, v9, v2, v3}, Lanu;->a(Ljfb;Lbig;Lkfd;Ljava/util/concurrent/ExecutorService;Lcdu;)Lany;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->w:Ljfb;

    .line 290
    invoke-interface/range {v59 .. v59}, Lamm;->r()Lajl;

    move-result-object v27

    .line 288
    move-object/from16 v0, v27

    invoke-static {v9, v0}, Laoe;->a(Ljfb;Lajl;)Laoh;

    move-result-object v27

    const-class v9, Laqy;

    .line 291
    move-object/from16 v0, v59

    invoke-interface {v0, v9}, Lamm;->a(Ljava/lang/Class;)Lchl;

    move-result-object v28

    const-class v9, Lbsu;

    .line 292
    move-object/from16 v0, v59

    invoke-interface {v0, v9}, Lamm;->a(Ljava/lang/Class;)Lchl;

    move-result-object v29

    .line 293
    invoke-interface/range {v59 .. v59}, Lamm;->f()Ljava/util/concurrent/Executor;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->h:Lasy;

    move-object/from16 v32, v0

    .line 296
    invoke-interface/range {v59 .. v59}, Lamm;->o()Laqx;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->i:Lanh;

    move-object/from16 v34, v0

    .line 298
    invoke-interface/range {v59 .. v59}, Lamm;->a()Lald;

    move-result-object v35

    .line 299
    invoke-interface/range {v59 .. v59}, Lamm;->b()Lamy;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->u:Lahh;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->k:Lavj;

    move-object/from16 v39, v0

    .line 303
    invoke-interface/range {v59 .. v59}, Lamm;->m()Laxp;

    move-result-object v40

    .line 304
    invoke-interface/range {v59 .. v59}, Lamm;->F()Lced;

    move-result-object v41

    .line 305
    invoke-interface/range {v59 .. v59}, Lamm;->n()Lbyf;

    move-result-object v42

    .line 306
    invoke-interface/range {v59 .. v59}, Lamm;->w()Lawy;

    move-result-object v43

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->w:Ljfb;

    .line 309
    invoke-interface/range {v59 .. v59}, Lamm;->p()Lanh;

    move-result-object v44

    .line 307
    move-object/from16 v0, v44

    invoke-static {v9, v0, v10}, Lana;->a(Ljfb;Lanh;Lbqg;)Land;

    move-result-object v44

    .line 311
    invoke-interface/range {v59 .. v59}, Lamm;->r()Lajl;

    move-result-object v45

    .line 313
    invoke-interface/range {v59 .. v59}, Lamm;->R()Lali;

    move-result-object v47

    .line 314
    invoke-interface/range {v59 .. v59}, Lamm;->L()Ljfb;

    move-result-object v48

    new-instance v49, Laxt;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->g:Lhoc;

    move-object/from16 v0, v49

    invoke-direct {v0, v9}, Laxt;-><init>(Lhoc;)V

    .line 317
    invoke-interface/range {v59 .. v59}, Lamm;->M()Lbrh;

    move-result-object v51

    .line 318
    invoke-interface/range {v59 .. v59}, Lamm;->A()Lbzf;

    move-result-object v52

    .line 319
    invoke-interface/range {v59 .. v59}, Lamm;->S()Llmg;

    move-result-object v53

    .line 320
    invoke-interface/range {v59 .. v59}, Lamm;->T()Llmg;

    move-result-object v54

    .line 321
    invoke-interface/range {v59 .. v59}, Lamm;->N()Ljava/io/File;

    move-result-object v55

    .line 322
    invoke-interface/range {v59 .. v59}, Lamm;->U()Lcfg;

    move-result-object v56

    new-instance v57, Lceq;

    .line 323
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getCacheDir()Ljava/io/File;

    move-result-object v9

    move-object/from16 v0, v57

    invoke-direct {v0, v9}, Lceq;-><init>(Ljava/io/File;)V

    new-instance v58, Laxw;

    .line 324
    invoke-interface/range {v59 .. v59}, Lamm;->L()Ljfb;

    move-result-object v9

    move-object/from16 v0, v58

    invoke-direct {v0, v9}, Laxw;-><init>(Ljfb;)V

    move-object/from16 v9, p0

    invoke-direct/range {v4 .. v58}, Laol;-><init>(Landroid/content/Context;Lawc;Laqa;Laxu;Lapg;Lbqg;Lapp;Laro;Lahx;Laiw;Lbqo;Lbig;Lbjp;Lbjp;Lbjl;Lbgf;Lbte;Lbjf;Lalg;Lalg;Lalo;Lany;Laoh;Lchl;Lchl;Ljava/util/concurrent/Executor;Lchk;Lasy;Laqx;Lanh;Lald;Lamy;Lcdu;Lapl;Lavj;Laxp;Lced;Lbyf;Lawy;Land;Lajl;ZLali;Ljfb;Laxt;Laxz;Lbrh;Lbzf;Llmg;Llmg;Ljava/io/File;Lcfg;Lcez;Laxw;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    .line 326
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->j:Lcdu;

    invoke-virtual {v4}, Lcdu;->c()V

    .line 332
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 333
    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v5}, Lavo;->i()Z

    move-result v5

    if-nez v5, :cond_2

    .line 334
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v5, v4}, Lavo;->a(Landroid/content/Intent;)V

    .line 336
    :cond_2
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    invoke-virtual {v0, p1}, Laol;->a(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    invoke-virtual {v0}, Laol;->o()V

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->f:Lchh;

    invoke-virtual {v0}, Lchh;->b()V

    .line 355
    invoke-super {p0}, Lahk;->onDestroy()V

    .line 356
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 366
    invoke-super {p0, p1}, Lahk;->onNewIntent(Landroid/content/Intent;)V

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v0, p1}, Lavo;->a(Landroid/content/Intent;)V

    .line 371
    invoke-virtual {p0, p1}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->setIntent(Landroid/content/Intent;)V

    .line 372
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_0

    .line 460
    sget-object v0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    .line 461
    const/4 v0, 0x0

    .line 475
    :goto_0
    return v0

    .line 464
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_2

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->v:Laph;

    invoke-virtual {v0}, Laph;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 466
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q()V

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->v:Laph;

    invoke-virtual {v0}, Laph;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    invoke-virtual {v0}, Laol;->n()V

    .line 469
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->finish()V

    .line 471
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 475
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    invoke-virtual {v0, p1}, Laol;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    iget-boolean v0, v0, Lamn;->i:Z

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    invoke-virtual {v0}, Laol;->b()V

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->n:Lbvy;

    invoke-virtual {v0}, Lbvy;->b()V

    .line 431
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    new-instance v1, Lahg;

    invoke-direct {v1, p0}, Lahg;-><init>(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)V

    invoke-virtual {v0, v1}, Lbgf;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lbgm; {:try_start_0 .. :try_end_0} :catch_0

    .line 444
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->m:Lbqo;

    invoke-virtual {v0}, Lbqo;->a()V

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    invoke-virtual {v0}, Lbgf;->b()V

    .line 447
    invoke-super {p0}, Lahk;->onPause()V

    .line 448
    return-void

    .line 441
    :catch_0
    move-exception v0

    .line 442
    sget-object v1, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    const-string v2, "render context not initialized"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 376
    invoke-super {p0}, Lahk;->onResume()V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->h:Lasy;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lasy;->a(Landroid/content/Intent;)V

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->n:Lbvy;

    invoke-virtual {v0}, Lbvy;->a()V

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    invoke-virtual {v0, p0}, Lbgf;->a(Landroid/content/Context;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->m:Lbqo;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    invoke-virtual {v0, v1}, Lbqo;->a(Lbgf;)V

    .line 384
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    new-instance v1, Lahf;

    invoke-direct {v1, p0}, Lahf;-><init>(Lcom/google/android/apps/moviemaker/MovieMakerActivity;)V

    invoke-virtual {v0, v1}, Lbgf;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lbgm; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    :goto_0
    return-void

    .line 394
    :catch_0
    move-exception v0

    .line 395
    sget-object v1, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->e:Ljava/lang/String;

    const-string v2, "render context not initialized"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 340
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v0}, Lavo;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->getChangingConfigurations()I

    move-result v0

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 340
    :goto_0
    invoke-virtual {v1, v0}, Lavo;->q(Z)V

    .line 343
    invoke-super {p0, p1}, Lahk;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->p:Lavo;

    invoke-virtual {v0, p1}, Lavo;->a(Landroid/os/Bundle;)V

    .line 345
    return-void

    .line 341
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 360
    invoke-super {p0}, Lahk;->onStart()V

    .line 361
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->setVolumeControlStream(I)V

    .line 362
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 480
    invoke-super {p0, p1}, Lahk;->onTrimMemory(I)V

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->q:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()V

    .line 486
    :cond_0
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_1

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o:Laol;

    invoke-virtual {v0}, Laol;->o()V

    .line 489
    :cond_1
    return-void
.end method
