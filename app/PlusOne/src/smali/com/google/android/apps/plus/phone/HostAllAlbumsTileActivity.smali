.class public Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 45
    invoke-direct {p0}, Lloa;-><init>()V

    .line 50
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 51
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 53
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->x:Llnh;

    .line 54
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 55
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 57
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 58
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 60
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->x:Llnh;

    .line 61
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 63
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->x:Llnh;

    .line 64
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 67
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lhmw;->af:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 72
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 75
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    .line 76
    invoke-virtual {v0, v1, v5}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 77
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 79
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsm;

    new-instance v2, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    .line 80
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->x:Llnh;

    const-class v1, Lcnt;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    .line 84
    const-string v1, "Albums"

    invoke-virtual {v0, v1, v5}, Lcnt;->a(Ljava/lang/String;I)V

    .line 86
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 112
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 114
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 115
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 116
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 104
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 92
    if-nez p1, :cond_0

    .line 93
    new-instance v0, Lebs;

    invoke-direct {v0}, Lebs;-><init>()V

    .line 94
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 95
    invoke-virtual {v0, v1}, Lu;->f(Landroid/os/Bundle;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 98
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostAllAlbumsTileActivity;->setContentView(I)V

    .line 99
    return-void
.end method
