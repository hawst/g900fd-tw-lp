.class public Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Lloa;-><init>()V

    .line 29
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 31
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->x:Llnh;

    .line 32
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 34
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->x:Llnh;

    .line 35
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 36
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lhmw;->o:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 86
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 96
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 3

    .prologue
    .line 51
    const v0, 0x7f100199

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->f()Lae;

    move-result-object v1

    const v2, 0x7f1003a5

    invoke-virtual {v1, v2}, Lae;->a(I)Lu;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;

    .line 54
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->b(Landroid/widget/ProgressBar;)V

    .line 55
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f040120

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->setContentView(I)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->h()Loo;

    move-result-object v0

    .line 45
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Loo;->c(Z)V

    .line 46
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f12000e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const v2, 0x7f1003a5

    .line 70
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 71
    const v1, 0x7f10016a

    if-ne v0, v1, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->f()Lae;

    move-result-object v0

    invoke-virtual {v0, v2}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;

    .line 73
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->e()V

    .line 74
    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    .line 75
    :cond_0
    const v1, 0x7f1004a2

    if-ne v0, v1, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->f()Lae;

    move-result-object v0

    invoke-virtual {v0, v2}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;

    .line 77
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->U()V

    .line 79
    :cond_1
    invoke-super {p0, p1}, Lloa;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    return v0
.end method
