.class public Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lloa;-><init>()V

    .line 36
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 37
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->x:Llnh;

    .line 38
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 39
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 40
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;IZZ)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 51
    const-string v1, "photos_app_url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 53
    const-string v1, "photos_promo_direct_to_home"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 54
    const-string v1, "photos_app_promo_no_later_button"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 55
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    return-object v0
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 118
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->x:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 120
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 94
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1004c9

    if-ne v0, v1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->x:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    invoke-direct {v1, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ex:Lhmv;

    .line 96
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->aA:Lhmw;

    .line 97
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 95
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photos_app_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-static {v0}, Lfuz;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 112
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->startActivity(Landroid/content/Intent;)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->finish()V

    .line 114
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photos_promo_direct_to_home"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->x:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->ey:Lhmv;

    .line 104
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    sget-object v3, Lhmw;->aA:Lhmw;

    .line 105
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v2

    .line 103
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 106
    invoke-static {p0}, Leyq;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 107
    const-string v2, "photos_promo_direct_to_home"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 108
    const-string v1, "photos_app_promo_shown"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 109
    const-string v1, "account_id"

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "account_id"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 109
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f1004ca

    .line 61
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v0, 0x7f04018f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->setContentView(I)V

    .line 63
    const v0, 0x7f1004c9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photos_app_promo_no_later_button"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->x:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    invoke-direct {v1, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ew:Lhmv;

    .line 72
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->aA:Lhmw;

    .line 73
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 71
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 74
    return-void

    .line 69
    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Lloa;->onResume()V

    .line 79
    invoke-static {p0}, Lfuy;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-static {p0}, Lfuy;->e(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->finish()V

    .line 85
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->startActivity(Landroid/content/Intent;)V

    .line 87
    :cond_0
    return-void
.end method
