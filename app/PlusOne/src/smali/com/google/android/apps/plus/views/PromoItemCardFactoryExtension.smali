.class public Lcom/google/android/apps/plus/views/PromoItemCardFactoryExtension;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkzj;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# static fields
.field private static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Integer;

    .line 40
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 41
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 42
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    .line 43
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    .line 44
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const/4 v3, 0x5

    .line 45
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 39
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/apps/plus/views/PromoItemCardFactoryExtension;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;J)I
    .locals 4

    .prologue
    .line 120
    invoke-static {p1}, Llcl;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 121
    const-string v1, "PromoCardFactoryExt"

    const-string v2, "Incorrect stream card factory for activity ID: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    const/4 v0, -0x1

    .line 153
    :goto_1
    return v0

    .line 121
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 125
    :cond_1
    invoke-static {p1}, Llcl;->b(Ljava/lang/String;)I

    move-result v0

    .line 126
    packed-switch v0, :pswitch_data_0

    .line 153
    const/4 v0, 0x0

    goto :goto_1

    .line 132
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_1

    .line 136
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_1

    .line 140
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_1

    .line 145
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_1

    .line 149
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_1

    .line 126
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/String;J)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/views/PromoItemCardFactoryExtension;->a(Landroid/content/Context;Ljava/lang/String;)Lldq;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/apps/plus/views/PromoItemCardFactoryExtension;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Lldq;
    .locals 4

    .prologue
    .line 52
    invoke-static {p2}, Llcl;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 53
    const-string v1, "PromoCardFactoryExt"

    const-string v2, "Incorrect stream card factory for activity ID: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const/4 v0, 0x0

    .line 108
    :cond_0
    :goto_1
    return-object v0

    .line 53
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_2
    invoke-static {p2}, Llcl;->b(Ljava/lang/String;)I

    move-result v2

    .line 60
    const-class v0, Llcg;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llcg;

    .line 62
    if-eqz v0, :cond_3

    .line 63
    const-class v1, Lhee;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhee;

    invoke-interface {v1}, Lhee;->d()I

    .line 64
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Llcg;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Llcf;

    .line 65
    if-eqz v0, :cond_3

    .line 66
    invoke-interface {v0}, Llcf;->a()Llch;

    move-result-object v0

    .line 67
    if-nez v0, :cond_0

    .line 73
    :cond_3
    packed-switch v2, :pswitch_data_0

    .line 108
    new-instance v0, Llch;

    invoke-direct {v0, p1}, Llch;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 79
    :pswitch_0
    new-instance v0, Lfzf;

    invoke-direct {v0, p1}, Lfzf;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 83
    :pswitch_1
    new-instance v0, Lldj;

    invoke-direct {v0, p1}, Lldj;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 87
    :pswitch_2
    new-instance v0, Llek;

    invoke-direct {v0, p1}, Llek;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 92
    :pswitch_3
    new-instance v0, Lkyo;

    invoke-direct {v0, p1}, Lkyo;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 96
    :pswitch_4
    new-instance v0, Llem;

    invoke-direct {v0, p1}, Llem;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 100
    :pswitch_5
    new-instance v0, Lldd;

    invoke-direct {v0, p1}, Lldd;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 104
    :pswitch_6
    new-instance v0, Llen;

    invoke-direct {v0, p1}, Llen;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PromoItemCardFactoryExtension;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    const-string v0, "promo"

    return-object v0
.end method
