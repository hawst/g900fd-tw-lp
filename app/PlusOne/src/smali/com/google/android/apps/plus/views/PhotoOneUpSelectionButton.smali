.class public Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lfwx;


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/Paint;

.field private static c:Landroid/graphics/Bitmap;

.field private static d:Landroid/graphics/Bitmap;

.field private static e:Landroid/graphics/Bitmap;

.field private static f:Landroid/graphics/Bitmap;

.field private static g:I

.field private static h:Landroid/graphics/Rect;


# instance fields
.field private i:Lfww;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Ljcl;

.field private p:Llip;

.field private q:Lfzp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 86
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->b()V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->b()V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->b()V

    .line 97
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 101
    sget-boolean v1, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->a:Z

    if-nez v1, :cond_0

    .line 102
    const v1, 0x7f0c006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 103
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 104
    sput-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->b:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 105
    const v1, 0x7f02047d

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->d:Landroid/graphics/Bitmap;

    .line 106
    const v1, 0x7f02047f

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->c:Landroid/graphics/Bitmap;

    .line 107
    const v1, 0x7f020479

    .line 108
    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->e:Landroid/graphics/Bitmap;

    .line 109
    const v1, 0x7f02015b

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->f:Landroid/graphics/Bitmap;

    .line 110
    const v1, 0x7f0d02aa

    .line 111
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->g:I

    .line 112
    new-instance v0, Landroid/graphics/Rect;

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->g:I

    mul-int/lit8 v1, v1, 0x2

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->g:I

    mul-int/lit8 v2, v2, 0x2

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->d:Landroid/graphics/Bitmap;

    .line 113
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->h:Landroid/graphics/Rect;

    .line 114
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->a:Z

    .line 117
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->j:Ljava/util/List;

    .line 118
    return-void
.end method


# virtual methods
.method public a(Lfww;)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->i:Lfww;

    if-ne p1, v0, :cond_0

    .line 184
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->l:Z

    if-eqz v0, :cond_2

    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->b(Z)V

    .line 190
    :cond_0
    :goto_1
    return-void

    .line 185
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->q:Lfzp;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->q:Lfzp;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->o:Ljcl;

    invoke-interface {v0, p0}, Lfzp;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method public a(Lfzp;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->q:Lfzp;

    .line 152
    return-void
.end method

.method public a(Ljcl;Z)V
    .locals 7

    .prologue
    .line 193
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->o:Ljcl;

    .line 194
    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->m:Z

    .line 195
    new-instance v0, Lfww;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->h:Landroid/graphics/Rect;

    .line 196
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->h:Landroid/graphics/Rect;

    .line 197
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lfww;-><init>(IIIILfwx;Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->i:Lfww;

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->invalidate()V

    .line 199
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->k:Z

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->requestLayout()V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->invalidate()V

    .line 148
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->m:Z

    return v0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->m:Z

    if-ne p1, v0, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->l:Z

    if-eqz v0, :cond_0

    .line 163
    :cond_2
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->m:Z

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->q:Lfzp;

    if-eqz v0, :cond_3

    .line 165
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->m:Z

    if-eqz v0, :cond_4

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->q:Lfzp;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->o:Ljcl;

    invoke-interface {v0, v1}, Lfzp;->a(Ljcl;)V

    .line 174
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->invalidate()V

    goto :goto_0

    .line 168
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->q:Lfzp;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->o:Ljcl;

    invoke-interface {v0, v1}, Lfzp;->b(Ljcl;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->m:Z

    goto :goto_1
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 202
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->o:Ljcl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->l:Z

    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->requestLayout()V

    .line 205
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->invalidate()V

    .line 206
    return-void

    .line 202
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 209
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->n:Z

    .line 210
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 132
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 134
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->k:Z

    if-eqz v0, :cond_0

    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->m:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->n:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->e:Landroid/graphics/Bitmap;

    .line 138
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->m:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    .line 139
    :goto_1
    sget v2, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->g:I

    int-to-float v2, v2

    sget v3, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->g:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 142
    :cond_0
    return-void

    .line 135
    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->c:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->l:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->d:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->f:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 138
    :cond_4
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->b:Landroid/graphics/Paint;

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 122
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->i:Lfww;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 125
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->i:Lfww;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->i:Lfww;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 216
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v0, v1

    :goto_1
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->j:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->p:Llip;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_2
    return v0

    .line 216
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_3
    if-ltz v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    invoke-interface {v0, v4, v5, v1}, Llip;->a(III)Z

    move-result v6

    if-eqz v6, :cond_2

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->p:Llip;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->invalidate()V

    move v0, v2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_3

    :pswitch_2
    iput-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->p:Llip;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_4
    if-ltz v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    invoke-interface {v0, v4, v5, v2}, Llip;->a(III)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->invalidate()V

    move v0, v2

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_4

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->p:Llip;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->p:Llip;

    const/4 v3, 0x3

    invoke-interface {v0, v4, v5, v3}, Llip;->a(III)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->invalidate()V

    iput-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->p:Llip;

    move v0, v2

    goto :goto_1

    :cond_4
    iput-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->p:Llip;

    goto :goto_0

    :cond_5
    move v0, v1

    .line 217
    goto :goto_2

    .line 216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
