.class public Lcom/google/android/apps/plus/views/PhotoTileView;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"

# interfaces
.implements Lfwx;
.implements Llke;


# static fields
.field private static A:Landroid/graphics/Rect;

.field private static B:Landroid/graphics/Paint;

.field private static C:Landroid/graphics/drawable/NinePatchDrawable;

.field private static D:Landroid/graphics/Bitmap;

.field private static E:Landroid/graphics/Bitmap;

.field private static F:Landroid/graphics/Bitmap;

.field private static G:Landroid/graphics/Bitmap;

.field private static H:I

.field private static I:I

.field private static J:I

.field private static K:Landroid/graphics/Rect;

.field private static L:Landroid/graphics/Rect;

.field private static M:Landroid/graphics/Paint;

.field private static N:Landroid/graphics/Bitmap;

.field private static O:I

.field private static a:Z

.field private static b:Landroid/graphics/Paint;

.field private static c:Landroid/text/TextPaint;

.field private static d:Landroid/text/TextPaint;

.field private static e:Landroid/graphics/drawable/Drawable;

.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:Landroid/graphics/Paint;

.field private static h:Landroid/graphics/Rect;

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:Landroid/graphics/Bitmap;

.field private static n:Landroid/graphics/drawable/Drawable;

.field private static o:Landroid/graphics/Bitmap;

.field private static p:Landroid/graphics/Paint;

.field private static q:Landroid/graphics/Rect;

.field private static r:Landroid/graphics/drawable/NinePatchDrawable;

.field private static s:Landroid/graphics/Rect;

.field private static t:Landroid/graphics/Rect;

.field private static u:Landroid/graphics/Bitmap;

.field private static v:I

.field private static x:Landroid/graphics/Bitmap;

.field private static y:Landroid/graphics/Paint;

.field private static z:Landroid/graphics/Paint;


# instance fields
.field private P:Ljava/lang/CharSequence;

.field private Q:Ljava/lang/CharSequence;

.field private R:Z

.field private S:Ljcl;

.field private T:Z

.field private U:Lhri;

.field private V:F

.field private W:Z

.field private aa:Z

.field private ab:I

.field private ac:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private ad:Llip;

.field private ae:Lfzv;

.field private af:I

.field private ag:J

.field private final ah:Lepy;

.field private final ai:Lctz;

.field private final aj:Lfyp;

.field private final ak:Lctq;

.field private final al:Ljma;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljma",
            "<",
            "Lctz;",
            ">;"
        }
    .end annotation
.end field

.field private final am:Ljma;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljma",
            "<",
            "Lctq;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lfww;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 182
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const v6, 0x7f0b013a

    const v7, 0x7f020475

    const/4 v1, 0x0

    const v5, 0x7f0d024c

    const/4 v2, 0x1

    .line 185
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 135
    sget-object v0, Lhri;->a:Lhri;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->U:Lhri;

    .line 148
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ac:Ljava/util/Set;

    .line 156
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->af:I

    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-boolean v0, Lcom/google/android/apps/plus/views/PhotoTileView;->a:Z

    if-nez v0, :cond_0

    const v0, 0x7f0b02ea

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->e:Landroid/graphics/drawable/Drawable;

    const v0, 0x7f020490

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->f:Landroid/graphics/drawable/Drawable;

    const v0, 0x7f0204d7

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->o:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    invoke-static {v0, v5}, Llib;->a(Landroid/text/TextPaint;I)V

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->d:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->d:Landroid/text/TextPaint;

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->d:Landroid/text/TextPaint;

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->d:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->d:Landroid/text/TextPaint;

    invoke-static {v0, v5}, Llib;->a(Landroid/text/TextPaint;I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->b:Landroid/graphics/Paint;

    const v4, 0x7f0b02df

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->b:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->g:Landroid/graphics/Paint;

    const v4, 0x7f0b02de

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->g:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->h:Landroid/graphics/Rect;

    const v0, 0x7f0d0296

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->l:I

    const v0, 0x7f0d0293

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->j:I

    const v0, 0x7f0d0294

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->i:I

    const v0, 0x7f0d0295

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->k:I

    const v0, 0x7f020191

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->n:Landroid/graphics/drawable/Drawable;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->p:Landroid/graphics/Paint;

    const v4, 0x7f0b02e4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->p:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->q:Landroid/graphics/Rect;

    const v0, 0x7f020470

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->N:Landroid/graphics/Bitmap;

    const v0, 0x7f0d02ad

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->O:I

    const v0, 0x7f0d02ac

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->r:Landroid/graphics/drawable/NinePatchDrawable;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->s:Landroid/graphics/Rect;

    const v0, 0x7f0204c4

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->u:Landroid/graphics/Bitmap;

    const v0, 0x7f0d02ab

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->v:I

    new-instance v0, Landroid/graphics/Rect;

    sget v5, Lcom/google/android/apps/plus/views/PhotoTileView;->v:I

    add-int/2addr v5, v4

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoTileView;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    sget v6, Lcom/google/android/apps/plus/views/PhotoTileView;->v:I

    add-int/2addr v4, v6

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoTileView;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    add-int/2addr v4, v6

    invoke-direct {v0, v1, v1, v5, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->t:Landroid/graphics/Rect;

    const v0, 0x7f0204d9

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->x:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->y:Landroid/graphics/Paint;

    const v4, 0x7f0b02e1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->y:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->z:Landroid/graphics/Paint;

    const v4, 0x7f0b02e0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->z:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->A:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->B:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->C:Landroid/graphics/drawable/NinePatchDrawable;

    const v0, 0x7f02018b

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->D:Landroid/graphics/Bitmap;

    const v0, 0x7f02018e

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->E:Landroid/graphics/Bitmap;

    const v0, 0x7f02018a

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->F:Landroid/graphics/Bitmap;

    const v0, 0x7f0203e4

    invoke-static {v3, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->G:Landroid/graphics/Bitmap;

    const v0, 0x7f0d02b4

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->H:I

    const v0, 0x7f0d02b5

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->I:I

    const v0, 0x7f0d02b6

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoTileView;->J:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->K:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    sput-boolean v2, Lcom/google/android/apps/plus/views/PhotoTileView;->a:Z

    .line 189
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(I)V

    .line 190
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_1

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_2

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->j(Z)V

    .line 191
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 192
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->d(Landroid/graphics/drawable/Drawable;)V

    .line 193
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->c(Landroid/graphics/drawable/Drawable;)V

    .line 195
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->W:Z

    .line 196
    new-instance v0, Lfzv;

    new-instance v1, Lfzw;

    invoke-direct {v1, p0}, Lfzw;-><init>(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    invoke-direct {v0, v1}, Lfzv;-><init>(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ae:Lfzv;

    .line 198
    const-class v0, Lfyp;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfyp;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->aj:Lfyp;

    .line 199
    const-class v0, Lepy;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepy;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ah:Lepy;

    .line 200
    const-class v0, Lctz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ai:Lctz;

    .line 201
    const-class v0, Lctq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ak:Lctq;

    .line 203
    new-instance v0, Lfzr;

    invoke-direct {v0, p0}, Lfzr;-><init>(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->al:Ljma;

    .line 215
    new-instance v0, Lfzs;

    invoke-direct {v0, p0}, Lfzs;-><init>(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->am:Ljma;

    .line 226
    return-void

    :cond_2
    move v0, v1

    .line 190
    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/PhotoTileView;)Ljcl;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->S:Ljcl;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/PhotoTileView;)Lctz;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ai:Lctz;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/views/PhotoTileView;)Lfyp;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->aj:Lfyp;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/views/PhotoTileView;)Lepy;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ah:Lepy;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 690
    invoke-super {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ac:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 693
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ad:Llip;

    .line 694
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->P:Ljava/lang/CharSequence;

    .line 695
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->Q:Ljava/lang/CharSequence;

    .line 696
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    .line 697
    sget-object v0, Lhri;->a:Lhri;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->U:Lhri;

    .line 698
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->V:F

    .line 699
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ab:I

    .line 700
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 268
    or-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ab:I

    .line 269
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->invalidate()V

    .line 270
    return-void
.end method

.method public a(Lfww;)V
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ah:Lepy;

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ah:Lepy;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->S:Ljcl;

    invoke-interface {v0, v1}, Lepy;->a(Ljcl;)V

    .line 707
    :cond_0
    return-void
.end method

.method public a(Lhri;FJ)V
    .locals 1

    .prologue
    .line 346
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->U:Lhri;

    .line 347
    iput p2, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->V:F

    .line 348
    iput-wide p3, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ag:J

    .line 349
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->invalidate()V

    .line 350
    return-void
.end method

.method public a(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 274
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 275
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->P:Ljava/lang/CharSequence;

    .line 279
    :goto_0
    return-void

    .line 277
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->P:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public a(Ljcl;)V
    .locals 2

    .prologue
    .line 291
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->S:Ljcl;

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ac:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 297
    :cond_0
    new-instance v0, Lfww;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->t:Landroid/graphics/Rect;

    invoke-direct {v0, v1, p0}, Lfww;-><init>(Landroid/graphics/Rect;Lfwx;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    .line 298
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 251
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->T:Z

    .line 252
    return-void
.end method

.method public b(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 283
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 284
    :cond_0
    const/4 v0, 0x0

    .line 286
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->Q:Ljava/lang/CharSequence;

    .line 288
    return-void

    .line 286
    :cond_1
    const-string v1, "+"

    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->W:Z

    .line 260
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->R:Z

    .line 309
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->R:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->h(Z)V

    .line 310
    return-void

    .line 309
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 321
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->aa:Z

    .line 322
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 646
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 647
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 649
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 685
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 651
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ac:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 652
    const/4 v5, 0x0

    invoke-interface {v0, v2, v3, v5}, Llip;->a(III)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 653
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ad:Llip;

    .line 654
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->invalidate()V

    move v0, v1

    .line 655
    goto :goto_1

    .line 662
    :pswitch_2
    iput-object v5, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ad:Llip;

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ac:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 664
    invoke-interface {v0, v2, v3, v1}, Llip;->a(III)Z

    goto :goto_2

    .line 666
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->invalidate()V

    goto :goto_0

    .line 671
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ad:Llip;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ad:Llip;

    const/4 v4, 0x3

    invoke-interface {v0, v2, v3, v4}, Llip;->a(III)Z

    .line 673
    iput-object v5, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ad:Llip;

    .line 674
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->invalidate()V

    move v0, v1

    .line 675
    goto :goto_1

    .line 649
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->R:Z

    return v0
.end method

.method public i()Ljcl;
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->S:Ljcl;

    return-object v0
.end method

.method protected j()Z
    .locals 1

    .prologue
    .line 715
    invoke-super {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ak:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 230
    invoke-super {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->onAttachedToWindow()V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ak:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ae:Lfzv;

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ak:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->am:Ljma;

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ai:Lctz;

    invoke-virtual {v0}, Lctz;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->al:Ljma;

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 235
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 239
    invoke-super {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->onDetachedFromWindow()V

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ak:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ae:Lfzv;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ak:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->am:Ljma;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ai:Lctz;

    invoke-virtual {v0}, Lctz;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->al:Ljma;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 243
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/high16 v7, -0x1000000

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 354
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    .line 355
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 356
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->onDraw(Landroid/graphics/Canvas;)V

    .line 357
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 363
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 364
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->A:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 365
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->A:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->z:Landroid/graphics/Paint;

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 367
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->x:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileView;->x:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->x:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 367
    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 464
    :cond_0
    :goto_1
    return-void

    .line 365
    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->y:Landroid/graphics/Paint;

    goto :goto_0

    .line 373
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->r()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ak:Lctq;

    invoke-virtual {v0}, Lctq;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 379
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->h:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 380
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->h:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 384
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->aa:Z

    if-eqz v0, :cond_5

    .line 385
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->N:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v1

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->N:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v1, v3

    sget v3, Lcom/google/android/apps/plus/views/PhotoTileView;->O:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    sget v3, Lcom/google/android/apps/plus/views/PhotoTileView;->O:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v3, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 390
    :cond_5
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ab:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 391
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ab:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x8

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    mul-int/lit8 v4, v0, 0x1

    mul-int/lit8 v5, v0, 0x2

    invoke-virtual {v3, v4, v2, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ab:I

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lfzu;->a(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setColor(I)V

    const-string v3, "L"

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v4, 0x4

    int-to-float v4, v4

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v5, v5, -0x4

    int-to-float v5, v5

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    mul-int/lit8 v4, v0, 0x2

    mul-int/lit8 v5, v0, 0x3

    invoke-virtual {v3, v4, v2, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ab:I

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lfzu;->a(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setColor(I)V

    const-string v3, "S"

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v4, 0x4

    int-to-float v4, v4

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v5, v5, -0x4

    int-to-float v5, v5

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    mul-int/lit8 v4, v0, 0x3

    mul-int/lit8 v0, v0, 0x4

    invoke-virtual {v3, v4, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ab:I

    const/16 v3, 0x8

    invoke-static {v1, v3}, Lfzu;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    const-string v0, "A"

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0x4

    int-to-float v1, v1

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->L:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->M:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 394
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ak:Lctq;

    invoke-virtual {v0}, Lctq;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->S:Ljcl;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ai:Lctz;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->S:Ljcl;

    invoke-virtual {v0, v1}, Lctz;->a(Ljcl;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 396
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->q:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 397
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->q:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 398
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->o:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v1

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 399
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    .line 398
    invoke-virtual {p1, v0, v1, v3, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 404
    :cond_7
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->s:Landroid/graphics/Rect;

    .line 405
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v1

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTileView;->r:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/NinePatchDrawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v1, v3

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v4

    .line 404
    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 407
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->r:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->s:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 408
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->r:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 410
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->W:Z

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ac:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    if-eqz v0, :cond_8

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ac:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 417
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoTileView;->v:I

    sub-int/2addr v0, v1

    .line 418
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileView;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/views/PhotoTileView;->v:I

    sub-int/2addr v1, v2

    .line 419
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileView;->u:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v2, v0, v1, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 426
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->U:Lhri;

    sget-object v1, Lhri;->a:Lhri;

    if-eq v0, v1, :cond_c

    .line 427
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v3

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->K:Landroid/graphics/Rect;

    sget v4, Lcom/google/android/apps/plus/views/PhotoTileView;->H:I

    sub-int v4, v3, v4

    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->C:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->K:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->C:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->B:Landroid/graphics/Paint;

    const/16 v4, 0xff

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    sget-object v0, Lfzt;->a:[I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->U:Lhri;

    invoke-virtual {v4}, Lhri;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->D:Landroid/graphics/Bitmap;

    :cond_a
    :goto_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v1, v4

    sget v4, Lcom/google/android/apps/plus/views/PhotoTileView;->I:I

    sub-int/2addr v1, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    sget v4, Lcom/google/android/apps/plus/views/PhotoTileView;->J:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->U:Lhri;

    sget-object v5, Lhri;->c:Lhri;

    if-ne v4, v5, :cond_b

    iget v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->V:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_b

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v4, v1

    int-to-float v5, v3

    int-to-float v6, v1

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->V:F

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/2addr v7, v3

    int-to-float v7, v7

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->E:Landroid/graphics/Bitmap;

    int-to-float v5, v1

    int-to-float v6, v3

    invoke-virtual {p1, v4, v5, v6, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_b
    int-to-float v1, v1

    int-to-float v3, v3

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->B:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 430
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->Q:Ljava/lang/CharSequence;

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->P:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 434
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 435
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 436
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->Q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_e

    .line 440
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/PhotoTileView;->k:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->d:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    sub-float v5, v0, v1

    .line 441
    sget v0, Lcom/google/android/apps/plus/views/PhotoTileView;->i:I

    int-to-float v4, v0

    .line 442
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->Q:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->Q:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoTileView;->d:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 447
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->P:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 448
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->m:Landroid/graphics/Bitmap;

    .line 449
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    invoke-static {v1}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v1

    int-to-float v1, v1

    .line 450
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/PhotoTileView;->k:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    sub-float v1, v3, v1

    .line 451
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileView;->getWidth()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/PhotoTileView;->j:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 453
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 454
    invoke-virtual {p1, v0, v3, v1, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 456
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->P:Ljava/lang/CharSequence;

    invoke-static {v0, v4}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;)I

    move-result v0

    int-to-float v0, v0

    .line 458
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float v5, v1, v4

    .line 459
    sget v1, Lcom/google/android/apps/plus/views/PhotoTileView;->l:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    sub-float v4, v3, v0

    .line 460
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->P:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->P:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoTileView;->c:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 427
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->F:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->F:Landroid/graphics/Bitmap;

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->B:Landroid/graphics/Paint;

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ag:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1770

    cmp-long v4, v4, v6

    if-gez v4, :cond_a

    iget v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->af:I

    if-eqz v4, :cond_a

    iget v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->af:I

    add-int/lit8 v4, v4, -0x8

    iput v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->af:I

    iget v4, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->af:I

    if-gez v4, :cond_f

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->af:I

    :cond_f
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoTileView;->B:Landroid/graphics/Paint;

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->af:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    const-wide/16 v4, 0x21

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->postInvalidateDelayed(J)V

    goto/16 :goto_2

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileView;->G:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 336
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/social/media/ui/MediaView;->onLayout(ZIIII)V

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->w:Lfww;

    invoke-virtual {v0}, Lfww;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 340
    sub-int v1, p4, p2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int v2, p5, p3

    .line 341
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v2, v3

    .line 340
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 343
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->T:Z

    if-eqz v0, :cond_0

    move p2, p1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->onMeasure(II)V

    .line 332
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ae:Lfzv;

    invoke-virtual {v0, p1}, Lfzv;->a(Landroid/view/View$OnClickListener;)V

    .line 304
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileView;->ae:Lfzv;

    :goto_0
    invoke-super {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    return-void

    .line 304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
