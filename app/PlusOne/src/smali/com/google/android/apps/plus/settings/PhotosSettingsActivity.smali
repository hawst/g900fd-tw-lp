.class public Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0}, Lloa;-><init>()V

    .line 29
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;->x:Llnh;

    .line 30
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 33
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 34
    new-instance v0, Lflm;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lflm;-><init>(Los;Llqr;)V

    .line 41
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;->y:Llqc;

    const v2, 0x7f120012

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;->x:Llnh;

    .line 42
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 43
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 44
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;->x:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 69
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 60
    const v0, 0x7f10066c

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 62
    const v0, 0x7f10066d

    new-instance v1, Litl;

    const-string v2, "plus_settings"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 63
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f04007a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;->setContentView(I)V

    .line 50
    return-void
.end method
