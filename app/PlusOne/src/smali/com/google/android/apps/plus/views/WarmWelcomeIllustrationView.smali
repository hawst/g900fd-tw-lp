.class public Lcom/google/android/apps/plus/views/WarmWelcomeIllustrationView;
.super Landroid/widget/ImageView;
.source "PG"


# instance fields
.field private a:Lgcc;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method


# virtual methods
.method public a(Lgcc;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/plus/views/WarmWelcomeIllustrationView;->a:Lgcc;

    .line 41
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/views/WarmWelcomeIllustrationView;->a:Lgcc;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/views/WarmWelcomeIllustrationView;->a:Lgcc;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/WarmWelcomeIllustrationView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/WarmWelcomeIllustrationView;->getMeasuredHeight()I

    invoke-interface {v0, v1}, Lgcc;->a(I)V

    .line 49
    :cond_0
    return-void
.end method
