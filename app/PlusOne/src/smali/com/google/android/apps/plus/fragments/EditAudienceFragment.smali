.class public Lcom/google/android/apps/plus/fragments/EditAudienceFragment;
.super Leak;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Lelt;
.implements Llio;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leak;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lelt;",
        "Llio;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;

.field private static final O:[I

.field private static final P:[I

.field private static final Q:[I


# instance fields
.field private R:I

.field private S:Z

.field private T:Z

.field private U:Landroid/widget/ListView;

.field private V:Ldzu;

.field private W:Lhxh;

.field private X:Z

.field private Y:Ldzv;

.field private Z:Z

.field private aa:I

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

.field private af:Z

.field private ag:Landroid/view/View;

.field private ah:Landroid/widget/TextView;

.field private ai:Landroid/widget/TextView;

.field private aj:Landroid/widget/CompoundButton;

.field private ak:Ljava/lang/String;

.field private al:Z

.field private am:Z

.field private an:Z

.field private ao:Lely;

.field private final ap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljqs;",
            ">;"
        }
    .end annotation
.end field

.field private final aq:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lhxc;",
            ">;"
        }
    .end annotation
.end field

.field private final ar:Landroid/database/DataSetObserver;

.field private as:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 99
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const-string v1, "in_same_visibility_group"

    aput-object v1, v0, v6

    const/4 v1, 0x6

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "verified"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->N:[Ljava/lang/String;

    .line 113
    new-array v0, v3, [I

    aput v3, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->O:[I

    .line 117
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->P:[I

    .line 123
    new-array v0, v6, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Q:[I

    return-void

    .line 117
    nop

    :array_0
    .array-data 4
        0x4
        0x3
        0x2
    .end array-data

    .line 123
    :array_1
    .array-data 4
        0x0
        0x1
        0x4
        0x3
        0x2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Leak;-><init>()V

    .line 156
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    .line 157
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    .line 335
    new-instance v0, Ldzp;

    invoke-direct {v0, p0}, Ldzp;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ar:Landroid/database/DataSetObserver;

    .line 342
    new-instance v0, Ldzq;

    invoke-direct {v0, p0}, Ldzq;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->as:Ljava/lang/Runnable;

    return-void
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Y:Ldzv;

    if-eqz v0, :cond_0

    .line 596
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Y:Ldzv;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_0
    invoke-interface {v1, v0}, Ldzv;->c_(Ljava/lang/String;)V

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    if-eqz v0, :cond_1

    .line 601
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ag()Lhgw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Lhgw;)V

    .line 603
    :cond_1
    return-void

    .line 596
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->W()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;I)I
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ah()[I

    move-result-object v0

    aget v0, v0, p1

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lhxh;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->W:Lhxh;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Lhxf;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lhxf;)V

    return-void
.end method

.method private a(Lhxf;)V
    .locals 5

    .prologue
    .line 736
    invoke-virtual {p1}, Lhxf;->a()Ljava/lang/String;

    move-result-object v0

    .line 737
    invoke-virtual {p1}, Lhxf;->b()Ljava/lang/String;

    move-result-object v1

    .line 738
    invoke-virtual {p1}, Lhxf;->c()I

    move-result v2

    .line 739
    invoke-virtual {p1}, Lhxf;->d()I

    move-result v3

    .line 740
    new-instance v4, Lhxc;

    invoke-direct {v4, v0, v2, v1, v3}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 741
    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->b(Ljava/lang/String;Lhxc;)V

    .line 742
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Z)Z
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->an:Z

    return p1
.end method

.method private aa()I
    .locals 2

    .prologue
    .line 645
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->an:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aa:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/16 v0, 0xa

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aa:I

    goto :goto_0
.end method

.method private af()I
    .locals 3

    .prologue
    .line 663
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 665
    return v0
.end method

.method private ag()Lhgw;
    .locals 4

    .prologue
    .line 812
    new-instance v0, Lhgw;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    .line 813
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v1, v2}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method private ah()[I
    .locals 2

    .prologue
    .line 968
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->R:I

    if-nez v0, :cond_0

    .line 969
    sget-object v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->O:[I

    .line 973
    :goto_0
    return-object v0

    .line 970
    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->R:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 971
    sget-object v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->P:[I

    goto :goto_0

    .line 973
    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Q:[I

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;I)I
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d(I)I

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ak:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    return-object v0
.end method

.method private d(I)I
    .locals 3

    .prologue
    .line 987
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ah()[I

    move-result-object v1

    .line 988
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 989
    aget v2, v1, v0

    if-ne v2, p1, :cond_0

    .line 993
    :goto_1
    return v0

    .line 988
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 993
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->T:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Llnl;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->at:Llnl;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)I
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v0

    return v0
.end method

.method public static synthetic g(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lhgw;
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ag()Lhgw;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ldzu;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z:Z

    return v0
.end method

.method public static synthetic k(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/views/TypeableAudienceView;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 1011
    sget-object v0, Lhmw;->s:Lhmw;

    return-object v0
.end method

.method public U()Z
    .locals 1

    .prologue
    .line 763
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->S:Z

    return v0
.end method

.method public V()Lhgw;
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h()Lhgw;

    move-result-object v0

    return-object v0
.end method

.method public W()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 842
    .line 846
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d(I)I

    move-result v0

    .line 847
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z:Z

    if-eqz v2, :cond_5

    const/4 v2, -0x1

    if-eq v0, v2, :cond_5

    .line 848
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    invoke-virtual {v2, v0}, Ldzu;->l(I)Landroid/database/Cursor;

    move-result-object v5

    .line 849
    if-eqz v5, :cond_5

    invoke-interface {v5}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    move v2, v1

    move v3, v1

    .line 851
    :cond_0
    const/4 v6, 0x2

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 852
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 853
    const/4 v6, 0x3

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 854
    packed-switch v6, :pswitch_data_0

    .line 868
    :pswitch_0
    add-int/lit8 v3, v3, 0x1

    .line 871
    :cond_1
    :goto_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 875
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    add-int/2addr v3, v5

    .line 876
    if-nez v3, :cond_4

    .line 877
    if-eqz v2, :cond_2

    .line 878
    const v0, 0x7f0a086d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 886
    :goto_2
    return-object v0

    .line 857
    :pswitch_1
    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :pswitch_2
    move v2, v4

    .line 861
    goto :goto_0

    :pswitch_3
    move v0, v4

    .line 865
    goto :goto_0

    .line 879
    :cond_2
    if-eqz v0, :cond_3

    .line 880
    const v0, 0x7f0a086e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 882
    :cond_3
    const v0, 0x7f0a086f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 885
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    add-int/2addr v0, v2

    .line 886
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11006a

    new-array v4, v4, [Ljava/lang/Object;

    .line 887
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    .line 886
    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 854
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public X()Z
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aj:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public Y()V
    .locals 2

    .prologue
    .line 961
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v0

    .line 962
    instance-of v1, v0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;

    if-eqz v1, :cond_0

    .line 963
    check-cast v0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->l()V

    .line 965
    :cond_0
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 455
    const v0, 0x7f040086

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 456
    const v0, 0x7f10016e

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ag:Landroid/view/View;

    .line 457
    const v0, 0x7f100172

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ah:Landroid/widget/TextView;

    .line 458
    const v0, 0x7f100173

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ai:Landroid/widget/TextView;

    .line 459
    const v0, 0x7f100170

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aj:Landroid/widget/CompoundButton;

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aj:Landroid/widget/CompoundButton;

    new-instance v1, Ldzr;

    invoke-direct {v1, p0}, Ldzr;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 473
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->at:Llnl;

    const v2, 0x7f0901dd

    invoke-direct {v1, v0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 475
    new-instance v0, Lely;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->p()Lae;

    move-result-object v2

    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->w()Lbb;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v4

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lely;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aa()I

    move-result v1

    invoke-virtual {v0, v1}, Lely;->j_(I)V

    .line 478
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-virtual {v0, v7}, Lely;->d(Z)V

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-virtual {v0, p0}, Lely;->a(Lelt;)V

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-virtual {v0, p3}, Lely;->a(Landroid/os/Bundle;)V

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ab:Z

    invoke-virtual {v0, v1}, Lely;->g(Z)V

    .line 482
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-virtual {v0, v7}, Lely;->a(Z)V

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lely;->e(Z)V

    .line 485
    const v0, 0x7f100248

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c(I)V

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(I)V

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    const v1, 0x7f0a081f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->b(I)V

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->as:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Ljava/lang/Runnable;)V

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    const v1, 0x7f1001a2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Lely;)V

    .line 492
    const v0, 0x102000a

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->U:Landroid/widget/ListView;

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->U:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->U:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 495
    return-object v6
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 607
    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    .line 641
    :goto_0
    return-object v0

    .line 609
    :pswitch_0
    new-instance v0, Lela;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->N:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ac:Z

    invoke-direct/range {v0 .. v5}, Lela;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/HashMap;Z)V

    goto :goto_0

    .line 614
    :pswitch_1
    new-instance v0, Lhxg;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aa()I

    move-result v3

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v4, v6

    const/4 v5, 0x1

    const-string v6, "circle_name"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "circle_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "type"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "contact_count"

    aput-object v6, v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 624
    :pswitch_2
    new-instance v0, Lekz;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->N:[Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ab:Z

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ac:Z

    invoke-direct/range {v0 .. v5}, Lekz;-><init>(Landroid/content/Context;I[Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 629
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 630
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_dasher_account"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    .line 631
    new-instance v0, Lezo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lezo;-><init>(Landroid/content/Context;Z)V

    goto :goto_0

    .line 635
    :pswitch_4
    new-instance v0, Lekz;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->N:[Ljava/lang/String;

    const-string v5, "interaction_sort_key DESC LIMIT 10"

    invoke-direct/range {v0 .. v6}, Lekz;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 607
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 403
    invoke-super {p0, p1}, Leak;->a(Landroid/app/Activity;)V

    .line 404
    new-instance v0, Ldzu;

    invoke-direct {v0, p0, p1}, Ldzu;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    .line 405
    new-instance v0, Lhxh;

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->w()Lbb;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v2

    const/4 v3, 0x1

    invoke-direct {v0, p1, v1, v2, v3}, Lhxh;-><init>(Landroid/content/Context;Lbb;II)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->W:Lhxh;

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->W:Lhxh;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ar:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lhxh;->a(Landroid/database/DataSetObserver;)V

    .line 408
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 412
    invoke-super {p0, p1}, Leak;->a(Landroid/os/Bundle;)V

    .line 413
    if-eqz p1, :cond_0

    .line 414
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lhgw;)V

    .line 417
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "audience_mode"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->R:I

    .line 420
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->w()Lbb;

    move-result-object v2

    .line 421
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ah()[I

    move-result-object v3

    move v0, v1

    .line 422
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 423
    aget v4, v3, v0

    packed-switch v4, :pswitch_data_0

    .line 422
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 423
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    invoke-virtual {v4, v1, v5}, Ldzu;->b(ZZ)V

    goto :goto_1

    .line 425
    :cond_1
    :goto_2
    array-length v0, v3

    if-ge v1, v0, :cond_4

    .line 426
    aget v0, v3, v1

    .line 427
    if-ne v0, v5, :cond_3

    .line 428
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z:Z

    if-eqz v4, :cond_2

    .line 429
    invoke-virtual {v2, v0, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 425
    :cond_2
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 432
    :cond_3
    invoke-virtual {v2, v0, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_3

    .line 436
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "domain_name"

    .line 437
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ak:Ljava/lang/String;

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->W:Lhxh;

    invoke-virtual {v0}, Lhxh;->b()V

    .line 440
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->X:Z

    .line 441
    return-void

    .line 423
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 660
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 651
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ad:Z

    .line 652
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 653
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d(I)I

    move-result v0

    .line 654
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    invoke-virtual {v1, v0, p2}, Ldzu;->a(ILandroid/database/Cursor;)V

    .line 655
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c(Landroid/view/View;)V

    .line 656
    return-void

    .line 651
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 70
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ldzv;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Y:Ldzv;

    .line 355
    return-void
.end method

.method public a(Lhgw;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 770
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->S:Z

    .line 771
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 772
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 773
    if-eqz p1, :cond_6

    .line 774
    invoke-virtual {p1}, Lhgw;->b()[Lhxc;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 775
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    invoke-virtual {v4}, Lhxc;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 774
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 778
    :cond_0
    invoke-virtual {p1}, Lhgw;->a()[Ljqs;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 779
    const/4 v0, 0x0

    .line 780
    invoke-virtual {v4}, Ljqs;->a()Ljava/lang/String;

    move-result-object v5

    .line 781
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 782
    const-string v6, "g:"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 793
    :cond_1
    :goto_2
    if-eqz v0, :cond_2

    .line 794
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    invoke-virtual {v5, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 778
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 782
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 783
    :cond_4
    invoke-virtual {v4}, Ljqs;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 785
    invoke-virtual {v4}, Ljqs;->c()Ljava/lang/String;

    move-result-object v0

    .line 786
    const-string v5, "p:"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 789
    const-string v5, "e:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 798
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z()V

    .line 799
    return-void
.end method

.method public a(Ljava/lang/String;Lhxc;)V
    .locals 1

    .prologue
    .line 913
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->b(Ljava/lang/String;Lhxc;)V

    .line 914
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Lhxc;)V

    .line 915
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->e()V

    .line 916
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 938
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 921
    if-eqz p4, :cond_0

    .line 922
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v1

    .line 923
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->eT:Lhmv;

    .line 925
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 926
    invoke-virtual {v1, p4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 923
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 930
    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Ljava/lang/String;Ljqs;)V

    .line 931
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Ljqs;)V

    .line 932
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ae:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->e()V

    .line 933
    return-void
.end method

.method public a(Ljava/lang/String;Ljqs;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 820
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->X:Z

    if-eqz v0, :cond_0

    .line 822
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 823
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 826
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z()V

    .line 827
    return-void
.end method

.method public a(Llin;Z)V
    .locals 7

    .prologue
    .line 670
    instance-of v0, p1, Lhxf;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 671
    check-cast v0, Lhxf;

    .line 672
    invoke-virtual {v0}, Lhxf;->a()Ljava/lang/String;

    move-result-object v1

    .line 673
    if-eqz p2, :cond_2

    .line 674
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v1

    .line 675
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v2

    .line 676
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->at:Llnl;

    .line 677
    invoke-virtual {v0}, Lhxf;->c()I

    move-result v4

    .line 676
    invoke-static {v3, v2, v4}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 678
    invoke-static {v1, v2}, Ldhv;->m(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 679
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->n()Lz;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 680
    invoke-virtual {v0}, Lhxf;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 681
    const v4, 0x7f0a04f3

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 683
    const v4, 0x7f0a0596

    new-instance v5, Ldzs;

    invoke-direct {v5, p0, v0, v1, v2}, Ldzs;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Lhxf;Landroid/app/Activity;I)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 692
    const v0, 0x7f0a0597

    new-instance v1, Ldzt;

    invoke-direct {v1, p1}, Ldzt;-><init>(Llin;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 701
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 717
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z()V

    .line 718
    return-void

    .line 703
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lhxf;)V

    goto :goto_0

    .line 706
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 708
    :cond_3
    instance-of v0, p1, Ljpg;

    if-eqz v0, :cond_0

    .line 709
    check-cast p1, Ljpg;

    .line 710
    invoke-virtual {p1}, Ljpg;->d()Ljava/lang/String;

    move-result-object v0

    .line 711
    if-eqz p2, :cond_6

    .line 712
    invoke-virtual {p1}, Ljpg;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Ljpg;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljpg;->f()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljpg;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "e:"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :cond_4
    :goto_1
    new-instance v0, Ljqs;

    invoke-virtual {p1}, Ljpg;->h()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0, v6, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Ljava/lang/String;Ljqs;)V

    goto :goto_0

    :cond_5
    const-string v0, "p:"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v3, v6

    goto :goto_1

    .line 714
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 368
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ab:Z

    .line 369
    return-void
.end method

.method public aO_()V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 500
    invoke-super {p0}, Leak;->aO_()V

    .line 502
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af()I

    move-result v1

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->au:Llnh;

    const-class v4, Lhei;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 505
    const-string v1, "is_dasher_account"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->al:Z

    if-nez v0, :cond_3

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ag:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 508
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->am:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->an:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a0548

    .line 511
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->am:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->an:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0a0549

    .line 514
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ah:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->o()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ak:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-virtual {v5, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ai:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->o()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ak:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v4, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aj:Landroid/widget/CompoundButton;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->an:Z

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 517
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aj:Landroid/widget/CompoundButton;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->am:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 523
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c(Landroid/view/View;)V

    .line 524
    return-void

    .line 508
    :cond_0
    const v0, 0x7f0a0546

    goto :goto_0

    .line 511
    :cond_1
    const v1, 0x7f0a0547

    goto :goto_1

    :cond_2
    move v0, v3

    .line 517
    goto :goto_2

    .line 520
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ag:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aj:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_3
.end method

.method public an_()V
    .locals 0

    .prologue
    .line 909
    return-void
.end method

.method public b(Ljava/lang/String;Lhxc;)V
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aq:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 834
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z()V

    .line 835
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 947
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 375
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z:Z

    .line 376
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 361
    iput p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->aa:I

    .line 362
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 567
    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 568
    const v1, 0x7f100249

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 569
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ad:Z

    if-eqz v2, :cond_0

    .line 570
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 571
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 572
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->h(Landroid/view/View;)V

    .line 588
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->Z()V

    .line 589
    return-void

    .line 573
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 574
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 575
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 576
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d(Landroid/view/View;)V

    goto :goto_0

    .line 577
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 578
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 579
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 580
    const v0, 0x7f0a07ed

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Landroid/view/View;I)V

    .line 581
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->g(Landroid/view/View;)V

    goto :goto_0

    .line 583
    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 584
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 585
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->h(Landroid/view/View;)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 382
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ac:Z

    .line 383
    return-void
.end method

.method protected c()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 543
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->W:Lhxh;

    invoke-virtual {v1}, Lhxh;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 554
    :cond_0
    :goto_0
    return v0

    .line 547
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->w()Lbb;

    move-result-object v2

    .line 548
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ah()[I

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_3

    .line 549
    invoke-virtual {v2, v1}, Lbb;->b(I)Ldo;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    invoke-virtual {v3, v1}, Ldzu;->l(I)Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 548
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 554
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    if-nez v0, :cond_0

    .line 560
    const/4 v0, 0x1

    .line 563
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V:Ldzu;

    invoke-virtual {v0}, Ldzu;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 445
    invoke-super {p0, p1}, Leak;->e(Landroid/os/Bundle;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-virtual {v0, p1}, Lely;->b(Landroid/os/Bundle;)V

    .line 449
    const-string v0, "audience"

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ag()Lhgw;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 450
    return-void
.end method

.method public e()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 755
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V()Lhgw;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    invoke-virtual {v2}, Lhgw;->h()I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Lhgw;->e()I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Lhgw;->i()I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Lhgw;->j()I

    move-result v2

    if-nez v2, :cond_3

    move v2, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 528
    invoke-super {p0}, Leak;->g()V

    .line 529
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-virtual {v0}, Lely;->f()V

    .line 532
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 536
    invoke-super {p0}, Leak;->h()V

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->ao:Lely;

    invoke-virtual {v0}, Lely;->g()V

    .line 540
    :cond_0
    return-void
.end method

.method public i(Z)V
    .locals 0

    .prologue
    .line 389
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->af:Z

    .line 390
    return-void
.end method

.method public j(Z)V
    .locals 0

    .prologue
    .line 398
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->T:Z

    .line 399
    return-void
.end method

.method public k(Z)V
    .locals 0

    .prologue
    .line 896
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->an:Z

    .line 897
    return-void
.end method

.method public l(Z)V
    .locals 0

    .prologue
    .line 900
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->al:Z

    .line 901
    return-void
.end method

.method public m(Z)V
    .locals 0

    .prologue
    .line 904
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->am:Z

    .line 905
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 746
    instance-of v0, p2, Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    .line 747
    check-cast p2, Landroid/widget/Checkable;

    invoke-interface {p2}, Landroid/widget/Checkable;->toggle()V

    .line 749
    :cond_0
    return-void
.end method
