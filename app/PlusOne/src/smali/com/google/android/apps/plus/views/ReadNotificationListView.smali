.class public Lcom/google/android/apps/plus/views/ReadNotificationListView;
.super Landroid/widget/ListView;
.source "PG"


# static fields
.field private static a:I


# instance fields
.field private b:F

.field private c:Z

.field private d:Landroid/view/View;

.field private e:Lgau;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 37
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    .line 55
    sget v0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a:I

    if-gez v0, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a:I

    .line 44
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    .line 55
    sget v0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a:I

    if-gez v0, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a:I

    .line 48
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    .line 55
    sget v0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a:I

    if-gez v0, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a:I

    .line 52
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/ReadNotificationListView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    return-object v0
.end method

.method private a(F)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    .line 197
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 198
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setTranslationY(F)V

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 202
    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/ReadNotificationListView;)Lgau;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->e:Lgau;

    return-object v0
.end method

.method private static b()Z
    .locals 2

    .prologue
    .line 182
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->clearAnimation()V

    .line 239
    invoke-static {}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    .line 241
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setTranslationY(F)V

    .line 242
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setAlpha(F)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 250
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    .line 251
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    .line 252
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    .line 62
    return-void
.end method

.method public a(Lgau;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->e:Lgau;

    .line 66
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v8, -0x40800000    # -1.0f

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 96
    invoke-static {}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 97
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 175
    :goto_0
    return v0

    .line 100
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 102
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    .line 103
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    .line 104
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 108
    :pswitch_1
    iput v8, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    .line 109
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a(F)V

    :goto_1
    move v0, v1

    .line 175
    goto :goto_0

    .line 114
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getFirstVisiblePosition()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getTop()I

    move-result v4

    if-ne v2, v4, :cond_1

    move v2, v1

    :goto_2
    if-nez v2, :cond_2

    .line 115
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v2, v0

    .line 114
    goto :goto_2

    .line 118
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 119
    iget v4, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    cmpg-float v4, v4, v3

    if-gez v4, :cond_3

    .line 120
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    .line 121
    iput v2, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    .line 123
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getTranslationY()F

    move-result v0

    add-float/2addr v0, v2

    .line 125
    iget v2, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    sub-float/2addr v0, v2

    .line 126
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    if-nez v2, :cond_4

    sget v2, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_4

    .line 127
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    .line 131
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    if-eqz v2, :cond_5

    cmpl-float v2, v0, v3

    if-lez v2, :cond_5

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 133
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a(F)V

    goto :goto_1

    .line 135
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 141
    :pswitch_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->c:Z

    if-eqz v2, :cond_9

    .line 142
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getTranslationY()F

    move-result v4

    add-float/2addr v2, v4

    .line 143
    iget v4, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    sub-float/2addr v2, v4

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getHeight()I

    move-result v4

    .line 145
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    int-to-float v7, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 149
    div-int/lit8 v7, v4, 0x5

    int-to-float v7, v7

    cmpl-float v2, v2, v7

    if-lez v2, :cond_8

    .line 151
    int-to-float v0, v4

    div-float v2, v6, v0

    .line 152
    int-to-float v0, v4

    move v4, v2

    move v2, v0

    move v0, v1

    .line 162
    :goto_3
    sub-float v4, v5, v4

    const/high16 v6, 0x43480000    # 200.0f

    mul-float/2addr v4, v6

    float-to-int v4, v4

    int-to-long v6, v4

    .line 163
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    if-eqz v0, :cond_6

    move v3, v5

    :cond_6
    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lgat;

    invoke-direct {v3, p0, v0}, Lgat;-><init>(Lcom/google/android/apps/plus/views/ReadNotificationListView;Z)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 164
    iput v8, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    goto/16 :goto_1

    .line 156
    :cond_8
    int-to-float v2, v4

    div-float v2, v6, v2

    sub-float v2, v5, v2

    move v4, v2

    move v2, v3

    .line 158
    goto :goto_3

    .line 166
    :cond_9
    iput v8, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->b:F

    .line 167
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setVisibility(I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 78
    invoke-super {p0, p1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 83
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 84
    if-nez p1, :cond_2

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 92
    :cond_0
    :goto_1
    return-void

    .line 83
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ReadNotificationListView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    goto :goto_1
.end method
