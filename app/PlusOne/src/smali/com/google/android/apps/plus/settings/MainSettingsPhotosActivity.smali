.class public Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lizi;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Lloa;-><init>()V

    .line 34
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->x:Llnh;

    .line 35
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 38
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 39
    new-instance v0, Lfkt;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfkt;-><init>(Los;Llqr;)V

    .line 46
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->x:Llnh;

    .line 47
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 48
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 49
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 96
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 97
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 98
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->startActivity(Landroid/content/Intent;)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->finish()V

    .line 101
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->b(I)V

    .line 93
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->x:Llnh;

    const-class v1, Lizi;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->x:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 62
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 75
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 77
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_settings"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 78
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->b(I)V

    .line 88
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f04007a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/MainSettingsPhotosActivity;->setContentView(I)V

    .line 55
    return-void
.end method
