.class public Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lcis;
.implements Ldxb;
.implements Lhmm;


# instance fields
.field private g:Landroid/graphics/Bitmap;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Llon;-><init>()V

    .line 51
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->e:Llnh;

    .line 52
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 53
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 55
    new-instance v0, Lhmf;

    iget-object v1, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->f:Llqc;

    invoke-direct {v0, v1}, Lhmf;-><init>(Llqr;)V

    .line 56
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->h:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 110
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 111
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->g:Landroid/graphics/Bitmap;

    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v2, v1}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->i()Lcip;

    move-result-object v1

    .line 115
    invoke-virtual {v1, v0}, Lcip;->a(Landroid/graphics/Bitmap;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->h:Z

    goto :goto_0
.end method

.method private i()Lcip;
    .locals 2

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->f()Lae;

    move-result-object v0

    .line 261
    const-string v1, "CropExternalFragment"

    .line 262
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcip;

    return-object v0
.end method


# virtual methods
.method public a(ILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->i()Lcip;

    move-result-object v0

    .line 179
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcip;->c(I)V

    .line 181
    if-eqz p2, :cond_0

    .line 182
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->setResult(ILandroid/content/Intent;)V

    .line 186
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->finish()V

    .line 187
    return-void

    .line 184
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->setResult(I)V

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->e:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->e:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 63
    return-void
.end method

.method public a(Ldxa;)V
    .locals 3

    .prologue
    .line 153
    if-nez p1, :cond_0

    .line 154
    new-instance v0, Lckz;

    invoke-direct {v0, p0}, Lckz;-><init>(Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;)V

    .line 159
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 160
    const v2, 0x7f0a00b7

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 161
    const v2, 0x7f0a00b8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 162
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 163
    const v2, 0x7f0a00bd

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 165
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 170
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-virtual {p1}, Ldxa;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->g:Landroid/graphics/Bitmap;

    .line 168
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->h()V

    goto :goto_0
.end method

.method public a([B)Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x1

    return v0
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->m:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 233
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 238
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->i()Lcip;

    move-result-object v0

    .line 239
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcip;->c(I)V

    .line 240
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->j()Ldxa;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldxc;->a(Ldxa;)V

    goto :goto_0
.end method

.method public j()Ldxa;
    .locals 8

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->i()Lcip;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Lcip;->a()Landroid/graphics/RectF;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->g:Landroid/graphics/Bitmap;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->g:Landroid/graphics/Bitmap;

    .line 217
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->g:Landroid/graphics/Bitmap;

    .line 218
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 219
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 220
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget-object v5, p0, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v0, v6

    .line 215
    invoke-static {v1, v2, v3, v4, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    new-instance v1, Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-direct {v1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;-><init>()V

    .line 224
    new-instance v2, Lnzi;

    invoke-direct {v2}, Lnzi;-><init>()V

    .line 225
    invoke-static {v1, v2}, Lhce;->a(Lcom/google/android/libraries/photoeditor/core/FilterChain;Lnzi;)Z

    .line 226
    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v1

    .line 228
    new-instance v2, Ldxa;

    invoke-direct {v2, v0, v1}, Ldxa;-><init>(Landroid/graphics/Bitmap;[B)V

    return-object v2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 74
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    sput-object v0, Lhan;->a:Lhao;

    .line 76
    const v0, 0x7f040182

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->setContentView(I)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 79
    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to get action bar reference"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 83
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 84
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 85
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 87
    const v1, 0x7f040034

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 89
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->i()Lcip;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcip;

    invoke-direct {v0}, Lcip;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->f()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x7f1002a3

    const-string v3, "CropExternalFragment"

    invoke-virtual {v1, v2, v0, v3}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    invoke-virtual {v1}, Lat;->b()I

    :cond_1
    invoke-virtual {v0, p0}, Lcip;->a(Lcis;)V

    .line 91
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 92
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, p1, p0, v1, p0}, Ldxc;->a(Landroid/os/Bundle;Landroid/app/Activity;Landroid/content/Intent;Ldxb;)V

    .line 94
    :cond_2
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Llon;->onDestroy()V

    .line 143
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {v0}, Ldxc;->e()V

    .line 146
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Llon;->onPause()V

    .line 125
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {v0}, Ldxc;->a()V

    .line 128
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Llon;->onResume()V

    .line 100
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {v0}, Ldxc;->d()V

    .line 104
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PlusCropActivity;->h()V

    .line 105
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0, p1}, Llon;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 134
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {v0, p1}, Ldxc;->a(Landroid/os/Bundle;)V

    .line 137
    :cond_0
    return-void
.end method
