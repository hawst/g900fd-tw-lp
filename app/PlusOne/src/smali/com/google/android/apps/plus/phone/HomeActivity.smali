.class public final Lcom/google/android/apps/plus/phone/HomeActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lbc;
.implements Lesa;
.implements Lheg;
.implements Lhep;
.implements Lhjj;
.implements Lhjn;
.implements Lhmq;
.implements Ljfl;
.implements Ljfn;
.implements Ljgf;
.implements Lmy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lloa;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lesa;",
        "Lheg;",
        "Lhep;",
        "Lhjj;",
        "Lhjn;",
        "Lhmq;",
        "Ljfl;",
        "Ljfn;",
        "Ljgf;",
        "Lmy;"
    }
.end annotation


# instance fields
.field private final e:Livx;

.field private f:Lhsu;

.field private g:Lhjf;

.field private h:Ljmc;

.field private i:Lhei;

.field private j:Lhkr;

.field private k:Leyj;

.field private l:Lhxb;

.field private m:Lcom/google/android/apps/plus/phone/HostLayout;

.field private n:Ljfs;

.field private o:Ljit;

.field private p:Lcom/google/android/apps/plus/views/EsDrawerLayout;

.field private q:Landroid/view/View;

.field private r:Lfcl;

.field private s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

.field private u:Landroid/os/Bundle;

.field private v:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private w:I

.field private z:Ljgi;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 124
    invoke-direct {p0}, Lloa;-><init>()V

    .line 158
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    .line 159
    invoke-virtual {v0, v1}, Livx;->a(Llnh;)Livx;

    move-result-object v0

    const-string v1, "active-plus-account"

    .line 160
    invoke-virtual {v0, v1}, Livx;->a(Ljava/lang/String;)Livx;

    move-result-object v0

    .line 161
    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    .line 163
    new-instance v0, Lhsu;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->y:Llqc;

    invoke-direct {v0, v1}, Lhsu;-><init>(Llqr;)V

    new-instance v1, Leyd;

    invoke-direct {v1, p0}, Leyd;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;)V

    .line 164
    invoke-virtual {v0, v1}, Lhsu;->a(Lhsw;)Lhsu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->f:Lhsu;

    .line 176
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    .line 178
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->g:Lhjf;

    .line 180
    new-instance v0, Ljmc;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Ljmc;-><init>(Lz;Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->h:Ljmc;

    .line 185
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 187
    new-instance v0, Lfcs;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfcs;-><init>(Landroid/app/Activity;Llqr;)V

    const/4 v1, 0x0

    .line 188
    invoke-virtual {v0, v1}, Lfcs;->a(I)Lfcs;

    move-result-object v0

    const-wide/32 v2, 0xa4cb800

    .line 189
    invoke-virtual {v0, v2, v3}, Lfcs;->a(J)Lfcs;

    .line 191
    new-instance v0, Ldxn;

    new-instance v1, Leye;

    invoke-direct {v1, p0}, Leye;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;)V

    invoke-direct {v0, v1}, Ldxn;-><init>(Ldxm;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    .line 207
    invoke-virtual {v0, v1}, Ldxn;->a(Llnh;)V

    .line 209
    new-instance v0, Llhe;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llhe;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    .line 210
    invoke-virtual {v0, v1}, Llhe;->a(Llnh;)Llhe;

    .line 213
    new-instance v0, Leyj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Leyj;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->k:Leyj;

    .line 231
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->v:Landroid/util/SparseArray;

    .line 560
    new-instance v0, Leyf;

    invoke-direct {v0, p0}, Leyf;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->z:Ljgi;

    .line 1115
    return-void
.end method

.method private a(ILandroid/os/Bundle;Lx;Z)Lu;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 654
    .line 656
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    .line 706
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->k:Leyj;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Leyj;->a(Z)V

    .line 715
    return-object v0

    .line 658
    :pswitch_1
    new-instance v0, Lehh;

    invoke-direct {v0}, Lehh;-><init>()V

    .line 659
    invoke-virtual {v0, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 661
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v2, v0, v1, p4}, Lcom/google/android/apps/plus/phone/HostLayout;->a(Lu;Lx;Z)V

    goto :goto_0

    .line 666
    :pswitch_2
    new-instance v0, Ledo;

    invoke-direct {v0}, Ledo;-><init>()V

    .line 667
    const-string v1, "refresh"

    invoke-virtual {p2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 668
    invoke-virtual {v0, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 669
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v1, v0, p3, p4}, Lcom/google/android/apps/plus/phone/HostLayout;->a(Lu;Lx;Z)V

    goto :goto_0

    .line 675
    :pswitch_3
    new-instance v0, Lkuf;

    invoke-direct {v0}, Lkuf;-><init>()V

    .line 676
    const-string v2, "refresh"

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 677
    invoke-virtual {v0, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 678
    const-string v2, "squares"

    const-wide/high16 v4, -0x8000000000000000L

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/apps/plus/phone/HomeActivity;->a(Ljava/lang/String;J)Z

    .line 680
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v2, v0, v1, p4}, Lcom/google/android/apps/plus/phone/HostLayout;->a(Lu;Lx;Z)V

    goto :goto_0

    .line 685
    :pswitch_4
    new-instance v0, Lkul;

    invoke-direct {v0}, Lkul;-><init>()V

    .line 686
    invoke-virtual {v0, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 688
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v2, v0, v1, p4}, Lcom/google/android/apps/plus/phone/HostLayout;->a(Lu;Lx;Z)V

    goto :goto_0

    .line 693
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lgcs;

    .line 694
    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcs;

    .line 695
    invoke-interface {v0}, Lgcs;->a()Lu;

    move-result-object v0

    .line 696
    invoke-virtual {v0, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 698
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v1, v0, p3, p4}, Lcom/google/android/apps/plus/phone/HostLayout;->a(Lu;Lx;Z)V

    goto :goto_0

    .line 703
    :pswitch_6
    new-instance v0, Letn;

    invoke-direct {v0}, Letn;-><init>()V

    .line 704
    invoke-virtual {v0, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 705
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v1, v0, p3, p4}, Lcom/google/android/apps/plus/phone/HostLayout;->a(Lu;Lx;Z)V

    goto :goto_0

    .line 656
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/HomeActivity;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/HostLayout;->b()Lu;

    move-result-object v1

    instance-of v1, v1, Lehh;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostLayout;->b()Lu;

    move-result-object v0

    check-cast v0, Lehh;

    invoke-virtual {v0}, Lehh;->aJ()Z

    move-result v0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    return-object v0
.end method

.method private b(ILandroid/os/Bundle;)Lu;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->d()I

    move-result v1

    .line 580
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v0, v3, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 583
    invoke-static {p0, v1, v2}, Leyq;->f(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    move-object v0, v2

    .line 639
    :goto_0
    return-object v0

    .line 588
    :cond_0
    const/4 v0, 0x6

    if-ne p1, v0, :cond_3

    .line 590
    invoke-static {p0}, Lfuy;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 591
    invoke-static {p0}, Lfuy;->e(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 599
    :goto_1
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    invoke-direct {v3, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->ez:Lhmv;

    .line 602
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 603
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->F_()Lhmw;

    move-result-object v4

    invoke-virtual {v3, v4}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v3

    .line 600
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 605
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    move-object v0, v2

    .line 606
    goto :goto_0

    .line 592
    :cond_1
    invoke-static {p0, v1}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 593
    invoke-static {p0}, Leyq;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 594
    const-string v3, "photos_promo_direct_to_home"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 595
    const-string v3, "account_id"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v1, v0

    goto :goto_1

    .line 597
    :cond_2
    invoke-static {p0, v1}, Leyq;->j(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 609
    :cond_3
    const/4 v0, 0x5

    if-ne p1, v0, :cond_4

    .line 610
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/PeopleListActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account_id"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "people_view_type"

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "query"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_for_unified_search"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    move-object v0, v2

    .line 611
    goto :goto_0

    .line 614
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    .line 615
    invoke-static {p0, v1}, Leyq;->i(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 616
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    move-object v0, v2

    .line 617
    goto/16 :goto_0

    .line 620
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v1, "destination"

    .line 621
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, p1, :cond_6

    const/16 v0, 0x9

    if-eq p1, v0, :cond_6

    const/16 v0, 0xa

    if-eq p1, v0, :cond_6

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostLayout;->b()Lu;

    move-result-object v0

    goto/16 :goto_0

    .line 629
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->m()V

    .line 631
    if-nez p2, :cond_7

    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    :cond_7
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v1, "account_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 633
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v1, "destination"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 635
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->v:Landroid/util/SparseArray;

    .line 636
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx;

    .line 635
    invoke-direct {p0, p1, v1, v0, v4}, Lcom/google/android/apps/plus/phone/HomeActivity;->a(ILandroid/os/Bundle;Lx;Z)Lu;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/phone/HomeActivity;)Ljfs;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->n:Ljfs;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/phone/HomeActivity;)V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->j:Lhkr;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lhkr;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v1}, Livx;->g()Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v2, Lhkr;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    invoke-virtual {v0}, Lhkr;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/phone/HomeActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->q:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/EsDrawerLayout;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->p:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/plus/phone/HomeActivity;)Ljit;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->o:Ljit;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/plus/phone/HomeActivity;)Llnh;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/plus/phone/HomeActivity;)Lhjf;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->g:Lhjf;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/plus/phone/HomeActivity;)Lhsu;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->f:Lhsu;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/plus/phone/HomeActivity;)Llnh;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/plus/phone/HomeActivity;)Llnh;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v1, "destination"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 724
    if-eq v0, v2, :cond_0

    .line 725
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->v:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/HostLayout;->c()Lx;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 728
    :cond_0
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 735
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 736
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 738
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    .line 739
    if-nez v1, :cond_0

    .line 740
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v1, "destination"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 749
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v1, "account_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 750
    return-void

    .line 742
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 743
    const-string v1, "destination"

    .line 744
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 745
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private o()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1032
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->i:Lhei;

    new-array v3, v2, [Ljava/lang/String;

    const-string v4, "logged_in"

    aput-object v4, v3, v1

    invoke-interface {v0, v3}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1033
    new-instance v3, Lhed;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->i:Lhei;

    invoke-direct {v3, v4}, Lhed;-><init>(Lhei;)V

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1034
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 1035
    if-nez v3, :cond_0

    move v0, v1

    .line 1044
    :goto_0
    return v0

    .line 1037
    :cond_0
    if-ne v3, v2, :cond_4

    .line 1038
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1039
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->i:Lhei;

    invoke-interface {v3, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 1040
    const-string v3, "is_plus_page"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "is_managed_account"

    .line 1041
    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    const-string v3, "page_count"

    .line 1042
    invoke-interface {v0, v3, v1}, Lhej;->a(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1044
    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 921
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 923
    :goto_0
    instance-of v2, v0, Lhmq;

    if-eqz v2, :cond_0

    check-cast v0, Lhmq;

    .line 924
    invoke-interface {v0}, Lhmq;->F_()Lhmw;

    move-result-object v1

    :cond_0
    return-object v1

    .line 921
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    .line 922
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostLayout;->b()Lu;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 846
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 856
    :cond_0
    :goto_0
    return-object v4

    .line 850
    :cond_1
    if-nez p1, :cond_0

    .line 851
    new-instance v0, Lhye;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    .line 852
    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    .line 851
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Leyi;->a:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 237
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 239
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhjf;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->g:Lhjf;

    .line 240
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ljgi;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->z:Ljgi;

    .line 241
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhsu;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->f:Lhsu;

    .line 242
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lhxb;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxb;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->l:Lhxb;

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->i:Lhei;

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lhkr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->j:Lhkr;

    .line 247
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 949
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->h()Loo;

    move-result-object v0

    .line 950
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 951
    invoke-virtual {v0}, Loo;->e()V

    .line 953
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->k:Leyj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leyj;->a(Z)V

    .line 954
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 939
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 940
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->h()Loo;

    move-result-object v0

    .line 941
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 942
    invoke-virtual {v0}, Loo;->e()V

    .line 945
    :cond_0
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 861
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 865
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 874
    :cond_0
    :goto_0
    return-void

    .line 869
    :cond_1
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    if-nez v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->g()Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 871
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->l:Lhxb;

    invoke-interface {v0}, Lhxb;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->w:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Ldhv;->a([B)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->w:I

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->l:Lhxb;

    invoke-interface {v1, v0}, Lhxb;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 124
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/phone/HomeActivity;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const v10, 0x7f10050f

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 776
    const v0, 0x7f1006e5

    new-instance v2, Lifa;

    invoke-direct {v2}, Lifa;-><init>()V

    invoke-interface {p1, v0, v2}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 777
    const v0, 0x7f100406

    new-instance v2, Lfkx;

    invoke-direct {v2}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v2}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 778
    const v0, 0x7f10053c

    new-instance v2, Litl;

    const-string v3, "plus_stream"

    invoke-direct {v2, v3}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v2}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 781
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v2, Lita;

    invoke-virtual {v0, v2}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->o:Ljit;

    if-eqz v0, :cond_0

    .line 787
    const v0, 0x7f100692

    .line 789
    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lie;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/actionbar/NotificationButtonView;

    .line 790
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->o:Ljit;

    invoke-interface {v2}, Ljit;->aa()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/actionbar/NotificationButtonView;->a(I)V

    .line 793
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->g()Lhej;

    move-result-object v0

    const-string v2, "is_google_plus"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794
    const v0, 0x7f10047f

    .line 796
    invoke-interface {p1, v0}, Lhjk;->c(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lie;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;

    .line 797
    iget v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->w:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->a(I)V

    .line 800
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->h()Loo;

    move-result-object v4

    .line 801
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 802
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->o()Z

    move-result v5

    const v0, 0x7f0401b2

    invoke-static {p0, v0, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    if-eqz v5, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v8

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v3, v2, v7, v6, v8}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->g()Lhej;

    move-result-object v6

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f100510

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const-string v7, "actionbar_avatar"

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setTransitionName(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const-string v7, "gaia_id"

    invoke-interface {v6, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "profile_photo_url"

    invoke-interface {v6, v8}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    if-eqz v5, :cond_5

    move-object v0, v2

    :goto_1
    new-instance v2, Lhmk;

    sget-object v7, Long;->i:Lhmn;

    invoke-direct {v2, v7}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    new-instance v2, Leyg;

    invoke-direct {v2, p0}, Leyg;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v5, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    invoke-virtual {v0}, Lfcl;->c()V

    const v0, 0x7f100176

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    invoke-virtual {v2}, Lfcl;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->a(Lhjn;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->g()Lhej;

    move-result-object v0

    const-string v2, "is_google_plus"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->o()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 803
    :cond_3
    invoke-virtual {v4, v3}, Loo;->a(Landroid/view/View;)V

    .line 804
    invoke-virtual {v4, v9}, Loo;->e(Z)V

    .line 805
    invoke-virtual {v4, v1}, Loo;->d(Z)V

    .line 811
    :goto_3
    return-void

    .line 802
    :cond_4
    const v0, 0x7f020416

    goto/16 :goto_0

    :cond_5
    move-object v0, v3

    goto :goto_1

    :cond_6
    const v0, 0x7f100511

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v2, "given_name"

    invoke-interface {v6, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v2, "display_name"

    invoke-interface {v6, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_7
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 807
    :cond_8
    invoke-virtual {v4, v6}, Loo;->a(Landroid/view/View;)V

    .line 808
    invoke-virtual {v4, v1}, Loo;->e(Z)V

    .line 809
    invoke-virtual {v4, v9}, Loo;->d(Z)V

    goto :goto_3
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 766
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 767
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 768
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 314
    invoke-super {p0, p1}, Lloa;->a(Lu;)V

    .line 316
    instance-of v0, p1, Ljfs;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 317
    check-cast v0, Ljfs;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->n:Ljfs;

    .line 319
    :cond_0
    instance-of v0, p1, Ljit;

    if-eqz v0, :cond_1

    .line 320
    check-cast p1, Ljit;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->o:Ljit;

    .line 322
    :cond_1
    return-void
.end method

.method public a(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 381
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 414
    :goto_0
    return-void

    .line 385
    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_4

    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->f()Lae;

    move-result-object v0

    const-string v2, "notification_fragment_tag"

    invoke-virtual {v0, v2}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    invoke-virtual {v0, v2}, Lat;->a(Lu;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    :cond_1
    iput-object v6, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->o:Ljit;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->v:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostLayout;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->f()Lae;

    move-result-object v0

    const-string v2, "navigation_fragment_tag"

    invoke-virtual {v0, v2}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    invoke-virtual {v0, v2}, Lat;->a(Lu;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    :cond_3
    iput-object v6, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->n:Ljfs;

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->stopService(Landroid/content/Intent;)Z

    .line 389
    :cond_4
    sget-object v0, Leyh;->a:[I

    add-int/lit8 v2, p3, -0x1

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 408
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->g:Lhjf;

    invoke-interface {v0}, Lhjf;->b()V

    goto :goto_0

    .line 391
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v2, Lieh;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v2, Ldxd;->i:Lief;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v3}, Livx;->d()I

    move-result v3

    invoke-interface {v0, v2, v3}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->f()Lae;

    move-result-object v2

    invoke-virtual {v2}, Lae;->a()Lat;

    move-result-object v3

    const-string v4, "notification_fragment_tag"

    invoke-virtual {v2, v4}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v4

    if-nez v4, :cond_6

    if-eqz v0, :cond_a

    new-instance v0, Lers;

    invoke-direct {v0}, Lers;-><init>()V

    :goto_3
    const v4, 0x7f1002fd

    const-string v5, "notification_fragment_tag"

    invoke-virtual {v3, v4, v0, v5}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    invoke-virtual {v3}, Lat;->b()I

    invoke-virtual {v2}, Lae;->b()Z

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->n()V

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->d()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->f()Lae;

    move-result-object v2

    new-instance v3, Ljfs;

    invoke-direct {v3}, Ljfs;-><init>()V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "account_id"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v3, v4}, Lu;->f(Landroid/os/Bundle;)V

    invoke-virtual {v2}, Lae;->a()Lat;

    move-result-object v0

    const v4, 0x7f1002fc

    const-string v5, "navigation_fragment_tag"

    invoke-virtual {v0, v4, v3, v5}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    invoke-virtual {v2}, Lae;->b()Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    invoke-direct {p0, v0, v2, v6, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->a(ILandroid/os/Bundle;Lx;Z)Lu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->g()Lbb;

    move-result-object v0

    invoke-virtual {v0, v1, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f0a024d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f030002

    const-string v2, "gplus_dogfood_dialog_version"

    invoke-static {p0, v0, v1, v2}, Llhr;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->h:Ljmc;

    invoke-virtual {v0}, Ljmc;->a()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->d()I

    move-result v0

    invoke-static {p0, v0}, Ldhv;->u(Landroid/content/Context;I)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/32 v4, 0x36ee80

    cmp-long v1, v2, v4

    if-ltz v1, :cond_5

    new-instance v1, Ldqj;

    invoke-direct {v1, p0, v0}, Ldqj;-><init>(Landroid/content/Context;I)V

    invoke-static {p0, v1}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto/16 :goto_2

    :cond_a
    new-instance v0, Ljlg;

    invoke-direct {v0}, Ljlg;-><init>()V

    goto/16 :goto_3

    .line 394
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->finish()V

    goto/16 :goto_1

    .line 397
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    .line 398
    invoke-virtual {v2}, Liwg;->c()Liwg;

    move-result-object v2

    const-class v3, Liwl;

    new-instance v4, Liwm;

    invoke-direct {v4}, Liwm;-><init>()V

    const v5, 0x7f0a05f4

    .line 400
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v4

    .line 401
    invoke-virtual {v4, v1}, Liwm;->a(Z)Liwm;

    move-result-object v1

    new-instance v4, Livs;

    invoke-direct {v4}, Livs;-><init>()V

    const-string v5, "logged_out"

    .line 403
    invoke-virtual {v4, v5}, Livs;->a(Ljava/lang/String;)Livs;

    move-result-object v4

    .line 402
    invoke-virtual {v1, v4}, Liwm;->a(Livq;)Liwm;

    move-result-object v1

    .line 405
    invoke-virtual {v1}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 399
    invoke-virtual {v2, v3, v1}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v1

    .line 397
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    goto/16 :goto_1

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 815
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 816
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    .line 818
    const v3, 0x7f10047f

    if-ne v0, v3, :cond_0

    .line 820
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 821
    const-string v0, "extra_people_notification_count"

    iget v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->w:I

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    invoke-direct {v4, p0, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v5, Lhmv;->bV:Lhmv;

    .line 825
    invoke-virtual {v4, v5}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    .line 826
    invoke-virtual {v4, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v3

    .line 827
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->F_()Lhmw;

    move-result-object v4

    invoke-virtual {v3, v4}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v3

    .line 823
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 829
    invoke-static {p0, v2}, Leyq;->n(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 836
    :goto_0
    return v0

    .line 831
    :cond_0
    const v2, 0x7f100692

    if-ne v0, v2, :cond_1

    .line 832
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->k:Leyj;

    invoke-virtual {v0, v1}, Leyj;->a(Z)V

    move v0, v1

    .line 833
    goto :goto_0

    .line 836
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;J)Z
    .locals 2

    .prologue
    .line 644
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->f()Lae;

    move-result-object v0

    const-string v1, "navigation_fragment_tag"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    .line 645
    if-eqz v0, :cond_0

    .line 646
    check-cast v0, Ljfs;

    .line 647
    invoke-virtual {v0, p1, p2, p3}, Ljfs;->a(Ljava/lang/String;J)Z

    move-result v0

    .line 649
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ax_()V
    .locals 1

    .prologue
    .line 934
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->g:Lhjf;

    invoke-interface {v0}, Lhjf;->b()V

    .line 935
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 930
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 958
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->k:Leyj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leyj;->a(Z)V

    .line 959
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 772
    return-void
.end method

.method public b(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 517
    if-nez p1, :cond_0

    .line 534
    :goto_0
    return v0

    .line 522
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/HostLayout;->b()Lu;

    move-result-object v1

    .line 524
    const-string v2, "destination"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 525
    const-string v1, "destination"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 526
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 525
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(ILandroid/os/Bundle;)Lu;

    move-result-object v0

    .line 527
    const/4 v1, 0x1

    .line 530
    :goto_1
    if-eqz v0, :cond_1

    instance-of v2, v0, Ljfn;

    if-eqz v2, :cond_1

    .line 531
    check-cast v0, Ljfn;

    invoke-interface {v0, p1}, Ljfn;->b(Landroid/content/Intent;)Z

    move-result v0

    or-int/2addr v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_1
.end method

.method public c(I)Z
    .locals 3

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    invoke-virtual {v0, p1}, Lfcl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1071
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->a(Lhjn;)V

    .line 1072
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    .line 1073
    invoke-virtual {v1}, Lfcl;->d()I

    move-result v1

    .line 1072
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    .line 1074
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->a(Lhjn;)V

    .line 1075
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Lixk;

    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    invoke-virtual {v1}, Liwg;->c()Liwg;

    move-result-object v1

    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 1080
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1077
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    invoke-virtual {v0, p1}, Lfcl;->b(I)I

    move-result v0

    .line 1078
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    invoke-virtual {v2, v0}, Liwg;->a(I)Liwg;

    move-result-object v0

    invoke-virtual {v0}, Liwg;->c()Liwg;

    move-result-object v0

    invoke-virtual {v1, v0}, Livx;->a(Liwg;)V

    goto :goto_0
.end method

.method public c(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 539
    if-nez p1, :cond_0

    .line 557
    :goto_0
    return v0

    .line 544
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/HostLayout;->b()Lu;

    move-result-object v1

    .line 546
    const-string v2, "destination"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 547
    const-string v1, "destination"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 548
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 547
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(ILandroid/os/Bundle;)Lu;

    move-result-object v0

    .line 549
    const/4 v1, 0x1

    .line 552
    :goto_1
    if-eqz v0, :cond_1

    instance-of v2, v0, Ljfl;

    if-eqz v2, :cond_1

    .line 553
    check-cast v0, Ljfl;

    .line 554
    invoke-interface {v0, p1}, Ljfl;->c(Landroid/content/Intent;)Z

    move-result v0

    or-int/2addr v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_1
.end method

.method public l()V
    .locals 2

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    invoke-virtual {v0}, Lfcl;->c()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->t:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    invoke-virtual {v1}, Lfcl;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    .line 1086
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 251
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lhnt;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnt;

    .line 253
    if-eqz v0, :cond_0

    .line 254
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lhnt;->c(J)V

    .line 257
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 291
    :cond_1
    :goto_0
    return-void

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lkzl;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lkzl;->b(Z)V

    .line 262
    const/4 v0, 0x0

    invoke-static {v0}, Lgci;->a(Z)V

    .line 264
    const v0, 0x7f0400c9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->setContentView(I)V

    .line 266
    const v0, 0x7f1002fa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/HostLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->m:Lcom/google/android/apps/plus/phone/HostLayout;

    .line 268
    const v0, 0x7f1002fb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->p:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->p:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(Lmy;)V

    .line 270
    const v0, 0x7f1002fd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->q:Landroid/view/View;

    .line 271
    new-instance v0, Lfcl;

    invoke-direct {v0, p0}, Lfcl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->r:Lfcl;

    .line 273
    if-eqz p1, :cond_3

    .line 274
    const-string v0, "destination"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    .line 281
    :goto_1
    if-nez p1, :cond_1

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    .line 283
    invoke-virtual {v1}, Liwg;->a()Liwg;

    move-result-object v1

    .line 284
    invoke-virtual {v1}, Liwg;->d()Liwg;

    move-result-object v1

    const-class v2, Liwl;

    new-instance v3, Liwm;

    invoke-direct {v3}, Liwm;-><init>()V

    const v4, 0x7f0a05f4

    .line 286
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v3

    .line 287
    invoke-virtual {v3}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 285
    invoke-virtual {v1, v2, v3}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v1

    .line 282
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    goto :goto_0

    .line 278
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->n()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lkdv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->p()V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->x:Llnh;

    const-class v1, Lkzl;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lkzl;->b(Z)V

    .line 308
    const/4 v0, 0x1

    invoke-static {v0}, Lgci;->a(Z)V

    .line 309
    invoke-super {p0}, Lloa;->onDestroy()V

    .line 310
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v11, -0x1

    .line 344
    invoke-super {p0, p1}, Lloa;->onNewIntent(Landroid/content/Intent;)V

    .line 346
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 348
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    if-eqz v3, :cond_2

    .line 349
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v4, "destination"

    invoke-virtual {v3, v4, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 350
    const-string v4, "destination"

    invoke-virtual {v0, v4, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 351
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v6, "square_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 352
    const-string v6, "square_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 353
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v8, "stream_id"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 354
    const-string v8, "stream_id"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 355
    iget-object v9, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    const-string v10, "clx_id"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 356
    const-string v10, "clx_id"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 357
    if-eq v3, v11, :cond_5

    if-eq v4, v11, :cond_5

    .line 358
    if-eq v3, v4, :cond_4

    move v0, v1

    .line 360
    :goto_0
    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 361
    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 362
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    or-int/2addr v2, v0

    .line 365
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 367
    if-eqz v2, :cond_3

    .line 368
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    .line 371
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    .line 372
    invoke-virtual {v1}, Liwg;->a()Liwg;

    move-result-object v1

    .line 373
    invoke-virtual {v1}, Liwg;->d()Liwg;

    move-result-object v1

    .line 371
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 375
    return-void

    :cond_4
    move v0, v2

    .line 358
    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 337
    invoke-super {p0}, Lloa;->onPause()V

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->i:Lhei;

    invoke-interface {v0, p0}, Lhei;->b(Lhep;)V

    .line 339
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 326
    invoke-super {p0}, Lloa;->onResume()V

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    :goto_0
    return-void

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->g:Lhjf;

    invoke-interface {v0}, Lhjf;->b()V

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->i:Lhei;

    invoke-interface {v0, p0}, Lhei;->a(Lhep;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 295
    invoke-super {p0, p1}, Lloa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 297
    const-string v0, "destination"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->u:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 299
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->m()V

    .line 302
    return-void
.end method
