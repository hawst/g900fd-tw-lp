.class public Lcom/google/android/apps/plus/views/BarGraphListView;
.super Landroid/widget/ListView;
.source "PG"


# static fields
.field private static a:Z

.field private static b:Landroid/text/TextPaint;

.field private static c:Landroid/text/TextPaint;

.field private static d:Landroid/graphics/Paint;

.field private static e:Landroid/graphics/Paint;

.field private static f:I

.field private static g:I

.field private static h:I


# instance fields
.field private i:[Lfwj;

.field private final j:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lfwj;",
            ">;"
        }
    .end annotation
.end field

.field private k:J

.field private l:J

.field private m:Ljava/lang/String;

.field private final n:Lfwi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/BarGraphListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/BarGraphListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const v5, 0x7f0d024d

    const v4, 0x7f0d024c

    const/4 v3, 0x1

    .line 107
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 83
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->j:Ljava/util/HashSet;

    .line 96
    new-instance v0, Lfwi;

    invoke-direct {v0, p0}, Lfwi;-><init>(Lcom/google/android/apps/plus/views/BarGraphListView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->n:Lfwi;

    .line 109
    sget-boolean v0, Lcom/google/android/apps/plus/views/BarGraphListView;->a:Z

    if-nez v0, :cond_0

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 112
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 113
    sput-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->b:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 114
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->b:Landroid/text/TextPaint;

    const v2, 0x7f0b02f5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 115
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->b:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 116
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->b:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 117
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->b:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 119
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 120
    sput-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->c:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 121
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->c:Landroid/text/TextPaint;

    const v2, 0x7f0b02f6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 122
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->c:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 123
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->c:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 124
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->c:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 126
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 127
    sput-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->d:Landroid/graphics/Paint;

    const v2, 0x7f0b02f7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 130
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 131
    sput-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->e:Landroid/graphics/Paint;

    const v2, 0x7f0b02f8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 132
    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphListView;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 134
    const v1, 0x7f0d02e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/BarGraphListView;->f:I

    .line 135
    const v1, 0x7f0d02e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/BarGraphListView;->g:I

    .line 136
    const v1, 0x7f0d02ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/google/android/apps/plus/views/BarGraphListView;->h:I

    .line 138
    sput-boolean v3, Lcom/google/android/apps/plus/views/BarGraphListView;->a:Z

    .line 141
    :cond_0
    new-instance v0, Lfwf;

    invoke-direct {v0}, Lfwf;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->n:Lfwi;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 150
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/BarGraphListView;)J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->k:J

    return-wide v0
.end method

.method public static synthetic b()Landroid/text/TextPaint;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/plus/views/BarGraphListView;->b:Landroid/text/TextPaint;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/BarGraphListView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->m:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic c()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/google/android/apps/plus/views/BarGraphListView;->g:I

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/views/BarGraphListView;)J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->l:J

    return-wide v0
.end method

.method public static synthetic d()Landroid/text/TextPaint;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/plus/views/BarGraphListView;->c:Landroid/text/TextPaint;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/views/BarGraphListView;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->j:Ljava/util/HashSet;

    return-object v0
.end method

.method public static synthetic e()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/google/android/apps/plus/views/BarGraphListView;->f:I

    return v0
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/views/BarGraphListView;)[Lfwj;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->i:[Lfwj;

    return-object v0
.end method

.method public static synthetic f()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/google/android/apps/plus/views/BarGraphListView;->h:I

    return v0
.end method

.method public static synthetic g()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/plus/views/BarGraphListView;->e:Landroid/graphics/Paint;

    return-object v0
.end method

.method public static synthetic h()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/plus/views/BarGraphListView;->d:Landroid/graphics/Paint;

    return-object v0
.end method


# virtual methods
.method public a()Lfwi;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->n:Lfwi;

    return-object v0
.end method

.method public a([Lfwj;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide/16 v6, 0x0

    .line 160
    if-nez p1, :cond_0

    new-array p1, v0, [Lfwj;

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->i:[Lfwj;

    .line 162
    iget-object v1, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->j:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 163
    iput-object p2, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->m:Ljava/lang/String;

    .line 164
    iput-wide v6, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->k:J

    .line 165
    iput-wide v6, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->l:J

    .line 167
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->i:[Lfwj;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->i:[Lfwj;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->i:[Lfwj;

    new-instance v2, Lfwj;

    const-string v3, ""

    invoke-direct {v2, v3, v6, v7}, Lfwj;-><init>(Ljava/lang/String;J)V

    aput-object v2, v1, v0

    .line 167
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->k:J

    iget-object v1, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->i:[Lfwj;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lfwj;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->k:J

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->i:[Lfwj;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lfwj;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->l:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->l:J

    goto :goto_1

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphListView;->n:Lfwi;

    invoke-virtual {v0}, Lfwi;->notifyDataSetChanged()V

    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphListView;->requestLayout()V

    .line 181
    return-void
.end method

.method public synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphListView;->a()Lfwi;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphListView;->a()Lfwi;

    move-result-object v0

    return-object v0
.end method
