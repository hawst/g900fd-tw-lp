.class public Lcom/google/android/apps/plus/views/FastScrollContainer;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private a:Lfyk;

.field private b:Lcom/google/android/apps/plus/views/FastScrollListView;

.field private c:Landroid/widget/TextView;

.field private d:Lfyl;

.field private e:Landroid/widget/AbsListView$OnScrollListener;

.field private final f:Lfyi;

.field private final g:Lfyh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 48
    sget-object v0, Lfyi;->b:Lfyi;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->f:Lfyi;

    .line 50
    sget-object v0, Lfyh;->b:Lfyh;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->g:Lfyh;

    .line 55
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    sget-object v0, Lfyi;->b:Lfyi;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->f:Lfyi;

    .line 50
    sget-object v0, Lfyh;->b:Lfyh;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->g:Lfyh;

    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    sget-object v0, Lfyi;->b:Lfyi;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->f:Lfyi;

    .line 50
    sget-object v0, Lfyh;->b:Lfyh;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->g:Lfyh;

    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 69
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/FastScrollContainer;->setWillNotDraw(Z)V

    .line 72
    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 74
    const/16 v0, 0x27

    invoke-static {p1, p2, p3, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->c:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->c:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->c:Landroid/widget/TextView;

    const v2, 0x7f0d0382

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0d0383

    .line 82
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 81
    invoke-virtual {v1, v2, v0, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0}, Lfyk;->b()V

    .line 99
    return-void
.end method

.method public a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->e:Landroid/widget/AbsListView$OnScrollListener;

    .line 95
    return-void
.end method

.method public a(Lfyl;)V
    .locals 1

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->d:Lfyl;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0, p1}, Lfyk;->a(Lfyl;)V

    .line 91
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0}, Lfyk;->c()V

    .line 103
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0}, Lfyk;->f()V

    .line 110
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0, p1}, Lfyk;->a(Landroid/graphics/Canvas;)V

    .line 127
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 151
    instance-of v0, p2, Lcom/google/android/apps/plus/views/FastScrollListView;

    if-eqz v0, :cond_1

    .line 152
    check-cast p2, Lcom/google/android/apps/plus/views/FastScrollListView;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->b:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 158
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->addView(Landroid/view/View;)V

    .line 159
    iget-object v5, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->f:Lfyi;

    .line 160
    iget-object v6, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->g:Lfyh;

    .line 162
    new-instance v0, Lfyk;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->b:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 163
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->b:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->c:Landroid/widget/TextView;

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lfyk;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/widget/TextView;Landroid/view/View;Lfyi;Lfyh;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->b:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->a(Lfyk;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->d:Lfyl;

    invoke-virtual {v0, v1}, Lfyk;->a(Lfyl;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->b:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/FastScrollListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 168
    :cond_0
    return-void

    .line 153
    :cond_1
    instance-of v0, p2, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

    if-eqz v0, :cond_0

    .line 154
    const v0, 0x7f100306

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/FastScrollListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->b:Lcom/google/android/apps/plus/views/FastScrollListView;

    goto :goto_0
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->b:Lcom/google/android/apps/plus/views/FastScrollListView;

    if-ne p2, v0, :cond_0

    .line 173
    iput-object v1, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->b:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->b()V

    .line 175
    iput-object v1, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    .line 177
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0, p1}, Lfyk;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    if-nez v0, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0, p1, p2, p3, p4}, Lfyk;->a(Landroid/widget/AbsListView;III)V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->e:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->e:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->e:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->e:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 134
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/plus/views/FastScrollContainer;->a:Lfyk;

    invoke-virtual {v0, p1}, Lfyk;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 187
    if-eqz v0, :cond_0

    .line 188
    const/4 v0, 0x1

    .line 191
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
