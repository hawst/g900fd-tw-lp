.class public Lcom/google/android/apps/plus/phone/EditEventActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Leae;
.implements Lhmq;


# instance fields
.field private e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Lloa;-><init>()V

    .line 30
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 31
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->x:Llnh;

    .line 32
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 33
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/EditEventActivity;)Lcom/google/android/apps/plus/fragments/EditEventFragment;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lhmw;->C:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 115
    return-void
.end method

.method public a(Lidh;)V
    .locals 0

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->finish()V

    .line 104
    return-void
.end method

.method public a(Lu;)V
    .locals 4

    .prologue
    .line 83
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_0

    .line 84
    check-cast p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Leae;)V

    .line 88
    :cond_0
    return-void
.end method

.method public aw_()V
    .locals 0

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->finish()V

    .line 109
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X()V

    .line 95
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->f:Ljava/lang/String;

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "owner_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->g:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "auth_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->h:Ljava/lang/String;

    .line 48
    const v0, 0x7f040122

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->setContentView(I)V

    .line 51
    const v0, 0x7f100178

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    new-instance v1, Lews;

    invoke-direct {v1, p0}, Lews;-><init>(Lcom/google/android/apps/plus/phone/EditEventActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    :cond_0
    const v0, 0x7f1003a7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageTextButton;

    .line 66
    if-eqz v0, :cond_1

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0579

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageTextButton;->a(Ljava/lang/String;)V

    .line 70
    new-instance v1, Lewt;

    invoke-direct {v1, p0}, Lewt;-><init>(Lcom/google/android/apps/plus/phone/EditEventActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :cond_1
    return-void
.end method
