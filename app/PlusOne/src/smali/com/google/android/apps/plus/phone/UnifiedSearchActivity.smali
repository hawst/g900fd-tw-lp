.class public Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 37
    invoke-direct {p0}, Lloa;-><init>()V

    .line 42
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 44
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->x:Llnh;

    .line 45
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 47
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->x:Llnh;

    .line 48
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 49
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 51
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 52
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 54
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->x:Llnh;

    .line 55
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 57
    new-instance v0, Ldxn;

    new-instance v1, Lfdx;

    invoke-direct {v1, p0}, Lfdx;-><init>(Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;)V

    invoke-direct {v0, v1}, Ldxn;-><init>(Ldxm;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->x:Llnh;

    .line 72
    invoke-virtual {v0, v1}, Ldxn;->a(Llnh;)V

    .line 75
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lhmw;->M:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 115
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 101
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 102
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 103
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 104
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 92
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 93
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 82
    if-nez p1, :cond_0

    .line 83
    new-instance v0, Leiw;

    invoke-direct {v0}, Leiw;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 86
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/UnifiedSearchActivity;->setContentView(I)V

    .line 87
    return-void
.end method
