.class public Lcom/google/android/apps/plus/views/UpdateItemCardFactoryExtension;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkzj;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# static fields
.field private static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Integer;

    .line 43
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 44
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 45
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    .line 46
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    .line 47
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const/4 v3, 0x5

    .line 48
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const/4 v3, 0x6

    .line 49
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const/4 v3, 0x7

    .line 50
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const/16 v3, 0x8

    .line 51
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const/16 v3, 0x9

    .line 52
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const/16 v3, 0xa

    .line 53
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const/16 v3, 0xb

    .line 54
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const/16 v3, 0xc

    .line 55
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const/16 v3, 0xd

    .line 56
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const/16 v3, 0xe

    .line 57
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 42
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/apps/plus/views/UpdateItemCardFactoryExtension;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;J)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    .line 117
    const-wide/16 v2, 0x100

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 118
    const/4 v0, 0x5

    .line 152
    :cond_0
    :goto_0
    return v0

    .line 119
    :cond_1
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 120
    const/16 v0, 0xa

    goto :goto_0

    .line 121
    :cond_2
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 122
    const/16 v0, 0xb

    goto :goto_0

    .line 123
    :cond_3
    const-wide/16 v2, 0x200

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 124
    const/4 v0, 0x4

    goto :goto_0

    .line 125
    :cond_4
    const-wide/16 v2, 0x400

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 126
    const/4 v0, 0x3

    goto :goto_0

    .line 127
    :cond_5
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 128
    const/16 v0, 0x8

    goto :goto_0

    .line 129
    :cond_6
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 130
    const/4 v0, 0x6

    goto :goto_0

    .line 131
    :cond_7
    const-wide/16 v2, 0x804

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 134
    const-wide/32 v2, 0x18000

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_8

    .line 136
    const/4 v0, 0x7

    goto :goto_0

    .line 137
    :cond_8
    const-wide/16 v2, 0x40

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 138
    const/16 v0, 0x9

    goto :goto_0

    .line 139
    :cond_9
    const-wide/16 v2, 0xa0

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_a

    .line 141
    const/4 v0, 0x2

    goto :goto_0

    .line 142
    :cond_a
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p2

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 145
    const-wide/32 v0, 0x200000

    and-long/2addr v0, p2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_b

    .line 146
    const/16 v0, 0xc

    goto :goto_0

    .line 147
    :cond_b
    const-wide/32 v0, 0x100000

    and-long/2addr v0, p2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_c

    .line 148
    const/16 v0, 0xd

    goto :goto_0

    .line 149
    :cond_c
    const-wide/32 v0, 0x400000

    and-long/2addr v0, p2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    .line 150
    const/16 v0, 0xe

    goto/16 :goto_0

    .line 152
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public synthetic a(Landroid/content/Context;Ljava/lang/String;J)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/UpdateItemCardFactoryExtension;->b(Landroid/content/Context;Ljava/lang/String;J)Lldq;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateItemCardFactoryExtension;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateItemCardFactoryExtension;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;J)Lldq;
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 63
    const-string v0, "update"

    invoke-static {p2}, Llbk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Llbk;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 64
    const-string v1, "UpdateCardFactoryExt"

    const-string v2, "Incorrect stream card factory for activity ID: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const/4 v0, 0x0

    .line 106
    :goto_2
    return-object v0

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 68
    :cond_2
    const-wide/16 v0, 0x100

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 69
    new-instance v0, Lfxl;

    invoke-direct {v0, p1}, Lfxl;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 70
    :cond_3
    const-wide/32 v0, 0x40000

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    .line 71
    new-instance v0, Lgaz;

    invoke-direct {v0, p1}, Lgaz;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 72
    :cond_4
    const-wide/32 v0, 0x80000

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 73
    new-instance v0, Lfyo;

    invoke-direct {v0, p1}, Lfyo;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 74
    :cond_5
    const-wide/16 v0, 0x200

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 75
    new-instance v0, Lfym;

    invoke-direct {v0, p1}, Lfym;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 76
    :cond_6
    const-wide/16 v0, 0x400

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    .line 77
    new-instance v0, Lgbd;

    invoke-direct {v0, p1}, Lgbd;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 78
    :cond_7
    const-wide/16 v0, 0x1000

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    .line 79
    new-instance v0, Lgao;

    invoke-direct {v0, p1}, Lgao;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 80
    :cond_8
    const-wide/16 v0, 0x804

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_9

    .line 82
    new-instance v0, Lfyn;

    invoke-direct {v0, p1}, Lfyn;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 83
    :cond_9
    const-wide/32 v0, 0x18000

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    .line 85
    new-instance v0, Lkyx;

    invoke-direct {v0, p1}, Lkyx;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 86
    :cond_a
    const-wide/32 v0, 0x200e0

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_b

    .line 90
    new-instance v0, Lfvv;

    invoke-direct {v0, p1}, Lfvv;-><init>(Landroid/content/Context;)V

    goto/16 :goto_2

    .line 91
    :cond_b
    const-wide/16 v0, 0x2000

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_c

    .line 93
    new-instance v0, Lfyn;

    invoke-direct {v0, p1}, Lfyn;-><init>(Landroid/content/Context;)V

    goto/16 :goto_2

    .line 94
    :cond_c
    const-wide/32 v0, 0x200000

    and-long/2addr v0, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_d

    .line 95
    new-instance v0, Lgar;

    invoke-direct {v0, p1}, Lgar;-><init>(Landroid/content/Context;)V

    goto/16 :goto_2

    .line 97
    :cond_d
    const-class v0, Llds;

    invoke-static {p1, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 98
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llds;

    .line 99
    invoke-interface {v0, p3, p4}, Llds;->a(J)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 100
    invoke-interface {v0, p1}, Llds;->a(Landroid/content/Context;)Lldq;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_e

    goto/16 :goto_2

    .line 106
    :cond_f
    new-instance v0, Lgbz;

    invoke-direct {v0, p1}, Lgbz;-><init>(Landroid/content/Context;)V

    goto/16 :goto_2
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    const-string v0, "update"

    return-object v0
.end method
