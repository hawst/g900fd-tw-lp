.class public Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Llln;

.field private final f:Lhee;

.field private final g:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 63
    invoke-direct {p0}, Lloa;-><init>()V

    .line 77
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 79
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->x:Llnh;

    .line 80
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 81
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 83
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 84
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 86
    new-instance v0, Lhmg;

    sget-object v1, Lond;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 89
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->x:Llnh;

    .line 90
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->e:Llln;

    .line 92
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->x:Llnh;

    .line 93
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 94
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->f:Lhee;

    .line 96
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->g:Ldie;

    .line 267
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 256
    sget-object v0, Lhmw;->ah:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 101
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 104
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    new-instance v4, Ldhg;

    invoke-direct {v4}, Ldhg;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 106
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lder;

    new-instance v2, Lder;

    invoke-direct {v2}, Lder;-><init>()V

    .line 108
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 109
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v1

    const-class v2, Lcsm;

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "cluster_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljvj;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcsb;

    invoke-direct {v0}, Lcsb;-><init>()V

    :goto_0
    invoke-virtual {v1, v2, v0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhoc;

    new-instance v2, Lhoc;

    iget-object v3, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lhoc;-><init>(Landroid/app/Activity;Llqr;)V

    .line 112
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 114
    return-void

    .line 110
    :cond_0
    new-instance v0, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    goto :goto_0
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 243
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 245
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 246
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 247
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 233
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 234
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 235
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 261
    return-void
.end method

.method public b(Lda;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "view_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 153
    if-eqz v1, :cond_6

    .line 154
    invoke-static {v1}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_0

    .line 156
    invoke-static {v1}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 157
    const-string v4, "profile"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 158
    invoke-static {p0, v2}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p1, v3}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 159
    const-string v3, "g:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v3, 0x2

    invoke-static {p0, v2, v0, v6, v3}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 176
    :cond_0
    :goto_1
    invoke-static {v1}, Ljvj;->a(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 189
    invoke-static {p0, v2}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v0

    .line 190
    invoke-virtual {v0, v1}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    .line 188
    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 197
    :goto_2
    return-void

    .line 159
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 161
    :cond_2
    const-string v4, "posts"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 162
    invoke-static {p0, v2}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p1, v3}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 163
    const-string v3, "g:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {p0, v2, v0, v6, v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 166
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->f:Lhee;

    invoke-interface {v3}, Lhee;->g()Lhej;

    move-result-object v3

    const-string v4, "gaia_id"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 167
    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 168
    if-eqz v0, :cond_5

    .line 169
    invoke-static {p0, v2}, Leyq;->j(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_1

    .line 171
    :cond_5
    invoke-static {p0, v2}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_1

    .line 178
    :pswitch_0
    invoke-static {p0, v2, v5}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_2

    .line 183
    :pswitch_1
    invoke-static {p0, v2, v5}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_2

    .line 195
    :cond_6
    invoke-static {p0, v2}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_2

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 205
    const-class v0, Ljuk;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    .line 206
    invoke-virtual {v0}, Ljuk;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 207
    invoke-super {p0, p1}, Lloa;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 226
    :goto_0
    return v0

    .line 210
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    .line 211
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 212
    packed-switch v3, :pswitch_data_0

    .line 226
    invoke-super {p0, p1}, Lloa;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 214
    :pswitch_0
    if-nez v2, :cond_1

    .line 215
    invoke-virtual {v0}, Ljuk;->f()V

    :cond_1
    move v0, v1

    .line 217
    goto :goto_0

    .line 220
    :pswitch_1
    if-nez v2, :cond_2

    .line 221
    invoke-virtual {v0}, Ljuk;->g()V

    :cond_2
    move v0, v1

    .line 223
    goto :goto_0

    .line 212
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public l()I
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x2

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 125
    if-nez p1, :cond_0

    .line 126
    new-instance v0, Ldex;

    invoke-direct {v0}, Ldex;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->g:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 129
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->setContentView(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;->e:Llln;

    new-instance v1, Ldev;

    invoke-direct {v1, p0}, Ldev;-><init>(Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;)V

    invoke-virtual {v0, v1}, Llln;->a(Lllm;)Llln;

    .line 141
    return-void
.end method
