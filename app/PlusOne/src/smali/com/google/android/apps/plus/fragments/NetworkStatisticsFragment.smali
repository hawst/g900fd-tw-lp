.class public Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;
.super Leak;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leak;",
        "Landroid/content/DialogInterface$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static R:[Ljava/lang/String;

.field private static S:[[I


# instance fields
.field private N:Lhee;

.field private O:Lcom/google/android/apps/plus/views/BarGraphListView;

.field private P:I

.field private Q:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 68
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, Lejm;->a:[Ljava/lang/String;

    aget-object v1, v1, v8

    sget-object v2, Lejm;->a:[Ljava/lang/String;

    aget-object v2, v2, v9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "+"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lejm;->a:[Ljava/lang/String;

    aget-object v1, v1, v8

    aput-object v1, v0, v7

    sget-object v1, Lejm;->a:[Ljava/lang/String;

    aget-object v1, v1, v9

    aput-object v1, v0, v8

    sget-object v1, Lejm;->a:[Ljava/lang/String;

    aget-object v1, v1, v10

    sget-object v2, Lejm;->a:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "+"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    sget-object v1, Lejm;->a:[Ljava/lang/String;

    aget-object v1, v1, v10

    aput-object v1, v0, v10

    const/4 v1, 0x5

    sget-object v2, Lejm;->a:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lejm;->a:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->R:[Ljava/lang/String;

    .line 88
    const/4 v0, 0x7

    new-array v0, v0, [[I

    new-array v1, v8, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v6

    new-array v1, v7, [I

    aput v8, v1, v6

    aput-object v1, v0, v7

    new-array v1, v7, [I

    aput v9, v1, v6

    aput-object v1, v0, v8

    new-array v1, v8, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v9

    new-array v1, v7, [I

    aput v10, v1, v6

    aput-object v1, v0, v10

    const/4 v1, 0x5

    new-array v2, v7, [I

    const/4 v3, 0x5

    aput v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [I

    const/4 v3, 0x6

    aput v3, v2, v6

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->S:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x4
        0x5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Leak;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    .line 255
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 249
    sget-object v0, Lhmw;->o:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 129
    const v0, 0x7f04011e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 131
    const v0, 0x7f10039f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/BarGraphListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->O:Lcom/google/android/apps/plus/views/BarGraphListView;

    .line 133
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->N:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-object v4

    .line 154
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 156
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->h:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->N:Lhee;

    .line 157
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 156
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    .line 158
    new-instance v0, Lhye;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->n()Lz;

    move-result-object v1

    sget-object v3, Lejm;->a:[Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->R:[Ljava/lang/String;

    iget v6, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    aget-object v5, v5, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, " DESC"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 170
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    .line 171
    new-array v5, v4, [Lfwj;

    .line 173
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move v3, v1

    .line 174
    :goto_0
    if-ge v3, v4, :cond_1

    .line 176
    sget-object v0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->S:[[I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    aget-object v0, v0, v2

    array-length v6, v0

    move v0, v1

    move v2, v1

    :goto_1
    if-ge v0, v6, :cond_0

    .line 177
    int-to-long v8, v2

    sget-object v2, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->S:[[I

    iget v7, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    aget-object v2, v2, v7

    aget v2, v2, v0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    add-long/2addr v8, v10

    long-to-int v2, v8

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 179
    :cond_0
    new-instance v0, Lfwj;

    const/4 v6, 0x1

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    int-to-long v8, v2

    invoke-direct {v0, v6, v8, v9}, Lfwj;-><init>(Ljava/lang/String;J)V

    aput-object v0, v5, v3

    .line 181
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 174
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->O:Lcom/google/android/apps/plus/views/BarGraphListView;

    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0015

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    aget-object v1, v1, v2

    .line 184
    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/plus/views/BarGraphListView;->a([Lfwj;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0, p1}, Leak;->a(Landroid/os/Bundle;)V

    .line 116
    if-eqz p1, :cond_0

    .line 117
    const-string v0, "view_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    const-string v0, "view_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    .line 119
    iget v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->Q:I

    .line 123
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->e(Z)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 125
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 34
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method protected a(Los;)V
    .locals 3

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 242
    const v1, 0x7f0f0014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 243
    invoke-virtual {p1}, Los;->h()Loo;

    move-result-object v1

    .line 244
    iget v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 245
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Leak;->aO_()V

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->a(Los;)V

    .line 146
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0, p1}, Leak;->c(Landroid/os/Bundle;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->N:Lhee;

    .line 110
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1}, Leak;->e(Landroid/os/Bundle;)V

    .line 139
    const-string v0, "view_type"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    return-void
.end method

.method public e(Landroid/view/MenuItem;)V
    .locals 4

    .prologue
    .line 198
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 199
    const v1, 0x7f1006ed

    if-ne v0, v1, :cond_1

    .line 200
    iget v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->Q:I

    .line 201
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 202
    const v1, 0x7f0a08ff

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 203
    const v1, 0x7f0f0014

    iget v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 204
    const v1, 0x7f0a0596

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 205
    const v1, 0x7f0a0597

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 206
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 207
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    const v1, 0x7f10016a

    if-ne v0, v1, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->at:Llnl;

    new-instance v1, Lejl;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->at:Llnl;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->N:Lhee;

    .line 210
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lejl;-><init>(Landroid/content/Context;I)V

    .line 209
    invoke-static {v0, v1}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 217
    packed-switch p2, :pswitch_data_0

    .line 234
    iput p2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->Q:I

    .line 238
    :goto_0
    return-void

    .line 219
    :pswitch_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->Q:I

    iget v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    if-eq v0, v1, :cond_0

    .line 220
    iget v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->Q:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->P:I

    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->a(Los;)V

    .line 224
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 229
    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
