.class public Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Lfxz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public a(Lfxz;Z)V
    .locals 2

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->e:Lfxz;

    .line 46
    if-eqz p2, :cond_0

    const/16 v0, 0x8

    .line 47
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 48
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 49
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->e:Lfxz;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->c:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->e:Lfxz;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lfxz;->b(I)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->a:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->e:Lfxz;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lfxz;->b(I)V

    goto :goto_0

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->d:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->e:Lfxz;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lfxz;->b(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 58
    const v0, 0x7f100281

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->a:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v0, 0x7f100280

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->c:Landroid/view/View;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->c:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v0, 0x7f100283

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->d:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    const v0, 0x7f100282

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->b:Landroid/view/View;

    .line 65
    return-void
.end method
