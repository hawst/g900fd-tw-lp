.class public Lcom/google/android/apps/plus/phone/NewEventActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Leae;
.implements Lhmq;


# instance fields
.field private e:Lcom/google/android/apps/plus/fragments/EditEventFragment;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Lloa;-><init>()V

    .line 35
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 36
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->x:Llnh;

    .line 37
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 38
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/NewEventActivity;)Lcom/google/android/apps/plus/fragments/EditEventFragment;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lhmw;->C:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 139
    return-void
.end method

.method public a(Lidh;)V
    .locals 3

    .prologue
    .line 101
    if-eqz p1, :cond_3

    .line 102
    invoke-virtual {p1}, Lidh;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->x:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    invoke-direct {v1, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aU:Lhmv;

    .line 105
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 103
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 109
    :cond_0
    invoke-virtual {p1}, Lidh;->m()Loyy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->x:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    invoke-direct {v1, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aS:Lhmv;

    .line 112
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 110
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 116
    :cond_1
    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v0

    .line 117
    iget-object v1, v0, Lpbj;->a:Lpbe;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lpbj;->a:Lpbe;

    iget-object v0, v0, Lpbe;->d:Ljava/lang/Boolean;

    .line 118
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->x:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    invoke-direct {v1, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aV:Lhmv;

    .line 121
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 119
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->x:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    invoke-direct {v1, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aT:Lhmv;

    .line 128
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 126
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 132
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->finish()V

    .line 133
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 76
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_0

    .line 77
    check-cast p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->c()V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Leae;)V

    .line 81
    :cond_0
    return-void
.end method

.method public aw_()V
    .locals 0

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->finish()V

    .line 97
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->e:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X()V

    .line 88
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f040122

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->setContentView(I)V

    .line 48
    const v0, 0x7f100178

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    new-instance v1, Lfau;

    invoke-direct {v1, p0}, Lfau;-><init>(Lcom/google/android/apps/plus/phone/NewEventActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    :cond_0
    const v0, 0x7f1003a7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_1

    .line 63
    new-instance v1, Lfav;

    invoke-direct {v1, p0}, Lfav;-><init>(Lcom/google/android/apps/plus/phone/NewEventActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    :cond_1
    return-void
.end method
