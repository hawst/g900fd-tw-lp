.class public Lcom/google/android/apps/plus/fragments/EditEventFragment;
.super Leak;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lbc;
.implements Lelt;
.implements Ljbb;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leak;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;",
        "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lelt;",
        "Ljbb;",
        "Llgs;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;

.field private static final O:[Ljava/lang/String;


# instance fields
.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Lidh;

.field private X:I

.field private Y:Leae;

.field private Z:Landroid/view/View;

.field private final aA:Lfhh;

.field private aB:Landroid/text/TextWatcher;

.field private aC:Landroid/text/TextWatcher;

.field private aa:Lcom/google/android/apps/plus/views/EventThemeView;

.field private ab:Landroid/widget/TextView;

.field private ac:Landroid/widget/ProgressBar;

.field private ad:Landroid/widget/EditText;

.field private ae:Landroid/widget/Button;

.field private af:Landroid/widget/Button;

.field private ag:Landroid/widget/Button;

.field private ah:Landroid/widget/Button;

.field private ai:Landroid/widget/CheckBox;

.field private aj:Landroid/view/View;

.field private ak:Landroid/widget/TextView;

.field private al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

.field private am:Ljava/lang/String;

.field private an:Landroid/view/View;

.field private ao:Landroid/view/View;

.field private ap:Ljava/lang/Integer;

.field private aq:Lely;

.field private ar:Lhgw;

.field private as:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

.field private aw:Lfds;

.field private ax:Landroid/widget/Spinner;

.field private ay:Lidk;

.field private az:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 130
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "theme_id"

    aput-object v1, v0, v2

    const-string v1, "image_url"

    aput-object v1, v0, v3

    const-string v1, "placeholder_path"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->N:[Ljava/lang/String;

    .line 140
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "event_data"

    aput-object v1, v0, v2

    const-string v1, "event_type"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->O:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Leak;-><init>()V

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    .line 1330
    new-instance v0, Leaa;

    invoke-direct {v0, p0}, Leaa;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aA:Lfhh;

    .line 1343
    new-instance v0, Leab;

    invoke-direct {v0, p0}, Leab;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aB:Landroid/text/TextWatcher;

    .line 1366
    new-instance v0, Leac;

    invoke-direct {v0, p0}, Leac;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aC:Landroid/text/TextWatcher;

    .line 1469
    return-void
.end method

.method public static synthetic Z()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->N:[Ljava/lang/String;

    return-object v0
.end method

.method private a(Loyy;)Ljava/util/TimeZone;
    .locals 2

    .prologue
    .line 247
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    iget-object v1, p1, Loyy;->c:Ljava/lang/String;

    .line 248
    invoke-virtual {v0, v1}, Lidk;->b(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 251
    :goto_0
    return-object v0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    .line 249
    invoke-virtual {v0}, Lidk;->b()Lidm;

    move-result-object v0

    invoke-virtual {v0}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;Landroid/net/Uri;Z)V
    .locals 3

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->a()Lpbl;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1194
    :cond_0
    :goto_0
    return-void

    .line 1176
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->a()Lpbl;

    move-result-object v0

    .line 1177
    iget-object v1, v0, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->e:Lpbh;

    if-nez v1, :cond_2

    .line 1178
    iget-object v1, v0, Lpbl;->l:Lpbj;

    new-instance v2, Lpbh;

    invoke-direct {v2}, Lpbh;-><init>()V

    iput-object v2, v1, Lpbj;->e:Lpbh;

    .line 1181
    :cond_2
    if-nez p4, :cond_3

    iget-object v1, v0, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->e:Lpbh;

    iget-object v1, v1, Lpbh;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->e:Lpbh;

    iget-object v1, v1, Lpbh;->a:Ljava/lang/Integer;

    .line 1182
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 1183
    :cond_3
    iput p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X:I

    .line 1184
    iget-object v0, v0, Lpbl;->l:Lpbj;

    iget-object v0, v0, Lpbj;->e:Lpbh;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lpbh;->a:Ljava/lang/Integer;

    .line 1185
    const/4 v0, 0x0

    .line 1187
    if-eqz p3, :cond_4

    .line 1188
    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 1189
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap()V

    .line 1192
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aa:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ao()V

    return-void
.end method

.method private a(Lhmv;)V
    .locals 4

    .prologue
    .line 1552
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v1

    .line 1553
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 1554
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 1556
    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1554
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1559
    :cond_0
    return-void
.end method

.method private a(Ljava/util/Calendar;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 905
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 906
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    .line 908
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 909
    if-nez v0, :cond_0

    .line 910
    const-string v1, "EditEventFragment"

    const-string v6, "Missing start time in event "

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    new-instance v0, Loyy;

    invoke-direct {v0}, Loyy;-><init>()V

    .line 912
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Loyy;->b:Ljava/lang/Long;

    .line 914
    :cond_0
    iget-object v1, v0, Loyy;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    move v1, v2

    .line 919
    :goto_1
    iget-object v6, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-nez v6, :cond_1

    if-nez v1, :cond_2

    .line 920
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Loyy;->b:Ljava/lang/Long;

    .line 921
    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Loyy;->c:Ljava/lang/String;

    .line 922
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1, v0}, Lidh;->a(Loyy;)V

    .line 925
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->am()V

    .line 927
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->T:Z

    .line 929
    :cond_2
    return-void

    .line 910
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 914
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/EditEventFragment;Z)Z
    .locals 0

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->T:Z

    return p1
.end method

.method public static synthetic aa()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->O:[Ljava/lang/String;

    return-object v0
.end method

.method private af()J
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/4 v2, 0x0

    .line 259
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 260
    const/16 v1, 0x5a

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    .line 261
    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 262
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 263
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 265
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private ag()Z
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h()Lhgw;

    move-result-object v0

    .line 393
    invoke-virtual {v0}, Lhgw;->h()I

    move-result v1

    invoke-virtual {v0}, Lhgw;->g()I

    move-result v0

    add-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ah()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 480
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-nez v3, :cond_0

    .line 513
    :goto_0
    return-void

    .line 484
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v3}, Lidh;->g()I

    move-result v3

    if-ne v3, v2, :cond_3

    move v3, v2

    .line 485
    :goto_1
    if-eqz v3, :cond_4

    .line 486
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Z:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 487
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 488
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ao:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 495
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    if-eqz v4, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setVisibility(I)V

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ad:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v3}, Lidh;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->i()Lpbj;

    move-result-object v0

    .line 500
    if-eqz v0, :cond_5

    iget-object v3, v0, Lpbj;->b:Lpbd;

    if-eqz v3, :cond_5

    iget-object v3, v0, Lpbj;->b:Lpbd;

    iget-object v3, v3, Lpbd;->a:Ljava/lang/String;

    .line 501
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 502
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->as:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v4, v0, Lpbj;->b:Lpbd;

    iget-object v4, v4, Lpbd;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Ljava/lang/String;)V

    .line 507
    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ai:Landroid/widget/CheckBox;

    iget-object v4, v0, Lpbj;->a:Lpbe;

    if-eqz v4, :cond_2

    iget-object v0, v0, Lpbj;->a:Lpbe;

    iget-object v0, v0, Lpbe;->d:Ljava/lang/Boolean;

    .line 508
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v2

    .line 507
    :cond_2
    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 510
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ai()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->am()V

    .line 511
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ak()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al()V

    .line 512
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->an()V

    goto :goto_0

    :cond_3
    move v3, v1

    .line 484
    goto :goto_1

    .line 490
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Z:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 491
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 492
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ao:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 504
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->as:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v4}, Lidh;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method private ai()V
    .locals 6

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 523
    if-nez v0, :cond_0

    .line 524
    const-string v1, "EditEventFragment"

    const-string v2, "Missing start time in event "

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    new-instance v0, Loyy;

    invoke-direct {v0}, Loyy;-><init>()V

    .line 526
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Loyy;->b:Ljava/lang/Long;

    .line 528
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ae:Landroid/widget/Button;

    .line 529
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v2

    iget-object v3, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Loyy;)Ljava/util/TimeZone;

    move-result-object v0

    .line 528
    invoke-static {v2, v4, v5, v0}, Lidi;->a(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 530
    return-void

    .line 524
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aj()V
    .locals 6

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->m()Loyy;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_0

    .line 535
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af:Landroid/widget/Button;

    .line 536
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v2

    iget-object v3, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Loyy;)Ljava/util/TimeZone;

    move-result-object v0

    .line 535
    invoke-static {v2, v4, v5, v0}, Lidi;->a(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 540
    :goto_0
    return-void

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private ak()V
    .locals 6

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 549
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 550
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ag:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v2

    iget-object v3, v0, Loyy;->b:Ljava/lang/Long;

    .line 551
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Loyy;)Ljava/util/TimeZone;

    move-result-object v0

    .line 550
    invoke-static {v2, v4, v5, v0}, Lidi;->b(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 553
    :cond_0
    return-void
.end method

.method private al()V
    .locals 6

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->m()Loyy;

    move-result-object v0

    .line 557
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 558
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ah:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v2

    iget-object v3, v0, Loyy;->b:Ljava/lang/Long;

    .line 559
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Loyy;)Ljava/util/TimeZone;

    move-result-object v0

    .line 558
    invoke-static {v2, v4, v5, v0}, Lidi;->b(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 563
    :goto_0
    return-void

    .line 561
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ah:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private am()V
    .locals 4

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 568
    if-eqz v0, :cond_0

    .line 570
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 571
    iget-object v2, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 572
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    invoke-virtual {v2, v1}, Lidk;->a(Ljava/util/Calendar;)V

    .line 575
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aw:Lfds;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    invoke-virtual {v1, v2}, Lfds;->a(Lidk;)V

    .line 576
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    iget-object v0, v0, Loyy;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lidk;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->az:I

    .line 577
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->az:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 579
    :cond_0
    return-void
.end method

.method private an()V
    .locals 2

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->o()Lpao;

    move-result-object v0

    .line 583
    if-eqz v0, :cond_0

    .line 584
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ak:Landroid/widget/TextView;

    iget-object v0, v0, Lpao;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588
    :goto_0
    return-void

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ak:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private ao()V
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    .line 594
    :cond_0
    return-void
.end method

.method private ap()V
    .locals 1

    .prologue
    .line 1202
    new-instance v0, Ldzz;

    invoke-direct {v0, p0}, Ldzz;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 1217
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/fragments/EditEventFragment;)I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X:I

    return v0
.end method

.method private b(Ljava/util/Calendar;)V
    .locals 6

    .prologue
    .line 932
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 934
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    .line 936
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->m()Loyy;

    move-result-object v0

    .line 937
    if-nez v0, :cond_0

    .line 938
    new-instance v0, Loyy;

    invoke-direct {v0}, Loyy;-><init>()V

    .line 939
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v0, Loyy;->b:Ljava/lang/Long;

    .line 942
    :cond_0
    iget-object v4, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-eqz v4, :cond_1

    .line 943
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Loyy;->b:Ljava/lang/Long;

    .line 944
    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Loyy;->c:Ljava/lang/String;

    .line 945
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1, v0}, Lidh;->b(Loyy;)V

    .line 946
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->T:Z

    .line 948
    :cond_1
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->P:Ljava/lang/String;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1220
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    if-eqz v0, :cond_1

    .line 1245
    :cond_0
    :goto_0
    return-void

    .line 1224
    :cond_1
    const v0, 0x7f100249

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1225
    const v1, 0x7f1001e9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1226
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-eqz v2, :cond_2

    .line 1227
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1228
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1229
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->h(Landroid/view/View;)V

    goto :goto_0

    .line 1230
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->U:Z

    if-nez v2, :cond_3

    .line 1231
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1232
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1233
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->d(Landroid/view/View;)V

    goto :goto_0

    .line 1234
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->V:Z

    if-eqz v2, :cond_4

    .line 1235
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1236
    const v2, 0x7f0a090b

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1237
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1238
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->h(Landroid/view/View;)V

    goto :goto_0

    .line 1240
    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1241
    const v2, 0x7f0a090c

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1242
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1243
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->h(Landroid/view/View;)V

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->R:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->an:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ab:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ac:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/apps/plus/views/EventThemeView;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aa:Lcom/google/android/apps/plus/views/EventThemeView;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ad:Landroid/widget/EditText;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lidh;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Leae;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->as:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 1633
    sget-object v0, Lhmw;->F:Lhmw;

    return-object v0
.end method

.method public U()V
    .locals 1

    .prologue
    .line 808
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->s()V

    .line 809
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj()V

    .line 810
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al()V

    .line 811
    return-void
.end method

.method public V()V
    .locals 1

    .prologue
    .line 899
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->s()V

    .line 900
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al()V

    .line 901
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj()V

    .line 902
    return-void
.end method

.method public W()V
    .locals 5

    .prologue
    .line 1005
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1006
    const v0, 0x7f0a0934

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Ljava/lang/String;)V

    .line 1008
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->a()Lpbl;

    move-result-object v0

    .line 1009
    if-eqz v0, :cond_0

    iget-object v1, v0, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->a:Lpbe;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->a:Lpbe;

    iget-object v1, v1, Lpbe;->d:Ljava/lang/Boolean;

    .line 1010
    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lpbl;->f:Loya;

    if-eqz v1, :cond_0

    .line 1012
    const/4 v1, 0x0

    iput-object v1, v0, Lpbl;->f:Loya;

    .line 1015
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    if-eqz v0, :cond_2

    .line 1016
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    .line 1017
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h()Lhgw;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->am:Ljava/lang/String;

    .line 1016
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILidh;Lhgw;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    .line 1023
    :cond_1
    :goto_0
    return-void

    .line 1020
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILidh;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public X()V
    .locals 5

    .prologue
    const v4, 0x7f0a07fd

    const v3, 0x7f0a07fa

    const/4 v1, 0x0

    .line 1029
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    if-eqz v0, :cond_4

    .line 1030
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ag()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 1031
    const v0, 0x7f0a0956

    .line 1032
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a0957

    .line 1033
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 1034
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 1035
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 1031
    invoke-static {v0, v2, v3, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 1036
    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    .line 1037
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 1054
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 1030
    goto :goto_0

    .line 1038
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    if-eqz v0, :cond_1

    .line 1039
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    invoke-interface {v0}, Leae;->aw_()V

    goto :goto_1

    .line 1042
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->T:Z

    if-eqz v0, :cond_5

    .line 1043
    const v0, 0x7f0a0958

    .line 1044
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a0959

    .line 1045
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 1046
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 1047
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 1043
    invoke-static {v0, v2, v3, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 1048
    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    .line 1049
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_1

    .line 1050
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    if-eqz v0, :cond_1

    .line 1051
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    invoke-interface {v0}, Leae;->aw_()V

    goto :goto_1
.end method

.method protected Y()I
    .locals 3

    .prologue
    .line 1562
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1564
    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 399
    const v0, 0x7f040089

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 400
    const v0, 0x7f10024b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Z:Landroid/view/View;

    .line 401
    const v0, 0x7f10024c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EventThemeView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aa:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aa:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/EventThemeView;->a(Ljbb;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aa:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/EventThemeView;->setClickable(Z)V

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aa:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/EventThemeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 406
    const v0, 0x7f10024d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ab:Landroid/widget/TextView;

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ab:Landroid/widget/TextView;

    const v2, 0x7f0a093c

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    const v0, 0x7f10024f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ac:Landroid/widget/ProgressBar;

    .line 411
    const v0, 0x7f100250

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ad:Landroid/widget/EditText;

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ad:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aB:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 414
    const v0, 0x7f100251

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ae:Landroid/widget/Button;

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ae:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 417
    const v0, 0x7f100253

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af:Landroid/widget/Button;

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    const v0, 0x7f100252

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ag:Landroid/widget/Button;

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ag:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    const v0, 0x7f100254

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ah:Landroid/widget/Button;

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ah:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 426
    const v0, 0x7f10025a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ak:Landroid/widget/TextView;

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ak:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 428
    const v0, 0x7f100258

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ao:Landroid/view/View;

    .line 430
    const v0, 0x7f100257

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ai:Landroid/widget/CheckBox;

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ai:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 432
    const v0, 0x7f100256

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj:Landroid/view/View;

    .line 433
    const v0, 0x7f100248

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    const v2, 0x7f0a0935

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->b(I)V

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    new-instance v2, Ldzw;

    invoke-direct {v2, p0}, Ldzw;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Ljava/lang/Runnable;)V

    .line 443
    const v0, 0x7f10024e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->an:Landroid/view/View;

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->an:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446
    const v0, 0x7f100245

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->as:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->as:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aC:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->as:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v2

    invoke-virtual {v0, p0, v2, v4, v4}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lu;ILjava/lang/String;Lkjm;)V

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->as:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Z)V

    .line 451
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v2

    const v3, 0x7f0901dd

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 454
    new-instance v2, Lely;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v3

    .line 455
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->w()Lbb;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v5

    invoke-direct {v2, v0, v3, v4, v5}, Lely;-><init>(Landroid/content/Context;Lae;Lbb;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    .line 456
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lely;->j_(I)V

    .line 457
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lely;->d(Z)V

    .line 458
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    invoke-virtual {v2, p0}, Lely;->a(Lelt;)V

    .line 459
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    invoke-virtual {v2, p3}, Lely;->a(Landroid/os/Bundle;)V

    .line 460
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Lely;)V

    .line 461
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c(I)V

    .line 462
    const v2, 0x7f1001a2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464
    new-instance v2, Lfds;

    invoke-direct {v2, v0}, Lfds;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aw:Lfds;

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aw:Lfds;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    invoke-virtual {v0, v2}, Lfds;->a(Lidk;)V

    .line 466
    const v0, 0x7f100255

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aw:Lfds;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    invoke-virtual {v0}, Lidk;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->az:I

    .line 469
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->az:I

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 472
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ah()V

    .line 474
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->c(Landroid/view/View;)V

    .line 476
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1083
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v1

    .line 1084
    packed-switch p1, :pswitch_data_0

    .line 1106
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1086
    :pswitch_0
    new-instance v0, Ldzx;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->at:Llnl;

    invoke-direct {v0, p0, v2, v1}, Ldzx;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;Landroid/content/Context;I)V

    goto :goto_0

    .line 1096
    :pswitch_1
    new-instance v0, Ldzy;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->at:Llnl;

    sget-object v3, Lidg;->a:Landroid/net/Uri;

    invoke-direct {v0, p0, v2, v3, v1}, Ldzy;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;Landroid/content/Context;Landroid/net/Uri;I)V

    goto :goto_0

    .line 1084
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(II)V
    .locals 7

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0xb

    .line 817
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 818
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidm;

    invoke-virtual {v0}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v0

    .line 819
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 820
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 822
    if-nez v0, :cond_4

    .line 823
    const-string v2, "EditEventFragment"

    const-string v3, "Missing start time in event "

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 828
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 829
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p2, :cond_2

    .line 830
    :cond_0
    invoke-virtual {v1, v5, p1}, Ljava/util/Calendar;->set(II)V

    .line 831
    invoke-virtual {v1, v6, p2}, Ljava/util/Calendar;->set(II)V

    .line 833
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 834
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Ljava/util/Calendar;)V

    .line 835
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ak()V

    .line 837
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->m()Loyy;

    move-result-object v0

    .line 838
    if-eqz v0, :cond_1

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-gez v0, :cond_1

    .line 841
    const/16 v0, 0xd

    const/16 v2, 0x1c20

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->add(II)V

    .line 843
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->b(Ljava/util/Calendar;)V

    .line 844
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj()V

    .line 845
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al()V

    .line 848
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    .line 850
    :cond_2
    return-void

    .line 823
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 826
    :cond_4
    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    goto :goto_1
.end method

.method public a(III)V
    .locals 6

    .prologue
    .line 731
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidm;

    .line 733
    invoke-virtual {v0}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v0

    .line 734
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 735
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 737
    if-nez v0, :cond_4

    .line 738
    const-string v2, "EditEventFragment"

    const-string v3, "Missing start time in event "

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 743
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 745
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x5

    .line 746
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p3, :cond_2

    .line 748
    :cond_0
    invoke-virtual {v1, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 750
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Ljava/util/Calendar;)V

    .line 751
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ai()V

    .line 752
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ak()V

    .line 754
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->m()Loyy;

    move-result-object v0

    .line 755
    if-eqz v0, :cond_1

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    .line 758
    const/16 v0, 0xd

    const/16 v2, 0x1c20

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->add(II)V

    .line 760
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->b(Ljava/util/Calendar;)V

    .line 761
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj()V

    .line 762
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al()V

    .line 765
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    .line 767
    :cond_2
    return-void

    .line 738
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 741
    :cond_4
    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    goto :goto_1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 956
    invoke-super {p0, p1, p2, p3}, Leak;->a(IILandroid/content/Intent;)V

    .line 957
    if-ne p2, v2, :cond_0

    if-nez p3, :cond_1

    .line 999
    :cond_0
    :goto_0
    return-void

    .line 961
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 963
    :pswitch_0
    const-string v0, "location"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 964
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->a()Lpbl;

    move-result-object v1

    .line 965
    if-nez v0, :cond_2

    .line 966
    iput-object v4, v1, Lpbl;->f:Loya;

    .line 979
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->an()V

    goto :goto_0

    .line 969
    :cond_2
    :try_start_0
    new-instance v2, Loya;

    invoke-direct {v2}, Loya;-><init>()V

    .line 970
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x154

    aput v5, v3, v4

    iput-object v3, v2, Loya;->b:[I

    .line 971
    sget-object v3, Lpao;->a:Loxr;

    new-instance v4, Lpao;

    invoke-direct {v4}, Lpao;-><init>()V

    .line 972
    invoke-static {v4, v0}, Lpao;->a(Loxu;[B)Loxu;

    move-result-object v0

    .line 971
    invoke-virtual {v2, v3, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 973
    iput-object v2, v1, Lpbl;->f:Loya;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 974
    :catch_0
    move-exception v0

    .line 975
    const-string v1, "EditEventFragment"

    const-string v2, "Unable to deserialize Place."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 984
    :pswitch_1
    const-string v0, "theme_id"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 985
    const-string v1, "theme_url"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 987
    if-eq v0, v2, :cond_0

    if-eqz v1, :cond_0

    .line 988
    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X:I

    .line 989
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 995
    :pswitch_2
    const-string v0, "extra_acl"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ar:Lhgw;

    goto :goto_0

    .line 961
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1075
    return-void
.end method

.method public a(ILfib;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1306
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 1328
    :cond_0
    :goto_0
    return-void

    .line 1310
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 1312
    if-eqz v0, :cond_2

    .line 1313
    invoke-virtual {v0}, Lt;->a()V

    .line 1316
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    .line 1318
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1319
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0a07f1

    :goto_1
    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1321
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1319
    :cond_3
    const v0, 0x7f0a0592

    goto :goto_1

    .line 1322
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    if-eqz v0, :cond_0

    .line 1323
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    if-eqz v0, :cond_5

    const v0, 0x7f0a074a

    :goto_2
    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1325
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1326
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-interface {v0, v1}, Leae;->a(Lidh;)V

    goto :goto_0

    .line 1323
    :cond_5
    const v0, 0x7f0a074b

    goto :goto_2
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1079
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 211
    invoke-super {p0, p1}, Leak;->a(Landroid/app/Activity;)V

    .line 215
    new-instance v0, Lidk;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->at:Llnl;

    invoke-direct {v0, v1}, Lidk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->at:Llnl;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lidk;->a(Ljava/util/Calendar;)V

    .line 217
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 288
    invoke-super {p0, p1}, Leak;->a(Landroid/os/Bundle;)V

    .line 290
    if-eqz p1, :cond_2

    .line 291
    const-string v0, "new_event"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    .line 292
    const-string v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->P:Ljava/lang/String;

    .line 293
    const-string v0, "owner_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Q:Ljava/lang/String;

    .line 294
    const-string v0, "event"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "event_type"

    .line 295
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "event"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 297
    const-string v1, "event_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 299
    if-nez v1, :cond_4

    .line 300
    :try_start_0
    new-instance v1, Lidh;

    new-instance v2, Lpbl;

    invoke-direct {v2}, Lpbl;-><init>()V

    .line 301
    invoke-static {v2, v0}, Lpbl;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lpbl;

    invoke-direct {v1, v0}, Lidh;-><init>(Lpbl;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    :cond_0
    :goto_0
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    .line 314
    :cond_1
    const-string v0, "external_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->am:Ljava/lang/String;

    .line 315
    const-string v0, "changed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->T:Z

    .line 318
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 320
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-nez v0, :cond_3

    .line 321
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 323
    :cond_3
    return-void

    .line 302
    :cond_4
    if-ne v1, v3, :cond_0

    .line 303
    :try_start_1
    new-instance v1, Lidh;

    new-instance v2, Lozp;

    invoke-direct {v2}, Lozp;-><init>()V

    .line 304
    invoke-static {v2, v0}, Lozp;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lozp;

    invoke-direct {v1, v0}, Lidh;-><init>(Lozp;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 306
    :catch_0
    move-exception v0

    .line 307
    const-string v1, "EditEventFragment"

    const-string v2, "Failed to parse binary proto data. "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 308
    invoke-virtual {v0}, Loxt;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1058
    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    if-eqz v0, :cond_0

    .line 1060
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    invoke-interface {v0}, Leae;->aw_()V

    .line 1063
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 0

    .prologue
    .line 1608
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap()V

    .line 1609
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1198
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 1111
    invoke-virtual {p1}, Ldo;->o()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1169
    :cond_0
    :goto_0
    return-void

    .line 1113
    :pswitch_0
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1114
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1115
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1118
    const/4 v3, 0x2

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1120
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1121
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 1122
    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1123
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1126
    :cond_1
    invoke-direct {p0, v1, v2, v0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(ILjava/lang/String;Landroid/net/Uri;Z)V

    goto :goto_0

    .line 1127
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-eqz v1, :cond_0

    .line 1128
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->a()Lpbl;

    move-result-object v1

    .line 1129
    if-eqz v1, :cond_0

    iget-object v2, v1, Lpbl;->l:Lpbj;

    iget-object v2, v2, Lpbj;->d:Llto;

    if-eqz v2, :cond_0

    .line 1130
    iget-object v1, v1, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->d:Llto;

    .line 1131
    invoke-static {v1}, Ldrm;->a(Llto;)Lltp;

    move-result-object v1

    .line 1132
    if-eqz v1, :cond_0

    .line 1133
    iget v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X:I

    iget-object v1, v1, Lltp;->d:Ljava/lang/String;

    invoke-direct {p0, v2, v1, v0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(ILjava/lang/String;Landroid/net/Uri;Z)V

    goto :goto_0

    .line 1141
    :pswitch_1
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->U:Z

    .line 1142
    if-nez p2, :cond_4

    .line 1143
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->V:Z

    .line 1166
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->c(Landroid/view/View;)V

    goto :goto_0

    .line 1145
    :cond_4
    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->V:Z

    .line 1146
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1147
    invoke-static {p2, v4, v5}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    .line 1149
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->R:Ljava/lang/String;

    .line 1151
    const/4 v1, -0x1

    .line 1152
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v2}, Lidh;->a()Lpbl;

    move-result-object v2

    .line 1153
    if-eqz v2, :cond_5

    iget-object v3, v2, Lpbl;->l:Lpbj;

    iget-object v3, v3, Lpbj;->d:Llto;

    if-eqz v3, :cond_5

    .line 1154
    iget-object v1, v2, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->d:Llto;

    iget-object v1, v1, Llto;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1157
    :cond_5
    if-eqz v2, :cond_6

    iget v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X:I

    if-eq v1, v2, :cond_6

    .line 1158
    iput v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X:I

    .line 1159
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->w()Lbb;

    move-result-object v1

    invoke-virtual {v1, v4, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1162
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ah()V

    goto :goto_1

    .line 1111
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 102
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Leae;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    .line 284
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1293
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 1296
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 1297
    return-void
.end method

.method public a(Ljava/lang/String;Lhxc;)V
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Lhxc;)V

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->e()V

    .line 609
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 631
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 614
    if-eqz p4, :cond_0

    .line 615
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    .line 616
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v1

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->eT:Lhmv;

    .line 619
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 620
    invoke-virtual {v1, p4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 617
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 624
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Ljqs;)V

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->e()V

    .line 626
    return-void
.end method

.method public aO_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1265
    invoke-super {p0}, Leak;->aO_()V

    .line 1266
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aA:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 1268
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1269
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1270
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 1271
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(ILfib;)V

    .line 1272
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    .line 1276
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ar:Lhgw;

    if-eqz v0, :cond_1

    .line 1277
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ar:Lhgw;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a(Lhgw;)V

    .line 1278
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ar:Lhgw;

    .line 1279
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ao()V

    .line 1281
    :cond_1
    return-void
.end method

.method public an_()V
    .locals 0

    .prologue
    .line 603
    return-void
.end method

.method public b(II)V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/16 v5, 0xb

    .line 856
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 857
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 859
    if-nez v0, :cond_2

    .line 860
    const-string v1, "EditEventFragment"

    const-string v2, "Missing start time in event "

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v1, v0

    .line 865
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->m()Loyy;

    move-result-object v0

    .line 866
    if-eqz v0, :cond_3

    .line 867
    iget-object v2, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 872
    :goto_2
    if-eqz v0, :cond_0

    .line 873
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p2, :cond_5

    .line 874
    :cond_0
    invoke-virtual {v4, v5, p1}, Ljava/util/Calendar;->set(II)V

    .line 875
    invoke-virtual {v4, v8, p2}, Ljava/util/Calendar;->set(II)V

    .line 877
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidm;

    invoke-virtual {v0}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v0

    .line 878
    invoke-virtual {v4, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 880
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 883
    :goto_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v6, v2

    if-lez v0, :cond_4

    .line 884
    const/4 v0, 0x6

    const/4 v2, 0x1

    invoke-virtual {v4, v0, v2}, Ljava/util/Calendar;->add(II)V

    .line 885
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    goto :goto_3

    .line 860
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 863
    :cond_2
    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    move-object v1, v0

    goto :goto_1

    .line 869
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v6, 0x6ddd00

    add-long/2addr v2, v6

    invoke-virtual {v4, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_2

    .line 888
    :cond_4
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->b(Ljava/util/Calendar;)V

    .line 889
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al()V

    .line 890
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj()V

    .line 891
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    .line 893
    :cond_5
    return-void
.end method

.method public b(III)V
    .locals 8

    .prologue
    .line 773
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidm;

    .line 775
    invoke-virtual {v0}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v0

    .line 776
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->m()Loyy;

    move-result-object v2

    .line 779
    if-eqz v2, :cond_2

    .line 780
    iget-object v0, v2, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 792
    :goto_0
    if-eqz v2, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x2

    .line 793
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x5

    .line 794
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p3, :cond_1

    .line 795
    :cond_0
    invoke-virtual {v1, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 797
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->b(Ljava/util/Calendar;)V

    .line 798
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aj()V

    .line 799
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al()V

    .line 800
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y:Leae;

    .line 802
    :cond_1
    return-void

    .line 782
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 783
    if-nez v0, :cond_3

    .line 784
    const-string v3, "EditEventFragment"

    const-string v4, "Missing start time in event "

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    new-instance v0, Loyy;

    invoke-direct {v0}, Loyy;-><init>()V

    .line 786
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Loyy;->b:Ljava/lang/Long;

    .line 788
    :cond_3
    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x6ddd00

    add-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0

    .line 784
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1067
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 640
    return-void
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-nez v0, :cond_0

    .line 224
    new-instance v0, Lpbl;

    invoke-direct {v0}, Lpbl;-><init>()V

    .line 226
    new-instance v1, Lpbj;

    invoke-direct {v1}, Lpbj;-><init>()V

    iput-object v1, v0, Lpbl;->l:Lpbj;

    .line 227
    iget-object v1, v0, Lpbl;->l:Lpbj;

    new-instance v2, Lpbe;

    invoke-direct {v2}, Lpbe;-><init>()V

    iput-object v2, v1, Lpbj;->a:Lpbe;

    .line 228
    iget-object v1, v0, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->a:Lpbe;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lpbe;->a:Ljava/lang/Boolean;

    .line 229
    iget-object v1, v0, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->a:Lpbe;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lpbe;->b:Ljava/lang/Boolean;

    .line 232
    new-instance v1, Loyy;

    invoke-direct {v1}, Loyy;-><init>()V

    .line 233
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Loyy;->b:Ljava/lang/Long;

    .line 234
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    invoke-virtual {v2}, Lidk;->b()Lidm;

    move-result-object v2

    invoke-virtual {v2}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Loyy;->c:Ljava/lang/String;

    .line 235
    new-instance v2, Loya;

    invoke-direct {v2}, Loya;-><init>()V

    iput-object v2, v0, Lpbl;->h:Loya;

    .line 236
    iget-object v2, v0, Lpbl;->h:Loya;

    new-array v3, v4, [I

    const/4 v4, 0x0

    const/16 v5, 0x196

    aput v5, v3, v4

    iput-object v3, v2, Loya;->b:[I

    .line 237
    iget-object v2, v0, Lpbl;->h:Loya;

    sget-object v3, Loyy;->a:Loxr;

    invoke-virtual {v2, v3, v1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 239
    new-instance v1, Lidh;

    invoke-direct {v1, v0}, Lidh;-><init>(Lpbl;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    .line 240
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/16 v2, 0x20

    invoke-static {v2}, Llsu;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->am:Ljava/lang/String;

    .line 242
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X:I

    .line 244
    :cond_0
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1071
    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 272
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->P:Ljava/lang/String;

    .line 273
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Q:Ljava/lang/String;

    .line 274
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->R:Ljava/lang/String;

    .line 275
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->X:I

    .line 276
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    .line 277
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 327
    invoke-super {p0, p1}, Leak;->e(Landroid/os/Bundle;)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    invoke-virtual {v0, p1}, Lely;->b(Landroid/os/Bundle;)V

    .line 331
    const-string v0, "new_event"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 332
    const-string v0, "event_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->P:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v0, "owner_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-eqz v0, :cond_0

    .line 335
    const-string v0, "event"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->f()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 336
    const-string v0, "event_type"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->g()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 339
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ap:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 341
    :cond_1
    const-string v0, "external_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->am:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v0, "changed"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->T:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 343
    return-void
.end method

.method public e()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 349
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    if-nez v1, :cond_0

    .line 382
    :goto_0
    return v0

    .line 353
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 354
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0788

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 355
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 359
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->S:Z

    if-eqz v1, :cond_2

    .line 360
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ag()Z

    move-result v1

    .line 362
    if-eqz v1, :cond_2

    .line 363
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v1

    .line 364
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0786

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 363
    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 365
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 370
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->m()Loyy;

    move-result-object v1

    .line 371
    if-eqz v1, :cond_3

    .line 372
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v2}, Lidh;->l()Loyy;

    move-result-object v2

    .line 373
    if-eqz v2, :cond_3

    iget-object v3, v2, Loyy;->b:Ljava/lang/Long;

    if-eqz v3, :cond_3

    iget-object v3, v1, Loyy;->b:Ljava/lang/Long;

    if-eqz v3, :cond_3

    iget-object v2, v2, Loyy;->b:Ljava/lang/Long;

    .line 374
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, v1, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_3

    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v1

    .line 376
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0787

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 375
    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 377
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 382
    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 1249
    invoke-super {p0}, Leak;->g()V

    .line 1250
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    if-eqz v0, :cond_0

    .line 1251
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    invoke-virtual {v0}, Lely;->f()V

    .line 1253
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 1257
    invoke-super {p0}, Leak;->h()V

    .line 1258
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    if-eqz v0, :cond_0

    .line 1259
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aq:Lely;

    invoke-virtual {v0}, Lely;->g()V

    .line 1261
    :cond_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ai:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_4

    .line 1614
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ao:Landroid/view/View;

    if-nez p2, :cond_5

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1615
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->x()Landroid/view/View;

    move-result-object v0

    .line 1617
    if-eqz v0, :cond_0

    .line 1618
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 1620
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->i()Lpbj;

    move-result-object v0

    .line 1621
    if-eqz v0, :cond_1

    iget-object v1, v0, Lpbj;->a:Lpbe;

    if-nez v1, :cond_2

    :cond_1
    if-eqz p2, :cond_4

    .line 1622
    :cond_2
    iget-object v1, v0, Lpbj;->a:Lpbe;

    if-nez v1, :cond_3

    .line 1623
    new-instance v1, Lpbe;

    invoke-direct {v1}, Lpbe;-><init>()V

    iput-object v1, v0, Lpbj;->a:Lpbe;

    .line 1626
    :cond_3
    iget-object v0, v0, Lpbj;->a:Lpbe;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lpbe;->d:Ljava/lang/Boolean;

    .line 1629
    :cond_4
    return-void

    .line 1614
    :cond_5
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 652
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v0

    .line 654
    if-nez v0, :cond_0

    .line 655
    const-string v1, "EditEventFragment"

    const-string v3, "Missing start time in event "

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    new-instance v0, Loyy;

    invoke-direct {v0}, Loyy;-><init>()V

    .line 657
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Loyy;->b:Ljava/lang/Long;

    .line 659
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->m()Loyy;

    move-result-object v3

    .line 661
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->Y()I

    move-result v1

    .line 663
    const v4, 0x7f1001a2

    if-ne v2, v4, :cond_3

    .line 664
    sget-object v0, Lhmv;->dL:Lhmv;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Lhmv;)V

    .line 665
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v0

    const v2, 0x7f0a0941

    .line 666
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->al:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    .line 667
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h()Lhgw;

    move-result-object v3

    const/16 v4, 0xc

    move v6, v5

    move v8, v5

    .line 665
    invoke-static/range {v0 .. v8}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Lhgw;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Landroid/content/Intent;I)V

    .line 725
    :cond_1
    :goto_1
    return-void

    .line 655
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 674
    :cond_3
    const v4, 0x7f100251

    if-ne v2, v4, :cond_4

    .line 675
    new-instance v1, Lead;

    invoke-direct {v1, v7}, Lead;-><init>(I)V

    .line 676
    invoke-virtual {v1, p0, v5}, Lead;->a(Lu;I)V

    .line 677
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 678
    const-string v3, "date_time"

    iget-object v4, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 679
    const-string v3, "time_zone"

    iget-object v0, v0, Loyy;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    invoke-virtual {v1, v2}, Lead;->f(Landroid/os/Bundle;)V

    .line 681
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v0

    const-string v2, "date"

    invoke-virtual {v1, v0, v2}, Lead;->a(Lae;Ljava/lang/String;)V

    goto :goto_1

    .line 682
    :cond_4
    const v4, 0x7f100253

    if-ne v2, v4, :cond_6

    .line 683
    new-instance v1, Lead;

    invoke-direct {v1, v5}, Lead;-><init>(I)V

    .line 684
    invoke-virtual {v1, p0, v5}, Lead;->a(Lu;I)V

    .line 685
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 686
    if-eqz v3, :cond_5

    .line 687
    const-string v4, "date_time"

    iget-object v3, v3, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 691
    :goto_2
    const-string v3, "time_zone"

    iget-object v0, v0, Loyy;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    invoke-virtual {v1, v2}, Lead;->f(Landroid/os/Bundle;)V

    .line 693
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v0

    const-string v2, "date"

    invoke-virtual {v1, v0, v2}, Lead;->a(Lae;Ljava/lang/String;)V

    goto :goto_1

    .line 689
    :cond_5
    const-string v3, "date_time"

    iget-object v4, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_2

    .line 694
    :cond_6
    const v4, 0x7f100252

    if-ne v2, v4, :cond_7

    .line 695
    new-instance v1, Leaf;

    invoke-direct {v1, v7}, Leaf;-><init>(I)V

    .line 696
    invoke-virtual {v1, p0, v5}, Leaf;->a(Lu;I)V

    .line 697
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 698
    const-string v3, "date_time"

    iget-object v4, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 699
    const-string v3, "time_zone"

    iget-object v0, v0, Loyy;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-virtual {v1, v2}, Leaf;->f(Landroid/os/Bundle;)V

    .line 701
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v0

    const-string v2, "time"

    invoke-virtual {v1, v0, v2}, Leaf;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 702
    :cond_7
    const v4, 0x7f100254

    if-ne v2, v4, :cond_9

    .line 703
    new-instance v1, Leaf;

    invoke-direct {v1, v5}, Leaf;-><init>(I)V

    .line 704
    invoke-virtual {v1, p0, v5}, Leaf;->a(Lu;I)V

    .line 705
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 706
    if-eqz v3, :cond_8

    .line 707
    const-string v4, "date_time"

    iget-object v3, v3, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 712
    :goto_3
    const-string v3, "time_zone"

    iget-object v0, v0, Loyy;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    invoke-virtual {v1, v2}, Leaf;->f(Landroid/os/Bundle;)V

    .line 714
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->p()Lae;

    move-result-object v0

    const-string v2, "time"

    invoke-virtual {v1, v0, v2}, Leaf;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 709
    :cond_8
    const-string v3, "date_time"

    iget-object v4, v0, Loyy;->b:Ljava/lang/Long;

    .line 710
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x6ddd00

    add-long/2addr v4, v6

    .line 709
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_3

    .line 715
    :cond_9
    const v0, 0x7f10025a

    if-ne v2, v0, :cond_b

    .line 716
    sget-object v0, Lhmv;->dy:Lhmv;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Lhmv;)V

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->o()Lpao;

    move-result-object v0

    .line 718
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/EventLocationActivity;

    invoke-direct {v3, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.PICK"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz v0, :cond_a

    const-string v1, "location"

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 720
    :cond_a
    invoke-virtual {p0, v3, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 721
    :cond_b
    const v0, 0x7f10024e

    if-ne v2, v0, :cond_1

    .line 722
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->n()Lz;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "account_id"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 723
    invoke-virtual {p0, v2, v7}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1569
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->az:I

    if-eq p3, v0, :cond_2

    .line 1570
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ax:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidm;

    .line 1571
    invoke-virtual {v0}, Lidm;->c()J

    move-result-wide v4

    .line 1572
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    invoke-virtual {v1}, Lidk;->b()Lidm;

    move-result-object v1

    invoke-virtual {v1}, Lidm;->c()J

    move-result-wide v2

    .line 1573
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->l()Loyy;

    move-result-object v1

    .line 1574
    if-nez v1, :cond_0

    .line 1575
    const-string v6, "EditEventFragment"

    const-string v7, "Missing start time in event "

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1}, Lidh;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v7, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1576
    new-instance v1, Loyy;

    invoke-direct {v1}, Loyy;-><init>()V

    .line 1577
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->af()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v1, Loyy;->b:Ljava/lang/Long;

    .line 1579
    :cond_0
    iget-object v6, v1, Loyy;->c:Ljava/lang/String;

    .line 1580
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1581
    invoke-static {v6}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 1582
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->ay:Lidk;

    invoke-virtual {v3, v2}, Lidk;->a(Ljava/util/TimeZone;)J

    move-result-wide v2

    .line 1585
    :cond_1
    sub-long/2addr v2, v4

    .line 1587
    invoke-virtual {v0}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Loyy;->c:Ljava/lang/String;

    .line 1588
    iget-object v0, v1, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Loyy;->b:Ljava/lang/Long;

    .line 1589
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0, v1}, Lidh;->a(Loyy;)V

    .line 1591
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v0}, Lidh;->m()Loyy;

    move-result-object v0

    .line 1592
    if-eqz v0, :cond_2

    .line 1593
    iget-object v4, v0, Loyy;->b:Ljava/lang/Long;

    if-eqz v4, :cond_2

    .line 1594
    iget-object v4, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Loyy;->b:Ljava/lang/Long;

    .line 1595
    iget-object v1, v1, Loyy;->c:Ljava/lang/String;

    iput-object v1, v0, Loyy;->c:Ljava/lang/String;

    .line 1596
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->W:Lidh;

    invoke-virtual {v1, v0}, Lidh;->b(Loyy;)V

    .line 1600
    :cond_2
    return-void

    .line 1575
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1604
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 1285
    invoke-super {p0}, Leak;->z()V

    .line 1286
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->aA:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 1287
    return-void
.end method
