.class public Lcom/google/android/apps/plus/views/TypeableAudienceView;
.super Lhgy;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static synthetic k:Z


# instance fields
.field public a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

.field private h:I

.field private i:Landroid/widget/ScrollView;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->k:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 199
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 209
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 219
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lhgy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    .line 44
    iput v1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h:I

    .line 221
    sget-object v0, Ldht;->a:[I

    .line 222
    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 223
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h:I

    .line 224
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 225
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/TypeableAudienceView;)V
    .locals 0

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->j()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/TypeableAudienceView;)V
    .locals 0

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->j()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 229
    const v0, 0x7f040228

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addView(Landroid/view/View;)V

    .line 231
    const v0, 0x7f100611

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->i:Landroid/widget/ScrollView;

    .line 232
    const v0, 0x7f1001a4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c:Landroid/view/ViewGroup;

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setThreshold(I)V

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d02d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setDropDownWidth(I)V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    new-instance v1, Lgbm;

    invoke-direct {v1, p0}, Lgbm;-><init>(Lcom/google/android/apps/plus/views/TypeableAudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    new-instance v1, Lgbn;

    invoke-direct {v1, p0}, Lgbn;-><init>(Lcom/google/android/apps/plus/views/TypeableAudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    new-instance v1, Lgbo;

    invoke-direct {v1, p0}, Lgbo;-><init>(Lcom/google/android/apps/plus/views/TypeableAudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->a(Lgbq;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setImeOptions(I)V

    .line 299
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->b(I)V

    .line 300
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 360
    iput p1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h:I

    .line 361
    return-void
.end method

.method public a(Lely;)V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 357
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 369
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 396
    invoke-super {p0}, Lhgy;->b()V

    .line 398
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->f()V

    .line 399
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 407
    iput p1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->j:I

    .line 408
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->f()V

    .line 409
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    return-object v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->f()V

    .line 431
    return-void
.end method

.method f()V
    .locals 3

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    if-nez v0, :cond_0

    .line 443
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->j:I

    if-eqz v0, :cond_1

    .line 442
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->j:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 441
    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 442
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 379
    iget v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->d:I

    .line 380
    const-class v0, Lhms;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    invoke-direct {v3, v1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->dH:Lhmv;

    .line 382
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 380
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 385
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 386
    sget-boolean v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c:Landroid/view/ViewGroup;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->requestFocus()Z

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-static {v0}, Llsn;->a(Landroid/view/View;)V

    .line 392
    :goto_0
    return-void

    .line 390
    :cond_1
    invoke-super {p0, p1}, Lhgy;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 346
    invoke-super {p0}, Lhgy;->onDetachedFromWindow()V

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 353
    :cond_0
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 304
    invoke-super/range {p0 .. p5}, Lhgy;->onLayout(ZIIII)V

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h:I

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->i:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->h:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->i:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->i:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->i:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 322
    check-cast p1, Lgbr;

    .line 323
    invoke-virtual {p1}, Lgbr;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lhgy;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    iget-object v1, p1, Lgbr;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    iget v1, p1, Lgbr;->b:I

    iget v2, p1, Lgbr;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->setSelection(II)V

    .line 327
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 310
    invoke-super {p0}, Lhgy;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 311
    new-instance v1, Lgbr;

    invoke-direct {v1, v0}, Lgbr;-><init>(Landroid/os/Parcelable;)V

    .line 313
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgbr;->a:Ljava/lang/String;

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->d()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    iput v0, v1, Lgbr;->b:I

    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->d()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    iput v0, v1, Lgbr;->c:I

    .line 317
    return-object v1
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView;->f:Z

    .line 374
    return-void
.end method
