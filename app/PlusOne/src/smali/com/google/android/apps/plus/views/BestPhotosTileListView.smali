.class public Lcom/google/android/apps/plus/views/BestPhotosTileListView;
.super Lcom/google/android/apps/plus/views/FastScrollListView;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private a:Z

.field private b:I

.field private c:I

.field private d:[I

.field private e:[I

.field private f:[Ljava/lang/String;

.field private g:Landroid/widget/AbsListView$OnScrollListener;

.field private h:Levi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/FastScrollListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-super {p0, p0}, Lcom/google/android/apps/plus/views/FastScrollListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected handleDataChanged()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getHeaderViewsCount()I

    move-result v2

    sub-int/2addr v0, v2

    .line 132
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->a:Z

    if-eqz v2, :cond_0

    if-lez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->h:Levi;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->h:Levi;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->d:[I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->f:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->e:[I

    iget v5, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->c:I

    iget v5, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->b:I

    invoke-virtual {v0, v2, v3, v4, v5}, Levi;->a([I[Ljava/lang/String;[II)I

    move-result v0

    .line 139
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->setSelection(I)V

    .line 140
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->a:Z

    .line 141
    iput-object v6, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->d:[I

    .line 142
    iput-object v6, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->f:[Ljava/lang/String;

    .line 145
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/views/FastScrollListView;->handleDataChanged()V

    .line 146
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 89
    check-cast p1, Lfwk;

    .line 91
    invoke-virtual {p1}, Lfwk;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/apps/plus/views/FastScrollListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 93
    iget v0, p1, Lfwk;->a:I

    iput v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->b:I

    .line 94
    iget v0, p1, Lfwk;->b:I

    iput v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->c:I

    .line 95
    iget-object v0, p1, Lfwk;->c:[I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->d:[I

    .line 96
    iget-object v0, p1, Lfwk;->d:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->f:[Ljava/lang/String;

    .line 97
    iget-object v0, p1, Lfwk;->e:[I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->e:[I

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->d:[I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->a:Z

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->requestLayout()V

    .line 101
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-super {p0}, Lcom/google/android/apps/plus/views/FastScrollListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 53
    new-instance v3, Lfwk;

    invoke-direct {v3, v0}, Lfwk;-><init>(Landroid/os/Parcelable;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getHeaderViewsCount()I

    move-result v2

    sub-int v4, v0, v2

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    if-lez v4, :cond_2

    const/4 v0, 0x1

    .line 57
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getSelectedItemId()J

    move-result-wide v6

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getLastVisiblePosition()I

    move-result v5

    .line 60
    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-ltz v2, :cond_3

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getSelectedItemPosition()I

    move-result v1

    .line 73
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->h:Levi;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->h:Levi;

    invoke-virtual {v0}, Levi;->c()Landroid/util/Pair;

    move-result-object v2

    .line 75
    if-eqz v2, :cond_1

    .line 76
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, v3, Lfwk;->c:[I

    .line 77
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Lfwk;->d:[Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->h:Levi;

    invoke-virtual {v0}, Levi;->d()[I

    move-result-object v0

    iput-object v0, v3, Lfwk;->e:[I

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->h:Levi;

    invoke-virtual {v0, v1}, Levi;->b(I)I

    move-result v0

    iput v0, v3, Lfwk;->a:I

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->h:Levi;

    invoke-virtual {v0, v5}, Levi;->b(I)I

    move-result v0

    iput v0, v3, Lfwk;->b:I

    .line 84
    :cond_1
    return-object v3

    :cond_2
    move v0, v1

    .line 56
    goto :goto_0

    .line 64
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->getFirstVisiblePosition()I

    move-result v2

    .line 65
    if-eqz v0, :cond_0

    if-lez v2, :cond_0

    .line 66
    if-lt v2, v4, :cond_4

    .line 67
    add-int/lit8 v1, v4, -0x1

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->g:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->g:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 125
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->g:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->g:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 117
    :cond_0
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 43
    instance-of v0, p1, Levi;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 44
    check-cast v0, Levi;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->h:Levi;

    .line 46
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 47
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->g:Landroid/widget/AbsListView$OnScrollListener;

    .line 109
    invoke-super {p0, p0}, Lcom/google/android/apps/plus/views/FastScrollListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 110
    return-void
.end method
