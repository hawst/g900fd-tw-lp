.class public Lcom/google/android/apps/plus/content/MovieMakerUtils$OnNewAamNotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 587
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 652
    const/high16 v0, 0x10000000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 653
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 654
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    .line 620
    const-string v0, "account_id"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 621
    if-ne v4, v1, :cond_1

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 625
    invoke-interface {v0, v4}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 629
    const/4 v1, 0x1

    invoke-static {p1, v4, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(Landroid/content/Context;IZ)V

    .line 632
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 633
    const-string v2, "com.google.android.apps.plus.ON_NEW_AAM_NOTIFICATION_VIEW_SINGLE"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 635
    const-string v1, "CLUSTER_ID_TO_VIEW"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 637
    invoke-static {p1, v4, v5}, Leyq;->b(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils$OnNewAamNotificationReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 639
    new-instance v1, Ljed;

    const-string v3, "gaia_id"

    .line 640
    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v2}, Ljed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x3

    const-string v3, "NotificationAutoAwesomes"

    move-object v0, p1

    .line 639
    invoke-static/range {v0 .. v5}, Ljfa;->a(Landroid/content/Context;Ljed;ILjava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils$OnNewAamNotificationReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 645
    :cond_2
    const-string v0, "com.google.android.apps.plus.ON_NEW_AAM_NOTIFICATION_VIEW_ALL"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    invoke-static {p1, v4, v5}, Leyq;->b(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils$OnNewAamNotificationReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
