.class public Lcom/google/android/apps/plus/views/VideoProgressView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private a:Landroid/widget/SeekBar;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:D

.field private e:D

.field private f:Z

.field private g:Lgcb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/VideoProgressView;->b()V

    .line 84
    iget-wide v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->e:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->d:D

    iget-wide v2, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->e:D

    div-double/2addr v0, v2

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->a:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 85
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->a:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->b:Landroid/widget/TextView;

    iget-wide v2, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->d:D

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/views/VideoProgressView;->c(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->c:Landroid/widget/TextView;

    iget-wide v2, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->e:D

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/views/VideoProgressView;->c(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    return-void
.end method

.method private c(D)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 106
    double-to-int v0, p1

    rem-int/lit8 v0, v0, 0x3c

    .line 107
    double-to-int v1, p1

    div-int/lit8 v1, v1, 0x3c

    rem-int/lit8 v1, v1, 0x3c

    .line 108
    double-to-int v2, p1

    div-int/lit16 v2, v2, 0xe10

    .line 110
    if-lez v2, :cond_0

    .line 111
    const-string v3, "%d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    :cond_0
    const-string v2, "%d:%02d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(D)V
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->f:Z

    if-eqz v0, :cond_0

    .line 71
    :goto_0
    return-void

    .line 68
    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->d:D

    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/VideoProgressView;->a()V

    goto :goto_0
.end method

.method public a(Lgcb;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->g:Lgcb;

    .line 80
    return-void
.end method

.method public b(D)V
    .locals 1

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->e:D

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/VideoProgressView;->a()V

    .line 76
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    const v0, 0x7f100624

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/VideoProgressView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->b:Landroid/widget/TextView;

    const v0, 0x7f100246

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/VideoProgressView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->c:Landroid/widget/TextView;

    const v0, 0x7f100343

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/VideoProgressView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->a:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->a:Landroid/widget/SeekBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->a:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/VideoProgressView;->a()V

    .line 51
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6

    .prologue
    .line 120
    if-eqz p3, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->e:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 121
    iget-wide v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->e:D

    int-to-double v2, p2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->d:D

    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/VideoProgressView;->b()V

    .line 124
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->f:Z

    .line 129
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    .prologue
    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->f:Z

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->g:Lgcb;

    iget-wide v2, p0, Lcom/google/android/apps/plus/views/VideoProgressView;->d:D

    invoke-interface {v0, v2, v3}, Lgcb;->a(D)V

    .line 135
    return-void
.end method
