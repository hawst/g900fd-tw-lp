.class public Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;


# instance fields
.field private final e:Lhee;

.field private f:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Lloa;-><init>()V

    .line 32
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 34
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->x:Llnh;

    .line 35
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 38
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->x:Llnh;

    .line 39
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->e:Lhee;

    .line 38
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lhmw;->o:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 74
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 45
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v0, 0x7f0400a5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->setContentView(I)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->h()Loo;

    move-result-object v0

    .line 50
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Loo;->c(Z)V

    .line 52
    const v0, 0x7f10028d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->f:Landroid/widget/ListView;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->x:Llnh;

    const-class v2, Lieh;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Lieh;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 57
    new-instance v0, Lexw;

    invoke-direct {v0}, Lexw;-><init>()V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 64
    new-instance v3, Lexv;

    const v4, 0x7f0400a4

    .line 65
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lief;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lief;

    invoke-direct {v3, p0, v4, v0, v1}, Lexv;-><init>(Landroid/content/Context;I[Lief;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    return-void
.end method
