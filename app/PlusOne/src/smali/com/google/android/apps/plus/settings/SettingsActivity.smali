.class public Lcom/google/android/apps/plus/settings/SettingsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;


# instance fields
.field private e:Lhee;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0}, Lloa;-><init>()V

    .line 32
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/SettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/SettingsActivity;->x:Llnh;

    .line 33
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/SettingsActivity;->e:Lhee;

    .line 36
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/SettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 37
    new-instance v0, Lfmb;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/SettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfmb;-><init>(Los;Llqr;)V

    .line 44
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/SettingsActivity;->y:Llqc;

    const v2, 0x7f120012

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/SettingsActivity;->x:Llnh;

    .line 45
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 46
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 81
    const v0, 0x7f10066c

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 82
    const v0, 0x7f10066d

    new-instance v1, Litl;

    const-string v2, "plus_settings"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 83
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 51
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const v0, 0x7f04007a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setContentView(I)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/SettingsActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->h()Loo;

    move-result-object v1

    .line 57
    const-string v2, "display_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 58
    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    if-eqz v1, :cond_1

    .line 60
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 61
    invoke-virtual {v1, v0}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 69
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-virtual {v1, v2}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 64
    invoke-virtual {v1, v0}, Loo;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
