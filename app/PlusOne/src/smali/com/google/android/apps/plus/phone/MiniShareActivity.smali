.class public Lcom/google/android/apps/plus/phone/MiniShareActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 36
    invoke-direct {p0}, Lloa;-><init>()V

    .line 45
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 47
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->x:Llnh;

    .line 48
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 50
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->x:Llnh;

    .line 51
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 52
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 54
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 55
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 58
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->x:Llnh;

    .line 59
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 60
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 62
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 107
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 109
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/MiniShareActivity;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 112
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 95
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 96
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 97
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 98
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 87
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x2

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 69
    if-nez p1, :cond_0

    .line 70
    new-instance v0, Leef;

    invoke-direct {v0}, Leef;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/MiniShareActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 73
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/MiniShareActivity;->setContentView(I)V

    .line 74
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/MiniShareActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 77
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/MiniShareActivity;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->f()V

    .line 78
    return-void

    .line 74
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
