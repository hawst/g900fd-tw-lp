.class public Lcom/google/android/apps/plus/phone/EventActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhjn;
.implements Lhmq;


# instance fields
.field private final e:Ldie;

.field private f:I

.field private g:I

.field private h:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

.field private j:Lctq;

.field private k:Ljma;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljma",
            "<",
            "Lctq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 63
    invoke-direct {p0}, Lloa;-><init>()V

    .line 72
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 73
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 75
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->x:Llnh;

    .line 76
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 78
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->x:Llnh;

    .line 79
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 80
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 82
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 83
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 85
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->x:Llnh;

    .line 86
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 89
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->e:Ldie;

    .line 92
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/EventActivity;Lctq;Loo;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-virtual {p1}, Lctq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p2, v0}, Loo;->e(Z)V

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p2, v1}, Loo;->d(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private m()Lu;
    .locals 6

    .prologue
    .line 126
    new-instance v0, Leco;

    invoke-direct {v0}, Leco;-><init>()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 128
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 130
    const-string v3, "event_id"

    const-string v4, "event_id"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v3, "owner_id"

    const-string v4, "owner_id"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v3, "invitation_token"

    const-string v4, "invitation_token"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v3, "auth_key"

    const-string v4, "auth_key"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v3, "rsvp"

    const-string v4, "rsvp"

    const/high16 v5, -0x80000000

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    const-string v1, "external_action"

    iget v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 141
    invoke-virtual {v0, v2}, Lu;->f(Landroid/os/Bundle;)V

    .line 143
    return-object v0
.end method

.method private n()Lu;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 147
    const-class v0, Lcnt;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    .line 148
    const-string v1, "Albums"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcnt;->a(Ljava/lang/String;I)V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 152
    new-instance v1, Lehb;

    invoke-direct {v1}, Lehb;-><init>()V

    .line 153
    const-string v2, "event_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 154
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 155
    const-string v3, "owner_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 156
    const-string v4, "auth_key"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    const/4 v4, 0x3

    new-array v5, v9, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "PLUS_EVENT"

    .line 159
    invoke-static {v6, v3, v2, v7}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v8

    .line 158
    invoke-static {v4, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 161
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 162
    const-string v4, "cluster_id"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v2, "auth_key"

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v0, "show_title"

    invoke-virtual {v3, v0, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 165
    const-string v0, "hide_footer"

    invoke-virtual {v3, v0, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    invoke-virtual {v1, v3}, Lu;->f(Landroid/os/Bundle;)V

    .line 169
    :cond_0
    return-object v1
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lhmw;->B:Lhmw;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 331
    iget v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    if-ne p1, v0, :cond_0

    .line 332
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    .line 334
    :cond_0
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 109
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 111
    new-instance v0, Lctq;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctq;-><init>(Landroid/app/Activity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->j:Lctq;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 114
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lctz;

    new-instance v2, Lctz;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lctz;-><init>(Landroid/app/Activity;Llqr;)V

    .line 115
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lctq;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EventActivity;->j:Lctq;

    .line 116
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 117
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsd;

    new-instance v2, Lcsd;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v2, v3}, Lcsd;-><init>(Llqr;)V

    .line 119
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 120
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsm;

    new-instance v2, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    .line 121
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 123
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 325
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 326
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 327
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 328
    return-void
.end method

.method public a(Loo;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 280
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 281
    invoke-virtual {p1, v3}, Loo;->c(Z)V

    .line 283
    const v0, 0x7f040031

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 284
    const v0, 0x7f100176

    .line 285
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->i:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->i:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EventActivity;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->i:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget v2, p0, Lcom/google/android/apps/plus/phone/EventActivity;->g:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->i:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->a(Lhjn;)V

    .line 289
    invoke-virtual {p1, v1}, Loo;->a(Landroid/view/View;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->k:Ljma;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->j:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->k:Ljma;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 294
    :cond_0
    new-instance v0, Lexn;

    invoke-direct {v0, p0, p1}, Lexn;-><init>(Lcom/google/android/apps/plus/phone/EventActivity;Loo;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->k:Ljma;

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->j:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->k:Ljma;

    invoke-interface {v0, v1, v3}, Ljlx;->a(Ljma;Z)V

    .line 302
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method public b(Loo;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->k:Ljma;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->j:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->k:Ljma;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 315
    :cond_0
    iput-object v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->k:Ljma;

    .line 317
    invoke-virtual {p1, v3}, Loo;->a(Landroid/view/View;)V

    .line 318
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 319
    invoke-virtual {p1, v2}, Loo;->d(Z)V

    .line 320
    invoke-virtual {p1, v2}, Loo;->c(Z)V

    .line 321
    return-void
.end method

.method public c(I)Z
    .locals 3

    .prologue
    .line 245
    iget v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->g:I

    if-ne v0, p1, :cond_0

    .line 246
    const/4 v0, 0x0

    .line 275
    :goto_0
    return v0

    .line 250
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 266
    const/4 v0, 0x0

    .line 271
    :goto_1
    if-eqz v0, :cond_1

    .line 272
    iput p1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->g:I

    .line 273
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 275
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 252
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->m()Lu;

    move-result-object v0

    goto :goto_1

    .line 257
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->x:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    invoke-direct {v1, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aM:Lhmv;

    .line 259
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 257
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 261
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->n()Lu;

    move-result-object v0

    goto :goto_1

    .line 250
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public l()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 174
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f040037

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->h:Landroid/widget/ArrayAdapter;

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->h:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->h:Landroid/widget/ArrayAdapter;

    const v1, 0x7f0a0907

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EventActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->h:Landroid/widget/ArrayAdapter;

    const v1, 0x7f0a0908

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EventActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 182
    if-nez p1, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_keyboard"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    .line 191
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 193
    if-nez p1, :cond_1

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 196
    const-string v1, "destination"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 200
    packed-switch v0, :pswitch_data_0

    .line 207
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->m()Lu;

    move-result-object v0

    .line 208
    iput v2, p0, Lcom/google/android/apps/plus/phone/EventActivity;->g:I

    .line 213
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 215
    :cond_1
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventActivity;->setContentView(I)V

    .line 216
    return-void

    .line 186
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "external_action"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    goto :goto_0

    .line 202
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->n()Lu;

    move-result-object v0

    .line 203
    iput v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->g:I

    goto :goto_1

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 227
    invoke-super {p0, p1}, Lloa;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 228
    const-string v0, "spinnerIndex"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 230
    const v0, 0x7f100176

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    .line 231
    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EventActivity;->c(I)Z

    .line 237
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->i:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    .line 239
    const-string v0, "external_action"

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "external_action"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 239
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    .line 241
    return-void

    .line 235
    :cond_0
    iput v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->g:I

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 220
    invoke-super {p0, p1}, Lloa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 221
    const-string v0, "spinnerIndex"

    iget v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 222
    const-string v0, "external_action"

    iget v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    return-void
.end method
