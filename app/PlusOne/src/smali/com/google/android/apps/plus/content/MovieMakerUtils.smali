.class public final Lcom/google/android/apps/plus/content/MovieMakerUtils;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static d:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:[Ljava/lang/String;

.field private static final f:I

.field private static final g:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 137
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "cluster_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "subtitle"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "cluster_count"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "media_attr"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a:[Ljava/lang/String;

    .line 145
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "timestamp"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b:[Ljava/lang/String;

    .line 184
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "last_aam_snapshot"

    aput-object v1, v0, v3

    const-string v1, "seen_aams"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c:[Ljava/lang/String;

    .line 203
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "filename"

    aput-object v1, v0, v3

    const-string v1, "representation_type"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->e:[Ljava/lang/String;

    .line 215
    const v0, 0x7f1000c8

    sput v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->f:I

    .line 265
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 587
    return-void
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 569
    sget-object v1, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    monitor-enter v1

    .line 570
    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    monitor-exit v1

    return v0

    .line 571
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 696
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 697
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v2

    .line 699
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 700
    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "gaia_id"

    .line 701
    invoke-interface {v2, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v8

    .line 700
    invoke-static {v8, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 703
    const-string v2, "all_tiles"

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "image_url"

    aput-object v4, v3, v8

    const-string v4, "view_id = ?  AND type = 2 AND view_order > 50100 AND (NOT media_attr & 4194304) AND cluster_id = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    aput-object v7, v5, v8

    aput-object p2, v5, v1

    const-string v8, "view_order"

    move-object v7, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 710
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 711
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 714
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 717
    if-nez v0, :cond_0

    .line 729
    :goto_1
    return-object v6

    .line 714
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 721
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p0, v0, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 722
    const-class v0, Lizs;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    .line 724
    const/4 v2, 0x0

    const/4 v5, 0x0

    move v3, p3

    move v4, p4

    :try_start_1
    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_1
    .catch Lkdn; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lkde; {:try_start_1 .. :try_end_1} :catch_0

    move-object v6, v0

    goto :goto_1

    .line 729
    :catch_0
    move-exception v0

    goto :goto_1

    .line 727
    :catch_1
    move-exception v0

    goto :goto_1

    :cond_1
    move-object v0, v6

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILjava/util/List;)Landroid/graphics/Bitmap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 667
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 668
    const v1, 0x7f0d014f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 669
    const v2, 0x7f0d0150

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v2, v0

    .line 670
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 671
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 672
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_1

    .line 673
    invoke-static {p0, p1, v0, v1, v2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;ILjava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 676
    if-eqz v0, :cond_0

    .line 677
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 680
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 681
    :goto_1
    return-object v0

    :cond_2
    invoke-static {v3, v1, v2}, Lfus;->a(Ljava/util/List;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 1364
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, v0

    .line 1365
    :goto_0
    if-nez v1, :cond_3

    .line 1375
    :cond_0
    :goto_1
    return-object p1

    .line 1364
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    move-object v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1368
    :cond_3
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1369
    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "logged_in"

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1370
    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->f(Landroid/content/Context;ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1371
    if-eqz v0, :cond_4

    move-object p1, v0

    .line 1372
    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 315
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 316
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "logged_in"

    aput-object v4, v1, v3

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 317
    invoke-static {p0, v1}, Ldhv;->w(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 318
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 319
    const-string v4, "gaia_id"

    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 322
    :cond_1
    return-object v2
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Z)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1067
    .line 1068
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1071
    invoke-static {v0, p2}, Ldtx;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1073
    invoke-static {v0, p2}, Ldtx;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1074
    if-eqz p3, :cond_1

    const/4 v0, 0x0

    .line 1076
    :goto_0
    const/4 v1, 0x3

    :try_start_0
    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const/4 v4, 0x2

    aput-object v0, v1, v4

    invoke-static {p0, p3, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;Z[Landroid/database/Cursor;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1078
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1079
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1080
    if-eqz v0, :cond_0

    .line 1081
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v1

    .line 1074
    :cond_1
    invoke-static {v0, p2}, Ldtx;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 1078
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1079
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1080
    if-eqz v0, :cond_2

    .line 1081
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method

.method private static varargs a(Landroid/content/Context;Z[Landroid/database/Cursor;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z[",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1102
    const-class v2, Lkdv;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkdv;

    invoke-interface {v2}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v5

    .line 1103
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1104
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1106
    move-object/from16 v0, p2

    array-length v8, v0

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v8, :cond_9

    aget-object v9, p2, v4

    .line 1107
    if-eqz v9, :cond_8

    .line 1108
    :cond_0
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1111
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1112
    invoke-static {v2, v3}, Ljvj;->a(J)Ljac;

    move-result-object v10

    .line 1113
    invoke-static {v10}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Ljac;)Ljel;

    move-result-object v11

    .line 1114
    const/4 v2, 0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1115
    invoke-interface {v6, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1117
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v11, :cond_0

    .line 1121
    sget-object v2, Ljel;->b:Ljel;

    if-ne v11, v2, :cond_1

    .line 1127
    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1129
    const/4 v3, 0x0

    .line 1131
    :try_start_0
    new-instance v2, Lnym;

    invoke-direct {v2}, Lnym;-><init>()V

    const/4 v14, 0x3

    .line 1132
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v14

    .line 1131
    invoke-static {v2, v14}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v2

    check-cast v2, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1137
    :goto_2
    move-object/from16 v0, p0

    invoke-static {v0, v13, v2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;Ljava/lang/String;Lnym;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1138
    :cond_1
    move-object/from16 v0, p0

    invoke-static {v0, v12, v10}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Landroid/net/Uri;

    move-result-object v3

    .line 1143
    const/4 v2, 0x0

    .line 1144
    const/4 v13, 0x5

    invoke-interface {v9, v13}, Landroid/database/Cursor;->isNull(I)Z

    move-result v13

    if-nez v13, :cond_4

    .line 1145
    const/4 v2, 0x5

    .line 1146
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1145
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1152
    :cond_2
    :goto_3
    if-eqz p1, :cond_3

    if-eqz v2, :cond_0

    .line 1153
    :cond_3
    new-instance v10, Ljej;

    if-eqz v2, :cond_7

    :goto_4
    invoke-direct {v10, v2, v11}, Ljej;-><init>(Landroid/net/Uri;Ljel;)V

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157
    invoke-interface {v6, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1133
    :catch_0
    move-exception v2

    .line 1134
    const-string v14, "MovieMakerUtils"

    const-string v15, "Failed to parse photo data"

    invoke-static {v14, v15, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v3

    goto :goto_2

    .line 1147
    :cond_4
    const/4 v13, 0x4

    invoke-interface {v9, v13}, Landroid/database/Cursor;->isNull(I)Z

    move-result v13

    if-nez v13, :cond_2

    .line 1148
    const/4 v2, 0x4

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1149
    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v14, "content"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/net/Uri;Landroid/content/Context;)Ljava/io/File;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    :cond_6
    invoke-virtual {v5, v13}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v10}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_3

    :cond_7
    move-object v2, v3

    .line 1153
    goto :goto_4

    .line 1106
    :cond_8
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 1160
    :cond_9
    return-object v7
.end method

.method public static a(Landroid/content/Context;IZ)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZ)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 358
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 359
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d(Landroid/content/Context;ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 362
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 363
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(J)Z

    move-result v2

    if-ne p2, v2, :cond_0

    .line 364
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 368
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 371
    return-object v0
.end method

.method private static a(Ljac;)Ljel;
    .locals 2

    .prologue
    .line 1291
    sget-object v0, Ldvi;->a:[I

    invoke-virtual {p0}, Ljac;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown media type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1293
    :pswitch_0
    sget-object v0, Ljel;->a:Ljel;

    .line 1299
    :goto_0
    return-object v0

    .line 1295
    :pswitch_1
    sget-object v0, Ljel;->b:Ljel;

    goto :goto_0

    .line 1299
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1291
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;IJ)Lnym;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1168
    .line 1169
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1174
    const/4 v1, 0x1

    :try_start_0
    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 1175
    const-string v1, "all_photos"

    sget-object v2, Ldvm;->a:[Ljava/lang/String;

    const-string v3, "photo_id = ?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1178
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 1179
    if-eqz v1, :cond_0

    .line 1185
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v9

    .line 1191
    :goto_0
    return-object v0

    .line 1182
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 1184
    if-eqz v1, :cond_2

    .line 1185
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1189
    :cond_2
    :try_start_3
    new-instance v1, Lnym;

    invoke-direct {v1}, Lnym;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;
    :try_end_3
    .catch Loxt; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 1184
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v9, :cond_3

    .line 1185
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1191
    :catch_0
    move-exception v0

    move-object v0, v9

    goto :goto_0

    .line 1184
    :catchall_1
    move-exception v0

    move-object v9, v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;J)Lnzc;
    .locals 6

    .prologue
    .line 1577
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->b(Ljava/lang/String;)I

    move-result v2

    .line 1578
    invoke-static {p0, v2, p2, p3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d(Landroid/content/Context;IJ)Lnzc;

    move-result-object v0

    .line 1579
    if-eqz v0, :cond_1

    iget-object v1, v0, Lnzc;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1589
    :cond_0
    return-object v0

    .line 1584
    :cond_1
    new-instance v0, Ldkg;

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Ldkg;-><init>(Landroid/content/Context;ILjava/lang/String;J)V

    .line 1585
    invoke-virtual {v0}, Ldkg;->l()V

    .line 1587
    invoke-static {p0, v2, p2, p3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d(Landroid/content/Context;IJ)Lnzc;

    move-result-object v0

    .line 1588
    if-eqz v0, :cond_2

    iget-object v1, v0, Lnzc;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1593
    :cond_2
    new-instance v0, Ljfg;

    const-string v1, "Couldn\'t find video stream URL."

    invoke-direct {v0, v1}, Ljfg;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 285
    invoke-static {p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 287
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 289
    const-string v3, "version"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 290
    sget-object v3, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c:[Ljava/lang/String;

    :goto_0
    const/4 v4, 0x2

    if-ge v0, v4, :cond_1

    aget-object v4, v3, v0

    .line 291
    invoke-interface {v1, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 292
    invoke-interface {v2, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 290
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 295
    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 296
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 954
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 955
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 958
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 959
    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "gaia_id"

    .line 960
    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 959
    invoke-static {v4, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 961
    const-string v2, "UPDATE all_tiles SET media_attr = (media_attr | 1048576) WHERE view_id = ?  AND type = 2 AND view_order > 50100 AND (NOT media_attr & 4194304) AND cluster_id = ?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 962
    invoke-virtual {v1, v5, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 963
    const/4 v0, 0x2

    invoke-virtual {v1, v0, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 964
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 968
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->e(Landroid/content/Context;I)V

    .line 969
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljdw;->a(ILjava/lang/String;)V

    .line 970
    return-void
.end method

.method public static a(Landroid/content/Context;IZLdvj;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 836
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljfb;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 867
    :goto_0
    return-void

    .line 840
    :cond_0
    new-instance v0, Ldvh;

    invoke-direct {v0, p0, p1, p2, p3}, Ldvh;-><init>(Landroid/content/Context;IZLdvj;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 866
    invoke-virtual {v0, v1}, Ldvh;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Map;Landroid/util/SparseArray;Lkfd;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljdz;",
            "Ljej;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljdz;",
            ">;>;",
            "Lkfd;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1234
    move v2, v3

    :goto_0
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1235
    invoke-virtual {p2, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 1236
    invoke-virtual {p2, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1237
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1238
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljdz;

    iget-object v6, v1, Ljdz;->b:Ljava/lang/String;

    .line 1241
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v7, v1, [J

    move v4, v3

    :goto_1
    array-length v1, v7

    if-ge v4, v1, :cond_0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljdz;

    iget-object v8, v1, Ljdz;->b:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-static {v8}, Llsk;->b(Z)V

    iget-object v1, v1, Ljdz;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    aput-wide v8, v7, v4

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1243
    :cond_0
    new-instance v4, Ldkg;

    invoke-direct {v4, p0, v5, v6, v7}, Ldkg;-><init>(Landroid/content/Context;ILjava/lang/String;[J)V

    .line 1245
    const-string v1, "MovieMakerUtils"

    const/4 v6, 0x4

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1246
    const-string v6, "Requesting photo data for %s photo IDs"

    const/4 v1, 0x1

    new-array v7, v1, [Ljava/lang/Object;

    .line 1247
    invoke-virtual {p2, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v3

    .line 1246
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 1249
    :cond_1
    invoke-interface {p3, v4}, Lkfd;->a(Lkff;)V

    .line 1252
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdz;

    .line 1253
    iget-object v4, v0, Ljdz;->a:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {p0, v5, v6, v7}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;IJ)Ljej;

    move-result-object v4

    .line 1254
    if-eqz v4, :cond_2

    .line 1255
    invoke-interface {p1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1234
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 1260
    :cond_4
    return-void
.end method

.method public static a(J)Z
    .locals 4

    .prologue
    .line 278
    const-wide/32 v0, 0x100000

    and-long/2addr v0, p0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lnym;)Z
    .locals 2

    .prologue
    .line 1333
    if-eqz p2, :cond_0

    iget v0, p2, Lnym;->J:I

    .line 1334
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1335
    const/4 v0, 0x1

    .line 1345
    :goto_0
    return v0

    .line 1338
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1341
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    .line 1342
    invoke-virtual {v0, p1}, Ljem;->b(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 1345
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1664
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1665
    const-string v2, "expire"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1667
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1669
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1671
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v6, 0x258

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    .line 1677
    :cond_0
    :goto_0
    return v0

    .line 1673
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1382
    invoke-static {p1}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->c(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    invoke-static {p0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Landroid/content/Context;)Ljdw;
    .locals 1

    .prologue
    .line 89
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;IJ)Ljej;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1199
    .line 1200
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1202
    const-string v1, "all_photos"

    sget-object v2, Ldvl;->a:[Ljava/lang/String;

    const-string v3, "photo_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    .line 1203
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    const-string v8, "1"

    move-object v6, v5

    move-object v7, v5

    .line 1202
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1207
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1208
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1218
    :goto_0
    return-object v5

    .line 1210
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1211
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1212
    invoke-static {v2, v3}, Ljvj;->a(J)Ljac;

    move-result-object v2

    .line 1213
    invoke-static {p0, v0, v2}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Landroid/net/Uri;

    move-result-object v0

    .line 1214
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1215
    invoke-static {v2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Ljac;)Ljel;

    move-result-object v2

    .line 1216
    new-instance v5, Ljej;

    invoke-direct {v5, v0, v2}, Ljej;-><init>(Landroid/net/Uri;Ljel;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1218
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static b(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 303
    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;IZ)V

    .line 304
    invoke-static {p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 306
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 307
    return-void
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 980
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 981
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 983
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 984
    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "gaia_id"

    .line 985
    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 984
    invoke-static {v4, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 986
    const-string v2, "UPDATE all_tiles SET media_attr = (media_attr & -1048577) WHERE view_id = ?  AND type = 2 AND view_order > 50100 AND (NOT media_attr & 4194304) AND cluster_id = ?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 987
    invoke-virtual {v1, v5, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 988
    const/4 v0, 0x2

    invoke-virtual {v1, v0, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 989
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 993
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->e(Landroid/content/Context;I)V

    .line 994
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljdw;->b(ILjava/lang/String;)V

    .line 995
    return-void
.end method

.method public static b(Landroid/content/Context;IZ)V
    .locals 3

    .prologue
    .line 545
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 548
    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    monitor-enter v1

    .line 549
    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    .line 550
    if-ltz v0, :cond_2

    .line 551
    sget-object v2, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->removeAt(I)V

    .line 553
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554
    const-string v0, "notification"

    .line 555
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 556
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1000c8

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 558
    if-eqz p2, :cond_0

    .line 559
    new-instance v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;-><init>()V

    .line 560
    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g(Landroid/content/Context;I)V

    goto :goto_0

    .line 553
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static c(Landroid/content/Context;Landroid/net/Uri;)J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 1553
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1566
    :cond_0
    :goto_0
    return-wide v0

    .line 1557
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->b(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 1558
    if-eqz v2, :cond_0

    .line 1562
    invoke-static {p0, v2}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->c(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v2

    .line 1563
    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    move-wide v0, v2

    .line 1564
    goto :goto_0
.end method

.method public static c(Landroid/content/Context;IJ)Landroid/graphics/Bitmap;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1600
    .line 1601
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1602
    const-string v1, "all_photos"

    sget-object v2, Ldvk;->a:[Ljava/lang/String;

    const-string v3, "photo_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    .line 1603
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    const-string v8, "1"

    move-object v6, v5

    move-object v7, v5

    .line 1602
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1606
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 1609
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1630
    :cond_0
    :goto_0
    return-object v5

    .line 1612
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1614
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1617
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1618
    const-class v1, Lizs;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lizs;

    .line 1619
    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p0, v0, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v7

    .line 1621
    const/4 v8, 0x0

    const/16 v9, 0x500

    const/16 v10, 0x2d0

    const/4 v11, 0x0

    :try_start_2
    invoke-virtual/range {v6 .. v11}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_2
    .catch Lkdn; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lkde; {:try_start_2 .. :try_end_2} :catch_1

    move-object v5, v0

    goto :goto_0

    .line 1614
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1624
    :catch_0
    move-exception v0

    .line 1625
    const-string v1, "MovieMakerUtils"

    const-string v2, "Couldn\'t load bitmap"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1626
    :catch_1
    move-exception v0

    .line 1627
    const-string v1, "MovieMakerUtils"

    const-string v2, "Couldn\'t load bitmap"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static c(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1328
    const-string v0, "mm_utils\\"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1041
    .line 1042
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1043
    const-string v2, "media_cache INNER JOIN all_tiles ON (media_cache.image_url =all_tiles.image_url)"

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "photo_id"

    aput-object v4, v3, v7

    const-string v4, "filename = ? AND type = 4 AND http_status = \'200\' AND representation_type IN(2, 8)"

    new-array v5, v1, [Ljava/lang/String;

    aput-object p2, v5, v7

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1053
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 1057
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1059
    :goto_0
    return-object v6

    .line 1057
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static c(Landroid/content/Context;I)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljeb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 331
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d(Landroid/content/Context;ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 334
    :try_start_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 335
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v8

    .line 336
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    new-instance v0, Ljeb;

    new-instance v1, Ljed;

    const-string v2, "gaia_id"

    .line 338
    invoke-interface {v8, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 339
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    .line 340
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    .line 341
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    .line 342
    invoke-interface {v7, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(J)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Ljeb;-><init>(Ljed;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 337
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 345
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 348
    return-object v6
.end method

.method public static c(Landroid/content/Context;IZ)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 818
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;IZLdvj;)V

    .line 819
    return-void
.end method

.method private static d(Landroid/content/Context;ILjava/lang/String;)Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 375
    .line 376
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 380
    const-class v1, Lhei;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 381
    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 382
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "gaia_id"

    .line 383
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    .line 382
    invoke-static {v6, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 385
    if-eqz p2, :cond_1

    .line 386
    const-string v4, "view_id = ?  AND type = 2 AND view_order > 50100 AND (NOT media_attr & 4194304) AND cluster_id = ?"

    .line 387
    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    aput-object v1, v5, v6

    aput-object p2, v5, v7

    .line 394
    :goto_0
    const/4 v1, 0x1

    :try_start_0
    const-string v2, "all_tiles"

    sget-object v3, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "view_order"

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 398
    const-string v1, "MovieMakerUtils"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    const-string v1, "[getClusterCursor], selection: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 401
    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a:[Ljava/lang/String;

    .line 402
    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1c

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", selectionArgs: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", columns: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    :cond_0
    return-object v0

    .line 389
    :cond_1
    const-string v4, "view_id = ?  AND type = 2 AND view_order > 50100 AND (NOT media_attr & 4194304)"

    .line 390
    new-array v5, v7, [Ljava/lang/String;

    aput-object v1, v5, v6

    goto :goto_0

    .line 398
    :catchall_0
    move-exception v0

    const-string v1, "MovieMakerUtils"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 399
    const-string v1, "[getClusterCursor], selection: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 401
    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a:[Ljava/lang/String;

    .line 402
    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1c

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", selectionArgs: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", columns: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    :cond_2
    throw v0
.end method

.method private static d(Landroid/content/Context;IJ)Lnzc;
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1638
    .line 1639
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1640
    const-string v1, "all_photos"

    sget-object v2, Ldvn;->a:[Ljava/lang/String;

    const-string v3, "photo_id = ? AND data NOT NULL"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    .line 1642
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    const-string v8, "1"

    move-object v6, v5

    move-object v7, v5

    .line 1640
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1646
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1647
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1659
    :goto_0
    return-object v5

    .line 1650
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1652
    :try_start_2
    new-instance v2, Lnym;

    invoke-direct {v2}, Lnym;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;

    .line 1653
    invoke-static {v0}, Llmz;->b(Lnym;)Lnzc;
    :try_end_2
    .catch Loxt; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    .line 1659
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1654
    :catch_0
    move-exception v0

    .line 1655
    :try_start_3
    const-string v2, "MovieMakerUtils"

    const-string v3, "Unable to parse proto while getting video stream URL."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1656
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static d(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 414
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljfb;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 427
    :goto_0
    return-void

    .line 417
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 418
    sget-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 420
    :cond_1
    new-instance v0, Ldvf;

    invoke-direct {v0, p0, p1}, Ldvf;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 426
    invoke-virtual {v0, v1}, Ldvf;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d:Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private static e(Landroid/content/Context;ILjava/lang/String;)Landroid/util/Pair;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    const-wide/16 v10, -0x1

    .line 1009
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 1033
    :goto_0
    return-object v0

    .line 1013
    :cond_0
    sget-object v3, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b:[Ljava/lang/String;

    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "all_tiles"

    const-string v4, "view_id = ? AND type = 4"

    new-array v5, v1, [Ljava/lang/String;

    aput-object p2, v5, v7

    const-string v8, "view_order"

    move-object v7, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    move-wide v0, v10

    move-wide v4, v10

    .line 1018
    :goto_1
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1019
    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 1020
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-eqz v7, :cond_5

    .line 1021
    cmp-long v7, v4, v10

    if-eqz v7, :cond_1

    cmp-long v7, v4, v2

    if-gez v7, :cond_2

    :cond_1
    move-wide v4, v2

    .line 1024
    :cond_2
    cmp-long v7, v0, v10

    if-eqz v7, :cond_3

    cmp-long v7, v0, v2

    if-lez v7, :cond_5

    :cond_3
    move-wide v0, v2

    move-wide v2, v4

    :goto_2
    move-wide v4, v2

    .line 1028
    goto :goto_1

    .line 1030
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1033
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 1030
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    move-wide v2, v4

    goto :goto_2
.end method

.method public static e(Landroid/content/Context;I)V
    .locals 11

    .prologue
    .line 435
    .line 436
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljfb;->b(I)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 437
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 438
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljfb;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljem;->c()Z

    move-result v2

    if-nez v2, :cond_7

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_a

    .line 454
    :cond_2
    :goto_2
    return-void

    .line 436
    :cond_3
    invoke-static {p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v0, "aam_cluster_max_view_timestamp"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;IZ)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->e(Landroid/content/Context;ILjava/lang/String;)Landroid/util/Pair;

    move-result-object v7

    iget-object v1, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v1

    invoke-virtual {v1}, Ljem;->g()Ljdw;

    move-result-object v1

    invoke-interface {v1, p1, v0, v8, v9}, Ljdw;->a(ILjava/lang/String;J)V

    cmp-long v1, v8, v4

    if-lez v1, :cond_4

    new-instance v1, Landroid/util/Pair;

    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-direct {v1, v7, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "new_aam_count"

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    new-instance v0, Ldvg;

    invoke-direct {v0}, Ldvg;-><init>()V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    move v2, v0

    :goto_4
    if-ge v2, v6, :cond_6

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_6
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    invoke-interface {v0, p1, v1, v4, v5}, Ljdw;->a(ILjava/util/List;J)V

    goto/16 :goto_0

    .line 438
    :cond_7
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->f(Landroid/content/Context;I)I

    move-result v2

    if-nez v2, :cond_8

    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_8
    invoke-static {p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-string v3, "last_new_system_app_dismiss_time"

    const-wide/16 v6, 0x0

    invoke-interface {v2, v3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0}, Ljem;->d()J

    move-result-wide v6

    sub-long/2addr v4, v2

    cmp-long v0, v4, v6

    if-lez v0, :cond_9

    const/4 v0, 0x1

    :goto_5
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v4

    invoke-virtual {v4}, Ljem;->g()Ljdw;

    move-result-object v4

    invoke-interface {v4, p1, v0, v2, v3}, Ljdw;->a(IZJ)V

    goto/16 :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    .line 441
    :cond_a
    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 442
    invoke-static {p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(I)I

    move-result v2

    if-eq v2, v0, :cond_2

    .line 445
    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;ILjava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 446
    sget-object v2, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    monitor-enter v2

    :try_start_0
    sget-object v4, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    xor-int/lit8 v5, v0, -0x1

    invoke-virtual {v4, p1, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v4

    if-ne v4, v0, :cond_b

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 447
    :goto_6
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Ljem;->i()V

    .line 449
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    .line 450
    invoke-interface {v0, p1, v1}, Ljdw;->a(ILjava/util/List;)V

    goto/16 :goto_2

    .line 446
    :cond_b
    :try_start_1
    sget-object v4, Lcom/google/android/apps/plus/content/MovieMakerUtils;->g:Landroid/util/SparseIntArray;

    invoke-virtual {v4, p1, v0}, Landroid/util/SparseIntArray;->put(II)V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v0, 0x0

    if-eqz v1, :cond_e

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x1

    move v10, v0

    move v0, v2

    move v2, v10

    :goto_7
    const v5, 0x7f110077

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v0, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/google/android/apps/plus/content/MovieMakerUtils$OnNewAamNotificationReceiver;

    invoke-direct {v6, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "com.google.android.apps.plus.ON_NEW_AAM_NOTIFICATION_DISMISSED"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "account_id"

    invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    const/high16 v7, 0x8000000

    invoke-static {p0, p1, v6, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    const/4 v7, 0x1

    if-ne v0, v7, :cond_c

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/google/android/apps/plus/content/MovieMakerUtils$OnNewAamNotificationReceiver;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v8, "com.google.android.apps.plus.ON_NEW_AAM_NOTIFICATION_VIEW_SINGLE"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "account_id"

    invoke-virtual {v7, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "CLUSTER_ID_TO_VIEW"

    invoke-virtual {v7, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :goto_8
    const/high16 v7, 0x8000000

    invoke-static {p0, p1, v0, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    const v0, 0x7f0a0b0a

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v8, Lbq;

    invoke-direct {v8}, Lbq;-><init>()V

    invoke-virtual {v8, v3}, Lbq;->a(Landroid/graphics/Bitmap;)Lbq;

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    new-instance v3, Lbs;

    invoke-direct {v3, p0}, Lbs;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    invoke-virtual {v3, v5}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    const-string v4, "account_name"

    invoke-interface {v0, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbs;->c(Ljava/lang/CharSequence;)Lbs;

    invoke-virtual {v3, v2}, Lbs;->b(Z)Lbs;

    const v0, 0x7f020368

    invoke-virtual {v3, v0}, Lbs;->a(I)Lbs;

    invoke-virtual {v3, v6}, Lbs;->b(Landroid/app/PendingIntent;)Lbs;

    invoke-virtual {v3, v7}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    const/4 v0, -0x1

    invoke-virtual {v3, v0}, Lbs;->c(I)Lbs;

    invoke-virtual {v3, v8}, Lbs;->a(Lce;)Lbs;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f1000c8

    invoke-virtual {v3}, Lbs;->c()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_6

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_c
    new-instance v0, Landroid/content/Intent;

    const-class v7, Lcom/google/android/apps/plus/content/MovieMakerUtils$OnNewAamNotificationReceiver;

    invoke-direct {v0, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "com.google.android.apps.plus.ON_NEW_AAM_NOTIFICATION_VIEW_ALL"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-class v7, Lcom/google/android/apps/plus/content/MovieMakerUtils$OnNewAamNotificationReceiver;

    invoke-virtual {v0, p0, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v7, "account_id"

    invoke-virtual {v0, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_8

    .line 452
    :cond_d
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;IZ)V

    goto/16 :goto_2

    :cond_e
    move v10, v0

    move v0, v2

    move v2, v10

    goto/16 :goto_7
.end method

.method public static f(Landroid/content/Context;I)I
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 797
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    .line 802
    :goto_0
    return v0

    .line 800
    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 802
    const-string v2, "new_aam_count"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method private static f(Landroid/content/Context;ILjava/lang/String;)Landroid/net/Uri;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1426
    invoke-static {p0, p2, p1}, Ljvd;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1427
    if-eqz v0, :cond_0

    .line 1428
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1429
    const-string v1, "content"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "media"

    .line 1430
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1432
    invoke-static {v0, p0}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/net/Uri;Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 1433
    if-eqz v1, :cond_0

    move-object v5, v0

    .line 1477
    :goto_0
    return-object v5

    .line 1441
    :cond_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1443
    const-string v1, "media_cache"

    sget-object v2, Lcom/google/android/apps/plus/content/MovieMakerUtils;->e:[Ljava/lang/String;

    const-string v3, "image_url = ? AND representation_type IN(2, 8)"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1448
    :try_start_0
    const-class v0, Lkdv;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v3

    .line 1449
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1450
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1451
    const/4 v0, 0x1

    .line 1452
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    sget-object v0, Ljac;->b:Ljac;

    move-object v1, v0

    .line 1455
    :goto_1
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1456
    const-string v6, "content"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1458
    invoke-static {v0, p0}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/net/Uri;Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 1459
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_3

    .line 1474
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-object v5, v0

    goto :goto_0

    .line 1452
    :cond_1
    :try_start_1
    sget-object v0, Ljac;->a:Ljac;

    move-object v1, v0

    goto :goto_1

    .line 1465
    :cond_2
    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1466
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1468
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1469
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 1474
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static h(Landroid/content/Context;I)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 925
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v1

    .line 926
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljfb;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 928
    invoke-virtual {v1}, Ljem;->e()Z

    move-result v2

    if-nez v2, :cond_1

    .line 943
    :cond_0
    :goto_0
    return v0

    .line 931
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->f(Landroid/content/Context;I)I

    move-result v2

    if-eqz v2, :cond_0

    .line 935
    invoke-static {p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 937
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 938
    const-string v3, "last_new_aam_in_app_dismiss_time"

    const-wide/16 v6, 0x0

    invoke-interface {v2, v3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 939
    invoke-virtual {v1}, Ljem;->f()J

    move-result-wide v6

    .line 940
    sub-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    const/4 v0, 0x1

    .line 941
    :cond_2
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v1

    invoke-virtual {v1}, Ljem;->g()Ljdw;

    move-result-object v1

    .line 942
    invoke-interface {v1, p1, v0, v2, v3}, Ljdw;->b(IZJ)V

    goto :goto_0
.end method

.method public static synthetic i(Landroid/content/Context;I)J
    .locals 5

    .prologue
    .line 89
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    const-wide v2, 0x7fffffffffffffffL

    :cond_0
    return-wide v2

    :cond_1
    const-wide/16 v0, 0x0

    const/4 v2, 0x1

    invoke-static {p0, p1, v2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;IZ)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->e(Landroid/content/Context;ILjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_0
.end method


# virtual methods
.method public g(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 878
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljfb;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 886
    :goto_0
    return-void

    .line 881
    :cond_0
    invoke-static {p2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 883
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 884
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_new_system_app_dismiss_time"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 885
    invoke-static {p1}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    invoke-interface {v0, p2}, Ljdw;->b(I)V

    goto :goto_0
.end method
