.class public Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"


# static fields
.field private static a:Landroid/graphics/drawable/Drawable;

.field private static b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    .line 38
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->i(I)V

    .line 39
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a(F)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020474

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a:Landroid/graphics/drawable/Drawable;

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->b:Landroid/graphics/drawable/Drawable;

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->i(I)V

    .line 39
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a(F)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020474

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a:Landroid/graphics/drawable/Drawable;

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->b:Landroid/graphics/drawable/Drawable;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->i(I)V

    .line 39
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a(F)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020474

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a:Landroid/graphics/drawable/Drawable;

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->b:Landroid/graphics/drawable/Drawable;

    .line 35
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->draw(Landroid/graphics/Canvas;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getWidth()I

    move-result v2

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getHeight()I

    move-result v3

    .line 61
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    .line 62
    if-le v3, v4, :cond_0

    sub-int v0, v3, v4

    .line 64
    :goto_0
    sget-object v5, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v1, v1, v2, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 65
    sget-object v4, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v1, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 67
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 68
    const/4 v1, 0x0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 69
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 70
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 72
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 73
    return-void

    :cond_0
    move v0, v1

    .line 62
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 46
    const v0, 0x7fffffff

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->resolveSize(II)I

    move-result v0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getPaddingLeft()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 48
    int-to-float v1, v1

    const v2, 0x3fe38e39

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 50
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 51
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 52
    invoke-super {p0, v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->onMeasure(II)V

    .line 53
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a(II)V

    .line 54
    return-void
.end method
