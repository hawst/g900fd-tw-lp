.class public Lcom/google/android/apps/plus/fragments/EditCommentFragment;
.super Leah;
.source "PG"


# instance fields
.field private N:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Leah;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "comment_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->N:Ljava/lang/String;

    .line 30
    invoke-super {p0, p1, p2, p3}, Leah;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->e()Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 7

    .prologue
    .line 40
    invoke-super {p0}, Leah;->b()V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->n()Lz;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 48
    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->e()Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d()Ljava/lang/String;

    move-result-object v4

    .line 51
    const-string v2, "activity_id"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 52
    const-string v5, "tile_id"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 54
    const-string v6, "photo_id"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 55
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->N:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->c(I)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->N:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->c(I)V

    goto :goto_0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f0a0794

    return v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const-string v0, "comment"

    return-object v0
.end method
