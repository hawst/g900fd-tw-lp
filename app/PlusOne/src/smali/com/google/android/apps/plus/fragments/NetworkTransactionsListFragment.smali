.class public Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;
.super Leam;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leam",
        "<",
        "Landroid/widget/ListView;",
        "Lfaq;",
        ">;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private P:Lhee;

.field private Q:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Leam;-><init>()V

    return-void
.end method

.method private V()Ljava/lang/String;
    .locals 3

    .prologue
    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->O:Lhyd;

    check-cast v0, Lfaq;

    invoke-virtual {v0}, Lfaq;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 138
    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    .line 139
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 140
    invoke-static {v0}, Lfas;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->at:Llnl;

    invoke-static {v2, v0}, Lfas;->b(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->at:Llnl;

    invoke-static {v2, v0}, Lfas;->a(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    const-string v2, "**************************\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 150
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lhmw;->o:Lhmw;

    return-object v0
.end method

.method public U()V
    .locals 5

    .prologue
    .line 154
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SENDTO"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 155
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const-string v2, "android.intent.extra.SUBJECT"

    const-string v3, "Debug Network stats for:"

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->P:Lhee;

    .line 157
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v4, "account_name"

    invoke-interface {v0, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    const-string v0, "android.intent.extra.TEXT"

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->V()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->a(Landroid/content/Intent;)V

    .line 161
    return-void

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 74
    const v0, 0x7f0400f5

    invoke-super {p0, p1, p2, p3, v0}, Leam;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 77
    new-instance v0, Lfaq;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->at:Llnl;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lfaq;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->O:Lhyd;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->N:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->O:Lhyd;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 81
    const v0, 0x7f0a0901

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->a(Landroid/view/View;I)V

    .line 84
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->d(Landroid/view/View;)V

    .line 86
    return-object v1
.end method

.method public bridge synthetic a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1, p2, p3, p4}, Leam;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    :goto_0
    return-object v4

    .line 98
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->g:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->P:Lhee;

    .line 99
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 98
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    .line 100
    new-instance v0, Lhye;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->n()Lz;

    move-result-object v1

    sget-object v3, Lfat;->a:[Ljava/lang/String;

    const-string v6, "time DESC"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->O:Lhyd;

    check-cast v0, Lfaq;

    invoke-virtual {v0, p1}, Lfaq;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->c()V

    .line 114
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->h(Landroid/view/View;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->g(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Leam;->a(Landroid/os/Bundle;)V

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->e(Z)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 66
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 34
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic aO_()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Leam;->aO_()V

    return-void
.end method

.method public bridge synthetic ae_()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Leam;->ae_()V

    return-void
.end method

.method public b(Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->Q:Landroid/widget/ProgressBar;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->Q:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->a(Landroid/widget/ProgressBar;)V

    .line 49
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Leam;->c(Landroid/os/Bundle;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->P:Lhee;

    .line 55
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->e(Landroid/content/Context;I)V

    .line 133
    return-void
.end method

.method public bridge synthetic e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0, p1}, Leam;->e(Landroid/os/Bundle;)V

    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0, p1, p2, p3, p4}, Leam;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Leam;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public bridge synthetic z()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Leam;->z()V

    return-void
.end method
