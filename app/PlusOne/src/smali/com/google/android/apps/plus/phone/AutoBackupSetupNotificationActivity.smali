.class public Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lheg;


# instance fields
.field private final g:Livx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Llon;-><init>()V

    .line 34
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    const-string v1, "active-photos-account"

    .line 35
    invoke-virtual {v0, v1}, Livx;->a(Ljava/lang/String;)Livx;

    move-result-object v0

    .line 36
    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->g:Livx;

    .line 34
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->e:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 42
    return-void
.end method

.method public a(ZIIII)V
    .locals 3

    .prologue
    .line 86
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 87
    const-class v0, Lhpu;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->g:Livx;

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lhpu;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Leyq;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skip_interstitials"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 89
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->finish()V

    .line 90
    return-void

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->g:Livx;

    invoke-virtual {v0}, Livx;->d()I

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsLauncherActivity;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 46
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 48
    if-nez p1, :cond_0

    .line 49
    const/4 v0, 0x4

    new-instance v2, Lhml;

    invoke-direct {v2}, Lhml;-><init>()V

    new-instance v3, Lhmk;

    sget-object v4, Lomt;->h:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    new-instance v3, Lhmk;

    sget-object v4, Lomz;->b:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    invoke-static {p0, v0, v2}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 52
    :cond_0
    const-class v0, Lhpu;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 57
    invoke-virtual {v0}, Lhpu;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 58
    invoke-virtual {v0}, Lhpu;->e()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 63
    :goto_0
    if-eq v0, v1, :cond_3

    .line 66
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsLauncherActivity;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 65
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->finish()V

    .line 80
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 60
    goto :goto_0

    .line 71
    :cond_3
    if-nez p1, :cond_1

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->g:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    .line 73
    invoke-virtual {v1}, Liwg;->a()Liwg;

    move-result-object v1

    const-class v2, Liwl;

    new-instance v3, Liwm;

    invoke-direct {v3}, Liwm;-><init>()V

    const v4, 0x7f0a05f4

    .line 75
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v3

    .line 76
    invoke-virtual {v3}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 74
    invoke-virtual {v1, v2, v3}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    goto :goto_1
.end method
