.class public Lcom/google/android/apps/plus/phone/PeopleSearchActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lelv;
.implements Lhmq;


# instance fields
.field private final e:Lhee;

.field private f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Lloa;-><init>()V

    .line 40
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 42
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->x:Llnh;

    .line 43
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 46
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->x:Llnh;

    .line 47
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->e:Lhee;

    .line 46
    return-void
.end method

.method private l()Z
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "picker_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lhmw;->k:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 147
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 149
    return-void
.end method

.method public a(Ljava/lang/String;Lhxc;)V
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 121
    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const-string v1, "circle_data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 123
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->finish()V

    .line 125
    return-void

    .line 126
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljqs;)V
    .locals 3

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 101
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const-string v1, "person_data"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 103
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->finish()V

    .line 115
    :goto_0
    return-void

    .line 105
    :cond_0
    if-eqz p2, :cond_1

    .line 106
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 108
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 109
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 112
    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 113
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Lu;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    if-eqz v0, :cond_0

    .line 63
    check-cast p1, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    .line 64
    const v0, 0x7f100199

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 65
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->b(Landroid/widget/ProgressBar;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->a(Lelv;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v4, "search_circles_usage"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->c(I)V

    .line 71
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->l()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->a(Z)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v4, "search_pub_profiles_enabled"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->b(Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v4, "search_phones_enabled"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->c(Z)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v4, "search_plus_pages_enabled"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->i(Z)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v2, "search_in_circles_enabled"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->j(Z)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v1, "query"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->a(Ljava/lang/String;)V

    .line 82
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 71
    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f040167

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->setContentView(I)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->h()Loo;

    move-result-object v0

    .line 57
    const v1, 0x7f0a0812

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 58
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lloa;->onResume()V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->f:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Z()V

    goto :goto_0
.end method
