.class public Lcom/google/android/apps/plus/phone/PhotosIntroActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhmq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lloa;-><init>()V

    .line 35
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 36
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->x:Llnh;

    .line 37
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 38
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 39
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lhmw;->h:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 80
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 82
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 83
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 84
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 54
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "photos_intro_shown"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {p0}, Leqd;->a(Landroid/content/Context;)V

    .line 55
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1004d3

    if-ne v0, v1, :cond_1

    .line 56
    new-instance v0, Livy;

    invoke-direct {v0, p0}, Livy;-><init>(Landroid/content/Context;)V

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Liwl;

    .line 58
    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    const-class v2, Lixj;

    .line 59
    invoke-virtual {v1, v2}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Livy;->a(Liwg;)Livy;

    move-result-object v0

    .line 60
    invoke-static {p0, v3, v4}, Leyq;->a(Landroid/content/Context;ILandroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Livy;->a(Landroid/content/Intent;)Livy;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Livy;->a()Landroid/content/Intent;

    move-result-object v0

    .line 56
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->startActivity(Landroid/content/Intent;)V

    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->finish()V

    .line 67
    return-void

    .line 62
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1004d2

    if-ne v0, v1, :cond_0

    .line 63
    invoke-static {p0, v3, v4}, Leyq;->a(Landroid/content/Context;ILandroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f040196

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->setContentView(I)V

    .line 46
    const v0, 0x7f1004d3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const v0, 0x7f1004d2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method
