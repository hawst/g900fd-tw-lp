.class public Lcom/google/android/apps/plus/fragments/EditPostFragment;
.super Leah;
.source "PG"


# instance fields
.field private N:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Leah;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3}, Leah;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->e()Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v1

    const v2, 0x7f0a07cd

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setHint(I)V

    .line 38
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-super {p0, p1}, Leah;->a(Landroid/os/Bundle;)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 29
    const-string v2, "reshare"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "is_only_text"

    .line 30
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->N:Z

    .line 31
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->N:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->e()Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 7

    .prologue
    .line 48
    invoke-super {p0}, Leah;->b()V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->n()Lz;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 56
    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->e()Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d()Ljava/lang/String;

    move-result-object v3

    .line 59
    const-string v4, "activity_id"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 60
    const-string v5, "reshare"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 62
    invoke-static {v0, v2, v4, v3, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->c(I)V

    goto :goto_0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 68
    const v0, 0x7f0a0798

    return v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string v0, "content"

    return-object v0
.end method
