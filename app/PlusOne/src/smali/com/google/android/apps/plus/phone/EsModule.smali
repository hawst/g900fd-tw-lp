.class public Lcom/google/android/apps/plus/phone/EsModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v6, 0x1bb

    .line 181
    const-class v0, Lkez;

    if-ne p2, v0, :cond_1

    .line 182
    const-class v0, Lkez;

    new-instance v1, Ldqq;

    invoke-direct {v1, p1}, Ldqq;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 483
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    const-class v0, Lkfn;

    if-ne p2, v0, :cond_2

    .line 184
    sget-object v0, Liak;->a:Lloy;

    .line 185
    const-class v0, Lkfn;

    new-instance v1, Leui;

    invoke-direct {v1}, Leui;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 189
    :cond_2
    const-class v0, Lkbu;

    if-ne p2, v0, :cond_3

    .line 191
    const-class v0, Lkbu;

    new-instance v1, Lfeq;

    invoke-direct {v1}, Lfeq;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 192
    :cond_3
    const-class v0, Lkfe;

    if-ne p2, v0, :cond_4

    .line 193
    const-class v0, Lkfe;

    new-instance v1, Leuf;

    invoke-direct {v1}, Leuf;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 194
    sget-object v0, Leug;->a:Lloz;

    goto :goto_0

    .line 195
    :cond_4
    const-class v0, Lhpo;

    if-ne p2, v0, :cond_5

    .line 198
    const-class v0, Lhpo;

    invoke-static {p1}, Lhpo;->a(Landroid/content/Context;)Lhpo;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 199
    :cond_5
    const-class v0, Lhzr;

    if-ne p2, v0, :cond_6

    .line 200
    const-class v0, Lhzr;

    new-instance v1, Ldri;

    invoke-direct {v1}, Ldri;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 201
    :cond_6
    const-class v0, Lief;

    if-ne p2, v0, :cond_7

    .line 202
    const-class v0, Lief;

    invoke-static {}, Ldxd;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/util/Collection;)V

    .line 203
    const-class v0, Lief;

    sget-object v1, Leuq;->a:Lief;

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 204
    const-class v0, Lief;

    sget-object v1, Lejy;->N:Lief;

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 205
    const-class v0, Lief;

    sget-object v1, Lfhz;->a:Lief;

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 206
    :cond_7
    const-class v0, Lheo;

    if-ne p2, v0, :cond_8

    .line 207
    const-class v1, Lheo;

    new-array v2, v8, [Lheo;

    new-instance v0, Leum;

    invoke-direct {v0}, Leum;-><init>()V

    aput-object v0, v2, v4

    new-instance v0, Ldhv;

    invoke-direct {v0}, Ldhv;-><init>()V

    aput-object v0, v2, v5

    const-class v0, Lfew;

    .line 210
    invoke-virtual {p3, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lheo;

    aput-object v0, v2, v7

    .line 207
    invoke-virtual {p3, v1, v2}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 212
    :cond_8
    const-class v0, Ljfy;

    if-ne p2, v0, :cond_9

    .line 213
    const-class v0, Ljfy;

    const/4 v1, 0x6

    new-array v1, v1, [Ljfy;

    new-instance v2, Lfcz;

    invoke-direct {v2}, Lfcz;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lexu;

    invoke-direct {v2}, Lexu;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Levo;

    invoke-direct {v2}, Levo;-><init>()V

    aput-object v2, v1, v7

    new-instance v2, Lfdh;

    invoke-direct {v2}, Lfdh;-><init>()V

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-instance v3, Lexx;

    invoke-direct {v3}, Lexx;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lfao;

    invoke-direct {v3}, Lfao;-><init>()V

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 221
    :cond_9
    const-class v0, Lfew;

    if-ne p2, v0, :cond_a

    .line 222
    const-class v0, Lfew;

    new-instance v1, Lfew;

    invoke-direct {v1, p1}, Lfew;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 223
    :cond_a
    const-class v0, Ljgd;

    if-ne p2, v0, :cond_b

    .line 224
    const-class v0, Ljgd;

    new-instance v1, Lfap;

    invoke-direct {v1}, Lfap;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 225
    :cond_b
    const-class v0, Lhgn;

    if-ne p2, v0, :cond_c

    .line 226
    const-class v0, Lhgn;

    new-instance v1, Lfep;

    invoke-direct {v1}, Lfep;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 227
    :cond_c
    const-class v0, Ldrz;

    if-ne p2, v0, :cond_d

    .line 228
    const-class v0, Ldrz;

    new-instance v1, Ldrz;

    invoke-direct {v1, p1}, Ldrz;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 229
    :cond_d
    const-class v0, Lixl;

    if-ne p2, v0, :cond_e

    .line 230
    const-class v0, Lixl;

    const-class v1, Ldrz;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 231
    :cond_e
    const-class v0, Liwq;

    if-ne p2, v0, :cond_f

    .line 232
    const-class v0, Liwq;

    const-class v1, Ldrz;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 233
    :cond_f
    const-class v0, Lkzj;

    if-ne p2, v0, :cond_10

    .line 234
    const-class v0, Lkzj;

    new-array v1, v7, [Lkzj;

    new-instance v2, Lcom/google/android/apps/plus/views/UpdateItemCardFactoryExtension;

    invoke-direct {v2}, Lcom/google/android/apps/plus/views/UpdateItemCardFactoryExtension;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lcom/google/android/apps/plus/views/PromoItemCardFactoryExtension;

    invoke-direct {v2}, Lcom/google/android/apps/plus/views/PromoItemCardFactoryExtension;-><init>()V

    aput-object v2, v1, v5

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 238
    :cond_10
    const-class v0, Ligx;

    if-ne p2, v0, :cond_11

    .line 239
    const-class v0, Ligx;

    const/16 v1, 0x9

    new-array v1, v1, [Ligx;

    new-instance v2, Lexc;

    invoke-direct {v2}, Lexc;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Ligt;

    invoke-direct {v2}, Ligt;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Lezx;

    invoke-direct {v2, p1}, Lezx;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v7

    new-instance v2, Lfbq;

    invoke-direct {v2, p1}, Lfbq;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-instance v3, Lesw;

    invoke-direct {v3}, Lesw;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lfeg;

    invoke-direct {v3}, Lfeg;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Lezd;

    invoke-direct {v3}, Lezd;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Lfcn;

    invoke-direct {v3, p1}, Lfcn;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Lfbh;

    invoke-direct {v3}, Lfbh;-><init>()V

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 250
    :cond_11
    const-class v0, Lkte;

    if-ne p2, v0, :cond_12

    .line 251
    const-class v0, Lkte;

    new-instance v1, Leyv;

    invoke-direct {v1, p1}, Leyv;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 252
    :cond_12
    const-class v0, Lkvh;

    if-ne p2, v0, :cond_13

    .line 253
    const-class v0, Lkvh;

    new-instance v1, Leyu;

    invoke-direct {v1, p1}, Leyu;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 254
    :cond_13
    const-class v0, Lloi;

    if-ne p2, v0, :cond_14

    .line 255
    const-class v0, Lloi;

    const/4 v1, 0x5

    new-array v1, v1, [Lloi;

    new-instance v2, Lesk;

    invoke-direct {v2}, Lesk;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lesm;

    invoke-direct {v2}, Lesm;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Ldrk;

    invoke-direct {v2}, Ldrk;-><init>()V

    aput-object v2, v1, v7

    new-instance v2, Leul;

    invoke-direct {v2}, Leul;-><init>()V

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-instance v3, Lfes;

    invoke-direct {v3}, Lfes;-><init>()V

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 262
    :cond_14
    const-class v0, Lkzi;

    if-ne p2, v0, :cond_15

    .line 264
    const-class v0, Lkzi;

    new-instance v1, Ldwl;

    invoke-direct {v1}, Ldwl;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 265
    :cond_15
    const-class v0, Lcsi;

    if-ne p2, v0, :cond_16

    .line 266
    const-class v0, Lcsi;

    new-instance v1, Lcsi;

    invoke-direct {v1}, Lcsi;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 267
    :cond_16
    const-class v0, Lhxk;

    if-ne p2, v0, :cond_17

    .line 268
    const-class v0, Lhxk;

    new-instance v1, Ldqr;

    invoke-direct {v1}, Ldqr;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 269
    :cond_17
    const-class v0, Ljpk;

    if-ne p2, v0, :cond_18

    .line 270
    const-class v0, Ljpk;

    new-instance v1, Lexi;

    invoke-direct {v1}, Lexi;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 279
    :cond_18
    const-class v0, Lhgs;

    if-ne p2, v0, :cond_19

    .line 280
    const-class v0, Lhgs;

    new-instance v1, Lfcg;

    invoke-direct {v1}, Lfcg;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 281
    :cond_19
    const-class v0, Lloc;

    if-ne p2, v0, :cond_1a

    .line 282
    const-class v0, Lloc;

    const/16 v1, 0x13

    new-array v1, v1, [Lloc;

    new-instance v2, Lcua;

    invoke-direct {v2}, Lcua;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lcts;

    invoke-direct {v2}, Lcts;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Lcsf;

    invoke-direct {v2}, Lcsf;-><init>()V

    aput-object v2, v1, v7

    new-instance v2, Leux;

    invoke-direct {v2}, Leux;-><init>()V

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-instance v3, Lfbk;

    invoke-direct {v3}, Lfbk;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lfeb;

    invoke-direct {v3}, Lfeb;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Lcnu;

    invoke-direct {v3}, Lcnu;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Lcnf;

    invoke-direct {v3}, Lcnf;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Lcni;

    invoke-direct {v3}, Lcni;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-instance v3, Lcnk;

    invoke-direct {v3}, Lcnk;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-instance v3, Lcnn;

    invoke-direct {v3}, Lcnn;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-instance v3, Lcnp;

    invoke-direct {v3}, Lcnp;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-instance v3, Lcny;

    invoke-direct {v3}, Lcny;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-instance v3, Lcnl;

    invoke-direct {v3}, Lcnl;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0xe

    new-instance v3, Lcoc;

    invoke-direct {v3}, Lcoc;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-instance v3, Lewg;

    invoke-direct {v3}, Lewg;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0x10

    new-instance v3, Lewi;

    invoke-direct {v3}, Lewi;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0x11

    new-instance v3, Leya;

    invoke-direct {v3}, Leya;-><init>()V

    aput-object v3, v1, v2

    const/16 v2, 0x12

    new-instance v3, Lewd;

    invoke-direct {v3}, Lewd;-><init>()V

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 303
    :cond_1a
    const-class v0, Llop;

    if-ne p2, v0, :cond_1b

    .line 304
    const-class v0, Llop;

    new-array v1, v7, [Llop;

    new-instance v2, Lewg;

    invoke-direct {v2}, Lewg;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lewi;

    invoke-direct {v2}, Lewi;-><init>()V

    aput-object v2, v1, v5

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 308
    :cond_1b
    const-class v0, Lhqa;

    if-ne p2, v0, :cond_1c

    .line 309
    const-class v0, Lhqa;

    new-instance v1, Lhqa;

    invoke-direct {v1, p1}, Lhqa;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 310
    :cond_1c
    const-class v0, Liah;

    if-ne p2, v0, :cond_1d

    .line 311
    const-class v0, Liah;

    new-array v1, v8, [Liah;

    new-instance v2, Ldwb;

    invoke-direct {v2}, Ldwb;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Ldtv;

    invoke-direct {v2}, Ldtv;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Llba;

    invoke-direct {v2}, Llba;-><init>()V

    aput-object v2, v1, v7

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 316
    :cond_1d
    const-class v0, Lcnb;

    if-ne p2, v0, :cond_1e

    .line 317
    const-class v0, Lcnb;

    new-instance v1, Lcnb;

    invoke-direct {v1, p1}, Lcnb;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 318
    :cond_1e
    const-class v0, Lcna;

    if-ne p2, v0, :cond_1f

    .line 319
    const-class v0, Lcna;

    new-array v1, v8, [Lcna;

    new-instance v2, Lcue;

    invoke-direct {v2}, Lcue;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lcug;

    invoke-direct {v2}, Lcug;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Lcui;

    invoke-direct {v2}, Lcui;-><init>()V

    aput-object v2, v1, v7

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 324
    :cond_1f
    const-class v0, Ljbi;

    if-ne p2, v0, :cond_20

    .line 325
    const-class v0, Ljbi;

    new-instance v1, Ldwc;

    invoke-direct {v1, p1}, Ldwc;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 327
    :cond_20
    const-class v0, Ljbj;

    if-ne p2, v0, :cond_21

    .line 328
    const-class v0, Ljbj;

    new-instance v1, Lffc;

    invoke-direct {v1, p1}, Lffc;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 330
    :cond_21
    const-class v0, Lhsa;

    if-ne p2, v0, :cond_22

    .line 331
    const-class v0, Lhsa;

    new-instance v1, Lfch;

    invoke-direct {v1}, Lfch;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 332
    :cond_22
    const-class v0, Lhrw;

    if-ne p2, v0, :cond_23

    .line 333
    const-class v0, Lhrw;

    new-instance v1, Lfci;

    invoke-direct {v1}, Lfci;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 334
    :cond_23
    const-class v0, Lhrt;

    if-ne p2, v0, :cond_24

    .line 335
    const-class v0, Lhrt;

    new-instance v1, Lhrt;

    invoke-direct {v1, p1}, Lhrt;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 336
    :cond_24
    const-class v0, Lihb;

    if-ne p2, v0, :cond_25

    .line 338
    const-class v0, Lihb;

    new-array v1, v7, [Lihb;

    new-instance v2, Letj;

    invoke-direct {v2}, Letj;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Letk;

    invoke-direct {v2}, Letk;-><init>()V

    aput-object v2, v1, v5

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 343
    :cond_25
    const-class v0, Lhru;

    if-ne p2, v0, :cond_26

    .line 344
    const-class v0, Lhru;

    new-instance v1, Lesh;

    invoke-direct {v1, p1}, Lesh;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 345
    :cond_26
    const-class v0, Lhpg;

    if-ne p2, v0, :cond_27

    .line 346
    const-class v0, Lhpg;

    new-instance v1, Lese;

    invoke-direct {v1, p1}, Lese;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 347
    :cond_27
    const-class v0, Lhjd;

    if-ne p2, v0, :cond_28

    .line 348
    const-class v0, Lhjd;

    const/4 v1, 0x4

    new-array v1, v1, [Lhjd;

    new-instance v2, Lhjw;

    const v3, 0x7f10067b

    invoke-direct {v2, v3}, Lhjw;-><init>(I)V

    aput-object v2, v1, v4

    new-instance v2, Ldhz;

    const v3, 0x7f100394

    invoke-direct {v2, v3}, Ldhz;-><init>(I)V

    aput-object v2, v1, v5

    new-instance v2, Lhju;

    const v3, 0x7f100692

    invoke-direct {v2, v3}, Lhju;-><init>(I)V

    aput-object v2, v1, v7

    new-instance v2, Lhju;

    const v3, 0x7f10047f

    invoke-direct {v2, v3}, Lhju;-><init>(I)V

    aput-object v2, v1, v8

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 354
    :cond_28
    const-class v0, Ljmb;

    if-ne p2, v0, :cond_29

    .line 355
    const-class v0, Ljmb;

    new-array v1, v8, [Ljmb;

    new-instance v2, Leur;

    invoke-direct {v2}, Leur;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Leuq;

    invoke-direct {v2}, Leuq;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Leuo;

    invoke-direct {v2}, Leuo;-><init>()V

    aput-object v2, v1, v7

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 360
    :cond_29
    const-class v0, Lkex;

    if-ne p2, v0, :cond_2a

    .line 361
    const-class v0, Lkex;

    new-instance v1, Lexy;

    invoke-direct {v1}, Lexy;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 362
    :cond_2a
    const-class v0, Ljuo;

    if-ne p2, v0, :cond_2b

    .line 363
    const-class v0, Ljuo;

    new-instance v1, Lcom/google/android/apps/plus/service/CastService;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/CastService;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 364
    :cond_2b
    const-class v0, Ljhb;

    if-ne p2, v0, :cond_2c

    .line 365
    const-class v0, Ljhb;

    new-instance v1, Ljhb;

    const-string v2, "enable_quic_transport_layer_android_k"

    invoke-direct {v1, p1, v2, v4}, Ljhb;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 366
    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 367
    const-class v0, Ljhb;

    new-instance v1, Ljhb;

    const-string v2, "enable_quic_pacing"

    invoke-direct {v1, p1, v2, v4}, Ljhb;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 368
    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 370
    :cond_2c
    const-class v0, Lorg/chromium/net/UrlRequestContextConfig;

    if-ne p2, v0, :cond_2e

    .line 371
    new-instance v1, Llnp;

    const-class v0, Ljhb;

    invoke-direct {v1, p1, v0}, Llnp;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 373
    new-instance v0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    invoke-direct {v0}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;-><init>()V

    sget-object v2, Lorg/chromium/net/UrlRequestContextConfig$HttpCache;->c:Lorg/chromium/net/UrlRequestContextConfig$HttpCache;

    const-wide/32 v4, 0x100000

    .line 374
    invoke-virtual {v0, v2, v4, v5}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Lorg/chromium/net/UrlRequestContextConfig$HttpCache;J)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    new-instance v2, Ljava/io/File;

    .line 375
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "cronet_cache"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v2

    const-string v0, "enable_quic_transport_layer_android_k"

    .line 376
    invoke-virtual {v1, v0}, Llnp;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Ljhb;

    invoke-virtual {v0}, Ljhb;->c()Z

    move-result v0

    invoke-virtual {v2, v0}, Lorg/chromium/net/UrlRequestContextConfig;->b(Z)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "www.google.com"

    .line 379
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "www.gstatic.com"

    .line 380
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "www.googleapis.com"

    .line 381
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "lh1.googleusercontent.com"

    .line 382
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "lh2.googleusercontent.com"

    .line 383
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "lh3.googleusercontent.com"

    .line 384
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "lh4.googleusercontent.com"

    .line 385
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "lh5.googleusercontent.com"

    .line 386
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "lh6.googleusercontent.com"

    .line 387
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "sp1.googleusercontent.com"

    .line 388
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "sp2.googleusercontent.com"

    .line 389
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "sp3.googleusercontent.com"

    .line 390
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "sp4.googleusercontent.com"

    .line 391
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "sp5.googleusercontent.com"

    .line 392
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "sp6.googleusercontent.com"

    .line 393
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "www-googleapis-staging.sandbox.google.com"

    .line 394
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "www-googleapis-test.sandbox.google.com"

    .line 395
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    const-string v2, "www-googleapis-staging-daily.sandbox.google.com"

    .line 396
    invoke-virtual {v0, v2, v6, v6}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v2

    .line 397
    const-string v0, "enable_quic_pacing"

    invoke-virtual {v1, v0}, Llnp;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Ljhb;

    invoke-virtual {v0}, Ljhb;->c()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 398
    const-string v0, "PACE"

    invoke-virtual {v2, v0}, Lorg/chromium/net/UrlRequestContextConfig;->b(Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;

    .line 400
    :cond_2d
    const-class v0, Lorg/chromium/net/UrlRequestContextConfig;

    invoke-virtual {p3, v0, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 401
    :cond_2e
    const-class v0, Lkwk;

    if-ne p2, v0, :cond_2f

    .line 402
    const-class v0, Lkwk;

    new-array v1, v7, [Lkwk;

    new-instance v2, Lkwd;

    invoke-direct {v2}, Lkwd;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lkub;

    invoke-direct {v2}, Lkub;-><init>()V

    aput-object v2, v1, v5

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 406
    :cond_2f
    const-class v0, Ljiy;

    if-ne p2, v0, :cond_30

    .line 407
    const-class v0, Ljiy;

    new-instance v1, Leuj;

    invoke-direct {v1}, Leuj;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 408
    :cond_30
    const-class v0, Liad;

    if-ne p2, v0, :cond_31

    .line 409
    const-class v0, Liad;

    new-instance v1, Ldrf;

    invoke-direct {v1}, Ldrf;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 412
    :cond_31
    const-class v0, Llbj;

    if-ne p2, v0, :cond_32

    .line 415
    const-class v0, Llbj;

    new-instance v1, Ldsz;

    invoke-direct {v1}, Ldsz;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 416
    :cond_32
    const-class v0, Llow;

    if-ne p2, v0, :cond_33

    .line 417
    sget-object v0, Llow;->a:Lloy;

    goto/16 :goto_0

    .line 418
    :cond_33
    const-class v0, Lkbw;

    if-ne p2, v0, :cond_34

    .line 425
    const-class v0, Lkbw;

    new-instance v1, Lfcq;

    invoke-direct {v1}, Lfcq;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 426
    :cond_34
    const-class v0, Lhto;

    if-ne p2, v0, :cond_35

    .line 427
    const-class v0, Lhto;

    new-instance v1, Lewj;

    invoke-direct {v1}, Lewj;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 428
    :cond_35
    const-class v0, Lfur;

    if-ne p2, v0, :cond_36

    .line 429
    const-class v0, Lfur;

    new-instance v1, Lfur;

    .line 430
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lfur;-><init>(Landroid/content/ContentResolver;)V

    .line 429
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 431
    :cond_36
    const-class v0, Lkze;

    if-ne p2, v0, :cond_37

    .line 432
    const-class v0, Lkze;

    new-instance v1, Leyw;

    invoke-direct {v1}, Leyw;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 433
    :cond_37
    const-class v0, Lleu;

    if-ne p2, v0, :cond_38

    .line 434
    const-class v0, Lleu;

    const/16 v1, 0xe

    new-array v1, v1, [Lleu;

    new-instance v2, Lfet;

    invoke-direct {v2, p1}, Lfet;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v4

    new-instance v2, Lffd;

    invoke-direct {v2, p1}, Lffd;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v5

    new-instance v2, Lffk;

    invoke-direct {v2, p1}, Lffk;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v7

    new-instance v2, Lffp;

    invoke-direct {v2, p1}, Lffp;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-instance v3, Lffq;

    invoke-direct {v3, p1}, Lffq;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lffs;

    invoke-direct {v3, p1}, Lffs;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Lfga;

    invoke-direct {v3, p1}, Lfga;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Lfhm;

    invoke-direct {v3, p1}, Lfhm;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Lfhn;

    invoke-direct {v3, p1}, Lfhn;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-instance v3, Lfhv;

    invoke-direct {v3, p1}, Lfhv;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-instance v3, Lfia;

    invoke-direct {v3, p1}, Lfia;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-instance v3, Lfig;

    invoke-direct {v3, p1}, Lfig;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-instance v3, Lfii;

    invoke-direct {v3, p1}, Lfii;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-instance v3, Lfiu;

    invoke-direct {v3, p1}, Lfiu;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 449
    :cond_38
    const-class v0, Llet;

    if-ne p2, v0, :cond_39

    .line 450
    const-class v0, Llet;

    new-instance v1, Lfhz;

    invoke-direct {v1, p1}, Lfhz;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 451
    :cond_39
    const-class v0, Liwn;

    if-ne p2, v0, :cond_3a

    .line 452
    const-class v0, Liwn;

    new-instance v1, Leyb;

    invoke-direct {v1, p1}, Leyb;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 453
    :cond_3a
    const-class v0, Lizn;

    if-ne p2, v0, :cond_3b

    .line 454
    const-class v0, Lizn;

    new-instance v1, Lcro;

    invoke-direct {v1}, Lcro;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 455
    :cond_3b
    const-class v0, Lhxb;

    if-ne p2, v0, :cond_3c

    .line 456
    const-class v0, Lhxb;

    new-instance v1, Lfgb;

    invoke-direct {v1}, Lfgb;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 457
    :cond_3c
    const-class v0, Lirr;

    if-ne p2, v0, :cond_3d

    .line 458
    const-class v0, Lirr;

    new-instance v1, Lexj;

    invoke-direct {v1}, Lexj;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 469
    :cond_3d
    const-class v0, Lhka;

    if-ne p2, v0, :cond_3e

    .line 470
    const-class v0, Lhka;

    new-instance v1, Ldia;

    invoke-direct {v1}, Ldia;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 471
    :cond_3e
    const-class v0, Ljba;

    if-ne p2, v0, :cond_3f

    .line 472
    const-class v0, Ljba;

    new-instance v1, Ljax;

    invoke-direct {v1}, Ljax;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 474
    :cond_3f
    const-class v0, Llcq;

    if-ne p2, v0, :cond_40

    .line 475
    const-class v0, Llcq;

    new-instance v1, Llcq;

    invoke-direct {v1, p1}, Llcq;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 476
    :cond_40
    const-class v0, Ljmg;

    if-ne p2, v0, :cond_41

    .line 477
    const-class v0, Ljmg;

    new-instance v1, Lfay;

    invoke-direct {v1}, Lfay;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 478
    :cond_41
    const-class v0, Liwb;

    if-ne p2, v0, :cond_42

    .line 479
    const-class v0, Liwb;

    new-instance v1, Lfab;

    invoke-direct {v1}, Lfab;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 480
    :cond_42
    const-class v0, Lkcn;

    if-ne p2, v0, :cond_0

    .line 481
    const-class v0, Lkcn;

    new-instance v1, Leyc;

    invoke-direct {v1}, Leyc;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0
.end method
