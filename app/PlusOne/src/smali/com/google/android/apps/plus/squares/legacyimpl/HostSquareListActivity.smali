.class public Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Lhee;

.field private final f:Ldie;

.field private g:Lkuf;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 35
    invoke-direct {p0}, Lloa;-><init>()V

    .line 40
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 42
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->x:Llnh;

    .line 43
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 45
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->x:Llnh;

    .line 46
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 47
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 49
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 50
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 52
    new-instance v0, Lhmg;

    sget-object v1, Lomv;->n:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->x:Llnh;

    .line 53
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 55
    new-instance v0, Lhmf;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->y:Llqc;

    invoke-direct {v0, v1}, Lhmf;-><init>(Llqr;)V

    .line 59
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->x:Llnh;

    .line 60
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->e:Lhee;

    .line 61
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->g:Lkuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->g:Lkuf;

    invoke-virtual {v0}, Lkuf;->F_()Lhmw;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhmw;->a:Lhmw;

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 70
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 96
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 97
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 98
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 99
    const v0, 0x7f100680

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 100
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 88
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 104
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 105
    const v1, 0x7f100680

    if-ne v0, v1, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->e:Lhee;

    .line 107
    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 109
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->startActivity(Landroid/content/Intent;)V

    .line 110
    const/4 v0, 0x1

    .line 112
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 76
    if-nez p1, :cond_0

    .line 77
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 78
    new-instance v1, Lkuf;

    invoke-direct {v1}, Lkuf;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->g:Lkuf;

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->g:Lkuf;

    invoke-virtual {v1, v0}, Lkuf;->f(Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->f:Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->g:Lkuf;

    invoke-virtual {v0, v1}, Ldie;->a(Lu;)V

    .line 82
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareListActivity;->setContentView(I)V

    .line 83
    return-void
.end method
