.class public Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lloa;-><init>()V

    .line 28
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 30
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->x:Llnh;

    .line 31
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 33
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->x:Llnh;

    .line 34
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 35
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lhmw;->o:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 67
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f04011d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->setContentView(I)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->h()Loo;

    move-result-object v0

    .line 43
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Loo;->c(Z)V

    .line 44
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f12000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 54
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 55
    const v1, 0x7f10016a

    if-eq v0, v1, :cond_0

    const v1, 0x7f1006ed

    if-ne v0, v1, :cond_1

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;->f()Lae;

    move-result-object v0

    const v1, 0x7f1002f5

    invoke-virtual {v0, v1}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;

    .line 57
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->e(Landroid/view/MenuItem;)V

    .line 58
    const/4 v0, 0x1

    .line 60
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lloa;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
