.class public Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Lloa;-><init>()V

    .line 33
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->x:Llnh;

    .line 34
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 35
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 37
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->x:Llnh;

    .line 38
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 40
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->x:Llnh;

    .line 41
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 42
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 45
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->y:Llqc;

    const v1, 0x7f100143

    invoke-direct {v0, p0, v1}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lhmw;->Q:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 84
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 70
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 71
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 72
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 73
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 62
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 52
    if-nez p1, :cond_0

    .line 53
    new-instance v0, Leql;

    invoke-direct {v0}, Leql;-><init>()V

    .line 54
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 56
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->setContentView(I)V

    .line 57
    return-void
.end method
