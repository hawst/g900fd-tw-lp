.class public Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmm;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Lloa;-><init>()V

    .line 32
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 34
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->x:Llnh;

    .line 35
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 36
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 38
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->x:Llnh;

    .line 39
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 41
    new-instance v0, Lhmf;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->y:Llqc;

    invoke-direct {v0, v1}, Lhmf;-><init>(Llqr;)V

    .line 42
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->x:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 48
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 76
    const v0, 0x7f0a0352

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 77
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 78
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 79
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_profile_tab"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 80
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lhmk;

    sget-object v1, Lomv;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 52
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->setContentView(I)V

    .line 55
    if-nez p1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->f()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    .line 57
    const v1, 0x7f100143

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "person_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lepe;->a(Ljava/lang/String;)Lu;

    move-result-object v2

    .line 57
    invoke-virtual {v0, v1, v2}, Lat;->b(ILu;)Lat;

    .line 59
    invoke-virtual {v0}, Lat;->b()I

    .line 61
    :cond_0
    return-void
.end method
