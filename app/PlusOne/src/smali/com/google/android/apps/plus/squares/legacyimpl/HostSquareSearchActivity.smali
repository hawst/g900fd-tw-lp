.class public Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 41
    invoke-direct {p0}, Lloa;-><init>()V

    .line 46
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 48
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->x:Llnh;

    .line 49
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 51
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->x:Llnh;

    .line 52
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 55
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 56
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 58
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->x:Llnh;

    .line 59
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 61
    new-instance v0, Lhmg;

    sget-object v1, Lomv;->s:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->x:Llnh;

    .line 62
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 64
    new-instance v0, Lhmf;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->y:Llqc;

    invoke-direct {v0, v1}, Lhmf;-><init>(Llqr;)V

    .line 67
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->e:Ldie;

    return-void
.end method

.method public static a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 83
    if-eqz p2, :cond_0

    .line 84
    const-string v1, "query"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    :cond_0
    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lhmw;->J:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 138
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 124
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 125
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 126
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 127
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 115
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 116
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 105
    if-nez p1, :cond_0

    .line 106
    new-instance v0, Lkxb;

    invoke-direct {v0}, Lkxb;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 109
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->setContentView(I)V

    .line 110
    return-void
.end method
