.class public Lcom/google/android/apps/plus/views/EsDrawerLayout;
.super Lms;
.source "PG"


# static fields
.field private static a:I


# instance fields
.field private b:F

.field private c:F

.field private d:Z

.field private e:Z

.field private f:Lfxd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x40800000    # -1.0f

    .line 36
    invoke-direct {p0, p1}, Lms;-><init>(Landroid/content/Context;)V

    .line 29
    iput v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    .line 30
    iput v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    .line 31
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    .line 32
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    .line 48
    sget v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    if-gez v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    .line 37
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x40800000    # -1.0f

    .line 40
    invoke-direct {p0, p1, p2}, Lms;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    .line 30
    iput v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    .line 31
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    .line 32
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    .line 48
    sget v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    if-gez v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    .line 41
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x40800000    # -1.0f

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lms;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    .line 30
    iput v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    .line 31
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    .line 32
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    .line 48
    sget v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    if-gez v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    .line 45
    :cond_0
    return-void
.end method

.method private a(FF)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 165
    iput p1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    .line 166
    iput p2, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    .line 167
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    .line 168
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    .line 169
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(II)V

    .line 170
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    const/4 v1, 0x0

    .line 176
    iput v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    .line 177
    iput v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    .line 178
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    .line 179
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    .line 180
    const/4 v0, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(II)V

    .line 181
    return-void
.end method


# virtual methods
.method public a(Lfxd;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->f:Lfxd;

    .line 55
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 59
    invoke-static {p1}, Lik;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 61
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->g(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 62
    invoke-super {p0, p1}, Lms;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 102
    :cond_0
    :goto_0
    return v2

    .line 65
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 98
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->f()V

    .line 102
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lms;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0

    .line 67
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(FF)V

    goto :goto_1

    .line 72
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 73
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 74
    iget v4, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    cmpg-float v4, v4, v7

    if-ltz v4, :cond_3

    iget v4, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    cmpg-float v4, v4, v7

    if-gez v4, :cond_4

    .line 75
    :cond_3
    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(FF)V

    .line 76
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_1

    .line 78
    :cond_4
    iget v4, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    sub-float v4, v0, v4

    .line 79
    iget v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 81
    mul-float v0, v4, v4

    mul-float v5, v3, v3

    add-float/2addr v0, v5

    sget v5, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    sget v6, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    mul-int/2addr v5, v6

    int-to-float v5, v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_5

    move v0, v1

    .line 84
    :goto_2
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    if-nez v5, :cond_2

    if-eqz v0, :cond_2

    .line 85
    cmpl-float v0, v4, v7

    if-lez v0, :cond_6

    cmpl-float v0, v4, v3

    if-lez v0, :cond_6

    .line 87
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    .line 88
    invoke-virtual {p0, v2, v8}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(II)V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 81
    goto :goto_2

    .line 90
    :cond_6
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    goto :goto_1

    .line 65
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 194
    const/4 v0, 0x0

    .line 196
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lms;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 186
    const/4 v0, 0x0

    .line 188
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lms;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 155
    invoke-super/range {p0 .. p5}, Lms;->onLayout(ZIIII)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->f:Lfxd;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->f:Lfxd;

    invoke-interface {v0, p0}, Lfxd;->a(Lcom/google/android/apps/plus/views/EsDrawerLayout;)V

    .line 159
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 107
    invoke-static {p1}, Lik;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 109
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->g(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 110
    invoke-super {p0, p1}, Lms;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 150
    :goto_0
    return v0

    .line 113
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 146
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->f()V

    .line 150
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lms;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 115
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(FF)V

    goto :goto_1

    .line 120
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 121
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 122
    iget v4, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    cmpg-float v4, v4, v7

    if-ltz v4, :cond_2

    iget v4, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    cmpg-float v4, v4, v7

    if-gez v4, :cond_3

    .line 123
    :cond_2
    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(FF)V

    .line 124
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_1

    .line 126
    :cond_3
    iget v4, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->b:F

    sub-float v4, v0, v4

    .line 127
    iget v0, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->c:F

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 129
    mul-float v0, v4, v4

    mul-float v5, v3, v3

    add-float/2addr v0, v5

    sget v5, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    sget v6, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a:I

    mul-int/2addr v5, v6

    int-to-float v5, v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_4

    move v0, v1

    .line 132
    :goto_2
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    if-nez v5, :cond_1

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    if-nez v5, :cond_1

    if-eqz v0, :cond_1

    .line 133
    cmpl-float v0, v4, v7

    if-lez v0, :cond_5

    cmpl-float v0, v4, v3

    if-lez v0, :cond_5

    .line 135
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->e:Z

    .line 136
    invoke-virtual {p0, v2, v8}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(II)V

    goto :goto_1

    :cond_4
    move v0, v2

    .line 129
    goto :goto_2

    .line 138
    :cond_5
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EsDrawerLayout;->d:Z

    goto :goto_1

    .line 113
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
