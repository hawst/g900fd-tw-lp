.class public Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Libe;


# static fields
.field private static a:Landroid/graphics/drawable/Drawable;

.field private static b:Landroid/graphics/drawable/Drawable;

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Landroid/graphics/drawable/Drawable;

.field private static e:Landroid/graphics/drawable/Drawable;

.field private static f:Ljava/lang/String;


# instance fields
.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    sget-object v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 58
    const v1, 0x7f020070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a:Landroid/graphics/drawable/Drawable;

    .line 59
    const v1, 0x7f02006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->b:Landroid/graphics/drawable/Drawable;

    .line 60
    const v1, 0x7f02006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c:Landroid/graphics/drawable/Drawable;

    .line 61
    const v1, 0x7f02006e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->d:Landroid/graphics/drawable/Drawable;

    .line 62
    const v1, 0x7f02006d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->e:Landroid/graphics/drawable/Drawable;

    .line 63
    const v1, 0x7f0a09ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->f:Ljava/lang/String;

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->h:Landroid/view/View;

    return-object v0
.end method

.method public a(ZLjava/lang/String;)Landroid/view/View;
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->i:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->j:Landroid/widget/TextView;

    if-eqz p2, :cond_2

    :goto_1
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->i:Landroid/view/View;

    return-object v0

    .line 133
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 134
    :cond_2
    sget-object p2, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->f:Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Landroid/view/View;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Libd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v0

    .line 147
    instance-of v2, v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    if-eqz v2, :cond_0

    .line 148
    invoke-static {v0, p1}, Llii;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 150
    new-instance v2, Libd;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->c()Ljava/lang/String;

    move-result-object v3

    .line 151
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->d()Ljava/lang/String;

    move-result-object v4

    .line 153
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->e()I

    move-result v0

    .line 152
    invoke-static {v0}, Ldib;->b(I)I

    move-result v0

    invoke-direct {v2, v3, v4, v0}, Libd;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 150
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    :cond_0
    return-object v1
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setPadding(IIII)V

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->g:Landroid/view/View;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 105
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c(Z)V

    .line 106
    return-void

    .line 104
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->g:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 95
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c(Z)V

    .line 96
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 113
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->g:Landroid/view/View;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->e:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c(Z)V

    .line 115
    return-void

    .line 113
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->g:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 119
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c(Z)V

    .line 120
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a(ZLjava/lang/String;)Landroid/view/View;

    .line 124
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 70
    const v0, 0x7f1001e5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->g:Landroid/view/View;

    .line 71
    const v0, 0x7f100183

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->h:Landroid/view/View;

    .line 72
    const v0, 0x7f100493

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->i:Landroid/view/View;

    .line 73
    const v0, 0x7f1001ec

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->j:Landroid/widget/TextView;

    .line 74
    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->h:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/AbsListView$RecyclerListener;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->h:Landroid/view/View;

    check-cast v0, Landroid/widget/AbsListView$RecyclerListener;

    invoke-interface {v0, p1}, Landroid/widget/AbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 82
    return-void
.end method
