.class public Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;
.implements Llhl;


# instance fields
.field private final e:Ldie;

.field private f:Leha;

.field private g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 50
    invoke-direct {p0}, Lloa;-><init>()V

    .line 59
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 61
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->x:Llnh;

    .line 62
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 64
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->x:Llnh;

    .line 65
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 66
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 68
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 69
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 71
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->x:Llnh;

    .line 72
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 74
    new-instance v0, Ldxn;

    new-instance v1, Lfcv;

    invoke-direct {v1, p0}, Lfcv;-><init>(Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;)V

    invoke-direct {v0, v1}, Ldxn;-><init>(Ldxm;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->x:Llnh;

    .line 91
    invoke-virtual {v0, v1}, Ldxn;->a(Llnh;)V

    .line 94
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 219
    sget-object v0, Lhmw;->M:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 213
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 215
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 201
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 202
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 203
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 204
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 191
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 192
    const v0, 0x7f0a0803

    invoke-virtual {p1, v0}, Loo;->c(I)V

    .line 193
    return-void
.end method

.method public a(Lu;)V
    .locals 2

    .prologue
    .line 160
    invoke-super {p0, p1}, Lloa;->a(Lu;)V

    .line 161
    instance-of v0, p1, Leha;

    if-eqz v0, :cond_0

    .line 162
    check-cast p1, Leha;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->f:Leha;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->f:Leha;

    iget v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    invoke-virtual {v0, v1}, Leha;->c(I)V

    .line 165
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 224
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public d(I)V
    .locals 4

    .prologue
    .line 169
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 170
    const-string v0, "extra_activity_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v0, "extra_related_topic_index"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 172
    iget v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    if-eq v0, p1, :cond_1

    .line 173
    iput p1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->f:Leha;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->f:Leha;

    iget v2, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    invoke-virtual {v0, v2}, Leha;->c(I)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->x:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->aj:Lhmv;

    .line 178
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 179
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 177
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 185
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(I)V

    .line 186
    return-void

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->x:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p0}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->ai:Lhmv;

    .line 182
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 183
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 181
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 107
    if-nez p1, :cond_0

    .line 108
    new-instance v0, Leha;

    invoke-direct {v0}, Leha;-><init>()V

    .line 109
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->e:Ldie;

    invoke-virtual {v2, v0}, Ldie;->a(Lu;)V

    .line 111
    :cond_0
    const v0, 0x7f0401cb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->setContentView(I)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 115
    if-eqz p1, :cond_1

    .line 116
    const-string v0, "current_tab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    .line 117
    const-string v0, "activity_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->h:Ljava/lang/String;

    .line 123
    :goto_0
    const v0, 0x7f100551

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 124
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f1005fc

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(Llhl;)V

    .line 127
    const-string v0, "relateds"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhua;

    .line 128
    invoke-interface {v0}, Lhua;->a()I

    move-result v2

    .line 130
    :goto_1
    if-ge v1, v2, :cond_2

    .line 131
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v4, 0x7f0400c7

    .line 132
    invoke-interface {v0, v1}, Lhua;->a(I)Ljava/lang/String;

    move-result-object v5

    .line 131
    invoke-virtual {v3, v4, v5, v1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;I)V

    .line 130
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 119
    :cond_1
    const-string v0, "tab"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    .line 120
    const-string v0, "activity_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->h:Ljava/lang/String;

    goto :goto_0

    .line 135
    :cond_2
    iget v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    if-le v2, v0, :cond_3

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->g:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    iget v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(I)V

    .line 138
    :cond_3
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0}, Lloa;->onResume()V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->f:Leha;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->f:Leha;

    iget v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    invoke-virtual {v0, v1}, Leha;->c(I)V

    .line 149
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0, p1}, Lloa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 154
    const-string v0, "current_tab"

    iget v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 155
    const-string v0, "activity_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    return-void
.end method
