.class public Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;
.super Landroid/app/IntentService;
.source "PG"


# instance fields
.field private a:Landroid/content/Intent;

.field private b:Lijk;

.field private c:Lily;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "GcmLocationReportingSuggestionService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 48
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    invoke-interface {v0}, Lijk;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->a:Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 78
    invoke-interface {v0, v1}, Lhei;->b(Ljava/lang/String;)I

    move-result v1

    .line 79
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    invoke-interface {v0}, Lijk;->c()V

    goto :goto_0

    .line 84
    :cond_2
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->c:Lily;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    invoke-interface {v0, v2, v1}, Lily;->a(Lijk;Landroid/accounts/Account;)Lijq;

    move-result-object v0

    invoke-interface {v0}, Lijq;->a()Lijs;

    move-result-object v0

    check-cast v0, Limf;

    .line 92
    invoke-interface {v0}, Limf;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 93
    const-class v0, Lhms;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->dc:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->Q:Lhmw;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v1}, Leyq;->c(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.libraries.social.notifications.FROM_NOTIFICATION"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "account_id"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v2, Lbs;

    invoke-direct {v2, p0}, Lbs;-><init>(Landroid/content/Context;)V

    const v3, 0x7f02027f

    invoke-virtual {v2, v3}, Lbs;->a(I)Lbs;

    move-result-object v2

    const v3, 0x7f0a03ee

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    move-result-object v2

    const v3, 0x7f0a03ef

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    move-result-object v2

    const v3, 0x7f0a03ed

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lbs;->e(Ljava/lang/CharSequence;)Lbs;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbs;->a(J)Lbs;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lbs;->b(Z)Lbs;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    move-result-object v0

    invoke-virtual {v0}, Lbs;->c()Landroid/app/Notification;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f10009e

    const/4 v3, 0x0

    invoke-static {p0, v1, v0, v2, v3}, Lfhu;->a(Landroid/content/Context;ILandroid/app/Notification;IZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    invoke-interface {v0}, Lijk;->c()V

    goto/16 :goto_0

    .line 96
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    invoke-interface {v0}, Lijk;->c()V

    goto/16 :goto_0

    .line 100
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    invoke-interface {v1}, Lijk;->c()V

    throw v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 52
    const-string v0, "gaia_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->a:Landroid/content/Intent;

    .line 54
    const-class v0, Lily;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lily;

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->c:Lily;

    .line 55
    const-class v0, Lijl;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lijl;

    const-class v1, Lilz;

    .line 56
    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lijj;

    invoke-interface {v0, v1}, Lijl;->a(Lijj;)Lijl;

    move-result-object v0

    .line 57
    invoke-interface {v0}, Lijl;->a()Lijk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->b:Lijk;

    invoke-interface {v0}, Lijk;->a()Liiw;

    move-result-object v0

    .line 59
    invoke-interface {v0}, Liiw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/GcmLocationReportingSuggestionService;->a()V

    .line 64
    :cond_0
    invoke-static {p1}, Lcom/google/android/libraries/social/gcm/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 65
    return-void
.end method
