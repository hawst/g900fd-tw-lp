.class public Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lfad;
.implements Lhob;


# instance fields
.field private g:I

.field private h:I

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lhoc;

.field private final k:Lcmz;

.field private l:Lhov;

.field private final m:Ljava/lang/Runnable;

.field private n:Lhox;

.field private o:Lhox;

.field private final p:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Llon;-><init>()V

    .line 72
    new-instance v0, Lhoc;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Landroid/app/Activity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->j:Lhoc;

    .line 74
    new-instance v0, Lcmz;

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcmz;-><init>(Lz;Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->k:Lcmz;

    .line 79
    new-instance v0, Lhov;

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l()Llqr;

    move-result-object v1

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    .line 81
    new-instance v0, Lcsy;

    invoke-direct {v0, p0}, Lcsy;-><init>(Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->m:Ljava/lang/Runnable;

    .line 104
    new-instance v0, Lcta;

    invoke-direct {v0, p0}, Lcta;-><init>(Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->p:Ljava/lang/Runnable;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;)Lcmz;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->k:Lcmz;

    return-object v0
.end method

.method private a(Lhmv;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 296
    .line 297
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 298
    if-eq v1, v2, :cond_0

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->e:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 301
    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 299
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 304
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcsz;

    invoke-direct {v0, p0, p1}, Lcsz;-><init>(Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->j()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->k()V

    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 290
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->setResult(ILandroid/content/Intent;)V

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->finish()V

    .line 292
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->m:Ljava/lang/Runnable;

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->n:Lhox;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->n:Lhox;

    invoke-virtual {v0, v1}, Lhov;->a(Lhox;)V

    .line 313
    :cond_0
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 316
    iget v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 317
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->f()Lae;

    move-result-object v0

    const-string v1, "manual_awesome_selector"

    .line 318
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-nez v0, :cond_0

    .line 319
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->n()Z

    move-result v0

    invoke-static {v0}, Lduf;->a(Z)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->n()Z

    move-result v0

    invoke-static {v0}, Lduf;->a(Z)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    invoke-static {v0}, Lfac;->a(Ljava/util/ArrayList;)Lfac;

    move-result-object v0

    .line 324
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->f()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    .line 325
    const-string v2, "manual_awesome_selector"

    invoke-virtual {v1, v0, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    .line 326
    invoke-virtual {v1}, Lat;->c()I

    .line 328
    :cond_0
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->o:Lhox;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->o:Lhox;

    invoke-virtual {v0, v1}, Lhov;->a(Lhox;)V

    .line 350
    :cond_0
    return-void
.end method

.method private n()Z
    .locals 3

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_movie"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x1

    .line 251
    iput v4, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->f()Lae;

    move-result-object v0

    const-string v1, "manual_awesome_selector"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 255
    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {v0}, Lt;->a()V

    .line 259
    :cond_0
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 260
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 261
    const-string v1, "manual_awesome_activity_return"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 263
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->setResult(ILandroid/content/Intent;)V

    .line 264
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->finish()V

    .line 287
    :goto_0
    return-void

    .line 266
    :cond_1
    iget v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    invoke-static {v0}, Lduf;->a(I)Lhmv;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->a(Lhmv;)V

    .line 267
    iput p1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    .line 269
    invoke-static {p0, v0, v1}, Lduf;->a(Landroid/content/Context;Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v0

    .line 268
    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 270
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 273
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "cluster_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "cluster_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    iget v3, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    .line 275
    invoke-static {p0, v0, v1, v2, v3}, Lduf;->a(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;I)Landroid/content/Intent;

    move-result-object v0

    .line 274
    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 281
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    iget v2, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    .line 282
    invoke-static {p0, v0, v1, v2}, Lduf;->a(Landroid/content/Context;ILjava/util/ArrayList;I)Landroid/content/Intent;

    move-result-object v0

    .line 281
    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 117
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->e:Llnh;

    const-class v1, Lhoc;

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->j:Lhoc;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 119
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 200
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->j()V

    .line 202
    const-string v0, "CreateMediaBundleTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 203
    const/4 v0, 0x0

    .line 204
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "hint_message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "hint_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 208
    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 210
    :cond_1
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 211
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    const-class v0, Ljgn;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    .line 213
    invoke-interface {v0}, Ljgn;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    .line 215
    invoke-static {p0, v0, v1}, Lduf;->b(Landroid/content/Context;Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 223
    :cond_2
    :goto_0
    sget-object v0, Lhmv;->eS:Lhmv;

    invoke-direct {p0, v0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->a(Lhmv;)V

    .line 224
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i()V

    .line 247
    :goto_1
    return-void

    .line 219
    :cond_3
    const v0, 0x7f0a0b3a

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 226
    :cond_4
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result_media"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 227
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 228
    const-string v1, "manual_awesome_activity_return"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 231
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "result_media"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 232
    const-string v2, "result_media"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 233
    sget-object v1, Lhmv;->eR:Lhmv;

    invoke-direct {p0, v1}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->a(Lhmv;)V

    .line 234
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->setResult(ILandroid/content/Intent;)V

    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->finish()V

    goto :goto_1

    .line 238
    :cond_5
    const-string v0, "ReadPhotosFeaturesTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 242
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->m()V

    .line 243
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->k()V

    goto :goto_1

    .line 246
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i()V

    goto :goto_1
.end method

.method public h()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 160
    iget v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    if-nez v0, :cond_1

    .line 161
    iput v2, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    .line 162
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i()V

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 164
    iput v2, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 170
    if-eq p2, v2, :cond_0

    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i()V

    .line 195
    :goto_0
    return-void

    .line 174
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    if-eqz p3, :cond_2

    const-string v0, "shareables"

    .line 175
    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 176
    sget-object v0, Lhmv;->eQ:Lhmv;

    invoke-direct {p0, v0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->a(Lhmv;)V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 179
    const-string v0, "shareables"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 181
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 182
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizr;

    .line 183
    invoke-interface {v0}, Lizr;->f()Lizu;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 185
    :cond_1
    new-instance v0, Ldom;

    iget v3, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    invoke-direct {v0, p0, v1, v2, v3}, Ldom;-><init>(Landroid/content/Context;ILjava/util/List;I)V

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->j:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    invoke-static {p0, v0, v1}, Lduf;->c(Landroid/content/Context;Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v0

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    .line 191
    invoke-direct {p0, v0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->b(Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v0

    const-wide/16 v2, 0x12c

    .line 190
    invoke-virtual {v1, v0, v2, v3}, Lhov;->a(Ljava/lang/Runnable;J)Lhox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->n:Lhox;

    goto :goto_0

    .line 193
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->j:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 125
    iput v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    .line 127
    if-eqz p1, :cond_0

    .line 128
    const-string v0, "selection_dialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    .line 129
    const-string v0, "render_type"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    .line 131
    const-string v0, "manual_awesome_types"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    .line 133
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->m()V

    .line 155
    invoke-super {p0}, Llon;->onPause()V

    .line 156
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 147
    invoke-super {p0}, Llon;->onResume()V

    .line 148
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->n()Z

    move-result v0

    invoke-static {v0}, Lduf;->a(Z)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->j:Lhoc;

    const-string v1, "ReadPhotosFeaturesTask"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->p:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lhov;->a(Ljava/lang/Runnable;J)Lhox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->o:Lhox;

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->j:Lhoc;

    new-instance v2, Ldpt;

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/4 v5, 0x2

    aput v5, v3, v4

    invoke-direct {v2, p0, v0, v3}, Ldpt;-><init>(Landroid/content/Context;I[I)V

    invoke-virtual {v1, v2}, Lhoc;->b(Lhny;)V

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->l:Lhov;

    const v1, 0x7f0a0a02

    invoke-virtual {p0, v1}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->b(Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Lhov;->a(Ljava/lang/Runnable;J)Lhox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->n:Lhox;

    .line 149
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->k()V

    .line 150
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0, p1}, Llon;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 138
    const-string v0, "selection_dialog"

    iget v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 139
    const-string v0, "render_type"

    iget v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "manual_awesome_types"

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 143
    :cond_0
    return-void
.end method
