.class public Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Lhee;

.field private final f:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 52
    invoke-direct {p0}, Lloa;-><init>()V

    .line 57
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 58
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 60
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->x:Llnh;

    .line 61
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 63
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->x:Llnh;

    .line 64
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 65
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 67
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 68
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 71
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->x:Llnh;

    .line 72
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->e:Lhee;

    .line 74
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lhmw;->ag:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 79
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 82
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 84
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 86
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v1

    const-class v2, Lcsm;

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "cluster_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljvj;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcsb;

    invoke-direct {v0}, Lcsb;-><init>()V

    :goto_0
    invoke-virtual {v1, v2, v0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->x:Llnh;

    const-class v1, Lcnt;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    .line 92
    const-string v1, "Albums"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcnt;->a(Ljava/lang/String;I)V

    .line 94
    return-void

    .line 87
    :cond_0
    new-instance v0, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    goto :goto_0
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 158
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 160
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 161
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 162
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 149
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 150
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 176
    return-void
.end method

.method public b(Lda;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "cluster_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->e:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 105
    if-eqz v0, :cond_5

    .line 106
    invoke-static {v0}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-static {v0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    const-string v3, "profile"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 109
    invoke-static {p0, v1}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 110
    const-string v3, "g:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x2

    invoke-static {p0, v1, v0, v4, v2}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 129
    :goto_1
    return-void

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_1
    const-string v3, "posts"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    .line 114
    invoke-static {p0, v1}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 115
    const-string v3, "g:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v4, v2}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 118
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 120
    if-eqz v0, :cond_4

    .line 121
    invoke-static {p0, v1}, Leyq;->j(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_1

    .line 123
    :cond_4
    invoke-static {p0, v1}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_1

    .line 127
    :cond_5
    invoke-static {p0, v1}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_1
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x2

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 139
    if-nez p1, :cond_0

    .line 140
    new-instance v0, Lehb;

    invoke-direct {v0}, Lehb;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->f:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 143
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;->setContentView(I)V

    .line 144
    return-void
.end method
