.class public Lcom/google/android/apps/plus/service/SlideshowService;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Landroid/os/Bundle;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Landroid/os/PowerManager$WakeLock;

.field private h:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    .line 95
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/SlideshowService;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->b:Landroid/os/Bundle;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/SlideshowService;Landroid/os/Bundle;)Ldf;
    .locals 14

    .prologue
    .line 45
    const-string v0, "account_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "view_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v0, "tile_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "photo_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v0, "oob_only"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    const-string v0, "show_oob_tile"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    const-string v0, "all_photos_row_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    const-string v0, "all_photos_offset"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcpw;

    const/4 v3, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/16 v6, 0x2710

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcpw;-><init>(Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/Long;III)V

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->e:Z

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const-string v0, "shareables"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "shareables"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    :cond_1
    const-string v0, "photo_ref"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lizu;

    new-instance v0, Lezk;

    const-string v3, "filter"

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v6, 0x0

    move-object v5, v9

    invoke-direct/range {v0 .. v6}, Lezk;-><init>(Landroid/content/Context;Ljava/util/List;ILizu;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lfbo;

    const-string v3, "filter"

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    move-object v3, v9

    move-object v5, v10

    invoke-direct/range {v0 .. v8}, Lfbo;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/SlideshowService;Lcpg;)Lizu;
    .locals 2

    .prologue
    .line 45
    invoke-static {p1}, Lcpe;->a(Lcpg;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p1, Lcpg;->f:Lizu;

    invoke-virtual {v1}, Lizu;->g()Ljac;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a()V
    .locals 2

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljuk;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    invoke-virtual {v0}, Ljuk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    monitor-exit p0

    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 336
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 376
    invoke-static {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 377
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;I)V
    .locals 1

    .prologue
    .line 380
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/SlideshowService;->b(Landroid/content/Context;Landroid/os/Bundle;I)Landroid/content/Intent;

    move-result-object v0

    .line 381
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 382
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/SlideshowService;Ljava/lang/Object;)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 45
    iget v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v0, Ljuk;

    invoke-static {v3, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljuk;

    new-instance v2, Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->b:Landroid/os/Bundle;

    invoke-direct {v2, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    instance-of v0, p1, Landroid/database/Cursor;

    if-eqz v0, :cond_7

    check-cast p1, Landroid/database/Cursor;

    iget v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->c()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v5}, Ljuk;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->e:Z

    if-eqz v0, :cond_3

    invoke-static {v3, p1}, Lezk;->a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;

    move-result-object v1

    const-string v0, "photo_ref"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v3, p1}, Lezk;->a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;

    move-result-object v0

    :goto_1
    iget v3, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    invoke-virtual {v5, v2, v3, v1, v0}, Ljuk;->a(Landroid/os/Bundle;ILizu;Lizu;)V

    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->d()V

    const/16 v0, 0x1388

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/service/SlideshowService;->a(I)V

    goto :goto_0

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    invoke-static {v3, p1}, Lfbo;->a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SlideshowService;->b:Landroid/os/Bundle;

    const-string v3, "photo_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "photo_id"

    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/service/SlideshowService;->b:Landroid/os/Bundle;

    const-string v3, "tile_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "tile_id"

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {p0, p1}, Lfbo;->a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;

    move-result-object v6

    :cond_6
    iget v1, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    invoke-virtual {v5, v2, v1, v0, v6}, Ljuk;->a(Landroid/os/Bundle;ILizu;Lizu;)V

    goto :goto_2

    :cond_7
    instance-of v0, p1, Lcpu;

    if-eqz v0, :cond_1

    check-cast p1, Lcpu;

    iget v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    invoke-virtual {p1}, Lcpu;->a()I

    move-result v1

    if-ge v0, v1, :cond_8

    move v0, v8

    :goto_3
    if-nez v0, :cond_9

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->c()V

    goto :goto_0

    :cond_8
    move v0, v9

    goto :goto_3

    :cond_9
    invoke-virtual {v5}, Ljuk;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    invoke-virtual {p1, v0}, Lcpu;->a(I)J

    move-result-wide v10

    iget v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1}, Lcpu;->a()I

    move-result v1

    if-ge v0, v1, :cond_b

    move v7, v8

    :goto_4
    const-string v0, "all_photos_row_id"

    invoke-virtual {v2, v0, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget v4, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    new-instance v0, Lfip;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfip;-><init>(Lcom/google/android/apps/plus/service/SlideshowService;Landroid/os/Bundle;Landroid/content/Context;ILjuk;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Long;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v9

    if-eqz v7, :cond_a

    iget v2, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Lcpu;->a(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    :cond_a
    aput-object v6, v1, v8

    invoke-virtual {v0, v1}, Lfip;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_2

    :cond_b
    move v7, v9

    goto :goto_4
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 385
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/SlideshowService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 386
    const-string v1, "com.google.android.apps.photos.STOP_SLIDESHOW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 387
    return-object v0
.end method

.method public static b(Landroid/content/Context;Landroid/os/Bundle;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 392
    invoke-static {p0}, Ldhv;->b(Landroid/content/Context;)I

    move-result v0

    .line 393
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const-string v1, "account_id"

    .line 394
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 395
    const-string v1, "account_id"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 398
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/SlideshowService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 399
    const-string v1, "com.google.android.apps.photos.START_SLIDESHOW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 400
    const-string v1, "slideshow_arguments"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 401
    const-string v1, "slideshow_position"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 403
    return-object v0
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_1
    monitor-exit p0

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/service/SlideshowService;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->d:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/service/SlideshowService;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    return v0
.end method

.method public static c(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 407
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/SlideshowService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 408
    const-string v1, "com.google.android.apps.photos.STOP_CASTING"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 409
    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->d:Z

    .line 203
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->b()V

    .line 206
    return-void

    .line 205
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->b()V

    throw v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 213
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.photos.SLIDESHOW_STATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 214
    const-string v1, "slideshow_playing"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/service/SlideshowService;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 215
    const-string v1, "slideshow_position"

    iget v2, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldr;->a(Landroid/content/Intent;)Z

    .line 217
    return-void
.end method

.method public static d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 413
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/SlideshowService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.photos.QUERY_STATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 414
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 415
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 122
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lfio;

    invoke-direct {v1, p0}, Lfio;-><init>(Lcom/google/android/apps/plus/service/SlideshowService;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->a:Landroid/os/Handler;

    .line 123
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SlideshowService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 124
    const-string v1, "SLIDE_SHOW_WAKE_LOCK"

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->g:Landroid/os/PowerManager$WakeLock;

    .line 126
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SlideshowService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 127
    const-string v1, "SLIDE_SHOW_WIFI_LOCK"

    invoke-virtual {v0, v2, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->h:Landroid/net/wifi/WifiManager$WifiLock;

    .line 128
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->d:Z

    if-eqz v0, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->c()V

    .line 158
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    const-string v0, "com.google.android.apps.photos.START_SLIDESHOW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->a()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->c()V

    :cond_0
    const-string v0, "slideshow_arguments"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->b:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->b:Landroid/os/Bundle;

    const-string v3, "view_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->e:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->b:Landroid/os/Bundle;

    const-string v3, "all_photos_row_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->f:Z

    const-string v0, "slideshow_position"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->c:I

    iput-boolean v1, p0, Lcom/google/android/apps/plus/service/SlideshowService;->d:Z

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/service/SlideshowService;->a(I)V

    .line 150
    :cond_1
    :goto_1
    const/4 v0, 0x2

    return v0

    :cond_2
    move v0, v2

    .line 136
    goto :goto_0

    .line 137
    :cond_3
    const-string v0, "com.google.android.apps.photos.STOP_SLIDESHOW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->c()V

    .line 139
    invoke-virtual {p0, p3}, Lcom/google/android/apps/plus/service/SlideshowService;->stopSelfResult(I)Z

    goto :goto_1

    .line 140
    :cond_4
    const-string v0, "com.google.android.apps.photos.QUERY_STATE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 141
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->d()V

    .line 142
    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/SlideshowService;->d:Z

    if-nez v0, :cond_1

    .line 143
    invoke-virtual {p0, p3}, Lcom/google/android/apps/plus/service/SlideshowService;->stopSelfResult(I)Z

    goto :goto_1

    .line 145
    :cond_5
    const-string v0, "com.google.android.apps.photos.STOP_CASTING"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->c()V

    .line 147
    invoke-virtual {p0, p3}, Lcom/google/android/apps/plus/service/SlideshowService;->stopSelfResult(I)Z

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SlideshowService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljuk;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    invoke-virtual {v0}, Ljuk;->c()V

    goto :goto_1
.end method
