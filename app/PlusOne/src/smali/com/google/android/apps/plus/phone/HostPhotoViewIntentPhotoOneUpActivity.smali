.class public Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;
.super Leyk;
.source "PG"


# static fields
.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# instance fields
.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "bucket_id"

    aput-object v1, v0, v2

    const-string v1, "case when _data LIKE \'%/DCIM/%\' then 1 else 0 end"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->f:[Ljava/lang/String;

    .line 35
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "bucket_id"

    aput-object v1, v0, v2

    const-string v1, "case when _data LIKE \'%/DCIM/%\' then 1 else 0 end"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Leyk;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 47
    invoke-super {p0, p1}, Leyk;->a(Landroid/os/Bundle;)V

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->x:Llnh;

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->y:Llqc;

    new-instance v4, Ldhg;

    invoke-direct {v4}, Ldhg;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->x:Llnh;

    const-class v1, Lder;

    new-instance v2, Lder;

    invoke-direct {v2}, Lder;-><init>()V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 51
    return-void
.end method

.method public b(Lda;)V
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->h:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 136
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v1

    .line 138
    const-string v2, "starting_tab_index"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 140
    invoke-virtual {p1, v1}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 142
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->h:Ljava/lang/String;

    invoke-static {v1}, Ljvj;->l(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    invoke-static {p0, v0}, Leyq;->e(Landroid/content/Context;I)Leyr;

    move-result-object v1

    .line 144
    invoke-virtual {v1}, Leyr;->a()Landroid/content/Intent;

    move-result-object v1

    .line 143
    invoke-virtual {p1, v1}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->h:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;)Ljuj;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    .line 145
    invoke-virtual {p1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    invoke-super {p0, p1}, Leyk;->b(Lda;)V

    goto :goto_0
.end method

.method protected l()Lu;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 55
    new-instance v8, Ldex;

    invoke-direct {v8}, Ldex;-><init>()V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 58
    invoke-virtual {v9}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 59
    const-string v0, "filter"

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 61
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 62
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->e:Lhee;

    invoke-interface {v2}, Lhee;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    const-string v2, "account_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->e:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-virtual {v10, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 65
    :cond_0
    const-string v2, "filter"

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    const-string v0, "external"

    invoke-virtual {v10, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 73
    invoke-static {v1}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 76
    invoke-static {v1}, Llsb;->c(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->f:[Ljava/lang/String;

    :goto_0
    const-string v5, "_id DESC LIMIT 1"

    move-object v4, v3

    .line 75
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 82
    if-eqz v2, :cond_1

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v6, :cond_4

    .line 84
    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->h:Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    .line 93
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 98
    :cond_2
    :goto_2
    const-string v0, "com.android.camera.action.REVIEW"

    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 99
    const-string v2, "prevent_delete"

    if-nez v0, :cond_6

    move v0, v6

    :goto_3
    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    :goto_4
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_5
    if-eqz v0, :cond_9

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 112
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p0, v0, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    .line 117
    :goto_6
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->h:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 118
    const-string v1, "photo_ref"

    invoke-virtual {v10, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 119
    const-string v0, "view_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->h:Ljava/lang/String;

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v0, "picker_mode"

    invoke-virtual {v10, v0, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 126
    :goto_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 128
    invoke-virtual {v8, v10}, Lu;->f(Landroid/os/Bundle;)V

    .line 129
    return-object v8

    .line 76
    :cond_3
    sget-object v2, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->g:[Ljava/lang/String;

    goto :goto_0

    .line 86
    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljvj;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotoViewIntentPhotoOneUpActivity;->h:Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 92
    :catch_0
    move-exception v0

    if-eqz v2, :cond_2

    .line 93
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 92
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_5

    .line 93
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    :cond_6
    move v0, v7

    .line 99
    goto :goto_3

    .line 103
    :cond_7
    const-string v0, "disable_up_button"

    invoke-virtual {v10, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 104
    const-string v0, "prevent_edit"

    invoke-virtual {v10, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    const-string v0, "prevent_share"

    invoke-virtual {v10, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    const-string v0, "prevent_delete"

    invoke-virtual {v10, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_4

    :cond_8
    move-object v0, v3

    .line 109
    goto :goto_5

    .line 114
    :cond_9
    sget-object v0, Ljac;->a:Ljac;

    invoke-static {p0, v1, v0}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    goto :goto_6

    .line 122
    :cond_a
    const-string v1, "photo_ref"

    invoke-virtual {v10, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 123
    const-string v0, "picker_mode"

    const/4 v1, 0x3

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_7
.end method
