.class public Lcom/google/android/apps/plus/views/AutoBackupBarView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Llke;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/view/View;

.field private d:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private e:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/ProgressBar;

.field private l:Lizu;

.field private m:I

.field private n:I

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Lfwd;

.field private r:Lhei;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->m:I

    .line 86
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a()V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->m:I

    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a()V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->m:I

    .line 96
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a()V

    .line 97
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->r:Lhei;

    .line 101
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 285
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    return-void
.end method


# virtual methods
.method public a(IZIIILjava/lang/Long;Ljava/lang/Float;Lizu;)V
    .locals 7

    .prologue
    .line 135
    iget v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->m:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    if-eqz p5, :cond_0

    .line 136
    iput p4, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->m:I

    .line 139
    :cond_0
    iput p1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->n:I

    .line 140
    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->o:Z

    .line 141
    const/4 v0, -0x1

    if-ne p3, v0, :cond_2

    const/4 v0, 0x0

    .line 142
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->p:Ljava/lang/String;

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->f:Landroid/widget/TextView;

    const v2, 0x7f0a0abe

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 148
    iget v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->n:I

    packed-switch v1, :pswitch_data_0

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    .line 235
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a:Landroid/view/View;

    iget v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->n:I

    if-nez v0, :cond_9

    const/16 v0, 0x8

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 236
    sget-object v0, Lfvc;->n:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->n:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    .line 238
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 239
    return-void

    .line 141
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->r:Lhei;

    .line 142
    invoke-interface {v0, p3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 150
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->c()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    const v1, 0x7f0204ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->o:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0a0ac1

    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 155
    const v0, 0x7f0a091f

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a(I)V

    goto :goto_1

    .line 153
    :cond_3
    const v0, 0x7f0a0ac2

    goto :goto_5

    .line 159
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->c()V

    .line 160
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    .line 161
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    const v2, 0x7f0204f0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 162
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    const v2, 0x7f0a0ac5

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->p:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 163
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    const v0, 0x7f0a0ac6

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a(I)V

    goto :goto_1

    .line 168
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->c()V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    const v1, 0x7f02055a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    const v1, 0x7f0a0ac3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    const v1, 0x7f020560

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_1

    .line 176
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->c:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->k:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->i:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->f:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    if-eqz p8, :cond_7

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    invoke-virtual {p8, v1}, Lizu;->a(Lizu;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 179
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    if-eqz v1, :cond_5

    .line 180
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 181
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 183
    :cond_5
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 184
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 185
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->clearAnimation()V

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 188
    :cond_6
    iput-object p8, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->l:Lizu;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 192
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->f:Landroid/widget/TextView;

    const v2, 0x7f110073

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 193
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 192
    invoke-virtual {v0, v2, p5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->k:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 195
    if-eqz p7, :cond_1

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->k:Landroid/widget/ProgressBar;

    invoke-virtual {p7}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto/16 :goto_1

    .line 201
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b()V

    .line 202
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    const v2, 0x7f0204ef

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    const v2, 0x7f110074

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 204
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 203
    invoke-virtual {v0, v2, p5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 208
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b()V

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    const v2, 0x7f0204ef

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    const v2, 0x7f110075

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 211
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 210
    invoke-virtual {v0, v2, p5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 215
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b()V

    .line 216
    iput p4, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->m:I

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    const v2, 0x7f0204ed

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 219
    if-eqz p6, :cond_8

    .line 220
    invoke-virtual {p6}, Ljava/lang/Long;->intValue()I

    move-result v0

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->f:Landroid/widget/TextView;

    const v2, 0x7f0a0abf

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11007a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 224
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v6

    invoke-virtual {v6, p6}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 223
    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 222
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 226
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    const v2, 0x7f0a0ac4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 235
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 236
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 238
    :cond_b
    const/16 v0, 0x8

    goto/16 :goto_4

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public a(Lfwd;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->q:Lfwd;

    .line 246
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->q:Lfwd;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->q:Lfwd;

    iget v1, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->n:I

    invoke-interface {v0, v1}, Lfwd;->a(I)V

    .line 298
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 107
    const v0, 0x7f1001ae

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a:Landroid/view/View;

    .line 108
    const v0, 0x7f100117

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->b:Landroid/widget/ImageView;

    .line 109
    const v0, 0x7f1001af

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->c:Landroid/view/View;

    .line 110
    const v0, 0x7f1001b0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 111
    const v0, 0x7f1001b1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 112
    const v0, 0x7f100118

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->f:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f100139

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->g:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f1001b2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->h:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f10016d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->i:Landroid/view/View;

    .line 116
    const v0, 0x7f10017d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    .line 117
    const v0, 0x7f100343

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->k:Landroid/widget/ProgressBar;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AutoBackupBarView;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    :cond_0
    return-void
.end method
