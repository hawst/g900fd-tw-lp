.class public Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ldzj;
.implements Lhmq;


# instance fields
.field private e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Lloa;-><init>()V

    .line 39
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 40
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->x:Llnh;

    .line 41
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 45
    new-instance v0, Livz;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->y:Llqc;

    invoke-direct {v0, v1}, Livz;-><init>(Llqr;)V

    const-class v1, Lixj;

    .line 46
    invoke-virtual {v0, v1}, Livz;->a(Ljava/lang/Class;)Livz;

    .line 47
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 144
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 145
    const-string v1, "person_id"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const-string v1, "display_name"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    const-string v1, "suggestion_id"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string v1, "activity_id"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    const-string v1, "promo_type"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->q()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 150
    const-string v1, "category_index"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->r()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 151
    const-string v1, "original_circle_ids"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 152
    const-string v1, "selected_circle_ids"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->c()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 154
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setResult(ILandroid/content/Intent;)V

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    .line 156
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "person_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "display_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "suggestion_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private q()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "promo_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private r()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "category_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private s()Z
    .locals 3

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "empty_selection_allowed"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lhmw;->t:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 65
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 67
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ljpr;

    new-instance v2, Ljqy;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Ljqy;-><init>(Landroid/content/Context;Llqr;)V

    .line 68
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldvv;

    new-instance v2, Ldvv;

    invoke-direct {v2, p0}, Ldvv;-><init>(Landroid/content/Context;)V

    .line 69
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 70
    return-void
.end method

.method public a(Lu;)V
    .locals 2

    .prologue
    .line 54
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    if-eqz v0, :cond_0

    .line 55
    check-cast p1, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->c(I)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->a(Z)V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->a(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->a(Ldzj;)V

    .line 61
    :cond_0
    return-void
.end method

.method public ap_()V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->l()V

    .line 109
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 88
    const v1, 0x7f1001db

    if-ne v0, v1, :cond_2

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->a()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->e:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->c()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "person_id"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "display_name"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "suggestion_id"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "activity_id"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "promo_type"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->q()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v3, "category_index"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->r()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v3, "original_circle_ids"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v3, "selected_circle_ids"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    .line 93
    :cond_0
    :goto_1
    return-void

    .line 89
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 90
    :cond_2
    const v1, 0x7f1001da

    if-ne v0, v1, :cond_0

    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->l()V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const v0, 0x7f040065

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setContentView(I)V

    .line 77
    const v0, 0x7f0a0830

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setTitle(I)V

    .line 79
    const v0, 0x7f1001db

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->f:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->f:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->s()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v0, 0x7f1001da

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    return-void
.end method
