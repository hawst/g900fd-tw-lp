.class public Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhmq;


# instance fields
.field private e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lloa;-><init>()V

    .line 29
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 30
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->x:Llnh;

    .line 31
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 32
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lhmw;->S:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 47
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 38
    instance-of v0, p1, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    if-eqz v0, :cond_0

    .line 39
    check-cast p1, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    .line 41
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 63
    const v1, 0x7f1001db

    if-ne v0, v1, :cond_0

    .line 64
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 65
    const-string v1, "circle_ids"

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->a()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 66
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->setResult(ILandroid/content/Intent;)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->finish()V

    .line 69
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f0400ac

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->setContentView(I)V

    .line 54
    const v0, 0x7f0a0694

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->setTitle(I)V

    .line 56
    const v0, 0x7f1001db

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    const v0, 0x7f1001da

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 58
    return-void
.end method
