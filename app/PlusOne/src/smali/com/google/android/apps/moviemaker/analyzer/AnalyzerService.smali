.class public Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field private a:Laqx;

.field private b:Laif;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 245
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;)Laqx;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->a:Laqx;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lbvy;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lbvy",
            "<",
            "Laii;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    new-instance v1, Lbvy;

    invoke-direct {v1, p0, v0}, Lbvy;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    return-object v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->b:Laif;

    return-object v0
.end method

.method public onCreate()V
    .locals 14

    .prologue
    .line 216
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 217
    invoke-static {p0}, Lapm;->a(Landroid/content/Context;)Lapm;

    move-result-object v0

    invoke-virtual {v0}, Lapm;->a()Lamm;

    move-result-object v1

    .line 218
    invoke-interface {v1}, Lamm;->o()Laqx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->a:Laqx;

    .line 219
    new-instance v0, Laif;

    .line 220
    invoke-interface {v1}, Lamm;->a()Lald;

    move-result-object v2

    .line 221
    invoke-interface {v1}, Lamm;->b()Lamy;

    move-result-object v3

    .line 222
    invoke-interface {v1}, Lamm;->c()Lasy;

    move-result-object v4

    .line 223
    invoke-interface {v1}, Lamm;->d()Lakq;

    move-result-object v5

    .line 224
    invoke-interface {v1}, Lamm;->k()Lcdu;

    move-result-object v6

    const-string v7, "notification"

    .line 225
    invoke-virtual {p0, v7}, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    .line 226
    invoke-interface {v1}, Lamm;->G()[Lajj;

    move-result-object v8

    .line 227
    invoke-interface {v1}, Lamm;->H()[Lajj;

    move-result-object v9

    .line 228
    invoke-interface {v1}, Lamm;->I()[Lajw;

    move-result-object v10

    .line 229
    invoke-interface {v1}, Lamm;->J()[Lajw;

    move-result-object v11

    .line 230
    invoke-interface {v1}, Lamm;->P()Lbjf;

    move-result-object v12

    .line 231
    invoke-interface {v1}, Lamm;->L()Ljfb;

    move-result-object v13

    move-object v1, p0

    invoke-direct/range {v0 .. v13}, Laif;-><init>(Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;Lald;Lamy;Lasy;Lakq;Lcdu;Landroid/app/NotificationManager;[Lajj;[Lajj;[Lajw;[Lajw;Lbjf;Ljfb;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->b:Laif;

    .line 232
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->b:Laif;

    invoke-virtual {v0}, Laif;->b()V

    .line 238
    return-void
.end method
