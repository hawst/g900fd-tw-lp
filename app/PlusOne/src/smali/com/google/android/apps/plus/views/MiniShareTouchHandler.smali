.class public Lcom/google/android/apps/plus/views/MiniShareTouchHandler;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:[I

.field private g:Lfyr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    .line 46
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->b:Landroid/view/View;

    .line 125
    const v0, 0x7f100118

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->d:Landroid/view/View;

    .line 126
    const v0, 0x7f100372

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->e:Landroid/view/View;

    .line 127
    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->c:Landroid/view/View;

    .line 128
    return-void
.end method

.method public a(Lfyr;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->g:Lfyr;

    .line 50
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const v5, 0x7fffffff

    const/4 v7, 0x3

    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 54
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    .line 55
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    .line 58
    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a:Landroid/view/View;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->b:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->b:Landroid/view/View;

    .line 59
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->c:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    invoke-virtual {v2, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 63
    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    aget v2, v2, v4

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    aget v2, v2, v4

    iget-object v3, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->c:Landroid/view/View;

    .line 64
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    aget v0, v0, v6

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    aget v0, v0, v6

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->c:Landroid/view/View;

    .line 66
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->b:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a:Landroid/view/View;

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a:Landroid/view/View;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->b:Landroid/view/View;

    .line 74
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->e:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->e:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    aget v0, v0, v6

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->b:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a:Landroid/view/View;

    .line 88
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    aget v0, v0, v6

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->b:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a:Landroid/view/View;

    .line 93
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a:Landroid/view/View;

    if-eqz v1, :cond_8

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v7, :cond_7

    invoke-virtual {v1, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 98
    :goto_1
    if-eq v0, v7, :cond_3

    if-ne v0, v6, :cond_4

    .line 99
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a:Landroid/view/View;

    .line 104
    :cond_4
    :goto_2
    return v6

    .line 81
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->d:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->d:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    goto :goto_0

    .line 84
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    aput v5, v0, v4

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->f:[I

    aput v5, v0, v6

    goto :goto_0

    .line 96
    :cond_7
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->getScrollX()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->getScrollY()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    invoke-virtual {v1, v2}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_1

    .line 101
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->g:Lfyr;

    if-eqz v1, :cond_4

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->g:Lfyr;

    invoke-interface {v1, v0}, Lfyr;->c(I)V

    goto :goto_2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    return v0
.end method
