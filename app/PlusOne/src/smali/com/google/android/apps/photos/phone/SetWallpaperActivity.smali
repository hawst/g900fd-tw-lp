.class public Lcom/google/android/apps/photos/phone/SetWallpaperActivity;
.super Llnu;
.source "PG"


# instance fields
.field private c:I

.field private d:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Llnu;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->c:I

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Llnu;->a(Landroid/os/Bundle;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->a:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 40
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 122
    invoke-super {p0, p1, p2, p3}, Llnu;->onActivityResult(IILandroid/content/Intent;)V

    .line 125
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    .line 126
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->setResult(I)V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->finish()V

    .line 136
    :cond_1
    :goto_0
    return-void

    .line 130
    :cond_2
    iput p1, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->c:I

    .line 131
    iget v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 132
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0, p1}, Llnu;->onCreate(Landroid/os/Bundle;)V

    .line 45
    if-eqz p1, :cond_0

    .line 46
    const-string v0, "activity-state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->c:I

    .line 47
    const-string v0, "picked-item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    .line 49
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-super {p0}, Llnu;->onResume()V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 64
    iget v1, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->c:I

    packed-switch v1, :pswitch_data_0

    .line 95
    :goto_0
    return-void

    .line 66
    :pswitch_0
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/apps/photos/phone/GetContentActivity;

    .line 69
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "image/*"

    .line 70
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "filter"

    .line 71
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 72
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 75
    :cond_0
    iput v2, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->c:I

    .line 81
    :pswitch_1
    :try_start_0
    invoke-static {}, Lfug;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "content"

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    .line 82
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/app/WallpaperManager;->getCropAndSetWallpaperIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 87
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->finish()V

    goto :goto_0

    .line 85
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->getWallpaperDesiredMinimumWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->getWallpaperDesiredMinimumHeight()I

    move-result v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.camera.action.CROP"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Licc;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    const-string v4, "image/*"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x2000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "outputX"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "outputY"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "aspectX"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "aspectY"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "scale"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "scaleUpIfNeeded"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "set-as-wallpaper"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 88
    :catch_0
    move-exception v0

    .line 90
    const-string v1, "SetWallpaper"

    const-string v2, "Unable to set wallpaper"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 64
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Llnu;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 54
    const-string v0, "activity-state"

    iget v1, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "picked-item"

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/SetWallpaperActivity;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 58
    :cond_0
    return-void
.end method
