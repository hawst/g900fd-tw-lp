.class public Lcom/google/android/apps/plus/locations/GcmLocationService;
.super Landroid/app/IntentService;
.source "PG"

# interfaces
.implements Lijm;
.implements Lijn;


# instance fields
.field private a:Lijk;

.field private b:Lily;

.field private c:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "GcmLocationService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/locations/GcmLocationService;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/GcmLocationService;->c()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/locations/GcmLocationService;)Lijk;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/locations/GcmLocationService;)Lily;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->b:Lily;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    invoke-interface {v0}, Lijk;->c()V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->c:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/libraries/social/gcm/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->c:Landroid/content/Intent;

    .line 182
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    invoke-interface {v0}, Lijk;->d()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->c:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->c:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/libraries/social/gcm/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 81
    :cond_1
    :goto_0
    return-void

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->c:Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, v1}, Lhei;->b(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/GcmLocationService;->c()V

    goto :goto_0

    :cond_3
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v2, v0, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->b:Lily;

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    invoke-interface {v0, v3, v2}, Lily;->a(Lijk;Landroid/accounts/Account;)Lijq;

    move-result-object v0

    new-instance v3, Letl;

    invoke-direct {v3, p0, v2, v1}, Letl;-><init>(Lcom/google/android/apps/plus/locations/GcmLocationService;Landroid/accounts/Account;I)V

    invoke-interface {v0, v3}, Lijq;->a(Lijt;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/GcmLocationService;->c()V

    goto :goto_0
.end method

.method public a(Liiw;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->c:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/libraries/social/gcm/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 76
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/GcmLocationService;->c()V

    .line 88
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 52
    const-class v0, Lily;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lily;

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->b:Lily;

    .line 54
    const-class v0, Lijl;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lijl;

    const-class v1, Lilz;

    .line 55
    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lijj;

    invoke-interface {v0, v1}, Lijl;->a(Lijj;)Lijl;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Lijl;->a()Lijk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    .line 58
    invoke-interface {v0, p0}, Lijk;->a(Lijm;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    .line 60
    invoke-interface {v0, p0}, Lijk;->a(Lijn;)V

    .line 61
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 65
    const-string v0, "gaia_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->c:Landroid/content/Intent;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/GcmLocationService;->a:Lijk;

    invoke-interface {v0}, Lijk;->b()V

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-static {p1}, Lcom/google/android/libraries/social/gcm/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    goto :goto_0
.end method
