.class public Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Lloa;-><init>()V

    .line 31
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 32
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 33
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;->x:Llnh;

    .line 34
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 35
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 36
    new-instance v0, Lfjo;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfjo;-><init>(Los;Llqr;)V

    .line 44
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;->x:Llnh;

    const-class v1, Liaq;

    new-instance v2, Lfjp;

    invoke-direct {v2, p0}, Lfjp;-><init>(Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 63
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 76
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 77
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_settings"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 78
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f04007a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;->setContentView(I)V

    .line 50
    return-void
.end method
