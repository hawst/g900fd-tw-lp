.class public Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private a:Lfuw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method public a(Lfuw;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;->a:Lfuw;

    .line 26
    return-void
.end method

.method public setPressed(Z)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;->a:Lfuw;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;->a:Lfuw;

    invoke-virtual {v0, p1}, Lfuw;->a(Z)Z

    move-result p1

    .line 33
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setPressed(Z)V

    .line 34
    return-void
.end method
