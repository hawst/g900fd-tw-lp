.class public Lcom/google/android/apps/plus/views/OneProfileAvatarView;
.super Landroid/widget/ImageView;
.source "PG"


# instance fields
.field private a:Lfys;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public a(Lfys;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->a:Lfys;

    .line 43
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 35
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->a:Lfys;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->a:Lfys;

    invoke-interface {v0}, Lfys;->a()V

    .line 39
    :cond_0
    return-void
.end method
