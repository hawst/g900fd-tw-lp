.class public Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;
.implements Linr;
.implements Lkdd;


# instance fields
.field private e:Lizu;

.field private f:Lkda;

.field private g:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lloa;-><init>()V

    .line 55
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 56
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->x:Llnh;

    .line 57
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 58
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 232
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->f()Lae;

    move-result-object v0

    .line 122
    const-string v1, "progress"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 124
    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {v0}, Lt;->b()V

    .line 128
    :cond_0
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 211
    const v0, 0x7f0a0a00

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 212
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lhmw;->ah:Lhmw;

    return-object v0
.end method

.method public a(ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 196
    if-eqz p2, :cond_0

    .line 197
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 201
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->finish()V

    .line 202
    return-void

    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->o()V

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 216
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 218
    return-void
.end method

.method public a(Lkda;)V
    .locals 3

    .prologue
    .line 155
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 156
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 155
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->n()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->e:Lizu;

    invoke-virtual {v1}, Lizu;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->e:Lizu;

    invoke-virtual {v1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Llsb;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move-object v0, v1

    :cond_2
    if-nez v0, :cond_3

    invoke-virtual {p1}, Lkda;->getCachedFile()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsProvider;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    :goto_1
    const-class v0, Lino;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lino;

    invoke-interface {v0, v1, p0}, Lino;->a(Landroid/net/Uri;Linr;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->n()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->o()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->finish()V

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->x:Llnh;

    const-class v1, Lizs;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->e:Lizu;

    const/4 v2, 0x4

    const/16 v3, 0x2022

    .line 139
    invoke-virtual {v0, v1, v2, v3, p0}, Lizs;->a(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->f:Lkda;

    .line 144
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 227
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->f:Lkda;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->f:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 151
    :cond_0
    return-void
.end method

.method public l()V
    .locals 3

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->f:Lkda;

    invoke-virtual {v0}, Lkda;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->f()Lae;

    move-result-object v0

    .line 111
    const-string v1, "progress"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v1

    if-nez v1, :cond_0

    .line 115
    const/4 v1, 0x0

    const v2, 0x7f0a0a01

    .line 116
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 115
    invoke-static {v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;)Lepl;

    move-result-object v1

    .line 117
    const-string v2, "progress"

    invoke-virtual {v1, v0, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public m()V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->o()V

    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->finish()V

    .line 208
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 67
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const-class v0, Lija;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lija;

    .line 70
    invoke-interface {v0, p0}, Lija;->a(Landroid/content/Context;)I

    move-result v0

    .line 71
    if-eqz v0, :cond_1

    .line 72
    invoke-static {p0}, Ljvo;->a(Landroid/content/Context;)Ljvo;

    move-result-object v1

    invoke-virtual {v1}, Ljvo;->a()V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->f()Lae;

    move-result-object v1

    const-string v2, "GMS_error"

    invoke-virtual {v1, v2}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Lfbg;

    invoke-direct {v2, v0}, Lfbg;-><init>(I)V

    const-string v0, "GMS_error"

    invoke-virtual {v2, v1, v0}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photo_ref"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->e:Lizu;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->b()V

    .line 81
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->g:Landroid/os/Handler;

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->g:Landroid/os/Handler;

    new-instance v1, Lfbf;

    invoke-direct {v1, p0}, Lfbf;-><init>(Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->c()V

    .line 133
    invoke-super {p0}, Lloa;->onDestroy()V

    .line 134
    return-void
.end method
