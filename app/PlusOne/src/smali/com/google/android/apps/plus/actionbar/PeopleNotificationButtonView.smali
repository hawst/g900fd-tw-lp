.class public Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:I

.field private e:I

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method private a()V
    .locals 9

    .prologue
    const/16 v1, 0x8

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 68
    iget v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    const/16 v3, 0x63

    if-gt v0, v3, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    .line 69
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v3, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->c:Landroid/widget/TextView;

    iget v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    if-gtz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    iget-object v3, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->a:Landroid/view/View;

    iget v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    if-gtz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->b:Landroid/view/View;

    iget v3, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    if-gtz v3, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 79
    iget v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    const/16 v3, 0xa

    if-ge v0, v3, :cond_4

    iget v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->d:I

    :goto_3
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    new-array v1, v8, [Ljava/lang/CharSequence;

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110070

    iget v5, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    .line 87
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    .line 85
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 84
    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->a:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    new-array v1, v8, [Ljava/lang/CharSequence;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a09b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 91
    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->b:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 95
    return-void

    .line 69
    :cond_1
    const-string v0, "\u221e"

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 72
    goto/16 :goto_1

    :cond_3
    move v0, v2

    .line 74
    goto :goto_2

    .line 79
    :cond_4
    iget v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->e:I

    goto :goto_3
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 60
    iput p1, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->f:I

    .line 62
    iget-boolean v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->g:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->a()V

    .line 65
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 44
    const v1, 0x7f0d0329

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->d:I

    .line 46
    const v1, 0x7f0d032a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->e:I

    .line 49
    const v0, 0x7f100480

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->a:Landroid/view/View;

    .line 50
    const v0, 0x7f100481

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->b:Landroid/view/View;

    .line 52
    const v0, 0x7f100482

    .line 53
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->c:Landroid/widget/TextView;

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->g:Z

    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/plus/actionbar/PeopleNotificationButtonView;->a()V

    .line 57
    return-void
.end method
