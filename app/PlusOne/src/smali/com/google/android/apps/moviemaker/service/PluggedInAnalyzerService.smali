.class public Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;
.super Lbvt;
.source "PG"

# interfaces
.implements Lbwk;


# instance fields
.field private a:Livc;

.field private b:Livc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbvt;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected a()Lbwe;
    .locals 21

    .prologue
    .line 35
    const-string v2, "power"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Landroid/os/PowerManager;

    .line 36
    invoke-static/range {p0 .. p0}, Lapm;->a(Landroid/content/Context;)Lapm;

    move-result-object v2

    invoke-virtual {v2}, Lapm;->a()Lamm;

    move-result-object v20

    .line 37
    invoke-interface/range {v20 .. v20}, Lamm;->L()Ljfb;

    move-result-object v18

    .line 38
    invoke-interface/range {v20 .. v20}, Lamm;->p()Lanh;

    move-result-object v10

    .line 40
    invoke-static {v10}, Lbwf;->a(Lanh;)Lbwl;

    move-result-object v11

    .line 41
    new-instance v2, Lbwf;

    new-instance v4, Lbvu;

    .line 46
    invoke-interface/range {v20 .. v20}, Lamm;->p()Lanh;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v3, v5, v1}, Lbvu;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Lanh;Ljfb;)V

    .line 48
    invoke-interface/range {v20 .. v20}, Lamm;->B()Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    move-result-object v5

    .line 50
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 49
    move-object/from16 v0, v18

    invoke-static {v11, v3, v0}, Lbwf;->a(Lbwl;Landroid/content/ContentResolver;Ljfb;)Lcfq;

    move-result-object v6

    .line 51
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->a(Landroid/content/Context;)Lbvy;

    move-result-object v7

    .line 52
    invoke-interface/range {v20 .. v20}, Lamm;->q()Lajt;

    move-result-object v8

    .line 53
    invoke-interface/range {v20 .. v20}, Lamm;->s()Lbks;

    move-result-object v9

    new-instance v12, Lbit;

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v12, v3}, Lbit;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->a:Livc;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->b:Livc;

    .line 59
    invoke-interface/range {v20 .. v20}, Lamm;->K()Ljdw;

    move-result-object v15

    .line 60
    invoke-interface/range {v20 .. v20}, Lamm;->v()Lbig;

    move-result-object v16

    .line 61
    invoke-interface/range {v20 .. v20}, Lamm;->h()Ljava/util/concurrent/Executor;

    move-result-object v17

    new-instance v19, Lapo;

    .line 63
    invoke-interface/range {v20 .. v20}, Lamm;->p()Lanh;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Lapo;-><init>(Lani;)V

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v19}, Lbwf;-><init>(Lbwk;Lbvs;Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;Lcfq;Lbvy;Lajt;Lbks;Lanh;Lbwl;Lbit;Livc;Livc;Ljdw;Lbig;Ljava/util/concurrent/Executor;Ljfb;Lapo;)V

    return-object v2
.end method

.method public a(JZ)V
    .locals 5

    .prologue
    .line 83
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 84
    const/4 v1, 0x2

    .line 86
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p1

    .line 87
    invoke-static {p0, p3}, Lbwf;->b(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v4

    .line 84
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 88
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 68
    invoke-static {p0}, Lapm;->a(Landroid/content/Context;)Lapm;

    move-result-object v0

    invoke-virtual {v0}, Lapm;->a()Lamm;

    move-result-object v0

    .line 69
    const-string v1, "PluggedInLog"

    invoke-interface {v0, v1}, Lamm;->a(Ljava/lang/String;)Livc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->a:Livc;

    .line 70
    const-string v1, "ClusteringLog"

    invoke-interface {v0, v1}, Lamm;->a(Ljava/lang/String;)Livc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->b:Livc;

    .line 71
    invoke-super {p0}, Lbvt;->onCreate()V

    .line 72
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->a:Livc;

    invoke-static {v0}, Lcek;->a(Ljava/io/Closeable;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;->b:Livc;

    invoke-static {v0}, Lcek;->a(Ljava/io/Closeable;)V

    .line 78
    invoke-super {p0}, Lbvt;->onDestroy()V

    .line 79
    return-void
.end method
