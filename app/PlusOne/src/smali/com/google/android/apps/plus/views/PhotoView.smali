.class public Lcom/google/android/apps/plus/views/PhotoView;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Lkdd;


# static fields
.field private static ay:Lizs;

.field private static f:Z

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:Landroid/graphics/Bitmap;

.field private static k:Landroid/graphics/Bitmap;

.field private static l:Landroid/graphics/Bitmap;

.field private static m:Z

.field private static n:Ljava/lang/String;

.field private static o:Ljava/lang/String;

.field private static p:Ljava/lang/String;

.field private static q:Landroid/text/TextPaint;

.field private static r:Landroid/text/TextPaint;

.field private static s:Landroid/graphics/Paint;

.field private static t:Landroid/text/TextPaint;


# instance fields
.field private A:Lcom/google/android/libraries/social/media/MediaResource;

.field private B:Lgae;

.field private C:Landroid/graphics/Matrix;

.field private D:Landroid/graphics/Matrix;

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Lgaf;

.field private N:Landroid/view/GestureDetector;

.field private O:Landroid/view/ScaleGestureDetector;

.field private P:Landroid/view/View$OnClickListener;

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Lgag;

.field private U:F

.field private V:F

.field private W:F

.field public a:Landroid/graphics/drawable/Drawable;

.field private aa:Z

.field private ab:F

.field private ac:J

.field private ad:Lgai;

.field private ae:Lgah;

.field private af:Landroid/graphics/RectF;

.field private ag:Landroid/graphics/RectF;

.field private ah:Landroid/graphics/RectF;

.field private ai:Landroid/graphics/RectF;

.field private aj:Z

.field private ak:Landroid/graphics/RectF;

.field private final al:Landroid/graphics/RectF;

.field private final am:Landroid/graphics/RectF;

.field private an:Lgaa;

.field private ao:Z

.field private ap:Z

.field private aq:F

.field private ar:F

.field private as:F

.field private at:Z

.field private au:J

.field private av:Z

.field private aw:Lfzz;

.field private ax:Z

.field public b:Lizu;

.field public c:Landroid/graphics/Matrix;

.field public d:F

.field public e:[F

.field private u:Lgad;

.field private v:Liro;

.field private w:Landroid/widget/ProgressBar;

.field private x:Lnzi;

.field private y:Lcom/google/android/libraries/social/media/MediaResource;

.field private z:Lcom/google/android/libraries/social/media/MediaResource;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 312
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 193
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    .line 195
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->D:Landroid/graphics/Matrix;

    .line 207
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->H:Z

    .line 262
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    .line 264
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ag:Landroid/graphics/RectF;

    .line 266
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    .line 268
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    .line 270
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ai:Landroid/graphics/RectF;

    .line 274
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ak:Landroid/graphics/RectF;

    .line 276
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    .line 278
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    .line 289
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->as:F

    .line 297
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->av:Z

    .line 306
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    if-nez v0, :cond_0

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    .line 313
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->v()V

    .line 314
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 317
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 193
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    .line 195
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->D:Landroid/graphics/Matrix;

    .line 207
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->H:Z

    .line 262
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    .line 264
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ag:Landroid/graphics/RectF;

    .line 266
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    .line 268
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    .line 270
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ai:Landroid/graphics/RectF;

    .line 274
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ak:Landroid/graphics/RectF;

    .line 276
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    .line 278
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    .line 289
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->as:F

    .line 297
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->av:Z

    .line 306
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    if-nez v0, :cond_0

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    .line 318
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->v()V

    .line 319
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 322
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 193
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    .line 195
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->D:Landroid/graphics/Matrix;

    .line 207
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->H:Z

    .line 262
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    .line 264
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ag:Landroid/graphics/RectF;

    .line 266
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    .line 268
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    .line 270
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ai:Landroid/graphics/RectF;

    .line 274
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ak:Landroid/graphics/RectF;

    .line 276
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    .line 278
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    .line 289
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->as:F

    .line 297
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->av:Z

    .line 306
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    if-nez v0, :cond_0

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    .line 323
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->v()V

    .line 324
    return-void
.end method

.method private a(FFFF)F
    .locals 6

    .prologue
    .line 1583
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PhotoView;->a(FFFFF)F

    move-result v0

    return v0
.end method

.method private a(FFFFF)F
    .locals 2

    .prologue
    .line 1604
    sub-float v0, p4, p3

    sub-float v1, p2, p1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1605
    sub-float v0, p2, p1

    add-float v1, p4, p3

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    add-float/2addr v0, p1

    .line 1611
    :goto_0
    return v0

    .line 1608
    :cond_0
    sub-float v0, p2, p4

    sub-float v1, p1, p3

    .line 1609
    invoke-static {v1, p5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1608
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/PhotoView;Lcom/google/android/libraries/social/media/MediaResource;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->A:Lcom/google/android/libraries/social/media/MediaResource;

    return-object p1
.end method

.method private a(Landroid/graphics/Matrix;)V
    .locals 6

    .prologue
    const/high16 v5, 0x41a00000    # 20.0f

    .line 1622
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1623
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1625
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->b(Landroid/graphics/RectF;)V

    .line 1626
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/PhotoView;->a(FFFF)F

    move-result v0

    .line 1628
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/PhotoView;->a(FFFF)F

    move-result v1

    .line 1631
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v5

    if-gtz v2, :cond_0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_2

    .line 1632
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ae:Lgah;

    invoke-virtual {v2, v0, v1}, Lgah;->a(FF)Z

    .line 1637
    :cond_1
    :goto_0
    return-void

    .line 1633
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ae:Lgah;

    invoke-static {v2}, Lgah;->a(Lgah;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1634
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1635
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->r()V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/PhotoView;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->u()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/PhotoView;ZFFZ)Z
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/PhotoView;->a(ZFFZ)Z

    move-result v0

    return v0
.end method

.method private a(ZFFZ)Z
    .locals 7

    .prologue
    .line 1544
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1545
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1548
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->b(Landroid/graphics/RectF;)V

    .line 1553
    if-nez p4, :cond_2

    .line 1554
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->right:F

    move-object v0, p0

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PhotoView;->a(FFFFF)F

    move-result v6

    .line 1556
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PhotoView;->a(FFFFF)F

    move-result v0

    move v1, v6

    .line 1564
    :goto_0
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->M:Lgaf;

    if-eqz v2, :cond_1

    cmpl-float v2, v1, p2

    if-nez v2, :cond_0

    cmpl-float v2, v0, p3

    if-eqz v2, :cond_1

    .line 1566
    :cond_0
    sub-float v2, v1, p2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1567
    sub-float v3, v0, p3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 1568
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->M:Lgaf;

    invoke-interface {v4, v2, v3}, Lgaf;->a(FF)V

    .line 1572
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1573
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->aq:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->aq:F

    .line 1574
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ar:F

    add-float/2addr v2, v0

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ar:F

    .line 1576
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->r()V

    .line 1578
    cmpl-float v1, v1, p2

    if-nez v1, :cond_3

    cmpl-float v0, v0, p3

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    move v0, p3

    move v1, p2

    .line 1560
    goto :goto_0

    .line 1578
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/PhotoView;)F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->as:F

    return v0
.end method

.method private b(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1652
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->aj:Z

    if-eqz v0, :cond_0

    .line 1653
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ai:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1657
    :goto_0
    return-void

    .line 1655
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ak:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method private b(Lkda;)V
    .locals 0

    .prologue
    .line 1370
    if-eqz p1, :cond_0

    .line 1371
    invoke-virtual {p1, p0}, Lkda;->unregister(Lkdd;)V

    .line 1373
    :cond_0
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/views/PhotoView;)F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->aq:F

    return v0
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/views/PhotoView;)F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ar:F

    return v0
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/views/PhotoView;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    return v0
.end method

.method public static synthetic f(Lcom/google/android/apps/plus/views/PhotoView;)Lnzi;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->x:Lnzi;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/plus/views/PhotoView;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public static synthetic o()I
    .locals 1

    .prologue
    .line 72
    sget v0, Lcom/google/android/apps/plus/views/PhotoView;->g:I

    return v0
.end method

.method public static synthetic p()Lizs;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 549
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    if-eqz v0, :cond_1

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    invoke-virtual {v0, v1}, Liro;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 553
    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    .line 554
    return-void
.end method

.method private r()V
    .locals 1

    .prologue
    .line 901
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->B:Lgae;

    if-eqz v0, :cond_0

    .line 902
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->B:Lgae;

    invoke-interface {v0}, Lgae;->aj_()V

    .line 905
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->an:Lgaa;

    invoke-virtual {v0}, Lgaa;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 906
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 908
    :cond_1
    return-void
.end method

.method private s()V
    .locals 1

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->y:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->b(Lkda;)V

    .line 1361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->y:Lcom/google/android/libraries/social/media/MediaResource;

    .line 1362
    return-void
.end method

.method private t()V
    .locals 1

    .prologue
    .line 1365
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->b(Lkda;)V

    .line 1366
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    .line 1367
    return-void
.end method

.method private u()V
    .locals 1

    .prologue
    .line 1618
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->a(Landroid/graphics/Matrix;)V

    .line 1619
    return-void
.end method

.method private v()V
    .locals 8

    .prologue
    const v7, 0x7f0d024e

    const/4 v0, 0x0

    const v6, 0x7f0b013a

    const/4 v1, 0x1

    .line 1673
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->setWillNotDraw(Z)V

    .line 1674
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->setFocusable(Z)V

    .line 1675
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->setClickable(Z)V

    .line 1677
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1679
    sget-boolean v3, Lcom/google/android/apps/plus/views/PhotoView;->f:Z

    if-nez v3, :cond_0

    .line 1680
    sput-boolean v1, Lcom/google/android/apps/plus/views/PhotoView;->f:Z

    .line 1682
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1685
    const v4, 0x7f020477

    invoke-static {v3, v4}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->j:Landroid/graphics/Bitmap;

    .line 1686
    const v4, 0x7f02046f

    .line 1687
    invoke-static {v3, v4}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->k:Landroid/graphics/Bitmap;

    .line 1688
    const v4, 0x7f02046b

    invoke-static {v3, v4}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->l:Landroid/graphics/Bitmap;

    .line 1691
    const v4, 0x7f0d0332

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sput v4, Lcom/google/android/apps/plus/views/PhotoView;->g:I

    .line 1693
    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4}, Landroid/text/TextPaint;-><init>()V

    .line 1694
    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->q:Landroid/text/TextPaint;

    .line 1695
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 1694
    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 1696
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->q:Landroid/text/TextPaint;

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1698
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->q:Landroid/text/TextPaint;

    invoke-virtual {v4, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1699
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->q:Landroid/text/TextPaint;

    invoke-virtual {v4, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1700
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->q:Landroid/text/TextPaint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1701
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->q:Landroid/text/TextPaint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1703
    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4}, Landroid/text/TextPaint;-><init>()V

    .line 1704
    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->r:Landroid/text/TextPaint;

    .line 1705
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 1704
    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 1706
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->r:Landroid/text/TextPaint;

    const v5, 0x7f0d024b

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1708
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->r:Landroid/text/TextPaint;

    invoke-virtual {v4, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1709
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->r:Landroid/text/TextPaint;

    invoke-virtual {v4, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1710
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->r:Landroid/text/TextPaint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1712
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 1713
    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->s:Landroid/graphics/Paint;

    const v5, 0x7f0b02e7

    .line 1714
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 1713
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1716
    const v4, 0x7f0a0649

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->n:Ljava/lang/String;

    .line 1717
    const v4, 0x7f0a064a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->o:Ljava/lang/String;

    .line 1719
    const v4, 0x7f0d02a8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/google/android/apps/plus/views/PhotoView;->h:I

    .line 1721
    const v4, 0x7f0d02a9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/google/android/apps/plus/views/PhotoView;->i:I

    .line 1724
    const v4, 0x7f0a064b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->p:Ljava/lang/String;

    .line 1726
    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4}, Landroid/text/TextPaint;-><init>()V

    .line 1727
    sput-object v4, Lcom/google/android/apps/plus/views/PhotoView;->t:Landroid/text/TextPaint;

    .line 1728
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 1727
    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 1729
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->t:Landroid/text/TextPaint;

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v4, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1731
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->t:Landroid/text/TextPaint;

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1732
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->t:Landroid/text/TextPaint;

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1733
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->t:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1735
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    sput-boolean v3, Lcom/google/android/apps/plus/views/PhotoView;->m:Z

    .line 1739
    :cond_0
    new-instance v3, Landroid/view/GestureDetector;

    const/4 v4, 0x0

    sget-boolean v5, Lcom/google/android/apps/plus/views/PhotoView;->m:Z

    if-nez v5, :cond_1

    move v0, v1

    :cond_1
    invoke-direct {v3, v2, p0, v4, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->N:Landroid/view/GestureDetector;

    .line 1740
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, v2, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->O:Landroid/view/ScaleGestureDetector;

    .line 1741
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_2

    .line 1742
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->O:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, v1}, Landroid/view/ScaleGestureDetector;->setQuickScaleEnabled(Z)V

    .line 1744
    :cond_2
    new-instance v0, Lgag;

    invoke-direct {v0, p0, p0}, Lgag;-><init>(Lcom/google/android/apps/plus/views/PhotoView;Lcom/google/android/apps/plus/views/PhotoView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->T:Lgag;

    .line 1745
    new-instance v0, Lgai;

    invoke-direct {v0, p0}, Lgai;-><init>(Lcom/google/android/apps/plus/views/PhotoView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ad:Lgai;

    .line 1746
    new-instance v0, Lgah;

    invoke-direct {v0, p0}, Lgah;-><init>(Lcom/google/android/apps/plus/views/PhotoView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ae:Lgah;

    .line 1747
    new-instance v0, Lgaa;

    invoke-direct {v0, p0}, Lgaa;-><init>(Lcom/google/android/apps/plus/views/PhotoView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->an:Lgaa;

    .line 1748
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    .line 1749
    return-void
.end method


# virtual methods
.method public a(FFF)F
    .locals 1

    .prologue
    .line 784
    invoke-static {p1, p3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 744
    if-nez p1, :cond_0

    .line 745
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->aj:Z

    .line 748
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aj:Z

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ai:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 751
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->i(Z)V

    .line 752
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->requestLayout()V

    .line 753
    return-void
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 792
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 795
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    const/4 v2, 0x5

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 797
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    .line 798
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 799
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    .line 800
    int-to-float v3, v3

    mul-float/2addr v3, v2

    .line 801
    int-to-float v4, v4

    mul-float/2addr v2, v4

    .line 804
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/views/PhotoView;->b(Landroid/graphics/RectF;)V

    .line 805
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    div-float/2addr v4, v3

    invoke-static {v6, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p1, Landroid/graphics/RectF;->left:F

    .line 806
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    int-to-float v5, v1

    sub-float/2addr v4, v5

    div-float/2addr v4, v2

    invoke-static {v6, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p1, Landroid/graphics/RectF;->top:F

    .line 807
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    int-to-float v0, v0

    sub-float v0, v4, v0

    div-float/2addr v0, v3

    invoke-static {v7, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 808
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v1, v1

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    invoke-static {v7, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 809
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->an:Lgaa;

    invoke-virtual {v0, p1}, Lgaa;->a(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)V

    .line 898
    return-void
.end method

.method public a(Lfzz;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aw:Lfzz;

    .line 328
    return-void
.end method

.method public a(Lgaf;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->M:Lgaf;

    .line 332
    return-void
.end method

.method public a(Lizu;Lnzi;)V
    .locals 1

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    invoke-virtual {v0, p1}, Lizu;->a(Lizu;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    if-ne v0, p1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->x:Lnzi;

    .line 1285
    invoke-static {v0, p2}, Loxu;->a(Loxu;Loxu;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1295
    :goto_0
    return-void

    .line 1289
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->c()V

    .line 1291
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    .line 1292
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoView;->x:Lnzi;

    .line 1294
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->b()V

    goto :goto_0
.end method

.method public a(Lizu;Lnzi;Lgad;Lgae;)V
    .locals 1

    .prologue
    .line 1277
    iput-object p3, p0, Lcom/google/android/apps/plus/views/PhotoView;->u:Lgad;

    .line 1278
    iput-object p4, p0, Lcom/google/android/apps/plus/views/PhotoView;->B:Lgae;

    .line 1279
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    invoke-virtual {v0, p1}, Lizu;->a(Lizu;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    if-ne v0, p1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->x:Lnzi;

    invoke-static {v0, p2}, Loxu;->a(Loxu;Loxu;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->c()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoView;->x:Lnzi;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->b()V

    .line 1280
    :cond_3
    return-void
.end method

.method public a(Lkda;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 994
    .line 995
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v3

    .line 1131
    :goto_0
    if-eqz v0, :cond_1

    .line 1132
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->B:Lgae;

    if-eqz v0, :cond_0

    .line 1133
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->B:Lgae;

    invoke-interface {v0, p0, p1}, Lgae;->a(Lcom/google/android/apps/plus/views/PhotoView;Lkda;)V

    .line 1136
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->J:Z

    if-nez v0, :cond_1

    .line 1137
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->removeView(Landroid/view/View;)V

    .line 1138
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->addView(Landroid/view/View;)V

    .line 1142
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 1143
    :goto_1
    return-void

    .line 997
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->y:Lcom/google/android/libraries/social/media/MediaResource;

    if-eq p1, v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->I:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->q()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->s()V

    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/social/media/MediaResource;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->y:Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->y:Lcom/google/android/libraries/social/media/MediaResource;

    if-ne v0, v1, :cond_2

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    :cond_2
    move-object v0, p1

    .line 999
    check-cast v0, Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 1000
    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getResourceType()I

    move-result v1

    invoke-static {v1}, Llsd;->a(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1001
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PhotoView;->c(Z)V

    .line 1006
    :cond_3
    :goto_2
    invoke-virtual {p1}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v1

    .line 1008
    instance-of v0, v1, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 1009
    check-cast v0, Landroid/graphics/Bitmap;

    .line 1010
    new-instance v4, Lkdo;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v4, v0, v5, v6}, Lkdo;-><init>(Landroid/graphics/Bitmap;II)V

    move-object v0, v4

    .line 1079
    :goto_3
    if-eqz v0, :cond_a

    .line 1080
    iput v7, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    iput v7, p0, Lcom/google/android/apps/plus/views/PhotoView;->U:F

    if-eqz v0, :cond_4

    new-instance v1, Lfwm;

    invoke-direct {v1, v0}, Lfwm;-><init>(Lkdo;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_4
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PhotoView;->i(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 1081
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->i()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1082
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->I:Z

    if-nez v0, :cond_5

    .line 1083
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->I:Z

    .line 1084
    new-instance v0, Lfzy;

    invoke-direct {v0, p0}, Lfzy;-><init>(Lcom/google/android/apps/plus/views/PhotoView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    .line 1089
    :cond_5
    :goto_4
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->K:Z

    :goto_5
    move v0, v2

    .line 1109
    goto/16 :goto_0

    .line 1002
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getResourceType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1003
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    goto :goto_2

    .line 1012
    :cond_7
    instance-of v0, v1, Lkdo;

    if-eqz v0, :cond_8

    move-object v0, v1

    .line 1013
    check-cast v0, Lkdo;

    goto :goto_3

    .line 1015
    :cond_8
    instance-of v0, v1, Ljava/io/File;

    if-eqz v0, :cond_d

    .line 1016
    check-cast v1, Ljava/io/File;

    .line 1017
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lfzx;

    invoke-direct {v2, v1, p1}, Lfzx;-><init>(Ljava/io/File;Lkda;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1074
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 1086
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->d()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1087
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->an:Lgaa;

    invoke-virtual {v0}, Lgaa;->e()V

    goto :goto_4

    .line 1091
    :cond_a
    instance-of v0, v1, Lirp;

    if-eqz v0, :cond_b

    .line 1092
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    .line 1093
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->J:Z

    .line 1094
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->removeView(Landroid/view/View;)V

    .line 1096
    new-instance v0, Liro;

    check-cast v1, Lirp;

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    .line 1097
    invoke-virtual {v4}, Lizs;->b()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Liro;-><init>(Lirp;Landroid/graphics/Bitmap$Config;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    invoke-virtual {v0, p0}, Liro;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1099
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->F:Z

    invoke-virtual {v0, v1}, Liro;->b(Z)V

    .line 1101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 1102
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->K:Z

    goto :goto_5

    .line 1105
    :cond_b
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->K:Z

    goto :goto_5

    .line 1115
    :pswitch_2
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->K:Z

    .line 1116
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    if-eqz v0, :cond_c

    .line 1117
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Animated Image could not be loaded."

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1118
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1119
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->removeView(Landroid/view/View;)V

    .line 1120
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->J:Z

    :cond_c
    move v0, v2

    goto/16 :goto_0

    :cond_d
    move-object v0, v4

    goto/16 :goto_3

    .line 995
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 590
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ax:Z

    if-eq v0, p1, :cond_0

    .line 591
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ax:Z

    .line 592
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 594
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    if-eqz v0, :cond_0

    sget-object v0, Ljac;->b:Ljac;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    invoke-virtual {v1}, Lizu;->g()Ljac;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ZFF)Z
    .locals 1

    .prologue
    .line 1527
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/views/PhotoView;->a(ZFFZ)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 14

    .prologue
    const/4 v2, 0x3

    const/4 v9, 0x0

    const/4 v13, 0x1

    const/4 v5, 0x0

    .line 912
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    if-eqz v0, :cond_1

    .line 914
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljbd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ljac;->d:Ljac;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    .line 915
    invoke-virtual {v1}, Lizu;->g()Ljac;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v13

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    .line 917
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->H:Z

    if-eqz v0, :cond_3

    move v0, v13

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->J:Z

    .line 919
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->t()V

    .line 921
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->u:Lgad;

    iget v0, v0, Lgad;->c:I

    if-eqz v0, :cond_4

    .line 922
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->u:Lgad;

    .line 923
    iget v2, v2, Lgad;->a:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->u:Lgad;

    iget v3, v3, Lgad;->b:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->u:Lgad;

    .line 924
    iget v4, v4, Lgad;->c:I

    move-object v5, p0

    .line 922
    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    .line 961
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getStatus()I

    move-result v0

    if-ne v0, v13, :cond_0

    .line 962
    iput-object v9, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    .line 965
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->removeView(Landroid/view/View;)V

    .line 967
    :cond_1
    return-void

    :cond_2
    move v0, v5

    .line 915
    goto :goto_0

    :cond_3
    move v0, v5

    .line 917
    goto :goto_1

    .line 925
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->x:Lnzi;

    if-eqz v0, :cond_5

    .line 926
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    new-instance v3, Ljub;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->x:Lnzi;

    invoke-direct {v3, v4}, Ljub;-><init>(Lnzi;)V

    const/16 v4, 0x40

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;ILizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    goto :goto_2

    .line 933
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v13}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 934
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljaa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljaa;

    invoke-interface {v0}, Ljaa;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 936
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 937
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v13

    .line 939
    :goto_3
    if-eqz v0, :cond_7

    move v6, v5

    .line 940
    :goto_4
    if-eqz v0, :cond_8

    iget v7, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 942
    :goto_5
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    const/16 v11, 0x240

    move v8, v2

    move-object v10, v9

    move-object v12, p0

    invoke-virtual/range {v3 .. v12}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    goto :goto_2

    :cond_6
    move v0, v5

    .line 937
    goto :goto_3

    .line 939
    :cond_7
    iget v6, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_4

    :cond_8
    move v7, v5

    .line 940
    goto :goto_5

    .line 952
    :cond_9
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->ay:Lizs;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    const/4 v6, 0x5

    .line 953
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v8

    const/16 v10, 0x1040

    move-object v11, p0

    .line 952
    invoke-virtual/range {v4 .. v11}, Lizs;->a(Lizu;IIILizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    goto/16 :goto_2
.end method

.method public b(FFF)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x258

    .line 1502
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->as:F

    .line 1503
    iput p2, p0, Lcom/google/android/apps/plus/views/PhotoView;->aq:F

    .line 1504
    iput p3, p0, Lcom/google/android/apps/plus/views/PhotoView;->ar:F

    .line 1507
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->U:F

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->W:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1509
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->m()F

    move-result v1

    .line 1510
    div-float v2, v0, v1

    .line 1513
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1515
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->r()V

    .line 1517
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->V:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 1518
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->T:Lgag;

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->V:F

    invoke-virtual {v0, v1, v2, v4, v5}, Lgag;->a(FFJ)Z

    .line 1519
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->u()V

    .line 1524
    :cond_0
    :goto_0
    return-void

    .line 1520
    :cond_1
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 1521
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->T:Lgag;

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    invoke-virtual {v0, v1, v2, v4, v5}, Lgag;->a(FFJ)Z

    .line 1522
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->u()V

    goto :goto_0
.end method

.method public b(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 819
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 820
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 821
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->am:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 822
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 601
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->av:Z

    if-eq v0, p1, :cond_0

    .line 602
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->av:Z

    .line 603
    if-nez p1, :cond_0

    .line 604
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->T:Lgag;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgag;->a(Z)V

    .line 605
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ad:Lgai;

    invoke-virtual {v0}, Lgai;->a()V

    .line 606
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ae:Lgah;

    invoke-virtual {v0}, Lgah;->a()V

    .line 607
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->l()V

    .line 610
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 972
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->A:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->b(Lkda;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->A:Lcom/google/android/libraries/social/media/MediaResource;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->I:Z

    .line 973
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->an:Lgaa;

    invoke-virtual {v0}, Lgaa;->c()V

    .line 974
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 620
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ap:Z

    if-eq v0, p1, :cond_0

    .line 621
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ap:Z

    .line 622
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 624
    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 627
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ao:Z

    if-eq v0, p1, :cond_0

    .line 628
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ao:Z

    .line 629
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 631
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 586
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    return v0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 638
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->L:Z

    .line 639
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 616
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ap:Z

    return v0
.end method

.method public f(Z)V
    .locals 2

    .prologue
    .line 724
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->F:Z

    if-eq v0, p1, :cond_2

    .line 725
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->F:Z

    .line 726
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Liro;

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Liro;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->F:Z

    invoke-virtual {v0, v1}, Liro;->b(Z)V

    .line 729
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    if-eqz v0, :cond_1

    .line 730
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->F:Z

    invoke-virtual {v0, v1}, Liro;->b(Z)V

    .line 732
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 734
    :cond_2
    return-void
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 634
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ao:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(Z)V
    .locals 1

    .prologue
    .line 1334
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->H:Z

    if-eq v0, p1, :cond_0

    .line 1335
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->H:Z

    .line 1336
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->A:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_1

    .line 1337
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->c()V

    .line 1338
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->b()V

    .line 1343
    :cond_0
    :goto_0
    return-void

    .line 1340
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    goto :goto_0
.end method

.method protected g()Z
    .locals 1

    .prologue
    .line 642
    const/4 v0, 0x0

    return v0
.end method

.method public h(Z)V
    .locals 1

    .prologue
    .line 1380
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->Q:Z

    .line 1381
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->Q:Z

    if-nez v0, :cond_0

    .line 1382
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->l()V

    .line 1384
    :cond_0
    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 654
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->K:Z

    return v0
.end method

.method protected i(Z)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/high16 v6, 0x41400000    # 12.0f

    const/4 v5, 0x0

    .line 1390
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->E:Z

    if-nez v0, :cond_1

    .line 1417
    :cond_0
    :goto_0
    return-void

    .line 1393
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1394
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1397
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1400
    if-nez p1, :cond_2

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->E:Z

    if-eqz v0, :cond_3

    .line 1401
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    int-to-float v3, v0

    int-to-float v4, v1

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/PhotoView;->b(Landroid/graphics/RectF;)V

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->aj:Z

    if-eqz v2, :cond_6

    cmpl-float v1, v0, v1

    if-lez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ag:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-int v4, v1, v0

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ag:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->D:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1402
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->m()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->L:Z

    if-eqz v0, :cond_7

    const v0, 0x3f4ccccd    # 0.8f

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    mul-float/2addr v0, v1

    :goto_2
    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->U:F

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    mul-float/2addr v1, v6

    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->V:F

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->L:Z

    if-eqz v0, :cond_8

    const/high16 v0, 0x3fc00000    # 1.5f

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->V:F

    mul-float/2addr v0, v1

    :goto_3
    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->W:F

    .line 1405
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    .line 1407
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->aw:Lfzz;

    if-eqz v0, :cond_4

    .line 1408
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->af:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1409
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1410
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->aw:Lfzz;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ah:Landroid/graphics/RectF;

    invoke-interface {v0, p0, v1, v2}, Lfzz;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/RectF;)V

    .line 1413
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->B:Lgae;

    if-eqz v0, :cond_0

    .line 1414
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->B:Lgae;

    invoke-interface {v0}, Lgae;->aj_()V

    .line 1415
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->B:Lgae;

    invoke-interface {v0}, Lgae;->b()V

    goto/16 :goto_0

    .line 1401
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ag:Landroid/graphics/RectF;

    sub-int v3, v1, v0

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->al:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ag:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    .line 1402
    :cond_7
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    goto/16 :goto_2

    :cond_8
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->V:F

    goto :goto_3
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 694
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->G:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->H:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 659
    invoke-super {p0}, Landroid/view/ViewGroup;->invalidate()V

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->an:Lgaa;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->an:Lgaa;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Lgaa;->a(Z)V

    .line 666
    :cond_0
    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    if-ne p1, v0, :cond_1

    .line 890
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 894
    :goto_0
    return-void

    .line 892
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 712
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->y:Lcom/google/android/libraries/social/media/MediaResource;

    if-nez v0, :cond_0

    .line 713
    const/4 v0, -0x1

    .line 715
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->y:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getResourceType()I

    move-result v0

    goto :goto_0
.end method

.method public j(Z)V
    .locals 1

    .prologue
    .line 1663
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->removeView(Landroid/view/View;)V

    .line 1664
    if-eqz p1, :cond_0

    .line 1665
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->addView(Landroid/view/View;)V

    .line 1667
    :cond_0
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 871
    invoke-super {p0}, Landroid/view/ViewGroup;->jumpDrawablesToCurrentState()V

    .line 872
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 873
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 875
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    if-eqz v0, :cond_1

    .line 876
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    invoke-virtual {v0}, Liro;->jumpToCurrentState()V

    .line 878
    :cond_1
    return-void
.end method

.method public k()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 812
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 829
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->D:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 835
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->r()V

    .line 836
    return-void
.end method

.method public m()F
    .locals 2

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1476
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public n()F
    .locals 2

    .prologue
    .line 1485
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->m()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 840
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 841
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->b()V

    .line 842
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 843
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 845
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 843
    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 849
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 850
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->q()V

    .line 851
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->c()V

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->z:Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->y:Lcom/google/android/libraries/social/media/MediaResource;

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->t()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->s()V

    .line 853
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 461
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->Q:Z

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ad:Lgai;

    invoke-virtual {v0}, Lgai;->a()V

    .line 464
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1147
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 1150
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->u:Lgad;

    iget-boolean v0, v0, Lgad;->d:Z

    if-eqz v0, :cond_1

    .line 1151
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoView;->s:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1152
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->n:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sget v2, Lcom/google/android/apps/plus/views/PhotoView;->h:I

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->q:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1154
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->o:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sget v2, Lcom/google/android/apps/plus/views/PhotoView;->i:I

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->r:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1208
    :cond_0
    :goto_0
    return-void

    .line 1156
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_9

    .line 1157
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->an:Lgaa;

    invoke-virtual {v0}, Lgaa;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1158
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    .line 1159
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1163
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aw:Lfzz;

    if-eqz v1, :cond_2

    .line 1164
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aw:Lfzz;

    invoke-interface {v1, p1}, Lfzz;->a(Landroid/graphics/Canvas;)V

    .line 1166
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    if-eqz v1, :cond_3

    .line 1167
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1169
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1170
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1173
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ax:Z

    if-nez v0, :cond_7

    .line 1174
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    .line 1175
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aw:Lfzz;

    if-eqz v1, :cond_5

    .line 1176
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1177
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aw:Lfzz;

    invoke-interface {v1, p1}, Lfzz;->a(Landroid/graphics/Canvas;)V

    .line 1180
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1181
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1182
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 1183
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->j:Landroid/graphics/Bitmap;

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {p1, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1189
    :cond_6
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aw:Lfzz;

    if-eqz v1, :cond_7

    .line 1190
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1196
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    invoke-virtual {v0}, Liro;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->H:Z

    if-eqz v0, :cond_0

    .line 1197
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    .line 1198
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->s()V

    .line 1199
    iput-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    .line 1200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->i(Z)V

    .line 1201
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    goto/16 :goto_0

    .line 1184
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1185
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1186
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 1187
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->k:Landroid/graphics/Bitmap;

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {p1, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1204
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->K:Z

    if-eqz v0, :cond_0

    .line 1205
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->p:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->t:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 469
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->Q:Z

    if-eqz v0, :cond_1

    .line 470
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->aa:Z

    if-nez v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ad:Lgai;

    invoke-virtual {v0, p3, p4}, Lgai;->a(FF)Z

    .line 473
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->aa:Z

    .line 475
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1212
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ak:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1213
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getChildCount()I

    move-result v1

    .line 1214
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1215
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1216
    invoke-virtual {v2, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 1214
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1219
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->E:Z

    .line 1220
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v0

    .line 1221
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v1

    .line 1223
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PhotoView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 1224
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    .line 1225
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    .line 1226
    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 1227
    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 1228
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->w:Landroid/widget/ProgressBar;

    add-int/2addr v2, v0

    add-int/2addr v3, v1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/ProgressBar;->layout(IIII)V

    .line 1231
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/PhotoView;->i(Z)V

    .line 1232
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 440
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 1236
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 1237
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getChildCount()I

    move-result v1

    .line 1238
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1239
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1240
    invoke-virtual {v2, p1, p2}, Landroid/view/View;->measure(II)V

    .line 1238
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1242
    :cond_0
    return-void
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 480
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    sub-float/2addr v0, v3

    .line 486
    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ab:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ab:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ab:F

    add-float/2addr v1, v0

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ab:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_2

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ab:F

    add-float/2addr v1, v0

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    :cond_1
    float-to-double v2, v0

    const-wide v4, 0x3fb999999999999aL    # 0.1

    cmpl-double v1, v2, v4

    if-lez v1, :cond_2

    .line 497
    :goto_0
    return v6

    .line 491
    :cond_2
    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->ab:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ab:F

    .line 492
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->S:Z

    .line 493
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->m()F

    move-result v0

    .line 494
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    mul-float/2addr v0, v1

    .line 496
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoView;->b(FFF)V

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 502
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->Q:Z

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->T:Lgag;

    invoke-virtual {v0}, Lgag;->a()V

    .line 504
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->S:Z

    .line 505
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ab:F

    .line 507
    :cond_0
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 512
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->Q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->S:Z

    if-eqz v0, :cond_0

    .line 513
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->R:Z

    .line 514
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->l()V

    .line 516
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aa:Z

    .line 517
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 448
    .line 449
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ac:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 450
    const-wide/16 v2, 0xc8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->Q:Z

    if-eqz v0, :cond_0

    .line 452
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    sub-float/2addr v0, p3

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->aq:F

    .line 453
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    sub-float/2addr v0, p4

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ar:F

    .line 454
    neg-float v0, p3

    neg-float v1, p4

    invoke-virtual {p0, v4, v0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->a(ZFF)Z

    .line 456
    :cond_0
    return v4
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 444
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->P:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->S:Z

    if-nez v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->P:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 429
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->S:Z

    .line 430
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 435
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->av:Z

    if-nez v0, :cond_1

    .line 383
    :cond_0
    :goto_0
    return v6

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->O:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->N:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 344
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 346
    packed-switch v0, :pswitch_data_0

    .line 355
    :cond_2
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 356
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 358
    packed-switch v2, :pswitch_data_1

    :pswitch_0
    goto :goto_0

    .line 377
    :pswitch_1
    iget-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->au:J

    sub-long v2, v0, v2

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 378
    iput-boolean v6, p0, Lcom/google/android/apps/plus/views/PhotoView;->at:Z

    .line 380
    :cond_3
    iput-wide v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->au:J

    goto :goto_0

    .line 348
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 349
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ac:J

    goto :goto_1

    .line 350
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v6, :cond_2

    .line 351
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ac:J

    goto :goto_1

    .line 367
    :pswitch_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->at:Z

    if-eqz v2, :cond_6

    iget-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->au:J

    sub-long/2addr v0, v2

    .line 368
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_6

    .line 369
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->R:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->Q:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->m()F

    move-result v1

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    div-float v0, v1, v0

    :goto_2
    const v2, 0x3f851eb8    # 1.04f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_8

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    :goto_3
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->V:F

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->aq:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->ar:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->T:Lgag;

    invoke-virtual {v2, v1, v0}, Lgag;->a(FF)Z

    new-instance v2, Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->C:Landroid/graphics/Matrix;

    invoke-direct {v2, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->aq:F

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->ar:F

    invoke-virtual {v2, v0, v0, v1, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/PhotoView;->a(Landroid/graphics/Matrix;)V

    :cond_5
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->R:Z

    .line 371
    :cond_6
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->at:Z

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->ad:Lgai;

    invoke-static {v0}, Lgai;->a(Lgai;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->u()V

    goto/16 :goto_0

    .line 369
    :cond_7
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_8
    const/high16 v0, 0x40200000    # 2.5f

    mul-float/2addr v0, v1

    goto :goto_3

    .line 346
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_2
    .end packed-switch

    .line 358
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 521
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->P:Landroid/view/View$OnClickListener;

    .line 522
    return-void
.end method

.method public setVisibility(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 857
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 858
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 859
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 861
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 859
    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 865
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->v:Liro;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
