.class public Lcom/google/android/apps/plus/phone/InviteContactActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lbc;
.implements Ldid;
.implements Lhmq;
.implements Lhob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llon;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ldid;",
        "Lhmq;",
        "Lhob;"
    }
.end annotation


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field private h:Lhet;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Ldib;

.field private final m:Lhoc;

.field private final n:Lhov;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 65
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "data1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Llon;-><init>()V

    .line 83
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->e:Llnh;

    .line 84
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->h:Lhet;

    .line 91
    new-instance v0, Lhoc;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Landroid/app/Activity;Llqr;)V

    .line 92
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->m:Lhoc;

    .line 93
    new-instance v0, Lhov;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->f:Llqc;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->n:Lhov;

    .line 94
    new-instance v0, Lirl;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lirl;-><init>(Landroid/app/Activity;Llqr;)V

    .line 100
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/InviteContactActivity;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 56
    :try_start_0
    new-instance v1, Lezc;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, v0}, Lezc;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->f()Lae;

    move-result-object v0

    const-string v2, "pick_email"

    invoke-virtual {v1, v0, v2}, Lezc;->a(Lae;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "InviteContactActivity"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "InviteContactActivity"

    const-string v2, "Cannot show dialog"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 416
    sget-object v0, Lhmw;->P:Lhmw;

    return-object v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 384
    const/16 v0, 0x42

    return v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 175
    const-string v0, "data_uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 176
    const-string v1, "entities"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 177
    new-instance v0, Lhye;

    sget-object v3, Lcom/google/android/apps/plus/phone/InviteContactActivity;->g:[Ljava/lang/String;

    const-string v4, "mimetype IN (\'vnd.android.cursor.item/name\',\'vnd.android.cursor.item/email_v2\')"

    move-object v1, p0

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 182
    if-nez p1, :cond_1

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->k:Z

    if-nez v0, :cond_0

    .line 191
    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->k:Z

    .line 193
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 194
    :cond_2
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 195
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_3

    .line 197
    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->j:Ljava/lang/String;

    .line 200
    :cond_3
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 201
    const-string v2, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_4

    .line 204
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 207
    :cond_4
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 208
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 213
    :cond_5
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 215
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 216
    if-nez v0, :cond_6

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->n:Lhov;

    new-instance v1, Leyy;

    invoke-direct {v1, p0}, Leyy;-><init>(Lcom/google/android/apps/plus/phone/InviteContactActivity;)V

    invoke-virtual {v0, v1}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    goto :goto_0

    .line 224
    :cond_6
    if-ne v0, v3, :cond_7

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->n:Lhov;

    new-instance v2, Leyz;

    invoke-direct {v2, p0, v1}, Leyz;-><init>(Lcom/google/android/apps/plus/phone/InviteContactActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v2}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    goto :goto_0

    .line 233
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->n:Lhov;

    new-instance v2, Leza;

    invoke-direct {v2, p0, v1}, Leza;-><init>(Lcom/google/android/apps/plus/phone/InviteContactActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v2}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 348
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->e:Llnh;

    const-class v1, Lhmq;

    .line 350
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhoc;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->m:Lhoc;

    .line 351
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 352
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 56
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 401
    const-string v0, "ModifyCircleMembershipsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->l:Ldib;

    if-eqz v0, :cond_0

    .line 403
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->l:Ldib;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->h:Lhet;

    invoke-virtual {v1}, Lhet;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ldib;->a(I)V

    .line 408
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->l:Ldib;

    .line 410
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    .line 412
    :cond_1
    return-void

    .line 406
    :cond_2
    invoke-virtual {p2}, Lhoz;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 358
    .line 359
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a08a3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 364
    new-instance v1, Ldpj;

    invoke-direct {v1, p0}, Ldpj;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->h:Lhet;

    .line 365
    invoke-virtual {v2}, Lhet;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ldpj;->a(I)Ldpj;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i:Ljava/lang/String;

    .line 366
    invoke-virtual {v1, v2}, Ldpj;->a(Ljava/lang/String;)Ldpj;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->j:Ljava/lang/String;

    .line 367
    invoke-virtual {v1, v2}, Ldpj;->b(Ljava/lang/String;)Ldpj;

    move-result-object v1

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ldpj;->b(I)Ldpj;

    move-result-object v1

    .line 369
    invoke-virtual {v1, p1}, Ldpj;->a(Ljava/util/ArrayList;)Ldpj;

    move-result-object v1

    .line 370
    invoke-virtual {v1, v5}, Ldpj;->b(Ljava/util/ArrayList;)Ldpj;

    move-result-object v1

    const/4 v2, 0x1

    .line 371
    invoke-virtual {v1, v2}, Ldpj;->a(Z)Ldpj;

    move-result-object v1

    .line 372
    invoke-virtual {v1, v3}, Ldpj;->b(Z)Ldpj;

    move-result-object v1

    .line 373
    invoke-virtual {v1, v3}, Ldpj;->c(Z)Ldpj;

    move-result-object v1

    .line 374
    invoke-virtual {v1, v0}, Ldpj;->c(Ljava/lang/String;)Ldpj;

    move-result-object v0

    .line 375
    invoke-virtual {v0}, Ldpj;->a()Ldpi;

    move-result-object v0

    .line 376
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->m:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 378
    new-instance v0, Ldib;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->e:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhms;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->l:Ldib;

    .line 380
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 421
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 279
    const-string v1, "e:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i:Ljava/lang/String;

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i()V

    .line 281
    return-void

    .line 279
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public h()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->h:Lhet;

    .line 252
    invoke-virtual {v0}, Lhet;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->j:Ljava/lang/String;

    .line 251
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "picker_mode"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "search_circles_usage"

    const/4 v3, -0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "search_pub_profiles_enabled"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "search_phones_enabled"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "search_plus_pages_enabled"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "search_in_circles_enabled"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "query"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "filter_null_gaia_ids"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v2, v4}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 255
    return-void
.end method

.method protected i()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->h:Lhet;

    .line 329
    invoke-virtual {v0}, Lhet;->d()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->j:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v0, p0

    .line 328
    invoke-static/range {v0 .. v5}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 333
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 285
    const/4 v0, 0x0

    .line 286
    packed-switch p1, :pswitch_data_0

    .line 319
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 320
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    .line 322
    :cond_1
    return-void

    .line 288
    :pswitch_0
    if-ne p2, v2, :cond_0

    .line 289
    const-string v2, "person_id"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i:Ljava/lang/String;

    .line 290
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 291
    const-string v0, "person_data"

    .line 292
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljqs;

    .line 293
    invoke-virtual {v0}, Ljqs;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->j:Ljava/lang/String;

    .line 294
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i()V

    move v0, v1

    .line 296
    goto :goto_0

    .line 301
    :pswitch_1
    if-ne p2, v2, :cond_0

    .line 302
    const-string v0, "selected_circle_ids"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 305
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->n:Lhov;

    new-instance v3, Lezb;

    invoke-direct {v3, p0, v0}, Lezb;-><init>(Lcom/google/android/apps/plus/phone/InviteContactActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    move v0, v1

    .line 313
    goto :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 147
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 148
    if-eqz p1, :cond_0

    .line 149
    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i:Ljava/lang/String;

    .line 150
    const-string v0, "person_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->j:Ljava/lang/String;

    .line 151
    const-string v0, "redirected"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->k:Z

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->e:Llnh;

    const-class v1, Lhms;

    .line 153
    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 152
    invoke-static {p0, v0, p1}, Ldib;->a(Landroid/content/Context;Lhms;Landroid/os/Bundle;)Ldib;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->l:Ldib;

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    :goto_0
    return-void

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 163
    if-nez v0, :cond_2

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    goto :goto_0

    .line 168
    :cond_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 169
    const-string v2, "data_uri"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->g()Lbb;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 337
    invoke-super {p0, p1}, Llon;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 338
    const-string v0, "person_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v0, "person_name"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string v0, "redirected"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->l:Ldib;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->l:Ldib;

    invoke-virtual {v0, p1}, Ldib;->a(Landroid/os/Bundle;)V

    .line 344
    :cond_0
    return-void
.end method
