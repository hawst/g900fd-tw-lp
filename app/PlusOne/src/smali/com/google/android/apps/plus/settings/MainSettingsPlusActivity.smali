.class public Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lizi;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Lloa;-><init>()V

    .line 31
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->x:Llnh;

    .line 32
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 35
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 36
    new-instance v0, Lfkv;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfkv;-><init>(Los;Llqr;)V

    .line 43
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->x:Llnh;

    .line 44
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 45
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 46
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 92
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 93
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->startActivity(Landroid/content/Intent;)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->finish()V

    .line 95
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->b(I)V

    .line 87
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->x:Llnh;

    const-class v1, Lizi;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 58
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 70
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 71
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_settings"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 72
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->b(I)V

    .line 82
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v0, 0x7f04007a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/MainSettingsPlusActivity;->setContentView(I)V

    .line 52
    return-void
.end method
