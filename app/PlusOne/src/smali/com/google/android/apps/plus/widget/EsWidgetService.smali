.class public Lcom/google/android/apps/plus/widget/EsWidgetService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static final a:[I

.field private static b:Z

.field private static c:Landroid/graphics/Bitmap;

.field private static d:I

.field private static e:I

.field private static f:I

.field private static g:Lizs;

.field private static h:Lhso;


# instance fields
.field private i:Lkzl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f10064f
        0x7f100650
        0x7f100651
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 91
    const-string v0, "EsWidgetService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method private a(ILgcj;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 637
    const-string v0, "EsWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] loadCursor"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 641
    :cond_0
    iget v1, p2, Lgcj;->a:I

    .line 642
    iget-object v0, p2, Lgcj;->b:Ljava/lang/String;

    .line 644
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "v.all.circles"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 645
    :cond_1
    invoke-static {v3, v3, v3, v4, v5}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v3

    .line 655
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetService;->i:Lkzl;

    sget-object v2, Llbf;->a:[Ljava/lang/String;

    const/16 v5, 0xa

    .line 657
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    move v5, v4

    .line 655
    invoke-interface/range {v0 .. v6}, Lkzl;->a(I[Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 659
    return-object v0

    .line 647
    :cond_2
    const-string v2, "v.whatshot"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 648
    invoke-static {v3, v3, v3, v4, v4}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 651
    :cond_3
    invoke-static {v3, v0, v3, v4, v5}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private a(Lgcj;ILandroid/database/Cursor;Z)Landroid/widget/RemoteViews;
    .locals 18

    .prologue
    .line 268
    move-object/from16 v0, p1

    iget v15, v0, Lgcj;->a:I

    .line 269
    const-string v2, "EsWidget"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 270
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] createRemoteViews: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 273
    :cond_0
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f040238

    invoke-direct {v3, v2, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 275
    const v2, 0x7f10062f

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 277
    const v2, 0x7f100637

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 278
    const v2, 0x7f100638

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 279
    const v2, 0x7f100639

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 280
    const v2, 0x7f10063a

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 281
    const v2, 0x7f10063b

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 282
    const v2, 0x7f10063c

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 284
    const v2, 0x7f10063d

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 285
    const v2, 0x7f100114

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 286
    const v2, 0x7f10063e

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 287
    const v2, 0x7f100641

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 288
    const v2, 0x7f100640

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 289
    const v2, 0x7f100642

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 291
    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 292
    if-nez v5, :cond_2

    .line 412
    :cond_1
    :goto_0
    return-object v3

    .line 296
    :cond_2
    const/16 v2, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 298
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/content/Context;Landroid/database/Cursor;)Lgch;

    move-result-object v4

    .line 300
    iget-object v2, v4, Lgch;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v6, 0x1

    .line 301
    :goto_1
    iget-object v2, v4, Lgch;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    const/4 v7, 0x1

    .line 302
    :goto_2
    if-nez v6, :cond_b

    iget-object v2, v4, Lgch;->d:Lizu;

    if-nez v2, :cond_b

    const/4 v2, 0x1

    move v14, v2

    .line 304
    :goto_3
    if-eqz v14, :cond_c

    const v2, 0x7f10064c

    move v10, v2

    .line 305
    :goto_4
    if-eqz v14, :cond_d

    const v2, 0x7f10064d

    move v9, v2

    .line 306
    :goto_5
    if-eqz v14, :cond_e

    const v2, 0x7f10064e

    move v8, v2

    .line 308
    :goto_6
    const v11, 0x7f100636

    if-eqz v14, :cond_f

    const/16 v2, 0x8

    :goto_7
    invoke-virtual {v3, v11, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 309
    if-eqz v7, :cond_10

    .line 310
    const v2, 0x7f100637

    const/4 v11, 0x0

    invoke-virtual {v3, v2, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 311
    const v2, 0x7f10063d

    const/16 v11, 0x8

    invoke-virtual {v3, v2, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 316
    :goto_8
    const v11, 0x7f10064b

    if-eqz v14, :cond_11

    const/4 v2, 0x0

    :goto_9
    invoke-virtual {v3, v11, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 318
    sget-object v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v10, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 319
    const/4 v2, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 320
    if-eqz v2, :cond_3

    .line 322
    :try_start_0
    sget-object v11, Lcom/google/android/apps/plus/widget/EsWidgetService;->h:Lhso;

    .line 323
    invoke-static {v2}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v16, 0x2

    const/16 v17, 0x1

    .line 322
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v2, v0, v1}, Lhso;->a(Ljava/lang/String;II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 325
    if-eqz v2, :cond_3

    .line 326
    invoke-virtual {v3, v10, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    :goto_a
    move-object/from16 v2, p0

    .line 335
    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Lgch;Ljava/lang/String;ZZ)Z

    move-result v2

    .line 338
    if-eqz v6, :cond_13

    .line 339
    if-nez p4, :cond_4

    .line 340
    const v6, 0x7f100642

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 341
    const v6, 0x7f100642

    iget-object v4, v4, Lgch;->c:Ljava/lang/String;

    invoke-virtual {v3, v6, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 343
    :cond_4
    if-eqz v2, :cond_12

    .line 344
    const v2, 0x7f100641

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 363
    :cond_5
    :goto_b
    const/4 v2, 0x4

    .line 364
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 363
    invoke-virtual {v3, v9, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 366
    move-object/from16 v0, p1

    iget-object v2, v0, Lgcj;->c:Ljava/lang/String;

    invoke-virtual {v3, v8, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 368
    const/16 v2, 0xc

    .line 369
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 368
    invoke-static {v2}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v9

    .line 370
    const/16 v2, 0xd

    .line 371
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 370
    invoke-static {v2}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v11

    .line 373
    const/4 v10, 0x0

    .line 374
    const/16 v2, 0xe

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 375
    const/16 v4, 0xf

    .line 376
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 377
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 378
    const v2, 0x7f0a0723

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 381
    :cond_6
    if-eqz v14, :cond_14

    move-object/from16 v7, p0

    move-object v8, v3

    .line 382
    invoke-static/range {v7 .. v13}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;J)V

    .line 387
    :cond_7
    :goto_c
    const/4 v11, 0x1

    move-object/from16 v6, p0

    move v7, v15

    move/from16 v8, p2

    move-object v9, v3

    move-object v10, v5

    invoke-static/range {v6 .. v11}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->a(Landroid/content/Context;IILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    .line 391
    const/4 v2, -0x1

    if-eq v15, v2, :cond_1

    if-eqz v5, :cond_1

    .line 392
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v15, v1, v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->a(Landroid/content/Context;IILjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 394
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v15, v1}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v4

    .line 396
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-ge v5, v6, :cond_8

    .line 400
    invoke-virtual {v4, v2}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 403
    :cond_8
    invoke-static/range {p0 .. p0}, Lda;->a(Landroid/content/Context;)Lda;

    move-result-object v5

    .line 404
    invoke-virtual {v5, v4}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 405
    invoke-virtual {v5, v2}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 407
    const/4 v2, 0x0

    const/high16 v4, 0x8000000

    invoke-virtual {v5, v2, v4}, Lda;->a(II)Landroid/app/PendingIntent;

    move-result-object v2

    .line 409
    const v4, 0x7f10064a

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_0

    .line 300
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 301
    :cond_a
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 302
    :cond_b
    const/4 v2, 0x0

    move v14, v2

    goto/16 :goto_3

    .line 304
    :cond_c
    const v2, 0x7f100643

    move v10, v2

    goto/16 :goto_4

    .line 305
    :cond_d
    const v2, 0x7f100644

    move v9, v2

    goto/16 :goto_5

    .line 306
    :cond_e
    const v2, 0x7f100645

    move v8, v2

    goto/16 :goto_6

    .line 308
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 313
    :cond_10
    const v2, 0x7f100637

    const/16 v11, 0x8

    invoke-virtual {v3, v2, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 314
    const v2, 0x7f10063d

    const/4 v11, 0x0

    invoke-virtual {v3, v2, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_8

    .line 316
    :cond_11
    const/16 v2, 0x8

    goto/16 :goto_9

    .line 328
    :catch_0
    move-exception v2

    .line 329
    const-string v10, "EsWidget"

    const-string v11, "Cannot download avatar"

    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_a

    .line 330
    :catch_1
    move-exception v2

    .line 331
    const-string v10, "EsWidget"

    const-string v11, "Canceled"

    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_a

    .line 346
    :cond_12
    const v2, 0x7f100640

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_b

    .line 348
    :cond_13
    if-eqz v7, :cond_5

    .line 349
    if-nez p4, :cond_5

    .line 350
    const v2, 0x7f100639

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 351
    const v2, 0x7f10063a

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 353
    const v2, 0x7f10063b

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 354
    const v2, 0x7f10063b

    iget-object v6, v4, Lgch;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 356
    const v2, 0x7f10063c

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 357
    const v2, 0x7f10063c

    .line 358
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f11003c

    iget v10, v4, Lgch;->b:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v16, 0x0

    iget v4, v4, Lgch;->b:I

    .line 359
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v11, v16

    .line 358
    invoke-virtual {v6, v7, v10, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 357
    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_b

    .line 384
    :cond_14
    const v2, 0x7f100647

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v2, 0x7f100648

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v2, 0x7f100646

    const/16 v4, 0x8

    invoke-virtual {v3, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    const v2, 0x7f100647

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    invoke-static {v3, v2, v9, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    const v2, 0x7f100648

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    invoke-static {v3, v2, v10, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    goto/16 :goto_c

    :cond_15
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_16

    const v2, 0x7f100647

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    invoke-static {v3, v2, v10, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    const v2, 0x7f100648

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    invoke-static {v3, v2, v11, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    goto/16 :goto_c

    :cond_16
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_17

    const v2, 0x7f100646

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    invoke-static {v3, v2, v11, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    goto/16 :goto_c

    :cond_17
    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    const v4, 0x7f100646

    sget v6, Lcom/google/android/apps/plus/widget/EsWidgetService;->e:I

    invoke-static {v3, v4, v2, v6}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    goto/16 :goto_c
.end method

.method private static a(Landroid/content/Context;Landroid/database/Cursor;)Lgch;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/16 v3, 0x17

    .line 416
    const/4 v0, 0x0

    .line 417
    new-instance v2, Lgch;

    invoke-direct {v2}, Lgch;-><init>()V

    .line 418
    const/16 v1, 0xa

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 419
    invoke-interface {p1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 420
    const-wide/16 v6, 0x40

    and-long/2addr v6, v4

    cmp-long v1, v6, v8

    if-eqz v1, :cond_2

    .line 421
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 422
    invoke-static {v1}, Lkzr;->a([B)Lkzr;

    move-result-object v1

    .line 423
    if-eqz v1, :cond_0

    .line 424
    invoke-virtual {v1}, Lkzr;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lgch;->a:Ljava/lang/String;

    .line 425
    invoke-virtual {v1}, Lkzr;->b()I

    move-result v3

    iput v3, v2, Lgch;->b:I

    .line 426
    iget v3, v2, Lgch;->b:I

    if-lez v3, :cond_0

    .line 427
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lkzr;->a(I)Lkzv;

    move-result-object v0

    :cond_0
    move-object v1, v0

    .line 438
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lkzv;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 439
    invoke-virtual {v1}, Lkzv;->e()Ljava/lang/String;

    move-result-object v3

    .line 441
    invoke-virtual {v1}, Lkzv;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Ljac;->b:Ljac;

    .line 440
    :goto_1
    invoke-static {p0, v3, v0}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, v2, Lgch;->d:Lizu;

    .line 442
    sget v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    iput v0, v2, Lgch;->e:I

    .line 443
    sget v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    iput v0, v2, Lgch;->f:I

    .line 445
    invoke-virtual {v1}, Lkzv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 446
    invoke-virtual {v1}, Lkzv;->a()Ljava/lang/String;

    move-result-object v0

    .line 447
    if-eqz v0, :cond_1

    .line 448
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lgch;->c:Ljava/lang/String;

    .line 453
    :cond_1
    return-object v2

    .line 430
    :cond_2
    const-wide/16 v6, 0x20

    and-long/2addr v4, v6

    cmp-long v1, v4, v8

    if-eqz v1, :cond_4

    .line 431
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 432
    if-eqz v1, :cond_4

    .line 433
    invoke-static {v1}, Lkzv;->a([B)Lkzv;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 441
    :cond_3
    sget-object v0, Ljac;->a:Ljac;

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public static synthetic a()Lizs;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->g:Lizs;

    return-object v0
.end method

.method private static a(Landroid/content/Context;J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 566
    invoke-static {p1, p2}, Llap;->a(J)I

    move-result v0

    .line 567
    if-eqz v0, :cond_0

    .line 568
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 570
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILgcj;Z)V
    .locals 21

    .prologue
    .line 666
    const-string v2, "EsWidget"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 667
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] loadActivities"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 670
    :cond_0
    const/4 v4, 0x0

    .line 672
    const-string v2, "v.whatshot"

    move-object/from16 v0, p2

    iget-object v3, v0, Lgcj;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 673
    const/4 v4, 0x1

    .line 674
    const/4 v5, 0x0

    .line 681
    :goto_0
    new-instance v14, Lkfp;

    invoke-direct {v14}, Lkfp;-><init>()V

    .line 682
    move/from16 v0, p3

    invoke-virtual {v14, v0}, Lkfp;->a(Z)V

    .line 683
    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x36

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Get activities for widget circleId: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " view: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Lkfp;->b(Ljava/lang/String;)V

    .line 684
    const-string v2, "Activities:SyncStream"

    invoke-virtual {v14, v2}, Lkfp;->c(Ljava/lang/String;)V

    .line 687
    :try_start_0
    move-object/from16 v0, p2

    iget v3, v0, Lgcj;->a:I

    .line 688
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x14

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const-wide/16 v18, 0x0

    const/16 v20, 0x0

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v20}, Llap;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZZZJ[Ljava/lang/String;)Lkff;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 698
    invoke-virtual {v14}, Lkfp;->f()V

    .line 699
    invoke-virtual {v14}, Lkfp;->g()V

    .line 700
    :goto_1
    return-void

    .line 675
    :cond_1
    const-string v2, "v.all.circles"

    move-object/from16 v0, p2

    iget-object v3, v0, Lgcj;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 676
    const/4 v5, 0x0

    goto :goto_0

    .line 678
    :cond_2
    move-object/from16 v0, p2

    iget-object v5, v0, Lgcj;->b:Ljava/lang/String;

    goto :goto_0

    .line 693
    :catch_0
    move-exception v2

    .line 694
    :try_start_1
    const-string v3, "EsWidget"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 695
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] loadActivities failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 698
    :cond_3
    invoke-virtual {v14}, Lkfp;->f()V

    .line 699
    invoke-virtual {v14}, Lkfp;->g()V

    goto :goto_1

    .line 698
    :catchall_0
    move-exception v2

    invoke-virtual {v14}, Lkfp;->f()V

    .line 699
    invoke-virtual {v14}, Lkfp;->g()V

    throw v2
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;J)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 534
    .line 535
    sget-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->a:[I

    .line 536
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 537
    sget-object v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->a:[I

    const/4 v0, 0x1

    aget v1, v2, v1

    .line 538
    sget v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    invoke-static {p1, v1, p2, v2}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    .line 541
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 542
    sget-object v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->a:[I

    add-int/lit8 v1, v0, 0x1

    aget v0, v2, v0

    .line 543
    sget v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    invoke-static {p1, v0, p3, v2}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    move v0, v1

    .line 546
    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 547
    sget-object v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->a:[I

    add-int/lit8 v1, v0, 0x1

    aget v0, v2, v0

    .line 548
    sget v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    invoke-static {p1, v0, p4, v2}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    .line 551
    :goto_1
    if-nez v1, :cond_2

    .line 552
    invoke-static {p0, p5, p6}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 553
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 554
    sget-object v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->a:[I

    add-int/lit8 v0, v1, 0x1

    aget v1, v3, v1

    .line 555
    sget v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->e:I

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V

    .line 559
    :goto_2
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 560
    sget-object v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->a:[I

    add-int/lit8 v1, v0, 0x1

    aget v0, v2, v0

    .line 561
    const/16 v2, 0x8

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    move v0, v1

    .line 562
    goto :goto_2

    .line 563
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move v1, v0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private static a(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 499
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 500
    invoke-virtual {p0, p1, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 501
    invoke-virtual {p0, p1, p3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 502
    return-void
.end method

.method private a(ILjava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 204
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return v2

    .line 208
    :cond_1
    new-array v3, v6, [Ljava/lang/String;

    const-string v0, "circle_id"

    aput-object v0, v3, v2

    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v5, v2

    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v1

    .line 212
    if-eqz v1, :cond_0

    .line 217
    :cond_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 218
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 219
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v2, v6

    goto :goto_0

    .line 224
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;Lgch;Ljava/lang/String;ZZ)Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const v9, 0x7f100638

    const v8, 0x7f100114

    const/4 v6, 0x0

    .line 579
    .line 580
    iget-object v0, p2, Lgch;->d:Lizu;

    if-eqz v0, :cond_7

    .line 582
    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->g:Lizs;

    iget-object v1, p2, Lgch;->d:Lizu;

    const/4 v2, 0x0

    iget v3, p2, Lgch;->e:I

    iget v4, p2, Lgch;->f:I

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    .line 591
    :goto_0
    if-nez v0, :cond_0

    if-nez p4, :cond_0

    .line 592
    sget v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    sget v1, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {p0}, Lful;->a(Landroid/content/Context;)Lful;

    move-result-object v2

    invoke-virtual {v2, p3}, Lful;->a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 595
    :cond_0
    if-eqz v0, :cond_3

    .line 596
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    if-gt v1, v2, :cond_1

    .line 597
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    if-le v1, v2, :cond_2

    .line 598
    :cond_1
    sget v1, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    sget v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    invoke-static {v0, v1, v2, v7}, Llrw;->a(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 603
    :cond_2
    if-eqz p5, :cond_4

    .line 604
    invoke-virtual {p1, v9, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 605
    invoke-virtual {p1, v9, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 618
    :cond_3
    :goto_1
    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    return v0

    .line 586
    :catch_0
    move-exception v0

    const-string v0, "EsWidget"

    iget-object v1, p2, Lgch;->d:Lizu;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not download image: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 589
    goto :goto_0

    .line 587
    :catch_1
    move-exception v0

    .line 588
    const-string v1, "EsWidget"

    const-string v2, "Canceled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v7

    goto :goto_0

    .line 607
    :cond_4
    invoke-virtual {p1, v8, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 608
    invoke-virtual {p1, v8, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 610
    sget-object v1, Ljac;->b:Ljac;

    iget-object v2, p2, Lgch;->d:Lizu;

    invoke-virtual {v2}, Lizu;->g()Ljac;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 611
    const v1, 0x7f10063e

    invoke-virtual {p1, v1, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    .line 612
    :cond_5
    sget-object v1, Ljac;->d:Ljac;

    iget-object v2, p2, Lgch;->d:Lizu;

    invoke-virtual {v2}, Lizu;->g()Ljac;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 613
    const v1, 0x7f10063f

    invoke-virtual {p1, v1, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    :cond_6
    move v0, v6

    .line 618
    goto :goto_2

    :cond_7
    move-object v0, v7

    goto :goto_1
.end method

.method public static synthetic b()Lhso;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->h:Lhso;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 97
    const-class v0, Lizs;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->g:Lizs;

    .line 98
    const-class v0, Lhso;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    sput-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->h:Lhso;

    .line 99
    const-class v0, Lkzl;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetService;->i:Lkzl;

    .line 100
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    sget-boolean v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->b:Z

    if-nez v2, :cond_0

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 130
    invoke-static {p0, v0}, Lhss;->c(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->c:Landroid/graphics/Bitmap;

    .line 132
    const v3, 0x7f0b0141

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->d:I

    .line 133
    const v3, 0x7f0b013d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->e:I

    .line 135
    const v3, 0x7f0c0075

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v3}, Llsc;->a(Landroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 135
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->f:I

    .line 138
    sput-boolean v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->b:Z

    .line 141
    :cond_0
    const-string v2, "appWidgetId"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 143
    if-nez v3, :cond_2

    .line 201
    :cond_1
    :goto_0
    return-void

    .line 147
    :cond_2
    invoke-static {p0, v3}, Lgci;->a(Landroid/content/Context;I)Lgcj;

    move-result-object v4

    .line 148
    const-string v2, "refresh"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 149
    const-string v2, "activity_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 151
    if-nez v4, :cond_3

    .line 152
    invoke-static {p0, v3}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 154
    :cond_3
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v7

    .line 155
    if-eqz v7, :cond_1

    .line 159
    if-eqz v5, :cond_4

    .line 160
    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(ILgcj;Z)V

    .line 162
    :cond_4
    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(ILgcj;)Landroid/database/Cursor;

    move-result-object v2

    .line 164
    if-eqz v2, :cond_5

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-nez v8, :cond_5

    if-nez v5, :cond_5

    .line 165
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 167
    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(ILgcj;Z)V

    .line 168
    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(ILgcj;)Landroid/database/Cursor;

    move-result-object v2

    .line 171
    :cond_5
    if-eqz v2, :cond_6

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_9

    .line 172
    :cond_6
    iget v0, v4, Lgcj;->a:I

    iget-object v1, v4, Lgcj;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 173
    invoke-static {p0, v3, v4}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->a(Landroid/content/Context;ILgcj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    :goto_1
    if-eqz v2, :cond_1

    .line 197
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 175
    :cond_7
    :try_start_1
    invoke-static {p0, v3}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->a(Landroid/content/Context;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 196
    :catchall_0
    move-exception v0

    move-object v1, v2

    if-eqz v1, :cond_8

    .line 197
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 178
    :cond_9
    :try_start_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    const/4 v5, -0x1

    invoke-interface {v2, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_a
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_e

    const/4 v5, 0x2

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_b

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 181
    :cond_b
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-le v5, v0, :cond_d

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_c

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_c
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Landroid/content/Context;Landroid/database/Cursor;)Lgch;

    move-result-object v6

    const/4 v8, 0x3

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lgcg;

    invoke-direct {v9, v6, v8}, Lgcg;-><init>(Lgch;Ljava/lang/String;)V

    invoke-static {v9}, Llsx;->a(Ljava/lang/Runnable;)V

    invoke-interface {v2, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 184
    :cond_d
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_10

    .line 185
    invoke-virtual {v7, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v5

    .line 186
    const-string v6, "appWidgetCategory"

    const/4 v8, -0x1

    .line 187
    invoke-virtual {v5, v6, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v10, :cond_f

    .line 191
    :goto_3
    invoke-direct {p0, v4, v3, v2, v0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->a(Lgcj;ILandroid/database/Cursor;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 193
    invoke-virtual {v7, v3, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_1

    .line 178
    :cond_e
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_f
    move v0, v1

    .line 187
    goto :goto_3

    :cond_10
    move v0, v1

    goto :goto_3
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    if-nez p1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    .line 110
    const-string v0, "appWidgetId"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 113
    if-eqz v0, :cond_0

    .line 114
    invoke-static {p0, v0}, Lgci;->a(Landroid/content/Context;I)Lgcj;

    move-result-object v1

    .line 115
    if-nez v1, :cond_2

    .line 116
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 118
    :cond_2
    const-string v1, "refresh"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 119
    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->a(Landroid/content/Context;IZ)V

    goto :goto_0
.end method
