.class public Lcom/google/android/apps/moviemaker/service/PostSyncService;
.super Lbvt;
.source "PG"

# interfaces
.implements Lbwu;


# instance fields
.field private a:Livc;

.field private b:Livc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/moviemaker/service/PostSyncService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbvt;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method protected a()Lbwe;
    .locals 19

    .prologue
    .line 42
    invoke-static/range {p0 .. p0}, Lapm;->a(Landroid/content/Context;)Lapm;

    move-result-object v1

    invoke-virtual {v1}, Lapm;->a()Lamm;

    move-result-object v18

    .line 43
    const-string v1, "power"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/service/PostSyncService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/os/PowerManager;

    .line 44
    new-instance v1, Lbwq;

    .line 46
    invoke-interface/range {v18 .. v18}, Lamm;->L()Ljfb;

    move-result-object v3

    new-instance v4, Lbvu;

    .line 50
    invoke-interface/range {v18 .. v18}, Lamm;->p()Lanh;

    move-result-object v5

    .line 51
    invoke-interface/range {v18 .. v18}, Lamm;->L()Ljfb;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2, v5, v6}, Lbvu;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Lanh;Ljfb;)V

    .line 52
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->a(Landroid/content/Context;)Lbvy;

    move-result-object v5

    .line 53
    invoke-interface/range {v18 .. v18}, Lamm;->q()Lajt;

    move-result-object v6

    new-instance v7, Lbit;

    .line 54
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/moviemaker/service/PostSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v7, v2}, Lbit;-><init>(Landroid/content/ContentResolver;)V

    .line 55
    invoke-interface/range {v18 .. v18}, Lamm;->p()Lanh;

    move-result-object v8

    const-string v2, "power"

    .line 56
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/service/PostSyncService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/PowerManager;

    .line 57
    invoke-interface/range {v18 .. v18}, Lamm;->v()Lbig;

    move-result-object v10

    new-instance v11, Lapo;

    .line 58
    invoke-interface/range {v18 .. v18}, Lamm;->p()Lanh;

    move-result-object v2

    invoke-direct {v11, v2}, Lapo;-><init>(Lani;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/moviemaker/service/PostSyncService;->a:Livc;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/moviemaker/service/PostSyncService;->b:Livc;

    .line 61
    invoke-interface/range {v18 .. v18}, Lamm;->K()Ljdw;

    move-result-object v14

    .line 62
    invoke-interface/range {v18 .. v18}, Lamm;->O()Lahw;

    move-result-object v15

    new-instance v16, Lbwd;

    .line 63
    invoke-interface/range {v18 .. v18}, Lamm;->p()Lanh;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lbwd;-><init>(Lanh;)V

    .line 64
    invoke-interface/range {v18 .. v18}, Lamm;->s()Lbks;

    move-result-object v17

    .line 65
    invoke-interface/range {v18 .. v18}, Lamm;->h()Ljava/util/concurrent/Executor;

    move-result-object v18

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v18}, Lbwq;-><init>(Lbwu;Ljfb;Lbvs;Lbvy;Lajt;Lbit;Lanh;Landroid/os/PowerManager;Lbig;Lapo;Livc;Livc;Ljdw;Lahw;Lbwv;Lbks;Ljava/util/concurrent/Executor;)V

    return-object v1
.end method

.method public a(Ljava/lang/String;J)V
    .locals 8

    .prologue
    .line 85
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/service/PostSyncService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 86
    const/4 v1, 0x2

    .line 88
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p2

    .line 89
    invoke-static {p0, p1}, Lbwq;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {p0, v5, v4, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 86
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 90
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lbwf;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/service/PostSyncService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 95
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 70
    invoke-static {p0}, Lapm;->a(Landroid/content/Context;)Lapm;

    move-result-object v0

    invoke-virtual {v0}, Lapm;->a()Lamm;

    move-result-object v0

    .line 71
    const-string v1, "PostSyncLog"

    invoke-interface {v0, v1}, Lamm;->a(Ljava/lang/String;)Livc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/service/PostSyncService;->a:Livc;

    .line 72
    const-string v1, "ClusteringLog"

    invoke-interface {v0, v1}, Lamm;->a(Ljava/lang/String;)Livc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/service/PostSyncService;->b:Livc;

    .line 73
    invoke-super {p0}, Lbvt;->onCreate()V

    .line 74
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Lbvt;->onDestroy()V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/service/PostSyncService;->a:Livc;

    invoke-static {v0}, Lcek;->a(Ljava/io/Closeable;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/service/PostSyncService;->b:Livc;

    invoke-static {v0}, Lcek;->a(Ljava/io/Closeable;)V

    .line 81
    return-void
.end method
