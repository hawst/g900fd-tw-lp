.class public Lcom/google/android/apps/plus/stories/phone/StoriesModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    const-class v0, Lfru;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    const-class v0, Lfru;

    new-instance v1, Lfru;

    invoke-direct {v1, p1}, Lfru;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    const-class v0, Ldhu;

    if-ne p2, v0, :cond_2

    .line 31
    const-class v0, Ldhu;

    new-instance v1, Lfnh;

    invoke-direct {v1}, Lfnh;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 32
    :cond_2
    const-class v0, Lfct;

    if-ne p2, v0, :cond_3

    .line 33
    const-class v0, Lfct;

    new-instance v1, Lfro;

    invoke-direct {v1}, Lfro;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 34
    :cond_3
    const-class v0, Lcrg;

    if-ne p2, v0, :cond_4

    .line 35
    const-class v0, Lcrg;

    new-instance v1, Lfng;

    invoke-direct {v1}, Lfng;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 36
    :cond_4
    const-class v0, Lfan;

    if-ne p2, v0, :cond_5

    .line 37
    const-class v0, Lfan;

    new-instance v1, Lfrl;

    invoke-direct {v1}, Lfrl;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 38
    :cond_5
    const-class v0, Lkjd;

    if-ne p2, v0, :cond_6

    .line 39
    const-class v0, Lkjd;

    new-instance v1, Lfrn;

    invoke-direct {v1}, Lfrn;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :cond_6
    const-class v0, Lcsx;

    if-ne p2, v0, :cond_7

    .line 41
    const-class v0, Lcsx;

    new-instance v1, Lfni;

    invoke-direct {v1}, Lfni;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :cond_7
    const-class v0, Llds;

    if-ne p2, v0, :cond_0

    .line 43
    const-class v0, Llds;

    new-instance v1, Lfrp;

    invoke-direct {v1, p1}, Lfrp;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
