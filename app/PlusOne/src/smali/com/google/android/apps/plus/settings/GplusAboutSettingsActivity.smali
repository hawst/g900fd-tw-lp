.class public Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lkiy;


# instance fields
.field private e:Lhee;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Lloa;-><init>()V

    .line 34
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->x:Llnh;

    .line 35
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->e:Lhee;

    .line 38
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 39
    new-instance v0, Lfjn;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfjn;-><init>(Los;Llqr;)V

    .line 51
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->x:Llnh;

    .line 52
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->x:Llnh;

    const-class v1, Lkiy;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 66
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 79
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 80
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_settings"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 81
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 90
    const-string v0, "about_terms_pref_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->x:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->f:Lhmv;

    .line 94
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 93
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 97
    :cond_0
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v0, 0x7f04007a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;->setContentView(I)V

    .line 60
    return-void
.end method
