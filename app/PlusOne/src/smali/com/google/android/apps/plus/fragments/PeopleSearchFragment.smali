.class public Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;
.super Lean;
.source "PG"

# interfaces
.implements Lelt;
.implements Lgax;
.implements Ljph;


# instance fields
.field private O:Ljava/lang/String;

.field private P:I

.field private Q:Lgav;

.field private R:Lely;

.field private S:Lelv;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lean;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->P:I

    .line 58
    return-void
.end method


# virtual methods
.method protected U()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    return-object v0
.end method

.method protected V()Z
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0}, Lely;->d()Z

    move-result v0

    return v0
.end method

.method protected W()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0}, Lely;->c()Z

    move-result v0

    return v0
.end method

.method protected X()I
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public Z()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Q:Lgav;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Q:Lgav;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgav;->a(Ljava/lang/CharSequence;)V

    .line 146
    :cond_0
    return-void
.end method

.method public a()I
    .locals 1

    .prologue
    .line 380
    const/16 v0, 0x4d

    return v0
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 199
    const v0, 0x7f040168

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 186
    invoke-super {p0, p1, p2, p3}, Lean;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 188
    const v1, 0x7f10012b

    .line 189
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 188
    invoke-static {v1}, Lgav;->a(Landroid/view/View;)Lgav;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Q:Lgav;

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Q:Lgav;

    const v2, 0x7f0a0818

    invoke-virtual {v1, v2}, Lgav;->a(I)V

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Q:Lgav;

    invoke-virtual {v1, p0}, Lgav;->a(Lgax;)V

    .line 193
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->c(Landroid/view/View;)V

    .line 194
    return-object v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 150
    invoke-super {p0, p1}, Lean;->a(Landroid/app/Activity;)V

    .line 151
    new-instance v0, Lely;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->p()Lae;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->w()Lbb;

    move-result-object v2

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->e()I

    move-result v3

    invoke-direct {v0, p1, v1, v2, v3}, Lely;-><init>(Landroid/content/Context;Lae;Lbb;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->T:Z

    invoke-virtual {v0, v1}, Lely;->c(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->U:Z

    invoke-virtual {v0, v1}, Lely;->e(Z)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->V:Z

    invoke-virtual {v0, v1}, Lely;->f(Z)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->P:I

    invoke-virtual {v0, v1}, Lely;->j_(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->W:Z

    invoke-virtual {v0, v1}, Lely;->g(Z)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->X:Z

    invoke-virtual {v0, v1}, Lely;->h(Z)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, v4}, Lely;->i(Z)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    .line 161
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "filter_null_gaia_ids"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 160
    invoke-virtual {v0, v1}, Lely;->b(Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p0}, Lely;->a(Lelt;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lely;->b(Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 168
    if-eqz p1, :cond_0

    .line 169
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    .line 171
    :cond_0
    invoke-super {p0, p1}, Lean;->a(Landroid/os/Bundle;)V

    .line 172
    return-void
.end method

.method public a(Lelv;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->S:Lelv;

    .line 76
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 216
    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    .line 218
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->n()Lz;

    move-result-object v0

    invoke-static {v0, p1}, Lewm;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lely;->b(Ljava/lang/String;)V

    .line 225
    :cond_1
    return-void

    .line 216
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 135
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    .line 137
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhxc;)V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->S:Lelv;

    invoke-interface {v0, p1, p2}, Lelv;->a(Ljava/lang/String;Lhxc;)V

    .line 332
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 357
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 337
    if-eqz p4, :cond_0

    .line 338
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->e()I

    move-result v1

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->eT:Lhmv;

    .line 341
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 342
    invoke-virtual {v1, p4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 339
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 346
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->x()Landroid/view/View;

    move-result-object v0

    .line 347
    if-eqz v0, :cond_1

    .line 348
    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->S:Lelv;

    invoke-interface {v0, p1, p2, p3}, Lelv;->a(Ljava/lang/String;Ljava/lang/String;Ljqs;)V

    .line 352
    return-void
.end method

.method public a(Ljpg;I)V
    .locals 0

    .prologue
    .line 327
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->T:Z

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p1}, Lely;->c(Z)V

    .line 97
    :cond_0
    return-void
.end method

.method public aa()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Q:Lgav;

    invoke-virtual {v0, v1}, Lgav;->a(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, v1}, Lely;->b(Ljava/lang/String;)V

    .line 285
    return-void
.end method

.method public an_()V
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->x()Landroid/view/View;

    move-result-object v0

    .line 320
    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->c(Landroid/view/View;)V

    .line 323
    :cond_0
    return-void
.end method

.method public b(Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Y:Landroid/widget/ProgressBar;

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->a(Landroid/widget/ProgressBar;)V

    .line 240
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 367
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->U:Z

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p1}, Lely;->e(Z)V

    .line 107
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 82
    iput p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->P:I

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p1}, Lely;->j_(I)V

    .line 86
    :cond_0
    return-void
.end method

.method protected c(Landroid/view/View;)V
    .locals 5

    .prologue
    const v4, 0x7f100484

    const v3, 0x102000a

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-nez v0, :cond_0

    .line 279
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0}, Lely;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 268
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 269
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->d(Landroid/view/View;)V

    goto :goto_0

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 271
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 272
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 273
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->h(Landroid/view/View;)V

    goto :goto_0

    .line 275
    :cond_2
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 276
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 277
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->h(Landroid/view/View;)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->V:Z

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p1}, Lely;->f(Z)V

    .line 114
    :cond_0
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0}, Lely;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0, p1}, Lean;->e(Landroid/os/Bundle;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p1}, Lely;->b(Landroid/os/Bundle;)V

    .line 180
    :cond_0
    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 204
    invoke-super {p0}, Lean;->g()V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0}, Lely;->f()V

    .line 206
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0}, Lean;->g()V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0}, Lely;->g()V

    .line 212
    return-void
.end method

.method public i(Z)V
    .locals 1

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->W:Z

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p1}, Lely;->g(Z)V

    .line 121
    :cond_0
    return-void
.end method

.method public j(Z)V
    .locals 1

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->X:Z

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p1}, Lely;->h(Z)V

    .line 128
    :cond_0
    return-void
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p1}, Lely;->a(Landroid/os/Bundle;)V

    .line 258
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->R:Lely;

    invoke-virtual {v0, p3}, Lely;->b(I)V

    .line 315
    return-void
.end method
