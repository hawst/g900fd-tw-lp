.class public Lcom/google/android/apps/plus/phone/AddToAlbumActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 38
    invoke-direct {p0}, Lloa;-><init>()V

    .line 43
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 45
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->x:Llnh;

    .line 46
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 48
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->x:Llnh;

    .line 49
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 52
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 53
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 55
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->x:Llnh;

    .line 56
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 59
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lhmw;->m:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 97
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 99
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 100
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 101
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 84
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 86
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 87
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 88
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 76
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 66
    if-nez p1, :cond_0

    .line 67
    new-instance v0, Lebj;

    invoke-direct {v0}, Lebj;-><init>()V

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 70
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AddToAlbumActivity;->setContentView(I)V

    .line 71
    return-void
.end method
