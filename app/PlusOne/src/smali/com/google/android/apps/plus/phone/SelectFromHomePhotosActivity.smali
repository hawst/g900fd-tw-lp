.class public Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field public final e:Lhee;

.field private final f:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 43
    invoke-direct {p0}, Lloa;-><init>()V

    .line 48
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 49
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 51
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->x:Llnh;

    .line 52
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 54
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->x:Llnh;

    .line 55
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 58
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 59
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 61
    new-instance v0, Lfcs;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfcs;-><init>(Landroid/app/Activity;Llqr;)V

    const/4 v1, 0x1

    .line 62
    invoke-virtual {v0, v1}, Lfcs;->a(I)Lfcs;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 63
    invoke-virtual {v0, v1}, Lfcs;->a(Ljava/lang/String;)Lfcs;

    .line 66
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->x:Llnh;

    .line 67
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 68
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->e:Lhee;

    .line 70
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 108
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 75
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 78
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 80
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 82
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsm;

    new-instance v2, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    .line 83
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->x:Llnh;

    const-class v1, Lcnt;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    .line 87
    const-string v1, "Photos"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcnt;->a(Ljava/lang/String;I)V

    .line 89
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 126
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 128
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 129
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 130
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 118
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x2

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 93
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 95
    if-nez p1, :cond_0

    .line 96
    new-instance v0, Legr;

    invoke-direct {v0}, Legr;-><init>()V

    .line 97
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 98
    const-string v2, "disable_up_button"

    const/4 v3, 0x2

    invoke-static {p0, v3}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 99
    invoke-virtual {v0, v1}, Lu;->f(Landroid/os/Bundle;)V

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->f:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 102
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;->setContentView(I)V

    .line 103
    return-void
.end method
