.class public Lcom/google/android/apps/plus/views/DreamViewFlipper;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Ljbb;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private e:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Landroid/database/Cursor;

.field private final i:Landroid/os/Handler;

.field private j:Z

.field private k:Z

.field private l:Z

.field private final m:Ljava/lang/Runnable;

.field private final n:Ljava/lang/Runnable;

.field private o:Lfxc;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "tile_id"

    aput-object v1, v0, v3

    const-string v1, "photo_id"

    aput-object v1, v0, v4

    const-string v1, "image_url"

    aput-object v1, v0, v5

    const-string v1, "owner_id"

    aput-object v1, v0, v6

    const-string v1, "NULL AS _data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "NULL AS _id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a:[Ljava/lang/String;

    .line 64
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "NULL AS tile_id"

    aput-object v1, v0, v3

    const-string v1, "NULL AS photo_id"

    aput-object v1, v0, v4

    const-string v1, "NULL AS image_url"

    aput-object v1, v0, v5

    const-string v1, "NULL AS owner_id"

    aput-object v1, v0, v6

    const-string v1, "NULL AS _data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->b:[Ljava/lang/String;

    .line 73
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "NULL AS tile_id"

    aput-object v1, v0, v3

    const-string v1, "NULL AS photo_id"

    aput-object v1, v0, v4

    const-string v1, "NULL AS image_url"

    aput-object v1, v0, v5

    const-string v1, "NULL AS owner_id"

    aput-object v1, v0, v6

    const-string v1, "_data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "NULL AS _id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->i:Landroid/os/Handler;

    .line 102
    new-instance v0, Lfwy;

    invoke-direct {v0, p0}, Lfwy;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->m:Ljava/lang/Runnable;

    .line 116
    new-instance v0, Lfwz;

    invoke-direct {v0, p0}, Lfwz;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->n:Ljava/lang/Runnable;

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->i:Landroid/os/Handler;

    .line 102
    new-instance v0, Lfwy;

    invoke-direct {v0, p0}, Lfwy;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->m:Ljava/lang/Runnable;

    .line 116
    new-instance v0, Lfwz;

    invoke-direct {v0, p0}, Lfwz;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->n:Ljava/lang/Runnable;

    .line 130
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->i:Landroid/os/Handler;

    .line 102
    new-instance v0, Lfwy;

    invoke-direct {v0, p0}, Lfwy;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->m:Ljava/lang/Runnable;

    .line 116
    new-instance v0, Lfwz;

    invoke-direct {v0, p0}, Lfwz;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->n:Ljava/lang/Runnable;

    .line 134
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->h:Landroid/database/Cursor;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 42
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/views/DreamViewFlipper;->c:[Ljava/lang/String;

    const-string v3, "bucket_id = ?"

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/provider/MediaStore$Images$Media;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 10

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Landroid/content/Context;)I

    move-result v2

    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v8, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v8, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    if-nez p2, :cond_1

    const-string v0, "SELECT count(*) FROM all_tiles WHERE view_id = ? AND media_attr & 512 == 0 AND type == 4 AND media_attr & 32 = 0 AND media_attr & 128 = 0 AND media_attr & 64 = 0"

    invoke-static {v9, v0, v8}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    new-instance v0, Ldip;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Ldip;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lkff;->l()V

    invoke-virtual {v0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    const-string v1, "all_tiles"

    sget-object v2, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a:[Ljava/lang/String;

    const-string v3, "view_id = ? AND media_attr & 512 == 0 AND type == 4 AND media_attr & 32 = 0 AND media_attr & 128 = 0 AND media_attr & 64 = 0"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, v9

    move-object v4, v8

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Lcom/google/android/libraries/social/media/ui/MediaView;)Lcom/google/android/libraries/social/media/ui/MediaView;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Landroid/database/Cursor;Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 9

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->j:Z

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v1, Lfxc;

    invoke-direct {v1, p0, p2}, Lfxc;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;Lcom/google/android/libraries/social/media/ui/MediaView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->o:Lfxc;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->o:Lfxc;

    new-array v2, v4, [Ljava/lang/Long;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lfxc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v1, v0, v2}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lizu;Lcom/google/android/libraries/social/media/ui/MediaView;)V

    goto :goto_0

    :cond_1
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v7, 0x0

    sget-object v8, Ljac;->a:Ljac;

    invoke-static/range {v1 .. v8}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lizu;Lcom/google/android/libraries/social/media/ui/MediaView;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Lizu;Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lizu;Lcom/google/android/libraries/social/media/ui/MediaView;)V

    return-void
.end method

.method private a(Lizu;Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 334
    invoke-virtual {p2, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 335
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 224
    new-instance v0, Lfxb;

    invoke-direct {v0, p0}, Lfxb;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 264
    invoke-virtual {v0, v1}, Lfxb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 265
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->l:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->k:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/DreamViewFlipper;Lcom/google/android/libraries/social/media/ui/MediaView;)Lcom/google/android/libraries/social/media/ui/MediaView;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    return-object p1
.end method

.method private b()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const-wide/16 v4, 0x3e8

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lfxa;

    invoke-direct {v1, p0}, Lfxa;-><init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    .line 167
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 187
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 188
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 189
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->j:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->b()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a()V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Lcom/google/android/libraries/social/media/ui/MediaView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Lcom/google/android/libraries/social/media/ui/MediaView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->h:Landroid/database/Cursor;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->m:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->i:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->n:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "all_photos"

    sget-object v2, Lcom/google/android/apps/plus/views/DreamViewFlipper;->b:[Ljava/lang/String;

    const-string v3, "is_primary = 1 AND media_attr & 32 = 0 AND media_attr & 128 = 0 AND media_attr & 64 = 0"

    const-string v7, "timestamp DESC"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->l:Z

    .line 152
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->k:Z

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->h:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->h:Landroid/database/Cursor;

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->o:Lfxc;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->o:Lfxc;

    invoke-virtual {v0, v2}, Lfxc;->cancel(Z)Z

    .line 162
    :cond_1
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 137
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->l:Z

    if-eqz v0, :cond_0

    .line 148
    :goto_0
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->l:Z

    .line 142
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->k:Z

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    .line 144
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Landroid/content/Context;)I

    move-result v1

    .line 143
    invoke-static {p1, v1}, Lcom/google/android/apps/plus/service/EsDreamService;->b(Landroid/content/Context;I)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f:Ljava/util/ArrayList;

    .line 145
    iput v2, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g:I

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-ne p1, v0, :cond_0

    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->j:Z

    .line 195
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->k:Z

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->k:Z

    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->b()V

    .line 200
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 204
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 205
    const v0, 0x7f1000f4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 206
    const v0, 0x7f10027c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Ljbb;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/plus/views/DreamViewFlipper;->e:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Ljbb;)V

    .line 211
    return-void
.end method
