.class public Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;
.super Lz;
.source "PG"


# instance fields
.field private e:Ljava/lang/Integer;

.field private f:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lz;-><init>()V

    .line 44
    new-instance v0, Lgcd;

    invoke-direct {v0, p0}, Lgcd;-><init>(Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->f:Lfhh;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 124
    if-eqz p1, :cond_1

    .line 125
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 126
    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p0, v0, v1}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 127
    new-instance v1, Ljuc;

    invoke-direct {v1, v0}, Ljuc;-><init>(Lizu;)V

    .line 128
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "image/jpg"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "is_internal"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 131
    const-string v1, "extra_acl"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    const-string v1, "extra_acl"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 133
    const-string v1, "extra_acl"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 136
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 141
    :goto_0
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->dismissDialog(I)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->finish()V

    .line 143
    return-void

    .line 138
    :cond_1
    const v0, 0x7f0a07be

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 101
    packed-switch p1, :pswitch_data_0

    .line 114
    :goto_0
    return-void

    .line 103
    :pswitch_0
    if-ne p2, v2, :cond_0

    .line 104
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->showDialog(I)V

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 106
    const-string v1, "account_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 109
    const-string v1, "camera-p.jpg"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->i(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 112
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->finish()V

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Lz;->onCreate(Landroid/os/Bundle;)V

    .line 59
    if-nez p1, :cond_1

    .line 60
    const-string v0, "camera-p.jpg"

    invoke-static {v0}, Leyq;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 61
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    const-string v0, "insert_camera_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "insert_camera_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    const v0, 0x7f0a003e

    if-ne p1, v0, :cond_0

    .line 148
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    const v1, 0x7f0a08fc

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 150
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lz;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lz;->onPause()V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->f:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 88
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lz;->onResume()V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->f:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    .line 78
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->b(Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    .line 82
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0, p1}, Lz;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "insert_camera_photo_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    :cond_0
    return-void
.end method
