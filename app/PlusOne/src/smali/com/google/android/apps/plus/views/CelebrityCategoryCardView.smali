.class public Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/LinearLayout;

.field private g:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 108
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/16 v3, 0x8

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v0, v3, v4

    const/4 v4, 0x1

    aput v0, v3, v4

    const/4 v4, 0x2

    aput v0, v3, v4

    const/4 v4, 0x3

    aput v0, v3, v4

    const/4 v0, 0x4

    aput v5, v3, v0

    const/4 v0, 0x5

    aput v5, v3, v0

    const/4 v0, 0x6

    aput v5, v3, v0

    const/4 v0, 0x7

    aput v5, v3, v0

    invoke-direct {v2, v3, v6, v6}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 114
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116
    return-void
.end method

.method public a(Lois;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    .line 73
    iget-object v0, p1, Lois;->d:[Loiu;

    array-length v0, v0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lois;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 78
    :goto_0
    if-ge v1, v3, :cond_2

    .line 79
    if-ge v1, v4, :cond_1

    .line 80
    iget-object v0, p1, Lois;->d:[Loiu;

    aget-object v6, v0, v1

    .line 83
    iget-object v0, v6, Loiu;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->b(Lohp;)Ljava/lang/String;

    move-result-object v7

    .line 84
    iget-object v0, v6, Loiu;->b:Lohv;

    iget-object v0, v0, Lohv;->c:Lohq;

    iget-object v8, v0, Lohq;->c:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 86
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 88
    invoke-static {v8}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 87
    invoke-virtual {v0, v7, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v7, v6, Loiu;->d:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;)V

    .line 92
    iget-object v0, v6, Loiu;->b:Lohv;

    iget-object v0, v0, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->a:Ljava/lang/String;

    .line 93
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    add-int/lit8 v0, v4, -0x1

    if-eq v1, v0, :cond_0

    .line 95
    const-string v0, ", "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 99
    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    goto :goto_1

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->e:Landroid/widget/TextView;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 55
    const v0, 0x7f1001e5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->b:Landroid/view/View;

    .line 56
    const v0, 0x7f1001e6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->c:Landroid/view/View;

    .line 57
    const v0, 0x7f1001e7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->d:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f1001ea

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->e:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f1001ec

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    .line 60
    const v0, 0x7f1001e8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->f:Landroid/widget/LinearLayout;

    .line 61
    const v0, 0x7f1001ed

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->g:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 65
    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->a:Landroid/graphics/drawable/Drawable;

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->g:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 69
    return-void
.end method
