.class public Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

.field private c:Landroid/view/View;

.field private d:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

.field private e:F

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-static {p1}, Llsc;->b(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->g:Z

    .line 39
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->g:Z

    if-eqz v0, :cond_0

    const v0, 0x3f666666    # 0.9f

    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->e:F

    .line 40
    return-void

    .line 39
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->f:I

    if-eq v0, p1, :cond_0

    .line 128
    iput p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->f:I

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->requestLayout()V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->invalidate()V

    .line 132
    :cond_0
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c:Landroid/view/View;

    return-object v0
.end method

.method public c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    return-object v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 45
    const v0, 0x7f1001eb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f10029a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f10029b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c:Landroid/view/View;

    .line 48
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 49
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->getMeasuredWidth()I

    move-result v1

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 92
    if-gez p3, :cond_0

    .line 93
    sub-int/2addr v0, p3

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredWidth()I

    move-result v2

    .line 97
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredHeight()I

    move-result v3

    .line 98
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    .line 100
    sub-int v5, v1, v2

    div-int/lit8 v5, v5, 0x2

    .line 101
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/4 v7, 0x0

    add-int/2addr v2, v5

    invoke-virtual {v6, v5, v7, v2, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->layout(IIII)V

    .line 103
    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    .line 104
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1

    .line 105
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    add-int/2addr v4, v1

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 108
    :cond_1
    return-void
.end method

.method public onMeasure(II)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 53
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->f:I

    const/high16 v1, 0x40000000    # 2.0f

    .line 54
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 55
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_2

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5, v0}, Landroid/widget/LinearLayout;->measure(II)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    move v1, v0

    .line 64
    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 65
    iget v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->e:F

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v4, v0

    .line 66
    sub-int v3, v4, v1

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 69
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 70
    if-gez v0, :cond_1

    .line 71
    sub-int v0, v3, v0

    .line 75
    :goto_1
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 76
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/high16 v3, -0x80000000

    .line 77
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 76
    invoke-virtual {v2, v5, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->measure(II)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 80
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->g:Z

    if-eqz v2, :cond_0

    .line 81
    add-int/2addr v0, v1

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 85
    :goto_2
    iget v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->f:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->setMeasuredDimension(II)V

    .line 86
    return-void

    :cond_0
    move v0, v4

    .line 83
    goto :goto_2

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method
