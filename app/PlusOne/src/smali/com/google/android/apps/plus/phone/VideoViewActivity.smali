.class public Lcom/google/android/apps/plus/phone/VideoViewActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lloa;-><init>()V

    .line 31
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/VideoViewActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 32
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/VideoViewActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/VideoViewActivity;->x:Llnh;

    .line 33
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 34
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 37
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/VideoViewActivity;->y:Llqc;

    const v1, 0x7f100143

    invoke-direct {v0, p0, v1}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/VideoViewActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lhmw;->ak:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/VideoViewActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 62
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 63
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 65
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 42
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Lera;

    invoke-direct {v0}, Lera;-><init>()V

    .line 46
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/VideoViewActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 49
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-gt v0, v1, :cond_1

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/VideoViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 55
    :goto_0
    const v0, 0x7f04022f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/VideoViewActivity;->setContentView(I)V

    .line 56
    return-void

    .line 53
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0}, Llii;->a(Landroid/app/Activity;Z)V

    goto :goto_0
.end method
