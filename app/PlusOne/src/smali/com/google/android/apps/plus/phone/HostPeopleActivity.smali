.class public Lcom/google/android/apps/plus/phone/HostPeopleActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;

.field private f:Lefj;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 38
    invoke-direct {p0}, Lloa;-><init>()V

    .line 43
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 45
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->x:Llnh;

    .line 46
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 48
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->x:Llnh;

    .line 49
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 52
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 53
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 55
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->x:Llnh;

    .line 56
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x1

    .line 57
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 60
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->f:Lefj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->f:Lefj;

    invoke-virtual {v0}, Lefj;->F_()Lhmw;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhmw;->a:Lhmw;

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 67
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 70
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ljpr;

    new-instance v2, Ljqy;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Ljqy;-><init>(Landroid/content/Context;Llqr;)V

    .line 71
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldvv;

    new-instance v2, Ldvv;

    invoke-direct {v2, p0}, Ldvv;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 73
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 98
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 99
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 100
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 101
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 90
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 79
    if-nez p1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "people_clear_cache"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 81
    new-instance v1, Lefj;

    invoke-direct {v1, v0}, Lefj;-><init>(Z)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->f:Lefj;

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->e:Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->f:Lefj;

    invoke-virtual {v0, v1}, Ldie;->a(Lu;)V

    .line 84
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->setContentView(I)V

    .line 85
    return-void
.end method
