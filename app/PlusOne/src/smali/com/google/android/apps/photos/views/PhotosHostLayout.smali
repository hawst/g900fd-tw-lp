.class public Lcom/google/android/apps/photos/views/PhotosHostLayout;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lhsw;
.implements Lmy;


# static fields
.field private static g:I

.field private static h:I

.field private static i:I


# instance fields
.field private a:Lmy;

.field private b:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

.field private c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

.field private d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

.field private e:Lae;

.field private f:Ldhq;

.field private final j:Lhsu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 66
    sput v0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->g:I

    .line 67
    sput v0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->h:I

    .line 68
    sput v0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lz;

    .line 88
    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    .line 89
    const-class v1, Lhsu;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsu;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->j:Lhsu;

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lz;

    .line 88
    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    .line 89
    const-class v1, Lhsu;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsu;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->j:Lhsu;

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lz;

    .line 88
    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    .line 89
    const-class v1, Lhsu;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsu;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->j:Lhsu;

    .line 84
    return-void
.end method


# virtual methods
.method public V()Z
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f()V

    .line 321
    const/4 v0, 0x1

    .line 323
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->b:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/photos/views/NavigationBarLayout;->a(I)V

    .line 171
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 271
    if-eqz p1, :cond_2

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a:Lmy;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a:Lmy;

    iget-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    invoke-interface {v0, v1}, Lmy;->a(Landroid/view/View;)V

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f:Ldhq;

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f:Ldhq;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ldhq;->b(Z)V

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->j:Lhsu;

    if-eqz v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->j:Lhsu;

    invoke-virtual {v0, p0}, Lhsu;->a(Lhsw;)Lhsu;

    .line 282
    :cond_2
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a:Lmy;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a:Lmy;

    invoke-interface {v0, p1, p2}, Lmy;->a(Landroid/view/View;F)V

    .line 264
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f:Ldhq;

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f:Ldhq;

    .line 267
    :cond_1
    return-void
.end method

.method public a(Ldhq;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f:Ldhq;

    .line 110
    return-void
.end method

.method public a(Lmy;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a:Lmy;

    .line 94
    return-void
.end method

.method public a(Lu;Lx;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->i()Lu;

    move-result-object v1

    .line 189
    instance-of v0, p1, Lhmq;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lhmq;

    .line 190
    invoke-interface {v0}, Lhmq;->F_()Lhmw;

    move-result-object v3

    .line 192
    :goto_0
    if-eqz v1, :cond_5

    .line 193
    if-eqz p3, :cond_3

    sget-object v0, Lhmw;->O:Lhmw;

    :goto_1
    move-object v1, v0

    .line 202
    :goto_2
    if-eqz p2, :cond_0

    .line 203
    invoke-virtual {p1, p2}, Lu;->a(Lx;)V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    .line 206
    const v2, 0x7f100143

    const-string v4, "hosted"

    invoke-virtual {v0, v2, p1, v4}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    .line 207
    if-eqz p3, :cond_6

    .line 208
    const/16 v2, 0x1003

    invoke-virtual {v0, v2}, Lat;->a(I)Lat;

    .line 212
    :goto_3
    invoke-virtual {v0}, Lat;->c()I

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    invoke-virtual {v0}, Lae;->b()Z

    .line 215
    if-eqz v3, :cond_1

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 217
    const-class v0, Lhms;

    invoke-static {v2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    invoke-direct {v4, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 219
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 220
    invoke-virtual {v1, v3}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v1

    .line 217
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 223
    :cond_1
    return-void

    :cond_2
    move-object v3, v2

    .line 190
    goto :goto_0

    .line 193
    :cond_3
    instance-of v0, v1, Lhmq;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Lhmq;

    .line 196
    invoke-interface {v0}, Lhmq;->F_()Lhmw;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_1

    :cond_5
    move-object v1, v3

    .line 199
    goto :goto_2

    .line 210
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lat;->a(I)Lat;

    goto :goto_3
.end method

.method public b()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/NavigationBarLayout;->a()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(I)V

    .line 287
    if-eqz p1, :cond_2

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a:Lmy;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a:Lmy;

    iget-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    invoke-interface {v0, v1}, Lmy;->b(Landroid/view/View;)V

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f:Ldhq;

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f:Ldhq;

    invoke-interface {v0, v2}, Ldhq;->b(Z)V

    .line 298
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->j:Lhsu;

    if-eqz v0, :cond_2

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->j:Lhsu;

    invoke-virtual {v0, p0}, Lhsu;->b(Lhsw;)Lhsu;

    .line 302
    :cond_2
    return-void
.end method

.method public c()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/NavigationBarLayout;->b()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    iget-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->j(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-static {p0}, Llsn;->b(Landroid/view/View;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/views/NavigationBarLayout;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    iget-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->h(Landroid/view/View;)V

    goto :goto_0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    iget-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->i(Landroid/view/View;)V

    .line 167
    :cond_0
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/NavigationBarLayout;->c()I

    move-result v0

    return v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    const-string v1, "hosted"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_0

    .line 228
    iget-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    .line 229
    invoke-virtual {v1, v0}, Lat;->a(Lu;)Lat;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Lat;->b()I

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    invoke-virtual {v0}, Lae;->b()Z

    .line 233
    :cond_0
    return-void
.end method

.method public i()Lu;
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    const-string v1, "hosted"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    return-object v0
.end method

.method public j()Lx;
    .locals 2

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->i()Lu;

    move-result-object v0

    .line 244
    if-eqz v0, :cond_0

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e:Lae;

    invoke-virtual {v1, v0}, Lae;->a(Lu;)Lx;

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()V
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(ILandroid/view/View;)V

    .line 256
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 116
    const v0, 0x7f10055a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->b:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    .line 117
    const v0, 0x7f1004cb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/views/NavigationBarLayout;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    .line 119
    const v0, 0x7f1002fb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d:Lcom/google/android/apps/plus/views/EsDrawerLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(Lmy;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 123
    sget v1, Lcom/google/android/apps/photos/views/PhotosHostLayout;->g:I

    if-gez v1, :cond_0

    .line 124
    const v1, 0x7f0d02ec

    .line 125
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/views/PhotosHostLayout;->g:I

    .line 126
    const v1, 0x7f0d02ed

    .line 127
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/views/PhotosHostLayout;->h:I

    .line 128
    const v1, 0x7f0c0072

    .line 129
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->i:I

    .line 131
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 135
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 136
    sget v1, Lcom/google/android/apps/photos/views/PhotosHostLayout;->i:I

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    sget v1, Lcom/google/android/apps/photos/views/PhotosHostLayout;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget v1, Lcom/google/android/apps/photos/views/PhotosHostLayout;->h:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c:Lcom/google/android/apps/photos/views/NavigationBarLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/photos/views/NavigationBarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 140
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 141
    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 314
    invoke-static {p1}, Llii;->f(Landroid/view/View;)V

    .line 315
    return-void
.end method
