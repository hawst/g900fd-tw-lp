.class public Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;
.super Llon;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Lheg;
.implements Lhob;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llon;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lheg;",
        "Lhob;",
        "Llgs;"
    }
.end annotation


# static fields
.field private static o:[Ljava/lang/String;


# instance fields
.field private g:Landroid/widget/ListView;

.field private h:Lgcl;

.field private final i:Ljava/lang/Object;

.field private j:I

.field private k:Livx;

.field private l:[Lnhm;

.field private m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lhoc;

.field private p:Landroid/view/ContextThemeWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "avatar_url"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "display_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->o:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Llon;-><init>()V

    .line 85
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->i:Ljava/lang/Object;

    .line 88
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    .line 89
    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    .line 94
    new-instance v0, Lhoc;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Landroid/app/Activity;Llqr;)V

    .line 95
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->n:Lhoc;

    .line 127
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;)Livx;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->m:Ljava/util/HashMap;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k()V

    return-void
.end method

.method private i()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 287
    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->o:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    array-length v3, v3

    invoke-direct {v2, v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 290
    iget-object v3, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 291
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v5, Lnhm;->b:Ljava/lang/String;

    aput-object v7, v6, v1

    iget-object v7, v5, Lnhm;->e:Ljava/lang/String;

    aput-object v7, v6, v8

    const/4 v7, 0x2

    iget-object v5, v5, Lnhm;->d:Ljava/lang/String;

    aput-object v5, v6, v7

    .line 296
    invoke-virtual {v2, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    invoke-virtual {v0, v8, v2}, Lgcl;->a(ILandroid/database/Cursor;)V

    .line 299
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->m:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 347
    const-string v0, "0"

    .line 362
    :goto_0
    return-object v0

    .line 350
    :cond_0
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 351
    const-string v0, "circle_id IN ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 353
    invoke-static {v1, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 354
    const/16 v0, 0x2c

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 357
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 358
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 361
    :cond_2
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 362
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private k()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 366
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h()V

    .line 369
    iget-object v6, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->n:Lhoc;

    new-instance v0, Ldoy;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    invoke-virtual {v1}, Livx;->d()I

    move-result v2

    const/4 v3, 0x0

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Ldoy;-><init>(Landroid/content/Context;ILjava/lang/String;IZ)V

    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    .line 371
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 418
    packed-switch p1, :pswitch_data_0

    .line 435
    :cond_0
    :goto_0
    return-object v0

    .line 420
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    invoke-virtual {v1}, Livx;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    if-eqz v1, :cond_0

    .line 423
    new-instance v0, Lesv;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    invoke-direct {v0, p0, v1, v2}, Lesv;-><init>(Landroid/content/Context;I[Lnhm;)V

    goto :goto_0

    .line 427
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    invoke-virtual {v1}, Livx;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 430
    new-instance v0, Lhxg;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    invoke-virtual {v1}, Livx;->d()I

    move-result v2

    const/16 v3, 0x11

    sget-object v4, Lgcm;->a:[Ljava/lang/String;

    .line 432
    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->j()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 566
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 571
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 225
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->e:Llnh;

    const-class v1, Lhoc;

    iget-object v2, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->n:Lhoc;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 227
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 547
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 548
    const-string v1, "account_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 549
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->startActivity(Landroid/content/Intent;)V

    .line 550
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 486
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 441
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 443
    :pswitch_0
    if-nez p2, :cond_1

    .line 444
    const v0, 0x7f0a0592

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 445
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 448
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->m:Ljava/util/HashMap;

    .line 450
    const/4 v0, -0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 451
    :cond_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 453
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 454
    invoke-static {v0}, Ldsm;->d(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 456
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_2

    .line 457
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->m:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 459
    if-nez v1, :cond_3

    .line 460
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 464
    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->m:Ljava/util/HashMap;

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 462
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_2

    .line 469
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->g()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 474
    :pswitch_1
    if-eqz p2, :cond_0

    .line 475
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    invoke-virtual {v0, v3, p2}, Lgcl;->a(ILandroid/database/Cursor;)V

    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h()V

    goto :goto_0

    .line 441
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x1020004

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 377
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    const v0, 0x7f100351

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 379
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    if-nez p2, :cond_2

    .line 385
    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k()V

    goto :goto_0

    .line 390
    :cond_2
    invoke-static {p2}, Ldoy;->a(Lhoz;)[Lnhm;

    move-result-object v0

    .line 391
    if-eqz v0, :cond_3

    array-length v1, v0

    if-lez v1, :cond_3

    .line 392
    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    .line 393
    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->i()V

    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->g()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2, v5, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 397
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->f()Lae;

    move-result-object v0

    const-string v1, "lwca_no_shares"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a082e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a082f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v0, v1, v2}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->f()Lae;

    move-result-object v1

    const-string v2, "lwca_no_shares"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 270
    const/4 v0, -0x1

    if-eq p5, v0, :cond_2

    .line 271
    if-eqz p1, :cond_1

    .line 272
    const v0, 0x7f0400fd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->setContentView(I)V

    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->g:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h()V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f100353

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lgck;

    invoke-direct {v1, p0}, Lgck;-><init>(Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k()V

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->i()V

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->g()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 279
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 280
    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 281
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    .line 282
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->finish()V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->finish()V

    .line 556
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 560
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->finish()V

    .line 561
    return-void
.end method

.method protected h()V
    .locals 5

    .prologue
    const v4, 0x1020004

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    invoke-virtual {v0}, Lgcl;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 535
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 543
    :goto_0
    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->g:Landroid/widget/ListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 538
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 539
    const v0, 0x7f10025f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 540
    const v0, 0x7f100260

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 541
    const v0, 0x7f100351

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_0

    .line 236
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->j:I

    .line 242
    :goto_0
    iget v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->j:I

    if-nez v0, :cond_1

    .line 243
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->finish()V

    .line 257
    :goto_1
    return-void

    .line 239
    :cond_0
    iput v2, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->j:I

    goto :goto_0

    .line 246
    :cond_1
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0901c9

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->p:Landroid/view/ContextThemeWrapper;

    .line 248
    new-instance v0, Lgcl;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->p:Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p0, v1}, Lgcl;-><init>(Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    .line 250
    if-eqz p1, :cond_2

    .line 251
    const-string v0, "user_locations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyv;

    .line 252
    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 253
    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Lixj;

    invoke-virtual {v1, v2}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v1

    const-class v2, Liwl;

    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    goto :goto_1

    .line 252
    :cond_3
    new-array v1, v2, [Lnhm;

    .line 253
    invoke-virtual {v0, v1}, Lhyv;->a([Loxu;)[Loxu;

    move-result-object v0

    check-cast v0, [Lnhm;

    goto :goto_2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    invoke-virtual {v0, p3}, Lgcl;->m(I)I

    move-result v0

    .line 495
    iget-object v3, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    invoke-virtual {v3, p3}, Lgcl;->n(I)I

    move-result v3

    .line 496
    iget-object v6, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->i:Ljava/lang/Object;

    monitor-enter v6

    .line 497
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    invoke-virtual {v5, v0}, Lgcl;->l(I)Landroid/database/Cursor;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 498
    iget-object v5, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->h:Lgcl;

    invoke-virtual {v5, v0}, Lgcl;->l(I)Landroid/database/Cursor;

    move-result-object v5

    .line 499
    invoke-interface {v5}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-gt v7, v3, :cond_1

    .line 500
    :cond_0
    monitor-exit v6

    .line 527
    :goto_0
    return-void

    .line 502
    :cond_1
    invoke-interface {v5, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 503
    if-nez v0, :cond_3

    .line 504
    const/4 v0, 0x1

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->m:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v0, v1, :cond_2

    move v0, v1

    :goto_1
    move v5, v0

    .line 515
    :goto_2
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    invoke-virtual {v0}, Livx;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 518
    iget v1, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->j:I

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lgcq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 520
    iget v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->j:I

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetProvider;->a(Landroid/content/Context;I)V

    .line 523
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 524
    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 525
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    .line 526
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->finish()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 506
    goto :goto_1

    .line 508
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v5, v2

    move-object v3, v4

    move-object v4, v0

    .line 512
    goto :goto_2

    .line 513
    :cond_4
    monitor-exit v6

    goto :goto_0

    .line 515
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 303
    invoke-super {p0}, Llon;->onResume()V

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k:Livx;

    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->n:Lhoc;

    const-string v1, "GetFriendLocationsTask"

    .line 305
    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->k()V

    .line 309
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 313
    invoke-super {p0, p1}, Llon;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    if-eqz v0, :cond_0

    .line 315
    const-string v0, "user_locations"

    new-instance v1, Lhyv;

    iget-object v2, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->l:[Lnhm;

    invoke-direct {v1, v2}, Lhyv;-><init>([Loxu;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 317
    :cond_0
    return-void
.end method
