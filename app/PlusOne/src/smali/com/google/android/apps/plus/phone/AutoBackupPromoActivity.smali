.class public Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lheg;


# instance fields
.field private final g:Livx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Llon;-><init>()V

    .line 27
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;->e:Llnh;

    .line 28
    invoke-virtual {v0, v1}, Livx;->a(Llnh;)Livx;

    move-result-object v0

    .line 29
    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;->g:Livx;

    .line 27
    return-void
.end method


# virtual methods
.method public a(ZIIII)V
    .locals 3

    .prologue
    .line 54
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;->g:Livx;

    invoke-virtual {v0}, Livx;->d()I

    move-result v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account_id"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v0, Ldxz;

    invoke-direct {v0}, Ldxz;-><init>()V

    invoke-virtual {v0, v1}, Lu;->f(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;->f()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2, v0}, Lat;->b(ILu;)Lat;

    invoke-virtual {v1}, Lat;->b()I

    .line 60
    :goto_0
    return-void

    .line 57
    :cond_0
    const-string v0, "AutoBackupPromo"

    const-string v1, "No account provided."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 33
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;->requestWindowFeature(I)Z

    .line 36
    if-nez p1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;->g:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-string v2, "active-photos-account"

    invoke-virtual {v1, v2}, Liwg;->a(Ljava/lang/String;)Liwg;

    move-result-object v1

    invoke-virtual {v1}, Liwg;->a()Liwg;

    move-result-object v1

    invoke-virtual {v1}, Liwg;->d()Liwg;

    move-result-object v1

    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 39
    :cond_0
    return-void
.end method
