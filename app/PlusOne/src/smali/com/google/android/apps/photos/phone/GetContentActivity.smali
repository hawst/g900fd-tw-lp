.class public Lcom/google/android/apps/photos/phone/GetContentActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lheg;
.implements Lhjj;
.implements Lhmq;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final e:Livx;

.field private final f:Ldie;

.field private g:Lhjf;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Lloa;-><init>()V

    .line 63
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 64
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->x:Llnh;

    .line 65
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 68
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->x:Llnh;

    .line 69
    invoke-virtual {v0, v1}, Livx;->a(Llnh;)Livx;

    move-result-object v0

    const-string v1, "active-photos-account"

    .line 70
    invoke-virtual {v0, v1}, Livx;->a(Ljava/lang/String;)Livx;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->e:Livx;

    .line 73
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    const v1, 0x7f100143

    invoke-direct {v0, p0, v1}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->f:Ldie;

    .line 76
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    .line 78
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->g:Lhjf;

    .line 76
    return-void
.end method

.method private static b(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 286
    if-nez p0, :cond_1

    move v0, v1

    .line 305
    :cond_0
    :goto_0
    return v0

    .line 290
    :cond_1
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 292
    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v0, v1

    :goto_1
    if-ltz v2, :cond_4

    .line 293
    aget-object v4, v3, v2

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 294
    const-string v5, "image/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 295
    or-int/lit8 v0, v0, 0x1

    .line 292
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 296
    :cond_3
    const-string v5, "video/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 297
    or-int/lit8 v0, v0, 0x2

    goto :goto_2

    .line 301
    :cond_4
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_0

    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_0

    move v0, v1

    .line 303
    goto :goto_0
.end method

.method private l()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 102
    if-eqz v1, :cond_0

    const-string v2, "is_for_movie_maker_launch"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private m()I
    .locals 7

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 108
    if-nez v3, :cond_0

    .line 153
    :goto_0
    return v2

    .line 112
    :cond_0
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 113
    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v5

    .line 114
    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 116
    const-string v6, "android.intent.action.PICK"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 118
    if-eqz v5, :cond_3

    const-string v4, "vnd.android.cursor.dir/"

    invoke-virtual {v5, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 127
    const-string v3, "/image"

    invoke-virtual {v5, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    :goto_1
    move v2, v0

    .line 153
    goto :goto_0

    .line 129
    :cond_2
    const-string v0, "/video"

    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    .line 130
    goto :goto_1

    .line 132
    :cond_3
    if-eqz v3, :cond_5

    .line 133
    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 134
    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 135
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/image"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 137
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 138
    invoke-virtual {v3, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 139
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "/video"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_4
    move v0, v1

    .line 140
    goto :goto_1

    .line 143
    :cond_5
    invoke-static {v5}, Lcom/google/android/apps/photos/phone/GetContentActivity;->b(Ljava/lang/String;)I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 145
    :cond_6
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 146
    invoke-static {v5}, Lcom/google/android/apps/photos/phone/GetContentActivity;->b(Ljava/lang/String;)I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 147
    :cond_7
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 148
    invoke-static {v5}, Lcom/google/android/apps/photos/phone/GetContentActivity;->b(Ljava/lang/String;)I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 149
    :cond_8
    if-eqz v5, :cond_9

    .line 150
    invoke-static {v5}, Lcom/google/android/apps/photos/phone/GetContentActivity;->b(Ljava/lang/String;)I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_1
.end method

.method private n()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_3

    .line 165
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 166
    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    .line 169
    :goto_0
    const-string v3, "android.intent.action.PICK"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.SEND"

    .line 170
    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    .line 171
    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    .line 172
    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object v1, v2

    .line 177
    :cond_1
    :goto_1
    return-object v1

    .line 174
    :cond_2
    if-eqz v0, :cond_1

    .line 175
    const-string v1, "android.intent.action.GET_CONTENT"

    goto :goto_1

    :cond_3
    move-object v0, v1

    move-object v2, v1

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 282
    sget-object v0, Lhmw;->h:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 82
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 84
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 85
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Lhjf;

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->g:Lhjf;

    .line 86
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 87
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 89
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsm;

    new-instance v2, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    .line 90
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 94
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->x:Llnh;

    const-class v1, Lcnq;

    new-instance v2, Lcnr;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->y:Llqc;

    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->n()Ljava/lang/String;

    move-result-object v4

    .line 96
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->m()I

    move-result v5

    invoke-direct {v2, p0, v3, v4, v5}, Lcnr;-><init>(Landroid/app/Activity;Llqr;Ljava/lang/String;I)V

    .line 95
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 98
    :cond_0
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 331
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 333
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 334
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 335
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->b(Z)V

    .line 323
    return-void
.end method

.method public a(ZIIII)V
    .locals 13

    .prologue
    .line 357
    if-eqz p1, :cond_1

    .line 358
    iget-object v7, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->f:Ldie;

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->n()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->m()I

    move-result v3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->l()Z

    move-result v10

    if-eqz v9, :cond_c

    const-string v1, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x0

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    or-int v4, v0, v1

    const-string v0, "android.intent.extra.ALLOW_MULTIPLE"

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v0, "exclude_tab_auto_awesome"

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "filter"

    invoke-virtual {v9, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    move v3, v2

    move v2, v1

    move v1, v0

    :goto_2
    const-class v0, Lcnt;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    and-int/lit8 v6, v2, 0x2

    if-eqz v6, :cond_6

    const-string v1, "PhotoSearch"

    const/4 v6, 0x7

    invoke-virtual {v0, v1, v6}, Lcnt;->a(Ljava/lang/String;I)V

    new-instance v1, Legc;

    invoke-direct {v1}, Legc;-><init>()V

    if-nez v4, :cond_4

    const-string v0, "query"

    const-string v4, "#videos"

    invoke-virtual {v8, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    const-string v0, "hide_search_view"

    const/4 v4, 0x1

    invoke-virtual {v8, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "search_local_videos"

    and-int/lit8 v0, v2, 0x4

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {v8, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    :goto_5
    or-int/lit8 v1, v2, 0x10

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->e:Livx;

    invoke-virtual {v2}, Livx;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "account_id"

    iget-object v4, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->e:Livx;

    invoke-virtual {v4}, Livx;->d()I

    move-result v4

    invoke-virtual {v8, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    if-eqz v3, :cond_a

    const-string v2, "photo_picker_mode"

    const/4 v3, 0x2

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_6
    const-string v2, "external"

    const/4 v3, 0x1

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "is_for_get_content"

    const/4 v3, 0x1

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "is_for_movie_maker_launch"

    invoke-virtual {v8, v2, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "finish_on_back"

    const/4 v3, 0x1

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "filter"

    invoke-virtual {v8, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "get_content_action"

    invoke-virtual {v8, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "disable_up_button"

    if-nez v10, :cond_b

    const/4 v1, 0x1

    :goto_7
    invoke-virtual {v8, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v8}, Lu;->f(Landroid/os/Bundle;)V

    invoke-virtual {v7, v0}, Ldie;->a(Lu;)V

    .line 360
    :cond_1
    return-void

    .line 358
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_4
    const-string v0, "local_folders_only"

    const/4 v4, 0x1

    invoke-virtual {v8, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    const-string v6, "Photos"

    const/4 v11, 0x1

    invoke-virtual {v0, v6, v11}, Lcnt;->a(Ljava/lang/String;I)V

    new-instance v6, Legr;

    invoke-direct {v6}, Legr;-><init>()V

    const/4 v0, 0x2

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->e:Livx;

    invoke-virtual {v1}, Livx;->f()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v0, 0x12

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->e:Livx;

    invoke-virtual {v1}, Livx;->f()Z

    move-result v1

    if-eqz v1, :cond_8

    if-nez v4, :cond_8

    or-int/lit8 v0, v0, 0x4

    and-int/lit8 v1, v2, 0x1

    if-nez v1, :cond_8

    or-int/lit8 v0, v0, 0x8

    :cond_8
    const-string v1, "tabs"

    invoke-virtual {v8, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "source_id"

    const/4 v1, -0x1

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_8
    if-eqz v0, :cond_9

    const-string v1, "starting_tab_index"

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v8, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_9
    move-object v0, v6

    goto/16 :goto_5

    :pswitch_0
    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_8

    :pswitch_1
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_8

    :cond_a
    const-string v2, "photo_picker_mode"

    const/4 v3, 0x1

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_6

    :cond_b
    const/4 v1, 0x0

    goto :goto_7

    :cond_c
    move-object v5, v4

    move v4, v0

    move v12, v1

    move v1, v2

    move v2, v3

    move v3, v12

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 344
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 327
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 311
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 313
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/GetContentActivity;->setContentView(I)V

    .line 315
    if-nez p1, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/GetContentActivity;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    invoke-virtual {v1}, Liwg;->a()Liwg;

    move-result-object v1

    invoke-virtual {v1}, Liwg;->d()Liwg;

    move-result-object v1

    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 318
    :cond_0
    return-void
.end method
