.class public Lcom/google/android/apps/plus/views/PeopleListRowView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private a:Z

.field private b:Landroid/view/View;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Lfuw;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Ljava/lang/Object;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private n:Landroid/widget/ImageView;

.field private o:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

.field private p:Z

.field private q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

.field private r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

.field private s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

.field private t:Z

.field private u:Lhxh;

.field private v:Lfze;

.field private w:Landroid/os/Bundle;

.field private x:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 135
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    .line 113
    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->i:Ljava/lang/Object;

    .line 116
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->l:Z

    .line 120
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->p:Z

    .line 127
    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->w:Landroid/os/Bundle;

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020416

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->c:Landroid/graphics/drawable/Drawable;

    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 139
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    .line 113
    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->i:Ljava/lang/Object;

    .line 116
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->l:Z

    .line 120
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->p:Z

    .line 127
    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->w:Landroid/os/Bundle;

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020416

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->c:Landroid/graphics/drawable/Drawable;

    .line 140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 143
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    .line 113
    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->i:Ljava/lang/Object;

    .line 116
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->l:Z

    .line 120
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->p:Z

    .line 127
    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->w:Landroid/os/Bundle;

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020416

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->c:Landroid/graphics/drawable/Drawable;

    .line 144
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->w:Landroid/os/Bundle;

    return-object v0
.end method

.method public a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 196
    iput p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    .line 197
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->i:Ljava/lang/Object;

    .line 198
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    if-eqz p1, :cond_1

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 209
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 211
    :cond_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;FJZ)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi",
            "NewApi"
        }
    .end annotation

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->d:Lfuw;

    if-eqz v0, :cond_0

    .line 535
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->d:Lfuw;

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lfuw;->a(Landroid/view/View;FJZ)V

    .line 537
    :cond_0
    return-void
.end method

.method public a(Lfze;Lhxh;Z)V
    .locals 1

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    .line 156
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->u:Lhxh;

    .line 157
    iput-boolean p3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->j:Z

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->a:Z

    .line 161
    return-void
.end method

.method public a(Ljava/lang/Object;Lfuw;)V
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/Object;Ljava/lang/String;Lfuw;)V

    .line 244
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;Lfuw;)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 247
    instance-of v0, p1, Lnrp;

    if-eqz v0, :cond_0

    .line 248
    check-cast p1, Lnrp;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lnrp;Ljava/lang/String;Lfuw;)V

    .line 256
    :goto_0
    return-void

    .line 249
    :cond_0
    instance-of v0, p1, Loiu;

    if-eqz v0, :cond_1

    .line 250
    check-cast p1, Loiu;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Loiu;Ljava/lang/String;Lfuw;)V

    goto :goto_0

    .line 251
    :cond_1
    instance-of v0, p1, Lohv;

    if-eqz v0, :cond_2

    .line 252
    check-cast p1, Lohv;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lohv;Ljava/lang/String;Lfuw;)V

    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    move-object v8, v1

    move v9, v6

    move v10, v6

    move-object v11, p3

    .line 254
    invoke-virtual/range {v0 .. v11}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZLfuw;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/String;Ljava/lang/String;ZZLandroid/os/Bundle;Lfuw;)V
    .locals 8

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->e:Ljava/lang/String;

    .line 377
    if-nez p1, :cond_3

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->o:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->setVisibility(I)V

    .line 380
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 381
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 382
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->b:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 383
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->b:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 385
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->x:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 386
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->x:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 388
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 389
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 530
    :cond_2
    :goto_0
    return-void

    .line 394
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->p:Z

    if-nez v1, :cond_9

    .line 395
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 396
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 449
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->b:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 451
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->b:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->a:Z

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 452
    invoke-virtual {v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getVisibility()I

    move-result v1

    .line 451
    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 455
    :cond_4
    if-eqz p3, :cond_10

    .line 456
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 457
    invoke-static {p3}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 456
    invoke-virtual {v1, p2, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 459
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    .line 460
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 479
    :cond_5
    :goto_3
    iput-object p4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->f:Ljava/lang/String;

    .line 482
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->f:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 483
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->f:Ljava/lang/String;

    .line 484
    const-string v1, "g:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->k:Z

    .line 494
    :goto_4
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->k:Z

    if-nez v1, :cond_18

    .line 495
    invoke-static {v2, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 496
    const/4 p5, 0x0

    .line 497
    const/4 p6, 0x0

    .line 498
    const/4 p7, 0x0

    move v6, p7

    move-object v5, p6

    .line 502
    :goto_5
    const-string v1, "f:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 503
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->o:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v7, p10

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Ljava/lang/String;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    .line 505
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->x:Landroid/widget/TextView;

    if-eqz v1, :cond_6

    .line 506
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->x:Landroid/widget/TextView;

    invoke-virtual {v1, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 507
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->x:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 514
    :cond_6
    :goto_6
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->o:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->setVisibility(I)V

    .line 516
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->g:Ljava/lang/String;

    .line 517
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->w:Landroid/os/Bundle;

    .line 519
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->a:Z

    if-eqz v1, :cond_7

    .line 520
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->d:Lfuw;

    .line 523
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->d:Lfuw;

    if-eqz v1, :cond_8

    .line 524
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->d:Lfuw;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    invoke-virtual {v1, v2}, Lfuw;->e(Landroid/view/View;)V

    .line 525
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->d:Lfuw;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;->a(Lfuw;)V

    .line 528
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    invoke-static {v1}, Lfvi;->g(Landroid/view/View;)V

    .line 529
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setVisibility(I)V

    goto/16 :goto_0

    .line 399
    :cond_9
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 400
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 402
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->x:Landroid/widget/TextView;

    if-eqz v2, :cond_a

    .line 403
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->x:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 406
    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 407
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 408
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->j:Z

    if-eqz v3, :cond_b

    const v3, 0x7f0a0519

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_7
    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 413
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lhxb;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhxb;

    .line 414
    invoke-interface {v1, p1}, Lhxb;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 415
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 416
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 417
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->b(Z)V

    goto/16 :goto_1

    .line 408
    :cond_b
    const v3, 0x7f0a0821

    .line 409
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    .line 419
    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    new-instance v2, Lhmj;

    invoke-direct {v2, p0}, Lhmj;-><init>(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 421
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    new-instance v2, Lhmk;

    sget-object v3, Lonl;->d:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v1, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 423
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->b(Z)V

    goto/16 :goto_1

    .line 426
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 427
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 428
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v1, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 429
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v1, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 431
    const-string v1, "15"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 432
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 433
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->t:Z

    .line 435
    const/4 p5, 0x0

    .line 436
    const/4 p6, 0x0

    .line 437
    const/4 p3, 0x0

    goto/16 :goto_1

    .line 439
    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 440
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->t:Z

    .line 442
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->u:Lhxh;

    .line 443
    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Lhxh;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 444
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/util/List;)V

    goto/16 :goto_1

    .line 452
    :cond_f
    const/16 v1, 0x8

    goto/16 :goto_2

    .line 462
    :cond_10
    const-string v1, "e:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 463
    const v1, 0x7f0201bc

    .line 464
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a087a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 463
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 465
    :cond_11
    const-string v1, "p:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 466
    const/4 v1, 0x0

    .line 467
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a087b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 466
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 468
    :cond_12
    const-string v1, "f:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 469
    const v1, 0x7f020183

    .line 470
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a087c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 469
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 472
    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 474
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    .line 475
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 485
    :cond_14
    const-string v1, "e:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_15

    const-string v1, "p:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 486
    :cond_15
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 487
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->k:Z

    goto/16 :goto_4

    .line 490
    :cond_16
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->k:Z

    move-object v2, p1

    goto/16 :goto_4

    .line 510
    :cond_17
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->o:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    move/from16 v3, p11

    move-object v4, p5

    move/from16 v7, p10

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Ljava/lang/String;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    goto/16 :goto_6

    :cond_18
    move v6, p7

    move-object v5, p6

    goto/16 :goto_5
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZLfuw;)V
    .locals 14

    .prologue
    .line 367
    const/4 v6, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move-object/from16 v13, p11

    invoke-virtual/range {v0 .. v13}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/String;Ljava/lang/String;ZZLandroid/os/Bundle;Lfuw;)V

    .line 370
    return-void
.end method

.method public a(Lnrp;Ljava/lang/String;Lfuw;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 259
    if-eqz p1, :cond_0

    iget-object v0, p1, Lnrp;->b:Lohv;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    if-nez v0, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    iget-object v1, p1, Lnrp;->b:Lohv;

    .line 264
    iget-object v0, v1, Lohv;->c:Lohq;

    .line 268
    iget-object v3, v0, Lohq;->f:Ljava/lang/Boolean;

    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 269
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v3, v2

    move-object v2, p2

    .line 288
    :goto_1
    iget-object v4, p1, Lnrp;->e:Ljava/lang/String;

    move-object v0, p0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lohv;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lfuw;)V

    goto :goto_0

    .line 272
    :cond_2
    iget-object v3, p1, Lnrp;->f:[Lpeo;

    if-eqz v3, :cond_3

    iget-object v3, p1, Lnrp;->f:[Lpeo;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 273
    iget-object v0, p1, Lnrp;->f:[Lpeo;

    aget-object v0, v0, v4

    if-eqz v0, :cond_5

    .line 274
    iget-object v0, p1, Lnrp;->f:[Lpeo;

    aget-object v0, v0, v4

    invoke-static {v0}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 276
    :goto_2
    iget-object v3, p1, Lnrp;->f:[Lpeo;

    array-length v3, v3

    if-le v3, v5, :cond_4

    iget-object v3, p1, Lnrp;->f:[Lpeo;

    aget-object v3, v3, v5

    if-eqz v3, :cond_4

    .line 278
    iget-object v2, p1, Lnrp;->f:[Lpeo;

    aget-object v2, v2, v5

    invoke-static {v2}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const-string v3, "\n"

    .line 279
    invoke-virtual {v2, v4, v3}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 280
    invoke-virtual {v2, v4, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    move-object v3, v2

    move-object v2, v0

    goto :goto_1

    .line 284
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p1, Lnrp;->c:Ljava/lang/Integer;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v4

    iget-object v5, p1, Lnrp;->d:[Lohv;

    .line 283
    invoke-static {v3, v4, v0, v5}, Llax;->a(Landroid/content/Context;ILohq;[Lohv;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v2

    move-object v2, v0

    goto :goto_1

    :cond_4
    move-object v3, v2

    move-object v2, v0

    goto :goto_1

    :cond_5
    move-object v0, v2

    goto :goto_2
.end method

.method public a(Lohv;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lfuw;)V
    .locals 14

    .prologue
    .line 322
    iget-object v0, p1, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v1

    .line 323
    iget-object v0, p1, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->b(Lohp;)Ljava/lang/String;

    move-result-object v2

    .line 328
    iget-object v0, p1, Lohv;->c:Lohq;

    if-nez v0, :cond_1

    .line 329
    const/4 v3, 0x0

    .line 330
    const/4 v4, 0x0

    .line 331
    const/4 v10, 0x0

    .line 332
    const/4 v11, 0x0

    .line 341
    :goto_0
    iget-object v5, p1, Lohv;->d:[Loij;

    .line 342
    const/4 v8, 0x0

    .line 344
    if-eqz v5, :cond_4

    array-length v0, v5

    if-lez v0, :cond_4

    .line 345
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 347
    const/4 v0, 0x0

    :goto_1
    array-length v7, v5

    if-ge v0, v7, :cond_3

    .line 348
    aget-object v7, v5, v0

    .line 349
    iget-object v8, v7, Loij;->b:Lohn;

    if-eqz v8, :cond_0

    iget-object v8, v7, Loij;->b:Lohn;

    iget-object v8, v8, Lohn;->c:Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 350
    iget-object v7, v7, Loij;->b:Lohn;

    iget-object v7, v7, Lohn;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    const/16 v7, 0x7c

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 347
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 334
    :cond_1
    iget-object v0, p1, Lohv;->c:Lohq;

    iget-object v3, v0, Lohq;->c:Ljava/lang/String;

    .line 335
    iget-object v0, p1, Lohv;->c:Lohq;

    iget-object v4, v0, Lohq;->a:Ljava/lang/String;

    .line 336
    iget-object v0, p1, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->f:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 338
    :goto_2
    iget-object v5, p1, Lohv;->c:Lohq;

    iget-object v5, v5, Lohq;->i:Ljava/lang/Boolean;

    invoke-static {v5}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v11

    move v10, v0

    goto :goto_0

    .line 336
    :cond_2
    iget-object v0, p1, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->f:Ljava/lang/Boolean;

    .line 337
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_2

    .line 354
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 355
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 358
    :cond_4
    const/4 v7, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v9, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v0 .. v13}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/String;Ljava/lang/String;ZZLandroid/os/Bundle;Lfuw;)V

    .line 361
    return-void
.end method

.method public a(Lohv;Ljava/lang/String;Lfuw;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 309
    if-eqz p1, :cond_0

    iget-object v0, p1, Lohv;->b:Lohp;

    if-nez v0, :cond_1

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v0, p1, Lohv;->c:Lohq;

    .line 314
    iget-object v1, v0, Lohq;->f:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 315
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move-object v2, p2

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, p3

    .line 317
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lohv;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lfuw;)V

    goto :goto_0

    .line 316
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v0, v3}, Llax;->a(Landroid/content/Context;ILohq;[Lohv;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public a(Loiu;Ljava/lang/String;Lfuw;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 293
    if-eqz p1, :cond_0

    iget-object v0, p1, Loiu;->b:Lohv;

    if-eqz v0, :cond_0

    iget-object v0, p1, Loiu;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    if-nez v0, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    iget-object v1, p1, Loiu;->b:Lohv;

    .line 299
    iget-object v0, p1, Loiu;->c:Loix;

    .line 300
    iget-object v2, v1, Lohv;->c:Lohq;

    .line 301
    iget-object v4, v2, Lohq;->f:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 302
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object v2, p2

    .line 304
    :goto_1
    iget-object v4, p1, Loiu;->d:Ljava/lang/String;

    move-object v0, p0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lohv;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lfuw;)V

    goto :goto_0

    .line 303
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v4

    if-eqz v0, :cond_3

    iget-object v0, v0, Loix;->a:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    :goto_2
    invoke-static {v4, v0, v2, v3}, Llax;->a(Landroid/content/Context;ILohq;[Lohv;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 192
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->l:Z

    .line 193
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->a:Z

    .line 165
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->p:Z

    .line 240
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->g:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->i:Ljava/lang/Object;

    return-object v0
.end method

.method public g()Landroid/view/View;
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;->getWidth()I

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 560
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 561
    const v1, 0x7f100174

    if-eq v0, v1, :cond_0

    const v1, 0x7f1001f8

    if-ne v0, v1, :cond_4

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->k:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->l:Z

    if-eqz v0, :cond_3

    .line 564
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 565
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhkr;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lhkr;->a(Landroid/view/View;)V

    .line 567
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->w:Landroid/os/Bundle;

    iget v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    invoke-interface {v0, v1, v2, v3, v4}, Lfze;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)V

    .line 598
    :cond_3
    :goto_0
    return-void

    .line 570
    :cond_4
    const v1, 0x7f10048c

    if-ne v0, v1, :cond_7

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    if-eqz v0, :cond_3

    .line 572
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->t:Z

    if-eqz v0, :cond_6

    .line 573
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->e:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->j:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v2, v0}, Lfze;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 575
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->g:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    invoke-interface {v0, v1, v2, v3, v4}, Lfze;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 579
    :cond_7
    const v1, 0x7f10048d

    if-ne v0, v1, :cond_8

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    if-eqz v0, :cond_3

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->f:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->j:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->g:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    invoke-interface/range {v0 .. v5}, Lfze;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0

    .line 584
    :cond_8
    const v1, 0x7f100491

    if-ne v0, v1, :cond_9

    .line 585
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    if-eqz v0, :cond_3

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lfze;->a(Lcom/google/android/apps/plus/views/PeopleListRowView;Ljava/lang/String;)V

    goto :goto_0

    .line 588
    :cond_9
    const v1, 0x7f100490

    if-ne v0, v1, :cond_3

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->k:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->l:Z

    if-eqz v0, :cond_3

    .line 591
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_b

    .line 592
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhkr;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lhkr;->a(Landroid/view/View;)V

    .line 594
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->w:Landroid/os/Bundle;

    iget v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    invoke-interface {v0, v1, v2, v3, v4}, Lfze;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)V

    goto/16 :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 215
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 217
    const v0, 0x7f100174

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->m:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 218
    const v0, 0x7f100117

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->n:Landroid/widget/ImageView;

    .line 219
    const v0, 0x7f1001f8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->o:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    .line 220
    const v0, 0x7f10048c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->q:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 222
    const v0, 0x7f10048d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->r:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 224
    const v0, 0x7f10048e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->x:Landroid/widget/TextView;

    .line 225
    const v0, 0x7f100490

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->s:Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PeopleListSwipeableRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    :cond_0
    const v0, 0x7f100491

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->b:Landroid/view/View;

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->b:Landroid/view/View;

    new-instance v1, Lhmi;

    invoke-direct {v1, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->b:Landroid/view/View;

    new-instance v1, Lhmk;

    sget-object v2, Lonl;->k:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 236
    :cond_1
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 6

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->v:Lfze;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->f:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->j:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->g:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->h:I

    invoke-interface/range {v0 .. v5}, Lfze;->b(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)Z

    move-result v0

    .line 606
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
