.class public Lcom/google/android/apps/plus/service/EsDreamService;
.super Landroid/service/dreams/DreamService;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/plus/views/DreamViewFlipper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/service/dreams/DreamService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 55
    const-string v0, "DreamSettings"

    invoke-virtual {p0, v0, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "selected_account_gaia_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 57
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 58
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 61
    const-string v2, ":"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 62
    aget-object v2, v1, v5

    .line 63
    const/4 v3, 0x1

    aget-object v3, v1, v3

    .line 64
    invoke-interface {v0, v3}, Lhei;->b(Ljava/lang/String;)I

    move-result v1

    .line 66
    if-ne v1, v4, :cond_3

    .line 67
    invoke-interface {v0, v2, v3}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    invoke-static {p0}, Ldhv;->b(Landroid/content/Context;)I

    move-result v0

    .line 74
    if-ne v0, v4, :cond_2

    .line 75
    invoke-static {p0}, Ldhv;->a(Landroid/content/Context;)I

    move-result v0

    .line 78
    :cond_2
    if-eq v0, v4, :cond_0

    .line 79
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Landroid/content/Context;I)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xc

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 7

    .prologue
    .line 87
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 88
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 89
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90
    const-string v2, "gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    const-string v2, "DreamSettings"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 93
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "selected_account_gaia_id"

    .line 94
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 95
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 96
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsDreamService;->c(Landroid/content/Context;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 111
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "selected_clusters"

    .line 112
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 113
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 114
    return-void
.end method

.method public static a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    const-string v0, ":"

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;I)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 101
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 104
    :goto_0
    return-object v0

    .line 103
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsDreamService;->c(Landroid/content/Context;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "selected_clusters"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 104
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;I)Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 121
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 122
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    const-string v1, "DreamSettings"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onAttachedToWindow()V

    .line 129
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/EsDreamService;->setInteractive(Z)V

    .line 130
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/EsDreamService;->setFullscreen(Z)V

    .line 131
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Landroid/content/Context;)I

    move-result v0

    .line 133
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 134
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsDreamService;->b(Landroid/content/Context;I)Ljava/util/Set;

    move-result-object v0

    .line 139
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 140
    const v0, 0x7f040095

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/EsDreamService;->setContentView(I)V

    .line 144
    :goto_1
    const v0, 0x7f10027b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/EsDreamService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/DreamViewFlipper;

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsDreamService;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    .line 145
    return-void

    .line 136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 142
    :cond_1
    const v0, 0x7f040094

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/EsDreamService;->setContentView(I)V

    goto :goto_1
.end method

.method public onDreamingStarted()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStarted()V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsDreamService;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsDreamService;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Landroid/content/Context;)V

    .line 154
    :cond_0
    return-void
.end method

.method public onDreamingStopped()V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStopped()V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsDreamService;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsDreamService;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a()V

    .line 163
    :cond_0
    return-void
.end method
