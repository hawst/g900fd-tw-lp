.class public Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;
.super Landroid/widget/AutoCompleteTextView;
.source "PG"


# instance fields
.field private a:Lgbq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 120
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)Lgbq;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->a:Lgbq;

    return-object v0
.end method


# virtual methods
.method public a(Lgbq;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->a:Lgbq;

    .line 187
    return-void
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    .prologue
    .line 170
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    .line 171
    new-instance v1, Lgbp;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v0, v2}, Lgbp;-><init>(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;Landroid/view/inputmethod/InputConnection;Z)V

    .line 173
    invoke-virtual {v1, p0}, Lgbp;->a(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)V

    .line 174
    return-object v1
.end method
