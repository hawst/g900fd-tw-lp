.class public Lcom/google/android/apps/plus/service/PhotosAppTransitionMonitor;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 22
    invoke-static {v0}, Lfuy;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-static {p1}, Lfuy;->c(Landroid/content/Context;)V

    .line 25
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lfhy;

    invoke-direct {v1, p1}, Lfhy;-><init>(Landroid/content/Context;)V

    const-string v2, "disable_photos_intent"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 30
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 32
    :cond_0
    return-void
.end method
