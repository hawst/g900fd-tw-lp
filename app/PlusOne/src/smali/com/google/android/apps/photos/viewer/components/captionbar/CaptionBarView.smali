.class public Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;


# static fields
.field private static a:Landroid/graphics/Bitmap;

.field private static b:Landroid/graphics/Bitmap;

.field private static c:Landroid/graphics/Paint;

.field private static d:Landroid/graphics/Paint;

.field private static e:F

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:I

.field private static n:I


# instance fields
.field private o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lczk;

.field private q:Lfwt;

.field private r:Lljg;

.field private s:Ljava/lang/String;

.field private t:Lljg;

.field private u:Landroid/text/Spannable;

.field private v:Lljg;

.field private w:Ljava/lang/String;

.field private x:I

.field private y:Z

.field private final z:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 102
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->o:Ljava/util/Set;

    .line 99
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->z:Ljava/text/NumberFormat;

    .line 114
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 117
    const v1, 0x7f0d02f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->e:F

    .line 118
    const v1, 0x7f0d0301

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->f:I

    .line 120
    const v1, 0x7f0d0309

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->g:I

    .line 122
    const v1, 0x7f0d030a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->h:I

    .line 124
    const v1, 0x7f0d030b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->i:I

    .line 126
    const v1, 0x7f0d0310

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->j:I

    .line 128
    const v1, 0x7f0d030d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->k:I

    .line 130
    const v1, 0x7f0d030e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->l:I

    .line 132
    const v1, 0x7f0d030f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->m:I

    .line 134
    const v1, 0x7f0d0311

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->n:I

    .line 136
    const v1, 0x7f0d030c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a:Landroid/graphics/Bitmap;

    .line 141
    const v1, 0x7f020085

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->b:Landroid/graphics/Bitmap;

    .line 143
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 144
    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 147
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->d:Landroid/graphics/Paint;

    .line 150
    :cond_0
    new-instance v0, Lczl;

    invoke-direct {v0, p0}, Lczl;-><init>(Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;)V

    invoke-static {p0, v0}, Liu;->a(Landroid/view/View;Lgw;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->o:Ljava/util/Set;

    .line 99
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->z:Ljava/text/NumberFormat;

    .line 114
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 117
    const v1, 0x7f0d02f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->e:F

    .line 118
    const v1, 0x7f0d0301

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->f:I

    .line 120
    const v1, 0x7f0d0309

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->g:I

    .line 122
    const v1, 0x7f0d030a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->h:I

    .line 124
    const v1, 0x7f0d030b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->i:I

    .line 126
    const v1, 0x7f0d0310

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->j:I

    .line 128
    const v1, 0x7f0d030d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->k:I

    .line 130
    const v1, 0x7f0d030e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->l:I

    .line 132
    const v1, 0x7f0d030f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->m:I

    .line 134
    const v1, 0x7f0d0311

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->n:I

    .line 136
    const v1, 0x7f0d030c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a:Landroid/graphics/Bitmap;

    .line 141
    const v1, 0x7f020085

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->b:Landroid/graphics/Bitmap;

    .line 143
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 144
    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 147
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->d:Landroid/graphics/Paint;

    .line 150
    :cond_0
    new-instance v0, Lczl;

    invoke-direct {v0, p0}, Lczl;-><init>(Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;)V

    invoke-static {p0, v0}, Liu;->a(Landroid/view/View;Lgw;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->o:Ljava/util/Set;

    .line 99
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->z:Ljava/text/NumberFormat;

    .line 114
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 117
    const v1, 0x7f0d02f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->e:F

    .line 118
    const v1, 0x7f0d0301

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->f:I

    .line 120
    const v1, 0x7f0d0309

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->g:I

    .line 122
    const v1, 0x7f0d030a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->h:I

    .line 124
    const v1, 0x7f0d030b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->i:I

    .line 126
    const v1, 0x7f0d0310

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->j:I

    .line 128
    const v1, 0x7f0d030d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->k:I

    .line 130
    const v1, 0x7f0d030e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->l:I

    .line 132
    const v1, 0x7f0d030f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->m:I

    .line 134
    const v1, 0x7f0d0311

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->n:I

    .line 136
    const v1, 0x7f0d030c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a:Landroid/graphics/Bitmap;

    .line 141
    const v1, 0x7f020085

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->b:Landroid/graphics/Bitmap;

    .line 143
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 144
    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 147
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->d:Landroid/graphics/Paint;

    .line 150
    :cond_0
    new-instance v0, Lczl;

    invoke-direct {v0, p0}, Lczl;-><init>(Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;)V

    invoke-static {p0, v0}, Liu;->a(Landroid/view/View;Lgw;)V

    .line 111
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->s:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;)Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->u:Landroid/text/Spannable;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c()V

    .line 283
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->r:Lljg;

    .line 284
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->t:Lljg;

    .line 285
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 289
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->o:Ljava/util/Set;

    .line 290
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->s:Ljava/lang/String;

    .line 291
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->u:Landroid/text/Spannable;

    .line 292
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->w:Ljava/lang/String;

    .line 293
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->p:Lczk;

    .line 294
    return-void
.end method

.method public a(Lczk;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->p:Lczk;

    .line 191
    return-void
.end method

.method public a(Ljava/lang/Long;)V
    .locals 6

    .prologue
    .line 340
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0xfa

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 341
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110076

    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->z:Ljava/text/NumberFormat;

    .line 342
    invoke-virtual {v5, p1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 341
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->w:Ljava/lang/String;

    .line 344
    return-void

    .line 341
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->u:Landroid/text/Spannable;

    .line 181
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    invoke-static {p1}, Llir;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->u:Landroid/text/Spannable;

    .line 184
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c()V

    .line 168
    if-nez p2, :cond_0

    const-string p2, ""

    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->s:Ljava/lang/String;

    .line 169
    new-instance v0, Lfwt;

    iget-object v4, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->s:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->p:Lczk;

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lfwt;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhsn;I)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->p:Lczk;

    invoke-virtual {v0, v1}, Lfwt;->a(Lhsn;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->o:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->b()V

    .line 174
    return-void
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->invalidate()V

    .line 327
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 335
    iput-boolean p1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->y:Z

    .line 336
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-virtual {v0}, Lfwt;->b()V

    .line 313
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-virtual {v0}, Lfwt;->c()V

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->o:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 320
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    .line 322
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 304
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->b()V

    .line 306
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 298
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c()V

    .line 300
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 228
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    if-nez v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->x:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-virtual {v0}, Lfwt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-virtual {v0}, Lfwt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 246
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-virtual {v1}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 247
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->b:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-virtual {v1}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-virtual {v0}, Lfwt;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    invoke-virtual {v0, p1}, Lfwt;->a(Landroid/graphics/Canvas;)V

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->r:Lljg;

    if-eqz v0, :cond_3

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->r:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    .line 255
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->r:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    .line 256
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 257
    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->r:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    .line 258
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->t:Lljg;

    if-eqz v0, :cond_4

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->t:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->t:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    .line 264
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 265
    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->t:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    .line 266
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 269
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    .line 271
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    .line 272
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 273
    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    .line 274
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_0

    .line 240
    :cond_5
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a:Landroid/graphics/Bitmap;

    goto/16 :goto_1
.end method

.method public onMeasure(II)V
    .locals 17

    .prologue
    .line 195
    invoke-super/range {p0 .. p2}, Landroid/view/View;->onMeasure(II)V

    .line 196
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    if-nez v1, :cond_1

    .line 197
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->setMeasuredDimension(II)V

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getPaddingLeft()I

    move-result v1

    sget v2, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->i:I

    add-int/2addr v2, v1

    .line 202
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getPaddingTop()I

    move-result v1

    sget v3, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->g:I

    add-int v10, v1, v3

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getPaddingTop()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->x:I

    .line 205
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getMeasuredWidth()I

    move-result v1

    .line 206
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->w:Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 209
    sget v3, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->n:I

    sub-int/2addr v1, v3

    move v9, v1

    .line 212
    :goto_1
    sub-int v1, v9, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getPaddingRight()I

    move-result v3

    sub-int v13, v1, v3

    .line 214
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    .line 216
    :goto_2
    sget v2, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->h:I

    add-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v1}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->setMeasuredDimension(II)V

    .line 218
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->p:Lczk;

    if-eqz v1, :cond_2

    .line 219
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->p:Lczk;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getMeasuredHeight()I

    invoke-interface {v1}, Lczk;->a()V

    .line 221
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->y:Z

    if-nez v1, :cond_0

    .line 222
    const/16 v1, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->setVisibility(I)V

    goto :goto_0

    .line 214
    :cond_3
    sget v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->f:I

    add-int/2addr v1, v2

    sget v3, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->k:I

    add-int v11, v1, v3

    sub-int v1, v13, v11

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->getContext()Landroid/content/Context;

    move-result-object v14

    const/16 v3, 0x20

    invoke-static {v14, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->q:Lfwt;

    sget v5, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->f:I

    add-int/2addr v5, v2

    sget v6, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->f:I

    add-int/2addr v6, v10

    invoke-virtual {v4, v2, v10, v5, v6}, Lfwt;->a(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->s:Ljava/lang/String;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v3, v1, v4}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    sub-int v1, v13, v11

    invoke-static {v3, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;)I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-instance v1, Lljg;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->e:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->r:Lljg;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->u:Landroid/text/Spannable;

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->w:Ljava/lang/String;

    if-nez v1, :cond_7

    sget v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->j:I

    add-int/2addr v1, v10

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->r:Lljg;

    invoke-virtual {v2, v11, v1}, Lljg;->a(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->r:Lljg;

    invoke-virtual {v2}, Lljg;->getHeight()I

    move-result v2

    add-int v12, v1, v2

    const/16 v1, 0xa

    invoke-static {v14, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->w:Ljava/lang/String;

    invoke-static {v15, v1}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->u:Landroid/text/Spannable;

    if-nez v1, :cond_4

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->t:Lljg;

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->w:Ljava/lang/String;

    if-nez v1, :cond_5

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    move v1, v12

    :goto_5
    sget v2, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->f:I

    sub-int/2addr v1, v10

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v1, v10

    goto/16 :goto_2

    :cond_4
    const/16 v1, 0x25

    invoke-static {v14, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    sub-int v1, v13, v11

    sub-int v4, v1, v16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->u:Landroid/text/Spannable;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v1, v3, v4, v2}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v1, Lljg;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->e:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->t:Lljg;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->t:Lljg;

    invoke-virtual {v1, v11, v12}, Lljg;->a(II)V

    invoke-static {v3, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;)I

    move-result v1

    add-int/2addr v1, v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->w:Ljava/lang/String;

    if-eqz v2, :cond_6

    sget v2, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->l:I

    add-int/2addr v1, v2

    move v11, v1

    goto :goto_4

    :cond_5
    new-instance v1, Lljg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->w:Ljava/lang/String;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->e:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v15

    move/from16 v4, v16

    invoke-direct/range {v1 .. v8}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    sget v1, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->m:I

    add-int/2addr v1, v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    invoke-virtual {v2, v11, v1}, Lljg;->a(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->v:Lljg;

    invoke-virtual {v2}, Lljg;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_5

    :cond_6
    move v11, v1

    goto :goto_4

    :cond_7
    move v1, v10

    goto/16 :goto_3

    :cond_8
    move v9, v1

    goto/16 :goto_1
.end method
