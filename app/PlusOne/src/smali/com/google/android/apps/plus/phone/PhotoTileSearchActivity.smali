.class public Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;

.field private f:I

.field private g:Landroid/widget/FrameLayout;

.field private h:Landroid/widget/FrameLayout;

.field private i:Legc;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 51
    invoke-direct {p0}, Lloa;-><init>()V

    .line 63
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 64
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 66
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->x:Llnh;

    .line 67
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 68
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 70
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 71
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 73
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->x:Llnh;

    .line 74
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 76
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->x:Llnh;

    .line 77
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 80
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->e:Ldie;

    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 137
    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->f:I

    .line 138
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->l()V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->i:Legc;

    if-eqz v0, :cond_0

    .line 140
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->m()V

    .line 142
    :cond_0
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 164
    const v0, 0x7f100301

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 165
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0324

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 166
    const v0, 0x7f100302

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 167
    return-void
.end method

.method private b(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 170
    const v0, 0x7f100301

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 171
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 172
    const v0, 0x7f100302

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 173
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->f:I

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->a(Landroid/view/ViewGroup;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->h:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->b(Landroid/view/ViewGroup;)V

    .line 161
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->h:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->a(Landroid/view/ViewGroup;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->b(Landroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 176
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->f:I

    packed-switch v0, :pswitch_data_0

    .line 186
    :goto_0
    return-void

    .line 180
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->i:Legc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Legc;->d(I)V

    goto :goto_0

    .line 183
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->i:Legc;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Legc;->d(I)V

    goto :goto_0

    .line 176
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 230
    sget-object v0, Lhmw;->M:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 91
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 93
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 94
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 95
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 97
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsm;

    new-instance v2, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    .line 98
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->x:Llnh;

    const-class v1, Lcnt;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    .line 102
    const-string v1, "PhotoSearch"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcnt;->a(Ljava/lang/String;I)V

    .line 104
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 218
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 219
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 220
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 221
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 208
    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 209
    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 210
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0, p1}, Lloa;->a(Lu;)V

    .line 147
    instance-of v0, p1, Legc;

    if-eqz v0, :cond_0

    .line 148
    check-cast p1, Legc;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->i:Legc;

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->m()V

    .line 151
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->g:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_1

    .line 191
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->a(I)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->h:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_0

    .line 193
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->a(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f100302

    .line 108
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 110
    if-nez p1, :cond_0

    .line 111
    new-instance v0, Legc;

    invoke-direct {v0}, Legc;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 114
    :cond_0
    const v0, 0x7f0400ce

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->setContentView(I)V

    .line 116
    if-eqz p1, :cond_1

    .line 117
    const-string v0, "selected_tab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->f:I

    .line 120
    :cond_1
    const v0, 0x7f1002ff

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->g:Landroid/widget/FrameLayout;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0a74

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 123
    const v0, 0x7f100300

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->h:Landroid/widget/FrameLayout;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0a75

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 127
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->l()V

    .line 128
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0, p1}, Lloa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 133
    const-string v0, "selected_tab"

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileSearchActivity;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 134
    return-void
.end method
