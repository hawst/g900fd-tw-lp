.class public Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;
.super Lfzb;
.source "PG"


# instance fields
.field public b:Ljava/lang/String;

.field private c:Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lfzb;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->f:Ljava/text/SimpleDateFormat;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->f:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lfzb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->f:Ljava/text/SimpleDateFormat;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->f:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lfzb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->f:Ljava/text/SimpleDateFormat;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->f:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 44
    return-void
.end method


# virtual methods
.method public a(Lnfq;I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 60
    .line 65
    if-ltz p2, :cond_6

    if-eqz p1, :cond_6

    iget-object v0, p1, Lnfq;->a:[Lnym;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lnfq;->a:[Lnym;

    array-length v0, v0

    if-le v0, p2, :cond_6

    .line 69
    iget-object v0, p1, Lnfq;->a:[Lnym;

    aget-object v2, v0, p2

    .line 70
    iget-object v0, v2, Lnym;->A:Lpcp;

    if-eqz v0, :cond_5

    .line 71
    iget-object v0, v2, Lnym;->A:Lpcp;

    iget-object v0, v0, Lpcp;->a:Ljava/lang/String;

    .line 73
    :goto_0
    iget-object v3, v2, Lnym;->j:Ljava/lang/String;

    .line 74
    iget-object v4, v2, Lnym;->m:Lnzb;

    if-eqz v4, :cond_4

    .line 75
    iget-object v4, v2, Lnym;->m:Lnzb;

    iget-object v4, v4, Lnzb;->a:Ljava/lang/String;

    .line 77
    iget-object v2, v2, Lnym;->m:Lnzb;

    iget-object v2, v2, Lnzb;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 78
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 79
    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->f:Ljava/text/SimpleDateFormat;

    invoke-virtual {v5, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    move-object v8, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v8

    .line 83
    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 84
    invoke-static {v4}, Llnf;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->b:Ljava/lang/String;

    .line 85
    new-instance v4, Lfzd;

    invoke-direct {v4, p0}, Lfzd;-><init>(Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 98
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->c:Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Ljac;->a:Ljac;

    invoke-static {v5, v3, v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a(Lizu;)V

    .line 103
    :goto_3
    if-eqz v2, :cond_2

    .line 104
    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    :goto_4
    if-eqz v0, :cond_3

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :goto_5
    return-void

    .line 94
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 100
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->c:Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;->a(Lizu;)V

    goto :goto_3

    .line 106
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 112
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    :cond_4
    move-object v2, v3

    move-object v4, v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0

    :cond_6
    move-object v0, v1

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lfzb;->onFinishInflate()V

    .line 54
    const v0, 0x7f1001b1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->c:Lcom/google/android/apps/plus/views/OneProfileVideoThumbnailView;

    .line 55
    const v0, 0x7f100118

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->d:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f100246

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->e:Landroid/widget/TextView;

    .line 57
    return-void
.end method
