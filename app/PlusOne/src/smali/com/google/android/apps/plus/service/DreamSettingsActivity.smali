.class public Lcom/google/android/apps/plus/service/DreamSettingsActivity;
.super Llon;
.source "PG"


# static fields
.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;


# instance fields
.field private j:Landroid/widget/ListView;

.field private k:Lfft;

.field private final l:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lhei;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 74
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "0 AS row_type"

    aput-object v1, v0, v2

    const-string v1, "cluster_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "1 AS media_type"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->g:[Ljava/lang/String;

    .line 86
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "0 AS row_type"

    aput-object v1, v0, v2

    const-string v1, "cluster_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "0 AS media_type"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->h:[Ljava/lang/String;

    .line 93
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "2 AS row_type"

    aput-object v1, v0, v2

    const-string v1, "account_id"

    aput-object v1, v0, v3

    const-string v1, "account_display_name"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->i:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Llon;-><init>()V

    .line 101
    new-instance v0, Lffy;

    invoke-direct {v0, p0}, Lffy;-><init>(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->l:Lbc;

    .line 102
    new-instance v0, Lffz;

    invoke-direct {v0, p0}, Lffz;-><init>(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->m:Lbc;

    .line 445
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)I
    .locals 1

    .prologue
    .line 45
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f0a0676

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->a(Landroid/content/Context;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;I)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 161
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->h:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 162
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 163
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    aput-object v5, v1, v4

    const/4 v2, 0x2

    .line 165
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v5, v1, v2

    .line 162
    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 168
    return-object v0
.end method

.method public static synthetic b(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f0a00be

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->a(Landroid/content/Context;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)Lfft;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->k:Lfft;

    if-nez v0, :cond_0

    new-instance v0, Lfft;

    invoke-direct {v0, p0, p0}, Lfft;-><init>(Lcom/google/android/apps/plus/service/DreamSettingsActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->k:Lfft;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->k:Lfft;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->k:Lfft;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)Lbc;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->m:Lbc;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 45
    const v0, 0x7f0a0675

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->a(Landroid/content/Context;I)Landroid/database/Cursor;

    move-result-object v1

    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->i:[Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->n:Lhei;

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "logged_in"

    aput-object v4, v3, v7

    invoke-interface {v0, v3}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->n:Lhei;

    invoke-interface {v4, v0}, Lhei;->a(I)Lhej;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    const-string v0, "display_name"

    invoke-interface {v4, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v9

    invoke-virtual {v2, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/database/MergeCursor;

    new-array v3, v9, [Landroid/database/Cursor;

    aput-object v1, v3, v7

    aput-object v2, v3, v8

    invoke-direct {v0, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v0
.end method

.method public static synthetic h()Landroid/database/Cursor;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 45
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "row_type"

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    return-object v0
.end method

.method public static synthetic i()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->g:[Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic j()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->h:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->e:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->n:Lhei;

    .line 109
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 113
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 114
    const v0, 0x7f040080

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->setContentView(I)V

    .line 115
    const v0, 0x7f100242

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 116
    const v0, 0x7f100243

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->j:Landroid/widget/ListView;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->g()Lbb;

    move-result-object v0

    .line 119
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->m:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 121
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->l:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 123
    return-void
.end method
