.class public Lcom/google/android/apps/plus/service/CastService;
.super Ljuo;
.source "PG"

# interfaces
.implements Lkdd;


# instance fields
.field private a:Z

.field private b:Landroid/content/BroadcastReceiver;

.field private c:Lcom/google/android/libraries/social/media/MediaResource;

.field private d:Landroid/os/Bundle;

.field private e:Lizu;

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljuo;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/libraries/social/media/MediaResource;Landroid/widget/RemoteViews;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 221
    const v1, 0x7f0a0b19

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-class v0, Ljuk;

    .line 223
    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    invoke-virtual {v0}, Ljuk;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 221
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 224
    const v1, 0x7f1004a8

    invoke-virtual {p2}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p3, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 225
    const v1, 0x7f1004aa

    const v2, 0x7f0a0b15

    .line 227
    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 225
    invoke-virtual {p3, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 228
    const v1, 0x7f1004ab

    invoke-virtual {p3, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 230
    invoke-static {p1}, Lcom/google/android/apps/plus/service/SlideshowService;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 232
    const v1, 0x7f1004a9

    const/high16 v2, 0x8000000

    .line 233
    invoke-static {p1, v4, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 232
    invoke-virtual {p3, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 235
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/CastService;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/CastService;->a:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/CastService;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/apps/plus/service/CastService;->a:Z

    return p1
.end method


# virtual methods
.method a()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lffo;

    invoke-direct {v0, p0}, Lffo;-><init>(Lcom/google/android/apps/plus/service/CastService;)V

    return-object v0
.end method

.method public a(Lkda;)V
    .locals 2

    .prologue
    .line 261
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    .line 262
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 268
    :goto_0
    return-void

    .line 265
    :cond_0
    check-cast p1, Lcom/google/android/libraries/social/media/MediaResource;

    iput-object p1, p0, Lcom/google/android/apps/plus/service/CastService;->c:Lcom/google/android/libraries/social/media/MediaResource;

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->d()Landroid/app/Notification;

    move-result-object v0

    .line 267
    const v1, 0x7f10009d

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/service/CastService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->c()V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/CastService;->e:Lizu;

    const/4 v2, 0x2

    .line 248
    invoke-virtual {v0, v1, v2, p0}, Lizs;->a(Lizu;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->c:Lcom/google/android/libraries/social/media/MediaResource;

    .line 249
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->c:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->c:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->c:Lcom/google/android/libraries/social/media/MediaResource;

    .line 257
    :cond_0
    return-void
.end method

.method public d()Landroid/app/Notification;
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const v11, 0x7f1004ac

    const/high16 v10, 0x8000000

    const/16 v2, 0x8

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 124
    new-instance v4, Lbs;

    invoke-direct {v4, v3}, Lbs;-><init>(Landroid/content/Context;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v5, "account_id"

    const/4 v6, -0x1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 132
    new-instance v5, Ldew;

    invoke-direct {v5, v3, v0}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "view_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "view_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ldew;->d(Ljava/lang/String;)Ldew;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "tile_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "tile_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ldew;->a(Ljava/lang/String;)Ldew;

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "all_photos_row_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "all_photos_row_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ldew;->a(J)Ldew;

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "photo_ref"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "photo_ref"

    .line 146
    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {v5, v0}, Ldew;->a(Lizu;)Ldew;

    .line 148
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "all_photos_offset"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v6, "all_photos_offset"

    .line 150
    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 149
    invoke-virtual {v5, v0}, Ldew;->a(I)Ldew;

    .line 152
    :cond_4
    invoke-static {v3}, Lda;->a(Landroid/content/Context;)Lda;

    move-result-object v0

    .line 153
    iget-object v6, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    const-string v7, "show_oob_tile"

    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 154
    const-class v6, Lcom/google/android/apps/plus/phone/HostStreamPhotoPagerActivity;

    invoke-virtual {v0, v6}, Lda;->a(Ljava/lang/Class;)Lda;

    .line 155
    invoke-virtual {v5, v9}, Ldew;->h(Z)Ldew;

    .line 160
    :goto_0
    invoke-virtual {v5}, Ldew;->a()Landroid/content/Intent;

    move-result-object v5

    .line 161
    const-string v6, "com.google.android.libraries.social.notifications.FROM_NOTIFICATION"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 162
    invoke-virtual {v0, v5}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 163
    invoke-virtual {v0, v1, v10}, Lda;->a(II)Landroid/app/PendingIntent;

    move-result-object v0

    .line 168
    iget-object v5, p0, Lcom/google/android/apps/plus/service/CastService;->c:Lcom/google/android/libraries/social/media/MediaResource;

    new-instance v6, Landroid/widget/RemoteViews;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f04017f

    invoke-direct {v6, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v3, v5, v6}, Lcom/google/android/apps/plus/service/CastService;->a(Landroid/content/Context;Lcom/google/android/libraries/social/media/MediaResource;Landroid/widget/RemoteViews;)V

    .line 171
    const v5, 0x7f0202de

    .line 172
    invoke-virtual {v4, v5}, Lbs;->a(I)Lbs;

    move-result-object v5

    .line 173
    invoke-virtual {v5, v6}, Lbs;->a(Landroid/widget/RemoteViews;)Lbs;

    .line 174
    invoke-virtual {v4, v0}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    .line 175
    invoke-virtual {v4, v9}, Lbs;->e(I)Lbs;

    .line 176
    invoke-virtual {v4}, Lbs;->c()Landroid/app/Notification;

    move-result-object v4

    .line 177
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v0, v5, :cond_5

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->c:Lcom/google/android/libraries/social/media/MediaResource;

    new-instance v5, Landroid/widget/RemoteViews;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f04017e

    invoke-direct {v5, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v3, v0, v5}, Lcom/google/android/apps/plus/service/CastService;->a(Landroid/content/Context;Lcom/google/android/libraries/social/media/MediaResource;Landroid/widget/RemoteViews;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/CastService;->a:Z

    if-eqz v0, :cond_7

    const v0, 0x7f1004ad

    invoke-virtual {v5, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {v5, v11, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-static {v3}, Lcom/google/android/apps/plus/service/SlideshowService;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v3, v1, v0, v10}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v5, v11, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :goto_1
    iput-object v5, v4, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 180
    :cond_5
    return-object v4

    .line 157
    :cond_6
    const-class v6, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;

    invoke-virtual {v0, v6}, Lda;->a(Ljava/lang/Class;)Lda;

    .line 158
    invoke-virtual {v5, v3}, Ldew;->a(Landroid/content/Context;)Ldew;

    goto :goto_0

    .line 178
    :cond_7
    invoke-virtual {v5, v11, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v6, 0x7f1004ad

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/CastService;->g:Z

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    invoke-virtual {v5, v6, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    iget v2, p0, Lcom/google/android/apps/plus/service/CastService;->f:I

    invoke-static {v3, v0, v2}, Lcom/google/android/apps/plus/service/SlideshowService;->b(Landroid/content/Context;Landroid/os/Bundle;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v3, v1, v0, v10}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v1, 0x7f1004ad

    invoke-virtual {v5, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->a()Landroid/content/BroadcastReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->b:Landroid/content/BroadcastReceiver;

    .line 61
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 62
    const-string v1, "com.google.android.apps.photos.SLIDESHOW_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/service/CastService;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Ldr;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 65
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/CastService;->b:Landroid/content/BroadcastReceiver;

    .line 96
    invoke-virtual {v0, v1}, Ldr;->a(Landroid/content/BroadcastReceiver;)V

    .line 97
    invoke-super {p0}, Ljuo;->onDestroy()V

    .line 98
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 70
    sget-boolean v0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a:Z

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->stopSelf()V

    .line 90
    :goto_0
    return v2

    .line 76
    :cond_0
    const-string v0, "notification_bundle"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    const-string v0, "notification_bundle"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->d:Landroid/os/Bundle;

    .line 78
    const-string v0, "notification_media"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    iput-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->e:Lizu;

    .line 79
    const-string v0, "notification_index"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/service/CastService;->f:I

    .line 81
    :cond_1
    const-string v0, "notification_video_playing"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    const-string v0, "notification_video_playing"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/CastService;->g:Z

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/service/CastService;->e:Lizu;

    if-eqz v0, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->b()V

    .line 89
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljuk;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    goto :goto_0
.end method
