.class public Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 33
    invoke-direct {p0}, Lloa;-><init>()V

    .line 38
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 40
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->x:Llnh;

    .line 41
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 43
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->x:Llnh;

    .line 44
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 45
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 47
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 48
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 50
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->x:Llnh;

    .line 51
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 54
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 62
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 77
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 78
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 79
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_location"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 80
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 90
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 91
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 68
    if-nez p1, :cond_0

    .line 69
    new-instance v0, Letn;

    invoke-direct {v0}, Letn;-><init>()V

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 72
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/locations/HostFriendLocationsActivity;->setContentView(I)V

    .line 73
    return-void
.end method
