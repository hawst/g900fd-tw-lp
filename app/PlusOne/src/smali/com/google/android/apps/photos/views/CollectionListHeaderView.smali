.class public Lcom/google/android/apps/photos/views/CollectionListHeaderView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private final a:Lctq;

.field private final b:Ljma;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljma",
            "<",
            "Lctq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lhqa;

.field private final d:Ljma;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljma",
            "<",
            "Lhqa;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ldhp;

.field private f:Lcra;

.field private g:Lctm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lctq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a:Lctq;

    .line 63
    new-instance v0, Ldhj;

    invoke-direct {v0, p0}, Ldhj;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->b:Ljma;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhqa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqa;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->c:Lhqa;

    .line 70
    new-instance v0, Ldhk;

    invoke-direct {v0, p0}, Ldhk;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->d:Ljma;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lctq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a:Lctq;

    .line 63
    new-instance v0, Ldhj;

    invoke-direct {v0, p0}, Ldhj;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->b:Ljma;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhqa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqa;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->c:Lhqa;

    .line 70
    new-instance v0, Ldhk;

    invoke-direct {v0, p0}, Ldhk;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->d:Ljma;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lctq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a:Lctq;

    .line 63
    new-instance v0, Ldhj;

    invoke-direct {v0, p0}, Ldhj;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->b:Ljma;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhqa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqa;

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->c:Lhqa;

    .line 70
    new-instance v0, Ldhk;

    invoke-direct {v0, p0}, Ldhk;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->d:Ljma;

    .line 59
    return-void
.end method

.method private a()V
    .locals 12

    .prologue
    .line 111
    const v0, 0x7f1001c9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 112
    const v1, 0x7f1001c8

    invoke-virtual {p0, v1}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 113
    const v2, 0x7f1001cb

    .line 114
    invoke-virtual {p0, v2}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 115
    iget-object v3, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a:Lctq;

    invoke-virtual {v3}, Lctq;->c()I

    move-result v5

    .line 117
    iget-object v3, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    if-nez v3, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    iget-object v3, v3, Lcra;->c:Ljava/lang/String;

    invoke-static {v3}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v6

    .line 122
    iget-object v3, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    iget-object v3, v3, Lcra;->c:Ljava/lang/String;

    invoke-static {v3}, Ljvj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 124
    const-string v4, "PLUS_EVENT"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 126
    iget-object v4, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    iget-wide v8, v4, Lcra;->h:J

    const-wide/16 v10, 0x200

    and-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-eqz v4, :cond_5

    if-nez v3, :cond_5

    const/4 v3, 0x1

    .line 128
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    iget-object v4, v4, Lcra;->k:Ljava/util/List;

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    .line 130
    :goto_2
    const/4 v7, 0x1

    if-eq v5, v7, :cond_9

    .line 131
    if-nez v5, :cond_7

    if-nez v4, :cond_2

    if-eqz v3, :cond_7

    :cond_2
    if-nez v6, :cond_7

    .line 136
    new-instance v4, Ldhm;

    invoke-direct {v4, p0, v3}, Ldhm;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;Z)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 144
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 161
    :goto_3
    if-eqz v6, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    iget-object v0, v0, Lcra;->c:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 163
    if-eqz v5, :cond_a

    const/4 v0, 0x5

    if-eq v5, v0, :cond_a

    const/4 v0, 0x1

    move v1, v0

    .line 165
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lhpu;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 166
    invoke-virtual {v0}, Lhpu;->c()Z

    move-result v0

    .line 167
    if-nez v1, :cond_e

    if-eqz v0, :cond_e

    .line 168
    const v0, 0x7f10007e

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 169
    if-eqz v0, :cond_3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    :cond_3
    const/4 v0, 0x1

    move v1, v0

    .line 170
    :goto_5
    if-eqz v1, :cond_4

    .line 171
    const v0, 0x7f1001ca

    .line 172
    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 173
    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 174
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 177
    const v0, 0x7f10007e

    invoke-virtual {v2, v0, v3}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 178
    new-instance v0, Ldho;

    invoke-direct {v0, p0, v3}, Ldho;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    iget-object v0, v0, Lcra;->c:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 185
    const v0, 0x7f02005a

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 126
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 128
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 145
    :cond_7
    const/4 v3, 0x2

    if-eq v5, v3, :cond_8

    const/4 v3, 0x4

    if-ne v5, v3, :cond_9

    :cond_8
    if-eqz v4, :cond_9

    .line 148
    new-instance v3, Ldhn;

    invoke-direct {v3, p0}, Ldhn;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 157
    :cond_9
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 158
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 163
    :cond_a
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_4

    .line 169
    :cond_b
    const/4 v0, 0x0

    move v1, v0

    goto :goto_5

    .line 186
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->c:Lhqa;

    invoke-virtual {v0, v3}, Lhqa;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 187
    if-eqz v1, :cond_0

    .line 188
    const v0, 0x7f02005a

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 191
    :cond_d
    const v0, 0x7f02004a

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 194
    :cond_e
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)Ldhp;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->e:Ldhp;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)Lctm;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->g:Lctm;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/photos/views/CollectionListHeaderView;)Lcra;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    return-object v0
.end method


# virtual methods
.method public a(Lctm;Lcra;Ldhp;)V
    .locals 3

    .prologue
    .line 83
    iput-object p2, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->f:Lcra;

    .line 84
    iput-object p1, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->g:Lctm;

    .line 85
    iput-object p3, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->e:Ldhp;

    .line 87
    const v0, 0x7f1001c7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 88
    iget-object v1, p2, Lcra;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    invoke-direct {p0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a()V

    .line 92
    const v0, 0x7f100174

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 94
    iget-object v1, p2, Lcra;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p2, Lcra;->f:Ljava/lang/String;

    .line 95
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    iget-object v1, p2, Lcra;->e:Ljava/lang/String;

    iget-object v2, p2, Lcra;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 98
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setClickable(Z)V

    .line 99
    new-instance v1, Ldhl;

    invoke-direct {v1, p0, p2}, Ldhl;-><init>(Lcom/google/android/apps/photos/views/CollectionListHeaderView;Lcra;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 201
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->b:Ljma;

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->c:Lhqa;

    invoke-virtual {v0}, Lhqa;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->d:Ljma;

    .line 204
    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 206
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 210
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->b:Ljma;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->c:Lhqa;

    invoke-virtual {v0}, Lhqa;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->d:Ljma;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 213
    return-void
.end method
