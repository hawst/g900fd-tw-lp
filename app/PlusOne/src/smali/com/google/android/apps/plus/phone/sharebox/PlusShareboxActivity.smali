.class public Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;
.super Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;
.source "PG"


# instance fields
.field private A:Lemx;

.field private B:Z

.field private C:Lieh;

.field private D:Z

.field private final E:Lhkd;

.field private final z:Lfef;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;-><init>()V

    .line 90
    new-instance v0, Lfef;

    invoke-direct {v0, p0}, Lfef;-><init>(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->z:Lfef;

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->B:Z

    .line 608
    new-instance v0, Lfee;

    invoke-direct {v0, p0}, Lfee;-><init>(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->E:Lhkd;

    return-void
.end method

.method public static synthetic A(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->o:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic B(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljbx;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->n:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Lhgw;)V
    .locals 0

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lhgw;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->m:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->n:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Lhgw;)V
    .locals 0

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lhgw;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->m:Z

    return p1
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Lhgw;)Lhgw;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->i:Lhgw;

    return-object p1
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->m:Z

    return p1
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljbx;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lkki;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->l:Z

    return v0
.end method

.method public static synthetic i(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->r:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Z
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->A()Z

    move-result v0

    return v0
.end method

.method public static synthetic k(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->r:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhxc;
    .locals 5

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v1

    const-string v0, "is_plus_page"

    invoke-interface {v1, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->e:Ljava/util/HashMap;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxc;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "is_dasher_account"

    invoke-interface {v1, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "is_default_restricted"

    invoke-interface {v1, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxc;

    invoke-virtual {v0}, Lhxc;->c()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->C:Lieh;

    sget-object v2, Lkmx;->e:Lief;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-interface {v0, v2, v3}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "is_child"

    invoke-interface {v1, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->e:Ljava/util/HashMap;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxc;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic m(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method public static synthetic n(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->j:Z

    return v0
.end method

.method public static synthetic o(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->r:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic p(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method public static synthetic q(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lfef;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->z:Lfef;

    return-object v0
.end method

.method public static synthetic r(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljbx;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    return-object v0
.end method

.method public static synthetic s(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic t(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->p:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    return-object v0
.end method

.method public static synthetic u(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->p:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    return-object v0
.end method

.method public static synthetic v(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic w(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->p:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    return-object v0
.end method

.method public static synthetic x(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->p:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    return-object v0
.end method

.method public static synthetic y(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic z(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->q:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected c(Landroid/content/Intent;)Lhgw;
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 415
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 416
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v3}, Lhee;->f()Z

    move-result v3

    if-nez v3, :cond_0

    move-object v0, v2

    .line 453
    :goto_0
    return-object v0

    .line 419
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v3}, Lhee;->g()Lhej;

    move-result-object v3

    const-string v4, "gaia_id"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 420
    const-string v4, "com.google.android.apps.plus.GOOGLE_PLUS_SHARE"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "android.intent.action.SEND"

    .line 421
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "com.google.android.apps.plus.SENDER_ID"

    .line 422
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 424
    const-string v1, "com.google.android.apps.plus.RECIPIENT_IDS"

    .line 425
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 426
    const-string v1, "com.google.android.apps.plus.RECIPIENT_DISPLAY_NAMES"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 428
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v4, v1

    .line 430
    :goto_1
    if-eqz v6, :cond_6

    if-eqz v4, :cond_6

    .line 431
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v4, v1, :cond_6

    .line 432
    new-instance v7, Ljava/util/ArrayList;

    .line 433
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 436
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->C:Lieh;

    sget-object v3, Lkmx;->a:Lief;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    .line 437
    invoke-interface {v8}, Lhee;->d()I

    move-result v8

    .line 436
    invoke-interface {v1, v3, v8}, Lieh;->c(Lief;I)Ljava/lang/Long;

    move-result-object v8

    move v3, v0

    .line 438
    :goto_2
    if-ge v3, v4, :cond_4

    if-eqz v8, :cond_2

    int-to-long v0, v3

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v0, v0, v10

    if-gez v0, :cond_4

    .line 439
    :cond_2
    new-instance v9, Ljqs;

    .line 440
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v9, v0, v1, v2}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v4, v0

    .line 428
    goto :goto_1

    .line 443
    :cond_4
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v4, v0, :cond_5

    .line 444
    const-string v0, "ShareboxActivity"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 445
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4e

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Only "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " recipients can be prepopulated in a post.  Some recipients were removed."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    :cond_5
    new-instance v0, Lhgw;

    invoke-direct {v0, v7, v2}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_6
    move-object v0, v2

    .line 453
    goto/16 :goto_0
.end method

.method public l()Landroid/widget/BaseAdapter;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->A:Lemx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->A:Lemx;

    instance-of v0, v0, Lemx;

    if-nez v0, :cond_1

    .line 232
    :cond_0
    new-instance v0, Lemx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    .line 233
    invoke-virtual {v1}, Ljbx;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v4

    move-object v1, p0

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lemx;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->A:Lemx;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->A:Lemx;

    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    .line 234
    invoke-static {v1, v2, v3}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->a(Landroid/view/WindowManager;Landroid/content/res/Resources;Z)I

    move-result v1

    invoke-virtual {v0, v1}, Lemx;->a(I)V

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->A:Lemx;

    return-object v0
.end method

.method protected m()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h:Lhoc;

    const-string v2, "CreatePostTask"

    .line 459
    invoke-virtual {v0, v2}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h:Lhoc;

    const-string v2, "ReshareTask"

    .line 460
    invoke-virtual {v0, v2}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 479
    :goto_0
    return v0

    .line 464
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->E()Lhgw;

    move-result-object v0

    .line 465
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lhgw;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 466
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->D()V

    move v0, v1

    .line 467
    goto :goto_0

    .line 470
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->d(Lhgw;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 471
    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->B:Z

    .line 472
    invoke-virtual {v0, v1}, Lhgw;->c(I)Lkxr;

    move-result-object v2

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->x:Llnh;

    const-class v3, Lksu;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lksu;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    .line 474
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-interface {v0, v3, v2}, Lksu;->a(ILkxr;)Landroid/content/Intent;

    move-result-object v0

    .line 475
    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    .line 476
    goto :goto_0

    .line 479
    :cond_4
    invoke-super {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->m()Z

    move-result v0

    goto :goto_0
.end method

.method protected n()V
    .locals 4

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->w()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 485
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->E()Lhgw;

    move-result-object v1

    .line 492
    const/4 v0, 0x0

    .line 493
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->j:Z

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lhgw;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 494
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->i:Lhgw;

    .line 497
    :cond_3
    new-instance v2, Lkjc;

    invoke-direct {v2}, Lkjc;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    .line 498
    invoke-virtual {v3}, Lkki;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkjc;->c(Ljava/lang/String;)Lkjc;

    move-result-object v2

    .line 499
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->I()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkjc;->d(Ljava/lang/String;)Lkjc;

    move-result-object v2

    .line 500
    invoke-virtual {v2, v1}, Lkjc;->a(Lhgw;)Lkjc;

    move-result-object v1

    .line 501
    invoke-virtual {v1, v0}, Lkjc;->b(Lhgw;)Lkjc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->v:[B

    .line 502
    invoke-virtual {v0, v1}, Lkjc;->a([B)Lkjc;

    move-result-object v0

    .line 503
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h:Lhoc;

    new-instance v2, Lklw;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    .line 504
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v2, p0, v3, v0}, Lklw;-><init>(Landroid/content/Context;ILkjc;)V

    .line 503
    invoke-virtual {v1, v2}, Lhoc;->c(Lhny;)V

    .line 506
    sget-object v0, Lhmv;->Z:Lhmv;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lhmv;)V

    .line 507
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->B:Z

    goto :goto_0
.end method

.method protected o()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 513
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 565
    :goto_0
    return-void

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 519
    sget-object v0, Lhmv;->dB:Lhmv;

    .line 521
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lhmv;)V

    .line 523
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->E()Lhgw;

    move-result-object v5

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->x:Llnh;

    const-class v1, Lkly;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkly;

    invoke-virtual {v0}, Lkly;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_plus_page"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->h()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->k()I

    move-result v3

    move v1, v8

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    iget-object v0, v0, Ljbx;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizr;

    invoke-interface {v0}, Lizr;->f()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v4

    sget-object v6, Ljac;->b:Ljac;

    if-ne v4, v6, :cond_1

    invoke-virtual {v0}, Lizu;->j()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v7

    :goto_2
    if-eqz v0, :cond_3

    .line 526
    const v0, 0x7f0a0b0f

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 527
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 525
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v8

    goto :goto_2

    .line 531
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->G()Z

    move-result v0

    if-nez v0, :cond_4

    .line 532
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 533
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 537
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->I()Ljava/lang/String;

    move-result-object v3

    .line 538
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v7

    .line 540
    :goto_3
    invoke-virtual {p0, p0, v0, v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Landroid/app/Activity;ZLjava/lang/String;)V

    .line 542
    invoke-virtual {p0, v3, v5}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Ljava/lang/String;Lhgw;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 544
    sget-object v0, Lhmv;->ec:Lhmv;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lhmv;)V

    .line 547
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->q()Lkjd;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    .line 548
    invoke-virtual {v0}, Lkki;->q()Lkjd;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    .line 549
    invoke-virtual {v1}, Lkki;->r()Loya;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->k:Ljava/lang/String;

    move-object v1, p0

    .line 548
    invoke-virtual/range {v0 .. v6}, Lkjd;->a(Landroid/content/Context;ILjava/lang/String;Loya;Lhgw;Ljava/lang/String;)Lhny;

    move-result-object v0

    .line 551
    :goto_4
    if-eqz v0, :cond_8

    .line 552
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 564
    :goto_5
    iput-boolean v8, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->B:Z

    goto/16 :goto_0

    :cond_6
    move v0, v8

    .line 538
    goto :goto_3

    .line 548
    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    .line 554
    :cond_8
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->c(Lhgw;)Lkjc;

    move-result-object v1

    .line 555
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->D:Z

    if-eqz v0, :cond_a

    new-instance v0, Lklk;

    invoke-direct {v0}, Lklk;-><init>()V

    .line 557
    :goto_6
    new-instance v2, Lkkn;

    invoke-direct {v2, v1, v0}, Lkkn;-><init>(Lkjc;Lkkm;)V

    .line 558
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->D:Z

    if-eqz v0, :cond_b

    :cond_9
    :goto_7
    if-eqz v7, :cond_d

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h:Lhoc;

    invoke-virtual {v0, v2}, Lhoc;->c(Lhny;)V

    goto :goto_5

    .line 555
    :cond_a
    new-instance v0, Lkmg;

    invoke-direct {v0}, Lkmg;-><init>()V

    goto :goto_6

    .line 558
    :cond_b
    invoke-virtual {v1}, Lkjc;->q()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v1}, Lkjc;->q()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v7

    :goto_8
    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->x:Llnh;

    const-class v3, Lkly;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkly;

    invoke-virtual {v0, v1, v8}, Lkly;->b(IZ)Z

    move-result v0

    if-eqz v0, :cond_9

    move v7, v8

    goto :goto_7

    :cond_c
    move v0, v8

    goto :goto_8

    .line 561
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h:Lhoc;

    invoke-virtual {v0, v2}, Lhoc;->b(Lhny;)V

    goto :goto_5
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 622
    packed-switch p1, :pswitch_data_0

    .line 635
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 637
    if-ne p2, v4, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->B:Z

    if-eqz v0, :cond_1

    .line 638
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->H()V

    .line 640
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->B:Z

    .line 641
    return-void

    .line 624
    :pswitch_0
    if-ne p2, v4, :cond_0

    .line 625
    const-string v0, "extra_acl"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 626
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->d(Lhgw;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 627
    invoke-virtual {v0, v3}, Lhgw;->c(I)Lkxr;

    move-result-object v1

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->x:Llnh;

    const-class v2, Lksu;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lksu;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    .line 629
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-interface {v0, v2, v1}, Lksu;->a(ILkxr;)Landroid/content/Intent;

    move-result-object v0

    .line 630
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 631
    const v0, 0x7f050023

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 622
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 165
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->onCreate(Landroid/os/Bundle;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->x:Llnh;

    const-class v1, Ljbt;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbt;

    const v1, 0x7f100050

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->E:Lhkd;

    invoke-interface {v0, v1, v2}, Ljbt;->a(ILhkd;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 170
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->c(Landroid/content/Intent;)Lhgw;

    move-result-object v1

    .line 171
    if-eqz v1, :cond_7

    .line 172
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->b(Lhgw;)V

    .line 178
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->N()V

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h:Lhoc;

    new-instance v1, Lhpf;

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f()Lae;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lhpf;-><init>(Landroid/content/Context;Lae;)V

    .line 183
    invoke-virtual {v0, v1}, Lhoc;->a(Lhos;)V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->g()Lbb;

    move-result-object v0

    .line 188
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->z:Lfef;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    invoke-virtual {v1}, Lkki;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 190
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->z:Lfef;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v2, Llkt;->a:Llkt;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llkt;Z)V

    .line 193
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s:Ljbx;

    invoke-virtual {v1}, Ljbx;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 194
    const/4 v1, 0x4

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->z:Lfef;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->k()Lhyt;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 198
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    .line 202
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    invoke-virtual {v1}, Lkki;->k()Lhyt;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Loya;

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->x:Llnh;

    const-class v2, Lkjd;

    invoke-virtual {v1, v2}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkjd;

    .line 204
    invoke-virtual {v1, v0}, Lkjd;->a(Loya;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 205
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t:Lkki;

    invoke-virtual {v2, v1, v0}, Lkki;->a(Lkjd;Loya;)V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->v()V

    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->z()V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h:Lhoc;

    invoke-virtual {v0, v1}, Lhoc;->a(Lhob;)Lhoc;

    .line 214
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->u:Lkkr;

    invoke-virtual {v0}, Lkkr;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->u:Lkkr;

    .line 216
    invoke-virtual {v0}, Lkkr;->e()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->u:Lkkr;

    .line 217
    invoke-virtual {v1}, Lkkr;->c()Lkng;

    move-result-object v1

    .line 215
    invoke-static {v0, v1}, Lknb;->a(Landroid/os/Bundle;Lkng;)Lkmz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lkmz;)V

    .line 219
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->C()V

    .line 221
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->C:Lieh;

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->C:Lieh;

    sget-object v1, Ljhj;->a:Lief;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    .line 224
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->D:Z

    .line 226
    return-void

    .line 173
    :cond_7
    const-string v1, "extra_acl"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 175
    const-string v1, "extra_acl"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->b(Lhgw;)V

    goto/16 :goto_0
.end method

.method protected p()V
    .locals 3

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 646
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f:Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    .line 648
    const-string v2, "is_dasher_account"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "is_default_restricted"

    .line 649
    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 650
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->w:Lkmv;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->j:Z

    invoke-virtual {v1, v0, v2}, Lkmv;->a(IZ)V

    .line 652
    :cond_0
    return-void
.end method

.method protected q()V
    .locals 2

    .prologue
    .line 656
    new-instance v0, Lkoe;

    const/16 v1, 0x54

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    invoke-virtual {v0, p0}, Lkoe;->a(Landroid/content/Context;)V

    .line 657
    return-void
.end method

.method protected r()V
    .locals 2

    .prologue
    .line 661
    new-instance v0, Lkoe;

    const/16 v1, 0x55

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    .line 662
    invoke-virtual {v0, p0}, Lkoe;->a(Landroid/content/Context;)V

    .line 663
    return-void
.end method

.method protected s()V
    .locals 2

    .prologue
    .line 667
    new-instance v0, Lkoe;

    const/16 v1, 0x56

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    .line 668
    invoke-virtual {v0, p0}, Lkoe;->a(Landroid/content/Context;)V

    .line 669
    return-void
.end method
