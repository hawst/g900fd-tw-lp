.class public Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 41
    invoke-direct {p0}, Lloa;-><init>()V

    .line 46
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 48
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->x:Llnh;

    .line 49
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 51
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->x:Llnh;

    .line 52
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 55
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 56
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 58
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->x:Llnh;

    .line 59
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 61
    new-instance v0, Ldxn;

    new-instance v1, Lfnf;

    invoke-direct {v1, p0}, Lfnf;-><init>(Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;)V

    invoke-direct {v0, v1}, Ldxn;-><init>(Ldxm;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->x:Llnh;

    .line 77
    invoke-virtual {v0, v1}, Ldxn;->a(Llnh;)V

    .line 80
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lhmw;->G:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 120
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 106
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 107
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 108
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 109
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 97
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 98
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 87
    if-nez p1, :cond_0

    .line 88
    new-instance v0, Lkul;

    invoke-direct {v0}, Lkul;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 91
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareStreamActivity;->setContentView(I)V

    .line 92
    return-void
.end method
