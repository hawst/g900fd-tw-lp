.class public Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field public final e:Lhee;

.field private final f:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 41
    invoke-direct {p0}, Lloa;-><init>()V

    .line 46
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 47
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 49
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->x:Llnh;

    .line 50
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 52
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->x:Llnh;

    .line 53
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 54
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 56
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 57
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 60
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->x:Llnh;

    .line 61
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 62
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->e:Lhee;

    .line 64
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lhmw;->al:Lhmw;

    return-object v0
.end method

.method public a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->e:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 84
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 69
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 72
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 74
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 76
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsm;

    new-instance v2, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    .line 77
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 79
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 93
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 94
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 95
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 96
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 115
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x2

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 100
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 102
    if-nez p1, :cond_0

    .line 103
    new-instance v0, Lecf;

    invoke-direct {v0}, Lecf;-><init>()V

    .line 104
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 105
    const-string v2, "local_folders_only"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    invoke-virtual {v0, v1}, Lu;->f(Landroid/os/Bundle;)V

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->f:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 109
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostAllFoldersTileActivity;->setContentView(I)V

    .line 110
    return-void
.end method
