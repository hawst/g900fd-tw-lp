.class public Lcom/google/android/apps/plus/views/OneProfileHeader;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lljh;


# static fields
.field private static a:Lnjp;

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:I

.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:Landroid/graphics/drawable/Drawable;

.field private static h:Landroid/graphics/drawable/Drawable;

.field private static i:Landroid/content/res/ColorStateList;

.field private static j:I

.field private static k:Lizs;

.field private static l:Landroid/graphics/drawable/Drawable;

.field private static m:I


# instance fields
.field private A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Z

.field private F:Landroid/widget/TextView;

.field private G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

.field private H:Landroid/widget/TextView;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/widget/ImageView;

.field private K:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

.field private L:Landroid/view/View;

.field private M:Landroid/widget/TextView;

.field private N:Landroid/widget/TextView;

.field private O:I

.field private n:Lljc;

.field private o:Lkda;

.field private p:Lkdd;

.field private q:Lfyz;

.field private r:Lfys;

.field private s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

.field private w:Ljava/lang/String;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 153
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 97
    new-instance v0, Lfyw;

    invoke-direct {v0, p0}, Lfyw;-><init>(Lcom/google/android/apps/plus/views/OneProfileHeader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->n:Lljc;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->O:I

    .line 165
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->k:Lizs;

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->k:Lizs;

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 169
    const v1, 0x7f0d0334

    .line 170
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->c:I

    .line 171
    const v1, 0x7f0d0335

    .line 172
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->d:I

    .line 173
    const v1, 0x7f0d032d

    .line 174
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->e:I

    .line 175
    const v1, 0x7f0d0336

    .line 176
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->m:I

    .line 177
    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->i:Landroid/content/res/ColorStateList;

    .line 178
    const v1, 0x7f0d032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->b:I

    .line 179
    const v1, 0x7f0b0141

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->j:I

    .line 180
    const v1, 0x7f0203ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->f:Landroid/graphics/drawable/Drawable;

    .line 181
    const v1, 0x7f02032d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->g:Landroid/graphics/drawable/Drawable;

    .line 182
    const v1, 0x7f020279

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->h:Landroid/graphics/drawable/Drawable;

    .line 184
    new-instance v0, Lnjp;

    invoke-direct {v0}, Lnjp;-><init>()V

    .line 185
    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->b:Ljava/lang/Float;

    .line 186
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->a:Ljava/lang/Float;

    .line 187
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->d:Ljava/lang/Float;

    .line 188
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->c:Ljava/lang/Float;

    .line 154
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 157
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 97
    new-instance v0, Lfyw;

    invoke-direct {v0, p0}, Lfyw;-><init>(Lcom/google/android/apps/plus/views/OneProfileHeader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->n:Lljc;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->O:I

    .line 165
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->k:Lizs;

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->k:Lizs;

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 169
    const v1, 0x7f0d0334

    .line 170
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->c:I

    .line 171
    const v1, 0x7f0d0335

    .line 172
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->d:I

    .line 173
    const v1, 0x7f0d032d

    .line 174
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->e:I

    .line 175
    const v1, 0x7f0d0336

    .line 176
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->m:I

    .line 177
    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->i:Landroid/content/res/ColorStateList;

    .line 178
    const v1, 0x7f0d032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->b:I

    .line 179
    const v1, 0x7f0b0141

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->j:I

    .line 180
    const v1, 0x7f0203ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->f:Landroid/graphics/drawable/Drawable;

    .line 181
    const v1, 0x7f02032d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->g:Landroid/graphics/drawable/Drawable;

    .line 182
    const v1, 0x7f020279

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->h:Landroid/graphics/drawable/Drawable;

    .line 184
    new-instance v0, Lnjp;

    invoke-direct {v0}, Lnjp;-><init>()V

    .line 185
    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->b:Ljava/lang/Float;

    .line 186
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->a:Ljava/lang/Float;

    .line 187
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->d:Ljava/lang/Float;

    .line 188
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->c:Ljava/lang/Float;

    .line 158
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 161
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    new-instance v0, Lfyw;

    invoke-direct {v0, p0}, Lfyw;-><init>(Lcom/google/android/apps/plus/views/OneProfileHeader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->n:Lljc;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->O:I

    .line 165
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->k:Lizs;

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->k:Lizs;

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 169
    const v1, 0x7f0d0334

    .line 170
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->c:I

    .line 171
    const v1, 0x7f0d0335

    .line 172
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->d:I

    .line 173
    const v1, 0x7f0d032d

    .line 174
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->e:I

    .line 175
    const v1, 0x7f0d0336

    .line 176
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->m:I

    .line 177
    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->i:Landroid/content/res/ColorStateList;

    .line 178
    const v1, 0x7f0d032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->b:I

    .line 179
    const v1, 0x7f0b0141

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->j:I

    .line 180
    const v1, 0x7f0203ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->f:Landroid/graphics/drawable/Drawable;

    .line 181
    const v1, 0x7f02032d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->g:Landroid/graphics/drawable/Drawable;

    .line 182
    const v1, 0x7f020279

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->h:Landroid/graphics/drawable/Drawable;

    .line 184
    new-instance v0, Lnjp;

    invoke-direct {v0}, Lnjp;-><init>()V

    .line 185
    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->b:Ljava/lang/Float;

    .line 186
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->a:Ljava/lang/Float;

    .line 187
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->d:Ljava/lang/Float;

    .line 188
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lnjp;->c:Ljava/lang/Float;

    .line 162
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/OneProfileHeader;Lfys;)Lfys;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->r:Lfys;

    return-object p1
.end method

.method private a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->r:Lfys;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    new-instance v1, Lfyy;

    invoke-direct {v1, p0}, Lfyy;-><init>(Lcom/google/android/apps/plus/views/OneProfileHeader;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->a(Lfys;)V

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->invalidate()V

    .line 509
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->requestLayout()V

    .line 511
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 512
    return-void
.end method

.method private a(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->e(Landroid/graphics/drawable/Drawable;)V

    .line 304
    return-void

    .line 302
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020416

    .line 303
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/OneProfileHeader;Landroid/graphics/drawable/Drawable;Z)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Landroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/OneProfileHeader;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->E:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/OneProfileHeader;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->B:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/views/OneProfileHeader;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->C:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/views/OneProfileHeader;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/views/OneProfileHeader;)Lfys;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->r:Lfys;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/plus/views/OneProfileHeader;)Lcom/google/android/apps/plus/views/OneProfileAvatarView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    return-object v0
.end method

.method public static synthetic i()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->f:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static synthetic j()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->g:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static synthetic k()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->h:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static synthetic l()Lizs;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->k:Lizs;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->a(I)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->a(Lizu;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->p:Lkdd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->o:Lkda;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->o:Lkda;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->p:Lkdd;

    invoke-virtual {v0, v1}, Lkda;->unregister(Lkdd;)V

    .line 199
    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->p:Lkdd;

    .line 200
    iput-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->o:Lkda;

    .line 201
    iput-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->w:Ljava/lang/String;

    .line 202
    iput-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    .line 203
    iput-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->K:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a()V

    .line 205
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->K:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(I)V

    .line 289
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->K:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->K:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V

    .line 294
    return-void
.end method

.method public a(Lfys;)V
    .locals 0

    .prologue
    .line 813
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->r:Lfys;

    .line 814
    return-void
.end method

.method public a(Lfyz;Llhl;)V
    .locals 1

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->K:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(Llhl;)V

    .line 285
    return-void
.end method

.method public a(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 618
    if-eqz p1, :cond_0

    .line 619
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->z:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a(I)V

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->z:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->setVisibility(I)V

    .line 624
    :goto_0
    return-void

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->z:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v10, 0x1

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 708
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 709
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v3

    .line 712
    if-eqz p1, :cond_5

    .line 713
    invoke-virtual {v3, p1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 714
    const v4, 0x7f110013

    .line 715
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v1, v6, v8

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 716
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 720
    :goto_0
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 721
    invoke-virtual {v3, p2}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 722
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    const v0, 0x7fffffff

    .line 723
    :goto_1
    const v4, 0x7f110014

    new-array v5, v10, [Ljava/lang/Object;

    aput-object v3, v5, v8

    .line 724
    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 725
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 728
    :cond_0
    if-eqz v1, :cond_2

    .line 729
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->H:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 730
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->H:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 735
    :goto_2
    if-eqz v0, :cond_3

    .line 736
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->I:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 737
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->I:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 742
    :goto_3
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->J:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 747
    :goto_4
    return-void

    .line 722
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Long;->intValue()I

    move-result v0

    goto :goto_1

    .line 732
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->H:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 739
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->I:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 745
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->J:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->u:Ljava/lang/String;

    .line 308
    return-void
.end method

.method public a(Ljava/lang/String;ILhmn;)V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->K:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v1, 0x7f04021c

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;ILhmn;)V

    .line 298
    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 3

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    :goto_0
    return-void

    .line 391
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->d()V

    goto :goto_0

    .line 396
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->e()V

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->a(Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v1, p1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->a(Lizu;)V

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->j(I)V

    .line 401
    if-eqz p4, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    move-object v0, p0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    :goto_0
    return-void

    .line 410
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->d()V

    goto :goto_0

    .line 415
    :cond_1
    iput-object p2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->e()V

    .line 417
    iget-object v7, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v6, Ljac;->a:Ljac;

    move-object v1, p1

    move-object v5, v4

    invoke-static/range {v0 .. v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->a(Lizu;)V

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->j(I)V

    .line 420
    if-eqz p4, :cond_2

    :goto_1
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    move-object v4, p0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    :goto_0
    return-void

    .line 373
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->d()V

    goto :goto_0

    .line 378
    :cond_1
    iput-object p2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->d()V

    .line 380
    iget-object v7, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    if-eqz p3, :cond_2

    sget-object v6, Ljac;->d:Ljac;

    :goto_1
    move-object v1, p1

    move-object v5, v4

    invoke-static/range {v0 .. v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->a(Lizu;)V

    .line 382
    if-eqz p4, :cond_3

    :goto_2
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 380
    :cond_2
    sget-object v6, Ljac;->a:Ljac;

    goto :goto_1

    :cond_3
    move-object v4, p0

    .line 382
    goto :goto_2
.end method

.method public a(Ljava/lang/String;Lnjp;IZZ)V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    :goto_0
    return-void

    .line 356
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->d()V

    goto :goto_0

    .line 361
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->t:Ljava/lang/String;

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->d()V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->a(Ljava/lang/String;Lnjp;IZ)V

    .line 364
    if-eqz p5, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    move-object v0, p0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;ZZZ)V
    .locals 9

    .prologue
    const/16 v8, 0x11

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 574
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 575
    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 577
    if-eqz p2, :cond_0

    .line 578
    const-string v0, "\u00a0\u00a0"

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 579
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 580
    new-instance v3, Lfza;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0203b1

    invoke-direct {v3, v4, v5}, Lfza;-><init>(Landroid/content/Context;I)V

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v1, v3, v0, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 584
    :cond_0
    if-eqz p3, :cond_1

    .line 585
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 588
    if-eqz p4, :cond_2

    .line 589
    const v0, 0x7f020319

    .line 590
    const v4, 0x7f0a02ec

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    :goto_0
    const-string v3, "\u00a0\u00a0"

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 599
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 600
    new-instance v4, Lfza;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Lfza;-><init>(Landroid/content/Context;I)V

    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v1, v4, v3, v0, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 604
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 605
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->x:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 606
    return-void

    .line 593
    :cond_2
    const v0, 0x7f0202e2

    .line 594
    const v4, 0x7f0a02ed

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/util/List;)V

    .line 675
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 676
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->b(Z)V

    .line 677
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->F:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 678
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 428
    iput-object v3, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->w:Ljava/lang/String;

    .line 430
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->l:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 431
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 432
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lhss;->e(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    sput-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->l:Landroid/graphics/drawable/Drawable;

    .line 435
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->l:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 437
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 609
    if-eqz p1, :cond_0

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->y:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->y:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 615
    :goto_0
    return-void

    .line 613
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->y:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 627
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->E:Z

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->n:Lljc;

    invoke-interface {v0}, Lljc;->a()V

    .line 629
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->n:Lljc;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a(Lljc;)V

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a(Z)V

    .line 631
    return-void
.end method

.method public a(ZZLjava/lang/String;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 750
    if-eqz p1, :cond_1

    .line 751
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->L:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 752
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->M:Landroid/widget/TextView;

    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 753
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->N:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 757
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 753
    goto :goto_0

    .line 755
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->L:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->a(I)V

    .line 425
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 346
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;Lnjp;IZZ)V

    .line 348
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 516
    return-void

    .line 515
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 311
    iget v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->O:I

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 320
    :goto_0
    return v0

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 316
    const/4 v3, 0x3

    const v4, 0x7f100454

    invoke-virtual {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 317
    sget v3, Lcom/google/android/apps/plus/views/OneProfileHeader;->e:I

    sget v4, Lcom/google/android/apps/plus/views/OneProfileHeader;->c:I

    sget v5, Lcom/google/android/apps/plus/views/OneProfileHeader;->e:I

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 319
    iput v2, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->O:I

    move v0, v2

    .line 320
    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->w:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    :goto_0
    return-void

    .line 444
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 445
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Z)V

    goto :goto_0

    .line 449
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->w:Ljava/lang/String;

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 451
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 452
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 453
    new-instance v0, Lfyx;

    invoke-direct {v0, p0}, Lfyx;-><init>(Lcom/google/android/apps/plus/views/OneProfileHeader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->p:Lkdd;

    .line 493
    sget-object v0, Lcom/google/android/apps/plus/views/OneProfileHeader;->k:Lizs;

    .line 494
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v4, Ljac;->a:Ljac;

    invoke-static {v1, p1, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->p:Lkdd;

    .line 493
    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->o:Lkda;

    goto :goto_0
.end method

.method public c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 661
    if-eqz p1, :cond_0

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->i:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    .line 664
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020118

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 663
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 665
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 671
    :goto_0
    return-void

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 668
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public c()Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 324
    iget v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->O:I

    if-ne v0, v5, :cond_0

    move v0, v1

    .line 333
    :goto_0
    return v0

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 329
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 330
    sget v2, Lcom/google/android/apps/plus/views/OneProfileHeader;->e:I

    sget v3, Lcom/google/android/apps/plus/views/OneProfileHeader;->d:I

    sget v4, Lcom/google/android/apps/plus/views/OneProfileHeader;->e:I

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 332
    iput v5, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->O:I

    .line 333
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->u:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 338
    const-string v0, "https://lh6.googleusercontent.com/-5vG8ole8nAI/UYFKqb0Y7YI/AAAAAAAABiA/YQzKopOzN1g/w0-h0/default_cover_1_c07bbaef481e775be41b71cecbb5cd60.jpg"

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->u:Ljava/lang/String;

    .line 341
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->u:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/OneProfileHeader;->a:Lnjp;

    const/4 v5, 0x1

    move-object v0, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;Lnjp;IZZ)V

    .line 343
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 634
    if-eqz p1, :cond_0

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->B:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->B:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 640
    :goto_0
    return-void

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->B:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public d(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 681
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/util/List;)V

    .line 682
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 683
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f0a0821

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->b(Z)V

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->F:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 688
    return-void

    .line 683
    :cond_0
    const v0, 0x7f0a0820

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->F:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 693
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 643
    if-eqz p1, :cond_0

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->C:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->C:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 649
    :goto_0
    return-void

    .line 647
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->C:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 809
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/OneProfileAvatarView;->setVisibility(I)V

    .line 810
    return-void

    .line 809
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 696
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->b(Z)V

    .line 697
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Z)V

    .line 698
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 699
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->F:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 700
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 652
    if-eqz p1, :cond_0

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 658
    :goto_0
    return-void

    .line 656
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 703
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 704
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 705
    return-void
.end method

.method public h()Lcom/google/android/apps/plus/views/OneProfileAvatarView;
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    if-nez v0, :cond_1

    .line 776
    :cond_0
    :goto_0
    return-void

    .line 765
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 766
    const v1, 0x7f100454

    if-ne v1, v0, :cond_2

    .line 767
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    invoke-interface {v0}, Lfyz;->X()V

    goto :goto_0

    .line 768
    :cond_2
    const v1, 0x7f100174

    if-ne v1, v0, :cond_3

    .line 769
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    invoke-interface {v0}, Lfyz;->aa_()V

    goto :goto_0

    .line 770
    :cond_3
    const v1, 0x7f10045a

    if-ne v1, v0, :cond_4

    .line 772
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    invoke-interface {v0}, Lfyz;->ae()V

    goto :goto_0

    .line 773
    :cond_4
    const v1, 0x7f100461

    if-ne v1, v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    invoke-interface {v0}, Lfyz;->ac()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 210
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 211
    const v0, 0x7f100454

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->i(I)V

    .line 213
    const v0, 0x7f100174

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->v:Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    .line 214
    const v0, 0x7f10013d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->x:Landroid/widget/TextView;

    .line 215
    const v0, 0x7f100456

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->y:Landroid/widget/TextView;

    .line 216
    const v0, 0x7f100419

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->z:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    .line 217
    const v0, 0x7f100457

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    sget v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    const v1, 0x7f100458

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->B:Landroid/widget/TextView;

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    const v1, 0x7f100459

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->C:Landroid/widget/TextView;

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    const v1, 0x7f10045a

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->D:Landroid/widget/TextView;

    .line 222
    const v0, 0x7f100460

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->F:Landroid/widget/TextView;

    .line 223
    const v0, 0x7f100461

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->G:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 226
    const v0, 0x7f10045c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->H:Landroid/widget/TextView;

    .line 227
    const v0, 0x7f10045e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->I:Landroid/widget/TextView;

    .line 228
    const v0, 0x7f10045d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->J:Landroid/widget/ImageView;

    .line 229
    const v0, 0x7f100462

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->K:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    .line 230
    const v0, 0x7f100463

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->L:Landroid/view/View;

    .line 231
    const v0, 0x7f100464

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->M:Landroid/widget/TextView;

    .line 232
    const v0, 0x7f100465

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->N:Landroid/widget/TextView;

    .line 234
    sget-object v0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->e:Lloy;

    .line 235
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Z)V

    .line 239
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    if-eqz v0, :cond_0

    .line 781
    const v0, 0x7f100461

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->q:Lfyz;

    invoke-interface {v0}, Lfyz;->ad()Z

    move-result v0

    .line 785
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 790
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileHeader;->s:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->i()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/OneProfileHeader;->m:I

    if-le v0, v1, :cond_1

    .line 794
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->b()Z

    move-result v0

    .line 799
    :goto_0
    if-eqz v0, :cond_0

    .line 800
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 802
    :cond_0
    return-void

    .line 796
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->c()Z

    move-result v0

    goto :goto_0
.end method
