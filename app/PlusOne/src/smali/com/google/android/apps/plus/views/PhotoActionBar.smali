.class public Lcom/google/android/apps/plus/views/PhotoActionBar;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:I


# instance fields
.field private b:Lfzl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 90
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->d()V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->d()V

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->d()V

    .line 101
    return-void
.end method

.method private a(Lfzk;)Landroid/view/View;
    .locals 2

    .prologue
    .line 259
    invoke-static {p1}, Lfzk;->a(Lfzk;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 260
    if-nez v0, :cond_0

    .line 261
    invoke-static {p1}, Lfzk;->b(Lfzk;)Lfzj;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->b(Lfzj;)Landroid/view/View;

    move-result-object v0

    .line 262
    invoke-static {p1}, Lfzk;->a(Lfzk;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 264
    :cond_0
    return-object v0
.end method

.method private a(ZLfzj;)V
    .locals 3

    .prologue
    .line 233
    invoke-static {p2}, Lfzj;->a(Lfzj;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 234
    if-eqz p1, :cond_2

    if-nez v0, :cond_2

    .line 235
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/PhotoActionBar;->c(Lfzj;)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 237
    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 238
    :cond_0
    return-void

    .line 237
    :cond_1
    const/16 v0, 0x8

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v2, v0

    goto :goto_0
.end method

.method private a(Lfzj;)Z
    .locals 1

    .prologue
    .line 217
    invoke-static {p1}, Lfzj;->a(Lfzj;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lfzj;)Landroid/view/View;
    .locals 1

    .prologue
    .line 268
    invoke-static {p1}, Lfzj;->a(Lfzj;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 269
    if-nez v0, :cond_0

    .line 270
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->c(Lfzj;)Landroid/view/View;

    move-result-object v0

    .line 272
    :cond_0
    return-object v0
.end method

.method private c(Lfzj;)Landroid/view/View;
    .locals 2

    .prologue
    .line 276
    invoke-static {p1}, Lfzj;->c(Lfzj;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 277
    invoke-static {p1}, Lfzj;->a(Lfzj;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 278
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 104
    sget v0, Lcom/google/android/apps/plus/views/PhotoActionBar;->a:I

    if-nez v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106
    const v1, 0x7f0d037c

    .line 107
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoActionBar;->a:I

    .line 109
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lfzk;->b:Lfzk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Lfzk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 153
    if-nez p1, :cond_0

    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    return-void

    .line 153
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public a(Lfzl;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    .line 113
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 168
    sget-object v0, Lfzj;->d:Lfzj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->b(Lfzj;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 169
    if-eqz p1, :cond_0

    const/high16 v1, 0x3f000000    # 0.5f

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 170
    return-void

    .line 169
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public a(ZI)V
    .locals 2

    .prologue
    .line 142
    sget-object v0, Lfzj;->c:Lfzj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->b(Lfzj;)Landroid/view/View;

    move-result-object v1

    .line 143
    if-eqz p1, :cond_0

    const v0, 0x7f02048b

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 146
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 147
    sget-object v0, Lfzk;->c:Lfzk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Lfzk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 148
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    return-void

    .line 143
    :cond_0
    const v0, 0x7f020489

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 180
    sget-object v0, Lfzj;->d:Lfzj;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZLfzj;)V

    .line 181
    sget-object v0, Lfzj;->d:Lfzj;

    invoke-static {v0}, Lfzj;->b(Lfzj;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 182
    :cond_0
    return-void

    .line 181
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lfzj;->a:Lfzj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Lfzj;)Z

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 157
    sget-object v0, Lfzk;->a:Lfzk;

    .line 158
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Lfzk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 159
    if-nez p1, :cond_0

    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    return-void

    .line 159
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 173
    sget-object v0, Lfzj;->b:Lfzj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->b(Lfzj;)Landroid/view/View;

    move-result-object v1

    .line 174
    if-eqz p1, :cond_0

    const v0, 0x7f02048c

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 177
    return-void

    .line 174
    :cond_0
    const v0, 0x7f020489

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lfzj;->c:Lfzj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Lfzj;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 284
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 285
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 283
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 287
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 163
    sget-object v0, Lfzk;->d:Lfzk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Lfzk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 164
    if-nez p1, :cond_0

    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    return-void

    .line 164
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lfzj;->c:Lfzj;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZLfzj;)V

    .line 186
    return-void
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lfzj;->e:Lfzj;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZLfzj;)V

    .line 190
    return-void
.end method

.method public e(Z)V
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lfzj;->b:Lfzj;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZLfzj;)V

    .line 194
    return-void
.end method

.method public f(Z)V
    .locals 1

    .prologue
    .line 197
    sget-object v0, Lfzj;->f:Lfzj;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZLfzj;)V

    .line 198
    return-void
.end method

.method public g(Z)V
    .locals 1

    .prologue
    .line 201
    sget-object v0, Lfzj;->a:Lfzj;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZLfzj;)V

    .line 202
    return-void
.end method

.method public h(Z)V
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lfzj;->g:Lfzj;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZLfzj;)V

    .line 223
    return-void
.end method

.method public i(Z)V
    .locals 2

    .prologue
    .line 226
    sget-object v0, Lfzj;->g:Lfzj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->b(Lfzj;)Landroid/view/View;

    move-result-object v1

    .line 227
    if-eqz p1, :cond_0

    const v0, 0x7f02048c

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 230
    return-void

    .line 227
    :cond_0
    const v0, 0x7f020489

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    if-nez v0, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    .line 123
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 124
    const v1, 0x7f100061

    if-ne v0, v1, :cond_2

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    invoke-interface {v0}, Lfzl;->a()V

    goto :goto_0

    .line 126
    :cond_2
    const v1, 0x7f1004a0

    if-ne v0, v1, :cond_3

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    invoke-interface {v0}, Lfzl;->b()V

    goto :goto_0

    .line 128
    :cond_3
    const v1, 0x7f1004a2

    if-ne v0, v1, :cond_4

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    invoke-interface {v0}, Lfzl;->c()V

    goto :goto_0

    .line 130
    :cond_4
    const v1, 0x7f1004a3

    if-ne v0, v1, :cond_5

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    invoke-interface {v0}, Lfzl;->d()V

    goto :goto_0

    .line 132
    :cond_5
    const v1, 0x7f10049c

    if-ne v0, v1, :cond_6

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    invoke-interface {v0}, Lfzl;->e()V

    goto :goto_0

    .line 134
    :cond_6
    const v1, 0x7f10049b

    if-ne v0, v1, :cond_7

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    invoke-interface {v0}, Lfzl;->f()V

    goto :goto_0

    .line 136
    :cond_7
    const v1, 0x7f10049e

    if-ne v0, v1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoActionBar;->b:Lfzl;

    invoke-interface {v0}, Lfzl;->g()V

    goto :goto_0
.end method
