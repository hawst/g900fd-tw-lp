.class public Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"

# interfaces
.implements Llke;


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/Paint;

.field private static c:Landroid/graphics/Paint;

.field private static d:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/16 v2, 0x25

    const/4 v3, 0x1

    .line 70
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->b(I)V

    .line 73
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->j(Z)V

    .line 74
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->g(Z)V

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-boolean v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->a:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->b:Landroid/graphics/Paint;

    const v2, 0x7f0b02df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->c:Landroid/graphics/Paint;

    const v2, 0x7f0b02de

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->d:Landroid/graphics/Rect;

    const v0, 0x7f0d0296

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    const v0, 0x7f0d0293

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    const v0, 0x7f0d0294

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    const v0, 0x7f0d0292

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    const v0, 0x7f0d0297

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    const v0, 0x7f0d0298

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    const v0, 0x7f02048f

    invoke-static {v1, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    const v0, 0x7f020191

    invoke-static {v1, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    const v0, 0x7f0205be

    invoke-static {v1, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    sput-boolean v3, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->a:Z

    .line 77
    :cond_0
    return-void
.end method


# virtual methods
.method public d()Z
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->isEnabled()Z

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    .line 122
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 123
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->onDraw(Landroid/graphics/Canvas;)V

    .line 124
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->d:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 134
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->d:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method
