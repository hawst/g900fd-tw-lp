.class public Lcom/google/android/apps/plus/fragments/EventLocationFragment;
.super Leam;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leam",
        "<",
        "Landroid/widget/ListView;",
        "Lhyd;",
        ">;",
        "Landroid/text/TextWatcher;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final P:[Ljava/lang/String;


# instance fields
.field private Q:Landroid/widget/EditText;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Liuh;

.field private U:D

.field private V:D

.field private W:Leaz;

.field private X:Liug;

.field private Y:Lilo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "location"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->P:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Leam;-><init>()V

    .line 435
    new-instance v0, Leax;

    invoke-direct {v0, p0}, Leax;-><init>(Lcom/google/android/apps/plus/fragments/EventLocationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->Y:Lilo;

    return-void
.end method

.method private U()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 227
    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 228
    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 229
    new-instance v1, Liuh;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Liuh;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->T:Liuh;

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->T:Liuh;

    goto :goto_0
.end method

.method private V()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->T:Liuh;

    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v5, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->n()Lz;

    move-result-object v0

    new-instance v1, Ldpa;

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->n()Lz;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->T:Liuh;

    invoke-direct {v1, v2, v3, v4, v5}, Ldpa;-><init>(Landroid/content/Context;ILiuh;Llae;)V

    .line 238
    invoke-static {v0, v1}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 243
    :cond_0
    return-void
.end method

.method private W()V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X:Liug;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X:Liug;

    invoke-virtual {v0}, Liug;->c()V

    .line 360
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X:Liug;

    .line 362
    :cond_0
    return-void
.end method

.method private X()I
    .locals 3

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 380
    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/EventLocationFragment;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->W()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/EventLocationFragment;DD)V
    .locals 9

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v8, v0, [F

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U:D

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V:D

    move-wide v4, p1

    move-wide v6, p3

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    const/high16 v1, 0x43480000    # 200.0f

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U:D

    iput-wide p3, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V:D

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "event.current.latitude"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U:D

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "event.current.longitude"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V:D

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V()V

    :cond_1
    return-void
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v1, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 246
    new-instance v3, Lhym;

    sget-object v0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->P:[Ljava/lang/String;

    invoke-direct {v3, v0}, Lhym;-><init>([Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v8

    .line 251
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v7

    const v2, 0x7f0a0943

    .line 252
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->e_(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const v1, 0x7f0a0944

    .line 253
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->e_(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    const/4 v1, 0x0

    aput-object v1, v0, v10

    .line 250
    invoke-virtual {v3, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 281
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->O:Lhyd;

    invoke-virtual {v0, v3}, Lhyd;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 282
    return-void

    .line 256
    :cond_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v8

    .line 257
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v7

    const v2, 0x7f0a0945

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    aput-object v5, v4, v8

    .line 258
    invoke-virtual {p0, v2, v4}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v2, 0x0

    aput-object v2, v0, v9

    const/4 v2, 0x0

    aput-object v2, v0, v10

    .line 256
    invoke-virtual {v3, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 262
    if-eqz p1, :cond_0

    .line 263
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 265
    :cond_2
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    .line 266
    invoke-static {v4}, Llae;->a([B)Llae;

    move-result-object v5

    .line 268
    if-eqz v5, :cond_3

    .line 269
    const/4 v2, 0x5

    new-array v6, v2, [Ljava/lang/Object;

    add-int/lit8 v2, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v8

    .line 270
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    .line 271
    invoke-virtual {v5}, Llae;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    .line 272
    invoke-virtual {v5}, Llae;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v9

    aput-object v4, v6, v10

    .line 269
    invoke-virtual {v3, v6}, Lhym;->a([Ljava/lang/Object;)V

    move v0, v2

    .line 276
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method private e()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 203
    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 447
    sget-object v0, Lhmw;->F:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 147
    const v0, 0x7f040099

    invoke-super {p0, p1, p2, p3, v0}, Leam;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 150
    new-instance v0, Leay;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->n()Lz;

    move-result-object v2

    invoke-direct {v0, v2}, Leay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->O:Lhyd;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->N:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->O:Lhyd;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->N:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 154
    const v0, 0x7f10025a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->Q:Landroid/widget/EditText;

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->Q:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->Q:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->R:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 158
    return-object v1
.end method

.method public bridge synthetic a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3, p4}, Leam;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->T:Liuh;

    if-nez v0, :cond_0

    const-string v0, "no_location_stream_key"

    .line 323
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X()I

    move-result v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsProvider;->a(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 324
    new-instance v0, Lhye;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->n()Lz;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "location"

    aput-object v6, v3, v5

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->T:Liuh;

    .line 322
    invoke-virtual {v0}, Liuh;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0, p1}, Leam;->a(Landroid/app/Activity;)V

    .line 135
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 137
    const-string v1, "event.current.latitude"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    const-string v1, "event.current.latitude"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U:D

    .line 139
    const-string v1, "event.current.longitude"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V:D

    .line 142
    :cond_0
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 330
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->b(Landroid/database/Cursor;)V

    .line 331
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 112
    invoke-super {p0, p1}, Leam;->a(Landroid/os/Bundle;)V

    .line 114
    if-eqz p1, :cond_0

    .line 115
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    .line 116
    const-string v0, "latitude"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U:D

    .line 117
    const-string v0, "longitude"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V:D

    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U()V

    .line 121
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 122
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 54
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Leaz;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->W:Leaz;

    .line 163
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->R:Ljava/lang/String;

    .line 211
    return-void
.end method

.method public aO_()V
    .locals 9

    .prologue
    .line 366
    invoke-super {p0}, Leam;->aO_()V

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X:Liug;

    if-nez v0, :cond_2

    new-instance v0, Liug;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->at:Llnl;

    const-wide/16 v2, 0xbb8

    const/4 v4, 0x0

    new-instance v5, Lfcw;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->at:Llnl;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->Y:Lilo;

    invoke-direct {v5, v6, v7, v8}, Lfcw;-><init>(Landroid/content/Context;ILilo;)V

    invoke-direct/range {v0 .. v5}, Liug;-><init>(Landroid/content/Context;JLandroid/location/Location;Lilo;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X:Liug;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->X:Liug;

    invoke-virtual {v0}, Liug;->b()V

    goto :goto_0
.end method

.method public bridge synthetic ae_()V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0}, Leam;->ae_()V

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 171
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 126
    invoke-super {p0, p1}, Leam;->e(Landroid/os/Bundle;)V

    .line 127
    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v0, "latitude"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U:D

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 129
    const-string v0, "longitude"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V:D

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 130
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const-wide v8, 0x416312d000000000L    # 1.0E7

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->O:Lhyd;

    invoke-virtual {v0, p3}, Lhyd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 288
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 290
    packed-switch v2, :pswitch_data_0

    .line 302
    const/4 v2, 0x4

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 303
    if-eqz v0, :cond_1

    .line 304
    invoke-static {v0}, Llae;->a([B)Llae;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Llae;->j()Lofq;

    move-result-object v2

    if-nez v2, :cond_3

    move-object v0, v1

    :cond_0
    :goto_0
    move-object v1, v0

    .line 312
    :cond_1
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->W:Leaz;

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->W:Leaz;

    invoke-interface {v0, v1}, Leaz;->a(Lpao;)V

    .line 317
    :cond_2
    return-void

    .line 296
    :pswitch_1
    new-instance v1, Lpao;

    invoke-direct {v1}, Lpao;-><init>()V

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    iput-object v0, v1, Lpao;->b:Ljava/lang/String;

    goto :goto_1

    .line 305
    :cond_3
    new-instance v0, Lpao;

    invoke-direct {v0}, Lpao;-><init>()V

    iget-object v1, v2, Lofq;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    iget-object v1, v2, Lofq;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    new-instance v1, Lozb;

    invoke-direct {v1}, Lozb;-><init>()V

    iget-object v3, v2, Lofq;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-double v4, v3

    div-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, v1, Lozb;->c:Ljava/lang/Double;

    iget-object v3, v2, Lofq;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-double v4, v3

    div-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, v1, Lozb;->d:Ljava/lang/Double;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    iput-object v3, v0, Lpao;->e:Loya;

    iget-object v3, v0, Lpao;->e:Loya;

    new-array v4, v6, [I

    const/16 v5, 0x157

    aput v5, v4, v7

    iput-object v4, v3, Loya;->b:[I

    iget-object v3, v0, Lpao;->e:Loya;

    sget-object v4, Lozb;->a:Loxr;

    invoke-virtual {v3, v4, v1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    :cond_4
    iget-object v1, v2, Lofq;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, v2, Lofq;->e:Ljava/lang/String;

    :goto_2
    if-eqz v1, :cond_0

    iput-object v1, v0, Lpao;->b:Ljava/lang/String;

    iput-object v1, v0, Lpao;->c:Ljava/lang/String;

    new-instance v1, Lpcg;

    invoke-direct {v1}, Lpcg;-><init>()V

    iget-object v2, v2, Lofq;->f:Ljava/lang/String;

    iput-object v2, v1, Lpcg;->c:Ljava/lang/String;

    new-instance v2, Loya;

    invoke-direct {v2}, Loya;-><init>()V

    iput-object v2, v0, Lpao;->d:Loya;

    iget-object v2, v0, Lpao;->d:Loya;

    new-array v3, v6, [I

    const/16 v4, 0x156

    aput v4, v3, v7

    iput-object v3, v2, Loya;->b:[I

    iget-object v2, v0, Lpao;->d:Loya;

    sget-object v3, Lpcg;->a:Loxr;

    invoke-virtual {v2, v3, v1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    iget-object v1, v2, Lofq;->f:Ljava/lang/String;

    goto :goto_2

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3, p4}, Leam;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Leam;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->Q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->S:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->b(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->U()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->V()V

    .line 176
    :cond_0
    return-void
.end method

.method public z()V
    .locals 0

    .prologue
    .line 373
    invoke-super {p0}, Leam;->z()V

    .line 375
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->W()V

    .line 376
    return-void
.end method
