.class public Lcom/google/android/apps/moviemaker/picker/PickerActivity;
.super Los;
.source "PG"


# instance fields
.field private e:Landroid/widget/GridView;

.field private f:Landroid/view/MenuItem;

.field private g:Landroid/graphics/Bitmap;

.field private h:Landroid/widget/SimpleCursorAdapter;

.field private i:Landroid/database/Cursor;

.field private j:Landroid/database/Cursor;

.field private k:Lbro;

.field private l:[Z

.field private m:Landroid/os/HandlerThread;

.field private n:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Los;-><init>()V

    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->l:[Z

    .line 273
    return-void

    .line 65
    :array_0
    .array-data 1
        0x0t
        0x1t
    .end array-data
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/picker/PickerActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->i:Landroid/database/Cursor;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->k()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/picker/PickerActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->j:Landroid/database/Cursor;

    return-object p1
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->j:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->i:Landroid/database/Cursor;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lbro;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/database/Cursor;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->i:Landroid/database/Cursor;

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->j:Landroid/database/Cursor;

    aput-object v3, v1, v2

    const-string v2, "datetaken"

    invoke-direct {v0, v1, v2, v4}, Lbro;-><init>([Landroid/database/Cursor;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->k:Lbro;

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h:Landroid/widget/SimpleCursorAdapter;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->k:Lbro;

    invoke-virtual {v0, v1}, Landroid/widget/SimpleCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->g:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->n:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)[Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->l:[Z

    return-object v0
.end method

.method private k()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->e:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getCheckedItemCount()I

    move-result v0

    .line 189
    if-lez v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h()Loo;

    move-result-object v1

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110002

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 190
    invoke-virtual {v1, v0}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->f:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->f:Landroid/view/MenuItem;

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h()Loo;

    move-result-object v0

    const v1, 0x7f0a00c5

    invoke-virtual {v0, v1}, Loo;->c(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->f:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->f:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 74
    invoke-super {p0, p1}, Los;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const v0, 0x7f040198

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->setContentView(I)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h()Loo;

    move-result-object v0

    .line 77
    invoke-virtual {v0, v6}, Loo;->b(Z)V

    .line 78
    invoke-virtual {v0, v8}, Loo;->d(Z)V

    .line 80
    const v0, 0x7f100304

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->e:Landroid/widget/GridView;

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202bc

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->g:Landroid/graphics/Bitmap;

    .line 86
    new-instance v0, Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0400be

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v4, v6

    new-array v5, v8, [I

    const v7, 0x7f100114

    aput v7, v5, v6

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h:Landroid/widget/SimpleCursorAdapter;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h:Landroid/widget/SimpleCursorAdapter;

    new-instance v1, Lbrt;

    invoke-direct {v1, p0}, Lbrt;-><init>(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SimpleCursorAdapter;->setViewBinder(Landroid/widget/SimpleCursorAdapter$ViewBinder;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->e:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v10}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v9}, Landroid/widget/GridView;->setChoiceMode(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->e:Landroid/widget/GridView;

    new-instance v1, Lbrq;

    invoke-direct {v1, p0}, Lbrq;-><init>(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 104
    new-array v0, v10, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, "_data"

    aput-object v1, v0, v8

    const-string v1, "datetaken"

    aput-object v1, v0, v9

    const/4 v1, 0x3

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    new-instance v2, Lbrr;

    invoke-direct {v2, p0, v0, v3}, Lbrr;-><init>(Lcom/google/android/apps/moviemaker/picker/PickerActivity;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v1, v6, v3, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    new-instance v2, Lbrs;

    invoke-direct {v2, p0, v0, v3}, Lbrs;-><init>(Lcom/google/android/apps/moviemaker/picker/PickerActivity;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v1, v8, v3, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 105
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120011

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 221
    const v0, 0x7f1006f5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->f:Landroid/view/MenuItem;

    .line 222
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->k()V

    .line 223
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->f:Landroid/view/MenuItem;

    if-ne p1, v0, :cond_3

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->e:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v0}, Landroid/widget/SimpleCursorAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->h:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/SimpleCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbro;

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->l:[Z

    invoke-virtual {v0}, Lbro;->a()I

    move-result v6

    aget-boolean v5, v5, v6

    if-eqz v5, :cond_1

    sget-object v5, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lbro;->getLong(I)J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lbro;->getLong(I)J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->finish()V

    .line 230
    const/4 v2, 0x1

    .line 233
    :cond_3
    return v2
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->m:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->m:Landroid/os/HandlerThread;

    .line 181
    invoke-super {p0}, Los;->onPause()V

    .line 182
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 170
    invoke-super {p0}, Los;->onResume()V

    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->k()V

    .line 172
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Tileloader"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->m:Landroid/os/HandlerThread;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->m:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 174
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->m:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->n:Landroid/os/Handler;

    .line 175
    return-void
.end method
