.class public Lcom/google/android/apps/plus/views/AvatarsView;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field private static a:[I

.field private static b:Ljava/lang/Integer;

.field private static c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->b:Ljava/lang/Integer;

    .line 32
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->c:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AvatarsView;->a(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AvatarsView;->a(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AvatarsView;->a(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method private a()I
    .locals 2

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->getChildCount()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/AvatarsView;->a:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private a(IIIII)V
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/AvatarsView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 180
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->a:[I

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 52
    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0d0149

    .line 53
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0d014a

    .line 54
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0d014b

    .line 55
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f0d014c

    .line 56
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    sput-object v1, Lcom/google/android/apps/plus/views/AvatarsView;->a:[I

    .line 58
    const v1, 0x7f0d014d

    .line 59
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AvatarsView;->b:Ljava/lang/Integer;

    .line 60
    const v1, 0x7f0d014e

    .line 61
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->c:Ljava/lang/Integer;

    .line 63
    :cond_0
    invoke-static {p0}, Llii;->g(Landroid/view/View;)V

    .line 64
    return-void
.end method

.method private a(Ldqv;IZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 82
    new-instance v3, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-virtual {v3, p2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 84
    invoke-virtual {v3, p3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c(Z)V

    .line 85
    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(Z)V

    .line 86
    iget-object v0, p1, Ldqv;->c:Ljava/lang/String;

    iget-object v4, p1, Ldqv;->d:Ljava/lang/String;

    .line 87
    invoke-static {v4}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 86
    invoke-virtual {v3, v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    if-eqz p4, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 89
    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 90
    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 91
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/AvatarsView;->addView(Landroid/view/View;)V

    .line 92
    return-void

    :cond_0
    move v0, v2

    .line 88
    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/List;ZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->removeAllViews()V

    .line 68
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 71
    if-ne v2, v3, :cond_1

    .line 72
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/google/android/apps/plus/views/AvatarsView;->a(Ldqv;IZZ)V

    .line 79
    :cond_0
    return-void

    :cond_1
    move v1, v0

    .line 74
    :goto_0
    if-ge v1, v2, :cond_0

    .line 75
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    invoke-direct {p0, v0, v3, p2, p3}, Lcom/google/android/apps/plus/views/AvatarsView;->a(Ldqv;IZZ)V

    .line 74
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 13

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->getMeasuredWidth()I

    move-result v11

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->getMeasuredHeight()I

    move-result v12

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->getChildCount()I

    move-result v0

    .line 116
    sget-object v1, Lcom/google/android/apps/plus/views/AvatarsView;->a:[I

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->a()I

    move-result v2

    aget v4, v1, v2

    .line 118
    packed-switch v0, :pswitch_data_0

    .line 147
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    .line 149
    const/4 v6, 0x1

    sub-int v7, v11, v4

    const/4 v8, 0x0

    move-object v5, p0

    move v9, v11

    move v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    .line 151
    const/4 v1, 0x2

    const/4 v2, 0x0

    sub-int v3, v12, v4

    move-object v0, p0

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    .line 153
    const/4 v1, 0x3

    sub-int v2, v11, v4

    sub-int v3, v12, v4

    move-object v0, p0

    move v4, v11

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    .line 157
    :goto_0
    return-void

    .line 120
    :pswitch_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    goto :goto_0

    .line 126
    :pswitch_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    .line 128
    const/4 v1, 0x1

    sub-int v2, v11, v4

    sub-int v3, v12, v4

    move-object v0, p0

    move v4, v11

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    goto :goto_0

    .line 134
    :pswitch_2
    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v3, v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->c:Ljava/lang/Integer;

    .line 135
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v5, v12, v0

    move-object v0, p0

    .line 134
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    .line 137
    const/4 v6, 0x1

    sub-int v0, v11, v4

    div-int/lit8 v7, v0, 0x2

    sget-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int v0, v11, v4

    div-int/lit8 v9, v0, 0x2

    sget-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->b:Ljava/lang/Integer;

    .line 138
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int v10, v4, v0

    move-object v5, p0

    .line 137
    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    .line 140
    const/4 v3, 0x2

    sget-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->c:Ljava/lang/Integer;

    .line 141
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v5, v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/AvatarsView;->c:Ljava/lang/Integer;

    .line 142
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v7, v12, v0

    move-object v2, p0

    move v6, v11

    .line 140
    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/plus/views/AvatarsView;->a(IIIII)V

    goto :goto_0

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 96
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/views/AvatarsView;->resolveSize(II)I

    move-result v1

    .line 97
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/views/AvatarsView;->resolveSize(II)I

    move-result v2

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->getChildCount()I

    move-result v3

    .line 100
    if-lez v3, :cond_1

    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AvatarsView;->a()I

    move-result v3

    .line 102
    :goto_0
    if-gt v0, v3, :cond_1

    .line 103
    sget-object v4, Lcom/google/android/apps/plus/views/AvatarsView;->a:[I

    aget v4, v4, v3

    sget-object v5, Lcom/google/android/apps/plus/views/AvatarsView;->a:[I

    aget v5, v5, v3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AvatarsView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v6, v4, v5}, Landroid/view/View;->measure(II)V

    .line 102
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_1
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/AvatarsView;->setMeasuredDimension(II)V

    .line 108
    return-void
.end method
