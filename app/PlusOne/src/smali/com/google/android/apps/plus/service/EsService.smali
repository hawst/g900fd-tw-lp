.class public Lcom/google/android/apps/plus/service/EsService;
.super Landroid/app/Service;
.source "PG"

# interfaces
.implements Lfif;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:Ljava/lang/String;

.field private static final b:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lfib;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lfhh;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lfhp;

.field private static f:Ljava/io/File;

.field private static g:Ljava/lang/Integer;

.field private static h:Landroid/os/Handler;

.field private static i:Lfhc;

.field private static j:Ldvu;

.field private static k:Ldls;

.field private static l:Ldil;

.field private static final m:Lfgz;


# instance fields
.field private n:Lfic;

.field private o:I

.field private final p:Lfhb;

.field private final q:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 463
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 542
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    .line 543
    new-instance v0, Lfha;

    invoke-direct {v0}, Lfha;-><init>()V

    .line 544
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    .line 545
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    .line 546
    new-instance v0, Lfhp;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lfhp;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    .line 548
    new-instance v0, Lfha;

    invoke-direct {v0}, Lfha;-><init>()V

    .line 549
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    .line 551
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->g:Ljava/lang/Integer;

    .line 582
    new-instance v0, Lfgz;

    invoke-direct {v0}, Lfgz;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->m:Lfgz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 193
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 616
    new-instance v0, Lfhb;

    invoke-direct {v0, p0}, Lfhb;-><init>(Lcom/google/android/apps/plus/service/EsService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->p:Lfhb;

    .line 617
    new-instance v0, Lfgd;

    invoke-direct {v0, p0}, Lfgd;-><init>(Lcom/google/android/apps/plus/service/EsService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->q:Ljava/lang/Runnable;

    return-void
.end method

.method static a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 698
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 699
    const-string v1, "op"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 701
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 1791
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1792
    const-string v1, "op"

    const/16 v2, 0x2ce

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1793
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1794
    const-string v1, "cover_photo_owner_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1795
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1796
    const-string v1, "top_offset"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1797
    const-string v1, "layout"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1799
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Landroid/graphics/RectF;I)I
    .locals 3

    .prologue
    .line 1768
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1769
    const-string v1, "op"

    const/16 v2, 0x2ce

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1770
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1771
    const-string v1, "cover_photo_owner_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1772
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1773
    const-string v1, "coordinates"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1774
    const-string v1, "rotation"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1776
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 717
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 718
    const-string v1, "op"

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 719
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 720
    const-string v1, "view"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 721
    if-eqz p3, :cond_0

    .line 722
    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 724
    :cond_0
    if-eqz p4, :cond_1

    .line 725
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 727
    :cond_1
    if-eqz p5, :cond_2

    .line 728
    const-string v1, "square_stream_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 731
    :cond_2
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;IILlae;)I
    .locals 3

    .prologue
    .line 745
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 746
    const-string v1, "op"

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 747
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 748
    const-string v1, "view"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 749
    const-string v1, "loc"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 751
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;IJ)I
    .locals 4

    .prologue
    .line 1258
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1259
    const-string v1, "op"

    const/16 v2, 0x6a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1260
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1261
    const-string v1, "all_photos_metadata_count"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1263
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;IJLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)I
    .locals 12

    .prologue
    .line 1583
    if-nez p4, :cond_0

    if-nez p12, :cond_0

    .line 1584
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Must have shapeId or relativeBounds"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object v3, p0

    move v4, p1

    move-object/from16 v5, p5

    move-wide v6, p2

    move-object/from16 v8, p4

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    .line 1586
    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1588
    const-string v3, "op"

    const/16 v4, 0x53

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1589
    const-string v3, "taggee_name"

    move-object/from16 v0, p6

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1590
    const-string v3, "taggee_id"

    move-object/from16 v0, p8

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1591
    const-string v3, "taggee_email"

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1592
    const-string v3, "collection_id"

    move-object/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1593
    if-eqz p12, :cond_1

    .line 1594
    const-string v3, "bounds"

    move-object/from16 v0, p12

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1597
    :cond_1
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    return v2
.end method

.method public static a(Landroid/content/Context;IJLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I
    .locals 11

    .prologue
    .line 1609
    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object v3, p0

    move v4, p1

    move-object v5, p4

    move-wide v6, p2

    move-object/from16 v9, p8

    move-object/from16 v10, p7

    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1611
    const-string v3, "op"

    const/16 v4, 0x52

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1612
    const-string v3, "retain_shape"

    move/from16 v0, p9

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1614
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    return v2
.end method

.method public static a(Landroid/content/Context;ILidh;)I
    .locals 3

    .prologue
    .line 2656
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2657
    const-string v1, "op"

    const/16 v2, 0x388

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2658
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2659
    const-string v1, "event"

    invoke-virtual {p2}, Lidh;->f()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2660
    const-string v1, "event_type"

    invoke-virtual {p2}, Lidh;->g()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2662
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILidh;Lhgw;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2636
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2637
    const-string v1, "op"

    const/16 v2, 0x387

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2638
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2639
    const-string v1, "event"

    invoke-virtual {p2}, Lidh;->f()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2640
    const-string v1, "event_type"

    invoke-virtual {p2}, Lidh;->g()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2641
    const-string v1, "audience"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2642
    const-string v1, "external_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2644
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILizu;)I
    .locals 3

    .prologue
    .line 1835
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1836
    const-string v1, "op"

    const/16 v2, 0x67

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1837
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1838
    const-string v1, "media"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1840
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 1417
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1418
    const-string v1, "op"

    const/16 v2, 0x59

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1419
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1420
    const-string v1, "photo_id"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1421
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1422
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1423
    const-string v1, "view_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1425
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/String;ZLjava/lang/String;I)I
    .locals 4

    .prologue
    .line 1433
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1434
    const-string v1, "op"

    const/16 v2, 0x5a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1435
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1436
    const-string v1, "photo_id"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1437
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1438
    const-string v1, "is_undo"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1439
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1440
    const/4 v1, -0x1

    if-eq p6, v1, :cond_0

    .line 1441
    const-string v1, "abuse_type"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1444
    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 783
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 784
    const-string v1, "op"

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 785
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 786
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 788
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1285
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1286
    const-string v1, "op"

    const/16 v2, 0x54

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1287
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1288
    const-string v1, "search_query"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1289
    const-string v1, "search_mode"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1290
    const-string v1, "resumetoken"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1291
    const-string v1, "view_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1292
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;J)I
    .locals 3

    .prologue
    .line 1676
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1677
    const-string v1, "op"

    const/16 v2, 0x4c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1678
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1679
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1680
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1682
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;JJLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;)I
    .locals 11

    .prologue
    .line 1563
    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object v3, p0

    move v4, p1

    move-object v5, p2

    move-wide v6, p3

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1565
    const-string v3, "op"

    const/16 v4, 0x51

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1566
    const-string v3, "suggested_gaia_id"

    move-object/from16 v0, p10

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1567
    const-string v3, "approved"

    move/from16 v0, p9

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1568
    const-string v3, "is_suggested"

    move/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1569
    const-string v3, "collection_id"

    move-object/from16 v0, p12

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1571
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    return v2
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1399
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1400
    const-string v1, "op"

    const/16 v2, 0x56

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1401
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1402
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1403
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1404
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1405
    const-string v1, "view_id"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1406
    const-string v1, "text"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1407
    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1409
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I
    .locals 3

    .prologue
    .line 1499
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1500
    const-string v1, "op"

    const/16 v2, 0x4f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1501
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1502
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1503
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1504
    const-string v1, "plus_oned"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1505
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1506
    const-string v1, "view_id"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1508
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Landroid/graphics/RectF;IZ)I
    .locals 3

    .prologue
    .line 1746
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1747
    const-string v1, "op"

    const/16 v2, 0x2c7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1748
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1749
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1750
    const-string v1, "coordinates"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1751
    const-string v1, "rotation"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1752
    const-string v1, "is_gallery_photo"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1754
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 682
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 683
    const-string v1, "op"

    const/16 v2, 0x63

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 684
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 685
    const-string v1, "owner_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 688
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I[B)I
    .locals 3

    .prologue
    .line 2614
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2615
    const-string v1, "op"

    const/16 v2, 0x386

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2616
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2617
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2618
    const-string v1, "rsvp_type"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2619
    const-string v1, "event_auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2620
    const-string v1, "promoted_post_data"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2622
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 1331
    invoke-static {p3}, Ljvj;->n(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332
    invoke-static {p3}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1333
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "op"

    const/16 v3, 0x48

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "account_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "gaia_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "album_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    .line 1335
    :goto_0
    return v0

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getCollectionTiles: collectionId is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x49

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "collection_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhgw;)I
    .locals 3

    .prologue
    .line 2711
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2712
    const-string v1, "op"

    const/16 v2, 0x38b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2713
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2714
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2715
    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2716
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2717
    const-string v1, "audience"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2719
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1459
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1460
    const-string v1, "op"

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1461
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1462
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1463
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1464
    const-string v1, "content"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1465
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1467
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 3

    .prologue
    .line 2554
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2555
    const-string v1, "op"

    const/16 v2, 0x38d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2556
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2557
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2558
    const-string v1, "pollingtoken"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2559
    const-string v1, "resumetoken"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2560
    const-string v1, "invitationtoken"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2561
    const-string v1, "event_auth_key"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2562
    const-string v1, "fetchnewer"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2564
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)I
    .locals 3

    .prologue
    .line 1875
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1876
    const-string v1, "op"

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1877
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1878
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1879
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1880
    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1881
    const-string v1, "content"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1882
    const-string v1, "promoted_post_data"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1884
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)I
    .locals 3

    .prologue
    .line 1969
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1970
    const-string v1, "op"

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1971
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1972
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1973
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1974
    const-string v1, "plusone_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1975
    const-string v1, "plus_oned"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1976
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1978
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[BZ)I
    .locals 3

    .prologue
    .line 1705
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1706
    const-string v1, "op"

    const/16 v2, 0x5b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1707
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1708
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1709
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1710
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1711
    const-string v1, "edit_info"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1712
    const-string v1, "set_edit_list_data"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1714
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I
    .locals 3

    .prologue
    .line 803
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 804
    const-string v1, "op"

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 805
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 806
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 807
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 808
    const-string v1, "reshare"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 810
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2735
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2736
    const-string v1, "op"

    const/16 v2, 0x3f1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2737
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2738
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2739
    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2740
    const-string v1, "blacklist"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2741
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2742
    const-string v1, "email"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2744
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZZI)I
    .locals 3

    .prologue
    .line 1942
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1943
    const-string v1, "op"

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1944
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1945
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1946
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1947
    const-string v1, "report"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1948
    const-string v1, "is_undo"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1949
    const/4 v1, -0x1

    if-eq p6, v1, :cond_0

    .line 1950
    const-string v1, "abuse_type"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1953
    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[B)I
    .locals 3

    .prologue
    .line 1854
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1855
    const-string v1, "op"

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1856
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1857
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1858
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1859
    const-string v1, "promoted_post_data"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1861
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1664
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1665
    const-string v1, "op"

    const/16 v2, 0x50

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1666
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1667
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1668
    const-string v1, "tile_ids"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1669
    const-string v1, "delete_duplicates"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1670
    const-string v1, "delete_type"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1672
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;Llai;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Llai;",
            ")I"
        }
    .end annotation

    .prologue
    .line 2863
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2864
    const-string v1, "op"

    const/16 v2, 0xc1c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2865
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2866
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2867
    const-string v1, "topic_ids_to_remove"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2868
    const-string v1, "modified_relateds"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2870
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2588
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    .line 2589
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [J

    .line 2591
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2592
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 2591
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2595
    :cond_0
    const-string v0, "op"

    const/16 v1, 0x38f

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2596
    const-string v0, "account_id"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2597
    const-string v0, "event_id"

    invoke-virtual {v2, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2598
    const-string v0, "photo_ids"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 2600
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Z)I
    .locals 3

    .prologue
    .line 765
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 766
    const-string v1, "op"

    const/16 v2, 0x1d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 767
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 768
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 769
    const-string v1, "is_stranger_post"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 771
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;ZLjava/lang/String;)I
    .locals 3

    .prologue
    .line 1821
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1822
    const-string v1, "op"

    const/16 v2, 0x5f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1823
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1824
    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1825
    const-string v1, "full_res"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1826
    const-string v1, "description"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1828
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Z[B)I
    .locals 1

    .prologue
    .line 990
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;Z[B)Landroid/content/Intent;

    move-result-object v0

    .line 992
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjeo;)I
    .locals 3

    .prologue
    .line 2515
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2516
    const-string v1, "op"

    const/16 v2, 0x68

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2517
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2518
    const-string v1, "moviemaker_edits"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2519
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILnyq;)I
    .locals 3

    .prologue
    .line 1690
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1691
    const-string v1, "op"

    const/16 v2, 0x58

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1692
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1693
    const-string v1, "photos_settings"

    .line 1694
    invoke-static {p2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    .line 1693
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1696
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;IZ)I
    .locals 3

    .prologue
    .line 1241
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1242
    const-string v1, "op"

    const/16 v2, 0x69

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1243
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1244
    const-string v1, "all_photos_force_refresh"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1246
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;I[B)I
    .locals 3

    .prologue
    .line 1726
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1727
    const-string v1, "op"

    const/16 v2, 0x2c9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1728
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1729
    const-string v1, "data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1731
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;)I
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/4 v3, 0x0

    .line 2755
    invoke-static {}, Llsx;->b()V

    .line 2757
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->d()I

    move-result v2

    .line 2758
    const-string v0, "rid"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2760
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v5}, Landroid/os/Bundle;->size()I

    move-result v1

    invoke-virtual {v7}, Landroid/os/Bundle;->size()I

    move-result v4

    if-ne v1, v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v7}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v9, "rid"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    move v1, v3

    :goto_0
    if-eqz v1, :cond_0

    const-string v1, "EsService"

    const/4 v3, 0x2

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "op"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Op was pending: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "rid"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2761
    :goto_1
    if-eqz v0, :cond_7

    .line 2762
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    invoke-virtual {v1, p1}, Lfhp;->a(Landroid/content/Intent;)V

    .line 2764
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2769
    :goto_2
    return v0

    .line 2760
    :cond_3
    invoke-virtual {v7, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    if-nez v9, :cond_5

    const-string v9, "EsService"

    const/4 v11, 0x3

    invoke-static {v9, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x29

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "pending request id key ["

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, "] has value null!"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    if-eqz v10, :cond_1

    move v1, v3

    goto :goto_0

    :cond_5
    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v3

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 2766
    :cond_7
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2767
    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move v0, v2

    .line 2769
    goto :goto_2

    :cond_8
    move v1, v4

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;J)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1028
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v3, v0}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;Z[B)Landroid/content/Intent;

    move-result-object v0

    .line 1030
    const-string v1, "com.google.android.libraries.social.notifications.FROM_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1031
    const-string v1, "com.google.android.libraries.social.notifications.notif_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1032
    const-string v1, "com.google.android.libraries.social.notifications.updated_version"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1033
    const/high16 v1, 0x10000000

    invoke-static {p0, v3, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lkzs;IJLjava/lang/String;Ljava/util/ArrayList;)Landroid/app/PendingIntent;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Lkzs;",
            "IJ",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .line 1087
    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v3}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v5

    .line 1089
    invoke-virtual/range {p3 .. p3}, Lkzs;->a()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1090
    invoke-virtual/range {p3 .. p3}, Lkzs;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1091
    invoke-virtual/range {p3 .. p3}, Lkzs;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1093
    invoke-virtual/range {p3 .. p3}, Lkzs;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lkzs;->b()Ljava/lang/String;

    move-result-object v4

    .line 1094
    invoke-virtual/range {p3 .. p3}, Lkzs;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    .line 1092
    :goto_0
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Leyq;->a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    move-object/from16 v0, p0

    invoke-static {v0, v5, v4, v2}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    :goto_1
    move-object v5, v2

    .line 1101
    :cond_0
    :goto_2
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1102
    move-object/from16 v0, p7

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1103
    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v7, p4

    move-wide/from16 v8, p5

    move-object/from16 v15, p8

    invoke-static/range {v3 .. v15}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;Lda;IJZZZZLjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1109
    const-string v2, "aid"

    move-object/from16 v0, p2

    invoke-virtual {v5, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1110
    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    return-object v2

    .line 1094
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1092
    :cond_2
    invoke-static {v3}, Leyq;->a(Ljava/util/List;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_1

    .line 1095
    :cond_3
    invoke-virtual/range {p3 .. p3}, Lkzs;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1098
    invoke-virtual/range {p3 .. p3}, Lkzs;->c()Ljava/lang/String;

    move-result-object v2

    .line 1097
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v2}, Litm;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;ILjava/util/ArrayList;[JLjava/lang/String;)Landroid/app/PendingIntent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;[J",
            "Ljava/lang/String;",
            ")",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .line 2397
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2398
    const-string v1, "op"

    const/16 v2, 0xd8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2399
    const-string v1, "notif_version"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 2400
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2401
    const-string v1, "view_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2402
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2403
    const-string v1, "notif_key"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2405
    :cond_0
    const/4 v1, 0x1

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1545
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1546
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1547
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1548
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1549
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1550
    const-string v1, "view_id"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1551
    if-eqz p5, :cond_0

    .line 1552
    const-string v1, "shape_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1554
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2210
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2211
    const-string v1, "op"

    const/16 v2, 0xd6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2212
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2214
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;II)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2227
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2228
    const-string v1, "op"

    const/16 v2, 0xd7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2229
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2230
    const-string v1, "read_state"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2232
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;III[B)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2284
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2285
    const-string v1, "op"

    const/16 v2, 0xd3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2286
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2287
    const-string v1, "read_state"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2288
    const-string v1, "high_priority"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2289
    const-string v1, "next_fetch_param"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2291
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;III[BZ)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2261
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2262
    const-string v1, "op"

    const/16 v2, 0xca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2263
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2264
    const-string v1, "read_state"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2265
    const-string v1, "high_priority"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2266
    const-string v1, "init_fetch_param"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2267
    const-string v1, "user_initiated"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2268
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;IJ[Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 1184
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1185
    const-string v1, "op"

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1186
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1187
    const-string v1, "activity_id_list"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1188
    const-string v1, "timestamp"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1189
    const-string v1, "mark_operation"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1191
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;I)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2477
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2478
    const-string v1, "op"

    const/16 v2, 0x2c3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2479
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2480
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2481
    const-string v1, "abuse_type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2483
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/util/Collection;)Ljava/lang/Integer;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Lfeu;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1125
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 1169
    :goto_0
    return-object v0

    .line 1132
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1133
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1135
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfeu;

    .line 1136
    invoke-virtual {v0}, Lfeu;->f()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1137
    invoke-virtual {v0}, Lfeu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1140
    invoke-virtual {v0}, Lfeu;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1142
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1143
    const-string v1, "extra_activity_id"

    invoke-virtual {v0}, Lfeu;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1144
    const-string v1, "extra_creation_source_id"

    invoke-virtual {v0}, Lfeu;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    const-string v1, "extra_stream_explanation"

    invoke-virtual {v0}, Lfeu;->c()I

    move-result v7

    invoke-virtual {v6, v1, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1146
    const-string v1, "extra_analytics_timestamp"

    invoke-virtual {v0}, Lfeu;->d()J

    move-result-wide v8

    invoke-virtual {v6, v1, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1148
    const-class v1, Lhms;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhms;

    new-instance v7, Lhmr;

    invoke-direct {v7, p0, p1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v8, Lhmv;->ad:Lhmv;

    .line 1150
    invoke-virtual {v7, v8}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v7

    .line 1151
    invoke-virtual {v0}, Lfeu;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lhmr;->a(Ljava/lang/Long;)Lhmr;

    move-result-object v7

    .line 1152
    invoke-virtual {v0}, Lfeu;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lhmr;->b(Ljava/lang/Long;)Lhmr;

    move-result-object v7

    .line 1153
    invoke-virtual {v7, v6}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v6

    .line 1148
    invoke-interface {v1, v6}, Lhms;->a(Lhmr;)V

    .line 1156
    invoke-virtual {v0}, Lfeu;->e()V

    goto :goto_1

    .line 1159
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v2

    .line 1160
    goto/16 :goto_0

    .line 1163
    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 1164
    const-string v0, "op"

    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1165
    const-string v0, "account_id"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1166
    const-string v2, "activity_id_list"

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1167
    const-string v0, "creation_source_list"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1169
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILnjt;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2048
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2049
    const-string v1, "op"

    const/16 v2, 0x2c0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2050
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2052
    invoke-static {p2}, Loxu;->a(Loxu;)[B

    move-result-object v1

    .line 2053
    const-string v2, "profile"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2055
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILnkf;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2068
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2069
    const-string v1, "op"

    const/16 v2, 0x2cf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2072
    invoke-static {p2}, Loxu;->a(Loxu;)[B

    move-result-object v1

    .line 2073
    const-string v2, "profile"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2075
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILandroid/content/Intent;II[B)V
    .locals 8

    .prologue
    .line 193
    new-instance v2, Lkfp;

    invoke-direct {v2}, Lkfp;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lkfp;->a(Z)V

    const-string v0, "Notification sync"

    invoke-virtual {v2, v0}, Lkfp;->b(Ljava/lang/String;)V

    const/4 v3, 0x0

    :try_start_0
    sget-object v4, Ldsl;->c:Ldsl;

    move-object v0, p1

    move v1, p2

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-static/range {v0 .. v7}, Ldsf;->a(Landroid/content/Context;ILkfp;Lles;Ldsl;II[B)V

    new-instance v0, Lfib;

    invoke-direct {v0}, Lfib;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p3, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Lkfp;->g()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Lfib;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4, v0}, Lfib;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p3, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Lkfp;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lkfp;->g()V

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;IZLandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 193
    new-instance v1, Lkfp;

    invoke-direct {v1}, Lkfp;-><init>()V

    const-string v0, "People sync"

    invoke-virtual {v1, v0}, Lkfp;->b(Ljava/lang/String;)V

    :try_start_0
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "is_google_plus"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p1, p2, v1, v0, p3}, Ldsm;->a(Landroid/content/Context;ILkfp;Lles;Z)V

    :cond_0
    new-instance v0, Lfib;

    invoke-direct {v0}, Lfib;-><init>()V

    const/4 v2, 0x0

    invoke-direct {p0, p4, v0, v2}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lkfp;->g()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Lfib;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Lfib;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p4, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lkfp;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lkfp;->g()V

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lfib;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    return-void
.end method

.method public static a(Lfhh;)V
    .locals 1

    .prologue
    .line 635
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 636
    return-void
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 2778
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 2779
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    .line 2781
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2782
    return-void
.end method

.method private a(Lkff;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2917
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->i:Lfhc;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService;->p:Lfhb;

    invoke-virtual {v0, p1, p2, v1}, Lfhc;->a(Lkff;Landroid/content/Intent;Lfhf;)V

    .line 2918
    return-void
.end method

.method public static a(Llox;)V
    .locals 8

    .prologue
    const/4 v3, -0x1

    .line 5675
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5676
    const-string v0, "No pending intents"

    invoke-virtual {p0, v0}, Llox;->println(Ljava/lang/String;)V

    .line 5685
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5686
    const-string v0, "No service results"

    invoke-virtual {p0, v0}, Llox;->println(Ljava/lang/String;)V

    .line 5692
    :cond_1
    return-void

    .line 5678
    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 5679
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 5680
    if-nez v1, :cond_3

    move v2, v3

    .line 5681
    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x25

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Pending request: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " opCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Llox;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 5680
    :cond_3
    const-string v2, "op"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    goto :goto_1

    .line 5688
    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 5689
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Service result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Llox;->println(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    .line 654
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;ILandroid/content/Intent;I)Z
    .locals 17

    .prologue
    .line 4014
    const-class v4, Lhei;

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    .line 4016
    sparse-switch p4, :sswitch_data_0

    .line 4425
    const/4 v4, 0x0

    .line 4428
    :goto_0
    return v4

    .line 4018
    :sswitch_0
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4019
    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4020
    new-instance v11, Ljava/lang/Thread;

    new-instance v4, Lfgi;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v10}, Lfgi;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v11, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4034
    invoke-virtual {v11}, Ljava/lang/Thread;->start()V

    .line 4428
    :goto_1
    const/4 v4, 0x1

    goto :goto_0

    .line 4040
    :sswitch_1
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4041
    const-string v4, "pollingtoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4042
    const-string v4, "resumetoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 4043
    const-string v4, "event_auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 4044
    const-string v4, "invitationtoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 4045
    const-string v4, "fetchnewer"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    .line 4046
    const-string v4, "resolvetokens"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    .line 4048
    new-instance v16, Ljava/lang/Thread;

    new-instance v4, Lfgj;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v15, p3

    invoke-direct/range {v4 .. v15}, Lfgj;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/Intent;)V

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4063
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Thread;->start()V

    goto :goto_1

    .line 4069
    :sswitch_2
    const-string v4, "include_blacklist"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 4071
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4072
    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4073
    new-instance v4, Ldka;

    move-object/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v4 .. v9}, Ldka;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 4075
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto :goto_1

    .line 4080
    :sswitch_3
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4081
    const-string v4, "photo_ids"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v6

    .line 4085
    array-length v4, v6

    new-array v7, v4, [Ljava/lang/Long;

    .line 4086
    const/4 v4, 0x0

    :goto_2
    array-length v8, v6

    if-ge v4, v8, :cond_0

    .line 4087
    aget-wide v8, v6, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v4

    .line 4086
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 4090
    :cond_0
    new-instance v4, Ldnn;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v4, v0, v1, v7, v5}, Ldnn;-><init>(Landroid/content/Context;I[Ljava/lang/Long;Ljava/lang/String;)V

    .line 4092
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4097
    :sswitch_4
    const-string v4, "circle_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4098
    const-string v5, "just_following"

    const/4 v6, 0x0

    .line 4099
    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 4101
    new-instance v6, Ldir;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v6, v0, v1, v4, v5}, Ldir;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 4103
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v6, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4108
    :sswitch_5
    const-string v4, "circle_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4109
    const-string v4, "circle_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4110
    const-string v4, "just_following"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 4112
    new-instance v4, Ldlj;

    move-object/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v4 .. v9}, Ldlj;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 4114
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4119
    :sswitch_6
    const-string v4, "circle_ids"

    .line 4120
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 4121
    new-instance v10, Ljava/lang/Thread;

    new-instance v4, Lfgk;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v9, p3

    invoke-direct/range {v4 .. v9}, Lfgk;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILjava/util/ArrayList;Landroid/content/Intent;)V

    invoke-direct {v10, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4133
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 4138
    :sswitch_7
    const-string v4, "refresh"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 4139
    new-instance v10, Ljava/lang/Thread;

    new-instance v4, Lfgl;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v9, p3

    invoke-direct/range {v4 .. v9}, Lfgl;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;IZLandroid/content/Intent;)V

    invoke-direct {v10, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4146
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 4151
    :sswitch_8
    new-instance v4, Ldjw;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v4, v0, v1}, Ldjw;-><init>(Landroid/content/Context;I)V

    .line 4152
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4157
    :sswitch_9
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/content/Context;)V

    .line 4158
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 4163
    :sswitch_a
    const-class v5, Ljgn;

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljgn;

    .line 4164
    move/from16 v0, p2

    invoke-interface {v4, v0}, Lhei;->d(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljgn;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4165
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lfgm;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v5, v0, v1, v2, v3}, Lfgm;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILandroid/content/Intent;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4184
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 4187
    :cond_1
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 4192
    :sswitch_b
    const-string v4, "filename"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4194
    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lfus;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/service/EsService;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4198
    :goto_3
    new-instance v4, Lfhs;

    invoke-direct {v4}, Lfhs;-><init>()V

    .line 4199
    sget-object v5, Lcom/google/android/apps/plus/service/EsService;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lfhs;->a(Ljava/lang/String;)V

    .line 4201
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 4196
    :catch_0
    move-exception v4

    const/4 v4, 0x0

    sput-object v4, Lcom/google/android/apps/plus/service/EsService;->a:Ljava/lang/String;

    goto :goto_3

    .line 4206
    :sswitch_c
    invoke-static/range {p1 .. p2}, Ldsc;->a(Landroid/content/Context;I)V

    .line 4207
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 4212
    :sswitch_d
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4213
    const-string v4, "rsvp_type"

    const/high16 v5, -0x80000000

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 4215
    const-string v4, "event_auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 4216
    const-string v4, "promoted_post_data"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v11

    .line 4218
    new-instance v13, Ljava/lang/Thread;

    new-instance v4, Lfgn;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v12, p3

    invoke-direct/range {v4 .. v12}, Lfgn;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;[BLandroid/content/Intent;)V

    invoke-direct {v13, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4232
    invoke-virtual {v13}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 4237
    :sswitch_e
    const/4 v7, 0x0

    .line 4239
    :try_start_1
    new-instance v4, Lpbl;

    invoke-direct {v4}, Lpbl;-><init>()V

    const-string v5, "event"

    .line 4241
    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    .line 4239
    invoke-static {v4, v5}, Lpbl;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Lpbl;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_1

    move-object v7, v4

    .line 4245
    :goto_4
    const-string v4, "audience"

    .line 4246
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lhgw;

    .line 4247
    const-string v4, "external_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4248
    new-instance v4, Ldis;

    move-object/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v4 .. v9}, Ldis;-><init>(Landroid/content/Context;ILpbl;Lhgw;Ljava/lang/String;)V

    .line 4250
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4242
    :catch_1
    move-exception v4

    .line 4243
    const-string v5, "EsService"

    const-string v6, "Could not deserialize PlusEvent from extras."

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 4255
    :sswitch_f
    const/4 v6, 0x0

    .line 4256
    const-string v4, "event_type"

    const/4 v5, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 4258
    if-nez v4, :cond_2

    .line 4259
    :try_start_2
    new-instance v5, Lidh;

    new-instance v4, Lpbl;

    invoke-direct {v4}, Lpbl;-><init>()V

    const-string v7, "event"

    .line 4260
    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v7

    .line 4259
    invoke-static {v4, v7}, Lpbl;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Lpbl;

    invoke-direct {v5, v4}, Lidh;-><init>(Lpbl;)V
    :try_end_2
    .catch Loxt; {:try_start_2 .. :try_end_2} :catch_2

    move-object v4, v5

    .line 4269
    :goto_5
    new-instance v5, Ldod;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v5, v0, v1, v4}, Ldod;-><init>(Landroid/content/Context;ILidh;)V

    .line 4271
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v5, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4261
    :cond_2
    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 4262
    :try_start_3
    new-instance v5, Lidh;

    new-instance v4, Lozp;

    invoke-direct {v4}, Lozp;-><init>()V

    const-string v7, "event"

    .line 4264
    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v7

    .line 4263
    invoke-static {v4, v7}, Lozp;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Lozp;

    invoke-direct {v5, v4}, Lidh;-><init>(Lozp;)V
    :try_end_3
    .catch Loxt; {:try_start_3 .. :try_end_3} :catch_2

    move-object v4, v5

    goto :goto_5

    .line 4266
    :catch_2
    move-exception v4

    .line 4267
    const-string v5, "EsService"

    const-string v7, "Could not deserialize PlusEvent from extras."

    invoke-static {v5, v7, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v6

    goto :goto_5

    .line 4276
    :sswitch_10
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4277
    const-string v5, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4278
    new-instance v6, Ldja;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v6, v0, v1, v4, v5}, Ldja;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 4280
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v6, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4285
    :sswitch_11
    new-instance v4, Ldkd;

    new-instance v5, Lkfo;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v5, v0, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v4, v0, v5, v1}, Ldkd;-><init>(Landroid/content/Context;Lkfo;I)V

    .line 4288
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4293
    :sswitch_12
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4294
    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4295
    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4296
    const-string v4, "audience"

    .line 4297
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lhgw;

    .line 4298
    new-instance v4, Ldjk;

    move-object/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v4 .. v10}, Ldjk;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhgw;)V

    .line 4300
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4305
    :sswitch_13
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4306
    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4307
    const-string v4, "blacklist"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 4308
    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 4309
    const-string v4, "email"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 4310
    new-instance v4, Ldjl;

    move-object/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v4 .. v11}, Ldjl;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 4312
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4317
    :sswitch_14
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lfgp;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move/from16 v3, p2

    invoke-direct {v5, v0, v1, v2, v3}, Lfgp;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;I)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4330
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 4335
    :sswitch_15
    const-string v4, "account_id"

    const/4 v5, -0x1

    .line 4336
    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x0

    .line 4335
    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Ldhv;->b(Landroid/content/Context;IZ)V

    .line 4337
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 4342
    :sswitch_16
    const-string v4, "account_id"

    const/4 v5, -0x1

    .line 4343
    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v5, "timestamp"

    const-wide/16 v6, -0x1

    .line 4344
    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 4342
    move-object/from16 v0, p0

    invoke-static {v0, v4, v6, v7}, Ldhv;->a(Landroid/content/Context;IJ)V

    .line 4345
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 4350
    :sswitch_17
    const-string v4, "package_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4351
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v7}, Llbn;->a(Landroid/content/Context;ILjava/lang/String;)Llbo;

    move-result-object v10

    .line 4354
    if-eqz v10, :cond_3

    .line 4355
    iget-object v5, v10, Llbo;->b:Ljava/lang/String;

    iget-object v6, v10, Llbo;->a:Ljava/lang/String;

    iget-object v8, v10, Llbo;->d:Ljava/lang/String;

    iget-object v9, v10, Llbo;->c:Ljava/lang/String;

    move-object/from16 v4, p1

    invoke-static/range {v4 .. v9}, Lfuz;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4358
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v7}, Llbn;->b(Landroid/content/Context;ILjava/lang/String;)V

    .line 4359
    const-string v4, "stream_install_interactive_post"

    iget-object v5, v10, Llbo;->c:Ljava/lang/String;

    .line 4360
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 4361
    const-class v4, Lhms;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhms;

    new-instance v6, Lhmr;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v6, v0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    if-eqz v5, :cond_4

    sget-object v5, Lhmv;->bt:Lhmv;

    .line 4363
    :goto_6
    invoke-virtual {v6, v5}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v5

    sget-object v6, Lhmw;->y:Lhmw;

    .line 4366
    invoke-virtual {v5, v6}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v5

    .line 4361
    invoke-interface {v4, v5}, Lhms;->a(Lhmr;)V

    .line 4369
    :cond_3
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 4361
    :cond_4
    sget-object v5, Lhmv;->bm:Lhmv;

    goto :goto_6

    .line 4374
    :sswitch_18
    const-string v4, "owner_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4375
    const-string v5, "photo_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4376
    new-instance v6, Ldma;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v6, v0, v1, v4, v5}, Ldma;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 4378
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v6, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4383
    :sswitch_19
    const-string v4, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4386
    new-instance v5, Ldmp;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v5, v0, v1, v4}, Ldmp;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 4388
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v5, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4393
    :sswitch_1a
    const-string v4, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4394
    const-string v4, "topic_ids_to_remove"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 4396
    const-string v4, "modified_relateds"

    .line 4397
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Llai;

    .line 4398
    new-instance v4, Ldlg;

    move-object/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v4 .. v9}, Ldlg;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;Llai;)V

    .line 4400
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 4405
    :sswitch_1b
    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4406
    const-string v4, "is_stranger_post"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 4408
    new-instance v11, Ljava/lang/Thread;

    new-instance v4, Lfgq;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v10}, Lfgq;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILjava/lang/String;ZLandroid/content/Intent;)V

    invoke-direct {v11, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4420
    invoke-virtual {v11}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :cond_5
    move-object v4, v6

    goto/16 :goto_5

    .line 4016
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_9
        0x1a -> :sswitch_19
        0x1d -> :sswitch_1b
        0x63 -> :sswitch_18
        0x1f4 -> :sswitch_7
        0x1f7 -> :sswitch_8
        0x2c4 -> :sswitch_4
        0x2c5 -> :sswitch_6
        0x2cc -> :sswitch_5
        0x385 -> :sswitch_0
        0x386 -> :sswitch_d
        0x387 -> :sswitch_e
        0x388 -> :sswitch_f
        0x38a -> :sswitch_11
        0x38b -> :sswitch_12
        0x38c -> :sswitch_10
        0x38d -> :sswitch_1
        0x38e -> :sswitch_2
        0x38f -> :sswitch_3
        0x3f1 -> :sswitch_13
        0x3f2 -> :sswitch_a
        0x460 -> :sswitch_b
        0x7d1 -> :sswitch_c
        0x8fc -> :sswitch_14
        0x8fd -> :sswitch_15
        0x960 -> :sswitch_16
        0xa28 -> :sswitch_17
        0xc1c -> :sswitch_1a
    .end sparse-switch
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 926
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 927
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 928
    const-string v2, "op"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 929
    const/16 v3, 0x10

    if-eq v2, v3, :cond_1

    const/16 v3, 0x11

    if-ne v2, v3, :cond_0

    .line 930
    :cond_1
    const-string v2, "aid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 931
    const/4 v0, 0x1

    .line 936
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;J)Z
    .locals 7

    .prologue
    .line 1478
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 1479
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1480
    const-string v2, "op"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1481
    const/16 v3, 0x4f

    if-ne v2, v3, :cond_0

    .line 1482
    const-string v2, "gaia_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "photo_id"

    const-wide/16 v4, 0x0

    .line 1483
    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    .line 1484
    const/4 v0, 0x1

    .line 1489
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 822
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 823
    const-string v1, "op"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 824
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 825
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 827
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 910
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 911
    const-string v1, "op"

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 912
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 913
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 914
    const-string v1, "source_stream_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 916
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1371
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1372
    const-string v1, "op"

    const/16 v2, 0x55

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1373
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1374
    const-string v1, "album_tile_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1375
    const-string v1, "collection_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1376
    const-string v1, "tile_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1377
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I
    .locals 3

    .prologue
    .line 1223
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1224
    const-string v1, "op"

    const/16 v2, 0x44

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1225
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1226
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1227
    const-string v1, "resumetoken"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1228
    const-string v1, "highlights_force_refresh"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1230
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Z)I
    .locals 3

    .prologue
    .line 841
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 842
    const-string v1, "op"

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 843
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 844
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 845
    const-string v1, "mute_state"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 847
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/Context;ILjava/lang/String;Z[B)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 970
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 971
    const-string v1, "op"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 972
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 973
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 974
    const-string v1, "shown_plus_one_promo"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 975
    const-string v1, "promoted_post_data"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 976
    return-object v0
.end method

.method public static b(I)Lfib;
    .locals 2

    .prologue
    .line 665
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->e()V

    .line 668
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfib;

    return-object v0
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2302
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2303
    const-string v1, "op"

    const/16 v2, 0xd4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2304
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2306
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;IJ)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 2240
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2241
    const-string v1, "op"

    const/16 v2, 0xd2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2242
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2243
    const-string v1, "last_version"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2245
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2009
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2010
    const-string v1, "op"

    const/16 v2, 0x2d5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2011
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2012
    const-string v1, "place_cluster_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2013
    const-string v1, "star_rating"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2014
    const-string v1, "review_text"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2015
    const-string v1, "aid"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2016
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;IZ)Ljava/lang/Integer;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2334
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2335
    const-string v1, "op"

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2336
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2337
    const-string v1, "refresh"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2339
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5546
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 5668
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    if-nez v0, :cond_0

    .line 5669
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/serviceresults.bin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    .line 5672
    :cond_0
    return-void
.end method

.method private b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5357
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    new-instance v1, Lfgr;

    invoke-direct {v1, p0, p1, p2, p3}, Lfgr;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5363
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILandroid/content/Intent;II[B)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 193
    :try_start_0
    sget-object v3, Ldsl;->c:Ldsl;

    move-object v0, p1

    move v1, p2

    move-object v2, p6

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Ldsf;->a(Landroid/content/Context;I[BLdsl;II)I

    new-instance v0, Lfib;

    invoke-direct {v0}, Lfib;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p3, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lfib;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v6, v0}, Lfib;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {p0, p3, v1, v6}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(Lfhh;)V
    .locals 1

    .prologue
    .line 644
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 645
    return-void
.end method

.method public static c(Landroid/content/Context;IJ)I
    .locals 4

    .prologue
    .line 2825
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2826
    const-string v1, "op"

    const/16 v2, 0x960

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2827
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2828
    const-string v1, "timestamp"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2830
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 891
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 892
    const-string v1, "op"

    const/16 v2, 0x26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 893
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 894
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 896
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1300
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1301
    const-string v1, "op"

    const/16 v2, 0x46

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1302
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1303
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1304
    const-string v1, "resumetoken"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1305
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1899
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1900
    const-string v1, "op"

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1901
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1902
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1903
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1904
    const-string v1, "content"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1906
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I
    .locals 3

    .prologue
    .line 1385
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1386
    const-string v1, "op"

    const/16 v2, 0x5e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1387
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1388
    const-string v1, "collection_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1389
    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1390
    const-string v1, "undo"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1391
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;Z)I
    .locals 3

    .prologue
    .line 861
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 862
    const-string v1, "op"

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 863
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 864
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 865
    const-string v1, "disable_comments"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 867
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static synthetic c()Ljava/util/Map;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    return-object v0
.end method

.method public static c(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 2316
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lfgo;

    invoke-direct {v1, p0, p1}, Lfgo;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2321
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2322
    return-void
.end method

.method private static d()I
    .locals 2

    .prologue
    .line 5553
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 5555
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    monitor-enter v1

    .line 5556
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->e()V

    .line 5557
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5560
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 5557
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 1046
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1047
    const-string v1, "op"

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1048
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1049
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1050
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1343
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1344
    const-string v1, "op"

    const/16 v2, 0x4a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1345
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1346
    const-string v1, "collection_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1347
    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1348
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;Z)I
    .locals 3

    .prologue
    .line 881
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 882
    const-string v1, "op"

    const/16 v2, 0x25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 883
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 884
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 885
    const-string v1, "disable_reshares"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 887
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/content/Context;I)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2350
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2351
    const-string v1, "op"

    const/16 v2, 0x1f7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2352
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2354
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2090
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2091
    const-string v1, "op"

    const/16 v2, 0x2d0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2092
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2093
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2094
    const-string v1, "suggestion_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2095
    const-string v1, "aid"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2097
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2456
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2457
    const-string v1, "op"

    const/16 v2, 0x2c2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2458
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2459
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2460
    const-string v1, "person_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2461
    const-string v1, "blocked"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2463
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1920
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1921
    const-string v1, "op"

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1922
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1923
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1924
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1926
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I
    .locals 3

    .prologue
    .line 2573
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2574
    const-string v1, "op"

    const/16 v2, 0x38e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2575
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2576
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2577
    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2578
    const-string v1, "include_blacklist"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2580
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;ILjava/lang/String;Z)I
    .locals 3

    .prologue
    .line 1987
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1988
    const-string v1, "op"

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1989
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1990
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1991
    const-string v1, "enabled"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1993
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1204
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1205
    const-string v1, "op"

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1206
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1207
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1209
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2113
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2114
    const-string v1, "op"

    const/16 v2, 0x2d1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2115
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2116
    const-string v1, "square_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2117
    const-string v1, "suggestion_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2118
    const-string v1, "aid"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2120
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private static e()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 5615
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 5659
    :cond_0
    :goto_0
    return-void

    .line 5619
    :cond_1
    sget-object v3, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    monitor-enter v3

    .line 5623
    :try_start_0
    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5624
    :try_start_1
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 5627
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->g:Ljava/lang/Integer;

    .line 5631
    :goto_1
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v4

    .line 5632
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfib;

    .line 5633
    sget-object v5, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    goto :goto_1

    .line 5636
    :catch_0
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    :goto_2
    :try_start_3
    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->g:Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x37

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "readResults: read results: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", lastRequestId: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 5641
    if-eqz v0, :cond_2

    .line 5643
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 5648
    :cond_2
    :goto_3
    if-eqz v1, :cond_3

    .line 5650
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 5658
    :cond_3
    :goto_4
    :try_start_6
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 5659
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0

    .line 5638
    :catch_1
    move-exception v1

    move-object v2, v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 5639
    :goto_5
    :try_start_7
    const-string v4, "EsService"

    const-string v5, "Cannot read service results"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 5641
    if-eqz v1, :cond_4

    .line 5643
    :try_start_8
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 5648
    :cond_4
    :goto_6
    if-eqz v2, :cond_3

    .line 5650
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    .line 5653
    :catch_2
    move-exception v0

    goto :goto_4

    .line 5641
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    :goto_7
    if-eqz v1, :cond_5

    .line 5643
    :try_start_a
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 5648
    :cond_5
    :goto_8
    if-eqz v2, :cond_6

    .line 5650
    :try_start_b
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 5653
    :cond_6
    :goto_9
    :try_start_c
    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_8

    :catch_7
    move-exception v1

    goto :goto_9

    .line 5641
    :catchall_2
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_7

    :catchall_3
    move-exception v0

    goto :goto_7

    :catchall_4
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_7

    .line 5638
    :catch_8
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_5

    :catch_9
    move-exception v0

    goto :goto_5

    .line 5636
    :catch_a
    move-exception v1

    move-object v1, v0

    goto/16 :goto_2

    :catch_b
    move-exception v1

    move-object v1, v2

    goto/16 :goto_2
.end method

.method public static e(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 2381
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2382
    const-string v1, "op"

    const/16 v2, 0x7d1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2383
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2385
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    .line 2386
    return-void
.end method

.method public static f(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 1270
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1271
    const-string v1, "op"

    const/16 v2, 0x45

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1272
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1273
    const-string v1, "resumetoken"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1274
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static f(Landroid/content/Context;I)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2417
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2418
    const-string v1, "op"

    const/16 v2, 0xce

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2419
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2421
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static f(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2134
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2135
    const-string v1, "op"

    const/16 v2, 0x2d2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2136
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2137
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2138
    const-string v1, "aid"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2140
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static f(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2030
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2031
    const-string v1, "op"

    const/16 v2, 0x2bf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2032
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2033
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2034
    const-string v1, "refresh"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2036
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static g(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 2793
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2794
    const-string v1, "op"

    const/16 v2, 0x8fc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2795
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2797
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static g(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 1535
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1536
    const-string v1, "op"

    const/16 v2, 0x4e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1537
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1538
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1540
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static g(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2675
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2676
    const-string v1, "op"

    const/16 v2, 0x38c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2677
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2678
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2679
    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2681
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static g(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2435
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2436
    const-string v1, "op"

    const/16 v2, 0x2c1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2437
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2438
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2439
    const-string v1, "muted"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2441
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static h(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 2808
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2809
    const-string v1, "op"

    const/16 v2, 0x8fd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2810
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2812
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static h(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 1634
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1635
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1636
    const-string v1, "op"

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1637
    const-string v1, "bucket_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1639
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static i(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2366
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2367
    const-string v1, "op"

    const/16 v2, 0x460

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2368
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2369
    const-string v1, "filename"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2371
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static j(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 2531
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2532
    const-string v1, "op"

    const/16 v2, 0x385

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2533
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2534
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2536
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static k(Landroid/content/Context;ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 2843
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lfhp;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2844
    const-string v1, "op"

    const/16 v2, 0xa28

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2845
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2846
    const-string v1, "package_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2848
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4433
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    if-eqz v0, :cond_3

    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    monitor-enter v4

    :try_start_0
    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->f:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfib;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    :goto_1
    :try_start_3
    const-string v3, "EsService"

    const-string v5, "Cannot save EsService results"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_1
    :goto_2
    if-eqz v2, :cond_2

    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_2
    :goto_3
    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 4434
    :cond_3
    return-void

    .line 4433
    :cond_4
    :try_start_7
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :goto_4
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_5
    if-eqz v2, :cond_5

    :try_start_9
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_5
    :goto_6
    if-eqz v3, :cond_6

    :try_start_a
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_6
    :goto_7
    :try_start_b
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    throw v0

    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_7

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_5

    :catchall_3
    move-exception v0

    goto :goto_5

    :catchall_4
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    goto :goto_5

    :catch_7
    move-exception v0

    move-object v2, v1

    goto :goto_1

    :catch_8
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method

.method public a(Landroid/content/Context;ILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 5469
    const-string v0, "gaia_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5471
    const-string v1, "abuse_type"

    const/4 v2, -0x1

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 5472
    new-instance v2, Ldmu;

    invoke-direct {v2, p1, p2, v0, v1}, Ldmu;-><init>(Landroid/content/Context;ILjava/lang/String;I)V

    .line 5474
    invoke-virtual {v2}, Ldmu;->l()V

    .line 5475
    const-string v0, "EsService"

    invoke-virtual {v2, v0}, Ldmu;->e(Ljava/lang/String;)V

    .line 5476
    return-void
.end method

.method public a(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5487
    new-instance v0, Ldiy;

    invoke-direct {v0, p1, p2, p3}, Ldiy;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 5488
    invoke-virtual {v0}, Ldiy;->l()V

    .line 5489
    const-string v1, "EsService"

    invoke-virtual {v0, v1}, Ldiy;->e(Ljava/lang/String;)V

    .line 5490
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 19

    .prologue
    .line 2922
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/service/EsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 2927
    :try_start_0
    const-string v4, "op"

    const/4 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    .line 2928
    const-string v4, "rid"

    const/4 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 2929
    const-string v4, "account_id"

    const/4 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 2940
    :try_start_1
    const-class v4, Lhei;

    invoke-static {v5, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    sparse-switch v18, :sswitch_data_0

    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_15

    .line 2951
    :cond_0
    :goto_1
    return-void

    .line 2930
    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2940
    :sswitch_0
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/service/EsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Liev;

    invoke-interface {v4}, Liev;->c()V

    :cond_1
    :goto_2
    const/4 v4, 0x1

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/service/EsService;->o:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/service/EsService;->stopSelfResult(I)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_2

    :sswitch_2
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "square_stream_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v4, "circle_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "view"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    new-instance v4, Ldio;

    invoke-direct/range {v4 .. v10}, Ldio;-><init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 2947
    :catch_1
    move-exception v4

    .line 2948
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 2949
    new-instance v5, Lfib;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7, v4}, Lfib;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v4}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto :goto_1

    .line 2940
    :sswitch_3
    :try_start_3
    const-string v4, "loc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Llae;

    new-instance v7, Ldin;

    invoke-direct {v7, v5, v6, v4}, Ldin;-><init>(Landroid/content/Context;ILlae;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto :goto_2

    :sswitch_4
    new-instance v4, Ldju;

    const-string v7, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Ldju;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto :goto_2

    :sswitch_5
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "reshare"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    new-instance v4, Ldjg;

    invoke-direct/range {v4 .. v9}, Ldjg;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_6
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "shown_plus_one_promo"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    const-string v4, "promoted_post_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v10

    new-instance v4, Ldmk;

    const/4 v8, 0x1

    invoke-direct/range {v4 .. v10}, Ldmk;-><init>(Landroid/content/Context;ILjava/lang/String;ZZ[B)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_7
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "promoted_post_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v10

    new-instance v4, Ldmk;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v10}, Ldmk;-><init>(Landroid/content/Context;ILjava/lang/String;ZZ[B)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_8
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "shown_plus_one_promo"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    new-instance v4, Ldoh;

    const/4 v9, 0x1

    invoke-direct/range {v4 .. v10}, Ldoh;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_9
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v4, Ldoh;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, Ldoh;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_a
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ldiw;

    invoke-direct {v7, v5, v6, v4}, Ldiw;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_b
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "mute_state"

    const/4 v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    new-instance v8, Ldlq;

    invoke-direct {v8, v5, v6, v4, v7}, Ldlq;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_c
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "disable_comments"

    const/4 v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    new-instance v8, Ldjd;

    invoke-direct {v8, v5, v6, v4, v7}, Ldjd;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_d
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "disable_reshares"

    const/4 v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    new-instance v8, Ldje;

    invoke-direct {v8, v5, v6, v4, v7}, Ldje;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_e
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ldnv;

    invoke-direct {v7, v5, v6, v4}, Ldnv;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_f
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "source_stream_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v4, Ldms;

    const/4 v9, 0x1

    invoke-direct/range {v4 .. v9}, Ldms;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_10
    const-string v4, "activity_id_list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const-string v4, "creation_source_list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    const-string v4, "timestamp"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    const-string v4, "mark_operation"

    const/4 v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    new-instance v4, Ldlf;

    const/4 v8, 0x0

    invoke-direct/range {v4 .. v11}, Ldlf;-><init>(Landroid/content/Context;I[Ljava/lang/String;ZIJ)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_11
    const-string v4, "resumetoken"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v4, "search_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v4, "view_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v4, "search_mode"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfgs;

    move-object/from16 v8, p0

    move-object v9, v5

    move v10, v6

    move-object/from16 v15, p1

    invoke-direct/range {v7 .. v15}, Lfgs;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_12
    const-string v4, "all_photos_force_refresh"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfgt;

    move-object/from16 v8, p0

    move-object v10, v5

    move v11, v6

    move-object/from16 v12, p1

    invoke-direct/range {v7 .. v12}, Lfgt;-><init>(Lcom/google/android/apps/plus/service/EsService;ZLandroid/content/Context;ILandroid/content/Intent;)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_13
    const-string v4, "all_photos_metadata_count"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    new-instance v4, Ljava/lang/Thread;

    new-instance v8, Lfgu;

    move-object/from16 v9, p0

    move-object v10, v5

    move v11, v6

    move-object/from16 v14, p1

    invoke-direct/range {v8 .. v14}, Lfgu;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;IJLandroid/content/Intent;)V

    invoke-direct {v4, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_14
    const-class v4, Lhei;

    invoke-static {v5, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    invoke-interface {v4, v6}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v7, "gaia_id"

    invoke-interface {v4, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "resumetoken"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lfvc;->i:Lfvc;

    invoke-virtual {v9}, Lfvc;->b()Z

    move-result v9

    if-eqz v9, :cond_3

    if-nez v8, :cond_3

    if-eqz v7, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const-string v4, "highlights_force_refresh"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfgv;

    move-object/from16 v8, p0

    move-object v10, v5

    move v11, v6

    move-object/from16 v12, p1

    invoke-direct/range {v7 .. v12}, Lfgv;-><init>(Lcom/google/android/apps/plus/service/EsService;ZLandroid/content/Context;ILandroid/content/Intent;)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :cond_3
    new-instance v4, Ldkp;

    invoke-direct {v4, v5, v6, v7, v8}, Ldkp;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_15
    const-string v4, "resumetoken"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ldih;

    invoke-direct {v7, v5, v6, v4}, Ldih;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_16
    const-string v4, "resumetoken"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v4, Ldoi;

    const/4 v9, 0x1

    invoke-direct/range {v4 .. v9}, Ldoi;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_17
    const-string v4, "collection_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "auth_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v4, Ldip;

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct/range {v4 .. v10}, Ldip;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_18
    const-string v4, "collection_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "auth_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ldix;

    invoke-direct {v8, v5, v6, v4, v7}, Ldix;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_19
    const-string v4, "album_tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "collection_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "auth_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "album_title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    new-instance v4, Ldob;

    invoke-direct/range {v4 .. v10}, Ldob;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_1a
    const-string v4, "album_tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "collection_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v4, Ldnb;

    invoke-direct/range {v4 .. v9}, Ldnb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_1b
    const-string v4, "collection_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "auth_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "undo"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    new-instance v4, Ldku;

    invoke-direct/range {v4 .. v9}, Ldku;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_1c
    const-string v4, "text"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v4, "photo_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "view_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v4, "auth_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    new-instance v4, Ldmb;

    invoke-direct/range {v4 .. v13}, Ldmb;-><init>(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_1d
    const-string v4, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "view_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v4, Ldiz;

    invoke-direct/range {v4 .. v9}, Ldiz;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_1e
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "is_undo"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v4, "abuse_type"

    const/4 v9, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    new-instance v4, Ldms;

    invoke-direct/range {v4 .. v11}, Ldms;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZLjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_1f
    const-string v4, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v7, 0x0

    const/16 v4, 0x23

    invoke-virtual {v8, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-lez v4, :cond_4

    const/4 v7, 0x0

    invoke-virtual {v8, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    :cond_4
    new-instance v4, Ldjh;

    invoke-direct/range {v4 .. v10}, Ldjh;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_20
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v4, "view_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v4, "photo_id"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v4, "plus_oned"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    new-instance v4, Ldmg;

    invoke-direct/range {v4 .. v12}, Ldmg;-><init>(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_21
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "album_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x0

    const-string v8, "ALBUM"

    invoke-static {v4, v7, v9, v8}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x3

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    invoke-static {v8, v10}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v4, Ldmf;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-direct/range {v4 .. v12}, Ldmf;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_22
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v4, Ldme;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v4 .. v9}, Ldme;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_23
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "photo_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    const-string v4, "shape_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    const-string v4, "approved"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "view_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "suggested_gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v4, "is_suggested"

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    const-string v4, "collection_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    new-instance v4, Ldmd;

    invoke-direct/range {v4 .. v17}, Ldmd;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;ZLjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_24
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v4, "photo_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v4, "shape_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "shape_id"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    :goto_3
    const-string v4, "bounds"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v17

    check-cast v17, Landroid/graphics/RectF;

    const-string v4, "taggee_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v4, "taggee_email"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v4, "taggee_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v4, "view_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v4, "collection_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    new-instance v4, Ldly;

    invoke-direct/range {v4 .. v17}, Ldly;-><init>(Landroid/content/Context;IJLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_5
    const/4 v9, 0x0

    goto :goto_3

    :sswitch_25
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v4, "photo_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v4, "shape_id"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v4, "view_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v4, "retain_shape"

    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    new-instance v4, Ldlz;

    invoke-direct/range {v4 .. v14}, Ldlz;-><init>(Landroid/content/Context;IJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_26
    const-string v4, "tile_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    const-string v4, "delete_duplicates"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    const-string v4, "delete_type"

    const/4 v9, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    new-instance v4, Ldjc;

    invoke-direct/range {v4 .. v9}, Ldjc;-><init>(Landroid/content/Context;ILjava/util/List;ZI)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_27
    const-string v4, "local_uris"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v8, v7

    :goto_4
    const/4 v7, 0x0

    if-eqz v4, :cond_16

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_16

    invoke-static {v5, v4}, Lcsa;->a(Landroid/content/Context;Ljava/util/List;)I

    move-result v4

    :goto_5
    new-instance v7, Lfib;

    if-ne v4, v8, :cond_7

    const/4 v4, 0x1

    :goto_6
    invoke-direct {v7, v4}, Lfib;-><init>(Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7, v4}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_6
    const/4 v7, 0x0

    move v8, v7

    goto :goto_4

    :cond_7
    const/4 v4, 0x0

    goto :goto_6

    :sswitch_28
    const-string v4, "bucket_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-static {v5}, Lcsa;->a(Landroid/content/Context;)V

    :goto_7
    new-instance v4, Lfib;

    const/4 v7, 0x1

    invoke-direct {v4, v7}, Lfib;-><init>(Z)V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_8
    invoke-static {v5, v4}, Lcsa;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_7

    :sswitch_29
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "photo_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    new-instance v4, Ldkg;

    invoke-direct/range {v4 .. v9}, Ldkg;-><init>(Landroid/content/Context;ILjava/lang/String;J)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_2a
    const-string v4, "photos_settings"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    new-instance v7, Lnyq;

    invoke-direct {v7}, Lnyq;-><init>()V

    invoke-static {v7, v4}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Lnyq;

    new-instance v7, Ldni;

    invoke-direct {v7, v5, v6, v4}, Ldni;-><init>(Landroid/content/Context;ILnyq;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_2b
    const-string v4, "edit_info"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    if-nez v4, :cond_9

    const/4 v10, 0x0

    :goto_8
    const-string v4, "photo_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "set_edit_list_data"

    const/4 v11, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    new-instance v4, Ldng;

    invoke-direct/range {v4 .. v11}, Ldng;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnzi;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_9
    new-instance v7, Lnzi;

    invoke-direct {v7}, Lnzi;-><init>()V

    invoke-static {v7, v4}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Lnzi;

    move-object v10, v4

    goto :goto_8

    :sswitch_2c
    const-string v4, "photo_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "coordinates"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/graphics/RectF;

    const-string v4, "rotation"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    const-string v4, "is_gallery_photo"

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    new-instance v4, Ldnk;

    invoke-direct/range {v4 .. v10}, Ldnk;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/graphics/RectF;IZ)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_2d
    const-string v4, "cover_photo_owner_type"

    const/high16 v7, -0x80000000

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v4, "photo_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "top_offset"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    const-string v4, "coordinates"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    const-string v10, "rotation"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/service/EsService;->p:Lfhb;

    new-instance v9, Lnjt;

    invoke-direct {v9}, Lnjt;-><init>()V

    new-instance v11, Lnib;

    invoke-direct {v11}, Lnib;-><init>()V

    iput-object v11, v9, Lnjt;->d:Lnib;

    iget-object v11, v9, Lnjt;->d:Lnib;

    new-instance v12, Lnjo;

    invoke-direct {v12}, Lnjo;-><init>()V

    iput-object v12, v11, Lnib;->h:Lnjo;

    iget-object v11, v9, Lnjt;->d:Lnib;

    iget-object v11, v11, Lnib;->h:Lnjo;

    const/4 v12, 0x3

    iput v12, v11, Lnjo;->a:I

    iget-object v11, v9, Lnjt;->d:Lnib;

    iget-object v11, v11, Lnib;->h:Lnjo;

    new-instance v12, Lnjq;

    invoke-direct {v12}, Lnjq;-><init>()V

    iput-object v12, v11, Lnjo;->b:Lnjq;

    iget-object v11, v9, Lnjt;->d:Lnib;

    iget-object v11, v11, Lnib;->h:Lnjo;

    iget-object v11, v11, Lnjo;->b:Lnjq;

    new-instance v12, Lnjp;

    invoke-direct {v12}, Lnjp;-><init>()V

    iput-object v12, v11, Lnjq;->d:Lnjp;

    iget-object v11, v9, Lnjt;->d:Lnib;

    iget-object v11, v11, Lnib;->h:Lnjo;

    iget-object v11, v11, Lnjo;->b:Lnjq;

    iget-object v11, v11, Lnjq;->d:Lnjp;

    iget v12, v4, Landroid/graphics/RectF;->left:F

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    iput-object v12, v11, Lnjp;->b:Ljava/lang/Float;

    iget-object v11, v9, Lnjt;->d:Lnib;

    iget-object v11, v11, Lnib;->h:Lnjo;

    iget-object v11, v11, Lnjo;->b:Lnjq;

    iget-object v11, v11, Lnjq;->d:Lnjp;

    iget v12, v4, Landroid/graphics/RectF;->top:F

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    iput-object v12, v11, Lnjp;->a:Ljava/lang/Float;

    iget-object v11, v9, Lnjt;->d:Lnib;

    iget-object v11, v11, Lnib;->h:Lnjo;

    iget-object v11, v11, Lnjo;->b:Lnjq;

    iget-object v11, v11, Lnjq;->d:Lnjp;

    iget v12, v4, Landroid/graphics/RectF;->right:F

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    iput-object v12, v11, Lnjp;->d:Ljava/lang/Float;

    iget-object v11, v9, Lnjt;->d:Lnib;

    iget-object v11, v11, Lnib;->h:Lnjo;

    iget-object v11, v11, Lnjo;->b:Lnjq;

    iget-object v11, v11, Lnjq;->d:Lnjp;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v11, Lnjp;->c:Ljava/lang/Float;

    iget-object v4, v9, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    iput-object v8, v4, Lnjq;->a:Ljava/lang/String;

    iget-object v4, v9, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    iput v7, v4, Lnjq;->c:I

    iget-object v4, v9, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v4, Lnjq;->e:Ljava/lang/Integer;

    new-instance v4, Ldln;

    invoke-direct {v4, v5, v6, v9}, Ldln;-><init>(Landroid/content/Context;ILnjt;)V

    :goto_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_a
    const-string v4, "layout"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    new-instance v7, Lnjt;

    invoke-direct {v7}, Lnjt;-><init>()V

    new-instance v4, Lnib;

    invoke-direct {v4}, Lnib;-><init>()V

    iput-object v4, v7, Lnjt;->d:Lnib;

    iget-object v4, v7, Lnjt;->d:Lnib;

    new-instance v10, Lnjo;

    invoke-direct {v10}, Lnjo;-><init>()V

    iput-object v10, v4, Lnib;->h:Lnjo;

    iget-object v4, v7, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    const/4 v10, 0x2

    iput v10, v4, Lnjo;->a:I

    iget-object v4, v7, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    new-instance v10, Lnjq;

    invoke-direct {v10}, Lnjq;-><init>()V

    iput-object v10, v4, Lnjo;->b:Lnjq;

    iget-object v4, v7, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    iput-object v8, v4, Lnjq;->a:Ljava/lang/String;

    iget-object v4, v7, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    new-instance v8, Lnjr;

    invoke-direct {v8}, Lnjr;-><init>()V

    iput-object v8, v4, Lnjq;->b:Lnjr;

    iget-object v4, v7, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    iget-object v4, v4, Lnjq;->b:Lnjr;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v4, Lnjr;->a:Ljava/lang/Integer;

    new-instance v4, Ldln;

    invoke-direct {v4, v5, v6, v7}, Ldln;-><init>(Landroid/content/Context;ILnjt;)V

    goto :goto_9

    :sswitch_2e
    invoke-interface {v4, v6}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v7, "gaia_id"

    invoke-interface {v4, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v4, "data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v12

    new-instance v7, Ldog;

    new-instance v9, Lkfo;

    invoke-direct {v9, v5, v6}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v11, "profile"

    move-object v8, v5

    invoke-direct/range {v7 .. v12}, Ldog;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;[B)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_2f
    const-string v4, "url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ldmw;

    new-instance v8, Lkfo;

    invoke-direct {v8, v5, v6}, Lkfo;-><init>(Landroid/content/Context;I)V

    invoke-direct {v7, v5, v8, v4}, Ldmw;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_30
    const-string v4, "media"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lizu;

    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lfgw;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v8, v0, v5, v4, v1}, Lfgw;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lizu;Landroid/content/Intent;)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_31
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "promoted_post_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v9

    new-instance v4, Ldmi;

    invoke-direct/range {v4 .. v9}, Ldmi;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[B)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_32
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "event_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "auth_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v4, "promoted_post_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v10

    new-instance v4, Ldmj;

    invoke-direct/range {v4 .. v10}, Ldmj;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_33
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v4, Ldjh;

    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, Ldjh;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_34
    const-string v4, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v4, Ldiz;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Ldiz;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_35
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "plus_oned"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    const-string v4, "tile_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    new-instance v4, Ldiq;

    invoke-direct/range {v4 .. v10}, Ldiq;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_36
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "enabled"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    new-instance v8, Ldnd;

    invoke-direct {v8, v5, v6, v4, v7}, Ldnd;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_37
    const/4 v4, 0x1

    invoke-static {v5, v6, v4}, Ldsf;->a(Landroid/content/Context;IZ)V

    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :sswitch_38
    const-string v4, "read_state"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v7, 0x0

    invoke-static {v5, v6, v4, v7}, Ldsf;->a(Landroid/content/Context;II[B)V

    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :sswitch_39
    const-string v4, "last_version"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v14

    const-wide/16 v8, 0x0

    cmp-long v4, v14, v8

    if-lez v4, :cond_b

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v7, p0

    move v8, v6

    invoke-static/range {v7 .. v12}, Ldsf;->a(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Z)V

    new-instance v4, Ldoe;

    invoke-direct {v4, v5, v6, v14, v15}, Ldoe;-><init>(Landroid/content/Context;IJ)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_b
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :sswitch_3a
    invoke-static {v5, v6}, Ldsf;->b(Landroid/content/Context;I)V

    const-string v4, "notif_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "notif_id"

    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v5, v6, v4}, Lfhu;->a(Landroid/content/Context;II)V

    :goto_a
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_c
    invoke-static {v5, v6}, Lfhu;->a(Landroid/content/Context;I)V

    goto :goto_a

    :sswitch_3b
    const-string v4, "notif_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    const-string v4, "notif_version"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v8

    const-string v4, "view_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_d

    new-instance v4, Ldnf;

    const/4 v9, 0x4

    const-string v10, "AST"

    invoke-direct/range {v4 .. v10}, Ldnf;-><init>(Landroid/content/Context;ILjava/util/List;[JILjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_d
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :sswitch_3c
    const-string v4, "read_state"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    const-string v4, "high_priority"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const-string v4, "init_fetch_param"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v14

    const-string v4, "user_initiated"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v5, v6, v12, v13, v4}, Ldsf;->a(Landroid/content/Context;IIIZ)Z

    move-result v4

    if-nez v4, :cond_e

    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_e
    const/4 v4, 0x2

    if-eq v12, v4, :cond_f

    const/4 v4, 0x2

    if-ne v13, v4, :cond_10

    :cond_f
    invoke-static {v5, v6, v12, v13}, Ldsf;->a(Landroid/content/Context;III)V

    :cond_10
    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfgx;

    move-object/from16 v8, p0

    move-object v9, v5

    move v10, v6

    move-object/from16 v11, p1

    invoke-direct/range {v7 .. v14}, Lfgx;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILandroid/content/Intent;II[B)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_3d
    const-string v4, "read_state"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    const-string v4, "high_priority"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const-string v4, "next_fetch_param"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v14

    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfgy;

    move-object/from16 v8, p0

    move-object v9, v5

    move v10, v6

    move-object/from16 v11, p1

    invoke-direct/range {v7 .. v14}, Lfgy;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILandroid/content/Intent;II[B)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_3e
    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v7, p0

    move v8, v6

    invoke-static/range {v7 .. v12}, Ldsf;->a(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Z)V

    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :sswitch_3f
    const-string v4, "place_cluster_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "star_rating"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    const-string v4, "review_text"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    new-instance v4, Ldoj;

    invoke-direct/range {v4 .. v10}, Ldoj;-><init>(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_40
    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfge;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v7, v0, v1, v5, v6}, Lfge;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;I)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_41
    const-string v4, "profile"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    new-instance v7, Lnjt;

    invoke-direct {v7}, Lnjt;-><init>()V

    invoke-static {v7, v4}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Lnjt;

    new-instance v7, Ldln;

    invoke-direct {v7, v5, v6, v4}, Ldln;-><init>(Landroid/content/Context;ILnjt;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_42
    invoke-interface {v4, v6}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v7, "gaia_id"

    invoke-interface {v4, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "profile"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    new-instance v8, Lnkf;

    invoke-direct {v8}, Lnkf;-><init>()V

    invoke-static {v8, v4}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Lnkf;

    new-instance v8, Ldlp;

    invoke-direct {v8, v5, v6, v7, v4}, Ldlp;-><init>(Landroid/content/Context;ILjava/lang/String;Lnkf;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_43
    const-string v4, "person_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "muted"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    sget-object v8, Lcom/google/android/apps/plus/service/EsService;->j:Ldvu;

    invoke-virtual {v8, v5, v6}, Ldvu;->a(Landroid/content/Context;I)Ldvt;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/plus/service/EsService;->k:Ldls;

    invoke-virtual {v9, v5, v6, v8}, Ldls;->a(Landroid/content/Context;ILdvt;)Ldlr;

    move-result-object v8

    invoke-virtual {v8, v4, v7}, Ldlr;->a(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_44
    const-string v4, "person_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "person_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "blocked"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    sget-object v9, Lcom/google/android/apps/plus/service/EsService;->j:Ldvu;

    invoke-virtual {v9, v5, v6}, Ldvu;->a(Landroid/content/Context;I)Ldvt;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->l:Ldil;

    invoke-virtual {v10, v5, v6, v9}, Ldil;->a(Landroid/content/Context;ILdvt;)Ldik;

    move-result-object v9

    invoke-virtual {v9, v4, v7, v8}, Ldik;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v9, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_45
    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfgf;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v7, v0, v5, v6, v1}, Lfgf;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;ILandroid/content/Intent;)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_46
    const-string v4, "owner_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "photo_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v4, "abuse_type"

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    if-lez v10, :cond_11

    const-wide/16 v12, 0x0

    cmp-long v4, v8, v12

    if-lez v4, :cond_11

    if-nez v7, :cond_14

    :cond_11
    const-string v4, "EsService"

    const/4 v11, 0x5

    invoke-static {v4, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_13

    const-string v4, "Invalid report abuse params. PhotoId:"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-nez v7, :cond_12

    const-string v7, "null"

    :cond_12
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x35

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ", ownerId:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", abuseType:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_13
    new-instance v4, Lfib;

    invoke-direct {v4}, Lfib;-><init>()V

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_14
    new-instance v4, Ldmt;

    invoke-direct/range {v4 .. v10}, Ldmt;-><init>(Landroid/content/Context;ILjava/lang/String;JI)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_47
    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfgg;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v7, v0, v1, v5, v6}, Lfgg;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;I)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_48
    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lfgh;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v7, v0, v1, v5, v6}, Lfgh;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;I)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :sswitch_49
    const-string v4, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ldjf;

    invoke-direct {v8, v5, v6, v7, v4}, Ldjf;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_4a
    const-string v4, "moviemaker_edits"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Ljeo;

    new-instance v7, Ldlb;

    invoke-direct {v7, v5, v6, v4}, Ldlb;-><init>(Landroid/content/Context;ILjeo;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lkff;Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 2943
    :cond_15
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v5, v6, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILandroid/content/Intent;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2946
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x20

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unsupported op code: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_16
    move v4, v7

    goto/16 :goto_5

    .line 2940
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xc -> :sswitch_4
        0xe -> :sswitch_5
        0x10 -> :sswitch_6
        0x11 -> :sswitch_7
        0x12 -> :sswitch_b
        0x13 -> :sswitch_f
        0x14 -> :sswitch_a
        0x15 -> :sswitch_8
        0x16 -> :sswitch_9
        0x18 -> :sswitch_c
        0x19 -> :sswitch_10
        0x1b -> :sswitch_2
        0x1c -> :sswitch_3
        0x1e -> :sswitch_31
        0x1f -> :sswitch_32
        0x20 -> :sswitch_33
        0x21 -> :sswitch_34
        0x22 -> :sswitch_1e
        0x23 -> :sswitch_35
        0x24 -> :sswitch_36
        0x25 -> :sswitch_d
        0x26 -> :sswitch_e
        0x40 -> :sswitch_1f
        0x44 -> :sswitch_14
        0x45 -> :sswitch_15
        0x46 -> :sswitch_16
        0x48 -> :sswitch_21
        0x49 -> :sswitch_17
        0x4a -> :sswitch_18
        0x4b -> :sswitch_19
        0x4c -> :sswitch_29
        0x4e -> :sswitch_22
        0x4f -> :sswitch_20
        0x50 -> :sswitch_26
        0x51 -> :sswitch_23
        0x52 -> :sswitch_25
        0x53 -> :sswitch_24
        0x54 -> :sswitch_11
        0x55 -> :sswitch_1a
        0x56 -> :sswitch_1c
        0x58 -> :sswitch_2a
        0x59 -> :sswitch_1d
        0x5a -> :sswitch_1e
        0x5b -> :sswitch_2b
        0x5e -> :sswitch_1b
        0x5f -> :sswitch_2f
        0x64 -> :sswitch_27
        0x66 -> :sswitch_28
        0x67 -> :sswitch_30
        0x68 -> :sswitch_4a
        0x69 -> :sswitch_12
        0x6a -> :sswitch_13
        0xca -> :sswitch_3c
        0xce -> :sswitch_3a
        0xd2 -> :sswitch_39
        0xd3 -> :sswitch_3d
        0xd4 -> :sswitch_3e
        0xd6 -> :sswitch_37
        0xd7 -> :sswitch_38
        0xd8 -> :sswitch_3b
        0x2bf -> :sswitch_40
        0x2c0 -> :sswitch_41
        0x2c1 -> :sswitch_43
        0x2c2 -> :sswitch_44
        0x2c3 -> :sswitch_45
        0x2c7 -> :sswitch_2c
        0x2c9 -> :sswitch_2e
        0x2ce -> :sswitch_2d
        0x2cf -> :sswitch_42
        0x2d0 -> :sswitch_47
        0x2d1 -> :sswitch_48
        0x2d2 -> :sswitch_49
        0x2d3 -> :sswitch_46
        0x2d5 -> :sswitch_3f
        0x2715 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Landroid/content/Intent;Lfib;Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 4448
    invoke-static {}, Llsx;->b()V

    .line 4450
    const-string v0, "op"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 4452
    const-string v1, "rid"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 4454
    sparse-switch v0, :sswitch_data_0

    .line 5340
    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onIntentProcessed: Unhandled op code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5345
    :cond_0
    :goto_0
    :sswitch_0
    const-string v0, "rid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "rid"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->c:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v0, "from_pool"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->e:Lfhp;

    invoke-virtual {v0, p1}, Lfhp;->a(Landroid/content/Intent;)V

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService;->q:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->m:Lfgz;

    .line 5346
    return-void

    .line 4457
    :sswitch_1
    if-eqz p3, :cond_4

    .line 4458
    check-cast p3, Ldio;

    invoke-virtual {p3}, Ldio;->a()Z

    move-result v0

    move v2, v0

    .line 4463
    :goto_1
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4464
    const-string v4, "account_id"

    const/4 v5, -0x1

    .line 4465
    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4464
    invoke-virtual {v0, v1, v2}, Lfhh;->a(IZ)V

    goto :goto_2

    .line 4460
    :cond_4
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    .line 4473
    :sswitch_2
    if-eqz p3, :cond_5

    .line 4474
    check-cast p3, Ldin;

    invoke-virtual {p3}, Ldin;->a()Z

    move-result v0

    move v2, v0

    .line 4479
    :goto_3
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4480
    const-string v4, "account_id"

    const/4 v5, -0x1

    .line 4481
    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4480
    invoke-virtual {v0, v1, v2}, Lfhh;->b(IZ)V

    goto :goto_4

    .line 4476
    :cond_5
    const/4 v0, 0x0

    move v2, v0

    goto :goto_3

    .line 4489
    :sswitch_3
    if-eqz p3, :cond_6

    .line 4490
    check-cast p3, Ldju;

    invoke-virtual {p3}, Ldju;->b()Lhgw;

    move-result-object v0

    move-object v2, v0

    .line 4494
    :goto_5
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4495
    const-string v4, "account_id"

    const/4 v5, -0x1

    .line 4496
    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v4, "aid"

    .line 4497
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4495
    invoke-virtual {v0, v1, v2, p2}, Lfhh;->a(ILhgw;Lfib;)V

    goto :goto_6

    .line 4492
    :cond_6
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_5

    .line 4504
    :sswitch_4
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4505
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4506
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4507
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4505
    invoke-virtual {v0, v1, p2}, Lfhh;->p(ILfib;)V

    goto :goto_7

    .line 4514
    :sswitch_5
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4515
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4516
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4517
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4515
    invoke-virtual {v0, v1, p2}, Lfhh;->U(ILfib;)V

    goto :goto_8

    .line 4524
    :sswitch_6
    check-cast p3, Ldlq;

    .line 4525
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4526
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4527
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4528
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4529
    invoke-virtual {p3}, Ldlq;->b()Z

    move-result v3

    .line 4526
    invoke-virtual {v0, v1, v3, p2}, Lfhh;->b(IZLfib;)V

    goto :goto_9

    .line 4536
    :sswitch_7
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4537
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4538
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4539
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "disable_comments"

    const/4 v4, 0x0

    .line 4540
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 4537
    invoke-virtual {v0, v1, v3, p2}, Lfhh;->c(IZLfib;)V

    goto :goto_a

    .line 4547
    :sswitch_8
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4548
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4549
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4550
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "disable_reshares"

    const/4 v4, 0x0

    .line 4551
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 4548
    invoke-virtual {v0, v1, v3, p2}, Lfhh;->d(IZLfib;)V

    goto :goto_b

    .line 4558
    :sswitch_9
    check-cast p3, Ldnv;

    .line 4559
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_c
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4560
    const-string v2, "account_id"

    const/4 v3, -0x1

    .line 4561
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4562
    invoke-virtual {p3}, Ldnv;->d()Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {p3}, Ldnv;->e()Landroid/text/Spanned;

    move-result-object v4

    .line 4563
    invoke-virtual {p3}, Ldnv;->f()Ljava/util/HashMap;

    move-result-object v5

    move-object v2, p2

    .line 4560
    invoke-virtual/range {v0 .. v5}, Lfhh;->a(ILfib;Landroid/text/Spanned;Landroid/text/Spanned;Ljava/util/HashMap;)V

    goto :goto_c

    .line 4569
    :sswitch_a
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4570
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4571
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4572
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4570
    invoke-virtual {v0, v1, p2}, Lfhh;->I(ILfib;)V

    goto :goto_d

    .line 4579
    :sswitch_b
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4580
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4581
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4580
    invoke-virtual {v0, v1, p2}, Lfhh;->x(ILfib;)V

    goto :goto_e

    .line 4588
    :sswitch_c
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4589
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4590
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4589
    invoke-virtual {v0, v1, p2}, Lfhh;->y(ILfib;)V

    goto :goto_f

    .line 4597
    :sswitch_d
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4598
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4599
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4598
    invoke-virtual {v0, v1, p2}, Lfhh;->A(ILfib;)V

    goto :goto_10

    .line 4606
    :sswitch_e
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4607
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4608
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4607
    invoke-virtual {v0, v1, p2}, Lfhh;->z(ILfib;)V

    goto :goto_11

    .line 4615
    :sswitch_f
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4616
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4617
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4616
    invoke-virtual {v0, v1, p2}, Lfhh;->O(ILfib;)V

    goto :goto_12

    .line 4624
    :sswitch_10
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4625
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4626
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4625
    invoke-virtual {v0, v1, p2}, Lfhh;->u(ILfib;)V

    goto :goto_13

    .line 4633
    :sswitch_11
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4634
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4635
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4634
    invoke-virtual {v0, v1, p2}, Lfhh;->w(ILfib;)V

    goto :goto_14

    .line 4642
    :sswitch_12
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4643
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4644
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4643
    invoke-virtual {v0, v1, p2}, Lfhh;->r(ILfib;)V

    goto :goto_15

    .line 4651
    :sswitch_13
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4652
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4653
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4652
    invoke-virtual {v0, v1, p2}, Lfhh;->a_(ILfib;)V

    goto :goto_16

    .line 4659
    :sswitch_14
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_17
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4660
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4661
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4660
    invoke-virtual {v0, v1, p2}, Lfhh;->b(ILfib;)V

    goto :goto_17

    .line 4668
    :sswitch_15
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4669
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4670
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "comment_id"

    .line 4671
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "is_undo"

    const/4 v4, 0x0

    .line 4672
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 4669
    invoke-virtual {v0, v1, p2}, Lfhh;->c(ILfib;)V

    goto :goto_18

    .line 4679
    :sswitch_16
    const-string v0, "plus_oned"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 4680
    const-string v0, "account_id"

    const/4 v3, -0x1

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 4681
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_19
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4682
    invoke-virtual {v0, v1, v3, v2, p2}, Lfhh;->a(IIZLfib;)V

    goto :goto_19

    .line 4688
    :sswitch_17
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4689
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4690
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4689
    invoke-virtual {v0, v1, p2}, Lfhh;->v(ILfib;)V

    goto :goto_1a

    .line 4696
    :sswitch_18
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4697
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4698
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4697
    invoke-virtual {v0, v1, p2}, Lfhh;->P(ILfib;)V

    goto :goto_1b

    .line 4705
    :sswitch_19
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4706
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4707
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4706
    invoke-virtual {v0, v1, p2}, Lfhh;->e(ILfib;)V

    goto :goto_1c

    .line 4713
    :sswitch_1a
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4714
    const-string v2, "account_id"

    const/4 v4, -0x1

    .line 4715
    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v2, "shape_id"

    const-wide/16 v4, 0x0

    .line 4716
    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_7

    const-string v2, "shape_id"

    const-wide/16 v4, 0x0

    .line 4717
    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 4714
    :goto_1e
    invoke-virtual {v0, v1, p2, v2}, Lfhh;->a(ILfib;Ljava/lang/Long;)V

    goto :goto_1d

    .line 4717
    :cond_7
    const/4 v2, 0x0

    goto :goto_1e

    .line 4723
    :sswitch_1b
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4724
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4725
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4724
    invoke-virtual {v0, v1, p2}, Lfhh;->b_(ILfib;)V

    goto :goto_1f

    .line 4731
    :sswitch_1c
    const-string v0, "account_id"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4733
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_20
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4734
    invoke-virtual {v0, v1, p2}, Lfhh;->j(ILfib;)V

    goto :goto_20

    .line 4740
    :sswitch_1d
    const-string v0, "account_id"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4742
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4743
    invoke-virtual {v0, v1, p2}, Lfhh;->K(ILfib;)V

    goto :goto_21

    .line 4750
    :sswitch_1e
    const-string v0, "account_id"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4752
    const/4 v0, 0x1

    .line 4754
    if-eqz p3, :cond_e

    .line 4755
    check-cast p3, Ldjc;

    .line 4757
    invoke-virtual {p3}, Ldjc;->b()I

    move-result v0

    move v2, v0

    .line 4760
    :goto_22
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_23
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4761
    packed-switch v2, :pswitch_data_0

    goto :goto_23

    .line 4768
    :pswitch_0
    invoke-virtual {v0, v1, p2}, Lfhh;->i(ILfib;)V

    goto :goto_23

    .line 4764
    :pswitch_1
    invoke-virtual {v0, v1, p2}, Lfhh;->h(ILfib;)V

    goto :goto_23

    .line 4772
    :pswitch_2
    invoke-virtual {v0, v1, p2}, Lfhh;->g(ILfib;)V

    goto :goto_23

    .line 4781
    :sswitch_1f
    const-string v0, "account_id"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4782
    const-string v0, "album_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4783
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4784
    invoke-virtual {v0, v1, p2}, Lfhh;->Q(ILfib;)V

    goto :goto_24

    .line 4790
    :sswitch_20
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_25
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 4791
    const-string v1, "account_id"

    const/4 v2, -0x1

    .line 4792
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    goto :goto_25

    .line 4799
    :sswitch_21
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4800
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4801
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4800
    invoke-virtual {v0, v1, p2}, Lfhh;->R(ILfib;)V

    goto :goto_26

    .line 4808
    :sswitch_22
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4809
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4810
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4809
    invoke-virtual {v0, v1, p2}, Lfhh;->S(ILfib;)V

    goto :goto_27

    .line 4817
    :sswitch_23
    const-string v0, "account_id"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4818
    const-string v0, "photo_id"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 4820
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4821
    invoke-virtual {v0, v1, p2}, Lfhh;->f(ILfib;)V

    goto :goto_28

    .line 4830
    :sswitch_24
    const-string v0, "account_id"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4832
    const/4 v0, 0x0

    .line 4833
    if-eqz p3, :cond_d

    .line 4834
    check-cast p3, Ldni;

    .line 4836
    invoke-virtual {p3}, Ldni;->e()Lnyq;

    move-result-object v0

    move-object v2, v0

    .line 4839
    :goto_29
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4840
    invoke-virtual {v0, v1, v2, p2}, Lfhh;->a(ILnyq;Lfib;)V

    goto :goto_2a

    .line 4847
    :sswitch_25
    const/4 v0, 0x0

    .line 4848
    if-eqz p3, :cond_c

    .line 4849
    check-cast p3, Ldng;

    .line 4850
    invoke-virtual {p3}, Ldng;->d()Lnzi;

    move-result-object v2

    .line 4851
    new-instance v0, Lffn;

    invoke-virtual {p3}, Ldng;->e()Z

    move-result v3

    invoke-direct {v0, v3}, Lffn;-><init>(Z)V

    .line 4852
    if-eqz v2, :cond_8

    .line 4853
    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lffn;->a([B)V

    :cond_8
    move-object v2, v0

    .line 4858
    :goto_2b
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4859
    const-string v4, "account_id"

    const/4 v5, -0x1

    .line 4860
    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4859
    invoke-virtual {v0, v1, v2}, Lfhh;->c_(ILfib;)V

    goto :goto_2c

    :cond_9
    move-object p2, v2

    .line 4863
    goto/16 :goto_0

    .line 4867
    :sswitch_26
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4868
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4869
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4870
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4868
    invoke-virtual {v0, v1, p2}, Lfhh;->T(ILfib;)V

    goto :goto_2d

    .line 4876
    :sswitch_27
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4877
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4878
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4877
    invoke-virtual {v0, v1, p2}, Lfhh;->F(ILfib;)V

    goto :goto_2e

    .line 4884
    :sswitch_28
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4885
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4886
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4887
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "comment_id"

    .line 4888
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4885
    invoke-virtual {v0, v1, p2}, Lfhh;->q(ILfib;)V

    goto :goto_2f

    .line 4895
    :sswitch_29
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_30
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4896
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4897
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4898
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "comment_id"

    .line 4899
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4896
    invoke-virtual {v0, v1, p2}, Lfhh;->V(ILfib;)V

    goto :goto_30

    .line 4906
    :sswitch_2a
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4907
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4908
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4909
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "comment_id"

    .line 4910
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "is_undo"

    const/4 v4, 0x0

    .line 4911
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 4907
    invoke-virtual {v0, v1, p2}, Lfhh;->W(ILfib;)V

    goto :goto_31

    .line 4918
    :sswitch_2b
    const-string v0, "plus_oned"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 4919
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4920
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 4921
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 4922
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "comment_id"

    .line 4923
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4920
    invoke-virtual {v0, v1, p2}, Lfhh;->a(ZLfib;)V

    goto :goto_32

    .line 4930
    :sswitch_2c
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_33
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4931
    invoke-virtual {v0, v1}, Lfhh;->a(I)V

    goto :goto_33

    .line 4938
    :sswitch_2d
    const-string v0, "account_id"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4939
    const-string v0, "aid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4940
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_34
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4941
    invoke-virtual {v0, v1, p2}, Lfhh;->C(ILfib;)V

    goto :goto_34

    .line 4948
    :sswitch_2e
    const-string v0, "account_id"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 4949
    const-string v0, "aid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4950
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_35
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4951
    invoke-virtual {v0, v1, p2}, Lfhh;->D(ILfib;)V

    goto :goto_35

    .line 4957
    :sswitch_2f
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_36
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4958
    invoke-virtual {v0, v1, p2}, Lfhh;->ae(ILfib;)V

    goto :goto_36

    .line 4964
    :sswitch_30
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_37
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4965
    invoke-virtual {v0, v1, p2}, Lfhh;->B(ILfib;)V

    goto :goto_37

    .line 4971
    :sswitch_31
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_38
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4972
    invoke-virtual {v0, v1, p2}, Lfhh;->af(ILfib;)V

    goto :goto_38

    .line 4978
    :sswitch_32
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_39
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 4979
    invoke-virtual {v0, v1, p2}, Lfhh;->J(ILfib;)V

    goto :goto_39

    .line 4986
    :sswitch_33
    const/4 p2, 0x0

    .line 4987
    goto/16 :goto_0

    .line 4998
    :sswitch_34
    const/4 p2, 0x0

    .line 4999
    goto/16 :goto_0

    .line 5003
    :sswitch_35
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5004
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5005
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5004
    invoke-virtual {v0, v1, p2}, Lfhh;->ac(ILfib;)V

    goto :goto_3a

    .line 5009
    :cond_a
    const/4 p2, 0x0

    .line 5010
    goto/16 :goto_0

    .line 5014
    :sswitch_36
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5015
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5016
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5015
    invoke-virtual {v0, v1, p2}, Lfhh;->ad(ILfib;)V

    goto :goto_3b

    .line 5023
    :sswitch_37
    const/4 p2, 0x0

    .line 5024
    goto/16 :goto_0

    .line 5031
    :sswitch_38
    const/4 p2, 0x0

    .line 5032
    goto/16 :goto_0

    .line 5037
    :sswitch_39
    const/4 p2, 0x0

    .line 5038
    goto/16 :goto_0

    .line 5042
    :sswitch_3a
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5043
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5044
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5043
    invoke-virtual {v0, v1, p2}, Lfhh;->ab(ILfib;)V

    goto :goto_3c

    .line 5050
    :sswitch_3b
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5051
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5052
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "person_id"

    .line 5053
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 5051
    invoke-virtual {v0, v1, p2}, Lfhh;->l(ILfib;)V

    goto :goto_3d

    .line 5060
    :sswitch_3c
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5061
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5062
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5061
    invoke-virtual {v0, v1, p2}, Lfhh;->Z(ILfib;)V

    goto :goto_3e

    .line 5068
    :sswitch_3d
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5069
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5070
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5069
    invoke-virtual {v0, v1, p2}, Lfhh;->aa(ILfib;)V

    goto :goto_3f

    .line 5077
    :sswitch_3e
    const/4 p2, 0x0

    .line 5078
    goto/16 :goto_0

    .line 5082
    :sswitch_3f
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_40
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 5083
    const-string v1, "person_id"

    .line 5084
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "suggestion_id"

    .line 5085
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "aid"

    .line 5086
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "account_id"

    const/4 v2, -0x1

    .line 5087
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    goto :goto_40

    .line 5093
    :sswitch_40
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_41
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5094
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5095
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "muted"

    const/4 v4, 0x0

    .line 5096
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 5094
    invoke-virtual {v0, v1, v3, p2}, Lfhh;->a(IZLfib;)V

    goto :goto_41

    .line 5102
    :sswitch_41
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_42
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5103
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5104
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "person_id"

    .line 5105
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "blocked"

    const/4 v5, 0x0

    .line 5106
    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 5103
    invoke-virtual {v0, v1, v3, v4, p2}, Lfhh;->a(ILjava/lang/String;ZLfib;)V

    goto :goto_42

    .line 5113
    :sswitch_42
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_43
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5114
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5115
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5114
    invoke-virtual {v0, v1, p2}, Lfhh;->L(ILfib;)V

    goto :goto_43

    .line 5121
    :sswitch_43
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_44
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 5122
    const-string v1, "account_id"

    const/4 v2, -0x1

    .line 5123
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    goto :goto_44

    .line 5129
    :sswitch_44
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_45
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 5130
    const-string v1, "account_id"

    const/4 v2, -0x1

    .line 5131
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    goto :goto_45

    .line 5137
    :sswitch_45
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_46
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 5138
    const-string v1, "account_id"

    const/4 v2, -0x1

    .line 5139
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    goto :goto_46

    .line 5145
    :sswitch_46
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_47
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 5146
    const-string v1, "account_id"

    const/4 v2, -0x1

    .line 5147
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    goto :goto_47

    .line 5153
    :sswitch_47
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_48
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5154
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5155
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5154
    invoke-virtual {v0, v1, p2}, Lfhh;->s(ILfib;)V

    goto :goto_48

    .line 5163
    :sswitch_48
    const/4 p2, 0x0

    .line 5164
    goto/16 :goto_0

    .line 5168
    :sswitch_49
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_49
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5169
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5170
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 5169
    invoke-virtual {v0, v1, v3, p2}, Lfhh;->a(IILfib;)V

    goto :goto_49

    .line 5176
    :sswitch_4a
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5177
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5178
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5177
    invoke-virtual {v0, v1, p2}, Lfhh;->M(ILfib;)V

    goto :goto_4a

    .line 5184
    :sswitch_4b
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5185
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5186
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5185
    invoke-virtual {v0, v1, p2}, Lfhh;->N(ILfib;)V

    goto :goto_4b

    .line 5192
    :sswitch_4c
    const-string v0, "full_res"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 5193
    const-string v0, "description"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5195
    const/4 v2, 0x0

    .line 5196
    const/4 v5, 0x0

    .line 5197
    if-eqz p3, :cond_b

    .line 5198
    check-cast p3, Ldmw;

    .line 5199
    invoke-virtual {p3}, Ldmw;->d()Ljava/io/File;

    move-result-object v2

    .line 5200
    invoke-virtual {p3}, Ldmw;->O_()Ljava/lang/String;

    move-result-object v5

    .line 5203
    :cond_b
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    move-object v6, p2

    .line 5204
    invoke-virtual/range {v0 .. v6}, Lfhh;->a(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lfib;)V

    goto :goto_4c

    .line 5211
    :sswitch_4d
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    move-object v2, p3

    .line 5212
    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, p2, v2}, Lfhh;->a(ILfib;Landroid/graphics/Bitmap;)V

    goto :goto_4d

    .line 5218
    :sswitch_4e
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5219
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5220
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5219
    invoke-virtual {v0, v1, p2}, Lfhh;->m(ILfib;)V

    goto :goto_4e

    .line 5226
    :sswitch_4f
    const/4 p2, 0x0

    .line 5227
    goto/16 :goto_0

    .line 5231
    :sswitch_50
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5232
    invoke-virtual {v0, v1, p2}, Lfhh;->E(ILfib;)V

    goto :goto_4f

    .line 5238
    :sswitch_51
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_50
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5239
    invoke-virtual {v0, v1, p2}, Lfhh;->n(ILfib;)V

    goto :goto_50

    .line 5245
    :sswitch_52
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_51
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5246
    invoke-virtual {v0, v1, p2}, Lfhh;->o(ILfib;)V

    goto :goto_51

    .line 5252
    :sswitch_53
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_52
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5253
    invoke-virtual {v0, v1, p2}, Lfhh;->H(ILfib;)V

    goto :goto_52

    .line 5259
    :sswitch_54
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_53
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_53

    .line 5266
    :sswitch_55
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_54
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5267
    invoke-virtual {v0, v1, p2}, Lfhh;->G(ILfib;)V

    goto :goto_54

    .line 5273
    :sswitch_56
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_55
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5274
    invoke-virtual {v0, v1, p2}, Lfhh;->t(ILfib;)V

    goto :goto_55

    .line 5296
    :sswitch_57
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_56
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5297
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5298
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "gaia_id"

    .line 5299
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "aid"

    .line 5300
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 5297
    invoke-virtual {v0, v1, p2}, Lfhh;->X(ILfib;)V

    goto :goto_56

    .line 5307
    :sswitch_58
    check-cast p3, Ldma;

    .line 5309
    invoke-virtual {p3}, Ldma;->b()Ljava/lang/String;

    move-result-object v3

    .line 5310
    invoke-virtual {p3}, Ldma;->c()Z

    move-result v4

    .line 5311
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_57
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5312
    const-string v2, "account_id"

    const/4 v5, -0x1

    .line 5313
    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object v5, p2

    .line 5312
    invoke-virtual/range {v0 .. v5}, Lfhh;->a(IILjava/lang/String;ZLfib;)V

    goto :goto_57

    .line 5320
    :sswitch_59
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_58
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhh;

    .line 5321
    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 5322
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v3, "aid"

    .line 5323
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 5321
    invoke-virtual {v0, v1, p2}, Lfhh;->Y(ILfib;)V

    goto :goto_58

    .line 5330
    :sswitch_5a
    const/4 p2, 0x0

    .line 5331
    goto/16 :goto_0

    .line 5335
    :sswitch_5b
    const/4 p2, 0x0

    .line 5336
    goto/16 :goto_0

    :cond_c
    move-object v2, v0

    goto/16 :goto_2b

    :cond_d
    move-object v2, v0

    goto/16 :goto_29

    :cond_e
    move v2, v0

    goto/16 :goto_22

    .line 4454
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_39
        0xc -> :sswitch_3
        0xe -> :sswitch_4
        0x10 -> :sswitch_2d
        0x11 -> :sswitch_2e
        0x12 -> :sswitch_6
        0x13 -> :sswitch_a
        0x14 -> :sswitch_5
        0x15 -> :sswitch_2d
        0x16 -> :sswitch_2e
        0x18 -> :sswitch_7
        0x19 -> :sswitch_33
        0x1a -> :sswitch_34
        0x1b -> :sswitch_1
        0x1c -> :sswitch_2
        0x1d -> :sswitch_5a
        0x1e -> :sswitch_26
        0x1f -> :sswitch_27
        0x20 -> :sswitch_28
        0x21 -> :sswitch_29
        0x22 -> :sswitch_2a
        0x23 -> :sswitch_2b
        0x24 -> :sswitch_2c
        0x25 -> :sswitch_8
        0x26 -> :sswitch_9
        0x40 -> :sswitch_12
        0x44 -> :sswitch_d
        0x45 -> :sswitch_f
        0x46 -> :sswitch_10
        0x48 -> :sswitch_17
        0x49 -> :sswitch_11
        0x4a -> :sswitch_1f
        0x4b -> :sswitch_20
        0x4c -> :sswitch_23
        0x4e -> :sswitch_18
        0x4f -> :sswitch_16
        0x50 -> :sswitch_1e
        0x51 -> :sswitch_19
        0x52 -> :sswitch_1b
        0x53 -> :sswitch_1a
        0x54 -> :sswitch_e
        0x55 -> :sswitch_21
        0x56 -> :sswitch_13
        0x58 -> :sswitch_24
        0x59 -> :sswitch_14
        0x5a -> :sswitch_15
        0x5b -> :sswitch_25
        0x5e -> :sswitch_22
        0x5f -> :sswitch_4c
        0x63 -> :sswitch_58
        0x64 -> :sswitch_1c
        0x66 -> :sswitch_1d
        0x67 -> :sswitch_4d
        0x68 -> :sswitch_5b
        0x69 -> :sswitch_b
        0x6a -> :sswitch_c
        0xca -> :sswitch_35
        0xce -> :sswitch_34
        0xd2 -> :sswitch_34
        0xd3 -> :sswitch_36
        0xd4 -> :sswitch_38
        0xd6 -> :sswitch_34
        0xd7 -> :sswitch_34
        0xd8 -> :sswitch_37
        0x1f4 -> :sswitch_47
        0x1f7 -> :sswitch_48
        0x2bf -> :sswitch_3b
        0x2c0 -> :sswitch_3c
        0x2c1 -> :sswitch_40
        0x2c2 -> :sswitch_41
        0x2c3 -> :sswitch_42
        0x2c4 -> :sswitch_44
        0x2c5 -> :sswitch_46
        0x2c7 -> :sswitch_4a
        0x2c9 -> :sswitch_49
        0x2cc -> :sswitch_45
        0x2ce -> :sswitch_4b
        0x2cf -> :sswitch_3d
        0x2d0 -> :sswitch_3f
        0x2d3 -> :sswitch_43
        0x2d5 -> :sswitch_3a
        0x385 -> :sswitch_2f
        0x386 -> :sswitch_50
        0x387 -> :sswitch_51
        0x388 -> :sswitch_52
        0x38a -> :sswitch_54
        0x38b -> :sswitch_55
        0x38c -> :sswitch_53
        0x38d -> :sswitch_30
        0x38e -> :sswitch_31
        0x38f -> :sswitch_32
        0x3f1 -> :sswitch_56
        0x3f2 -> :sswitch_3e
        0x460 -> :sswitch_4e
        0x7d1 -> :sswitch_4f
        0x8fc -> :sswitch_0
        0x8fd -> :sswitch_0
        0x960 -> :sswitch_0
        0xa28 -> :sswitch_0
        0xa92 -> :sswitch_57
        0xc1c -> :sswitch_59
    .end sparse-switch

    .line 4761
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2913
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 2875
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 2878
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 2879
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    .line 2882
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->i:Lfhc;

    if-nez v0, :cond_1

    .line 2883
    const-class v0, Lkfd;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    .line 2884
    new-instance v1, Lfhc;

    invoke-direct {v1, v0}, Lfhc;-><init>(Lkfd;)V

    sput-object v1, Lcom/google/android/apps/plus/service/EsService;->i:Lfhc;

    .line 2887
    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->j:Ldvu;

    if-nez v0, :cond_2

    invoke-static {}, Ldvt;->a()Ldvu;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->j:Ldvu;

    invoke-static {}, Ldlr;->b()Ldls;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->k:Ldls;

    invoke-static {}, Ldik;->a()Ldil;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->l:Ldil;

    .line 2889
    :cond_2
    new-instance v0, Lfic;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->h:Landroid/os/Handler;

    const-string v2, "ServiceThread"

    invoke-direct {v0, v1, v2, p0}, Lfic;-><init>(Landroid/os/Handler;Ljava/lang/String;Lfif;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->n:Lfic;

    .line 2890
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->n:Lfic;

    invoke-virtual {v0}, Lfic;->start()V

    .line 2891
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 2904
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->n:Lfic;

    if-eqz v0, :cond_0

    .line 2905
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->n:Lfic;

    invoke-virtual {v0}, Lfic;->a()V

    .line 2906
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->n:Lfic;

    .line 2908
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 2909
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 2895
    if-eqz p1, :cond_0

    .line 2896
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->n:Lfic;

    invoke-virtual {v0, p1}, Lfic;->a(Landroid/content/Intent;)V

    .line 2898
    :cond_0
    iput p3, p0, Lcom/google/android/apps/plus/service/EsService;->o:I

    .line 2899
    const/4 v0, 0x2

    return v0
.end method
