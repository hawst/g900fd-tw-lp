.class public Lcom/google/android/apps/plus/service/EsSyncAdapterService;
.super Landroid/app/Service;
.source "PG"


# static fields
.field private static final a:Liwe;

.field private static final b:Liwe;

.field private static final c:Ljava/lang/Object;

.field private static d:Lfhk;

.field private static final e:Ljava/lang/Object;

.field private static f:Landroid/os/PowerManager$WakeLock;

.field private static g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lfhi;",
            ">;"
        }
    .end annotation
.end field

.field private static h:Lfhi;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 100
    new-instance v0, Liwf;

    invoke-direct {v0}, Liwf;-><init>()V

    const-wide/32 v2, 0x36ee80

    .line 101
    invoke-virtual {v0, v2, v3}, Liwf;->a(J)Liwf;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Liwf;->a()Liwe;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a:Liwe;

    .line 104
    new-instance v0, Liwf;

    invoke-direct {v0}, Liwf;-><init>()V

    const/4 v1, 0x1

    .line 105
    invoke-virtual {v0, v1}, Liwf;->a(Z)Liwf;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Liwf;->a()Liwe;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->b:Liwe;

    .line 108
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->c:Ljava/lang/Object;

    .line 109
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->d:Lfhk;

    .line 155
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->e:Ljava/lang/Object;

    .line 167
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->g:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 1304
    return-void
.end method

.method public static synthetic a(Lfhi;)Lfhi;
    .locals 0

    .prologue
    .line 95
    sput-object p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->h:Lfhi;

    return-object p0
.end method

.method static synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static a(Landroid/accounts/Account;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xe10

    const/4 v2, 0x1

    .line 259
    const/4 v0, 0x0

    .line 260
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {p0, v1}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 261
    if-eqz v1, :cond_1

    .line 262
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/PeriodicSync;

    .line 263
    iget-wide v4, v0, Landroid/content/PeriodicSync;->period:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    move v1, v2

    .line 264
    goto :goto_0

    .line 266
    :cond_0
    const-string v4, "com.google.android.apps.plus.content.EsProvider"

    iget-object v0, v0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    invoke-static {p0, v4, v0}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    move v1, v0

    .line 270
    :cond_2
    if-nez v1, :cond_3

    .line 271
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 272
    const-string v1, "sync_periodic"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 273
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {p0, v1, v0, v6, v7}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 276
    :cond_3
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 240
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 241
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 242
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 243
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v3

    .line 244
    const-string v4, "is_plus_page"

    invoke-interface {v3, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 245
    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 246
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 247
    invoke-static {v3}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/accounts/Account;)V

    goto :goto_0

    .line 249
    :cond_1
    const-string v4, "EsSyncAdapterService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 250
    const-string v4, "Can NOT ensurePeriodicSyncScheduled since accountName is empty.Account ID: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1a

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " Account Name: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 256
    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 331
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 332
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 333
    const-string v1, "android contacts"

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    return-void
.end method

.method public static a(Landroid/content/Context;IJ)V
    .locals 8

    .prologue
    .line 494
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 495
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 496
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 497
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.plus.sharequeueupdate"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 498
    const-string v3, "account"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 500
    const/4 v1, 0x0

    invoke-static {p0, p1, v2, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 501
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 502
    const/4 v2, 0x2

    .line 503
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v4, p2

    const-wide/16 v6, 0x7d0

    add-long/2addr v4, v6

    .line 502
    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 505
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 317
    const-class v0, Llex;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llex;

    .line 318
    invoke-virtual {v0, p1}, Llex;->a(Ljava/lang/String;)Lleu;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "No registered synclet for name %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {v0, v3, v4}, Llsk;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 322
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 323
    const-string v2, "ignore_settings"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 324
    const-string v1, "synclet_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-static {p2}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    .line 326
    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 328
    return-void

    :cond_0
    move v0, v2

    .line 319
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 199
    invoke-static {p0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 200
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 201
    return-void
.end method

.method public static a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 189
    invoke-static {p0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 190
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v1, v3}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 191
    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/accounts/Account;)V

    .line 192
    if-eqz p1, :cond_0

    .line 193
    if-eqz v0, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ignore_settings"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v2, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 195
    :cond_0
    return-void
.end method

.method public static synthetic a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 95
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v1, Ldxd;->i:Lief;

    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->f:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 12

    .prologue
    .line 282
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 283
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.plus.mandatorysync"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 284
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 285
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 287
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 289
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 292
    const-string v4, "last_mandatory_sync_ts"

    const-wide/16 v8, 0x0

    invoke-interface {v1, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 293
    const-wide/16 v8, 0x0

    cmp-long v1, v4, v8

    if-nez v1, :cond_1

    .line 294
    const/4 v1, 0x1

    .line 295
    const-wide/32 v4, 0x2932e00

    add-long/2addr v2, v4

    .line 306
    :goto_0
    if-eqz v1, :cond_0

    .line 308
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->c(Landroid/content/Context;)V

    .line 311
    :cond_0
    const/4 v1, 0x2

    const-wide/32 v4, 0x2932e00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 313
    return-void

    .line 297
    :cond_1
    const-wide/32 v8, 0x2932e00

    add-long/2addr v8, v4

    const-wide/16 v10, 0x2710

    add-long/2addr v10, v2

    cmp-long v1, v8, v10

    if-gtz v1, :cond_2

    .line 298
    const/4 v1, 0x1

    .line 299
    const-wide/32 v4, 0x2932e00

    add-long/2addr v2, v4

    goto :goto_0

    .line 301
    :cond_2
    const/4 v1, 0x0

    .line 302
    const-wide/32 v2, 0x2932e00

    add-long/2addr v2, v4

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;I)V
    .locals 11

    .prologue
    const/4 v10, 0x5

    const/4 v1, 0x0

    .line 351
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 352
    new-instance v3, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.plus.checkandengagesync"

    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 354
    const-class v2, Lhei;

    invoke-static {p0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhei;

    .line 355
    invoke-interface {v2, p1}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v4, "account_name"

    .line 356
    invoke-interface {v2, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 357
    const-string v4, "account"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 360
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 361
    const/16 v5, 0xb

    .line 362
    invoke-virtual {v2, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    add-int/lit8 v6, v6, 0xa

    .line 361
    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 363
    const/16 v5, 0xc

    const/16 v6, 0x3c

    invoke-virtual {v2, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-virtual {v4, v5, v2}, Ljava/util/Calendar;->set(II)V

    .line 366
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v2, v6, v8

    if-gez v2, :cond_0

    .line 367
    const/4 v2, 0x1

    invoke-virtual {v4, v10, v2}, Ljava/util/Calendar;->add(II)V

    .line 370
    :cond_0
    invoke-static {p0, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 371
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 373
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 375
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 207
    invoke-static {p0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 208
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 209
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 210
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->g:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhi;

    .line 211
    if-eqz v0, :cond_0

    .line 212
    iget-object v0, v0, Lfhi;->a:Lkfp;

    invoke-virtual {v0}, Lkfp;->b()V

    .line 213
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->g:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    :cond_0
    return-void
.end method

.method public static synthetic c()Liwe;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->b:Liwe;

    return-object v0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 378
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 379
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 380
    const-string v2, "last_mandatory_sync_ts"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 381
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 383
    const/4 v2, 0x0

    .line 384
    sget-object v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->e:Ljava/lang/Object;

    monitor-enter v3

    .line 385
    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->f:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 386
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 387
    const/4 v4, 0x1

    const-string v5, "mandatory_sync"

    invoke-virtual {v0, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->f:Landroid/os/PowerManager$WakeLock;

    .line 391
    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_2

    .line 392
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    move v0, v1

    .line 395
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    if-eqz v0, :cond_1

    .line 398
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$MandatorySyncService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 400
    :cond_1
    return-void

    .line 395
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 221
    invoke-static {p0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 222
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static synthetic d()Lfhi;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->h:Lfhi;

    return-object v0
.end method

.method static synthetic d(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 95
    const-class v0, Lher;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lher;

    const-class v1, Lhei;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    const-class v2, Liwc;

    invoke-static {p0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liwc;

    sget-object v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a:Liwe;

    invoke-interface {v2, v3}, Liwc;->a(Liwe;)V

    invoke-interface {v0}, Lher;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    :try_start_0
    invoke-interface {v1, v0}, Lhei;->a(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Lhei;->c(I)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-interface {v1, v5}, Lhei;->a(I)Lhej;

    move-result-object v5

    const-string v6, "is_managed_account"

    invoke-interface {v5, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "is_google_plus"

    invoke-interface {v5, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "gplus_no_mobile_tos"

    invoke-interface {v5, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    :cond_2
    const-string v6, "logged_out"

    invoke-interface {v5, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    const-string v5, "com.google.android.apps.plus.content.EsProvider"

    const/4 v6, 0x1

    invoke-static {v0, v5, v6}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "ignore_settings"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v6, "sync_mandatory"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v6, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v6, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/accounts/Account;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v5, "EsSyncAdapterService"

    const-string v6, "Cannot do mandatory sync for account "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_3
    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 337
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 338
    const-string v1, "ignore_settings"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 339
    const-string v1, "sync_check_and_engage"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 344
    invoke-static {p0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    .line 343
    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 345
    return-void
.end method

.method public static synthetic e()Ljava/util/Map;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->g:Ljava/util/Map;

    return-object v0
.end method

.method public static e(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 479
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 480
    const-string v1, "ignore_settings"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 481
    const-string v1, "sync_share_queue"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 486
    invoke-static {p0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    .line 485
    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 487
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->d:Lfhk;

    invoke-virtual {v0}, Lfhk;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 173
    sget-object v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->d:Lfhk;

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Lfhk;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lfhk;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->d:Lfhk;

    .line 177
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
