.class public Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;


# static fields
.field private static a:Landroid/graphics/Bitmap;

.field private static b:Landroid/graphics/Bitmap;

.field private static c:Landroid/graphics/Paint;

.field private static d:Landroid/graphics/Paint;

.field private static e:F

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:I

.field private static n:I

.field private static o:I


# instance fields
.field private A:Landroid/text/Spannable;

.field private B:I

.field private p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private q:Llip;

.field private r:Lhsn;

.field private s:Lljv;

.field private t:Lfwt;

.field private u:Lljg;

.field private v:Lljg;

.field private w:Llir;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 107
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    .line 119
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 123
    const v1, 0x7f0d02f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->e:F

    .line 125
    const v1, 0x7f0d02f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->f:I

    .line 127
    const v1, 0x7f0d02f8

    .line 128
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->g:I

    .line 129
    const v1, 0x7f0d02f9

    .line 130
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->i:I

    .line 131
    const v1, 0x7f0d02fa

    .line 132
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->h:I

    .line 133
    const v1, 0x7f0d02fb

    .line 134
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->j:I

    .line 135
    const v1, 0x7f0d02fc

    .line 136
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->k:I

    .line 137
    const v1, 0x7f0d02fd

    .line 138
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->l:I

    .line 139
    const v1, 0x7f0d02fe

    .line 140
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->m:I

    .line 141
    const v1, 0x7f0d02ff

    .line 142
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->n:I

    .line 143
    const v1, 0x7f0d0300

    .line 144
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->o:I

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a:Landroid/graphics/Bitmap;

    .line 148
    const v1, 0x7f020085

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->b:Landroid/graphics/Bitmap;

    .line 150
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 151
    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    const v2, 0x7f0b02fc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 152
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 154
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->c:Landroid/graphics/Paint;

    .line 108
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    .line 119
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 123
    const v1, 0x7f0d02f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->e:F

    .line 125
    const v1, 0x7f0d02f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->f:I

    .line 127
    const v1, 0x7f0d02f8

    .line 128
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->g:I

    .line 129
    const v1, 0x7f0d02f9

    .line 130
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->i:I

    .line 131
    const v1, 0x7f0d02fa

    .line 132
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->h:I

    .line 133
    const v1, 0x7f0d02fb

    .line 134
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->j:I

    .line 135
    const v1, 0x7f0d02fc

    .line 136
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->k:I

    .line 137
    const v1, 0x7f0d02fd

    .line 138
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->l:I

    .line 139
    const v1, 0x7f0d02fe

    .line 140
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->m:I

    .line 141
    const v1, 0x7f0d02ff

    .line 142
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->n:I

    .line 143
    const v1, 0x7f0d0300

    .line 144
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->o:I

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a:Landroid/graphics/Bitmap;

    .line 148
    const v1, 0x7f020085

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->b:Landroid/graphics/Bitmap;

    .line 150
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 151
    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    const v2, 0x7f0b02fc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 152
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 154
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->c:Landroid/graphics/Paint;

    .line 112
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    .line 119
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 123
    const v1, 0x7f0d02f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->e:F

    .line 125
    const v1, 0x7f0d02f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->f:I

    .line 127
    const v1, 0x7f0d02f8

    .line 128
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->g:I

    .line 129
    const v1, 0x7f0d02f9

    .line 130
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->i:I

    .line 131
    const v1, 0x7f0d02fa

    .line 132
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->h:I

    .line 133
    const v1, 0x7f0d02fb

    .line 134
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->j:I

    .line 135
    const v1, 0x7f0d02fc

    .line 136
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->k:I

    .line 137
    const v1, 0x7f0d02fd

    .line 138
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->l:I

    .line 139
    const v1, 0x7f0d02fe

    .line 140
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->m:I

    .line 141
    const v1, 0x7f0d02ff

    .line 142
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->n:I

    .line 143
    const v1, 0x7f0d0300

    .line 144
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->o:I

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a:Landroid/graphics/Bitmap;

    .line 148
    const v1, 0x7f020085

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->b:Landroid/graphics/Bitmap;

    .line 150
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 151
    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    const v2, 0x7f0b02fc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 152
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 154
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->c:Landroid/graphics/Paint;

    .line 116
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 429
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->c()V

    .line 432
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->u:Lljg;

    .line 433
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->v:Lljg;

    .line 434
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 438
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->q:Llip;

    .line 439
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->A:Landroid/text/Spannable;

    .line 441
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->r:Lhsn;

    .line 442
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->s:Lljv;

    .line 444
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->x:Ljava/lang/String;

    .line 445
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->y:Ljava/lang/String;

    .line 446
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->z:Ljava/lang/String;

    .line 447
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->B:I

    .line 448
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 279
    invoke-static {v0, p1, p2}, Llhu;->b(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->z:Ljava/lang/String;

    .line 280
    return-void
.end method

.method public a(Lhsn;)V
    .locals 2

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->r:Lhsn;

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->r:Lhsn;

    invoke-virtual {v0, v1}, Lfwt;->a(Lhsn;)V

    .line 238
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->A:Landroid/text/Spannable;

    .line 269
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    invoke-static {p1}, Llir;->b(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->A:Landroid/text/Spannable;

    .line 272
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->x:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    :goto_0
    return-void

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->c()V

    .line 249
    iput-object p1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->x:Ljava/lang/String;

    .line 250
    if-nez p2, :cond_1

    const-string p2, ""

    :cond_1
    iput-object p2, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->y:Ljava/lang/String;

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    if-eqz v0, :cond_2

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 256
    :cond_2
    new-instance v0, Lfwt;

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->x:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->y:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->r:Lhsn;

    const/4 v6, 0x2

    move-object v1, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lfwt;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhsn;I)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->r:Lhsn;

    invoke-virtual {v0, v1}, Lfwt;->a(Lhsn;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->b()V

    goto :goto_0
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->invalidate()V

    .line 481
    return-void
.end method

.method public a(Lljv;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->s:Lljv;

    .line 231
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 464
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-virtual {v0}, Lfwt;->b()V

    .line 467
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-virtual {v0}, Lfwt;->c()V

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 474
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    .line 476
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 190
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 192
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 224
    :goto_0
    return v0

    .line 194
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 195
    invoke-interface {v0, v3, v4, v2}, Llip;->a(III)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 196
    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->q:Llip;

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->invalidate()V

    goto :goto_1

    :cond_1
    move v0, v1

    .line 200
    goto :goto_0

    .line 204
    :pswitch_2
    iput-object v5, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->q:Llip;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 206
    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    goto :goto_2

    .line 208
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->invalidate()V

    move v0, v2

    .line 209
    goto :goto_0

    .line 213
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->q:Llip;

    if-eqz v0, :cond_3

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->q:Llip;

    const/4 v2, 0x3

    invoke-interface {v0, v3, v4, v2}, Llip;->a(III)Z

    .line 215
    iput-object v5, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->q:Llip;

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->invalidate()V

    move v0, v1

    .line 217
    goto :goto_0

    :cond_3
    move v0, v2

    .line 219
    goto :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 458
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 459
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->b()V

    .line 460
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 452
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 453
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->c()V

    .line 454
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 178
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 180
    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->B:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-virtual {v0}, Lfwt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-virtual {v0}, Lfwt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-virtual {v1}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->b:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-virtual {v1}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-virtual {v0}, Lfwt;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    invoke-virtual {v0, p1}, Lfwt;->a(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->v:Lljg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->v:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->v:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->v:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->u:Lljg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->u:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->u:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->u:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    invoke-virtual {v0}, Llir;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    invoke-virtual {v1}, Llir;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    invoke-virtual {v2, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 184
    :cond_3
    return-void

    .line 182
    :cond_4
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 15

    .prologue
    .line 160
    invoke-super/range {p0 .. p2}, Landroid/view/View;->onMeasure(II)V

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getPaddingLeft()I

    move-result v0

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->i:I

    add-int v9, v0, v1

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getPaddingTop()I

    move-result v0

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->j:I

    add-int v10, v0, v1

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getMeasuredWidth()I

    move-result v11

    .line 166
    sub-int v0, v11, v9

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->h:I

    sub-int v12, v0, v1

    .line 168
    iput v10, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->B:I

    .line 170
    sget v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->l:I

    add-int/2addr v0, v9

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->k:I

    add-int/2addr v1, v10

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->t:Lfwt;

    sget v3, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->f:I

    add-int/2addr v3, v0

    sget v4, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->f:I

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Lfwt;->a(IIII)V

    :cond_0
    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->f:I

    sget v3, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->m:I

    add-int/2addr v2, v3

    add-int v13, v0, v2

    sget v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->n:I

    add-int v8, v1, v0

    sub-int v0, v12, v13

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v14

    const/16 v1, 0x10

    invoke-static {v14, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->y:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->y:Ljava/lang/String;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v1, v2, v0, v3}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    sub-int v0, v12, v13

    invoke-static {v2, v1}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;)I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    new-instance v0, Lljg;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->e:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->u:Lljg;

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->u:Lljg;

    invoke-virtual {v0, v13, v8}, Lljg;->a(II)V

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->u:Lljg;

    invoke-virtual {v0}, Lljg;->getHeight()I

    move-result v0

    add-int/2addr v0, v8

    move v8, v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->z:Ljava/lang/String;

    if-eqz v0, :cond_2

    sub-int v3, v12, v13

    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->z:Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v0, " "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Lljg;

    const/16 v2, 0xa

    invoke-static {v14, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->e:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->v:Lljg;

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->v:Lljg;

    invoke-virtual {v0, v13, v8}, Lljg;->a(II)V

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->v:Lljg;

    invoke-virtual {v0}, Lljg;->getHeight()I

    move-result v0

    add-int/2addr v8, v0

    :cond_2
    sget v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->f:I

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->k:I

    add-int/2addr v0, v1

    sub-int v1, v8, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v10

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->A:Landroid/text/Spannable;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 173
    :goto_0
    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->g:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v11, v0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->setMeasuredDimension(II)V

    .line 174
    return-void

    .line 171
    :cond_3
    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->o:I

    add-int v10, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v0, Llir;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->A:Landroid/text/Spannable;

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x19

    invoke-static {v2, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->e:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->s:Lljv;

    move v3, v12

    invoke-direct/range {v0 .. v8}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    invoke-virtual {v0, v9, v10}, Llir;->a(II)V

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->p:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->w:Llir;

    invoke-virtual {v0}, Llir;->d()I

    move-result v0

    goto :goto_0
.end method
