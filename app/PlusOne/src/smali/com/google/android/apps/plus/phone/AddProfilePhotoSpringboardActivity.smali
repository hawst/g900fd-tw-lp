.class public Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhmm;
.implements Lhmq;


# instance fields
.field private e:Lhee;

.field private f:Lcom/google/android/apps/plus/views/ClientOobActionBar;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lloa;-><init>()V

    .line 66
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->x:Llnh;

    .line 67
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->e:Lhee;

    .line 70
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 71
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 141
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 142
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "profile_photo_url"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 143
    :goto_0
    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->b(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 150
    :cond_0
    :goto_1
    return-void

    .line 142
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 147
    :cond_2
    new-instance v0, Ldqe;

    invoke-direct {v0, p0, p1, p2, p3}, Ldqe;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 76
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Lhej;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "is_google_plus"

    .line 78
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "profile_photo_url"

    .line 79
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 80
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->b(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;I)Z
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 86
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 87
    sget-object v3, Ldxd;->n:Lief;

    invoke-interface {v0, v3, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 113
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    const-string v0, "com.google.android.plus.NOTIFICATIONS"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 93
    const-string v0, "add_profile_photo_prompt_count"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 94
    const-string v0, "add_profile_photo_prompt_short_timestamp"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 98
    cmp-long v0, v6, v10

    if-nez v0, :cond_2

    move v0, v2

    .line 109
    :goto_1
    if-eqz v0, :cond_0

    .line 110
    add-int/lit8 v1, v4, 0x1

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "add_profile_photo_prompt_count"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "add_profile_photo_prompt_short_timestamp"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 100
    :cond_2
    sub-long v10, v8, v6

    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v5, Ldxd;->o:Lief;

    invoke-interface {v0, v5, p1}, Lieh;->c(Lief;I)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    cmp-long v0, v10, v12

    if-lez v0, :cond_3

    if-gt v4, v2, :cond_3

    move v0, v2

    .line 102
    goto :goto_1

    .line 103
    :cond_3
    sub-long v6, v8, v6

    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v5, Ldxd;->p:Lief;

    invoke-interface {v0, v5, p1}, Lieh;->c(Lief;I)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v0, v6, v10

    if-lez v0, :cond_4

    const/4 v0, 0x2

    if-gt v4, v0, :cond_4

    move v0, v2

    .line 106
    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 248
    sget-object v0, Lhmw;->L:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 241
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->x:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 244
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 257
    new-instance v0, Lhmk;

    sget-object v1, Lona;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 253
    return-void
.end method

.method public l()V
    .locals 0

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->finish()V

    .line 225
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 237
    :goto_0
    return-void

    .line 231
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->onBackPressed()V

    goto :goto_0

    .line 234
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->l()V

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x1020019
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v2, 0x7f0a09e2

    const/4 v5, 0x0

    .line 154
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 156
    if-nez p1, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "springboard_launcher"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->e:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v3

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lhmv;->K:Lhmv;

    move-object v1, v0

    :goto_0
    const-class v0, Lhms;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    invoke-direct {v4, p0, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v3, Lhmw;->w:Lhmw;

    invoke-virtual {v1, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 160
    :cond_0
    const v0, 0x7f04003b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->setContentView(I)V

    .line 162
    const v0, 0x7f10017e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "springboard_launcher"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_1

    move v1, v2

    :goto_1
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    return-void

    .line 157
    :pswitch_0
    sget-object v0, Lhmv;->J:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_1
    sget-object v0, Lhmv;->I:Lhmv;

    move-object v1, v0

    goto :goto_0

    .line 165
    :pswitch_2
    const v1, 0x7f0a09e1

    goto :goto_1

    :pswitch_3
    move v1, v2

    goto :goto_1

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 165
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 213
    invoke-super {p0, p1}, Lloa;->onPostCreate(Landroid/os/Bundle;)V

    .line 215
    const v0, 0x7f100181

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClientOobActionBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->f:Lcom/google/android/apps/plus/views/ClientOobActionBar;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->f:Lcom/google/android/apps/plus/views/ClientOobActionBar;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->f:Lcom/google/android/apps/plus/views/ClientOobActionBar;

    const v1, 0x7f0a09e0

    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->a(ILandroid/view/View$OnClickListener;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->f:Lcom/google/android/apps/plus/views/ClientOobActionBar;

    new-instance v1, Lhmk;

    sget-object v2, Lona;->b:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->a(Lhmk;)V

    .line 221
    :cond_0
    return-void
.end method
