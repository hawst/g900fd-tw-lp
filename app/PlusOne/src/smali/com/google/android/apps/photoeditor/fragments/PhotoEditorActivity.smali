.class public Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lcjk;
.implements Lcjt;
.implements Lcky;
.implements Ldxb;
.implements Lhmm;


# instance fields
.field private g:Lckw;

.field private h:Landroid/os/Handler;

.field private i:Lcmn;

.field private j:Ldxa;

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Llon;-><init>()V

    .line 84
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->e:Llnh;

    .line 85
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 86
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 720
    return-void
.end method

.method private a(IILandroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    .prologue
    .line 491
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 492
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 493
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 494
    invoke-virtual {v0, p2, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 496
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 497
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 498
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->b(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;ZZ)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 658
    .line 659
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v0

    const-string v1, "root"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcjm;

    .line 660
    if-nez v0, :cond_0

    .line 669
    :goto_0
    return-void

    .line 664
    :cond_0
    if-eqz p1, :cond_1

    .line 665
    invoke-virtual {v0, p2}, Lcjm;->a(Z)V

    goto :goto_0

    .line 667
    :cond_1
    invoke-virtual {v0}, Lcjm;->c()V

    goto :goto_0
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v0

    .line 336
    invoke-static {p1}, Lcht;->a(I)Lcje;

    move-result-object v1

    .line 337
    if-nez v1, :cond_0

    .line 349
    :goto_0
    return-void

    .line 341
    :cond_0
    invoke-virtual {v1, p0}, Lcje;->a(Lcjk;)V

    .line 343
    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    .line 345
    const v2, 0x7f1002a3

    const-string v3, "FilterFragment"

    invoke-virtual {v0, v2, v1, v3}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    .line 346
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lat;->a(Ljava/lang/String;)Lat;

    .line 347
    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Lat;->a(I)Lat;

    .line 348
    invoke-virtual {v0}, Lat;->b()I

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->m()V

    return-void
.end method

.method private b(Ldxa;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 544
    .line 546
    invoke-virtual {p1}, Ldxa;->b()[B

    move-result-object v0

    if-eqz v0, :cond_3

    .line 547
    invoke-virtual {p1}, Ldxa;->b()[B

    move-result-object v0

    invoke-static {v0}, Lhce;->a([B)Lnzi;

    move-result-object v3

    .line 549
    if-eqz v3, :cond_3

    .line 550
    invoke-virtual {p1}, Ldxa;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 552
    invoke-static {v3, v0}, Lhce;->a(Lnzi;Landroid/graphics/Bitmap;)Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    .line 554
    if-nez v0, :cond_0

    .line 556
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-direct {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;-><init>()V

    .line 559
    :cond_0
    const/4 v1, 0x0

    .line 560
    invoke-static {v3}, Lhbt;->a(Lnzi;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, v3, Lnzi;->b:Lpla;

    iget-object v4, v4, Lpla;->e:Lplb;

    if-eqz v4, :cond_2

    iget-object v4, v3, Lnzi;->b:Lpla;

    iget-object v4, v4, Lpla;->e:Lplb;

    iget-object v4, v4, Lplb;->a:Lphc;

    if-eqz v4, :cond_2

    .line 566
    iget-object v1, v3, Lnzi;->b:Lpla;

    iget-object v1, v1, Lpla;->e:Lplb;

    iget-object v1, v1, Lplb;->a:Lphc;

    iget v1, v1, Lphc;->a:I

    .line 567
    if-lez v1, :cond_1

    const/4 v4, 0x2

    if-le v1, v4, :cond_2

    .line 568
    :cond_1
    const/4 v1, 0x1

    .line 571
    :cond_2
    iget-object v4, v3, Lnzi;->b:Lpla;

    if-nez v4, :cond_4

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a([BI)V

    move-object v2, v0

    .line 576
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    .line 577
    invoke-virtual {p1}, Ldxa;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v3, Lckx;

    invoke-direct {v3, p0}, Lckx;-><init>(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;)V

    .line 576
    invoke-virtual {v0, v1, v2, v3}, Lckw;->a(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;Lchs;)V

    .line 578
    return-void

    .line 571
    :cond_4
    iget-object v2, v3, Lnzi;->b:Lpla;

    .line 572
    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v0

    .line 534
    const-string v1, "EditSessionFragment"

    .line 535
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lckw;

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lckw;->d(Z)V

    .line 540
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->finish()V

    .line 541
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 672
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->invalidateOptionsMenu()V

    .line 674
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v0

    .line 675
    const-string v1, "root"

    .line 676
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcjm;

    .line 677
    if-eqz v0, :cond_0

    .line 678
    invoke-virtual {v0}, Lcjm;->a()V

    .line 682
    :cond_0
    invoke-direct {p0, v2, v2}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->a(ZZ)V

    .line 683
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k:Z

    if-nez v0, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Lae;->e()I

    move-result v1

    if-gtz v1, :cond_0

    .line 301
    const/16 v1, 0x12

    if-ne p1, v1, :cond_2

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 303
    new-instance v1, Lckr;

    invoke-direct {v1, p0, v0}, Lckr;-><init>(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;Lae;)V

    .line 319
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 320
    const v2, 0x7f0a00b5

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 321
    const v2, 0x7f0a00b4

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 322
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 323
    const v2, 0x7f0a00b6

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 325
    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 327
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 330
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->b(I)V

    goto :goto_0
.end method

.method public a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 622
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->a(ZZ)V

    .line 624
    if-eqz p2, :cond_0

    .line 625
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->setResult(ILandroid/content/Intent;)V

    .line 630
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k()V

    .line 631
    return-void

    .line 627
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->setResult(I)V

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 91
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->e:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->e:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 95
    return-void
.end method

.method public a(Lcmn;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i:Lcmn;

    .line 255
    return-void
.end method

.method public a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v0

    .line 354
    if-nez v0, :cond_0

    .line 355
    const-string v0, "PhotoEditorActivity"

    const-string v1, "Cannot apply filter: edit session is not ready!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k:Z

    if-nez v0, :cond_2

    .line 386
    :goto_1
    return-void

    .line 356
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v1

    if-eq v1, v3, :cond_1

    if-nez p2, :cond_1

    .line 357
    const-string v0, "PhotoEditorActivity"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Failed to apply \"%s\" filter!"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 358
    invoke-virtual {p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v5

    invoke-static {v5}, Lhak;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 357
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 360
    :cond_1
    invoke-virtual {v0, p1, p2}, Lchp;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 369
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v1

    .line 371
    const-string v0, "root"

    .line 372
    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcjm;

    .line 374
    if-eqz v0, :cond_3

    .line 375
    invoke-virtual {v0}, Lcjm;->a()V

    .line 378
    :cond_3
    const-string v0, "FilterFragment"

    .line 379
    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcje;

    .line 380
    if-eqz v0, :cond_4

    .line 381
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcje;->a(Lcjk;)V

    .line 382
    invoke-virtual {v1}, Lae;->c()V

    .line 385
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->invalidateOptionsMenu()V

    goto :goto_1
.end method

.method public a(Ldxa;)V
    .locals 3

    .prologue
    .line 585
    if-nez p1, :cond_0

    .line 586
    new-instance v0, Lckv;

    invoke-direct {v0, p0}, Lckv;-><init>(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;)V

    .line 593
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 594
    const v2, 0x7f0a00b7

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 595
    const v2, 0x7f0a00b8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 596
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 597
    const v2, 0x7f0a00b2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 599
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 600
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 613
    :goto_0
    return-void

    .line 602
    :cond_0
    invoke-virtual {p1}, Ldxa;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 603
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x20

    if-ge v0, v1, :cond_2

    .line 604
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k()V

    .line 607
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->j:Ldxa;

    if-nez v0, :cond_3

    .line 608
    iput-object p1, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->j:Ldxa;

    goto :goto_0

    .line 610
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->b(Ldxa;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 245
    if-nez p1, :cond_0

    .line 250
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->h:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;I)V
    .locals 4

    .prologue
    .line 237
    if-nez p1, :cond_0

    .line 242
    :goto_0
    return-void

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->h:Landroid/os/Handler;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public a([B)Z
    .locals 3

    .prologue
    const/16 v2, 0x100

    .line 638
    invoke-static {p1}, Lhce;->a([B)Lnzi;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lhce;->a(Lnzi;Landroid/graphics/Bitmap;)Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->s:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 452
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 453
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k()V

    .line 462
    :goto_0
    return-void

    .line 456
    :cond_1
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->a(ZZ)V

    .line 458
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->c()V

    .line 460
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->j()Ldxa;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldxc;->a(Ldxa;)V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i:Lcmn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i:Lcmn;

    invoke-interface {v0, p1}, Lcmn;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 260
    :cond_0
    invoke-super {p0, p1}, Llon;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->l:Z

    return v0
.end method

.method public i()Lchp;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    invoke-virtual {v0}, Lckw;->a()Lchp;

    move-result-object v0

    return-object v0
.end method

.method public j()Ldxa;
    .locals 3

    .prologue
    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    .line 441
    new-instance v1, Lnzi;

    invoke-direct {v1}, Lnzi;-><init>()V

    .line 442
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v2

    invoke-virtual {v2}, Lchp;->g()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 444
    invoke-static {v0, v1}, Lhce;->a(Lcom/google/android/libraries/photoeditor/core/FilterChain;Lnzi;)Z

    .line 445
    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 447
    new-instance v1, Ldxa;

    invoke-direct {v1, v2, v0}, Ldxa;-><init>(Landroid/graphics/Bitmap;[B)V

    return-object v1
.end method

.method public onBackPressed()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    .line 390
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v1

    .line 391
    const-string v0, "FilterFragment"

    .line 392
    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcje;

    .line 394
    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {v0}, Lcje;->al()Z

    move-result v1

    if-nez v1, :cond_0

    .line 396
    iget-object v1, v0, Lcje;->at:Llnl;

    new-instance v2, Lhml;

    invoke-direct {v2}, Lhml;-><init>()V

    new-instance v3, Lhmk;

    sget-object v4, Lonc;->d:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    .line 397
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    .line 398
    iget-object v0, v0, Lcje;->at:Llnl;

    invoke-virtual {v2, v0}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v0

    .line 396
    invoke-static {v1, v5, v0}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 399
    invoke-super {p0}, Llon;->onBackPressed()V

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lae;->e()I

    move-result v0

    if-nez v0, :cond_2

    .line 406
    new-instance v0, Lcks;

    invoke-direct {v0, p0}, Lcks;-><init>(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;)V

    .line 417
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 418
    const v2, 0x7f0a00b0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 419
    const v2, 0x7f0a00b1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 420
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 421
    const v2, 0x7f0a00b2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 422
    const v2, 0x7f0a00b3

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 425
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 426
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 428
    :cond_2
    new-instance v0, Lhml;

    invoke-direct {v0}, Lhml;-><init>()V

    new-instance v1, Lhmk;

    sget-object v2, Lonc;->l:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    .line 429
    invoke-virtual {v0, v1}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v0

    .line 430
    invoke-virtual {v0, p0}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v0

    .line 428
    invoke-static {p0, v5, v0}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 432
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 104
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 106
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    sput-object v0, Lhan;->a:Lhao;

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 109
    const/16 v4, 0x9

    invoke-virtual {v0, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 111
    const v4, 0x7f040182

    invoke-virtual {p0, v4}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->setContentView(I)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    .line 114
    if-nez v4, :cond_0

    .line 115
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to get action bar reference"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    invoke-virtual {v4, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 118
    invoke-virtual {v4, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 119
    invoke-virtual {v4, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 120
    invoke-virtual {v4, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 122
    invoke-virtual {v0, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 124
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->h:Landroid/os/Handler;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v4

    .line 127
    const-string v0, "EditSessionFragment"

    .line 128
    invoke-virtual {v4, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lckw;

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    if-nez v0, :cond_1

    .line 130
    new-instance v0, Lckw;

    invoke-direct {v0}, Lckw;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    invoke-virtual {v0, v2}, Lckw;->d(Z)V

    .line 132
    invoke-virtual {v4}, Lae;->a()Lat;

    move-result-object v0

    .line 133
    iget-object v4, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    const-string v5, "EditSessionFragment"

    invoke-virtual {v0, v4, v5}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    .line 134
    invoke-virtual {v0}, Lat;->b()I

    .line 136
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    iget-object v4, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    invoke-static {}, Lhdv;->a()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const v6, 0x7fffffff

    invoke-static {v5, v0, v6}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_7

    :goto_1
    invoke-virtual {v4, v0, v3, v3}, Lckw;->a(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;Lchs;)V

    .line 141
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 142
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 143
    invoke-virtual {v0, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->g:Lckw;

    invoke-virtual {v0}, Lckw;->a()Lchp;

    move-result-object v0

    iget v4, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v4, v3}, Lchp;->a(II)V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v3

    const-string v0, "root"

    invoke-virtual {v3, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcjm;

    if-nez v0, :cond_2

    new-instance v0, Lcjm;

    invoke-direct {v0}, Lcjm;-><init>()V

    invoke-virtual {v3}, Lae;->a()Lat;

    move-result-object v3

    const v4, 0x7f1002a3

    const-string v5, "root"

    invoke-virtual {v3, v4, v0, v5}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    invoke-virtual {v3}, Lat;->b()I

    :cond_2
    invoke-virtual {v0, p0}, Lcjm;->a(Lcjt;)V

    .line 153
    if-nez p1, :cond_c

    .line 154
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Libq;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_3
    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->l:Z

    iget-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->l:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "edit_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v1, Lckt;

    invoke-direct {v1, p0}, Lckt;-><init>(Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;)V

    invoke-static {p0}, Libq;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f0a00b9

    :goto_3
    const v2, 0x7f0a00bd

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->a(IILandroid/content/DialogInterface$OnClickListener;)V

    .line 160
    :cond_4
    :goto_4
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->f()Z

    move-result v0

    if-nez v0, :cond_5

    .line 161
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, p1, p0, v1, p0}, Ldxc;->a(Landroid/os/Bundle;Landroid/app/Activity;Landroid/content/Intent;Ldxb;)V

    .line 163
    :cond_5
    return-void

    :cond_6
    move-object v0, v3

    .line 137
    goto/16 :goto_0

    :cond_7
    const/16 v0, 0x780

    const/16 v5, 0x438

    invoke-static {v0, v5}, Lhdv;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 154
    goto :goto_2

    :cond_9
    const v0, 0x7f0a00ba

    goto :goto_3

    :cond_a
    new-instance v1, Lcku;

    invoke-direct {v1}, Lcku;-><init>()V

    invoke-static {p0}, Libq;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    const v0, 0x7f0a00bb

    :goto_5
    const v2, 0x104000a

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->a(IILandroid/content/DialogInterface$OnClickListener;)V

    goto :goto_4

    :cond_b
    const v0, 0x7f0a00bc

    goto :goto_5

    .line 156
    :cond_c
    const-string v0, "editorAssetsAreAvailable"

    .line 157
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->l:Z

    goto :goto_4
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 229
    invoke-super {p0}, Llon;->onDestroy()V

    .line 231
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {v0}, Ldxc;->e()V

    .line 234
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x1

    .line 466
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1006f1

    if-ne v1, v2, :cond_1

    .line 467
    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lonc;->n:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 468
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    .line 469
    invoke-virtual {v1, p0}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 467
    invoke-static {p0, v4, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 471
    invoke-static {}, Ldxc;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 472
    sget-object v1, Ldxc;->i:Ldxc;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ldxc;->a(I)V

    .line 485
    :cond_0
    :goto_0
    return v0

    .line 475
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1006f2

    if-ne v1, v2, :cond_2

    .line 476
    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lonc;->q:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 477
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    .line 478
    invoke-virtual {v1, p0}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 476
    invoke-static {p0, v4, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 480
    invoke-static {}, Ldxc;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 481
    sget-object v1, Ldxc;->i:Ldxc;

    invoke-virtual {v1, v0}, Ldxc;->a(I)V

    goto :goto_0

    .line 485
    :cond_2
    invoke-super {p0, p1}, Llon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 208
    invoke-super {p0}, Llon;->onPause()V

    .line 210
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k:Z

    .line 212
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {v0}, Ldxc;->a()V

    .line 215
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0}, Llon;->onResume()V

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->k:Z

    .line 171
    sget-object v0, Lhan;->a:Lhao;

    invoke-interface {v0, p0}, Lhao;->setUpContext(Landroid/content/ContextWrapper;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->f()Lae;

    move-result-object v1

    .line 176
    const-string v0, "root"

    .line 177
    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcjm;

    .line 178
    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {v0, p0}, Lcjm;->a(Lcjt;)V

    .line 183
    :cond_0
    const-string v0, "FilterFragment"

    .line 184
    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcje;

    .line 185
    if-eqz v0, :cond_1

    .line 186
    invoke-virtual {v0, p0}, Lcje;->a(Lcjk;)V

    .line 189
    :cond_1
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 190
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {v0}, Ldxc;->d()V

    .line 193
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->i()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 195
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->m()V

    .line 204
    :goto_0
    return-void

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->j:Ldxa;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->j:Ldxa;

    invoke-virtual {v0}, Ldxa;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->j:Ldxa;

    invoke-direct {p0, v0}, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->b(Ldxa;)V

    goto :goto_0

    .line 201
    :cond_4
    new-instance v0, Ldxa;

    invoke-direct {v0}, Ldxa;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->j:Ldxa;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0, p1}, Llon;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 221
    const-string v0, "editorAssetsAreAvailable"

    iget-boolean v1, p0, Lcom/google/android/apps/photoeditor/fragments/PhotoEditorActivity;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 222
    invoke-static {}, Ldxc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    sget-object v0, Ldxc;->i:Ldxc;

    invoke-virtual {v0, p1}, Ldxc;->a(Landroid/os/Bundle;)V

    .line 225
    :cond_0
    return-void
.end method
