.class public Lcom/google/android/apps/plus/phone/InviteContactSpringboardActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lheg;


# instance fields
.field private final g:Livx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Llon;-><init>()V

    .line 23
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactSpringboardActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    .line 24
    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactSpringboardActivity;->g:Livx;

    .line 23
    return-void
.end method


# virtual methods
.method public a(ZIIII)V
    .locals 3

    .prologue
    .line 45
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 46
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactSpringboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 47
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/apps/plus/phone/InviteContactActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 48
    const/high16 v1, 0x2800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 50
    const-string v1, "account_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactSpringboardActivity;->g:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/InviteContactSpringboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactSpringboardActivity;->finish()V

    .line 54
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 28
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 30
    if-nez p1, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactSpringboardActivity;->g:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Liwl;

    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 33
    :cond_0
    return-void
.end method
