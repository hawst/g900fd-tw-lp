.class public Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;
.super Lu;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Llio;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lu;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Llio;"
    }
.end annotation


# instance fields
.field private N:Landroid/widget/ListView;

.field private O:Lest;

.field private P:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private R:Landroid/view/ContextThemeWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lu;-><init>()V

    .line 65
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;)I
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->b()I

    move-result v0

    return v0
.end method

.method private b()I
    .locals 3

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 223
    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->Q:Ljava/util/HashMap;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 201
    if-nez p1, :cond_0

    .line 219
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->O:Lest;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->O:Lest;

    invoke-virtual {v2}, Lest;->a()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 209
    iget-object v2, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->O:Lest;

    invoke-virtual {v2}, Lest;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    .line 216
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->N:Landroid/widget/ListView;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 217
    const v2, 0x1020004

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_3

    .line 218
    :goto_3
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v2, v0

    move v0, v1

    .line 212
    goto :goto_1

    :cond_2
    move v2, v3

    .line 216
    goto :goto_2

    :cond_3
    move v1, v3

    .line 217
    goto :goto_3

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->R:Landroid/view/ContextThemeWrapper;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 151
    const v1, 0x7f040069

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 153
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->N:Landroid/widget/ListView;

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->N:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->O:Lest;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->N:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 157
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->c(Landroid/view/View;)V

    .line 158
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    packed-switch p1, :pswitch_data_0

    .line 172
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 165
    :pswitch_0
    new-instance v0, Lhxg;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->n()Lz;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->b()I

    move-result v2

    const/16 v3, 0x11

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "circle_id"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "circle_name"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "contact_count"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "type"

    aput-object v6, v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 106
    invoke-super {p0, p1}, Lu;->a(Landroid/os/Bundle;)V

    .line 108
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->n()Lz;

    move-result-object v1

    const v2, 0x7f0901c9

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->R:Landroid/view/ContextThemeWrapper;

    .line 109
    new-instance v0, Lest;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->R:Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p0, v1}, Lest;-><init>(Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->O:Lest;

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz p1, :cond_3

    const-string v0, "selected_circles"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    :cond_0
    const-string v0, "sharing_user_count"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->Q:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->Q:Ljava/util/HashMap;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->Q:Ljava/util/HashMap;

    .line 113
    :cond_1
    const-string v0, "FriendLocationsFilter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->Q:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Dialog: circleToUserCountMap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 118
    return-void

    .line 111
    :cond_3
    const-string v0, "circle_ids"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 195
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178
    if-nez p2, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 190
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 186
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->O:Lest;

    invoke-virtual {v0, p2}, Lest;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->c(Landroid/view/View;)V

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Llin;Z)V
    .locals 2

    .prologue
    .line 233
    check-cast p1, Lhxf;

    invoke-virtual {p1}, Lhxf;->a()Ljava/lang/String;

    move-result-object v0

    .line 234
    if-eqz p2, :cond_0

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0, p1}, Lu;->e(Landroid/os/Bundle;)V

    .line 144
    const-string v0, "selected_circles"

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->P:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 145
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 243
    check-cast p2, Lhxf;

    invoke-virtual {p2}, Lhxf;->toggle()V

    .line 244
    return-void
.end method
