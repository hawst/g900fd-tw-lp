.class public Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Lhee;

.field private final f:Ldie;

.field private g:Lkuj;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 40
    invoke-direct {p0}, Lloa;-><init>()V

    .line 45
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 47
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->x:Llnh;

    .line 48
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 49
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 51
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 52
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 54
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->x:Llnh;

    .line 55
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 58
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->x:Llnh;

    .line 59
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->e:Lhee;

    .line 61
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lhmw;->I:Lhmw;

    return-object v0
.end method

.method public a()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lkte;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkte;

    iget-object v2, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->e:Lhee;

    .line 80
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x0

    .line 79
    invoke-interface {v0, v2, v1, v3}, Lkte;->a(ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 128
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 114
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 115
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 116
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 117
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 105
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 106
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 95
    if-nez p1, :cond_0

    .line 96
    new-instance v0, Lkuj;

    invoke-direct {v0}, Lkuj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->g:Lkuj;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->f:Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->g:Lkuj;

    invoke-virtual {v0, v1}, Ldie;->a(Lu;)V

    .line 99
    :cond_0
    const v0, 0x7f0400cb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->setContentView(I)V

    .line 100
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->g:Lkuj;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;->g:Lkuj;

    invoke-virtual {v0}, Lkuj;->e()V

    .line 70
    const/4 v0, 0x1

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
