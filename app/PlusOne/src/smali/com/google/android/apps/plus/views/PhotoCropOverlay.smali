.class public Lcom/google/android/apps/plus/views/PhotoCropOverlay;
.super Landroid/view/View;
.source "PG"


# static fields
.field private static a:Landroid/graphics/Paint;

.field private static b:Landroid/graphics/Paint;

.field private static c:Z


# instance fields
.field private d:F

.field private e:I

.field private f:Lnh;

.field private g:Lnh;

.field private h:Z

.field private i:Z

.field private final j:Landroid/graphics/Rect;

.field private final k:Landroid/graphics/Rect;

.field private l:Lfzo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 50
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->d:F

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->e:I

    .line 61
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->k:Landroid/graphics/Rect;

    .line 69
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a()V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->d:F

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->e:I

    .line 61
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->k:Landroid/graphics/Rect;

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a()V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->d:F

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->e:I

    .line 61
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->k:Landroid/graphics/Rect;

    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a()V

    .line 80
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 84
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->setWillNotDraw(Z)V

    .line 85
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->setFocusable(Z)V

    .line 86
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->setClickable(Z)V

    .line 88
    new-instance v1, Lnh;

    invoke-direct {v1, v0}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->f:Lnh;

    .line 89
    new-instance v1, Lnh;

    invoke-direct {v1, v0}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->g:Lnh;

    .line 91
    sget-boolean v1, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->c:Z

    if-nez v1, :cond_0

    .line 92
    sput-boolean v3, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->c:Z

    .line 94
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 96
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 97
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 98
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a:Landroid/graphics/Paint;

    const v2, 0x7f0b02e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 102
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->b:Landroid/graphics/Paint;

    const v2, 0x7f0b02e9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 105
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->b:Landroid/graphics/Paint;

    const v2, 0x7f0d02a6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 107
    :cond_0
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const v6, 0x3dcccccd    # 0.1f

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getWidth()I

    move-result v1

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->e:I

    if-nez v0, :cond_1

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getWidth()I

    move-result v0

    .line 163
    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getWidth()I

    move-result v1

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    .line 166
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->k:Landroid/graphics/Rect;

    const/4 v3, 0x0

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getHeight()I

    move-result v4

    invoke-virtual {v2, v1, v3, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->k:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->d:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    cmpl-float v3, v3, v1

    if-lez v3, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    mul-float/2addr v1, v5

    float-to-int v1, v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    sub-int/2addr v3, v1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    add-int/2addr v1, v5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 172
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    float-to-int v0, v0

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    float-to-int v1, v1

    .line 174
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 175
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    sub-int v1, v3, v1

    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 176
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v2, v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->f:Lnh;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lnh;->a(II)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->g:Lnh;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lnh;->a(II)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->l:Lfzo;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->l:Lfzo;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    invoke-direct {v1, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-interface {v0, v1}, Lfzo;->a(Landroid/graphics/Rect;)V

    .line 185
    :cond_0
    return-void

    .line 164
    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->e:I

    goto/16 :goto_0

    .line 169
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    div-float v1, v4, v1

    mul-float/2addr v1, v3

    mul-float/2addr v1, v5

    float-to-int v1, v1

    iget v3, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    sub-int/2addr v4, v1

    iget v5, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1
.end method


# virtual methods
.method public a(F)V
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->d:F

    .line 130
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->b()V

    .line 131
    return-void
.end method

.method public a(FF)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->g:Lnh;

    invoke-virtual {v0, p1}, Lnh;->a(F)Z

    move-result v0

    .line 140
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->f:Lnh;

    invoke-virtual {v3, p2}, Lnh;->a(F)Z

    move-result v3

    or-int/2addr v3, v0

    .line 142
    cmpl-float v0, p2, v4

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->h:Z

    .line 143
    cmpl-float v0, p1, v4

    if-lez v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->i:Z

    .line 145
    if-eqz v3, :cond_0

    .line 146
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 148
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 142
    goto :goto_0

    :cond_2
    move v1, v2

    .line 143
    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 118
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->e:I

    .line 119
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->b()V

    .line 120
    return-void
.end method

.method public a(Lfzo;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->l:Lfzo;

    .line 111
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 208
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getWidth()I

    move-result v10

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getHeight()I

    move-result v11

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getWidth()I

    move-result v0

    int-to-float v5, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v0

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->f:Lnh;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->f:Lnh;

    invoke-virtual {v0}, Lnh;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 225
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 227
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->h:Z

    if-eqz v2, :cond_2

    .line 228
    neg-int v2, v10

    int-to-float v2, v2

    int-to-float v3, v11

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 229
    const/high16 v2, 0x43340000    # 180.0f

    int-to-float v3, v10

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v1, v10, v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v11, v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 234
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->f:Lnh;

    invoke-virtual {v1, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 235
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    move v0, v8

    .line 239
    :goto_1
    if-eqz v0, :cond_0

    .line 240
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->g:Lnh;

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->g:Lnh;

    invoke-virtual {v0}, Lnh;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 248
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 250
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->i:Z

    if-eqz v1, :cond_3

    .line 251
    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 257
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->g:Lnh;

    invoke-virtual {v1, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 258
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    move v0, v8

    .line 262
    :goto_3
    if-eqz v0, :cond_1

    .line 263
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 266
    :cond_1
    return-void

    .line 232
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 254
    :cond_3
    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    .line 255
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    neg-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_2

    :cond_4
    move v0, v9

    goto :goto_3

    :cond_5
    move v0, v9

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 152
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 154
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->b()V

    .line 155
    return-void
.end method
