.class public Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final c:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Landroid/content/pm/PackageManager;

.field private final f:Ljfb;

.field private final g:Lanh;

.field private final h:Ljava/util/concurrent/Executor;

.field private final i:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    const-class v0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    aput-object v1, v0, v3

    const-class v1, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    aput-object v1, v0, v4

    const-class v1, Lcom/google/android/apps/moviemaker/service/PostCaptureAnalyzerService;

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-class v2, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->a:[Ljava/lang/Class;

    .line 44
    new-array v0, v5, [Ljava/lang/Class;

    const-class v1, Lcom/google/android/apps/moviemaker/receiver/NewVideoReceiver;

    aput-object v1, v0, v3

    const-class v1, Lcom/google/android/apps/moviemaker/receiver/PowerStatusReceiver;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->b:[Ljava/lang/Class;

    .line 50
    new-array v0, v4, [Ljava/lang/Class;

    const-class v1, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->c:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Lanh;Ljfb;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lale;

    invoke-direct {v0, p0}, Lale;-><init>(Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->i:Ljava/lang/Runnable;

    .line 100
    const-string v0, "context"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->d:Landroid/content/Context;

    .line 101
    const-string v0, "packageManager"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->e:Landroid/content/pm/PackageManager;

    .line 102
    const-string v0, "gservicesSettings"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->g:Lanh;

    .line 103
    const-string v0, "movieMakerProvider"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->f:Ljfb;

    .line 104
    const-string v0, "executor"

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->h:Ljava/util/concurrent/Executor;

    .line 105
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;)Ljfb;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->f:Ljfb;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 27
    sget-object v2, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->a:[Ljava/lang/Class;

    move v0, v1

    :goto_0
    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->d:Landroid/content/Context;

    invoke-direct {v4, v5, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v4, v1, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->b:[Ljava/lang/Class;

    move v0, v1

    :goto_1
    const/4 v3, 0x2

    if-ge v0, v3, :cond_1

    aget-object v3, v2, v0

    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->d:Landroid/content/Context;

    invoke-direct {v4, v5, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v4, p1, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0e0006

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v2, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->c:[Ljava/lang/Class;

    move v0, v1

    :goto_2
    if-gtz v0, :cond_2

    aget-object v3, v2, v1

    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->d:Landroid/content/Context;

    invoke-direct {v4, v5, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v4, v1, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;)Lanh;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->g:Lanh;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->h:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->i:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 109
    return-void
.end method
