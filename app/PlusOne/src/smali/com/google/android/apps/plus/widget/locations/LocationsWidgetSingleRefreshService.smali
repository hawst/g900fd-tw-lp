.class public Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;
.super Landroid/app/IntentService;
.source "PG"


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lnhm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    const-string v0, "LocationsWidgetSingleRefreshService"

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;-><init>(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a:Ljava/util/HashMap;

    .line 64
    return-void
.end method

.method private a(Lgcp;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 237
    :try_start_0
    iget-object v0, p1, Lgcp;->d:Lnij;

    iget-object v0, v0, Lnij;->h:Ljava/lang/String;

    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p0, v0, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 238
    const-class v0, Lizs;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    const/4 v2, 0x0

    const/16 v3, 0x122

    const/16 v4, 0xd2

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    .line 249
    :goto_0
    return-object v0

    .line 245
    :catch_0
    move-exception v0

    move-object v0, v6

    goto :goto_0

    .line 247
    :catch_1
    move-exception v0

    move-object v0, v6

    goto :goto_0
.end method

.method private a(Lgcr;Ljava/util/HashMap;)Lgcp;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgcr;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lnhm;",
            ">;)",
            "Lgcp;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 131
    .line 133
    iget-object v0, p1, Lgcr;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    iget-object v0, p1, Lgcr;->c:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v1, p1, Lgcr;->c:Ljava/lang/String;

    iget-object v0, p1, Lgcr;->c:Ljava/lang/String;

    .line 136
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a(Ljava/lang/String;Lnhm;)Lgcp;

    move-result-object v5

    .line 157
    :cond_0
    :goto_0
    return-object v5

    .line 138
    :cond_1
    iget-object v0, p1, Lgcr;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "gaia_id"

    aput-object v0, v3, v1

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lgcr;->a:I

    iget-object v2, p1, Lgcr;->b:Ljava/lang/String;

    .line 143
    iget-object v4, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "0"

    :goto_1
    move-object v6, v5

    .line 142
    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move-object v0, v5

    .line 146
    :cond_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 147
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a(Ljava/lang/String;Lnhm;)Lgcp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 149
    if-eqz v0, :cond_2

    .line 150
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v5, v0

    .line 155
    goto :goto_0

    .line 143
    :cond_4
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v4, "gaia_id IN ("

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v6, v4}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    const/16 v4, 0x2c

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v6, v4}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    const/16 v4, 0x29

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 154
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Ljava/lang/String;Lnhm;)Lgcp;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 166
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-object v0

    .line 169
    :cond_1
    new-instance v1, Lgcp;

    invoke-direct {v1}, Lgcp;-><init>()V

    .line 170
    iget-object v2, p2, Lnhm;->c:[Lnij;

    invoke-static {v2}, Liuo;->a([Lnij;)Lnij;

    move-result-object v2

    iput-object v2, v1, Lgcp;->d:Lnij;

    .line 171
    iget-object v2, v1, Lgcp;->d:Lnij;

    if-eqz v2, :cond_0

    .line 174
    iput-object p1, v1, Lgcp;->a:Ljava/lang/String;

    .line 175
    iget-object v0, p2, Lnhm;->d:Ljava/lang/String;

    iput-object v0, v1, Lgcp;->b:Ljava/lang/String;

    .line 176
    iget-object v0, p2, Lnhm;->e:Ljava/lang/String;

    invoke-static {v0}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgcp;->c:Ljava/lang/String;

    move-object v0, v1

    .line 177
    goto :goto_0
.end method

.method private b(Lgcp;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 255
    :try_start_0
    const-class v0, Lhso;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    iget-object v2, p1, Lgcp;->c:Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v4, 0x2

    invoke-interface {v0, v2, v3, v4}, Lhso;->a(Ljava/lang/String;II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    .line 263
    :goto_0
    return-object v0

    .line 259
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 261
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const v10, 0x7f100356

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 72
    const-string v0, "appWidgetId"

    .line 73
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lgcq;->a(Landroid/content/Context;I)Lgcr;

    move-result-object v4

    .line 78
    if-nez v4, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->stopSelf()V

    .line 81
    :cond_0
    iget v0, v4, Lgcr;->a:I

    const/4 v6, -0x1

    if-ne v0, v6, :cond_1

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    .line 83
    invoke-static/range {v0 .. v5}, Lgcq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 97
    :goto_0
    return-void

    .line 88
    :cond_1
    iget v0, v4, Lgcr;->a:I

    new-instance v2, Litx;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v2, v6, v0, v3, v3}, Litx;-><init>(Landroid/content/Context;IIZ)V

    invoke-virtual {v2}, Litx;->l()V

    invoke-virtual {v2}, Litx;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    :goto_1
    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a:Ljava/util/HashMap;

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a(Lgcr;Ljava/util/HashMap;)Lgcp;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_2

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 92
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f040100

    invoke-direct {v4, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v6, 0x7f10035d

    iget-object v7, v0, Lgcp;->b:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v6, 0x7f10035e

    iget-object v7, v0, Lgcp;->d:Lnij;

    iget-object v7, v7, Lnij;->g:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v6, 0x7f10035f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, v0, Lgcp;->d:Lnij;

    iget-object v8, v8, Lnij;->e:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lgcq;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a(Lgcp;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->b(Lgcp;)Landroid/graphics/Bitmap;

    move-result-object v7

    const v8, 0x7f10035c

    invoke-virtual {v4, v8, v7}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v4, v10, v6}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-class v8, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetProvider;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "com.google.android.apps.plus.widget.locations.LAUNCH"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "appWidgetId"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "gaia_id"

    iget-object v0, v0, Lgcp;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x8000000

    invoke-static {v6, v1, v0, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const v0, 0x7f100355

    invoke-virtual {v4, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f100358

    const/16 v3, 0x8

    invoke-virtual {v4, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {v2, v1, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 96
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 95
    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetProvider;->a(Landroid/content/Context;IZ)V

    goto/16 :goto_0

    .line 88
    :cond_3
    invoke-virtual {v2}, Litx;->i()[Lnhm;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    array-length v6, v2

    move v0, v3

    :goto_2
    if-ge v0, v6, :cond_4

    aget-object v7, v2, v0

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetSingleRefreshService;->a:Ljava/util/HashMap;

    iget-object v9, v7, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v8, v9, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v0, v5

    goto/16 :goto_1
.end method
