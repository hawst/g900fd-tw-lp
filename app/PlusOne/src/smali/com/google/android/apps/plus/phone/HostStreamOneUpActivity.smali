.class public Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:J

.field private final f:Ldie;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 43
    invoke-direct {p0}, Lloa;-><init>()V

    .line 49
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 51
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->x:Llnh;

    .line 52
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 54
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->x:Llnh;

    .line 55
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 58
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 59
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 61
    new-instance v0, Lfcs;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfcs;-><init>(Landroid/app/Activity;Llqr;)V

    const/4 v1, 0x2

    .line 62
    invoke-virtual {v0, v1}, Lfcs;->a(I)Lfcs;

    move-result-object v0

    const-string v1, "profile_picture_springboard"

    .line 63
    invoke-virtual {v0, v1}, Lfcs;->a(Ljava/lang/String;)Lfcs;

    .line 65
    new-instance v0, Llhe;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llhe;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->x:Llnh;

    .line 66
    invoke-virtual {v0, v1}, Llhe;->a(Llnh;)Llhe;

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->e:J

    .line 71
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->x:Llnh;

    .line 72
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 74
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->f:Ldie;

    return-void
.end method

.method public static b(I)Lu;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 131
    new-instance v1, Lehu;

    invoke-direct {v1}, Lehu;-><init>()V

    .line 132
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 133
    const-string v3, "host_mode"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 134
    const-string v3, "force_full_bleed"

    if-eqz p0, :cond_0

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 135
    invoke-virtual {v1, v2}, Lehu;->f(Landroid/os/Bundle;)V

    .line 136
    return-object v1

    .line 134
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lhmw;->m:Lhmw;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 118
    const v0, 0x7f10032c

    .line 119
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    .line 120
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a(I)V

    .line 121
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 164
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 166
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->m()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 169
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 152
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 153
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 154
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 155
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 142
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 143
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 144
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public l()I
    .locals 1

    .prologue
    .line 93
    const v0, 0x7f0400d0

    return v0
.end method

.method protected m()I
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1020002

    if-ne v0, v1, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->onBackPressed()V

    .line 128
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 83
    if-nez p1, :cond_0

    .line 84
    invoke-static {p0}, Llsc;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 85
    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->g:I

    .line 86
    iget v0, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->g:I

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->b(I)Lu;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->f:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->l()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->setContentView(I)V

    .line 90
    return-void

    .line 85
    :cond_1
    invoke-static {p0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 98
    invoke-super {p0, p1}, Lloa;->onPostCreate(Landroid/os/Bundle;)V

    .line 99
    iget v0, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->g:I

    if-eqz v0, :cond_0

    .line 100
    iget v0, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->a(I)V

    .line 105
    :goto_0
    new-instance v0, Lkoe;

    const/16 v1, 0x59

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->e:J

    invoke-direct {v0, v1, v2, v3}, Lkoe;-><init>(IJ)V

    .line 106
    invoke-virtual {v0, p0}, Lkoe;->a(Landroid/content/Context;)V

    .line 107
    new-instance v0, Lkoe;

    const/16 v1, 0x5a

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    invoke-virtual {v0, p0}, Lkoe;->a(Landroid/content/Context;)V

    .line 108
    return-void

    .line 102
    :cond_0
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const v0, 0x7f100303

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method
