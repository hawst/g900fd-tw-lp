.class public Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;
.super Llon;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Lheg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llon;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lheg;"
    }
.end annotation


# instance fields
.field private g:Z

.field private h:I

.field private i:Landroid/widget/ListView;

.field private j:Lgce;

.field private final k:Ljava/lang/Object;

.field private l:Livx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Llon;-><init>()V

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->k:Ljava/lang/Object;

    .line 66
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    .line 67
    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->l:Livx;

    .line 69
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 171
    packed-switch p1, :pswitch_data_0

    .line 181
    :cond_0
    :goto_0
    return-object v0

    .line 173
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->l:Livx;

    invoke-virtual {v1}, Livx;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    new-instance v0, Lhxg;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->l:Livx;

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    const/4 v2, 0x4

    sget-object v3, Lgcf;->a:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 197
    :goto_0
    return-void

    .line 189
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->j:Lgce;

    invoke-virtual {v0, p2}, Lgce;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->g:Z

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->h()V

    .line 193
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(ZIIII)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 139
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 140
    const v0, 0x7f040233

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->setContentView(I)V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->g:Z

    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->i:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->i:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040234

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f10062d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f10062e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a0826

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f10048e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v3, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    new-instance v0, Lgce;

    invoke-direct {v0, p0}, Lgce;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->j:Lgce;

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->i:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->j:Lgce;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->h()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->g()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    goto :goto_0
.end method

.method protected h()V
    .locals 7

    .prologue
    const v6, 0x7f100260

    const v5, 0x7f10025f

    const v4, 0x1020004

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 247
    iget-boolean v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->g:Z

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->i:Landroid/widget/ListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 249
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 250
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 251
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 258
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 254
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 255
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 256
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_1

    .line 113
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->h:I

    .line 119
    :goto_0
    iget v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->h:I

    if-nez v0, :cond_2

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    .line 127
    :cond_0
    :goto_1
    return-void

    .line 116
    :cond_1
    iput v2, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->h:I

    goto :goto_0

    .line 124
    :cond_2
    if-nez p1, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->l:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Liwl;

    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->i:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p3, v0

    .line 210
    if-gez v0, :cond_0

    .line 211
    const-string v1, "v.all.circles"

    .line 212
    const v0, 0x7f0a06fa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 230
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->l:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    .line 232
    iget v3, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->h:I

    invoke-static {p0, v3, v2, v1, v0}, Lgci;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)V

    .line 234
    iget v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->h:I

    invoke-static {p0, v2, v0}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->b(Landroid/content/Context;II)V

    .line 237
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 238
    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 239
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    .line 241
    :goto_1
    return-void

    .line 214
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->k:Ljava/lang/Object;

    monitor-enter v2

    .line 215
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->j:Lgce;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->j:Lgce;

    invoke-virtual {v1}, Lgce;->a()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 216
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->j:Lgce;

    invoke-virtual {v1}, Lgce;->a()Landroid/database/Cursor;

    move-result-object v3

    .line 217
    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gt v1, v0, :cond_2

    .line 218
    :cond_1
    monitor-exit v2

    goto :goto_1

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 221
    :cond_2
    :try_start_1
    invoke-interface {v3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 222
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 223
    const/4 v0, 0x2

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 227
    monitor-exit v2

    goto :goto_0

    .line 225
    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
