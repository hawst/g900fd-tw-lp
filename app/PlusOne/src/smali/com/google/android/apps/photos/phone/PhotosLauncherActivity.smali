.class public Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field private static final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    .line 118
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 119
    sget-object v1, Ldxd;->x:Lief;

    invoke-interface {v0, v1, p1}, Lieh;->a(Lief;I)Ljava/lang/String;

    move-result-object v1

    .line 120
    sget-object v2, Ldxd;->y:Lief;

    invoke-interface {v0, v2, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    .line 122
    invoke-static {p0, v1, v0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    .line 127
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 128
    const-string v1, "photos_app_promo_last_shown_ts"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 131
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 132
    invoke-static {p0}, Lfuy;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    if-nez p2, :cond_0

    .line 133
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    sget-wide v2, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 134
    invoke-static {p0}, Llsa;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v11, -0x1

    const/4 v3, 0x0

    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 51
    const-string v0, "photos_intro_shown"

    invoke-interface {v5, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    .line 54
    invoke-static {p0}, Ldhv;->b(Landroid/content/Context;)I

    move-result v4

    .line 53
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 55
    if-eq v7, v11, :cond_0

    move v4, v2

    .line 59
    :goto_0
    if-eq v7, v11, :cond_1

    .line 60
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 61
    sget-object v1, Ldxd;->x:Lief;

    invoke-interface {v0, v1, v7}, Lieh;->a(Lief;I)Ljava/lang/String;

    move-result-object v1

    .line 62
    sget-object v8, Ldxd;->y:Lief;

    invoke-interface {v0, v8, v7}, Lieh;->b(Lief;I)Z

    move-result v0

    .line 84
    :goto_1
    invoke-static {p0, v1, v0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "photos_app_promo_shown"

    invoke-virtual {v8, v9, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_2

    .line 87
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "photos_promo_direct_to_home"

    invoke-virtual {v8, v9, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 93
    const-string v8, "android.intent.action.MAIN"

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 95
    if-eqz v2, :cond_3

    .line 96
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 97
    const-string v4, "photos_app_promo_last_shown_ts"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-interface {v2, v4, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 98
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 99
    invoke-static {p0, v1, v7, v3, v0}, Lcom/google/android/apps/plus/phone/PhotosAppPromoActivity;->a(Landroid/content/Context;Ljava/lang/String;IZZ)Landroid/content/Intent;

    move-result-object v0

    .line 112
    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->finish()V

    .line 114
    return-void

    :cond_0
    move v4, v3

    .line 55
    goto :goto_0

    .line 65
    :cond_1
    const-string v0, "gs_photos_app_url"

    const-string v1, ""

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    const-string v0, "gs_disable_later_photos_app_promo"

    invoke-interface {v5, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 68
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lctd;

    invoke-direct {v9, p0, v5}, Lctd;-><init>(Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;Landroid/content/SharedPreferences;)V

    const-string v10, "fetch_gservices_value_and_put_in_shared_preferences"

    invoke-direct {v8, v9, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto :goto_1

    :cond_2
    move v2, v3

    .line 85
    goto :goto_2

    .line 101
    :cond_3
    if-eqz v3, :cond_4

    .line 102
    invoke-static {p0, v7}, Leyq;->j(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 103
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_3

    .line 104
    :cond_4
    if-nez v6, :cond_5

    if-nez v4, :cond_5

    if-nez v8, :cond_5

    .line 106
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/PhotosIntroActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_3

    .line 109
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 110
    invoke-static {p0, v11, v0}, Leyq;->a(Landroid/content/Context;ILandroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_3
.end method
