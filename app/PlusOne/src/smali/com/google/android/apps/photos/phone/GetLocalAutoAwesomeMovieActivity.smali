.class public Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final e:Lhee;

.field private final f:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 46
    invoke-direct {p0}, Lloa;-><init>()V

    .line 51
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 52
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 53
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->x:Llnh;

    .line 54
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 56
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->x:Llnh;

    .line 57
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 58
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 60
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 61
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 64
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->x:Llnh;

    .line 65
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x0

    .line 66
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->e:Lhee;

    .line 68
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lhmw;->h:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 73
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 75
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 76
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 77
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 79
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsm;

    new-instance v2, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    .line 80
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->x:Llnh;

    const-class v1, Lcnt;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    .line 84
    const-string v1, "LocalAutoAwesomeMovies"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcnt;->a(Ljava/lang/String;I)V

    .line 86
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 114
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 116
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 117
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 118
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->b(Z)V

    .line 106
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 90
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 92
    if-nez p1, :cond_0

    .line 93
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 94
    new-instance v1, Leea;

    invoke-direct {v1}, Leea;-><init>()V

    .line 95
    const-string v2, "hide_search_view"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 96
    const-string v2, "account_id"

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->e:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    invoke-virtual {v1, v0}, Lu;->f(Landroid/os/Bundle;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->f:Ldie;

    invoke-virtual {v0, v1}, Ldie;->a(Lu;)V

    .line 100
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;->setContentView(I)V

    .line 101
    return-void
.end method
