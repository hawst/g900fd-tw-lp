.class public Lcom/google/android/apps/plus/phone/TileCropActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 37
    invoke-direct {p0}, Lloa;-><init>()V

    .line 42
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 44
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->x:Llnh;

    .line 45
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 47
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->x:Llnh;

    .line 48
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 49
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 51
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 52
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 54
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->x:Llnh;

    .line 55
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 58
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lhmw;->m:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 97
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 98
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 100
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 83
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 84
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 85
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 86
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 75
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 65
    if-nez p1, :cond_0

    .line 66
    new-instance v0, Leiq;

    invoke-direct {v0}, Leiq;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/TileCropActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 69
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/TileCropActivity;->setContentView(I)V

    .line 70
    return-void
.end method
