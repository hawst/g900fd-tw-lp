.class public Lcom/google/android/apps/plus/phone/ProfileEditActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;


# instance fields
.field private e:Lhet;

.field private final f:Ldie;

.field private g:Lenl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Lloa;-><init>()V

    .line 35
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->x:Llnh;

    .line 36
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->e:Lhet;

    .line 39
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->y:Llqc;

    const v1, 0x1020002

    invoke-direct {v0, p0, v1}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lhmw;->g:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 114
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Lloa;->a(Lu;)V

    .line 47
    instance-of v0, p1, Lenl;

    if-eqz v0, :cond_0

    .line 48
    check-cast p1, Lenl;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    .line 50
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    invoke-virtual {v0}, Lenl;->af()V

    .line 108
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 56
    if-nez p1, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 58
    const-string v1, "profile_edit_view_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 59
    sparse-switch v0, :sswitch_data_0

    .line 97
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No View Type provided!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :sswitch_0
    new-instance v0, Lenx;

    invoke-direct {v0}, Lenx;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    .line 99
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->f:Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    invoke-virtual {v0, v1}, Ldie;->a(Lu;)V

    .line 101
    :cond_0
    return-void

    .line 67
    :sswitch_1
    new-instance v0, Leow;

    invoke-direct {v0}, Leow;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    goto :goto_0

    .line 70
    :sswitch_2
    new-instance v0, Leog;

    invoke-direct {v0}, Leog;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    goto :goto_0

    .line 73
    :sswitch_3
    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->x:Llnh;

    const-class v2, Ljnv;

    invoke-virtual {v0, v2}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljnv;

    .line 75
    if-eqz v0, :cond_2

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->e:Lhet;

    invoke-virtual {v2}, Lhet;->d()I

    move-result v2

    .line 76
    invoke-interface {v0, v1, v2}, Ljnv;->a(Landroid/content/Context;I)Z

    move-result v0

    .line 79
    :goto_1
    if-eqz v0, :cond_1

    .line 80
    new-instance v0, Leng;

    invoke-direct {v0}, Leng;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    goto :goto_0

    .line 82
    :cond_1
    new-instance v0, Leof;

    invoke-direct {v0}, Leof;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    goto :goto_0

    .line 88
    :sswitch_4
    new-instance v0, Leof;

    invoke-direct {v0}, Leof;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    goto :goto_0

    .line 91
    :sswitch_5
    new-instance v0, Leon;

    invoke-direct {v0}, Leon;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    goto :goto_0

    .line 94
    :sswitch_6
    new-instance v0, Leos;

    invoke-direct {v0}, Leos;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileEditActivity;->g:Lenl;

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 59
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_2
        0x8 -> :sswitch_4
        0xa -> :sswitch_3
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0xf -> :sswitch_1
        0x13 -> :sswitch_4
        0x1a -> :sswitch_5
        0x24 -> :sswitch_6
        0x26 -> :sswitch_1
    .end sparse-switch
.end method
