.class public Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Llgs;


# instance fields
.field public N:Ljava/lang/Boolean;

.field private final O:Lfhh;

.field private final P:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ldsx;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Lhee;

.field private R:Landroid/content/Context;

.field private S:Landroid/widget/TextView;

.field private T:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private U:Landroid/widget/Button;

.field private V:Landroid/widget/Button;

.field private W:Ljava/lang/Integer;

.field private X:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0}, Llol;-><init>()V

    .line 63
    new-instance v0, Ldxe;

    invoke-direct {v0, p0}, Ldxe;-><init>(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->O:Lfhh;

    .line 107
    new-instance v0, Ldxf;

    invoke-direct {v0, p0}, Ldxf;-><init>(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->P:Lbc;

    .line 157
    new-instance v0, Lhmf;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 158
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Llnh;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->au:Llnh;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;[B)V
    .locals 3

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->Q:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;I[B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    const/4 v0, 0x0

    const v1, 0x7f0a08ee

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->p()Lae;

    move-result-object v1

    const-string v2, "pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->p()Lae;

    move-result-object v0

    const-string v1, "pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lt;->a()V

    :cond_0
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->d()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Lhee;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->Q:Lhee;

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->N:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->S:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->a()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 222
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->U:Landroid/widget/Button;

    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->V:Landroid/widget/Button;

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 224
    return-void

    :cond_0
    move v0, v2

    .line 222
    goto :goto_0

    :cond_1
    move v1, v2

    .line 223
    goto :goto_1
.end method

.method public static synthetic e(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->T:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Llnl;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->at:Llnl;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Llnh;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->au:Llnh;

    return-object v0
.end method


# virtual methods
.method protected a()I
    .locals 3

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "springboard_launcher"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 163
    packed-switch v0, :pswitch_data_0

    .line 172
    const v0, 0x7f0a09e3

    :goto_0
    return v0

    .line 165
    :pswitch_0
    const v0, 0x7f0a09e4

    goto :goto_0

    .line 168
    :pswitch_1
    const v0, 0x7f0a09e5

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->at:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 277
    const v1, 0x7f040158

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 279
    const v0, 0x7f100474

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->T:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->T:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->T:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    new-instance v2, Lhmk;

    sget-object v3, Long;->i:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 283
    const v0, 0x7f100475

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->U:Landroid/widget/Button;

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->U:Landroid/widget/Button;

    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->U:Landroid/widget/Button;

    new-instance v2, Lhmk;

    sget-object v3, Lona;->f:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 288
    const v0, 0x7f100476

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->V:Landroid/widget/Button;

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->V:Landroid/widget/Button;

    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->V:Landroid/widget/Button;

    new-instance v2, Lhmk;

    sget-object v3, Lona;->a:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 293
    const v0, 0x7f10017e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->S:Landroid/widget/TextView;

    .line 294
    return-object v1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 362
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 363
    packed-switch p1, :pswitch_data_0

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 365
    :pswitch_0
    if-eqz p3, :cond_0

    .line 367
    const-string v0, "data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 368
    if-eqz v1, :cond_0

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->N:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lhmv;->ax:Lhmv;

    .line 372
    :goto_1
    new-instance v2, Ldxg;

    invoke-direct {v2, p0, v1, v0}, Ldxg;-><init>(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;[BLhmv;)V

    invoke-static {v2}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 369
    :cond_1
    sget-object v0, Lhmv;->aw:Lhmv;

    goto :goto_1

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 339
    return-void
.end method

.method public a(ILfib;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->R:Landroid/content/Context;

    const v1, 0x7f0a0592

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 185
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    .line 187
    sget-object v0, Lhmv;->R:Lhmv;

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->N:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "springboard_launcher"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 192
    packed-switch v1, :pswitch_data_0

    move-object v1, v0

    .line 199
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 208
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->w:Lhmw;

    .line 209
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 206
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0

    .line 194
    :pswitch_0
    sget-object v0, Lhmv;->Q:Lhmv;

    move-object v1, v0

    .line 195
    goto :goto_1

    .line 198
    :pswitch_1
    sget-object v0, Lhmv;->P:Lhmv;

    move-object v1, v0

    goto :goto_1

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 321
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 322
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->R:Landroid/content/Context;

    .line 323
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 234
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 236
    if-eqz p1, :cond_2

    .line 237
    const-string v0, "profile_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const-string v0, "profile_request_id"

    .line 239
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    .line 241
    :cond_0
    const-string v0, "photo_changed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    const-string v0, "photo_changed"

    .line 243
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->N:Ljava/lang/Boolean;

    .line 245
    :cond_1
    const-string v0, "camera_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    const-string v0, "camera_request_id"

    .line 247
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->X:Ljava/lang/Integer;

    .line 251
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->w()Lbb;

    move-result-object v0

    const/16 v1, 0x64

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->P:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 253
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 327
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 299
    invoke-super {p0}, Llol;->aO_()V

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->O:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->d()V

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->a(ILfib;)V

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    .line 311
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 331
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 228
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->Q:Lhee;

    .line 230
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 335
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 257
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 259
    const-string v0, "profile_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->W:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->N:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 263
    const-string v0, "photo_changed"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->N:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->X:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 267
    const-string v0, "camera_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->X:Ljava/lang/Integer;

    .line 268
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 267
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 270
    :cond_2
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->Q:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->T:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->U:Landroid/widget/Button;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->V:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 350
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->n()Lz;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/String;

    .line 351
    invoke-static {v3, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    const/16 v8, 0x1e0

    const/16 v9, 0x10e

    move v5, v3

    move v7, v3

    .line 349
    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IZILjava/lang/Integer;ZII)Landroid/content/Intent;

    move-result-object v0

    .line 356
    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->a(Landroid/content/Intent;I)V

    .line 358
    :cond_1
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 315
    invoke-super {p0}, Llol;->z()V

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->O:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 317
    return-void
.end method
