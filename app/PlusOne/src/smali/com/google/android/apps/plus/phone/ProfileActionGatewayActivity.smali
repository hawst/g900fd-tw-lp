.class public Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lbc;
.implements Ldid;
.implements Lhmq;
.implements Lhob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llon;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ldid;",
        "Lhmq;",
        "Lhob;"
    }
.end annotation


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field private final h:Lhoc;

.field private final i:Lhov;

.field private j:Lhet;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Ldib;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "sourceid"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "display_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Llon;-><init>()V

    .line 67
    new-instance v0, Lhoc;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Landroid/app/Activity;Llqr;)V

    .line 68
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->h:Lhoc;

    .line 70
    new-instance v0, Lhov;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->f:Llqc;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->i:Lhov;

    .line 73
    new-instance v0, Lirl;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lirl;-><init>(Landroid/app/Activity;Llqr;)V

    .line 76
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->e:Llnh;

    .line 77
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->j:Lhet;

    .line 76
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 290
    sget-object v0, Lhmw;->P:Lhmw;

    return-object v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 296
    const/16 v0, 0x42

    return v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 106
    const-string v0, "data_uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 107
    new-instance v0, Lhye;

    sget-object v3, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->g:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const v6, 0x7f0a0884

    const/high16 v5, 0x2000000

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 112
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->m:Z

    if-eqz v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 116
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->m:Z

    .line 118
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 119
    :cond_1
    invoke-static {p0, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    goto :goto_0

    .line 124
    :cond_2
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    invoke-static {p0, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    goto :goto_0

    .line 131
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    const-string v1, "g:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    const-string v1, "e:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    const-string v1, "p:"

    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 133
    const-string v1, "ProfileActionGateway"

    const-string v2, "Unrecognized aggregate ID format: "

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-static {p0, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    goto :goto_0

    .line 133
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 140
    :cond_5
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 141
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->l:Ljava/lang/String;

    .line 143
    const-string v1, "conversation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->j:Lhet;

    invoke-virtual {v0}, Lhet;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Leyq;->h(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_6
    invoke-static {p0, v0}, Leyq;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    goto/16 :goto_0

    .line 145
    :cond_7
    const-string v1, "hangout"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->j:Lhet;

    invoke-virtual {v0}, Lhet;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    const-string v2, "vnd.google.android.hangouts/vnd.google.android.hangout_privileged"

    invoke-static {p0, v2, v0, v3}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v2, "participant_gaia"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_8
    if-eqz v0, :cond_9

    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_9
    invoke-static {p0, v0}, Leyq;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    goto/16 :goto_0

    .line 147
    :cond_a
    const-string v1, "addtocircle"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->h()V

    goto/16 :goto_0

    .line 150
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->j:Lhet;

    invoke-virtual {v0}, Lhet;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    goto/16 :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 199
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->e:Llnh;

    const-class v1, Lhmq;

    .line 201
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhoc;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->h:Lhoc;

    .line 202
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 203
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 275
    const-string v0, "ModifyCircleMembershipsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->n:Ldib;

    if-eqz v0, :cond_0

    .line 277
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->n:Ldib;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->j:Lhet;

    invoke-virtual {v1}, Lhet;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ldib;->a(I)V

    .line 282
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->n:Ldib;

    .line 284
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    .line 286
    :cond_1
    return-void

    .line 280
    :cond_2
    invoke-virtual {p2}, Lhoz;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 248
    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a08a3

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->l:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 254
    new-instance v1, Ldpj;

    invoke-direct {v1, p0}, Ldpj;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->j:Lhet;

    .line 255
    invoke-virtual {v2}, Lhet;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ldpj;->a(I)Ldpj;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    .line 256
    invoke-virtual {v1, v2}, Ldpj;->a(Ljava/lang/String;)Ldpj;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->l:Ljava/lang/String;

    .line 257
    invoke-virtual {v1, v2}, Ldpj;->b(Ljava/lang/String;)Ldpj;

    move-result-object v1

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ldpj;->b(I)Ldpj;

    move-result-object v1

    .line 259
    invoke-virtual {v1, p1}, Ldpj;->a(Ljava/util/ArrayList;)Ldpj;

    move-result-object v1

    .line 260
    invoke-virtual {v1, v5}, Ldpj;->b(Ljava/util/ArrayList;)Ldpj;

    move-result-object v1

    .line 261
    invoke-virtual {v1, v6}, Ldpj;->a(Z)Ldpj;

    move-result-object v1

    .line 262
    invoke-virtual {v1, v4}, Ldpj;->b(Z)Ldpj;

    move-result-object v1

    .line 263
    invoke-virtual {v1, v4}, Ldpj;->c(Z)Ldpj;

    move-result-object v1

    .line 264
    invoke-virtual {v1, v0}, Ldpj;->c(Ljava/lang/String;)Ldpj;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Ldpj;->a()Ldpi;

    move-result-object v0

    .line 266
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->h:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 268
    new-instance v0, Ldib;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->e:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhms;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->n:Ldib;

    .line 270
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method protected h()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->j:Lhet;

    .line 210
    invoke-virtual {v0}, Lhet;->d()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->l:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v0, p0

    .line 209
    invoke-static/range {v0 .. v5}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 214
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 219
    packed-switch p1, :pswitch_data_0

    .line 239
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    .line 242
    :cond_1
    return-void

    .line 221
    :pswitch_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 222
    const-string v0, "selected_circle_ids"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 225
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->i:Lhov;

    new-instance v2, Lfcm;

    invoke-direct {v2, p0, v0}, Lfcm;-><init>(Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    .line 233
    const/4 v0, 0x1

    goto :goto_0

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 86
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 94
    if-nez v0, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->finish()V

    goto :goto_0

    .line 99
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 100
    const-string v2, "data_uri"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->g()Lbb;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 188
    invoke-super {p0, p1}, Llon;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 189
    const-string v0, "person_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v0, "person_name"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v0, "redirected"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->n:Ldib;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;->n:Ldib;

    invoke-virtual {v0, p1}, Ldib;->a(Landroid/os/Bundle;)V

    .line 195
    :cond_0
    return-void
.end method
