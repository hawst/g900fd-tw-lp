.class public Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lfwv;
.implements Lljh;


# static fields
.field private static a:Landroid/graphics/Bitmap;

.field private static b:Landroid/graphics/Bitmap;

.field private static c:Landroid/graphics/Bitmap;

.field private static d:Landroid/graphics/drawable/NinePatchDrawable;

.field private static e:Landroid/graphics/drawable/NinePatchDrawable;

.field private static f:Landroid/graphics/Paint;

.field private static g:Landroid/graphics/Paint;

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:I

.field private static n:Llct;


# instance fields
.field private A:Z

.field private B:I

.field private C:Llah;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Z

.field private o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private p:Llip;

.field private q:Lgba;

.field private r:Lfwu;

.field private s:Lfwu;

.field private t:Lfwu;

.field private u:Lfwt;

.field private v:Landroid/text/StaticLayout;

.field private w:Liwk;

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const v2, 0x7f0d019b

    .line 110
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    .line 118
    sget-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->n:Llct;

    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 122
    invoke-static {v0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->n:Llct;

    .line 124
    const v0, 0x7f0d02a1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->j:I

    .line 125
    const v0, 0x7f0d029f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->h:I

    .line 126
    const v0, 0x7f0d02a0

    .line 127
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->i:I

    .line 129
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->k:I

    .line 130
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->l:I

    .line 131
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->m:I

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->a:Landroid/graphics/Bitmap;

    .line 135
    const v0, 0x7f020085

    invoke-static {v1, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->b:Landroid/graphics/Bitmap;

    .line 136
    const v0, 0x7f02031e

    invoke-static {v1, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->c:Landroid/graphics/Bitmap;

    .line 137
    const v0, 0x7f0200b0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->d:Landroid/graphics/drawable/NinePatchDrawable;

    .line 138
    const v0, 0x7f0200b1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->e:Landroid/graphics/drawable/NinePatchDrawable;

    .line 140
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 141
    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->g:Landroid/graphics/Paint;

    const v2, 0x7f0b0315

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 142
    sget-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 144
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->f:Landroid/graphics/Paint;

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->n:Llct;

    .line 111
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->setWillNotDraw(Z)V

    .line 112
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 113
    new-instance v1, Liwk;

    invoke-direct {v1, p1, v0}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v0, Lixj;

    .line 114
    invoke-virtual {v1, v0}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->w:Liwk;

    .line 115
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 244
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0}, Lfwt;->b()V

    .line 249
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0}, Lfwt;->c()V

    .line 261
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    .line 262
    iput-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    .line 263
    iput-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->v:Landroid/text/StaticLayout;

    .line 264
    iput-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    .line 265
    iput-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 268
    iput-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->p:Llip;

    .line 269
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 488
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->c()V

    .line 489
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->q:Lgba;

    .line 490
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->x:Ljava/lang/String;

    .line 491
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->y:Z

    .line 492
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->z:Z

    .line 493
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->A:Z

    .line 494
    iput v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->B:I

    .line 495
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    .line 496
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->D:Ljava/lang/String;

    .line 497
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->E:Ljava/lang/String;

    .line 498
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->F:Z

    .line 152
    const/16 v0, 0xe

    .line 153
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 152
    invoke-static {v0}, Ljux;->a([B)Ljux;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Ljux;->c()Lnzx;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 157
    invoke-virtual {v0}, Ljux;->c()Lnzx;

    move-result-object v0

    sget-object v1, Lnzr;->a:Loxr;

    invoke-virtual {v0, v1}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 158
    if-eqz v0, :cond_0

    iget-object v1, v0, Lnzr;->c:[Lnyz;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lnzr;->c:[Lnyz;

    array-length v1, v1

    if-eqz v1, :cond_0

    .line 161
    iget-object v0, v0, Lnzr;->c:[Lnyz;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 163
    if-eqz v0, :cond_0

    .line 164
    iget-object v1, v0, Lnyz;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->D:Ljava/lang/String;

    .line 165
    iget-object v1, v0, Lnyz;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->E:Ljava/lang/String;

    .line 167
    iget-object v0, v0, Lnyz;->e:Ljava/lang/String;

    .line 168
    invoke-static {v0}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 167
    invoke-static {v0}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 171
    new-instance v0, Lfwt;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->D:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->q:Lgba;

    const/4 v6, 0x2

    move-object v1, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lfwt;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhsn;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 178
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->b()V

    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->requestLayout()V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->invalidate()V

    .line 182
    return-void
.end method

.method public a(Lfwu;)V
    .locals 6

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->q:Lgba;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->w:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->w:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    if-ne p1, v0, :cond_4

    .line 461
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->q:Lgba;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->D:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->E:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->y:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    invoke-interface/range {v0 .. v5}, Lgba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLlah;)V

    .line 463
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 464
    invoke-static {v0}, Llhn;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    .line 466
    invoke-virtual {v0}, Llah;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 467
    :goto_1
    if-eqz v0, :cond_3

    const v0, 0x7f0a058c

    .line 469
    :goto_2
    const/16 v1, 0x4000

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 471
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    .line 472
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 471
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 473
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 475
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p0, v1}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    goto :goto_0

    .line 466
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 467
    :cond_3
    const v0, 0x7f0a058b

    goto :goto_2

    .line 478
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    if-ne p1, v0, :cond_5

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->q:Lgba;

    invoke-interface {v0}, Lgba;->ac()V

    goto :goto_0

    .line 480
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    if-ne p1, v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->q:Lgba;

    invoke-interface {v0}, Lgba;->ad()V

    goto :goto_0
.end method

.method public a(Lgba;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->q:Lgba;

    .line 223
    return-void
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 185
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->F:Z

    .line 187
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->c()V

    .line 189
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 191
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->x:Ljava/lang/String;

    .line 192
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->B:I

    .line 193
    const-wide/16 v8, 0x1

    and-long/2addr v8, v4

    cmp-long v0, v8, v10

    if-nez v0, :cond_0

    const-wide/16 v8, 0x400

    and-long/2addr v8, v4

    cmp-long v0, v8, v10

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->y:Z

    .line 195
    const-wide/16 v8, 0x10

    and-long/2addr v8, v4

    cmp-long v0, v8, v10

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->z:Z

    .line 196
    const-wide/16 v8, 0x4

    and-long/2addr v4, v8

    cmp-long v0, v4, v10

    if-eqz v0, :cond_4

    :goto_2
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->A:Z

    .line 197
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->D:Ljava/lang/String;

    .line 198
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->E:Ljava/lang/String;

    .line 199
    const/4 v0, 0x5

    .line 200
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-static {v0}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 203
    new-instance v0, Lfwt;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->D:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->q:Lgba;

    move-object v1, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lfwt;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhsn;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 207
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 208
    if-eqz v0, :cond_1

    .line 209
    invoke-static {v0}, Llah;->a([B)Llah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    .line 212
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->b()V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->requestLayout()V

    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->invalidate()V

    .line 216
    return-void

    :cond_2
    move v0, v1

    .line 193
    goto :goto_0

    :cond_3
    move v0, v1

    .line 195
    goto :goto_1

    :cond_4
    move v2, v1

    .line 196
    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 411
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 412
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 414
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 450
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 416
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 417
    const/4 v5, 0x0

    invoke-interface {v0, v2, v3, v5}, Llip;->a(III)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 418
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->p:Llip;

    .line 419
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->invalidate()V

    move v0, v1

    .line 420
    goto :goto_1

    .line 427
    :pswitch_2
    iput-object v5, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->p:Llip;

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 429
    invoke-interface {v0, v2, v3, v1}, Llip;->a(III)Z

    goto :goto_2

    .line 431
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->invalidate()V

    goto :goto_0

    .line 436
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->p:Llip;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->p:Llip;

    const/4 v4, 0x3

    invoke-interface {v0, v2, v3, v4}, Llip;->a(III)Z

    .line 438
    iput-object v5, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->p:Llip;

    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->invalidate()V

    move v0, v1

    .line 440
    goto :goto_1

    .line 414
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 227
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0}, Lfwt;->b()V

    .line 232
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0}, Lfwt;->c()V

    .line 241
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 369
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 370
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->g:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0}, Lfwt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0}, Lfwt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 381
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v1}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 382
    sget-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->b:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v1}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->v:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sget v1, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->m:I

    add-int/2addr v0, v1

    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->v:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 387
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 388
    iget-object v2, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->v:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 389
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0}, Lfwt;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    invoke-virtual {v0, p1}, Lfwt;->a(Landroid/graphics/Canvas;)V

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    if-eqz v0, :cond_2

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 400
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    if-eqz v0, :cond_3

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 403
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    if-eqz v0, :cond_4

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 406
    :cond_4
    return-void

    .line 377
    :cond_5
    sget-object v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->a:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 18

    .prologue
    .line 273
    invoke-super/range {p0 .. p2}, Landroid/view/View;->onMeasure(II)V

    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 276
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getMeasuredWidth()I

    move-result v1

    .line 277
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getMeasuredHeight()I

    move-result v16

    .line 279
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getPaddingLeft()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->k:I

    add-int/2addr v3, v4

    .line 281
    sub-int/2addr v1, v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v1, v4

    sget v4, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->l:I

    sub-int/2addr v1, v4

    .line 282
    add-int v9, v3, v1

    .line 284
    sget v1, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->j:I

    sub-int v1, v16, v1

    div-int/lit8 v10, v1, 0x2

    .line 285
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    if-eqz v1, :cond_0

    .line 286
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->u:Lfwt;

    sget v4, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->j:I

    add-int/2addr v4, v3

    sget v5, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->j:I

    add-int/2addr v5, v10

    invoke-virtual {v1, v3, v10, v4, v5}, Lfwt;->a(IIII)V

    .line 289
    :cond_0
    sget v1, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->j:I

    sget v4, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->m:I

    add-int/2addr v1, v4

    add-int v17, v3, v1

    .line 293
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->x:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 295
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->B:I

    if-nez v1, :cond_6

    const/4 v4, 0x0

    .line 297
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 298
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->B:I

    if-gtz v1, :cond_1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->A:Z

    if-eqz v1, :cond_12

    .line 299
    :cond_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->B:I

    if-lez v1, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f110037

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->B:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->B:I

    .line 301
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 299
    invoke-virtual {v1, v3, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 303
    :goto_1
    new-instance v1, Lfwu;

    sget-object v3, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->n:Llct;

    iget-object v3, v3, Llct;->e:Landroid/graphics/Bitmap;

    sget-object v5, Lfuu;->f:Landroid/text/TextPaint;

    sget-object v6, Lfuu;->b:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v7, Lfuu;->c:Landroid/graphics/drawable/NinePatchDrawable;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->A:Z

    if-eqz v8, :cond_8

    move-object/from16 v8, p0

    :goto_2
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v1 .. v13}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;ZI)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    .line 309
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, v9, v1

    .line 310
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v16, v3

    div-int/lit8 v9, v3, 0x2

    .line 311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3, v1, v9}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 312
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->A:Z

    if-eqz v3, :cond_2

    .line 313
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->s:Lfwu;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 315
    :cond_2
    sget-object v3, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->n:Llct;

    iget v3, v3, Llct;->k:I

    sub-int v8, v1, v3

    .line 318
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    invoke-virtual {v1}, Llah;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    move v6, v1

    .line 319
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    if-nez v1, :cond_a

    const/4 v1, 0x1

    .line 320
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a059c

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v10, 0x1

    .line 321
    invoke-static {v1, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v7

    .line 320
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 322
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 323
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->z:Z

    if-nez v4, :cond_3

    if-lez v1, :cond_f

    .line 324
    :cond_3
    new-instance v1, Lfwu;

    if-eqz v6, :cond_b

    sget-object v4, Lfuu;->g:Landroid/text/TextPaint;

    :goto_6
    if-eqz v6, :cond_c

    sget-object v5, Lfuu;->d:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_7
    if-eqz v6, :cond_d

    sget-object v6, Lfuu;->e:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_8
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->z:Z

    if-eqz v7, :cond_e

    move-object/from16 v7, p0

    :goto_9
    invoke-direct/range {v1 .. v9}, Lfwu;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;II)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    .line 332
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, v8, v1

    .line 333
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v16, v3

    div-int/lit8 v3, v3, 0x2

    .line 334
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4, v1, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 335
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->z:Z

    if-eqz v3, :cond_4

    .line 336
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 338
    :cond_4
    sget-object v3, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->n:Llct;

    iget v3, v3, Llct;->k:I

    sub-int v8, v1, v3

    .line 360
    :goto_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->E:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 361
    const/16 v1, 0x10

    .line 362
    invoke-static {v2, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->E:Ljava/lang/String;

    sub-int v3, v8, v17

    const/4 v4, 0x1

    .line 361
    invoke-static {v1, v2, v3, v4}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->v:Landroid/text/StaticLayout;

    .line 365
    :cond_5
    return-void

    .line 295
    :cond_6
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->B:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 299
    :cond_7
    const v1, 0x7f0a058a

    .line 302
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 303
    :cond_8
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 318
    :cond_9
    const/4 v1, 0x0

    move v6, v1

    goto/16 :goto_4

    .line 319
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->C:Llah;

    invoke-virtual {v1}, Llah;->b()I

    move-result v1

    goto/16 :goto_5

    .line 324
    :cond_b
    sget-object v4, Lfuu;->f:Landroid/text/TextPaint;

    goto/16 :goto_6

    :cond_c
    sget-object v5, Lfuu;->b:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_7

    :cond_d
    sget-object v6, Lfuu;->c:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_8

    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_9

    .line 340
    :cond_f
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->r:Lfwu;

    goto :goto_a

    .line 343
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->F:Z

    if-nez v1, :cond_11

    .line 345
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 346
    const v1, 0x7f0a065c

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 347
    new-instance v1, Lfwu;

    sget-object v3, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->c:Landroid/graphics/Bitmap;

    const/16 v5, 0x1f

    .line 348
    invoke-static {v2, v5}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->d:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v7, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->e:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v11, 0x1

    const/4 v12, 0x1

    sget v13, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->i:I

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v8, p0

    invoke-direct/range {v1 .. v15}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IIZZIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    .line 352
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    sget v3, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->h:I

    invoke-virtual {v1, v3}, Lfwu;->a(I)V

    .line 353
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, v9, v1

    .line 354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v16, v3

    div-int/lit8 v3, v3, 0x2

    .line 355
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4, v1, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 356
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->o:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->t:Lfwu;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 357
    sget-object v3, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->n:Llct;

    iget v3, v3, Llct;->k:I

    sub-int v8, v1, v3

    goto/16 :goto_a

    :cond_11
    move v8, v9

    goto/16 :goto_a

    :cond_12
    move v8, v9

    move v9, v10

    goto/16 :goto_3
.end method
