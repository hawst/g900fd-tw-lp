.class public Lcom/google/android/apps/plus/views/PhotoViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "PG"


# instance fields
.field private a:I

.field private b:Lgak;

.field private c:I

.field private d:Landroid/widget/Scroller;

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v0, Lgaj;

    invoke-direct {v0, p0}, Lgaj;-><init>(Lcom/google/android/apps/plus/views/PhotoViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->e:Ljava/lang/Runnable;

    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->o()V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    new-instance v0, Lgaj;

    invoke-direct {v0, p0}, Lgaj;-><init>(Lcom/google/android/apps/plus/views/PhotoViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->e:Ljava/lang/Runnable;

    .line 82
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->o()V

    .line 83
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/PhotoViewPager;I)I
    .locals 0

    .prologue
    .line 20
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->c:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/PhotoViewPager;)Landroid/widget/Scroller;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->d:Landroid/widget/Scroller;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/PhotoViewPager;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->c:I

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/views/PhotoViewPager;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->e:Ljava/lang/Runnable;

    return-object v0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 86
    const/4 v0, 0x1

    new-instance v1, Lfzq;

    invoke-direct {v1}, Lfzq;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(ZLkd;)V

    .line 87
    return-void
.end method


# virtual methods
.method public a(Lgak;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->b:Lgak;

    .line 145
    return-void
.end method

.method public l()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->m()V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->f()Z

    .line 150
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-direct {v0, v2, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->d:Landroid/widget/Scroller;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->d:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getWidth()I

    move-result v3

    const/16 v5, 0x258

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->post(Ljava/lang/Runnable;)Z

    .line 153
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->f()Z

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->g()V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 165
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->c:I

    .line 166
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->c:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->b:Lgak;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->b:Lgak;

    invoke-interface {v0}, Lgak;->G_()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 100
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    .line 102
    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    if-ne v3, v1, :cond_1

    .line 103
    :cond_0
    iput v5, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->a:I

    .line 106
    :cond_1
    sparse-switch v3, :sswitch_data_0

    .line 132
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_2
    return v2

    :cond_3
    move v0, v2

    .line 97
    goto :goto_0

    .line 108
    :sswitch_0
    if-eqz v0, :cond_2

    .line 109
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->a:I

    .line 110
    if-eq v0, v5, :cond_2

    goto :goto_2

    .line 121
    :sswitch_1
    invoke-static {p1, v2}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->a:I

    goto :goto_1

    .line 126
    :sswitch_2
    invoke-static {p1}, Lik;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 127
    invoke-static {p1, v0}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 128
    iget v4, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->a:I

    if-ne v3, v4, :cond_2

    .line 130
    if-nez v0, :cond_4

    .line 131
    :goto_3
    invoke-static {p1, v1}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoViewPager;->a:I

    goto :goto_1

    :cond_4
    move v1, v2

    .line 130
    goto :goto_3

    .line 106
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x6 -> :sswitch_2
    .end sparse-switch
.end method
