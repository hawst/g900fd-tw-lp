.class public Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 30
    invoke-direct {p0}, Lloa;-><init>()V

    .line 35
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 37
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->x:Llnh;

    .line 38
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 40
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->x:Llnh;

    .line 41
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 42
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 44
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 45
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 48
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->x:Llnh;

    .line 49
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    const/4 v1, 0x1

    .line 50
    invoke-virtual {v0, v1}, Lhet;->a(Z)Lhet;

    .line 52
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 97
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 99
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 100
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 102
    return-void
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->b(Z)V

    .line 79
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 57
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 59
    if-nez p1, :cond_0

    .line 60
    new-instance v0, Lfop;

    invoke-direct {v0}, Lfop;-><init>()V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 62
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 63
    const-string v3, "story_id"

    const-string v4, "story_id"

    .line 64
    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 63
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v3, "story_render_sizes"

    const-string v4, "story_render_sizes"

    .line 66
    invoke-virtual {v1, v4}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 65
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 67
    const-string v3, "story_element_ref"

    const-string v4, "story_element_ref"

    .line 68
    invoke-virtual {v1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 67
    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    invoke-virtual {v0, v2}, Lu;->f(Landroid/os/Bundle;)V

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 73
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;->setContentView(I)V

    .line 74
    return-void
.end method
