.class public Lcom/google/android/apps/plus/stories/phone/StoryActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;

.field private f:Lfos;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Lloa;-><init>()V

    .line 36
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 37
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->y:Llqc;

    const/high16 v2, 0x7f120000

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->x:Llnh;

    .line 38
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 39
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 41
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->x:Llnh;

    .line 42
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 45
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->y:Llqc;

    const v1, 0x1020002

    invoke-direct {v0, p0, v1}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 55
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 56
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 57
    return-void
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->b(Z)V

    .line 115
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->b(Landroid/graphics/drawable/Drawable;)V

    .line 116
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->f:Lfos;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->f:Lfos;

    invoke-virtual {v0}, Lfos;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-super {p0}, Lloa;->onBackPressed()V

    goto :goto_0
.end method

.method public onContextMenuClosed(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0, p1}, Lloa;->onContextMenuClosed(Landroid/view/Menu;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->f:Lfos;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->f:Lfos;

    invoke-virtual {v0}, Lfos;->e()V

    .line 101
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 61
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 64
    new-instance v0, Lkoe;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    invoke-virtual {v0, p0}, Lkoe;->a(Landroid/content/Context;)V

    .line 66
    if-nez p1, :cond_0

    .line 67
    new-instance v0, Lfos;

    invoke-direct {v0}, Lfos;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->f:Lfos;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 70
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 71
    const-string v2, "story_id"

    const-string v3, "story_id"

    .line 72
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 71
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v2, "auth_key"

    const-string v3, "auth_key"

    .line 74
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 73
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v2, "gpinv"

    const-string v3, "gpinv"

    .line 76
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 75
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v2, "from_url_gateway"

    const-string v3, "from_url_gateway"

    const/4 v4, 0x0

    .line 78
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 77
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 79
    const-string v2, "owner_id"

    const-string v3, "owner_id"

    .line 80
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->f:Lfos;

    invoke-virtual {v0, v1}, Lfos;->f(Landroid/os/Bundle;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->e:Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->f:Lfos;

    invoke-virtual {v0, v1}, Ldie;->a(Lu;)V

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/StoryActivity;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->f()V

    .line 93
    return-void
.end method
