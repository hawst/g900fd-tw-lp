.class public Lcom/google/android/apps/plus/phone/EventLocationActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Leaz;
.implements Lhmq;


# instance fields
.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Lloa;-><init>()V

    .line 37
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 38
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->x:Llnh;

    .line 39
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 40
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lhmw;->Z:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 103
    return-void
.end method

.method public a(Lpao;)V
    .locals 3

    .prologue
    .line 91
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 92
    if-eqz p1, :cond_0

    .line 93
    const-string v1, "location"

    invoke-static {p1}, Loxu;->a(Loxu;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 95
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->setResult(ILandroid/content/Intent;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->finish()V

    .line 97
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 70
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    if-eqz v0, :cond_0

    .line 71
    check-cast p1, Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    .line 72
    invoke-virtual {p1, p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->a(Leaz;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->a(Ljava/lang/String;)V

    .line 77
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 48
    if-nez p1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 50
    if-eqz v0, :cond_0

    .line 52
    :try_start_0
    new-instance v1, Lpao;

    invoke-direct {v1}, Lpao;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lpao;

    .line 53
    iget-object v0, v0, Lpao;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->e:Ljava/lang/String;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :cond_0
    :goto_0
    const v0, 0x7f040098

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->setContentView(I)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->h()Loo;

    move-result-object v0

    .line 64
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Loo;->c(Z)V

    .line 65
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lley;->a(Loo;Z)V

    .line 66
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    const-string v1, "EventLocationActivity"

    const-string v2, "Unable to parse Place from byte array."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 81
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 82
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->onBackPressed()V

    .line 84
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lloa;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
