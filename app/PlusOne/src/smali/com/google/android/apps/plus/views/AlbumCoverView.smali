.class public Lcom/google/android/apps/plus/views/AlbumCoverView;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"

# interfaces
.implements Lljv;
.implements Llke;


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/drawable/Drawable;

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Landroid/graphics/drawable/Drawable;

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I


# instance fields
.field private j:I

.field private k:I

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/CharSequence;

.field private n:Landroid/text/StaticLayout;

.field private o:Llir;

.field private p:Lfvw;

.field private q:Lfuo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 81
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 83
    sget-boolean v0, Lcom/google/android/apps/plus/views/AlbumCoverView;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v2, Lcom/google/android/apps/plus/views/AlbumCoverView;->b:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f020490

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->c:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f020046

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->d:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0d0299

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->e:I

    const v1, 0x7f0d029a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->f:I

    const v1, 0x7f0d029b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->g:I

    const v1, 0x7f0d029c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->h:I

    const v1, 0x7f0d029d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AlbumCoverView;->i:I

    sput-boolean v3, Lcom/google/android/apps/plus/views/AlbumCoverView;->a:Z

    .line 85
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->b(I)V

    .line 86
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/AlbumCoverView;->d(I)V

    .line 87
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/AlbumCoverView;->j(Z)V

    .line 88
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCoverView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 89
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCoverView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->d(Landroid/graphics/drawable/Drawable;)V

    .line 90
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCoverView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->c(Landroid/graphics/drawable/Drawable;)V

    .line 91
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->e(Landroid/graphics/drawable/Drawable;)V

    .line 93
    new-instance v0, Lfuo;

    invoke-direct {v0, p0}, Lfuo;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->q:Lfuo;

    .line 94
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 222
    invoke-super {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 224
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->l:Ljava/lang/CharSequence;

    .line 225
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->m:Ljava/lang/CharSequence;

    .line 226
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    .line 227
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    .line 228
    return-void
.end method

.method public a(Landroid/text/style/URLSpan;)V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->p:Lfvw;

    if-eqz v0, :cond_0

    .line 269
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    invoke-virtual {v1}, Llir;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 270
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->p:Lfvw;

    invoke-interface {v1, p0, v0}, Lfvw;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 272
    :cond_0
    return-void
.end method

.method public a(Lfvw;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->p:Lfvw;

    .line 263
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->l:Ljava/lang/CharSequence;

    .line 99
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->m:Ljava/lang/CharSequence;

    .line 104
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v1

    .line 179
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 180
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->onDraw(Landroid/graphics/Canvas;)V

    .line 181
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->r()Z

    move-result v1

    if-nez v1, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 190
    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    if-eqz v1, :cond_2

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 198
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    if-eqz v1, :cond_3

    .line 199
    sget v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->i:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    invoke-virtual {v2}, Llir;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 202
    :cond_3
    sget v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->e:I

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getMeasuredHeight()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/AlbumCoverView;->h:I

    sub-int/2addr v2, v3

    sub-int v0, v2, v0

    .line 205
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    if-eqz v2, :cond_4

    .line 206
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 208
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 209
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/AlbumCoverView;->i:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 212
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    if-eqz v2, :cond_0

    .line 213
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 214
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    invoke-virtual {v2, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    .line 215
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    sget v0, Lcom/google/android/apps/plus/views/AlbumCoverView;->i:I

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 119
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/social/media/ui/MediaView;->onLayout(ZIIII)V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getMeasuredWidth()I

    move-result v0

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getMeasuredHeight()I

    move-result v6

    .line 124
    sget v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->e:I

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->f:I

    sub-int v2, v0, v1

    .line 125
    sget v0, Lcom/google/android/apps/plus/views/AlbumCoverView;->g:I

    sub-int v0, v6, v0

    sget v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->h:I

    sub-int v1, v0, v1

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->l:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/16 v4, 0x29

    invoke-static {v5, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    invoke-static {v4}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v7

    div-int v7, v1, v7

    if-lez v7, :cond_2

    const/4 v8, 0x2

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v4, v0, v2, v7}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    if-eqz v0, :cond_3

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->n:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v4, Lcom/google/android/apps/plus/views/AlbumCoverView;->i:I

    add-int/2addr v0, v4

    sub-int v0, v1, v0

    move v4, v0

    .line 134
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->q:Lfuo;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    invoke-virtual {v0, v1}, Lfuo;->b(Llip;)V

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->m:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x25

    invoke-static {v5, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v0}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v5

    div-int/2addr v4, v5

    if-lez v4, :cond_0

    const/4 v3, 0x1

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Llir;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIFLljv;)Llir;

    move-result-object v3

    :cond_0
    iput-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    sget v0, Lcom/google/android/apps/plus/views/AlbumCoverView;->i:I

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    sget v1, Lcom/google/android/apps/plus/views/AlbumCoverView;->e:I

    sget v2, Lcom/google/android/apps/plus/views/AlbumCoverView;->h:I

    sub-int v2, v6, v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    .line 141
    invoke-virtual {v3}, Llir;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 140
    invoke-virtual {v0, v1, v2}, Llir;->a(II)V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->q:Lfuo;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->o:Llir;

    invoke-virtual {v0, v1}, Lfuo;->a(Llip;)V

    .line 144
    :cond_1
    return-void

    :cond_2
    move-object v0, v3

    .line 129
    goto :goto_0

    :cond_3
    move v4, v1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->onMeasure(II)V

    .line 110
    iget v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->j:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->j:I

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->k:I

    .line 113
    iget v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->j:I

    iget v1, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->k:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/AlbumCoverView;->a(II)V

    .line 115
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCoverView;->q:Lfuo;

    invoke-virtual {v0, p1}, Lfuo;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 258
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
