.class public Lcom/google/android/apps/plus/views/ProfileAlbumTileView;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# static fields
.field private static a:Landroid/graphics/Bitmap;

.field private static b:Landroid/graphics/Bitmap;

.field private static c:Landroid/graphics/Bitmap;

.field private static d:Landroid/graphics/Bitmap;


# instance fields
.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private h:Landroid/widget/ImageView;

.field private i:Ljava/lang/String;

.field private j:Lgas;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->a(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->a(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->a(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 71
    const v1, 0x7f02030c

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->a:Landroid/graphics/Bitmap;

    .line 72
    const v1, 0x7f02017c

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->b:Landroid/graphics/Bitmap;

    .line 73
    const v1, 0x7f0201a7

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->c:Landroid/graphics/Bitmap;

    .line 74
    const v1, 0x7f0201cb

    .line 75
    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->d:Landroid/graphics/Bitmap;

    .line 77
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->g:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 83
    return-void
.end method

.method public a(Landroid/database/Cursor;Lgas;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x6

    const/4 v6, 0x0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->e:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->f:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->g:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v1, v2, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 106
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 110
    const/4 v0, -0x1

    .line 112
    invoke-interface {p1, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 113
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    move v1, v0

    .line 116
    :goto_1
    const/4 v0, 0x0

    .line 118
    packed-switch v1, :pswitch_data_0

    .line 137
    :goto_2
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 139
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->i:Ljava/lang/String;

    .line 141
    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iput-object p2, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->j:Lgas;

    .line 143
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11003b

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 103
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 102
    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 120
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->d:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 124
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->b:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 128
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->a:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 132
    :pswitch_4
    sget-object v0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->c:Landroid/graphics/Bitmap;

    goto :goto_2

    :cond_1
    move v1, v0

    goto :goto_1

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->j:Lgas;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->j:Lgas;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgas;->a(Ljava/lang/String;)V

    .line 158
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 88
    const v0, 0x7f10013b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->e:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f100194

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->f:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f10002e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->g:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 91
    const v0, 0x7f100193

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->h:Landroid/widget/ImageView;

    .line 92
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 147
    .line 148
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 150
    return-void

    :cond_0
    move p2, p1

    .line 148
    goto :goto_0
.end method
