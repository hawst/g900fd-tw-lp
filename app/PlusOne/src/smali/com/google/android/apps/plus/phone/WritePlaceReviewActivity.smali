.class public Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;


# instance fields
.field private final e:Ldie;

.field private f:Lerf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lloa;-><init>()V

    .line 25
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 26
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->x:Llnh;

    .line 27
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 30
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->y:Llqc;

    const v1, 0x1020002

    invoke-direct {v0, p0, v1}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lhmw;->n:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 47
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0, p1}, Lloa;->a(Lu;)V

    .line 38
    instance-of v0, p1, Lerf;

    if-eqz v0, :cond_0

    .line 39
    check-cast p1, Lerf;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->f:Lerf;

    .line 41
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->f:Lerf;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->f:Lerf;

    invoke-virtual {v0}, Lerf;->d()V

    .line 64
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 53
    if-nez p1, :cond_0

    .line 54
    new-instance v0, Lerf;

    invoke-direct {v0}, Lerf;-><init>()V

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/WritePlaceReviewActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 57
    :cond_0
    return-void
.end method
