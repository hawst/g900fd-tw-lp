.class public Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lfwv;
.implements Lfwx;
.implements Lljh;


# static fields
.field private static a:I

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:Landroid/graphics/Paint;

.field private static i:Landroid/graphics/Paint;

.field private static j:Landroid/graphics/Bitmap;

.field private static k:Landroid/graphics/Bitmap;


# instance fields
.field private l:Lljg;

.field private m:Lfww;

.field private n:Lfwu;

.field private o:Lfwu;

.field private p:Lfwu;

.field private final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private r:Llip;

.field private s:Ldax;

.field private t:Lfvu;

.field private u:Loae;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lfvu;->a:Lfvu;

    if-nez v1, :cond_0

    new-instance v1, Lfvu;

    invoke-direct {v1, v0}, Lfvu;-><init>(Landroid/content/Context;)V

    sput-object v1, Lfvu;->a:Lfvu;

    :cond_0
    sget-object v0, Lfvu;->a:Lfvu;

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->t:Lfvu;

    .line 81
    sget-object v0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->i:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 84
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 85
    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->i:Landroid/graphics/Paint;

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 85
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    sget-object v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->i:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 89
    const v1, 0x7f0d0304

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->b:I

    .line 90
    const v1, 0x7f0d0305

    .line 91
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->a:I

    .line 92
    const v1, 0x7f0d0306

    .line 93
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->c:I

    .line 94
    const v1, 0x7f0d0307

    .line 95
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->d:I

    .line 96
    const v1, 0x7f0d0302

    .line 97
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    .line 98
    const v1, 0x7f0d0303

    .line 99
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->f:I

    .line 101
    const v1, 0x7f0d0308

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->g:I

    .line 104
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 105
    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->h:Landroid/graphics/Paint;

    const v2, 0x7f0b02fd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 106
    sget-object v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->h:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 107
    sget-object v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->h:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->g:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 109
    const v1, 0x7f020347

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->j:Landroid/graphics/Bitmap;

    .line 110
    const v1, 0x7f02018f

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->k:Landroid/graphics/Bitmap;

    .line 112
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 244
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->r:Llip;

    .line 245
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    .line 246
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->m:Lfww;

    .line 247
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    .line 248
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    .line 249
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->s:Ldax;

    .line 250
    iput-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    .line 251
    return-void
.end method

.method public a(IZ)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 274
    if-lez p1, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 275
    :goto_0
    if-nez v3, :cond_1

    if-nez p2, :cond_1

    .line 276
    iput-object v7, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->requestLayout()V

    .line 278
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->invalidate()V

    .line 291
    :goto_1
    return-void

    :cond_0
    move-object v3, v7

    .line 274
    goto :goto_0

    .line 281
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 282
    new-instance v0, Lfwu;

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->k:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->t:Lfvu;

    iget-object v4, v4, Lfvu;->b:Landroid/text/TextPaint;

    const v5, 0x7f020096

    .line 284
    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/NinePatchDrawable;

    const v9, 0x7f020097

    .line 285
    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    check-cast v6, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz p2, :cond_2

    move-object v7, p0

    :cond_2
    move v9, v8

    invoke-direct/range {v0 .. v9}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;II)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->requestLayout()V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->invalidate()V

    goto :goto_1
.end method

.method public a(Ldax;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->s:Ldax;

    .line 255
    return-void
.end method

.method public a(Lfwu;)V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->s:Ldax;

    if-nez v0, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->s:Ldax;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    iget-object v1, v1, Loae;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-interface {v0}, Ldax;->b()V

    goto :goto_0

    .line 233
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->s:Ldax;

    invoke-interface {v0}, Ldax;->a()V

    goto :goto_0

    .line 235
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->s:Ldax;

    invoke-interface {v0}, Ldax;->c()V

    goto :goto_0
.end method

.method public a(Lfww;)V
    .locals 3

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->s:Ldax;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    iget-object v1, v1, Loae;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    iget-object v2, v2, Loae;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ldax;->a(Ljava/lang/String;I)V

    .line 357
    return-void
.end method

.method public a(Loae;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 294
    iput-object p1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    if-nez v0, :cond_0

    move v0, v7

    .line 297
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    iget-object v2, v2, Loae;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    move v3, v1

    .line 298
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a059c

    new-array v5, v1, [Ljava/lang/Object;

    .line 299
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    .line 298
    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 303
    new-instance v0, Lfwu;

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v3, :cond_2

    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x1d

    invoke-static {v3, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    .line 305
    :goto_2
    const v4, 0x7f020096

    .line 306
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/NinePatchDrawable;

    const v6, 0x7f020097

    .line 307
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/NinePatchDrawable;

    move-object v6, p0

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lfwu;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;II)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->requestLayout()V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->invalidate()V

    .line 313
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    iget-object v0, v0, Loae;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_1
    move v3, v7

    .line 297
    goto :goto_1

    .line 305
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    goto :goto_2
.end method

.method public a(Z)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 259
    if-eqz p1, :cond_0

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 261
    new-instance v0, Lfwu;

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->j:Landroid/graphics/Bitmap;

    const v3, 0x7f020096

    .line 262
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/NinePatchDrawable;

    const v5, 0x7f020097

    .line 263
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/NinePatchDrawable;

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0a0589

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v5, p0

    move v7, v6

    move v9, v6

    invoke-direct/range {v0 .. v9}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;I)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    :goto_0
    return-void

    .line 268
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 180
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    .line 181
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    .line 183
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 221
    :goto_0
    return v0

    .line 185
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 187
    invoke-interface {v0, v4, v5, v2}, Llip;->a(III)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 188
    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->r:Llip;

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->invalidate()V

    :cond_0
    move v0, v1

    .line 193
    goto :goto_0

    .line 185
    :cond_1
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    .line 197
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->r:Llip;

    if-eqz v0, :cond_2

    .line 198
    iput-object v3, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->r:Llip;

    .line 199
    invoke-virtual {p0, v2}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->setPressed(Z)V

    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->invalidate()V

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_2
    if-ltz v3, :cond_3

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 205
    invoke-interface {v0, v4, v5, v1}, Llip;->a(III)Z

    move-result v0

    or-int/2addr v2, v0

    .line 203
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 207
    goto :goto_0

    .line 211
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->r:Llip;

    if-eqz v0, :cond_4

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->r:Llip;

    const/4 v2, 0x3

    invoke-interface {v0, v4, v5, v2}, Llip;->a(III)Z

    .line 213
    iput-object v3, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->r:Llip;

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->invalidate()V

    move v0, v1

    .line 215
    goto :goto_0

    :cond_4
    move v0, v2

    .line 217
    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getWidth()I

    move-result v6

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getHeight()I

    move-result v0

    .line 158
    int-to-float v3, v6

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->i:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 160
    sget v0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    .line 161
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    if-eqz v1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v1

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    if-eqz v1, :cond_1

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v1

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    if-eqz v1, :cond_2

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v1

    .line 174
    :cond_2
    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->a:I

    int-to-float v1, v1

    int-to-float v2, v0

    sget v3, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->c:I

    sub-int v3, v6, v3

    int-to-float v3, v3

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 176
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 116
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getPaddingLeft()I

    move-result v0

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->a:I

    add-int v8, v0, v1

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getPaddingTop()I

    move-result v0

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->b:I

    add-int/2addr v1, v0

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getMeasuredWidth()I

    move-result v10

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    if-eqz v0, :cond_6

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/2addr v2, v8

    iget-object v3, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    .line 126
    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v1

    .line 125
    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v2

    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->g:I

    add-int/2addr v0, v2

    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v2

    .line 130
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    if-eqz v2, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->f:I

    add-int/2addr v0, v2

    .line 133
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    .line 134
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v1

    .line 133
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->o:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v2

    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->g:I

    add-int/2addr v0, v2

    sget v2, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v2

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    if-eqz v2, :cond_5

    .line 139
    sget v0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    sub-int v0, v10, v0

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v4, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    .line 141
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v1

    .line 140
    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->p:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->g:I

    add-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->e:I

    add-int/2addr v0, v1

    move v9, v0

    .line 146
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    if-nez v0, :cond_2

    move v0, v7

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->n:Lfwu;

    if-nez v1, :cond_3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    .line 148
    :goto_4
    sget v0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->d:I

    add-int/2addr v0, v9

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v10, v0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->setMeasuredDimension(II)V

    .line 149
    return-void

    :cond_1
    move v0, v8

    .line 131
    goto/16 :goto_1

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->u:Loae;

    iget-object v0, v0, Loae;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3

    :cond_3
    if-lez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11006e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    invoke-static {v2, v1}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v3

    new-instance v0, Lljg;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    invoke-virtual {v0, v8, v9}, Lljg;->a(II)V

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->m:Lfww;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    new-instance v2, Lfww;

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    invoke-virtual {v0}, Lljg;->getWidth()I

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    invoke-virtual {v0}, Lljg;->getHeight()I

    move-result v6

    move v3, v8

    move v4, v9

    move-object v7, p0

    move-object v8, v1

    invoke-direct/range {v2 .. v8}, Lfww;-><init>(IIIILfwx;Ljava/lang/CharSequence;)V

    iput-object v2, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->m:Lfww;

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->q:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->m:Lfww;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->l:Lljg;

    invoke-virtual {v0}, Lljg;->getHeight()I

    move-result v0

    add-int/2addr v9, v0

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a08b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_5
    move v9, v0

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method
