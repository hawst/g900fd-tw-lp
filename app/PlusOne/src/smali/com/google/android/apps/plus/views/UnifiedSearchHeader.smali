.class public Lcom/google/android/apps/plus/views/UnifiedSearchHeader;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:I

.field private static b:I

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Landroid/graphics/drawable/Drawable;

.field private static e:Landroid/graphics/drawable/Drawable;


# instance fields
.field private A:I

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/LinearLayout;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/widget/LinearLayout;

.field private q:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

.field private r:Lgby;

.field private s:Landroid/text/SpannableStringBuilder;

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation
.end field

.field private w:I

.field private x:Ljava/lang/Runnable;

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 103
    sget v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    if-nez v1, :cond_0

    .line 104
    const v1, 0x7f0d0345

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    .line 105
    const v1, 0x7f0d0347

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    .line 107
    const v1, 0x7f020197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c:Landroid/graphics/drawable/Drawable;

    .line 109
    const v1, 0x7f0b02ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 110
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->d:Landroid/graphics/drawable/Drawable;

    .line 111
    const v1, 0x7f020490

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->e:Landroid/graphics/drawable/Drawable;

    .line 114
    :cond_0
    const v1, 0x7f0d0346

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->w:I

    .line 116
    new-instance v0, Lgbt;

    invoke-direct {v0, p0}, Lgbt;-><init>(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->x:Ljava/lang/Runnable;

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 103
    sget v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    if-nez v1, :cond_0

    .line 104
    const v1, 0x7f0d0345

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    .line 105
    const v1, 0x7f0d0347

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    .line 107
    const v1, 0x7f020197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c:Landroid/graphics/drawable/Drawable;

    .line 109
    const v1, 0x7f0b02ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 110
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->d:Landroid/graphics/drawable/Drawable;

    .line 111
    const v1, 0x7f020490

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->e:Landroid/graphics/drawable/Drawable;

    .line 114
    :cond_0
    const v1, 0x7f0d0346

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->w:I

    .line 116
    new-instance v0, Lgbt;

    invoke-direct {v0, p0}, Lgbt;-><init>(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->x:Ljava/lang/Runnable;

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 103
    sget v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    if-nez v1, :cond_0

    .line 104
    const v1, 0x7f0d0345

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    .line 105
    const v1, 0x7f0d0347

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    .line 107
    const v1, 0x7f020197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c:Landroid/graphics/drawable/Drawable;

    .line 109
    const v1, 0x7f0b02ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 110
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->d:Landroid/graphics/drawable/Drawable;

    .line 111
    const v1, 0x7f020490

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->e:Landroid/graphics/drawable/Drawable;

    .line 114
    :cond_0
    const v1, 0x7f0d0346

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->w:I

    .line 116
    new-instance v0, Lgbt;

    invoke-direct {v0, p0}, Lgbt;-><init>(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->x:Ljava/lang/Runnable;

    .line 99
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v1, 0x0

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->y:I

    if-ge v0, v2, :cond_1

    new-instance v4, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v4, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    invoke-virtual {v4, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    sget v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    sget v6, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    invoke-direct {v5, v2, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-lez v0, :cond_0

    sget v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    :goto_1
    iput v2, v5, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v0, v1

    :goto_2
    iget v2, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->z:I

    if-ge v0, v2, :cond_3

    new-instance v4, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v4, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    sget-object v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->c(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v4, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    sget v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    sget v6, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    invoke-direct {v5, v2, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-lez v0, :cond_2

    sget v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    :goto_3
    iput v2, v5, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_3

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->d()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v0, v1

    :goto_4
    iget v2, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->A:I

    if-ge v0, v2, :cond_5

    new-instance v4, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v4, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x2

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    sget-object v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->c(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v4, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->w:I

    iget v6, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->w:I

    invoke-direct {v5, v2, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-lez v0, :cond_4

    sget v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    :goto_5
    iput v2, v5, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    move v2, v1

    goto :goto_5

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->e()V

    :cond_6
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)Lgby;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->r:Lgby;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->s:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->s:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 326
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->f()V

    .line 327
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->g:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)Lcom/google/android/libraries/social/ui/tabbar/TabBar;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->q:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    return-object v0
.end method

.method private c()V
    .locals 8

    .prologue
    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->t:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    move v0, v3

    .line 337
    :goto_0
    if-nez v0, :cond_2

    .line 338
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->i:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 343
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    .line 344
    const-string v1, "UnifiedSearchHeader"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 345
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "bindPeople(): availableCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; maxCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 349
    :cond_0
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v5, v4, v0

    move v1, v3

    .line 351
    :goto_2
    if-ge v1, v5, :cond_4

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 353
    invoke-virtual {v0, v7, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 351
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 340
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->i:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 356
    :goto_3
    if-ge v2, v4, :cond_3

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 358
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->t:Ljava/util/ArrayList;

    sub-int v6, v2, v5

    .line 359
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-virtual {v0, v7, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 356
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 363
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->f()V

    .line 364
    return-void

    :cond_4
    move v2, v1

    goto :goto_3
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v6, 0x8

    const/4 v3, 0x0

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->u:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    move v0, v3

    .line 374
    :goto_0
    if-nez v0, :cond_2

    .line 375
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->l:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 380
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    .line 381
    const-string v1, "UnifiedSearchHeader"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x43

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "bindCommunities(): availableCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; maxCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 386
    :cond_0
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v5, v4, v0

    move v1, v3

    .line 388
    :goto_2
    if-ge v1, v5, :cond_5

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 390
    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 391
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 388
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 372
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 377
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->l:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 393
    :goto_3
    if-ge v2, v4, :cond_4

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 395
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->u:Ljava/util/ArrayList;

    sub-int v6, v2, v5

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 396
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 397
    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 401
    :goto_4
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 393
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 399
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, Ljac;->a:Ljac;

    invoke-static {v6, v1, v7}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    goto :goto_4

    .line 404
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->f()V

    .line 405
    return-void

    :cond_5
    move v2, v1

    goto :goto_3
.end method

.method private e()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x0

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->v:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    move v0, v3

    .line 415
    :goto_0
    if-nez v0, :cond_2

    .line 416
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->o:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 421
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    .line 422
    const-string v1, "UnifiedSearchHeader"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 423
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "bindPhotos(): availableCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; maxCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 427
    :cond_0
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v5, v4, v0

    move v1, v3

    .line 429
    :goto_2
    if-ge v1, v5, :cond_4

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 431
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 432
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 429
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 418
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->o:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 434
    :goto_3
    if-ge v2, v4, :cond_3

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 436
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->v:Ljava/util/ArrayList;

    sub-int v6, v2, v5

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizu;

    .line 437
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 438
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 434
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 441
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->f()V

    .line 442
    return-void

    :cond_4
    move v2, v1

    goto :goto_3
.end method

.method private f()V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 446
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->i:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    .line 447
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->l:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_4

    move v4, v1

    .line 448
    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->o:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_5

    .line 450
    :goto_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->h:Landroid/view/View;

    if-eqz v0, :cond_6

    if-nez v3, :cond_0

    if-nez v4, :cond_0

    if-eqz v1, :cond_6

    :cond_0
    move v0, v2

    :goto_4
    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 452
    iget-object v6, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->k:Landroid/view/View;

    if-eqz v3, :cond_7

    if-nez v4, :cond_1

    if-eqz v1, :cond_7

    :cond_1
    move v0, v2

    :goto_5
    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 454
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->n:Landroid/view/View;

    if-eqz v4, :cond_8

    if-eqz v1, :cond_8

    :goto_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 456
    return-void

    :cond_2
    move v0, v2

    .line 445
    goto :goto_0

    :cond_3
    move v3, v2

    .line 446
    goto :goto_1

    :cond_4
    move v4, v2

    .line 447
    goto :goto_2

    :cond_5
    move v1, v2

    .line 448
    goto :goto_3

    :cond_6
    move v0, v5

    .line 450
    goto :goto_4

    :cond_7
    move v0, v5

    .line 452
    goto :goto_5

    :cond_8
    move v2, v5

    .line 454
    goto :goto_6
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    move v2, v1

    .line 202
    :goto_0
    if-ge v2, v3, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 204
    invoke-virtual {v0, v4, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    move v2, v1

    .line 208
    :goto_1
    if-ge v2, v3, :cond_1

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 210
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 208
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 214
    :goto_2
    if-ge v1, v2, :cond_2

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 216
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 214
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 218
    :cond_2
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->q:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(I)V

    .line 460
    return-void
.end method

.method public a(Landroid/text/SpannableStringBuilder;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->s:Landroid/text/SpannableStringBuilder;

    .line 312
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b()V

    .line 313
    return-void
.end method

.method public a(Lgby;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->r:Lgby;

    .line 127
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330
    iput-object p1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->t:Ljava/util/ArrayList;

    .line 331
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c()V

    .line 332
    return-void
.end method

.method public b(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 367
    iput-object p1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->u:Ljava/util/ArrayList;

    .line 368
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->d()V

    .line 369
    return-void
.end method

.method public c(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lizu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 408
    iput-object p1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->v:Ljava/util/ArrayList;

    .line 409
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->e()V

    .line 410
    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const v4, 0x7f0400c8

    const/4 v3, 0x0

    .line 131
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 132
    const v0, 0x7f100612

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->f:Landroid/view/View;

    .line 133
    const v0, 0x7f100613

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->g:Landroid/widget/TextView;

    .line 134
    const v0, 0x7f100614

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->h:Landroid/view/View;

    .line 135
    const v0, 0x7f100615

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->i:Landroid/view/View;

    .line 136
    const v0, 0x7f100616

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    .line 137
    const v0, 0x7f100617

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->k:Landroid/view/View;

    .line 138
    const v0, 0x7f100618

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->l:Landroid/view/View;

    .line 139
    const v0, 0x7f100619

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    .line 140
    const v0, 0x7f10061a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->n:Landroid/view/View;

    .line 141
    const v0, 0x7f10061b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->o:Landroid/view/View;

    .line 142
    const v0, 0x7f10061c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    .line 143
    const v0, 0x7f100462

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->q:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->q:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    new-instance v1, Lgbu;

    invoke-direct {v1, p0}, Lgbu;-><init>(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(Llhl;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->i:Landroid/view/View;

    new-instance v1, Lgbv;

    invoke-direct {v1, p0}, Lgbv;-><init>(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->l:Landroid/view/View;

    new-instance v1, Lgbw;

    invoke-direct {v1, p0}, Lgbw;-><init>(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->o:Landroid/view/View;

    new-instance v1, Lgbx;

    invoke-direct {v1, p0}, Lgbx;-><init>(Lcom/google/android/apps/plus/views/UnifiedSearchHeader;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->q:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v2, 0x7f0a0a72

    .line 193
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-virtual {v1, v4, v2, v3}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;I)V

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->q:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v2, 0x7f0a0a73

    .line 195
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    .line 194
    invoke-virtual {v1, v4, v0, v2}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;I)V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->q:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(I)V

    .line 197
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    .line 222
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    sget v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->y:I

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    .line 226
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a:I

    sget v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->z:I

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->w:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->w:I

    sget v2, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b:I

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->A:I

    .line 231
    iget v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->y:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->z:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->m:Landroid/widget/LinearLayout;

    .line 232
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->A:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->p:Landroid/widget/LinearLayout;

    .line 233
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->x:Ljava/lang/Runnable;

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 235
    const-string v0, "UnifiedSearchHeader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    const-string v0, "onMeasure(): layout update requested; mMaxPeopleAvatarCount="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->y:I

    iget v2, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->z:I

    iget v3, p0, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->A:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x4f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; mMaxCommunitiesAvatarCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; mMaxPhotoCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 242
    :cond_1
    return-void
.end method
