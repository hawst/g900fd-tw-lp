.class public Lcom/google/android/apps/plus/phone/HostLayout;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lfxd;


# instance fields
.field private a:Landroid/widget/FrameLayout;

.field private b:Lae;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lz;

    .line 56
    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lz;

    .line 56
    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lz;

    .line 56
    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    .line 52
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    const-string v1, "hosted"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_0

    .line 140
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    .line 141
    invoke-virtual {v1, v0}, Lat;->a(Lu;)Lat;

    .line 142
    invoke-virtual {v1}, Lat;->c()I

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    invoke-virtual {v0}, Lae;->b()Z

    .line 145
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/plus/views/EsDrawerLayout;)V
    .locals 2

    .prologue
    .line 175
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostLayout;->a:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(ILandroid/view/View;)V

    .line 177
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(Lfxd;)V

    .line 178
    return-void
.end method

.method public a(Lu;Lx;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostLayout;->b()Lu;

    move-result-object v1

    .line 101
    instance-of v0, p1, Lhmq;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lhmq;

    .line 102
    invoke-interface {v0}, Lhmq;->F_()Lhmw;

    move-result-object v3

    .line 104
    :goto_0
    if-eqz v1, :cond_5

    .line 105
    if-eqz p3, :cond_3

    sget-object v0, Lhmw;->O:Lhmw;

    :goto_1
    move-object v1, v0

    .line 114
    :goto_2
    if-eqz p2, :cond_0

    .line 115
    invoke-virtual {p1, p2}, Lu;->a(Lx;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    .line 118
    const v2, 0x7f100143

    const-string v4, "hosted"

    invoke-virtual {v0, v2, p1, v4}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    .line 119
    if-eqz p3, :cond_6

    .line 120
    const/16 v2, 0x1003

    invoke-virtual {v0, v2}, Lat;->a(I)Lat;

    .line 124
    :goto_3
    invoke-virtual {v0}, Lat;->c()I

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    invoke-virtual {v0}, Lae;->b()Z

    .line 127
    if-eqz v3, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 129
    const-class v0, Lhms;

    invoke-static {v2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    invoke-direct {v4, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 131
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 132
    invoke-virtual {v1, v3}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v1

    .line 129
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 135
    :cond_1
    return-void

    :cond_2
    move-object v3, v2

    .line 102
    goto :goto_0

    .line 105
    :cond_3
    instance-of v0, v1, Lhmq;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Lhmq;

    .line 108
    invoke-interface {v0}, Lhmq;->F_()Lhmw;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_1

    :cond_5
    move-object v1, v3

    .line 111
    goto :goto_2

    .line 122
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lat;->a(I)Lat;

    goto :goto_3
.end method

.method public b()Lu;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    const-string v1, "hosted"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    return-object v0
.end method

.method public c()Lx;
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostLayout;->b()Lu;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostLayout;->b:Lae;

    invoke-virtual {v1, v0}, Lae;->a(Lu;)Lx;

    move-result-object v0

    .line 159
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 63
    const v0, 0x7f1002fd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostLayout;->a:Landroid/widget/FrameLayout;

    .line 64
    const v0, 0x7f1002fb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EsDrawerLayout;

    .line 65
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->a(Lfxd;)V

    .line 66
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 70
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 73
    const v2, 0x7f0d02ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 77
    const v3, 0x7f0d02ef

    .line 78
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int v3, v0, v3

    .line 77
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 79
    const v3, 0x7f0c0073

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 81
    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostLayout;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 86
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 87
    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 164
    invoke-static {p1}, Llii;->f(Landroid/view/View;)V

    .line 165
    return-void
.end method
