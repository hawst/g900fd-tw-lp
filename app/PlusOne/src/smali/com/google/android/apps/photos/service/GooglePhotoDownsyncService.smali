.class public Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;
.super Landroid/app/Service;
.source "PG"


# static fields
.field private static final a:J

.field private static final b:Ljava/lang/Object;

.field private static c:Lcuo;

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldvz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 76
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a:J

    .line 81
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->b:Ljava/lang/Object;

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->d:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 629
    return-void
.end method

.method public static a(Ljava/lang/String;)Ldvz;
    .locals 3

    .prologue
    .line 95
    sget-object v1, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->d:Ljava/util/Map;

    monitor-enter v1

    .line 96
    :try_start_0
    sget-object v0, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->d:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvz;

    .line 97
    if-nez v0, :cond_0

    .line 98
    new-instance v0, Ldvz;

    invoke-direct {v0}, Ldvz;-><init>()V

    .line 99
    const-string v2, "PhotoSyncService"

    invoke-virtual {v0, v2}, Ldvz;->a(Ljava/lang/String;)V

    .line 100
    sget-object v2, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->d:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :cond_0
    monitor-exit v1

    return-object v0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 154
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 155
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "is_google_plus"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 157
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 158
    const-string v3, "is_managed_account"

    invoke-interface {v1, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 159
    const-string v3, "account_name"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 160
    invoke-static {v1}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Landroid/content/Context;Landroid/accounts/Account;)V

    goto :goto_0

    .line 163
    :cond_1
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 229
    .line 230
    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-wide v6, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a:J

    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v3, Lcuk;->b:Lief;

    invoke-interface {v0, v3, v1}, Lieh;->c(Lief;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 231
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v5

    .line 232
    invoke-static {p1, v5}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 233
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/PeriodicSync;

    .line 234
    iget-wide v10, v0, Landroid/content/PeriodicSync;->period:J

    cmp-long v3, v10, v6

    if-nez v3, :cond_1

    move v3, v4

    .line 235
    :goto_1
    if-nez v3, :cond_0

    .line 236
    iget-object v0, v0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    invoke-static {p1, v5, v0}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 238
    :cond_0
    or-int v0, v1, v3

    move v1, v0

    .line 239
    goto :goto_0

    :cond_1
    move v3, v2

    .line 234
    goto :goto_1

    .line 241
    :cond_2
    if-nez v1, :cond_3

    .line 242
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 243
    const-string v1, "sync_periodic"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 244
    invoke-static {p1, v5, v0, v6, v7}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 246
    :cond_3
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 124
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v1

    .line 126
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 127
    invoke-static {p0, v0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 128
    return-void
.end method

.method public static synthetic a(Ljava/lang/Exception;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    instance-of v1, p0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v1, :cond_1

    check-cast p0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {p0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x1f4

    if-eq v1, v2, :cond_0

    const/16 v2, 0x1f6

    if-eq v1, v2, :cond_0

    const/16 v2, 0x1f7

    if-eq v1, v2, :cond_0

    const/16 v2, 0x1f8

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 170
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 171
    const-string v0, "downsync_installed"

    invoke-interface {v2, v0, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 175
    :cond_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 176
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "logged_in"

    aput-object v3, v1, v4

    const-string v3, "is_google_plus"

    aput-object v3, v1, v6

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 178
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v4, "account_name"

    .line 179
    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 180
    new-instance v4, Landroid/accounts/Account;

    const-string v5, "com.google"

    invoke-direct {v4, v1, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v5, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v4, v5}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    .line 184
    if-eqz v4, :cond_1

    .line 185
    invoke-static {p0, v1}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 187
    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 191
    :cond_2
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "downsync_installed"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 132
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v1

    .line 134
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 135
    invoke-static {v0, v1}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method public static synthetic b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 57
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .prologue
    .line 57
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "logged_in"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "is_google_plus"

    aput-object v4, v1, v3

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v5, "account_name"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public static c(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 198
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 199
    const-string v0, "downsync_installed"

    invoke-interface {v2, v0, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    :goto_0
    return-void

    .line 203
    :cond_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 204
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "logged_in"

    aput-object v3, v1, v5

    const/4 v3, 0x1

    const-string v4, "is_google_plus"

    aput-object v4, v1, v3

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 206
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v4, "account_name"

    .line 207
    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 208
    invoke-static {v1}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 211
    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "downsync_installed"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 140
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v1

    .line 142
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 143
    return-void
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 147
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v1

    .line 149
    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->c:Lcuo;

    invoke-virtual {v0}, Lcuo;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 108
    sget-object v1, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 109
    :try_start_0
    sget-object v0, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->c:Lcuo;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lcuo;

    invoke-virtual {p0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcuo;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->c:Lcuo;

    .line 112
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
