.class public Lcom/google/android/apps/photos/phone/PhotosHomeActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Ldhq;
.implements Lheg;
.implements Lhjj;
.implements Lhmq;
.implements Lieg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lloa;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ldhq;",
        "Lheg;",
        "Lhjj;",
        "Lhmq;",
        "Lieg;"
    }
.end annotation


# instance fields
.field private final e:Livx;

.field private final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcsx;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

.field private h:Landroid/os/Bundle;

.field private i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z

.field private k:Landroid/widget/ListView;

.field private l:Lhjf;

.field private m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

.field private n:Lcsr;

.field private o:Landroid/widget/ListView;

.field private p:Leus;

.field private q:Lpg;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 102
    invoke-direct {p0}, Lloa;-><init>()V

    .line 127
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 128
    new-instance v0, Licd;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Licd;-><init>(Landroid/content/Context;Llqr;)V

    .line 129
    new-instance v0, Lctt;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lctt;-><init>(Landroid/app/Activity;Llqr;)V

    .line 132
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    const v2, 0x7f1002fb

    invoke-direct {v0, p0, v1, v2}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    .line 135
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    .line 136
    invoke-virtual {v0, v1}, Livx;->a(Llnh;)Livx;

    move-result-object v0

    const-string v1, "active-photos-account"

    .line 137
    invoke-virtual {v0, v1}, Livx;->a(Ljava/lang/String;)Livx;

    move-result-object v0

    .line 138
    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    .line 143
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->f:Landroid/util/SparseArray;

    .line 147
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->i:Landroid/util/SparseArray;

    .line 150
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    .line 152
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->l:Lhjf;

    .line 150
    return-void
.end method

.method private a(I)Lu;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 260
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f()V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->i()Lu;

    move-result-object v0

    .line 293
    :goto_0
    return-object v0

    .line 265
    :cond_0
    if-nez p1, :cond_1

    .line 266
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    .line 267
    invoke-static {p0, v1}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v1

    .line 268
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 269
    invoke-virtual {p0, v1}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 270
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f()V

    goto :goto_0

    .line 272
    :cond_1
    const/16 v1, 0x9

    if-ne p1, v1, :cond_2

    .line 274
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v1}, Livx;->g()Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 275
    invoke-static {v1}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 276
    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-static {p0, v2, v1, v0}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 278
    invoke-virtual {p0, v1}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 279
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f()V

    goto :goto_0

    .line 283
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->q()V

    .line 285
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const-string v1, "account_id"

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const-string v1, "destination"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 289
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->i:Landroid/util/SparseArray;

    .line 290
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx;

    const/4 v2, 0x1

    .line 289
    invoke-direct {p0, p1, v1, v0, v2}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->a(ILandroid/os/Bundle;Lx;Z)Lu;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ILandroid/os/Bundle;Lx;Z)Lu;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 303
    const-class v0, Lctz;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    .line 304
    invoke-virtual {v0}, Lctz;->c()V

    .line 305
    const-class v0, Lcnt;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f()V

    .line 310
    const-string v2, "Photos"

    .line 312
    packed-switch p1, :pswitch_data_0

    .line 413
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->f:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcsx;

    .line 414
    if-eqz v1, :cond_0

    .line 415
    iget-object v4, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v1, p2, p3, p4, v4}, Lcsx;->a(Landroid/os/Bundle;Lx;ZLcom/google/android/apps/photos/views/PhotosHostLayout;)V

    :cond_0
    move-object v1, v2

    move-object v2, v3

    .line 420
    :goto_0
    invoke-virtual {v0, v1, p1}, Lcnt;->a(Ljava/lang/String;I)V

    .line 422
    return-object v2

    .line 314
    :pswitch_0
    new-instance v1, Leet;

    invoke-direct {v1}, Leet;-><init>()V

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    .line 315
    goto :goto_0

    .line 319
    :pswitch_1
    const-string v2, "Photos"

    .line 321
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 322
    new-instance v3, Legr;

    invoke-direct {v3}, Legr;-><init>()V

    .line 323
    const/4 v1, 0x6

    .line 325
    invoke-static {}, Lecm;->aa()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 326
    const/16 v1, 0x46

    .line 328
    :cond_1
    const-string v5, "tabs"

    invoke-virtual {p2, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 329
    const-string v1, "show_promos"

    invoke-virtual {p2, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 330
    const-string v1, "show_autobackup_status"

    invoke-virtual {p2, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 331
    const-string v1, "starting_tab_index"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 332
    const-string v1, "starting_tab_index"

    const-string v5, "starting_tab_index"

    .line 333
    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 332
    invoke-virtual {p2, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 336
    :cond_2
    const-string v1, "scroll_to_uri"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 337
    if-eqz v1, :cond_3

    .line 338
    const-string v4, "scroll_to_uri"

    invoke-virtual {p2, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 339
    const-string v1, "starting_tab_index"

    invoke-virtual {p2, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 341
    :cond_3
    invoke-virtual {v3, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 342
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v1, v3, p3, p4}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lu;Lx;Z)V

    move-object v1, v2

    move-object v2, v3

    .line 343
    goto :goto_0

    .line 347
    :pswitch_2
    const-string v1, "Albums"

    .line 349
    new-instance v2, Lebs;

    invoke-direct {v2}, Lebs;-><init>()V

    .line 350
    invoke-virtual {v2, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 351
    iget-object v3, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v3, v2, p3, p4}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lu;Lx;Z)V

    goto :goto_0

    .line 357
    :pswitch_3
    const-string v1, "AutoAwesomes"

    .line 359
    new-instance v2, Lecb;

    invoke-direct {v2}, Lecb;-><init>()V

    .line 360
    const-string v4, "hide_search_view"

    invoke-virtual {p2, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 361
    invoke-virtual {v2, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 362
    iget-object v4, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v4, v2, v3, p4}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lu;Lx;Z)V

    goto/16 :goto_0

    .line 367
    :pswitch_4
    const-string v1, "PhotoSearch"

    .line 369
    new-instance v2, Legc;

    invoke-direct {v2}, Legc;-><init>()V

    .line 370
    const-string v4, "query"

    const-string v5, "#videos"

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v4, "is_videos_destination"

    invoke-virtual {p2, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 372
    const-string v4, "hide_search_view"

    invoke-virtual {p2, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 373
    const-string v4, "search_local_videos"

    invoke-virtual {p2, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 374
    invoke-virtual {v2, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 375
    iget-object v4, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v4, v2, v3, p4}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lu;Lx;Z)V

    goto/16 :goto_0

    .line 380
    :pswitch_5
    const-string v1, "PhotosOfYou"

    .line 382
    new-instance v2, Legu;

    invoke-direct {v2}, Legu;-><init>()V

    .line 383
    invoke-virtual {v2, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 384
    iget-object v3, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v3, v2, p3, p4}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lu;Lx;Z)V

    goto/16 :goto_0

    .line 389
    :pswitch_6
    new-instance v1, Lecf;

    invoke-direct {v1}, Lecf;-><init>()V

    .line 390
    const-string v3, "local_folders_only"

    invoke-virtual {p2, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 391
    invoke-virtual {v1, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 392
    iget-object v3, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v3, v1, p3, p4}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lu;Lx;Z)V

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    .line 393
    goto/16 :goto_0

    .line 397
    :pswitch_7
    new-instance v1, Leiu;

    invoke-direct {v1}, Leiu;-><init>()V

    .line 398
    invoke-virtual {v1, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 399
    iget-object v4, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v4, v1, v3, p4}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lu;Lx;Z)V

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    .line 400
    goto/16 :goto_0

    .line 404
    :pswitch_8
    const-string v1, "PhotoSearch"

    .line 406
    new-instance v2, Legc;

    invoke-direct {v2}, Legc;-><init>()V

    .line 407
    invoke-virtual {v2, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 408
    iget-object v4, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v4, v2, v3, p4}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lu;Lx;Z)V

    goto/16 :goto_0

    .line 312
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method

.method private m()V
    .locals 3

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Lixj;

    .line 438
    invoke-virtual {v1, v2}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v1

    const v2, 0x7f0a05fd

    .line 439
    invoke-virtual {p0, v2}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Liwg;->d(Ljava/lang/String;)Liwg;

    move-result-object v1

    .line 440
    invoke-virtual {v1}, Liwg;->d()Liwg;

    move-result-object v1

    .line 441
    invoke-virtual {v1}, Liwg;->a()Liwg;

    move-result-object v1

    .line 437
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 443
    return-void
.end method

.method private n()V
    .locals 5

    .prologue
    .line 680
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    if-nez v0, :cond_0

    .line 736
    :goto_0
    return-void

    .line 684
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;->a(Lhee;)V

    .line 685
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->o()V

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    invoke-virtual {v0}, Lcsr;->a()V

    .line 689
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    const/4 v1, 0x1

    const v2, 0x7f020533

    const v3, 0x7f0a0a77

    invoke-virtual {v0, v1, v2, v3}, Lcsr;->a(III)V

    .line 694
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 695
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->f:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->f:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsx;

    .line 696
    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    .line 697
    invoke-virtual {v0}, Lcsx;->c()I

    move-result v3

    invoke-virtual {v0}, Lcsx;->a()I

    move-result v4

    invoke-virtual {v0}, Lcsx;->b()I

    move-result v0

    .line 696
    invoke-virtual {v2, v3, v4, v0}, Lcsr;->a(III)V

    .line 694
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 700
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 701
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    const/4 v1, 0x2

    const v2, 0x7f020530

    const v3, 0x7f0a0a78

    invoke-virtual {v0, v1, v2, v3}, Lcsr;->a(III)V

    .line 706
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    const/4 v1, 0x3

    const v2, 0x7f0204c6

    const v3, 0x7f0a0a79

    invoke-virtual {v0, v1, v2, v3}, Lcsr;->a(III)V

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    const/4 v1, 0x4

    const v2, 0x7f020538

    const v3, 0x7f0a0a7a

    invoke-virtual {v0, v1, v2, v3}, Lcsr;->a(III)V

    .line 716
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    const/4 v1, 0x5

    const v2, 0x7f020552

    const v3, 0x7f0a0a7b

    invoke-virtual {v0, v1, v2, v3}, Lcsr;->a(III)V

    .line 722
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    invoke-virtual {v0}, Lcsr;->b()V

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    const/16 v1, 0x8

    const v2, 0x7f020512

    const v3, 0x7f0a0a7c

    invoke-virtual {v0, v1, v2, v3}, Lcsr;->a(III)V

    .line 729
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    const/4 v1, 0x6

    const v2, 0x7f0204f9

    const v3, 0x7f0a0a7d

    invoke-virtual {v0, v1, v2, v3}, Lcsr;->a(III)V

    .line 734
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    invoke-virtual {v0}, Lcsr;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method

.method private o()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739
    .line 740
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 741
    new-array v1, v3, [Ljava/lang/String;

    const-string v4, "logged_in"

    aput-object v4, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 742
    new-instance v4, Lhed;

    invoke-direct {v4, v0}, Lhed;-><init>(Lhei;)V

    invoke-static {v1, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 743
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .line 744
    if-eqz v4, :cond_7

    .line 745
    if-ne v4, v3, :cond_4

    .line 746
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 747
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 748
    const-string v1, "is_plus_page"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "is_managed_account"

    .line 749
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "page_count"

    .line 750
    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_3

    :cond_1
    move v0, v3

    .line 754
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;->a(Z)V

    .line 755
    iget-object v4, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v1}, Livx;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;->setVisibility(I)V

    .line 757
    if-eqz v0, :cond_6

    .line 758
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p:Leus;

    invoke-virtual {v0}, Leus;->c()V

    .line 762
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v2

    .line 750
    goto :goto_0

    :cond_4
    move v0, v3

    .line 752
    goto :goto_0

    .line 755
    :cond_5
    const/16 v1, 0x8

    goto :goto_1

    .line 759
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->g()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 760
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(I)V

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_0
.end method

.method private p()V
    .locals 4

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 775
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v1, Lcsx;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 776
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 778
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsx;

    .line 779
    invoke-virtual {v0}, Lcsx;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Lcsx;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 780
    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcsx;->c()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 784
    :cond_1
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 790
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const-string v1, "destination"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 792
    if-eq v0, v2, :cond_0

    .line 793
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->i:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->j()Lx;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 796
    :cond_0
    return-void
.end method

.method private r()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 803
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 804
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 806
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    .line 807
    if-nez v1, :cond_0

    .line 808
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const-string v1, "destination"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 817
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const-string v1, "account_id"

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 818
    return-void

    .line 810
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 811
    const-string v1, "destination"

    .line 812
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 813
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private s()V
    .locals 4

    .prologue
    .line 824
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const-string v1, "destination"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 825
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->a(ILandroid/os/Bundle;Lx;Z)Lu;

    .line 827
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 866
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 868
    :goto_0
    instance-of v2, v0, Lhmq;

    if-eqz v2, :cond_0

    check-cast v0, Lhmq;

    .line 869
    invoke-interface {v0}, Lhmq;->F_()Lhmw;

    move-result-object v1

    :cond_0
    return-object v1

    .line 866
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    .line 867
    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->i()Lu;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 562
    :goto_0
    return-object v4

    .line 555
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 557
    :pswitch_0
    new-instance v0, Lhye;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    .line 558
    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    .line 557
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lctc;->a:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0

    .line 555
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 161
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 164
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    .line 165
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    move-result-object v0

    const-class v1, Lhjf;

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->l:Lhjf;

    .line 166
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsg;

    new-instance v2, Lcsg;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsg;-><init>(Lz;Llqr;)V

    .line 167
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcsm;

    new-instance v2, Lcsj;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lcsj;-><init>(Lz;Llqr;)V

    .line 168
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Leji;

    new-instance v2, Leji;

    iget-object v3, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->y:Llqc;

    new-instance v4, Lepz;

    invoke-direct {v4}, Lepz;-><init>()V

    invoke-direct {v2, p0, v3, v4}, Leji;-><init>(Landroid/app/Activity;Llqr;Lejk;)V

    .line 169
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 172
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 568
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 572
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 577
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 579
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->o()V

    .line 581
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 582
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 583
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;->a([B)V

    .line 587
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;->a(Lhee;)V

    goto :goto_0

    .line 577
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 102
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 841
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    sget-object v2, Lifb;->b:Lifb;

    invoke-direct {v1, v2}, Lifa;-><init>(Lifb;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 843
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 844
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 845
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 831
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->b(Z)V

    .line 832
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 833
    return-void
.end method

.method public a(ZIIII)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 449
    if-nez p1, :cond_0

    .line 482
    :goto_0
    return-void

    .line 453
    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f()V

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->h()V

    .line 457
    :cond_1
    sget-object v0, Lctb;->a:[I

    add-int/lit8 v1, p3, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 479
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->k()V

    .line 480
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n()V

    .line 481
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g()Lbb;

    move-result-object v0

    invoke-virtual {v0, v6}, Lbb;->b(I)Ldo;

    move-result-object v1

    if-nez v1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g()Lbb;

    move-result-object v0

    invoke-virtual {v0, v6, v7, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 459
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_navigation_bar"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e()V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p()V

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->r()V

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->s()V

    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0a05e5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f030003

    const-string v2, "photos_dogfood_dialog_version"

    invoke-static {p0, v0, v1, v2}, Llhr;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->d()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.libraries.social.notifications.notif_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-class v0, Lhms;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->eA:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->j:Z

    if-nez v0, :cond_2

    const-class v0, Lhms;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->eC:Lhmv;

    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto/16 :goto_1

    .line 462
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p()V

    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->r()V

    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->s()V

    goto/16 :goto_1

    .line 465
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    .line 466
    invoke-virtual {v1}, Liwg;->c()Liwg;

    move-result-object v1

    const-class v2, Liwl;

    new-instance v3, Liwm;

    invoke-direct {v3}, Liwm;-><init>()V

    const v4, 0x7f0a05fd

    .line 468
    invoke-virtual {p0, v4}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v3

    .line 469
    invoke-virtual {v3, v6}, Liwm;->a(Z)Liwm;

    move-result-object v3

    new-instance v4, Livs;

    invoke-direct {v4}, Livs;-><init>()V

    const-string v5, "logged_out"

    .line 471
    invoke-virtual {v4, v5}, Livs;->a(Ljava/lang/String;)Livs;

    move-result-object v4

    .line 470
    invoke-virtual {v3, v4}, Liwm;->a(Livq;)Liwm;

    move-result-object v3

    .line 473
    invoke-virtual {v3}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 467
    invoke-virtual {v1, v2, v3}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v1

    .line 465
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    goto/16 :goto_1

    .line 481
    :cond_7
    invoke-virtual {v0, v6, v7, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto/16 :goto_0

    .line 457
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->q:Lpg;

    invoke-virtual {v0, p1}, Lpg;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 850
    const/4 v0, 0x1

    .line 857
    :goto_0
    return v0

    .line 853
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 854
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->f()V

    .line 857
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 862
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 837
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->d()I

    move-result v2

    .line 876
    if-eqz p1, :cond_0

    sget-object v0, Lhmv;->c:Lhmv;

    move-object v1, v0

    .line 877
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 878
    new-instance v3, Lhmr;

    invoke-direct {v3, p0, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 880
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 878
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 882
    if-eqz p1, :cond_1

    .line 883
    new-instance v1, Lhmr;

    invoke-direct {v1, p0, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 885
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->F_()Lhmw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->O:Lhmw;

    .line 886
    invoke-virtual {v1, v2}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v1

    .line 883
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 895
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->l:Lhjf;

    invoke-interface {v0}, Lhjf;->b()V

    .line 896
    return-void

    .line 876
    :cond_0
    sget-object v0, Lhmv;->d:Lhmv;

    move-object v1, v0

    goto :goto_0

    .line 889
    :cond_1
    new-instance v1, Lhmr;

    invoke-direct {v1, p0, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmw;->O:Lhmw;

    .line 891
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 892
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->F_()Lhmw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v1

    .line 889
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_1
.end method

.method public l()V
    .locals 0

    .prologue
    .line 617
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p()V

    .line 618
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n()V

    .line 619
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    if-ne p1, v0, :cond_1

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->g()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(I)V

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 642
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    .line 643
    invoke-virtual {v1}, Liwg;->c()Liwg;

    move-result-object v1

    const-class v2, Lixj;

    .line 644
    invoke-virtual {v1, v2}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v1

    const-class v2, Liwl;

    new-instance v3, Liwm;

    invoke-direct {v3}, Liwm;-><init>()V

    const v4, 0x7f0a05f4

    .line 646
    invoke-virtual {p0, v4}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v3

    .line 647
    invoke-virtual {v3}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 645
    invoke-virtual {v1, v2, v3}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v1

    .line 642
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 653
    :cond_1
    :goto_0
    return-void

    .line 650
    :cond_2
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->a(I)Lu;

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 611
    invoke-super {p0, p1}, Lloa;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->q:Lpg;

    invoke-virtual {v0}, Lpg;->b()V

    .line 613
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongViewCast"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 177
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 179
    if-eqz p1, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->j:Z

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v3, Lkzl;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    invoke-interface {v0, v1}, Lkzl;->b(Z)V

    .line 182
    invoke-static {v2}, Lgci;->a(Z)V

    .line 184
    const v0, 0x7f040190

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->setContentView(I)V

    .line 186
    const v0, 0x7f1002fa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/views/PhotosHostLayout;

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Ldhq;)V

    .line 189
    if-eqz p1, :cond_0

    .line 190
    const-string v0, "destination"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    .line 196
    :cond_0
    if-nez p1, :cond_1

    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m()V

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->b()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->k:Landroid/widget/ListView;

    .line 201
    new-instance v0, Lcsr;

    invoke-direct {v0, p0}, Lcsr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->k:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->k:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->c()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->o:Landroid/widget/ListView;

    .line 206
    new-instance v0, Leus;

    invoke-direct {v0, p0}, Leus;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p:Leus;

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p:Leus;

    invoke-virtual {v0, v1}, Leus;->a(Z)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p:Leus;

    invoke-virtual {v0, v1}, Leus;->b(Z)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p:Leus;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->o:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a()Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m:Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/photos/views/SelectedAccountNavigationView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    new-instance v1, Lpg;

    const v0, 0x7f1002fb

    .line 216
    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lms;

    const v2, 0x7f0a0ae3

    const v3, 0x7f0a0ae4

    invoke-direct {v1, p0, v0, v2, v3}, Lpg;-><init>(Landroid/app/Activity;Lms;II)V

    iput-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->q:Lpg;

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->q:Lpg;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(Lmy;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v1, Lieh;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 223
    invoke-interface {v0, p0}, Lieh;->a(Lieg;)V

    .line 225
    if-eqz p1, :cond_3

    .line 226
    const-string v0, "navigation_bar_visible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->e()V

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    const-string v1, "navigation_bar_mode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->a(I)V

    .line 232
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 179
    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v1, Lieh;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 602
    invoke-interface {v0, p0}, Lieh;->b(Lieg;)V

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v1, Lkdv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->p()V

    .line 604
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->x:Llnh;

    const-class v1, Lkzl;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lkzl;->b(Z)V

    .line 605
    const/4 v0, 0x1

    invoke-static {v0}, Lgci;->a(Z)V

    .line 606
    invoke-super {p0}, Lloa;->onDestroy()V

    .line 607
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->k:Landroid/widget/ListView;

    if-ne p1, v0, :cond_2

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    invoke-virtual {v0, p3}, Lcsr;->a(I)I

    move-result v0

    .line 625
    if-ne v0, v1, :cond_1

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->a(I)Lu;

    goto :goto_0

    .line 630
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->o:Landroid/widget/ListView;

    if-ne p1, v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p:Leus;

    invoke-virtual {v0, p3}, Leus;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    invoke-virtual {v1}, Liwg;->c()Liwg;

    move-result-object v1

    const-class v2, Lixj;

    invoke-virtual {v1, v2}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v1

    const-class v2, Lixk;

    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p:Leus;

    invoke-virtual {v0, p3}, Leus;->b(I)I

    move-result v0

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->o()V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    invoke-virtual {v2}, Liwg;->c()Liwg;

    move-result-object v2

    const-class v3, Lixj;

    invoke-virtual {v2, v3}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v2

    invoke-virtual {v2, v0}, Liwg;->a(I)Liwg;

    move-result-object v0

    invoke-virtual {v1, v0}, Livx;->a(Liwg;)V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 431
    invoke-super {p0, p1}, Lloa;->onNewIntent(Landroid/content/Intent;)V

    .line 432
    invoke-virtual {p0, p1}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 433
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->m()V

    .line 434
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 595
    invoke-super {p0, p1}, Lloa;->onPostCreate(Landroid/os/Bundle;)V

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->q:Lpg;

    invoke-virtual {v0}, Lpg;->a()V

    .line 597
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 236
    invoke-super {p0}, Lloa;->onResume()V

    .line 237
    invoke-static {p0}, Lfuy;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    invoke-static {p0}, Lfuy;->e(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 240
    invoke-virtual {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->finish()V

    .line 257
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    invoke-static {}, Lfvc;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    .line 247
    invoke-virtual {v0}, Lcsr;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->e:Livx;

    .line 248
    invoke-virtual {v0}, Livx;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n:Lcsr;

    .line 253
    invoke-virtual {v1}, Lcsr;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 252
    invoke-direct {p0, v0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->a(I)Lu;

    .line 255
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->p()V

    .line 256
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->n()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 523
    invoke-super {p0, p1}, Lloa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 525
    const-string v0, "destination"

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->h:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 527
    invoke-direct {p0}, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->q()V

    .line 531
    const-string v0, "navigation_bar_visible"

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->d()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 532
    const-string v0, "navigation_bar_mode"

    iget-object v1, p0, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;->g:Lcom/google/android/apps/photos/views/PhotosHostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/photos/views/PhotosHostLayout;->g()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 533
    return-void
.end method
