.class public Lcom/google/android/apps/plus/phone/EditAudienceActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Ldzv;
.implements Lhmq;


# instance fields
.field private e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

.field private f:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lloa;-><init>()V

    .line 36
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 38
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->x:Llnh;

    .line 39
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 42
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->x:Llnh;

    .line 43
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 42
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lhmw;->aa:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 147
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 149
    return-void
.end method

.method public a(Lu;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v0, :cond_0

    .line 63
    check-cast p1, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Ldzv;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->b(Z)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "circle_usage_type"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 66
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "search_plus_pages_enabled"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 68
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Z)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "filter_null_gaia_ids"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 70
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c(Z)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "audience_is_read_only"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 72
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->j(Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "empty_selection_allowed"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 74
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->i(Z)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "restrict_to_domain"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 76
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->k(Z)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "hide_domain_restrict_toggle"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 78
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->l(Z)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "enable_domain_restrict_toggle"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->m(Z)V

    .line 83
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method public c_(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->h()Loo;

    move-result-object v0

    invoke-virtual {v0, p1}, Loo;->b(Ljava/lang/CharSequence;)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 136
    return-void
.end method

.method public l()V
    .locals 0

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->az_()V

    .line 143
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 50
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v0, 0x7f040085

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->setContentView(I)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->h()Loo;

    move-result-object v1

    .line 55
    invoke-virtual {v1, v0}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {v1, v2}, Loo;->c(Z)V

    .line 57
    invoke-static {v1, v2}, Lley;->a(Loo;Z)V

    .line 58
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 118
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 119
    const v1, 0x7f100678

    if-ne v0, v1, :cond_0

    .line 120
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 121
    const-string v1, "extra_acl"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->V()Lhgw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 122
    const-string v1, "restrict_to_domain"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 123
    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->X()Z

    move-result v2

    .line 122
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 124
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->setResult(ILandroid/content/Intent;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->finish()V

    .line 126
    const/4 v0, 0x1

    .line 128
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lloa;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 111
    const v0, 0x7f100678

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->f:Landroid/view/MenuItem;

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->f:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->e()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 113
    invoke-interface {p1}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Lloa;->onResume()V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->U()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_acl"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 96
    if-eqz v0, :cond_0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->e:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lhgw;)V

    goto :goto_0
.end method
