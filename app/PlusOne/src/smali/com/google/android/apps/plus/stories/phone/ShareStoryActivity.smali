.class public Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lbc;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llon;",
        "Lbc",
        "<",
        "Lfob;",
        ">;",
        "Llgs;"
    }
.end annotation


# instance fields
.field private g:Lhov;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Landroid/content/Intent;

.field private k:Z

.field private l:Lhox;

.field private final m:Ljava/lang/Runnable;

.field private final n:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Llon;-><init>()V

    .line 46
    new-instance v0, Lhov;

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->l()Llqr;

    move-result-object v1

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->g:Lhov;

    .line 53
    new-instance v0, Lfqn;

    invoke-direct {v0, p0}, Lfqn;-><init>(Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->m:Ljava/lang/Runnable;

    .line 59
    new-instance v0, Lfqo;

    invoke-direct {v0, p0}, Lfqo;-><init>(Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->n:Ljava/lang/Runnable;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->h()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;)V
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->f()Lae;

    move-result-object v0

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lt;->a()V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->f()Lae;

    move-result-object v0

    const-string v1, "progress_dialog"

    .line 191
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 192
    if-eqz v0, :cond_0

    .line 198
    :goto_0
    return-void

    .line 195
    :cond_0
    const/4 v0, 0x0

    const v1, 0x7f0a01bc

    .line 196
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 195
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->f()Lae;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lfob;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 108
    new-instance v0, Lfqd;

    iget v2, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->h:I

    iget-object v3, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->i:Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move v7, v6

    move-object v8, v4

    invoke-direct/range {v0 .. v8}, Lfqd;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V

    .line 109
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfqd;->a(Z)V

    .line 110
    return-object v0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 224
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 68
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->e:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 70
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->j:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 211
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lfob;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 181
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p2, Lfob;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->a(Lfob;)V

    return-void
.end method

.method public a(Lfob;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lfob;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 115
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->k:Z

    if-eqz v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->g:Lhov;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->l:Lhox;

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->g:Lhov;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->l:Lhox;

    invoke-virtual {v0, v1}, Lhov;->a(Lhox;)V

    .line 124
    :cond_1
    if-nez p1, :cond_3

    move-object v9, v5

    .line 126
    :goto_1
    if-eqz v9, :cond_2

    iget-object v0, v9, Lmmq;->g:Lmlz;

    if-eqz v0, :cond_2

    iget-object v0, v9, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->c:Loya;

    if-nez v0, :cond_4

    iget-object v0, v9, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    if-nez v0, :cond_4

    .line 128
    :cond_2
    const v0, 0x7f0a01bf

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 132
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->setResult(I)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->finish()V

    goto :goto_0

    .line 124
    :cond_3
    iget-object v0, p1, Lfob;->a:Lmmq;

    move-object v9, v0

    goto :goto_1

    .line 137
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->e:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->h:I

    .line 138
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 139
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    iget-object v1, v9, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    if-eqz v1, :cond_8

    .line 143
    iget-object v1, v9, Lmmq;->b:Lodo;

    if-eqz v1, :cond_5

    iget-object v1, v9, Lmmq;->b:Lodo;

    iget-object v1, v1, Lodo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 148
    :cond_5
    iget v1, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->h:I

    iget-object v0, v9, Lmmq;->g:Lmlz;

    iget-object v2, v0, Lmlz;->a:Ljava/lang/String;

    iget-object v0, v9, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    iget-object v0, v0, Logr;->o:Ljava/lang/Boolean;

    .line 150
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_6

    move v3, v7

    :goto_2
    iget-object v0, v9, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    iget-object v0, v0, Logr;->x:Ljava/lang/Boolean;

    .line 151
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    move-object v0, p0

    move-object v6, v5

    .line 148
    invoke-static/range {v0 .. v6}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;ZZLhgw;[B)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->j:Landroid/content/Intent;

    .line 154
    iget-object v0, v9, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    iget-object v0, v0, Logr;->o:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->g:Lhov;

    new-instance v1, Lfqp;

    invoke-direct {v1, p0}, Lfqp;-><init>(Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;)V

    invoke-virtual {v0, v1}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    .line 176
    :goto_3
    iput-boolean v7, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->k:Z

    goto/16 :goto_0

    :cond_6
    move v3, v8

    .line 150
    goto :goto_2

    .line 167
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->j:Landroid/content/Intent;

    invoke-virtual {p0, v0, v8}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_3

    .line 172
    :cond_8
    iget v1, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->h:I

    invoke-static {p0, v1, v5}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v2

    const-string v0, "story_id"

    iget-object v3, v9, Lmmq;->a:Lmok;

    iget-object v3, v3, Lmok;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v9, Lmmq;->b:Lodo;

    if-eqz v1, :cond_9

    iget-object v1, v9, Lmmq;->b:Lodo;

    iget-object v1, v1, Lodo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "link_url"

    iget-object v1, v9, Lmmq;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_9
    iget-object v0, v9, Lmmq;->g:Lmlz;

    if-eqz v0, :cond_a

    iget-object v0, v9, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->c:Loya;

    if-eqz v0, :cond_a

    const-string v0, "story_embed"

    new-instance v1, Lhyt;

    iget-object v3, v9, Lmmq;->g:Lmlz;

    iget-object v3, v3, Lmlz;->c:Loya;

    invoke-direct {v1, v3}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_a
    iget-object v0, v9, Lmmq;->g:Lmlz;

    if-eqz v0, :cond_b

    iget-object v0, v9, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    if-eqz v0, :cond_b

    const-string v0, "activity_is_public"

    iget-object v1, v9, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    iget-object v1, v1, Logr;->o:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "story_reshare"

    invoke-virtual {v2, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "story_activity_id"

    iget-object v1, v9, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "story_activity_domain_restrict"

    iget-object v1, v9, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    iget-object v1, v1, Logr;->x:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 173
    :cond_b
    invoke-virtual {p0, v2, v8}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_3
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->finish()V

    .line 220
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 185
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->setResult(ILandroid/content/Intent;)V

    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->finish()V

    .line 187
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 74
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->h:I

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "story_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->i:Ljava/lang/String;

    .line 77
    if-eqz p1, :cond_0

    .line 78
    const-string v0, "share_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->j:Landroid/content/Intent;

    .line 79
    const-string v0, "share_intent_started"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->k:Z

    .line 81
    :cond_0
    if-eqz p1, :cond_2

    const-string v0, "show_dialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    invoke-direct {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->h()V

    .line 87
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->k:Z

    if-nez v0, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->g()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 90
    :cond_1
    return-void

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->g:Lhov;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lhov;->a(Ljava/lang/Runnable;J)Lhox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->l:Lhox;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1}, Llon;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 97
    const-string v0, "show_dialog"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 98
    const-string v0, "share_intent_started"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->j:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "share_intent"

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/phone/ShareStoryActivity;->j:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 102
    :cond_0
    return-void
.end method
