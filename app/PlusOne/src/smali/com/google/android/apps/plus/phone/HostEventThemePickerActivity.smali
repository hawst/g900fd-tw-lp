.class public Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Ledy;
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Ldie;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 38
    invoke-direct {p0}, Lloa;-><init>()V

    .line 44
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 46
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->x:Llnh;

    .line 47
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 49
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->x:Llnh;

    .line 50
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 51
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 53
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 54
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 56
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->x:Llnh;

    .line 57
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 60
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->e:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lhmw;->E:Lhmw;

    return-object v0
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 76
    const-string v1, "theme_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 77
    const-string v1, "theme_url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->finish()V

    .line 80
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 118
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 104
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 105
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 106
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 107
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 96
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0, p1}, Lloa;->a(Lu;)V

    .line 68
    instance-of v0, p1, Ledt;

    if-eqz v0, :cond_0

    .line 69
    check-cast p1, Ledt;

    invoke-virtual {p1, p0}, Ledt;->a(Ledy;)V

    .line 71
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 86
    if-nez p1, :cond_0

    .line 87
    new-instance v0, Ledt;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ledt;-><init>(I)V

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->e:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 90
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->setContentView(I)V

    .line 91
    return-void
.end method
