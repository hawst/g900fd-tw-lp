.class public Lcom/google/android/apps/plus/phone/OneProfileActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# static fields
.field public static final e:Lloy;


# instance fields
.field private final f:Ldie;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Llrt;

    const-string v1, "debug.plus.gcore_add2circles"

    invoke-direct {v0, v1}, Llrt;-><init>(Ljava/lang/String;)V

    .line 65
    invoke-virtual {v0}, Llrr;->a()Llrr;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Llrr;->b()Llrr;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Llrr;->c()Lloy;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->e:Lloy;

    .line 63
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 54
    invoke-direct {p0}, Lloa;-><init>()V

    .line 70
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 72
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->x:Llnh;

    .line 73
    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 75
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->x:Llnh;

    .line 76
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 77
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 79
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 80
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 82
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->x:Llnh;

    .line 83
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 85
    new-instance v0, Ldxn;

    new-instance v1, Lfax;

    invoke-direct {v1, p0}, Lfax;-><init>(Lcom/google/android/apps/plus/phone/OneProfileActivity;)V

    invoke-direct {v0, v1}, Ldxn;-><init>(Ldxm;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->x:Llnh;

    .line 101
    invoke-virtual {v0, v1}, Ldxn;->a(Llnh;)V

    .line 104
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lhmw;->j:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 127
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 130
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lctz;

    new-instance v2, Lctz;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lctz;-><init>(Landroid/app/Activity;Llqr;)V

    .line 131
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lctq;

    new-instance v2, Lctq;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Lctq;-><init>(Landroid/app/Activity;Llqr;)V

    .line 132
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 135
    sget-object v0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->e:Lloy;

    .line 136
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 193
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 194
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 195
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 196
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 184
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 185
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OneProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "person_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    invoke-static {v0}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    const-string v1, "extra_gaia_id"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->e:Lloy;

    .line 173
    invoke-super {p0, p1, p2, p3}, Lloa;->onActivityResult(IILandroid/content/Intent;)V

    .line 179
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 147
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 149
    if-nez p1, :cond_0

    .line 150
    new-instance v0, Leet;

    invoke-direct {v0}, Leet;-><init>()V

    .line 151
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->f:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 153
    :cond_0
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OneProfileActivity;->setContentView(I)V

    .line 155
    return-void
.end method
