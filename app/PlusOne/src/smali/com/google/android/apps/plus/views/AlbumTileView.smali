.class public Lcom/google/android/apps/plus/views/AlbumTileView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# static fields
.field private static a:Landroid/graphics/Bitmap;

.field private static b:Landroid/graphics/Bitmap;

.field private static c:Landroid/graphics/Bitmap;

.field private static d:Landroid/graphics/Bitmap;

.field private static e:Landroid/graphics/Bitmap;

.field private static f:Landroid/graphics/Bitmap;


# instance fields
.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private j:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AlbumTileView;->a(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AlbumTileView;->a(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AlbumTileView;->a(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumTileView;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 59
    const v1, 0x7f02053f

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumTileView;->a:Landroid/graphics/Bitmap;

    .line 60
    const v1, 0x7f0204e1

    .line 61
    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumTileView;->b:Landroid/graphics/Bitmap;

    .line 62
    const v1, 0x7f02051c

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumTileView;->c:Landroid/graphics/Bitmap;

    .line 63
    const v1, 0x7f020507

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumTileView;->e:Landroid/graphics/Bitmap;

    .line 64
    const v1, 0x7f0204ff

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumTileView;->d:Landroid/graphics/Bitmap;

    .line 65
    const v1, 0x7f0204dd

    .line 66
    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/AlbumTileView;->f:Landroid/graphics/Bitmap;

    .line 68
    :cond_0
    return-void
.end method


# virtual methods
.method public a(IZ)V
    .locals 2

    .prologue
    .line 91
    if-eqz p2, :cond_0

    .line 92
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumTileView;->e:Landroid/graphics/Bitmap;

    .line 119
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 120
    return-void

    .line 94
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 116
    const/4 v0, 0x0

    goto :goto_0

    .line 96
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumTileView;->c:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 100
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumTileView;->f:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 104
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumTileView;->b:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 108
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumTileView;->a:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 112
    :pswitch_4
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumTileView;->d:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 94
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lizu;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 124
    return-void
.end method

.method public a(Ljava/lang/Integer;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 75
    if-nez p1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->h:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumTileView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11003b

    .line 80
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    .line 79
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 129
    const v0, 0x7f10013b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->g:Landroid/widget/TextView;

    .line 130
    const v0, 0x7f100194

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->h:Landroid/widget/TextView;

    .line 131
    const v0, 0x7f10002e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->i:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 132
    const v0, 0x7f100193

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumTileView;->j:Landroid/widget/ImageView;

    .line 133
    return-void
.end method
