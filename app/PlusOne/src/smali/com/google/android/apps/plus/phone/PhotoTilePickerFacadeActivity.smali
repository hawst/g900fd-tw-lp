.class public Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lhob;


# instance fields
.field private final g:Lhee;

.field private final h:Lhoc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Llon;-><init>()V

    .line 34
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->e:Llnh;

    .line 35
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->g:Lhee;

    .line 37
    new-instance v0, Lhoc;

    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->l()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Landroid/app/Activity;Llqr;)V

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->h:Lhoc;

    .line 40
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 75
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->e:Llnh;

    const-class v1, Lhoc;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->h:Lhoc;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 77
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 98
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->setResult(ILandroid/content/Intent;)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->finish()V

    .line 100
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, -0x1

    .line 104
    packed-switch p1, :pswitch_data_0

    .line 110
    invoke-super {p0, p1, p2, p3}, Llon;->onActivityResult(IILandroid/content/Intent;)V

    .line 113
    :goto_0
    return-void

    .line 106
    :pswitch_0
    const/4 v1, 0x0

    const/4 v5, 0x0

    if-eq p2, v4, :cond_0

    :goto_1
    invoke-virtual {p0, p2, v5}, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->finish()V

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v0, "photo_url"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "account_id"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->e:Llnh;

    const-class v5, Lhei;

    invoke-virtual {v0, v5}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, v4}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v5, "gaia_id"

    invoke-interface {v0, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v6, "photo_url"

    invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account_gaia_id"

    invoke-virtual {v5, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "media_type"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v3, "media_type"

    invoke-virtual {v5, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "photo_id"

    invoke-virtual {v2, v0, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    const-string v0, "picasa_photo_id"

    invoke-virtual {v5, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move p2, v1

    goto :goto_1

    :cond_1
    const-string v0, "tile_id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->h:Lhoc;

    new-instance v0, Lfbp;

    const-string v2, "photo_id"

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfbp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    goto :goto_0

    :cond_2
    move p2, v1

    goto :goto_1

    .line 104
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 81
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 83
    if-nez p1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->g:Lhee;

    .line 85
    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    new-array v0, v3, [Ljava/lang/String;

    .line 86
    invoke-static {v4, v0}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object v0, p0

    move v5, v4

    move v7, v4

    move v8, v3

    move v9, v3

    .line 84
    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IZILjava/lang/Integer;ZII)Landroid/content/Intent;

    move-result-object v0

    .line 90
    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/phone/PhotoTilePickerFacadeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 92
    :cond_0
    return-void
.end method
