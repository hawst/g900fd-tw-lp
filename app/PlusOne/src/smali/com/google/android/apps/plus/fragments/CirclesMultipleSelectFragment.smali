.class public Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Ldzb;
.implements Lhob;
.implements Llgs;
.implements Llio;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ldzb;",
        "Lhob;",
        "Llgs;",
        "Llio;"
    }
.end annotation


# instance fields
.field private final N:Licq;

.field private O:Ljava/lang/String;

.field private P:Z

.field private Q:Landroid/widget/ListView;

.field private R:Ldzi;

.field private S:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private T:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private U:Landroid/view/ContextThemeWrapper;

.field private V:I

.field private W:Z

.field private X:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Ldzj;

.field private Z:Lhee;

.field private aa:Lhoc;

.field private ab:Ldvv;

.field private ac:Ljpr;

.field private ad:Ljpb;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Llol;-><init>()V

    .line 86
    new-instance v0, Licq;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->N:Licq;

    .line 117
    return-void
.end method

.method private U()V
    .locals 1

    .prologue
    .line 600
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    invoke-static {}, Ljpe;->b()V

    .line 603
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->d()V

    .line 604
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;)Lhee;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;Ljob;J)V
    .locals 4

    .prologue
    .line 61
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljob;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    invoke-interface {p1}, Ljob;->b()V

    new-instance v0, Lkoe;

    const/16 v1, 0x57

    invoke-direct {v0, v1, p2, p3}, Lkoe;-><init>(IJ)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    new-instance v0, Lkoe;

    const/16 v1, 0x58

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 361
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->S:Ljava/util/ArrayList;

    .line 362
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 363
    const/4 v0, 0x0

    .line 364
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 365
    const/16 v1, 0x7c

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 366
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 367
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 369
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->S:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    add-int/lit8 v0, v1, 0x1

    .line 371
    goto :goto_0

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 377
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->S:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    .line 380
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Y:Ldzj;

    if-eqz v0, :cond_3

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Y:Ldzj;

    invoke-interface {v0}, Ldzj;->ap_()V

    .line 383
    :cond_3
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 592
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    invoke-static {}, Ljpe;->b()V

    .line 595
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->d()V

    .line 596
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 597
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->U:Landroid/view/ContextThemeWrapper;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 218
    const v0, 0x7f040069

    invoke-virtual {v1, v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 220
    const v0, 0x102000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    .line 222
    const v0, 0x7f040067

    invoke-virtual {v1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 224
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 227
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->W:Z

    if-eqz v0, :cond_0

    .line 228
    const v0, 0x7f040068

    invoke-virtual {v1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 230
    const v1, 0x7f0a0870

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 235
    return-object v2
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    :goto_0
    return-object v6

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 245
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 247
    :pswitch_0
    new-instance v0, Lhye;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    .line 248
    invoke-static {v3, v2}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "name"

    aput-object v4, v3, v8

    const-string v4, "packed_circle_ids"

    aput-object v4, v3, v5

    const-string v4, "person_id=?"

    new-array v5, v5, [Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->O:Ljava/lang/String;

    aput-object v7, v5, v8

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v0

    goto :goto_0

    .line 256
    :pswitch_1
    new-instance v6, Lhxg;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->V:I

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v3, v8

    const-string v4, "circle_id"

    aput-object v4, v3, v5

    const-string v4, "circle_name"

    aput-object v4, v3, v7

    const/4 v4, 0x3

    const-string v5, "contact_count"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "type"

    aput-object v5, v3, v4

    invoke-direct {v6, v0, v2, v1, v3}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->S:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 543
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 547
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 162
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 163
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0901c9

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->U:Landroid/view/ContextThemeWrapper;

    .line 164
    new-instance v0, Ldzi;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->U:Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p0, v1}, Ldzi;-><init>(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    .line 165
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->aa:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 184
    if-nez p1, :cond_2

    .line 186
    new-instance v0, Lkoe;

    const/16 v1, 0x6b

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 193
    :goto_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->V:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 196
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->ad:Ljpb;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    .line 201
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 200
    invoke-interface {v0, v1, v2}, Ljpb;->d(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->ad:Ljpb;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "first_add"

    invoke-interface {v0, p0, v1, v2, v3}, Ljpb;->a(Lu;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 205
    :cond_1
    return-void

    .line 189
    :cond_2
    const-string v0, "new_circles"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    .line 190
    const-string v0, "existing_circle_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->X:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 531
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 355
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 270
    if-nez p2, :cond_1

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    const v2, 0x7f0a0592

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 272
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 278
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 292
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->O:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->P:Z

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    invoke-virtual {v0}, Ldzi;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    invoke-virtual {v0}, Ldzi;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->N:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 280
    :pswitch_0
    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->P:Z

    .line 281
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 282
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->b(Ljava/lang/String;)V

    .line 286
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    invoke-virtual {v0}, Ldzi;->notifyDataSetChanged()V

    goto :goto_1

    .line 284
    :cond_3
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 289
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    invoke-virtual {v0, p2}, Ldzi;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->X:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->X:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    if-eqz v0, :cond_6

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    if-gez v0, :cond_5

    move v0, v1

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    invoke-virtual {v3, v0, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_6
    :goto_3
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->X:Ljava/util/ArrayList;

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    invoke-static {v0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    invoke-virtual {v0}, Ldzi;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    new-instance v0, Ldzh;

    invoke-direct {v0, p0, v4, v5}, Ldzh;-><init>(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;J)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->ac:Ljpr;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    sget-object v5, Ljof;->a:Ljqc;

    invoke-interface {v3, v0, v4, v5}, Ljpr;->a(Ljoy;ILjqc;)V

    :cond_8
    iget v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->V:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    invoke-static {v0, v2}, Ldhv;->p(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->S:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    if-nez v0, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->S:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    :cond_9
    :goto_4
    new-instance v0, Lkoe;

    const/16 v2, 0x6c

    invoke-direct {v0, v2}, Lkoe;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    invoke-virtual {v0, v2}, Lkoe;->a(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_a
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->O:Ljava/lang/String;

    if-nez v0, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->S:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    if-nez v0, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    goto :goto_4

    .line 292
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->N:Licq;

    sget-object v2, Lict;->b:Lict;

    invoke-virtual {v0, v2}, Licq;->a(Lict;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Q:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto/16 :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 61
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ldzj;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Y:Ldzj;

    .line 158
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->O:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 552
    const-string v0, "AddCircleTaskLegacy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->e()V

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 553
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->U()V

    goto :goto_0

    .line 554
    :cond_2
    const-string v0, "AddCircleTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 555
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 556
    invoke-static {}, Ljpe;->b()V

    .line 558
    :cond_3
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->e()V

    goto :goto_0

    :cond_4
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Ljpe;->b()V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->ab:Ldvv;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ldvv;->a(I)Lhny;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->aa:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    goto :goto_0

    .line 559
    :cond_6
    const-string v0, "LoadCirclesTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 561
    invoke-static {}, Ljpe;->b()V

    .line 563
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->U()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 474
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 527
    :goto_0
    return-void

    .line 478
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 480
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    if-eqz v2, :cond_4

    .line 482
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    invoke-virtual {v2}, Ldzi;->a()Landroid/database/Cursor;

    move-result-object v2

    .line 483
    if-eqz v2, :cond_3

    .line 484
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 486
    :cond_1
    const/4 v4, 0x2

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 487
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v0

    .line 494
    :goto_1
    if-eqz v2, :cond_4

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    const v2, 0x7f0a08a0

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 496
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 491
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_3
    move v2, v1

    goto :goto_1

    .line 501
    :cond_4
    const v2, 0x7f0a0837

    .line 502
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 501
    invoke-static {v6, v2, v1}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v2

    .line 504
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->p()Lae;

    move-result-object v4

    const-string v5, "req_pending"

    invoke-virtual {v2, v4, v5}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 506
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->X:Ljava/util/ArrayList;

    .line 507
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    if-eqz v2, :cond_6

    .line 508
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->R:Ldzi;

    invoke-virtual {v2}, Ldzi;->a()Landroid/database/Cursor;

    move-result-object v2

    .line 509
    if-eqz v2, :cond_6

    .line 510
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 512
    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->X:Ljava/util/ArrayList;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 513
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 518
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 519
    if-nez p3, :cond_8

    .line 521
    :goto_2
    invoke-static {}, Ljpe;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 522
    invoke-static {}, Ljpe;->b()V

    .line 524
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->ab:Ldvv;

    invoke-virtual {v1, v2, v3, v6, v0}, Ldvv;->a(ILjava/lang/String;Ljava/lang/String;Z)Lhny;

    move-result-object v0

    .line 526
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->aa:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 519
    goto :goto_2
.end method

.method public a(Llin;Z)V
    .locals 2

    .prologue
    .line 436
    check-cast p1, Lhxf;

    invoke-virtual {p1}, Lhxf;->a()Ljava/lang/String;

    move-result-object v0

    .line 437
    if-eqz p2, :cond_2

    .line 438
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 439
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Y:Ldzj;

    if-eqz v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Y:Ldzj;

    invoke-interface {v0}, Ldzj;->ap_()V

    .line 448
    :cond_1
    return-void

    .line 442
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 153
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->W:Z

    .line 154
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 535
    return-void
.end method

.method public c()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 149
    iput p1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->V:I

    .line 150
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->aa:Lhoc;

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->au:Llnh;

    const-class v1, Ldvv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvv;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->ab:Ldvv;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->Z:Lhee;

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->au:Llnh;

    const-class v1, Ljpr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpr;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->ac:Ljpr;

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->au:Llnh;

    const-class v1, Ljpb;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpb;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->ad:Ljpb;

    .line 176
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 539
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 607
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 609
    if-eqz v0, :cond_0

    .line 610
    invoke-virtual {v0}, Lt;->a()V

    .line 612
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 209
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 210
    const-string v0, "new_circles"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 211
    const-string v0, "existing_circle_ids"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->X:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 212
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 461
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->W:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->at:Llnl;

    .line 463
    invoke-static {}, Ldyy;->U()Ldyy;

    move-result-object v0

    .line 464
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ldyy;->a(Lu;I)V

    .line 465
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->p()Lae;

    move-result-object v1

    const-string v2, "new_circle_input"

    invoke-virtual {v0, v1, v2}, Ldyy;->a(Lae;Ljava/lang/String;)V

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 466
    :cond_1
    if-eqz p3, :cond_0

    .line 467
    check-cast p2, Lhxf;

    invoke-virtual {p2}, Lhxf;->toggle()V

    goto :goto_0
.end method
