.class public Lcom/google/android/apps/plus/phone/EsApplication;
.super Landroid/app/Application;
.source "PG"

# interfaces
.implements Liev;
.implements Llnk;


# static fields
.field private static a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private static b:J


# instance fields
.field private final c:Llnm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/plus/phone/EsApplication;->b:J

    .line 87
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 89
    new-instance v0, Llnm;

    new-instance v1, Lewv;

    invoke-direct {v1}, Lewv;-><init>()V

    invoke-direct {v0, v1}, Llnm;-><init>(Llnn;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsApplication;->c:Llnm;

    .line 294
    return-void
.end method

.method public static synthetic d()Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/apps/plus/phone/EsApplication;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 110
    invoke-static {p0}, La;->a(Landroid/content/Context;)V

    .line 111
    return-void
.end method

.method public a(Llox;)V
    .locals 12

    .prologue
    const-wide/16 v8, 0x400

    .line 377
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    .line 378
    invoke-virtual {v1}, Ljava/lang/Runtime;->gc()V

    .line 380
    const-string v0, "STATUS DUMP"

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 382
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 383
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x14

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Build: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :goto_0
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 389
    new-instance v2, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v2}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 390
    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 392
    invoke-static {p0}, Llsg;->a(Landroid/content/Context;)I

    move-result v0

    .line 393
    invoke-virtual {v1}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    div-long/2addr v4, v8

    .line 394
    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    div-long/2addr v6, v8

    iget-wide v8, v2, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    const-wide/32 v10, 0x100000

    div-long/2addr v8, v10

    iget-boolean v1, v2, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0xaa

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Memory class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " MB; total heap: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " KB; free heap: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " KB; available system memory: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " MB; low memory: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 392
    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 398
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->a(Llox;)V

    .line 399
    const-class v0, Lkdv;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0, p1}, Lkdv;->a(Llox;)V

    .line 400
    const-string v0, "=== END DUMP"

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 401
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsApplication;->a()V

    .line 107
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 371
    new-instance v0, Llox;

    invoke-direct {v0}, Llox;-><init>()V

    .line 372
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->a(Llox;)V

    .line 373
    const/4 v1, 0x4

    const-string v2, "EsApplication"

    invoke-virtual {v0}, Llox;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 374
    return-void
.end method

.method public h_()Llnh;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsApplication;->c:Llnm;

    invoke-virtual {v0, p0}, Llnm;->a(Landroid/content/Context;)Llnh;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 119
    const-class v0, Lhnt;

    .line 120
    invoke-static {v2, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnt;

    .line 121
    if-eqz v0, :cond_0

    .line 122
    sget-wide v4, Lcom/google/android/apps/plus/phone/EsApplication;->b:J

    invoke-interface {v0, v4, v5}, Lhnt;->a(J)V

    .line 126
    :cond_0
    const-class v1, Lkly;

    invoke-static {v2, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkly;

    new-instance v3, Leww;

    invoke-direct {v3}, Leww;-><init>()V

    .line 127
    invoke-virtual {v1, v3}, Lkly;->a(Lkmf;)V

    .line 134
    invoke-static {p0}, Llpd;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 292
    :cond_1
    :goto_0
    return-void

    .line 138
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/phone/EsApplication;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 139
    new-instance v1, Llpd;

    new-instance v3, Lexa;

    invoke-direct {v3, p0}, Lexa;-><init>(Lcom/google/android/apps/plus/phone/EsApplication;)V

    invoke-direct {v1, p0, v3}, Llpd;-><init>(Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 142
    invoke-static {p0}, Ljbd;->a(Landroid/content/Context;)V

    .line 144
    invoke-static {v2}, Lcom/google/android/libraries/social/jni/crashreporter/NativeCrashHandler;->a(Landroid/content/Context;)V

    .line 147
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-ne v1, v3, :cond_3

    .line 148
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 149
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 150
    const-string v4, "content://com.google.android.gallery3d.GooglePhotoProvider/settings"

    .line 151
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 153
    const-string v5, "auto_upload_enabled"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 156
    const/4 v5, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v1, v4, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_3
    :goto_1
    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;)V

    .line 163
    invoke-static {p0}, Ldqw;->a(Landroid/content/Context;)V

    .line 164
    new-instance v1, Ldqx;

    invoke-direct {v1, p0}, Ldqx;-><init>(Landroid/content/Context;)V

    sput-object v1, Ldxc;->i:Ldxc;

    .line 166
    invoke-static {v2}, Ldhv;->i(Landroid/content/Context;)V

    .line 167
    invoke-static {v2}, Llib;->a(Landroid/content/Context;)V

    .line 168
    invoke-static {v2}, Ldrm;->b(Landroid/content/Context;)V

    .line 170
    invoke-static {v2}, Ldhv;->a(Landroid/content/Context;)I

    move-result v3

    .line 176
    new-instance v1, Lewx;

    invoke-direct {v1}, Lewx;-><init>()V

    .line 183
    const-class v1, Lkza;

    invoke-static {v2, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkza;

    .line 184
    invoke-interface {v1}, Lkza;->a()V

    .line 186
    new-instance v1, Ljava/lang/Thread;

    new-instance v4, Lewy;

    invoke-direct {v4, p0, v2, v3}, Lewy;-><init>(Lcom/google/android/apps/plus/phone/EsApplication;Landroid/content/Context;I)V

    const-string v3, "app_on_create"

    invoke-direct {v1, v4, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 276
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 278
    sget-boolean v1, Lfuu;->a:Z

    if-nez v1, :cond_4

    const/4 v1, 0x1

    sput-boolean v1, Lfuu;->a:Z

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v1, 0x7f0200b8

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lfuu;->b:Landroid/graphics/drawable/NinePatchDrawable;

    const v1, 0x7f0200b9

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lfuu;->c:Landroid/graphics/drawable/NinePatchDrawable;

    const v1, 0x7f0200b3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lfuu;->d:Landroid/graphics/drawable/NinePatchDrawable;

    const v1, 0x7f0200b4

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lfuu;->e:Landroid/graphics/drawable/NinePatchDrawable;

    const/16 v1, 0x10

    invoke-static {v2, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    sput-object v1, Lfuu;->f:Landroid/text/TextPaint;

    const/16 v1, 0x20

    invoke-static {v2, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    sput-object v1, Lfuu;->g:Landroid/text/TextPaint;

    .line 280
    :cond_4
    invoke-static {}, Lfvc;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lfvc;->g:Lfvc;

    .line 281
    invoke-virtual {v1}, Lfvc;->b()Z

    move-result v1

    if-nez v1, :cond_6

    .line 286
    :cond_5
    const-class v1, Lcom/google/android/apps/plus/service/PicasaNetworkReceiver;

    sput-object v1, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->b:Ljava/lang/Class;

    .line 287
    const-class v1, Lcom/google/android/apps/plus/service/PicasaNetworkReceiver;

    invoke-static {v1}, Ljdk;->a(Ljava/lang/Class;)V

    .line 289
    :cond_6
    if-eqz v0, :cond_1

    .line 290
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lhnt;->b(J)V

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto/16 :goto_1
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 347
    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    .line 351
    const-class v0, Lkdv;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->o()V

    .line 352
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 2

    .prologue
    .line 356
    const-string v0, "EsApplication"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x2a

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Trimming memory (onTrimMemory "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 360
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    const/4 v0, 0x5

    if-le p1, v0, :cond_2

    .line 361
    :cond_1
    const-class v0, Lkdv;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->o()V

    .line 363
    :cond_2
    return-void
.end method
