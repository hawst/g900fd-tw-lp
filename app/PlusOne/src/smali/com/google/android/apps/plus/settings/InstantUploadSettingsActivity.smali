.class public Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Lloa;-><init>()V

    .line 30
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 31
    new-instance v0, Lfjt;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lfjt;-><init>(Los;Llqr;)V

    .line 38
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->y:Llqc;

    const v2, 0x7f120012

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->x:Llnh;

    .line 39
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 40
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 41
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_in_photo_app"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->x:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 67
    :cond_0
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 57
    const v0, 0x7f10066c

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 58
    const v0, 0x7f10066d

    new-instance v1, Litl;

    const-string v2, "auto_backup"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 59
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v0, 0x7f04007a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->setContentView(I)V

    .line 47
    return-void
.end method
