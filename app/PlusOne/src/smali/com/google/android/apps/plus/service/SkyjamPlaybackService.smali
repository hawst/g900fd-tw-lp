.class public Lcom/google/android/apps/plus/service/SkyjamPlaybackService;
.super Landroid/app/Service;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Lfif;


# static fields
.field private static a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lfin;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/lang/String;

.field private static f:Ljava/lang/String;

.field private static g:I

.field private static h:Ljava/lang/String;

.field private static i:Ljava/lang/String;

.field private static j:I

.field private static k:I


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Lfic;

.field private d:Landroid/media/MediaPlayer;

.field private l:Landroid/app/NotificationManager;

.field private final m:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->a:Ljava/util/ArrayList;

    .line 57
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->g:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 74
    new-instance v0, Lfij;

    invoke-direct {v0, p0}, Lfij;-><init>(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->m:Ljava/lang/Runnable;

    return-void
.end method

.method public static synthetic a(I)I
    .locals 0

    .prologue
    .line 29
    sput p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->j:I

    return p0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 375
    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 376
    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    .line 378
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a07cb

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 390
    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 391
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 392
    const-string v1, "com.google.android.apps.plus.service.SkyjamPlaybackService.STOP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 393
    const-string v1, "account_id"

    sget v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 394
    const-string v1, "music_url"

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    const-string v1, "song"

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    const-string v1, "activity_id"

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 397
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 399
    :cond_0
    return-void
.end method

.method public static a(Lfin;)V
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method public static synthetic b(I)I
    .locals 0

    .prologue
    .line 29
    sput p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->k:I

    return p0
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f()V

    return-void
.end method

.method public static b(Lfin;)V
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 366
    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->k:I

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b:Landroid/os/Handler;

    return-object v0
.end method

.method private c(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 355
    div-int/lit16 v0, p1, 0x3e8

    .line 356
    div-int/lit8 v1, v0, 0x3c

    .line 357
    rem-int/lit8 v0, v0, 0x3c

    .line 358
    const v2, 0x7f0a07ca

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic d()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->j:I

    return v0
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Ldno;

    new-instance v1, Lkfo;

    sget v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->g:I

    invoke-direct {v1, p0, v2}, Lkfo;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Ldno;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    invoke-virtual {v0}, Ldno;->l()V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b:Landroid/os/Handler;

    new-instance v2, Lfil;

    invoke-direct {v2, p0, v0}, Lfil;-><init>(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;Ldno;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static synthetic e()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 332
    sget-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    .line 333
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 334
    :goto_0
    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    .line 336
    iget-object v3, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b:Landroid/os/Handler;

    new-instance v4, Lfim;

    invoke-direct {v4, v1, v0, v2}, Lfim;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 348
    return-void

    .line 333
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b()V

    .line 213
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 152
    if-nez v0, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    const-string v1, "com.google.android.apps.plus.service.SkyjamPlaybackService.PLAY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 157
    const-string v0, "music_url"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b()V

    .line 163
    :cond_2
    const-string v0, "account_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->g:I

    .line 164
    const-string v0, "music_url"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    .line 165
    const-string v0, "song"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f:Ljava/lang/String;

    .line 166
    const-string v0, "activity_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->h:Ljava/lang/String;

    .line 168
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 174
    const v0, 0x7f0a07c7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    .line 175
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f()V

    .line 177
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lfik;

    invoke-direct {v1, p0}, Lfik;-><init>(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 183
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 185
    :cond_3
    const-string v1, "com.google.android.apps.plus.service.SkyjamPlaybackService.STOP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "music_url"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b()V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stopSelf()V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 289
    const v0, 0x7f0a07c8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    .line 290
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    :goto_0
    return-void

    .line 292
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b()V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 311
    iput-object v2, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    .line 314
    :cond_1
    const v0, 0x7f0a07cb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    .line 315
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f()V

    .line 317
    sput-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->e:Ljava/lang/String;

    .line 318
    sput-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f:Ljava/lang/String;

    .line 319
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->g:I

    .line 320
    sput-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->h:Ljava/lang/String;

    .line 322
    sput v3, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->j:I

    .line 323
    sput v3, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->k:I

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->l:Landroid/app/NotificationManager;

    const/16 v1, 0x6ab0

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 326
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 2

    .prologue
    .line 217
    const-string v0, "SkyjamPlaybackService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x17

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "buffering: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b()V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stopSelf()V

    .line 230
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 116
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->l:Landroid/app/NotificationManager;

    .line 118
    const v0, 0x7f0a07cb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    .line 120
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b:Landroid/os/Handler;

    .line 122
    new-instance v0, Lfic;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b:Landroid/os/Handler;

    const-string v2, "SkyjamServiceThread"

    invoke-direct {v0, v1, v2, p0}, Lfic;-><init>(Landroid/os/Handler;Ljava/lang/String;Lfif;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c:Lfic;

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c:Lfic;

    invoke-virtual {v0}, Lfic;->start()V

    .line 124
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c:Lfic;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c:Lfic;

    invoke-virtual {v0}, Lfic;->a()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c:Lfic;

    .line 141
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 142
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2

    .prologue
    .line 234
    const-string v0, "SkyjamPlaybackService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x2a

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "error: what="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extra="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 238
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b()V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stopSelf()V

    .line 240
    const/4 v0, 0x1

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    if-ne p1, v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 252
    const v0, 0x7f0a07c9

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    sget v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->j:I

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    sget v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->k:I

    .line 253
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 252
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->i:Ljava/lang/String;

    .line 254
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f()V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 257
    const v0, 0x7f0a07c4

    new-array v1, v4, [Ljava/lang/Object;

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f:Ljava/lang/String;

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 259
    const v1, 0x7f0a07c5

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 260
    const v2, 0x7f0a07c6

    new-array v3, v4, [Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->f:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 263
    new-instance v3, Landroid/app/Notification;

    const v4, 0x7f030002

    .line 264
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v3, v4, v0, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 265
    sget v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->g:I

    sget-object v4, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->h:Ljava/lang/String;

    .line 266
    invoke-static {p0, v0, v4}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v4, 0x8000000

    .line 265
    invoke-static {p0, v5, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 268
    invoke-virtual {v3, p0, v1, v2, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 269
    iget v0, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Landroid/app/Notification;->flags:I

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->l:Landroid/app/NotificationManager;

    const/16 v1, 0x6ab0

    invoke-virtual {v0, v1, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 272
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->c:Lfic;

    invoke-virtual {v0, p1}, Lfic;->a(Landroid/content/Intent;)V

    .line 132
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
