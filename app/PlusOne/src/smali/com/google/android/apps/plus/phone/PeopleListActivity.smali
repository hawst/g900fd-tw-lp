.class public Lcom/google/android/apps/plus/phone/PeopleListActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;


# instance fields
.field private final e:Lhee;

.field private final f:Ldie;

.field private g:Leku;

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f100143

    .line 57
    invoke-direct {p0}, Lloa;-><init>()V

    .line 64
    new-instance v0, Lkbx;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 66
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->y:Llqc;

    const v2, 0x7f120009

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->x:Llnh;

    .line 67
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 68
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 70
    new-instance v0, Lizj;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1, v3}, Lizj;-><init>(Landroid/app/Activity;Llqr;I)V

    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 71
    invoke-virtual {v0, v1}, Lizj;->a(Ljava/lang/String;)Lizj;

    .line 73
    new-instance v0, Lhmg;

    sget-object v1, Lonm;->k:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->x:Llnh;

    .line 74
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 77
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->x:Llnh;

    .line 78
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->e:Lhee;

    .line 80
    new-instance v0, Ldie;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->y:Llqc;

    invoke-direct {v0, p0, v3}, Ldie;-><init>(Lz;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->f:Ldie;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->h:I

    packed-switch v0, :pswitch_data_0

    .line 234
    :pswitch_0
    sget-object v0, Lhmw;->aa:Lhmw;

    :goto_0
    return-object v0

    .line 228
    :pswitch_1
    sget-object v0, Lhmw;->k:Lhmw;

    goto :goto_0

    .line 230
    :pswitch_2
    sget-object v0, Lhmw;->x:Lhmw;

    goto :goto_0

    .line 232
    :pswitch_3
    sget-object v0, Lhmw;->v:Lhmw;

    goto :goto_0

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 132
    iget v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->h:I

    packed-switch v0, :pswitch_data_0

    .line 139
    invoke-super {p0}, Lloa;->a()Landroid/content/Intent;

    move-result-object v0

    :goto_0
    return-object v0

    .line 135
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->x:Llnh;

    const-class v1, Lkex;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkex;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->e:Lhee;

    .line 136
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-interface {v0, p0, v1}, Lkex;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 118
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->x:Llnh;

    const-class v1, Lhmq;

    .line 121
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ljpr;

    new-instance v2, Ljqy;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Ljqy;-><init>(Landroid/content/Context;Llqr;)V

    .line 122
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldvv;

    new-instance v2, Ldvv;

    invoke-direct {v2, p0}, Ldvv;-><init>(Landroid/content/Context;)V

    .line 123
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "disable_up_button"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->x:Llnh;

    const-class v1, Llln;

    new-instance v2, Llln;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->y:Llqc;

    invoke-direct {v2, p0, v3}, Llln;-><init>(Los;Llqr;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 128
    :cond_0
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 214
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 215
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 216
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 217
    return-void
.end method

.method public a(Loo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 201
    invoke-static {p1, v2}, Lley;->a(Loo;Z)V

    .line 202
    invoke-virtual {p1, v2}, Loo;->d(Z)V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "disable_up_button"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 206
    :cond_0
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 110
    instance-of v0, p1, Leku;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 111
    check-cast v0, Leku;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    .line 113
    :cond_0
    invoke-super {p0, p1}, Lloa;->a(Lu;)V

    .line 114
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    invoke-virtual {v0}, Leku;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->setResult(I)V

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->finish()V

    .line 106
    return-void

    .line 104
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "people_view_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->h:I

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "people_clear_cache"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->i:Z

    .line 93
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 95
    if-nez p1, :cond_0

    .line 96
    iget v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->h:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->f:Ldie;

    invoke-virtual {v1, v0}, Ldie;->a(Lu;)V

    .line 99
    :cond_0
    const v0, 0x7f0400cb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->setContentView(I)V

    .line 100
    return-void

    .line 96
    :pswitch_0
    new-instance v0, Lejp;

    invoke-direct {v0}, Lejp;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_1
    new-instance v0, Lekm;

    invoke-direct {v0}, Lekm;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_2
    new-instance v0, Leml;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->i:Z

    invoke-direct {v0, v1}, Leml;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_3
    new-instance v0, Lejr;

    invoke-direct {v0, v2}, Lejr;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_4
    new-instance v0, Lejr;

    invoke-direct {v0, v3}, Lejr;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_5
    new-instance v0, Lekk;

    invoke-direct {v0}, Lekk;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_6
    new-instance v0, Lejy;

    invoke-direct {v0}, Lejy;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_7
    new-instance v0, Leju;

    invoke-direct {v0}, Leju;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_8
    new-instance v0, Lekd;

    invoke-direct {v0}, Lekd;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "is_for_unified_search"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    new-instance v2, Lelz;

    invoke-direct {v2, v0, v1}, Lelz;-><init>(Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_a
    new-instance v0, Lejc;

    invoke-direct {v0}, Lejc;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto :goto_0

    :pswitch_b
    new-instance v0, Leme;

    invoke-direct {v0}, Leme;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto/16 :goto_0

    :pswitch_c
    new-instance v0, Lelc;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->i:Z

    invoke-direct {v0, v1}, Lelc;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto/16 :goto_0

    :pswitch_d
    new-instance v0, Leko;

    invoke-direct {v0}, Leko;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->g:Leku;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_2
        :pswitch_9
        :pswitch_7
    .end packed-switch
.end method
