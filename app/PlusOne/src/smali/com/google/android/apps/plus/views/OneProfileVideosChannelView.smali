.class public Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;
.super Lfzb;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lfzb;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lfzb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lfzb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lnfq;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 43
    if-eqz p1, :cond_0

    iget-object v0, p1, Lnfq;->b:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->c:Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0355

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 43
    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->a:Lfzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->a:Lfzc;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lfzc;->l(Ljava/lang/String;)V

    .line 59
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-super {p0}, Lfzb;->onFinishInflate()V

    .line 38
    const v0, 0x7f100118

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->b:Landroid/widget/TextView;

    .line 39
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->setPadding(IIII)V

    .line 40
    return-void
.end method
