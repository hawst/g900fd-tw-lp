.class public final Llav;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmbe;",
        "Lmbf;",
        ">;"
    }
.end annotation


# instance fields
.field private final A:J

.field private final B:[Ljava/lang/String;

.field private C:J

.field private D:Ljava/lang/String;

.field private E:[B

.field private final a:I

.field private final b:Lkzl;

.field private final c:I

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:Z

.field private final t:Ljava/lang/String;

.field private final u:[B

.field private final v:I

.field private final w:I

.field private final x:[Ljava/lang/String;

.field private final y:[Ljava/lang/String;

.field private final z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZJ[Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 75
    new-instance v4, Lkfo;

    move-object/from16 v0, p13

    invoke-direct {v4, p1, p2, v0}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    const-string v5, "getactivities"

    new-instance v6, Lmbe;

    invoke-direct {v6}, Lmbe;-><init>()V

    new-instance v7, Lmbf;

    invoke-direct {v7}, Lmbf;-><init>()V

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 77
    iput p2, p0, Llav;->a:I

    .line 78
    iget-object v2, p0, Llav;->f:Landroid/content/Context;

    const-class v3, Lkzl;

    invoke-static {v2, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkzl;

    iput-object v2, p0, Llav;->b:Lkzl;

    .line 79
    iput p3, p0, Llav;->c:I

    .line 80
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "f."

    invoke-virtual {p4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    const/4 v2, 0x2

    invoke-virtual {p4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    .line 83
    :cond_0
    iput-object p4, p0, Llav;->p:Ljava/lang/String;

    .line 84
    iput-object p5, p0, Llav;->q:Ljava/lang/String;

    .line 85
    iput-object p6, p0, Llav;->r:Ljava/lang/String;

    .line 86
    move/from16 v0, p7

    iput-boolean v0, p0, Llav;->s:Z

    .line 87
    move-object/from16 v0, p8

    iput-object v0, p0, Llav;->t:Ljava/lang/String;

    .line 88
    move-object/from16 v0, p9

    iput-object v0, p0, Llav;->u:[B

    .line 89
    if-lez p10, :cond_1

    :goto_0
    move/from16 v0, p10

    iput v0, p0, Llav;->v:I

    .line 90
    new-instance v2, Llcr;

    invoke-direct {v2, p1}, Llcr;-><init>(Landroid/content/Context;)V

    iget v2, v2, Llcr;->a:I

    iput v2, p0, Llav;->w:I

    .line 91
    move-object/from16 v0, p11

    iput-object v0, p0, Llav;->x:[Ljava/lang/String;

    .line 92
    move-object/from16 v0, p12

    iput-object v0, p0, Llav;->y:[Ljava/lang/String;

    .line 93
    move/from16 v0, p14

    iput-boolean v0, p0, Llav;->z:Z

    .line 94
    move-wide/from16 v0, p15

    iput-wide v0, p0, Llav;->A:J

    .line 95
    move-object/from16 v0, p17

    iput-object v0, p0, Llav;->B:[Ljava/lang/String;

    .line 96
    return-void

    .line 89
    :cond_1
    const/16 p10, 0xa

    goto :goto_0
.end method


# virtual methods
.method protected a(Lmbe;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 100
    new-instance v0, Lnve;

    invoke-direct {v0}, Lnve;-><init>()V

    iput-object v0, p1, Lmbe;->a:Lnve;

    .line 101
    iget-object v4, p1, Lmbe;->a:Lnve;

    .line 102
    new-instance v0, Logk;

    invoke-direct {v0}, Logk;-><init>()V

    iput-object v0, v4, Lnve;->a:Logk;

    .line 103
    iget-object v3, v4, Lnve;->a:Logk;

    iget v0, p0, Llav;->c:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_0
    iput v0, v3, Logk;->a:I

    .line 104
    iget v0, p0, Llav;->c:I

    if-eq v0, v8, :cond_0

    iget v0, p0, Llav;->c:I

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Llav;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Llav;->q:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 106
    :cond_1
    iget-object v0, v4, Lnve;->a:Logk;

    iput v7, v0, Logk;->b:I

    .line 110
    :goto_1
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v3, p0, Llav;->p:Ljava/lang/String;

    iput-object v3, v0, Logk;->d:Ljava/lang/String;

    .line 111
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v3, p0, Llav;->q:Ljava/lang/String;

    iput-object v3, v0, Logk;->c:Ljava/lang/String;

    .line 112
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v3, p0, Llav;->r:Ljava/lang/String;

    iput-object v3, v0, Logk;->e:Ljava/lang/String;

    .line 113
    iget-object v0, p0, Llav;->t:Ljava/lang/String;

    iput-object v0, v4, Lnve;->b:Ljava/lang/String;

    .line 114
    iget-object v0, p0, Llav;->u:[B

    iput-object v0, v4, Lnve;->g:[B

    .line 115
    iget-object v0, v4, Lnve;->a:Logk;

    iget v3, p0, Llav;->v:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Logk;->f:Ljava/lang/Integer;

    .line 117
    iget-object v0, v4, Lnve;->a:Logk;

    iput v8, v0, Logk;->g:I

    .line 118
    iget-object v0, v4, Lnve;->a:Logk;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Logk;->k:Ljava/lang/Boolean;

    .line 119
    iget-object v0, v4, Lnve;->a:Logk;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Logk;->i:Ljava/lang/Integer;

    .line 120
    iget-object v0, v4, Lnve;->a:Logk;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Logk;->m:Ljava/lang/Integer;

    .line 121
    iget-object v0, v4, Lnve;->a:Logk;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Logk;->j:Ljava/lang/Integer;

    .line 123
    iget-object v0, v4, Lnve;->a:Logk;

    new-instance v3, Lofl;

    invoke-direct {v3}, Lofl;-><init>()V

    iput-object v3, v0, Logk;->l:Lofl;

    .line 124
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->l:Lofl;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v0, Lofl;->e:Ljava/lang/Boolean;

    .line 125
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->l:Lofl;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v3, v0, Lofl;->b:Ljava/lang/Boolean;

    .line 126
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->l:Lofl;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v0, Lofl;->c:Ljava/lang/Boolean;

    .line 127
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->l:Lofl;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v0, Lofl;->d:Ljava/lang/Boolean;

    .line 128
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->l:Lofl;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v0, Lofl;->a:Ljava/lang/Boolean;

    .line 129
    iget-object v0, v4, Lnve;->a:Logk;

    new-instance v3, Logu;

    invoke-direct {v3}, Logu;-><init>()V

    iput-object v3, v0, Logk;->n:Logu;

    .line 130
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->n:Logu;

    iget-object v3, p0, Llav;->b:Lkzl;

    iget-boolean v5, p0, Llav;->s:Z

    iget v6, p0, Llav;->a:I

    .line 131
    invoke-interface {v3, v5}, Lkzl;->a(Z)[I

    move-result-object v3

    iput-object v3, v0, Logu;->a:[I

    .line 132
    iget-object v0, v4, Lnve;->a:Logk;

    iput v7, v0, Logk;->h:I

    .line 134
    iget v0, p0, Llav;->c:I

    if-nez v0, :cond_5

    iget-object v0, p0, Llav;->p:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p0, Llav;->q:Ljava/lang/String;

    if-nez v0, :cond_5

    move v3, v1

    .line 136
    :goto_2
    if-nez v3, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lnve;->c:Ljava/lang/Boolean;

    .line 137
    iget-object v0, v4, Lnve;->a:Logk;

    new-instance v5, Logv;

    invoke-direct {v5}, Logv;-><init>()V

    iput-object v5, v0, Logk;->p:Logv;

    .line 138
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->p:Logv;

    iget-object v5, p0, Llav;->b:Lkzl;

    iget-object v6, p0, Llav;->f:Landroid/content/Context;

    iget v7, p0, Llav;->a:I

    .line 139
    invoke-interface {v5, v6, v7, v3}, Lkzl;->a(Landroid/content/Context;IZ)[I

    move-result-object v3

    iput-object v3, v0, Logv;->a:[I

    .line 141
    iget-object v0, v4, Lnve;->a:Logk;

    new-instance v3, Logj;

    invoke-direct {v3}, Logj;-><init>()V

    iput-object v3, v0, Logk;->o:Logj;

    .line 142
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->o:Logj;

    iget-object v3, p0, Llav;->f:Landroid/content/Context;

    iget v5, p0, Llav;->a:I

    invoke-static {v3, v5}, Llcl;->a(Landroid/content/Context;I)[I

    move-result-object v3

    iput-object v3, v0, Logj;->b:[I

    .line 144
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->o:Logj;

    iget-object v3, p0, Llav;->f:Landroid/content/Context;

    .line 145
    invoke-static {v3}, Llap;->a(Landroid/content/Context;)[I

    move-result-object v3

    iput-object v3, v0, Logj;->a:[I

    .line 146
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->o:Logj;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Logj;->c:Ljava/lang/Boolean;

    .line 147
    iget-object v0, v4, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->o:Logj;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Logj;->d:Ljava/lang/Boolean;

    .line 148
    iget-object v0, v4, Lnve;->a:Logk;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Logk;->q:Ljava/lang/Boolean;

    .line 149
    iget-object v0, v4, Lnve;->a:Logk;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Logk;->r:Ljava/lang/Boolean;

    .line 151
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, v4, Lnve;->d:Loxz;

    .line 152
    iget-object v0, v4, Lnve;->d:Loxz;

    iget-object v3, p0, Llav;->b:Lkzl;

    iget-object v5, p0, Llav;->f:Landroid/content/Context;

    iget v6, p0, Llav;->a:I

    invoke-interface {v3, v5, v6}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v3

    iput-object v3, v0, Loxz;->a:[I

    .line 154
    iget-boolean v0, p0, Llav;->z:Z

    if-eqz v0, :cond_8

    .line 155
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lnve;->e:Ljava/lang/Boolean;

    .line 156
    iget-object v0, p0, Llav;->B:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 157
    new-instance v0, Lnvn;

    invoke-direct {v0}, Lnvn;-><init>()V

    iput-object v0, v4, Lnve;->j:Lnvn;

    .line 158
    iget-object v0, v4, Lnve;->j:Lnvn;

    iget-wide v6, p0, Llav;->A:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lnvn;->a:Ljava/lang/Long;

    .line 159
    iget-object v0, v4, Lnve;->j:Lnvn;

    iget-object v1, p0, Llav;->B:[Ljava/lang/String;

    iput-object v1, v0, Lnvn;->b:[Ljava/lang/String;

    .line 160
    const-string v0, "GetActivitiesOperation"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161
    const-string v0, "Check for new posts: "

    iget-object v1, p0, Llav;->B:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 168
    :cond_2
    :goto_4
    new-instance v0, Locx;

    invoke-direct {v0}, Locx;-><init>()V

    iput-object v0, v4, Lnve;->f:Locx;

    .line 169
    iget-object v0, v4, Lnve;->f:Locx;

    iget v1, p0, Llav;->w:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Locx;->a:Ljava/lang/Integer;

    .line 171
    iget-object v0, p0, Llav;->x:[Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 172
    new-instance v0, Lnvk;

    invoke-direct {v0}, Lnvk;-><init>()V

    .line 173
    iput v8, v0, Lnvk;->b:I

    .line 174
    iget-object v1, p0, Llav;->x:[Ljava/lang/String;

    iput-object v1, v0, Lnvk;->a:[Ljava/lang/String;

    .line 175
    iput-object v0, v4, Lnve;->h:Lnvk;

    .line 178
    :cond_3
    iget-object v0, p0, Llav;->y:[Ljava/lang/String;

    if-eqz v0, :cond_a

    iget v0, p0, Llav;->c:I

    if-nez v0, :cond_a

    .line 179
    new-instance v0, Lnvp;

    invoke-direct {v0}, Lnvp;-><init>()V

    iput-object v0, v4, Lnve;->i:Lnvp;

    .line 180
    iget-object v0, v4, Lnve;->i:Lnvp;

    iget-object v1, p0, Llav;->y:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [Lnvo;

    iput-object v1, v0, Lnvp;->a:[Lnvo;

    .line 182
    :goto_5
    iget-object v0, p0, Llav;->y:[Ljava/lang/String;

    array-length v0, v0

    if-ge v2, v0, :cond_a

    .line 183
    new-instance v0, Lnvo;

    invoke-direct {v0}, Lnvo;-><init>()V

    .line 184
    new-instance v1, Lpym;

    invoke-direct {v1}, Lpym;-><init>()V

    iput-object v1, v0, Lnvo;->b:Lpym;

    .line 185
    iget-object v1, v0, Lnvo;->b:Lpym;

    const/16 v3, 0x3e9

    iput v3, v1, Lpym;->a:I

    .line 186
    iget-object v1, v0, Lnvo;->b:Lpym;

    iget-object v3, p0, Llav;->y:[Ljava/lang/String;

    aget-object v3, v3, v2

    iput-object v3, v1, Lpym;->b:Ljava/lang/String;

    .line 187
    iget-object v1, v4, Lnve;->i:Lnvp;

    iget-object v1, v1, Lnvp;->a:[Lnvo;

    aput-object v0, v1, v2

    .line 182
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 103
    :pswitch_1
    const/4 v0, 0x5

    goto/16 :goto_0

    :pswitch_2
    const/16 v0, 0x10

    goto/16 :goto_0

    :pswitch_3
    const/16 v0, 0x12

    goto/16 :goto_0

    .line 108
    :cond_4
    iget-object v0, v4, Lnve;->a:Logk;

    iput v1, v0, Logk;->b:I

    goto/16 :goto_1

    :cond_5
    move v3, v2

    .line 134
    goto/16 :goto_2

    :cond_6
    move v0, v2

    .line 136
    goto/16 :goto_3

    .line 161
    :cond_7
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 165
    :cond_8
    iget-boolean v0, p0, Llav;->s:Z

    if-nez v0, :cond_9

    :goto_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lnve;->e:Ljava/lang/Boolean;

    goto/16 :goto_4

    :cond_9
    move v1, v2

    goto :goto_6

    .line 191
    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Llav;->C:J

    .line 192
    return-void

    .line 103
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Lmbf;)V
    .locals 2

    .prologue
    .line 196
    invoke-super {p0, p1}, Lkgg;->a_(Loxu;)V

    .line 197
    iget-object v0, p1, Lmbf;->a:Loda;

    iget-object v0, v0, Loda;->a:Logi;

    iget-object v0, v0, Logi;->c:Ljava/lang/String;

    iput-object v0, p0, Llav;->D:Ljava/lang/String;

    .line 198
    iget-object v0, p1, Lmbf;->a:Loda;

    iget-object v0, v0, Loda;->a:Logi;

    iget-object v0, v0, Logi;->d:[B

    iput-object v0, p0, Llav;->E:[B

    .line 199
    iget-object v0, p1, Lmbf;->a:Loda;

    iget-object v0, v0, Loda;->c:Lodc;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p1, Lmbf;->a:Loda;

    iget-object v0, v0, Loda;->c:Lodc;

    iget-object v0, v0, Lodc;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Llav;->C:J

    .line 202
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lmbe;

    invoke-virtual {p0, p1}, Llav;->a(Lmbe;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lmbf;

    invoke-virtual {p0, p1}, Llav;->a(Lmbf;)V

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Llav;->D:Ljava/lang/String;

    return-object v0
.end method

.method public j()[B
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Llav;->E:[B

    return-object v0
.end method

.method public k()J
    .locals 2

    .prologue
    .line 213
    iget-wide v0, p0, Llav;->C:J

    return-wide v0
.end method
