.class public Lhxq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lhzm;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lhxq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lhxs;

.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lhxr;

    invoke-direct {v0}, Lhxr;-><init>()V

    sput-object v0, Lhxq;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-class v0, Lhxq;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhxs;

    iput-object v0, p0, Lhxq;->a:Lhxs;

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhxq;->b:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public a()Lhxs;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lhxq;->a:Lhxs;

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    packed-switch p1, :pswitch_data_0

    .line 52
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 50
    :pswitch_0
    iget-object v0, p0, Lhxq;->a:Lhxs;

    invoke-virtual {v0}, Lhxs;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 70
    instance-of v1, p1, Lhxq;

    if-eqz v1, :cond_0

    .line 71
    check-cast p1, Lhxq;

    .line 72
    iget-object v1, p0, Lhxq;->a:Lhxs;

    iget-object v2, p1, Lhxq;->a:Lhxs;

    invoke-virtual {v1, v2}, Lhxs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhxq;->b:Ljava/lang/String;

    iget-object v2, p1, Lhxq;->b:Ljava/lang/String;

    .line 73
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 75
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 58
    const/16 v0, 0x11

    .line 59
    iget-object v1, p0, Lhxq;->a:Lhxs;

    if-eqz v1, :cond_0

    .line 60
    iget-object v0, p0, Lhxq;->a:Lhxs;

    invoke-virtual {v0}, Lhxs;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 62
    :cond_0
    iget-object v1, p0, Lhxq;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 63
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhxq;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lhxq;->a:Lhxs;

    invoke-virtual {v0}, Lhxs;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lhxq;->a:Lhxs;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 86
    iget-object v0, p0, Lhxq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    return-void
.end method
