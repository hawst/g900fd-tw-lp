.class public final Lerj;
.super Lhya;
.source "PG"


# static fields
.field private static t:I

.field private static u:Lkdv;


# instance fields
.field private final a:I

.field private b:Landroid/view/View$OnTouchListener;

.field private c:Lfaw;

.field private d:Landroid/view/LayoutInflater;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:J

.field private j:J

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:[B

.field private q:[B

.field private r:Z

.field private s:Lmyx;

.field private final v:Lezf;

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, -0x1

    sput v0, Lerj;->t:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lezf;Landroid/view/View$OnTouchListener;Lfaw;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 259
    invoke-direct {p0, p1, v2}, Lhya;-><init>(Landroid/content/Context;B)V

    .line 99
    iput-boolean v2, p0, Lerj;->l:Z

    .line 100
    iput-boolean v2, p0, Lerj;->m:Z

    .line 102
    iput-boolean v2, p0, Lerj;->n:Z

    .line 103
    iput-boolean v2, p0, Lerj;->o:Z

    .line 116
    sget-object v0, Lerj;->u:Lkdv;

    if-nez v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lerj;->as()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lkdv;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    sput-object v0, Lerj;->u:Lkdv;

    .line 120
    :cond_0
    sget v0, Lerj;->t:I

    if-gez v0, :cond_1

    .line 121
    invoke-virtual {p0}, Lerj;->as()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d028b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lerj;->t:I

    .line 261
    :cond_1
    iput-object p3, p0, Lerj;->b:Landroid/view/View$OnTouchListener;

    .line 262
    iput-object p4, p0, Lerj;->c:Lfaw;

    .line 263
    iput-object p2, p0, Lerj;->v:Lezf;

    .line 264
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lerj;->d:Landroid/view/LayoutInflater;

    .line 265
    iput p5, p0, Lerj;->a:I

    .line 268
    invoke-virtual {p0, v2, v3}, Lerj;->b(ZZ)V

    .line 271
    invoke-virtual {p0, v2, v3}, Lerj;->b(ZZ)V

    .line 274
    invoke-virtual {p0, v2, v2}, Lerj;->b(ZZ)V

    .line 277
    invoke-virtual {p0, v3, v3}, Lerj;->b(ZZ)V

    .line 280
    invoke-virtual {p0, v2, v3}, Lerj;->b(ZZ)V

    .line 283
    invoke-virtual {p0, v3, v3}, Lerj;->b(ZZ)V

    .line 286
    invoke-virtual {p0, v3, v3}, Lerj;->b(ZZ)V

    .line 289
    invoke-virtual {p0, v2, v3}, Lerj;->b(ZZ)V

    .line 291
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 292
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lerj;->e:I

    .line 293
    const v1, 0x7f0b0141

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lerj;->f:I

    .line 294
    return-void
.end method

.method static synthetic a(Lerj;)I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lerj;->a:I

    return v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 409
    if-nez p1, :cond_1

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    iget v1, p0, Lerj;->a:I

    packed-switch v1, :pswitch_data_0

    .line 427
    const-string v1, "next_unread_high_fetch_param"

    .line 428
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lerj;->p:[B

    .line 429
    const-string v1, "next_unread_low_fetch_param"

    .line 430
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lerj;->q:[B

    .line 431
    const-string v1, "unread_low_summary"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 435
    :goto_1
    if-eqz v1, :cond_7

    array-length v2, v1

    if-lez v2, :cond_7

    .line 437
    if-eqz v1, :cond_2

    :try_start_0
    iget-object v2, p0, Lerj;->s:Lmyx;

    if-nez v2, :cond_6

    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 438
    invoke-virtual {p0}, Lerj;->b()V

    .line 440
    :cond_3
    new-instance v0, Lmyx;

    invoke-direct {v0}, Lmyx;-><init>()V

    .line 441
    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmyx;

    iput-object v0, p0, Lerj;->s:Lmyx;

    .line 442
    const-string v0, "HostNotifBarAdapter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 443
    iget-object v0, p0, Lerj;->s:Lmyx;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Update low priority summary: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 453
    :cond_4
    :goto_3
    iget-object v0, p0, Lerj;->s:Lmyx;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lerj;->s:Lmyx;

    iget-object v0, v0, Lmyx;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    :cond_5
    invoke-virtual {p0}, Lerj;->b()V

    goto :goto_0

    .line 418
    :pswitch_0
    const-string v1, "next_read_high_fetch_param"

    .line 419
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lerj;->p:[B

    .line 420
    const-string v1, "next_read_low_fetch_param"

    .line 421
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lerj;->q:[B

    .line 422
    const-string v1, "read_low_summary"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    goto :goto_1

    .line 437
    :cond_6
    :try_start_1
    iget-object v2, p0, Lerj;->s:Lmyx;

    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    .line 445
    :catch_0
    move-exception v0

    .line 446
    const-string v1, "HostNotifBarAdapter"

    const-string v2, "Failed to deserialize the low-priority summmary "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 447
    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lerj;->s:Lmyx;

    goto :goto_3

    .line 414
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lerj;IIZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lerj;->s()I

    move-result v3

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    move v2, v0

    :goto_0
    const/4 v4, 0x4

    if-ne p2, v4, :cond_1

    :goto_1
    invoke-static {v2, v0, p3}, Lhmv;->a(ZZZ)Lhmv;

    move-result-object v1

    iget-object v0, p0, Lerj;->k:Landroid/content/Context;

    const-class v2, Lhms;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v4, p0, Lerj;->k:Landroid/content/Context;

    invoke-direct {v2, v4, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->w:Lhmw;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    return-void

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lerj;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 71
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/AnimationDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/AnimationDrawable;->setVisible(ZZ)Z

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    return-void
.end method

.method static synthetic a(Lerj;Ljava/util/List;ILandroid/content/Intent;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 71
    invoke-direct {p0}, Lerj;->s()I

    move-result v2

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    if-eqz p3, :cond_1

    iget-object v0, p0, Lerj;->k:Landroid/content/Context;

    invoke-virtual {v0, p3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    iget-object v0, v0, Ldqv;->c:Ljava/lang/String;

    invoke-static {v0, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    const-wide/16 v6, 0x0

    move-object v3, p4

    move-object v5, v4

    invoke-static/range {v1 .. v7}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    invoke-static {v1, v2, v0, v4}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    if-le v0, v1, :cond_0

    :try_start_0
    invoke-static {p1}, Ldqu;->a(Ljava/util/List;)[B

    move-result-object v0

    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/PeopleListActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "circle_actor_data"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v0, "people_view_type"

    const/16 v1, 0xd

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "destination_intent"

    invoke-virtual {v3, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lerj;->k:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "HostNotifBarAdapter"

    const-string v2, "Failed to serialize actors "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic a(Lerj;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lerj;->l:Z

    return p1
.end method

.method private b(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 538
    invoke-static {p1}, Lerj;->c(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lerj;)Lfaw;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lerj;->c:Lfaw;

    return-object v0
.end method

.method private b(IZ)V
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0, p1}, Lerj;->l(I)Landroid/database/Cursor;

    move-result-object v0

    .line 184
    instance-of v1, v0, Lexk;

    if-eqz v1, :cond_0

    .line 185
    check-cast v0, Lexk;

    invoke-virtual {v0, p2}, Lexk;->a(Z)V

    .line 186
    invoke-virtual {p0}, Lerj;->av()V

    .line 187
    invoke-virtual {p0}, Lerj;->notifyDataSetChanged()V

    .line 189
    :cond_0
    return-void
.end method

.method static synthetic b(Lerj;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lerj;->m:Z

    return p1
.end method

.method private c(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 830
    if-nez p1, :cond_1

    iget v0, p0, Lerj;->g:I

    if-nez v0, :cond_1

    iget v0, p0, Lerj;->h:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lerj;->l:Z

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 833
    :goto_0
    invoke-virtual {p0, v1, v0}, Lerj;->a(IZ)V

    .line 834
    return-void

    .line 830
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/database/Cursor;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1171
    if-eqz p0, :cond_2

    invoke-interface {p0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    sget-object v3, Ldsh;->a:[Ljava/lang/String;

    const/16 v3, 0x1e

    if-ne v2, v3, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 1172
    instance-of v1, p0, Landroid/database/AbstractWindowedCursor;

    if-eqz v1, :cond_0

    .line 1173
    check-cast p0, Landroid/database/AbstractWindowedCursor;

    invoke-virtual {p0}, Landroid/database/AbstractWindowedCursor;->hasWindow()Z

    move-result v0

    .line 1178
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 1171
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1178
    goto :goto_1
.end method

.method static synthetic c(Lerj;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lerj;->l:Z

    return v0
.end method

.method static synthetic c(Lerj;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lerj;->r:Z

    return p1
.end method

.method static synthetic d(Lerj;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lerj;->m:Z

    return v0
.end method

.method static synthetic e(Lerj;)[B
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lerj;->s:Lmyx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lerj;->s:Lmyx;

    iget-object v0, v0, Lmyx;->a:Lmye;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lerj;->s:Lmyx;

    iget-object v0, v0, Lmyx;->a:Lmye;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lerj;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lerj;->q()V

    return-void
.end method

.method static synthetic g(Lerj;)I
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lerj;->s()I

    move-result v0

    return v0
.end method

.method static synthetic h(Lerj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lerj;->k:Landroid/content/Context;

    return-object v0
.end method

.method private o()Z
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lerj;->p:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lerj;->s:Lmyx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lerj;->s:Lmyx;

    iget-object v0, v0, Lmyx;->e:Ljava/lang/Boolean;

    .line 472
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 523
    iget-boolean v0, p0, Lerj;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lerj;->q:[B

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lerj;->l:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lerj;->p:[B

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 525
    :goto_0
    iget v1, p0, Lerj;->a:I

    packed-switch v1, :pswitch_data_0

    .line 532
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lerj;->a(IZ)V

    .line 535
    :goto_1
    return-void

    .line 523
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 527
    :pswitch_0
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lerj;->a(IZ)V

    goto :goto_1

    .line 525
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private q()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 837
    invoke-virtual {p0, v1}, Lerj;->l(Z)V

    .line 840
    iget v0, p0, Lerj;->a:I

    packed-switch v0, :pswitch_data_0

    .line 847
    const/4 v0, 0x3

    .line 850
    :goto_0
    iget-boolean v3, p0, Lerj;->l:Z

    if-nez v3, :cond_0

    move v1, v2

    :cond_0
    invoke-direct {p0, v0, v1}, Lerj;->b(IZ)V

    .line 851
    iget v0, p0, Lerj;->a:I

    if-ne v0, v2, :cond_1

    .line 852
    iget-boolean v0, p0, Lerj;->l:Z

    invoke-direct {p0, v0}, Lerj;->c(Z)V

    .line 853
    invoke-virtual {p0}, Lerj;->notifyDataSetChanged()V

    .line 856
    :cond_1
    invoke-virtual {p0, v2}, Lerj;->l(Z)V

    .line 857
    return-void

    .line 842
    :pswitch_0
    const/4 v0, 0x6

    .line 843
    goto :goto_0

    .line 840
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private r()[B
    .locals 1

    .prologue
    .line 878
    iget-boolean v0, p0, Lerj;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lerj;->q:[B

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lerj;->p:[B

    goto :goto_0
.end method

.method private s()I
    .locals 2

    .prologue
    .line 978
    iget-object v0, p0, Lerj;->k:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 572
    const/16 v0, 0x8

    return v0
.end method

.method protected a(II)I
    .locals 0

    .prologue
    .line 591
    return p1
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 637
    packed-switch p2, :pswitch_data_0

    .line 645
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 642
    :pswitch_1
    iget-object v0, p0, Lerj;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f04012a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 637
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 608
    packed-switch p2, :pswitch_data_0

    .line 631
    :pswitch_0
    invoke-super {p0, p1, p2, p3, p4}, Lhya;->a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 610
    :pswitch_1
    iget-object v0, p0, Lerj;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f0401b1

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 615
    :pswitch_2
    iget-object v0, p0, Lerj;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f040104

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 621
    :pswitch_3
    iget-object v0, p0, Lerj;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f0400f7

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 625
    :pswitch_4
    iget-object v0, p0, Lerj;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f040126

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 608
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 557
    iput-wide p1, p0, Lerj;->i:J

    .line 558
    invoke-virtual {p0}, Lerj;->notifyDataSetChanged()V

    .line 559
    return-void
.end method

.method public a(Landroid/content/Context;II)V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1048
    invoke-virtual {p0, p3}, Lerj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1049
    invoke-static {v0}, Lerj;->c(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1082
    :cond_0
    :goto_0
    return-void

    .line 1053
    :cond_1
    invoke-static {p1, p2, v0}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v9

    .line 1056
    if-eqz v9, :cond_0

    .line 1057
    const/16 v1, 0xb

    .line 1058
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_3

    move v1, v6

    .line 1060
    :goto_1
    if-eqz v1, :cond_2

    .line 1061
    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1062
    const/16 v2, 0x14

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1063
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1064
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1065
    new-array v4, v6, [J

    aput-wide v10, v4, v5

    .line 1066
    new-instance v0, Ldph;

    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    const-string v8, "GPLUS_APP_V3"

    move v2, p2

    move v7, v5

    invoke-direct/range {v0 .. v8}, Ldph;-><init>(Landroid/content/Context;ILjava/util/List;[JZZZLjava/lang/String;)V

    .line 1069
    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    invoke-static {v1, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 1075
    :cond_2
    :try_start_0
    invoke-virtual {p1, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1076
    :catch_0
    move-exception v0

    .line 1077
    const-string v1, "HostNotifBarAdapter"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1078
    const-string v1, "HostNotifBarAdapter"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot launch activity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    move v1, v5

    .line 1058
    goto :goto_1
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 542
    invoke-static {p1}, Lerj;->c(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 544
    :cond_0
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 545
    iget-wide v2, p0, Lerj;->j:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 546
    iput-wide v0, p0, Lerj;->j:J

    .line 548
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 550
    :cond_2
    return-void
.end method

.method public a(Landroid/database/Cursor;IJLandroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v1, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 324
    iput-wide p3, p0, Lerj;->i:J

    .line 325
    invoke-direct {p0, p5}, Lerj;->a(Landroid/os/Bundle;)V

    .line 326
    iget v3, p0, Lerj;->g:I

    .line 328
    invoke-virtual {p0}, Lerj;->av()V

    .line 329
    invoke-virtual {p0, v2}, Lerj;->l(Z)V

    .line 331
    invoke-virtual {p0, v2, v2}, Lerj;->a(IZ)V

    .line 334
    invoke-virtual {p0, v6, v2}, Lerj;->a(IZ)V

    .line 335
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 339
    invoke-direct {p0}, Lerj;->o()Z

    move-result v0

    invoke-virtual {p0, v4, v0}, Lerj;->a(IZ)V

    .line 341
    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    .line 342
    invoke-virtual {p0, v5, p1}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 351
    :cond_0
    :goto_0
    invoke-virtual {p0, v5}, Lerj;->l(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lerj;->b(Landroid/database/Cursor;)I

    move-result v0

    iput v0, p0, Lerj;->g:I

    .line 352
    invoke-virtual {p0, v4}, Lerj;->l(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lerj;->b(Landroid/database/Cursor;)I

    move-result v0

    iput v0, p0, Lerj;->h:I

    .line 354
    iget v0, p0, Lerj;->g:I

    if-eq v3, v0, :cond_1

    .line 355
    iget v0, p0, Lerj;->g:I

    if-eq v0, v3, :cond_1

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "extra_prev_num_unread_notifi"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "extra_num_unread_notifi"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-direct {p0}, Lerj;->s()I

    move-result v3

    iget-object v0, p0, Lerj;->k:Landroid/content/Context;

    const-class v5, Lhms;

    invoke-static {v0, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v5, Lhmr;

    iget-object v6, p0, Lerj;->k:Landroid/content/Context;

    invoke-direct {v5, v6, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->bx:Lhmv;

    invoke-virtual {v5, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    sget-object v5, Lhmw;->w:Lhmw;

    invoke-virtual {v3, v5}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v3

    invoke-virtual {v3, v4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v3

    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 359
    :cond_1
    const/4 v0, 0x5

    new-instance v3, Landroid/database/MatrixCursor;

    new-array v4, v2, [Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0, v0, v3}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 361
    invoke-virtual {p0, p1}, Lerj;->a(Landroid/database/Cursor;)V

    .line 362
    invoke-direct {p0, v2}, Lerj;->c(Z)V

    .line 363
    invoke-direct {p0}, Lerj;->p()V

    .line 364
    invoke-virtual {p0, v1}, Lerj;->l(Z)V

    .line 365
    return-void

    .line 343
    :cond_2
    if-ne p2, v5, :cond_0

    .line 344
    invoke-virtual {p0, v4, p1}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 345
    if-eqz p1, :cond_3

    invoke-direct {p0, p1}, Lerj;->b(Landroid/database/Cursor;)I

    move-result v0

    if-nez v0, :cond_4

    .line 346
    :cond_3
    invoke-virtual {p0}, Lerj;->b()V

    .line 348
    :cond_4
    iget-boolean v0, p0, Lerj;->l:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    invoke-direct {p0, v4, v0}, Lerj;->b(IZ)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public a(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x6

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 368
    invoke-direct {p0, p3}, Lerj;->a(Landroid/os/Bundle;)V

    .line 369
    invoke-virtual {p0, v0}, Lerj;->l(Z)V

    .line 371
    invoke-virtual {p0, v0, v0}, Lerj;->a(IZ)V

    .line 374
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v4}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 375
    invoke-virtual {p0, v5, v0}, Lerj;->a(IZ)V

    .line 376
    invoke-virtual {p0, v5, v4}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 379
    invoke-direct {p0}, Lerj;->o()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lerj;->a(IZ)V

    .line 380
    invoke-virtual {p0, v3, p2}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 383
    const/4 v2, 0x5

    invoke-virtual {p0, v2, p1}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 385
    if-eqz p2, :cond_0

    invoke-direct {p0, p2}, Lerj;->b(Landroid/database/Cursor;)I

    move-result v2

    if-nez v2, :cond_1

    .line 386
    :cond_0
    invoke-virtual {p0}, Lerj;->b()V

    .line 389
    :cond_1
    iget-boolean v2, p0, Lerj;->l:Z

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    invoke-direct {p0, v3, v0}, Lerj;->b(IZ)V

    .line 392
    invoke-virtual {p0, v1, v4}, Lerj;->a(ILandroid/database/Cursor;)V

    .line 394
    invoke-direct {p0}, Lerj;->p()V

    .line 395
    invoke-virtual {p0, v1}, Lerj;->l(Z)V

    .line 396
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v5, 0x2

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 659
    packed-switch p2, :pswitch_data_0

    .line 793
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 662
    :pswitch_1
    const v0, 0x7f100343

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 663
    const v0, 0x7f10013b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 664
    iget-boolean v2, p0, Lerj;->w:Z

    if-eqz v2, :cond_1

    .line 665
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 666
    const v1, 0x7f0a0695

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 668
    iput-boolean v3, p0, Lerj;->w:Z

    goto :goto_0

    .line 670
    :cond_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 671
    const v1, 0x7f0a057b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 672
    iget-object v0, p0, Lerj;->v:Lezf;

    if-eqz v0, :cond_0

    .line 673
    iget-boolean v0, p0, Lerj;->l:Z

    if-eqz v0, :cond_3

    .line 674
    iget v0, p0, Lerj;->a:I

    if-ne v0, v5, :cond_2

    .line 675
    iget-object v0, p0, Lerj;->v:Lezf;

    invoke-direct {p0}, Lerj;->r()[B

    move-result-object v1

    invoke-interface {v0, v1, v5, v5}, Lezf;->a([BII)V

    goto :goto_0

    .line 679
    :cond_2
    iget-object v0, p0, Lerj;->v:Lezf;

    invoke-direct {p0}, Lerj;->r()[B

    move-result-object v1

    invoke-interface {v0, v1, v7, v5}, Lezf;->a([BII)V

    goto :goto_0

    .line 684
    :cond_3
    iget v0, p0, Lerj;->a:I

    if-ne v0, v5, :cond_4

    .line 685
    iget-object v0, p0, Lerj;->v:Lezf;

    invoke-direct {p0}, Lerj;->r()[B

    move-result-object v1

    invoke-interface {v0, v1, v5, v8}, Lezf;->a([BII)V

    goto :goto_0

    .line 689
    :cond_4
    iget-object v0, p0, Lerj;->v:Lezf;

    invoke-direct {p0}, Lerj;->r()[B

    move-result-object v1

    invoke-interface {v0, v1, v7, v8}, Lezf;->a([BII)V

    goto :goto_0

    .line 700
    :pswitch_2
    const v0, 0x7f1003ac

    .line 701
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 702
    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    invoke-static {v1, v0, v4}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 704
    const v0, 0x7f1003ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1003aa

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f1003ab

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-object v5, p0, Lerj;->k:Landroid/content/Context;

    invoke-static {v5}, Ldsf;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    new-instance v2, Lerm;

    invoke-direct {v2, p0, v1}, Lerm;-><init>(Lerj;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lern;

    invoke-direct {v2, p0, v1}, Lern;-><init>(Lerj;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    sget-object v4, Ljac;->d:Ljac;

    invoke-static {v1, v5, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(Z)V

    invoke-virtual {v2, v8}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    invoke-virtual {v2, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->c(I)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->f(Z)V

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->e(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f020402

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(I)V

    new-instance v1, Lero;

    invoke-direct {v1, v2}, Lero;-><init>(Lcom/google/android/libraries/social/media/ui/MediaView;)V

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lerp;

    invoke-direct {v1, v2}, Lerp;-><init>(Lcom/google/android/libraries/social/media/ui/MediaView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 709
    :pswitch_3
    const v0, 0x7f10050e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 710
    const v0, 0x7f100175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 711
    iget-boolean v0, p0, Lerj;->n:Z

    if-eqz v0, :cond_6

    move v0, v3

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 713
    iget-boolean v0, p0, Lerj;->n:Z

    if-eqz v0, :cond_7

    :goto_2
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 715
    new-instance v0, Lerk;

    invoke-direct {v0, p0}, Lerk;-><init>(Lerj;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_6
    move v0, v4

    .line 711
    goto :goto_1

    :cond_7
    move v4, v3

    .line 713
    goto :goto_2

    .line 741
    :pswitch_4
    const v0, 0x7f1002eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 742
    const v1, 0x7f100245

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 743
    const v2, 0x7f100363

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 744
    const v5, 0x7f100362

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 745
    const v6, 0x7f100175

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 747
    iget-object v7, p0, Lerj;->s:Lmyx;

    if-eqz v7, :cond_8

    .line 748
    iget-boolean v7, p0, Lerj;->m:Z

    if-eqz v7, :cond_a

    .line 749
    iget-object v7, p0, Lerj;->s:Lmyx;

    iget-object v7, v7, Lmyx;->d:Ljava/lang/String;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 750
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 758
    :cond_8
    :goto_3
    iget-boolean v0, p0, Lerj;->l:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lerj;->o:Z

    if-nez v0, :cond_b

    move v0, v3

    :goto_4
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 760
    iget-boolean v0, p0, Lerj;->l:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lerj;->o:Z

    if-eqz v0, :cond_c

    :cond_9
    move v0, v4

    :goto_5
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 762
    iget-boolean v0, p0, Lerj;->o:Z

    if-eqz v0, :cond_d

    :goto_6
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 764
    new-instance v0, Lerl;

    invoke-direct {v0, p0, v2, v5}, Lerl;-><init>(Lerj;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 752
    :cond_a
    iget-object v7, p0, Lerj;->s:Lmyx;

    iget-object v7, v7, Lmyx;->b:Ljava/lang/String;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 753
    iget-object v0, p0, Lerj;->s:Lmyx;

    iget-object v0, v0, Lmyx;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 754
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_b
    move v0, v4

    .line 758
    goto :goto_4

    :cond_c
    move v0, v3

    .line 760
    goto :goto_5

    :cond_d
    move v3, v4

    .line 762
    goto :goto_6

    .line 659
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 862
    invoke-static {p3}, Lerj;->c(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 875
    :goto_0
    return-void

    .line 866
    :cond_0
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 871
    :pswitch_1
    invoke-virtual {p0, p1, p3, p2}, Lerj;->a(Landroid/view/View;Landroid/database/Cursor;I)V

    goto :goto_0

    .line 866
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/view/View;Landroid/database/Cursor;I)V
    .locals 20

    .prologue
    .line 947
    const/16 v2, 0x9

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 948
    const/4 v2, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    .line 949
    const/16 v2, 0x18

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    .line 950
    :goto_0
    if-eqz v2, :cond_1

    const/4 v6, 0x2

    .line 952
    :goto_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    .line 953
    const/16 v2, 0xd

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 954
    const/16 v2, 0xc

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    .line 955
    const/4 v2, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 956
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 957
    const/4 v2, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 959
    const/16 v2, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 960
    invoke-static {v2}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v13

    .line 962
    const/16 v2, 0x14

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 963
    const/4 v2, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 964
    const/16 v2, 0x13

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 966
    const/16 v2, 0x1a

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 967
    invoke-static {v2}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v19

    .line 969
    invoke-direct/range {p0 .. p0}, Lerj;->s()I

    move-result v2

    .line 970
    move-object/from16 v0, p0

    iget-object v3, v0, Lerj;->k:Landroid/content/Context;

    .line 971
    move-object/from16 v0, p2

    invoke-static {v3, v2, v0}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v17

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v7, p3

    .line 972
    invoke-virtual/range {v3 .. v19}, Lerj;->a(Landroid/view/View;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 975
    return-void

    .line 949
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 950
    :cond_1
    const/16 v2, 0xb

    .line 951
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    goto :goto_1
.end method

.method protected a(Landroid/view/View;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "IIII",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;J",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 988
    move/from16 v0, p4

    invoke-virtual {p0, v0}, Lerj;->o(I)I

    move-result v2

    add-int v3, v2, p5

    .line 989
    move/from16 v0, p4

    invoke-virtual {p0, v0}, Lerj;->k(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    add-int/2addr v3, v2

    .line 990
    const v2, 0x7f1003b6

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 991
    invoke-static {v12}, Lfvi;->g(Landroid/view/View;)V

    .line 992
    iget-wide v4, p0, Lerj;->i:J

    cmp-long v2, p11, v4

    if-lez v2, :cond_4

    if-nez p3, :cond_4

    const/4 v2, 0x1

    move v11, v2

    .line 993
    :goto_1
    if-eqz v11, :cond_5

    const v2, 0x7f020454

    :goto_2
    invoke-virtual {v12, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 998
    new-instance v2, Lerq;

    invoke-direct {v2, p0, v3}, Lerq;-><init>(Lerj;I)V

    invoke-virtual {v12, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1008
    const v2, 0x1020006

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 1009
    const v3, 0x7f1003b7

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Lcom/google/android/apps/plus/views/AvatarsView;

    .line 1010
    if-eqz p10, :cond_0

    invoke-interface/range {p10 .. p10}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_0
    const/4 v3, 0x1

    move v4, v3

    :goto_3
    if-eqz v4, :cond_8

    const/4 v3, 0x0

    :goto_4
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    if-eqz v4, :cond_9

    const/16 v3, 0x8

    :goto_5
    invoke-virtual {v10, v3}, Lcom/google/android/apps/plus/views/AvatarsView;->setVisibility(I)V

    if-eqz v4, :cond_a

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Z)V

    sget v3, Lerj;->t:I

    sget v4, Lerj;->t:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    invoke-static/range {p13 .. p13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lerj;->k:Landroid/content/Context;

    sget-object v4, Ljac;->a:Ljac;

    move-object/from16 v0, p13

    invoke-static {v3, v0, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 1013
    :cond_1
    :goto_6
    if-eqz p3, :cond_2

    const/4 v2, 0x2

    move/from16 v0, p3

    if-ne v0, v2, :cond_b

    .line 1014
    :cond_2
    iget-object v2, p0, Lerj;->b:Landroid/view/View$OnTouchListener;

    invoke-virtual {v12, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1019
    :goto_7
    if-eqz v11, :cond_c

    iget v2, p0, Lerj;->f:I

    move v4, v2

    .line 1021
    :goto_8
    const v2, 0x7f1003b9

    .line 1022
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1024
    const v3, 0x7f1003ba

    .line 1025
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1027
    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1028
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1029
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1036
    :goto_9
    const v3, 0x7f1003b8

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1038
    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1039
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1041
    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1042
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1044
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static/range {p8 .. p8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1045
    return-void

    .line 989
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 992
    :cond_4
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_1

    .line 993
    :cond_5
    if-eqz p3, :cond_6

    const v2, 0x7f020452

    goto/16 :goto_2

    :cond_6
    const v2, 0x7f020453

    goto/16 :goto_2

    .line 1010
    :cond_7
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_3

    :cond_8
    const/16 v3, 0x8

    goto/16 :goto_4

    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_a
    const/4 v2, 0x0

    const/4 v3, 0x1

    move-object/from16 v0, p10

    invoke-virtual {v10, v0, v2, v3}, Lcom/google/android/apps/plus/views/AvatarsView;->a(Ljava/util/List;ZZ)V

    new-instance v2, Lerr;

    move-object v3, p0

    move-object/from16 v4, p16

    move-object v5, p2

    move/from16 v6, p6

    move-object/from16 v7, p14

    move-object/from16 v8, p15

    move-object/from16 v9, p10

    invoke-direct/range {v2 .. v9}, Lerr;-><init>(Lerj;Ljava/util/List;Ljava/lang/String;ILandroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/AvatarsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_6

    .line 1016
    :cond_b
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_7

    .line 1019
    :cond_c
    iget v2, p0, Lerj;->e:I

    move v4, v2

    goto/16 :goto_8

    .line 1031
    :cond_d
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1032
    move-object/from16 v0, p9

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1033
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    goto/16 :goto_9
.end method

.method public a(Lmyx;)V
    .locals 0

    .prologue
    .line 1206
    iput-object p1, p0, Lerj;->s:Lmyx;

    .line 1207
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1194
    iput-boolean p1, p0, Lerj;->l:Z

    .line 1195
    return-void
.end method

.method public a(ZZ)V
    .locals 0

    .prologue
    .line 177
    iput-boolean p1, p0, Lerj;->n:Z

    .line 178
    iput-boolean p2, p0, Lerj;->o:Z

    .line 179
    invoke-virtual {p0}, Lerj;->notifyDataSetChanged()V

    .line 180
    return-void
.end method

.method protected a(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 301
    if-eq p1, p2, :cond_1

    .line 302
    instance-of v0, p1, Lexk;

    if-eqz v0, :cond_1

    instance-of v0, p2, Lexk;

    if-eqz v0, :cond_1

    .line 304
    check-cast p1, Lexk;

    .line 305
    check-cast p2, Lexk;

    .line 306
    invoke-virtual {p1}, Lexk;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 307
    invoke-virtual {p2}, Lexk;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 310
    :goto_0
    return v0

    .line 307
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 310
    :cond_1
    invoke-super {p0, p1, p2}, Lhya;->a(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 596
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 399
    iput-boolean v0, p0, Lerj;->l:Z

    .line 400
    iput-boolean v0, p0, Lerj;->m:Z

    .line 401
    return-void
.end method

.method public b(I)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 141
    invoke-direct {p0}, Lerj;->s()I

    move-result v2

    .line 142
    invoke-virtual {p0, p1}, Lerj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 143
    invoke-static {v0}, Lerj;->c(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    .line 144
    invoke-virtual {p0, p1}, Lerj;->m(I)I

    move-result v9

    .line 145
    invoke-direct {p0, v0}, Lerj;->b(Landroid/database/Cursor;)I

    move-result v10

    .line 146
    packed-switch v9, :pswitch_data_0

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 149
    :pswitch_0
    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 150
    const/16 v1, 0x14

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 151
    invoke-direct {p0}, Lerj;->s()I

    move-result v3

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v1, "extra_notification_id"

    invoke-virtual {v6, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    const-class v8, Lhms;

    invoke-static {v1, v8}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhms;

    new-instance v8, Lhmr;

    iget-object v11, p0, Lerj;->k:Landroid/content/Context;

    invoke-direct {v8, v11, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->bI:Lhmv;

    invoke-virtual {v8, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    sget-object v8, Lhmw;->w:Lhmw;

    invoke-virtual {v3, v8}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v3

    invoke-interface {v1, v3}, Lhms;->a(Lhmr;)V

    .line 152
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 153
    instance-of v1, v0, Lexk;

    if-eqz v1, :cond_1

    check-cast v0, Lexk;

    invoke-virtual {v0, v4}, Lexk;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lerj;->av()V

    invoke-virtual {p0}, Lerj;->notifyDataSetChanged()V

    .line 154
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 155
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    new-array v4, v5, [J

    aput-wide v12, v4, v7

    .line 157
    new-instance v0, Ldph;

    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    const-string v8, "GPLUS_APP_V3"

    move v6, v5

    invoke-direct/range {v0 .. v8}, Ldph;-><init>(Landroid/content/Context;ILjava/util/List;[JZZZLjava/lang/String;)V

    .line 161
    iget-object v1, p0, Lerj;->k:Landroid/content/Context;

    invoke-static {v1, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 165
    if-ne v10, v5, :cond_0

    const/4 v0, 0x3

    if-ne v9, v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lerj;->as()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2, v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;II)Ljava/lang/Integer;

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 1198
    iput-boolean p1, p0, Lerj;->m:Z

    .line 1199
    return-void
.end method

.method protected b(II)Z
    .locals 1

    .prologue
    .line 601
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lerj;->l:Z

    .line 405
    invoke-direct {p0}, Lerj;->q()V

    .line 406
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 476
    iget v0, p0, Lerj;->a:I

    packed-switch v0, :pswitch_data_0

    .line 482
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lerj;->o(I)I

    move-result v0

    :goto_0
    return v0

    .line 478
    :pswitch_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lerj;->o(I)I

    move-result v0

    goto :goto_0

    .line 476
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public e()I
    .locals 2

    .prologue
    .line 488
    iget v0, p0, Lerj;->a:I

    packed-switch v0, :pswitch_data_0

    .line 495
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lerj;->l(I)Landroid/database/Cursor;

    move-result-object v0

    .line 498
    :goto_0
    invoke-static {v0}, Lerj;->c(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_1
    return v0

    .line 490
    :pswitch_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lerj;->l(I)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 498
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 505
    iget v0, p0, Lerj;->a:I

    packed-switch v0, :pswitch_data_0

    .line 513
    invoke-virtual {p0, v1}, Lerj;->a(I)I

    move-result v0

    .line 516
    :goto_0
    if-lez v0, :cond_0

    .line 517
    const/4 v0, 0x1

    iput-boolean v0, p0, Lerj;->w:Z

    .line 518
    invoke-virtual {p0}, Lerj;->notifyDataSetChanged()V

    .line 520
    :cond_0
    return-void

    .line 508
    :pswitch_0
    invoke-virtual {p0, v1}, Lerj;->a(I)I

    move-result v0

    goto :goto_0

    .line 505
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public g()J
    .locals 2

    .prologue
    .line 553
    iget-wide v0, p0, Lerj;->j:J

    return-wide v0
.end method

.method public getItemId(I)J
    .locals 5

    .prologue
    .line 201
    invoke-virtual {p0, p1}, Lerj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 202
    invoke-virtual {p0, p1}, Lerj;->m(I)I

    move-result v2

    .line 203
    const/4 v1, 0x2

    if-eq v2, v1, :cond_0

    const/4 v1, 0x5

    if-eq v2, v1, :cond_0

    const/4 v1, 0x3

    if-eq v2, v1, :cond_0

    const/4 v1, 0x6

    if-ne v2, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 208
    :goto_0
    invoke-virtual {p0, p1}, Lerj;->n(I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 210
    const-wide/high16 v0, -0x8000000000000000L

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 216
    :goto_1
    return-wide v0

    .line 203
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 211
    :cond_2
    invoke-static {v0}, Lerj;->c(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v1, :cond_3

    .line 213
    const/16 v1, 0x14

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    goto :goto_1

    .line 216
    :cond_3
    const-wide v0, 0x7fffffffffffffffL

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 577
    invoke-virtual {p0}, Lerj;->aw()V

    .line 578
    invoke-virtual {p0, p1}, Lerj;->m(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 585
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lhya;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lerj;->g:I

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 822
    iget-boolean v0, p0, Lerj;->r:Z

    return v0
.end method

.method public k()V
    .locals 1

    .prologue
    .line 826
    const/4 v0, 0x0

    iput-boolean v0, p0, Lerj;->r:Z

    .line 827
    return-void
.end method

.method public k_(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-virtual {p0, p1}, Lerj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 128
    invoke-static {v0}, Lerj;->c(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    invoke-virtual {p0, p1}, Lerj;->m(I)I

    move-result v2

    .line 130
    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 137
    :goto_0
    return v0

    .line 133
    :pswitch_0
    const/16 v2, 0x18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 1186
    iget-boolean v0, p0, Lerj;->l:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 1190
    iget-boolean v0, p0, Lerj;->m:Z

    return v0
.end method

.method public n()Lmyx;
    .locals 1

    .prologue
    .line 1202
    iget-object v0, p0, Lerj;->s:Lmyx;

    return-object v0
.end method
