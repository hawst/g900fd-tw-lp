.class public final Llzd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojy;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10204
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10207
    iput-object v0, p0, Llzd;->apiHeader:Llyr;

    .line 10210
    iput-object v0, p0, Llzd;->a:Lojy;

    .line 10204
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10227
    const/4 v0, 0x0

    .line 10228
    iget-object v1, p0, Llzd;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 10229
    const/4 v0, 0x1

    iget-object v1, p0, Llzd;->apiHeader:Llyr;

    .line 10230
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10232
    :cond_0
    iget-object v1, p0, Llzd;->a:Lojy;

    if-eqz v1, :cond_1

    .line 10233
    const/4 v1, 0x2

    iget-object v2, p0, Llzd;->a:Lojy;

    .line 10234
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10236
    :cond_1
    iget-object v1, p0, Llzd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10237
    iput v0, p0, Llzd;->ai:I

    .line 10238
    return v0
.end method

.method public a(Loxn;)Llzd;
    .locals 2

    .prologue
    .line 10246
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10247
    sparse-switch v0, :sswitch_data_0

    .line 10251
    iget-object v1, p0, Llzd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzd;->ah:Ljava/util/List;

    .line 10255
    :cond_1
    iget-object v1, p0, Llzd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10257
    :sswitch_0
    return-object p0

    .line 10262
    :sswitch_1
    iget-object v0, p0, Llzd;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 10263
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzd;->apiHeader:Llyr;

    .line 10265
    :cond_2
    iget-object v0, p0, Llzd;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10269
    :sswitch_2
    iget-object v0, p0, Llzd;->a:Lojy;

    if-nez v0, :cond_3

    .line 10270
    new-instance v0, Lojy;

    invoke-direct {v0}, Lojy;-><init>()V

    iput-object v0, p0, Llzd;->a:Lojy;

    .line 10272
    :cond_3
    iget-object v0, p0, Llzd;->a:Lojy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10247
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10215
    iget-object v0, p0, Llzd;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 10216
    const/4 v0, 0x1

    iget-object v1, p0, Llzd;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10218
    :cond_0
    iget-object v0, p0, Llzd;->a:Lojy;

    if-eqz v0, :cond_1

    .line 10219
    const/4 v0, 0x2

    iget-object v1, p0, Llzd;->a:Lojy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10221
    :cond_1
    iget-object v0, p0, Llzd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10223
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10200
    invoke-virtual {p0, p1}, Llzd;->a(Loxn;)Llzd;

    move-result-object v0

    return-object v0
.end method
