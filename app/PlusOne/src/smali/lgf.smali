.class public final Llgf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llgj;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/Button;


# direct methods
.method constructor <init>(Llgh;)V
    .locals 7

    .prologue
    const v6, 0x7f1001d8

    const v2, 0x7f04005b

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x8

    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llgf;->a:Ljava/util/ArrayList;

    .line 268
    iget-object v0, p1, Llgh;->a:Llgl;

    .line 269
    iget-object v0, p1, Llgh;->b:Landroid/content/Context;

    iput-object v0, p0, Llgf;->b:Landroid/content/Context;

    .line 272
    iget-object v0, p0, Llgf;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 273
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 274
    iget-object v1, p1, Llgh;->c:Ljava/lang/String;

    .line 275
    iget-object v1, p1, Llgh;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 279
    packed-switch v1, :pswitch_data_0

    .line 297
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 281
    :pswitch_0
    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Llgf;->c:Landroid/view/View;

    .line 284
    iget-object v0, p0, Llgf;->c:Landroid/view/View;

    const v2, 0x7f1001d9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 302
    :goto_0
    iget-object v0, p0, Llgf;->f:Landroid/widget/Button;

    if-nez v0, :cond_0

    iget-object v0, p0, Llgf;->c:Landroid/view/View;

    const v2, 0x7f1001d7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Llgf;->f:Landroid/widget/Button;

    :cond_0
    iget-object v0, p0, Llgf;->g:Landroid/widget/Button;

    if-nez v0, :cond_1

    iget-object v0, p0, Llgf;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Llgf;->g:Landroid/widget/Button;

    :cond_1
    iget-object v0, p0, Llgf;->c:Landroid/view/View;

    const v2, 0x7f1001d5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Llgf;->d:Landroid/widget/TextView;

    iget-object v0, p0, Llgf;->c:Landroid/view/View;

    const v2, 0x7f1001d6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Llgf;->e:Landroid/widget/TextView;

    .line 304
    packed-switch v1, :pswitch_data_1

    .line 306
    :goto_1
    iget-object v0, p1, Llgh;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Llgf;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    iget-object v0, p0, Llgf;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 287
    :pswitch_1
    const v2, 0x7f04005a

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Llgf;->c:Landroid/view/View;

    .line 289
    iget-object v0, p0, Llgf;->c:Landroid/view/View;

    const v2, 0x7f1001d4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Llgf;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0100

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 292
    :pswitch_2
    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Llgf;->c:Landroid/view/View;

    .line 294
    iget-object v0, p0, Llgf;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Llgf;->g:Landroid/widget/Button;

    goto/16 :goto_0

    .line 304
    :pswitch_3
    iget-object v0, p0, Llgf;->f:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Llgf;->g:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Llgf;->g:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Llgf;->f:Landroid/widget/Button;

    iget-object v0, p1, Llgh;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llgk;

    invoke-direct {p0, v1, v0}, Llgf;->a(Landroid/widget/Button;Llgk;)V

    goto :goto_1

    :pswitch_5
    iget-object v1, p0, Llgf;->f:Landroid/widget/Button;

    iget-object v0, p1, Llgh;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llgk;

    invoke-direct {p0, v1, v0}, Llgf;->a(Landroid/widget/Button;Llgk;)V

    iget-object v1, p0, Llgf;->g:Landroid/widget/Button;

    iget-object v0, p1, Llgh;->d:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llgk;

    invoke-direct {p0, v1, v0}, Llgf;->a(Landroid/widget/Button;Llgk;)V

    goto/16 :goto_1

    .line 306
    :cond_2
    iget-object v0, p0, Llgf;->d:Landroid/widget/TextView;

    iget-object v1, p1, Llgh;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 279
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 304
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Landroid/widget/Button;Llgk;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 391
    iget-object v0, p2, Llgk;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 393
    iget v0, p2, Llgk;->b:I

    if-eqz v0, :cond_0

    .line 394
    iget v0, p2, Llgk;->b:I

    invoke-virtual {p1, v1, v1, v0, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 397
    :cond_0
    new-instance v0, Llgg;

    invoke-direct {v0, p0, p2}, Llgg;-><init>(Llgf;Llgk;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 404
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Llgf;->c:Landroid/view/View;

    return-object v0
.end method

.method public a(Llgj;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Llgf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 216
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 217
    iget-object v1, p0, Llgf;->d:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 218
    iget-object v1, p0, Llgf;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    :cond_0
    iget-object v1, p0, Llgf;->e:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 221
    iget-object v1, p0, Llgf;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_1
    iget-object v1, p0, Llgf;->f:Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 224
    iget-object v1, p0, Llgf;->f:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_2
    iget-object v1, p0, Llgf;->g:Landroid/widget/Button;

    if-eqz v1, :cond_3

    .line 227
    iget-object v1, p0, Llgf;->g:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    :cond_3
    const-string v1, "\n"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
