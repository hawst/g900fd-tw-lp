.class public final Lnsj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnsj;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Lnsk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1560
    const/4 v0, 0x0

    new-array v0, v0, [Lnsj;

    sput-object v0, Lnsj;->a:[Lnsj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1561
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1643
    const/4 v0, 0x0

    iput-object v0, p0, Lnsj;->e:Lnsk;

    .line 1561
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1666
    const/4 v0, 0x0

    .line 1667
    iget-object v1, p0, Lnsj;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1668
    const/4 v0, 0x1

    iget-object v1, p0, Lnsj;->b:Ljava/lang/String;

    .line 1669
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1671
    :cond_0
    iget-object v1, p0, Lnsj;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1672
    const/4 v1, 0x2

    iget-object v2, p0, Lnsj;->c:Ljava/lang/String;

    .line 1673
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1675
    :cond_1
    iget-object v1, p0, Lnsj;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1676
    const/4 v1, 0x3

    iget-object v2, p0, Lnsj;->d:Ljava/lang/String;

    .line 1677
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1679
    :cond_2
    iget-object v1, p0, Lnsj;->e:Lnsk;

    if-eqz v1, :cond_3

    .line 1680
    const/4 v1, 0x4

    iget-object v2, p0, Lnsj;->e:Lnsk;

    .line 1681
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1683
    :cond_3
    iget-object v1, p0, Lnsj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1684
    iput v0, p0, Lnsj;->ai:I

    .line 1685
    return v0
.end method

.method public a(Loxn;)Lnsj;
    .locals 2

    .prologue
    .line 1693
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1694
    sparse-switch v0, :sswitch_data_0

    .line 1698
    iget-object v1, p0, Lnsj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1699
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsj;->ah:Ljava/util/List;

    .line 1702
    :cond_1
    iget-object v1, p0, Lnsj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1704
    :sswitch_0
    return-object p0

    .line 1709
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsj;->b:Ljava/lang/String;

    goto :goto_0

    .line 1713
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsj;->c:Ljava/lang/String;

    goto :goto_0

    .line 1717
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsj;->d:Ljava/lang/String;

    goto :goto_0

    .line 1721
    :sswitch_4
    iget-object v0, p0, Lnsj;->e:Lnsk;

    if-nez v0, :cond_2

    .line 1722
    new-instance v0, Lnsk;

    invoke-direct {v0}, Lnsk;-><init>()V

    iput-object v0, p0, Lnsj;->e:Lnsk;

    .line 1724
    :cond_2
    iget-object v0, p0, Lnsj;->e:Lnsk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1694
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1648
    iget-object v0, p0, Lnsj;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1649
    const/4 v0, 0x1

    iget-object v1, p0, Lnsj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1651
    :cond_0
    iget-object v0, p0, Lnsj;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1652
    const/4 v0, 0x2

    iget-object v1, p0, Lnsj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1654
    :cond_1
    iget-object v0, p0, Lnsj;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1655
    const/4 v0, 0x3

    iget-object v1, p0, Lnsj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1657
    :cond_2
    iget-object v0, p0, Lnsj;->e:Lnsk;

    if-eqz v0, :cond_3

    .line 1658
    const/4 v0, 0x4

    iget-object v1, p0, Lnsj;->e:Lnsk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1660
    :cond_3
    iget-object v0, p0, Lnsj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1662
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1557
    invoke-virtual {p0, p1}, Lnsj;->a(Loxn;)Lnsj;

    move-result-object v0

    return-object v0
.end method
