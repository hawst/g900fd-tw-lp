.class final Leiz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Lkxl;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Leiw;


# direct methods
.method constructor <init>(Leiw;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Leiz;->a:Leiw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lkxl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v0, Lkxf;

    iget-object v1, p0, Leiz;->a:Leiw;

    invoke-virtual {v1}, Leiw;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leiz;->a:Leiw;

    iget-object v2, v2, Leiw;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Leiz;->a:Leiw;

    iget-object v3, v3, Leiw;->O:Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, Lfdy;->b:[Ljava/lang/String;

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lkxf;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V

    return-object v0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lkxl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 107
    check-cast p2, Lkxl;

    invoke-virtual {p0, p2}, Leiz;->a(Lkxl;)V

    return-void
.end method

.method public a(Lkxl;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkxl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 119
    :goto_0
    const-string v1, "UnifiedSearchFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    if-nez v0, :cond_2

    const-string v1, "null"

    .line 121
    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Communities Loader Finished; cursor.getCount()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    iget-object v1, p0, Leiz;->a:Leiw;

    invoke-static {v1}, Leiw;->a(Leiw;)Lfdy;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfdy;->c(Landroid/database/Cursor;)V

    .line 124
    return-void

    .line 118
    :cond_1
    invoke-virtual {p1}, Lkxl;->c()Lhym;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1
.end method
