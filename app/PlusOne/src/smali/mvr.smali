.class public final Lmvr;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lmuu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14017
    invoke-direct {p0}, Loxq;-><init>()V

    .line 14020
    sget-object v0, Lmuu;->a:[Lmuu;

    iput-object v0, p0, Lmvr;->a:[Lmuu;

    .line 14017
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 14038
    .line 14039
    iget-object v1, p0, Lmvr;->a:[Lmuu;

    if-eqz v1, :cond_1

    .line 14040
    iget-object v2, p0, Lmvr;->a:[Lmuu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 14041
    if-eqz v4, :cond_0

    .line 14042
    const/4 v5, 0x1

    .line 14043
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 14040
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 14047
    :cond_1
    iget-object v1, p0, Lmvr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14048
    iput v0, p0, Lmvr;->ai:I

    .line 14049
    return v0
.end method

.method public a(Loxn;)Lmvr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 14057
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 14058
    sparse-switch v0, :sswitch_data_0

    .line 14062
    iget-object v2, p0, Lmvr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 14063
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmvr;->ah:Ljava/util/List;

    .line 14066
    :cond_1
    iget-object v2, p0, Lmvr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14068
    :sswitch_0
    return-object p0

    .line 14073
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 14074
    iget-object v0, p0, Lmvr;->a:[Lmuu;

    if-nez v0, :cond_3

    move v0, v1

    .line 14075
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmuu;

    .line 14076
    iget-object v3, p0, Lmvr;->a:[Lmuu;

    if-eqz v3, :cond_2

    .line 14077
    iget-object v3, p0, Lmvr;->a:[Lmuu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14079
    :cond_2
    iput-object v2, p0, Lmvr;->a:[Lmuu;

    .line 14080
    :goto_2
    iget-object v2, p0, Lmvr;->a:[Lmuu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 14081
    iget-object v2, p0, Lmvr;->a:[Lmuu;

    new-instance v3, Lmuu;

    invoke-direct {v3}, Lmuu;-><init>()V

    aput-object v3, v2, v0

    .line 14082
    iget-object v2, p0, Lmvr;->a:[Lmuu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 14083
    invoke-virtual {p1}, Loxn;->a()I

    .line 14080
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14074
    :cond_3
    iget-object v0, p0, Lmvr;->a:[Lmuu;

    array-length v0, v0

    goto :goto_1

    .line 14086
    :cond_4
    iget-object v2, p0, Lmvr;->a:[Lmuu;

    new-instance v3, Lmuu;

    invoke-direct {v3}, Lmuu;-><init>()V

    aput-object v3, v2, v0

    .line 14087
    iget-object v2, p0, Lmvr;->a:[Lmuu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 14058
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 14025
    iget-object v0, p0, Lmvr;->a:[Lmuu;

    if-eqz v0, :cond_1

    .line 14026
    iget-object v1, p0, Lmvr;->a:[Lmuu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 14027
    if-eqz v3, :cond_0

    .line 14028
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 14026
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14032
    :cond_1
    iget-object v0, p0, Lmvr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 14034
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14013
    invoke-virtual {p0, p1}, Lmvr;->a(Loxn;)Lmvr;

    move-result-object v0

    return-object v0
.end method
