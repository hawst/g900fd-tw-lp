.class public final Logq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8872
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8898
    const/4 v0, 0x0

    .line 8899
    iget-object v1, p0, Logq;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8900
    const/4 v0, 0x1

    iget-object v1, p0, Logq;->b:Ljava/lang/String;

    .line 8901
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8903
    :cond_0
    iget-object v1, p0, Logq;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 8904
    const/4 v1, 0x2

    iget-object v2, p0, Logq;->a:Ljava/lang/Boolean;

    .line 8905
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8907
    :cond_1
    iget-object v1, p0, Logq;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 8908
    const/4 v1, 0x3

    iget-object v2, p0, Logq;->c:Ljava/lang/String;

    .line 8909
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8911
    :cond_2
    iget-object v1, p0, Logq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8912
    iput v0, p0, Logq;->ai:I

    .line 8913
    return v0
.end method

.method public a(Loxn;)Logq;
    .locals 2

    .prologue
    .line 8921
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8922
    sparse-switch v0, :sswitch_data_0

    .line 8926
    iget-object v1, p0, Logq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8927
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Logq;->ah:Ljava/util/List;

    .line 8930
    :cond_1
    iget-object v1, p0, Logq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8932
    :sswitch_0
    return-object p0

    .line 8937
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logq;->b:Ljava/lang/String;

    goto :goto_0

    .line 8941
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Logq;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 8945
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logq;->c:Ljava/lang/String;

    goto :goto_0

    .line 8922
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8883
    iget-object v0, p0, Logq;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 8884
    const/4 v0, 0x1

    iget-object v1, p0, Logq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8886
    :cond_0
    iget-object v0, p0, Logq;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 8887
    const/4 v0, 0x2

    iget-object v1, p0, Logq;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8889
    :cond_1
    iget-object v0, p0, Logq;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 8890
    const/4 v0, 0x3

    iget-object v1, p0, Logq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8892
    :cond_2
    iget-object v0, p0, Logq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8894
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8868
    invoke-virtual {p0, p1}, Logq;->a(Loxn;)Logq;

    move-result-object v0

    return-object v0
.end method
