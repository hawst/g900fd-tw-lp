.class public final Lntt;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnry;

.field public b:Lnsr;

.field private c:Lnsg;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 744
    invoke-direct {p0}, Loxq;-><init>()V

    .line 747
    sget-object v0, Lnry;->a:[Lnry;

    iput-object v0, p0, Lntt;->a:[Lnry;

    .line 750
    iput-object v1, p0, Lntt;->b:Lnsr;

    .line 753
    iput-object v1, p0, Lntt;->c:Lnsg;

    .line 744
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 777
    .line 778
    iget-object v1, p0, Lntt;->a:[Lnry;

    if-eqz v1, :cond_1

    .line 779
    iget-object v2, p0, Lntt;->a:[Lnry;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 780
    if-eqz v4, :cond_0

    .line 781
    const/4 v5, 0x1

    .line 782
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 779
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 786
    :cond_1
    iget-object v1, p0, Lntt;->b:Lnsr;

    if-eqz v1, :cond_2

    .line 787
    const/4 v1, 0x2

    iget-object v2, p0, Lntt;->b:Lnsr;

    .line 788
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 790
    :cond_2
    iget-object v1, p0, Lntt;->c:Lnsg;

    if-eqz v1, :cond_3

    .line 791
    const/4 v1, 0x3

    iget-object v2, p0, Lntt;->c:Lnsg;

    .line 792
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 794
    :cond_3
    iget-object v1, p0, Lntt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 795
    iput v0, p0, Lntt;->ai:I

    .line 796
    return v0
.end method

.method public a(Loxn;)Lntt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 804
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 805
    sparse-switch v0, :sswitch_data_0

    .line 809
    iget-object v2, p0, Lntt;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 810
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lntt;->ah:Ljava/util/List;

    .line 813
    :cond_1
    iget-object v2, p0, Lntt;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 815
    :sswitch_0
    return-object p0

    .line 820
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 821
    iget-object v0, p0, Lntt;->a:[Lnry;

    if-nez v0, :cond_3

    move v0, v1

    .line 822
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnry;

    .line 823
    iget-object v3, p0, Lntt;->a:[Lnry;

    if-eqz v3, :cond_2

    .line 824
    iget-object v3, p0, Lntt;->a:[Lnry;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 826
    :cond_2
    iput-object v2, p0, Lntt;->a:[Lnry;

    .line 827
    :goto_2
    iget-object v2, p0, Lntt;->a:[Lnry;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 828
    iget-object v2, p0, Lntt;->a:[Lnry;

    new-instance v3, Lnry;

    invoke-direct {v3}, Lnry;-><init>()V

    aput-object v3, v2, v0

    .line 829
    iget-object v2, p0, Lntt;->a:[Lnry;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 830
    invoke-virtual {p1}, Loxn;->a()I

    .line 827
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 821
    :cond_3
    iget-object v0, p0, Lntt;->a:[Lnry;

    array-length v0, v0

    goto :goto_1

    .line 833
    :cond_4
    iget-object v2, p0, Lntt;->a:[Lnry;

    new-instance v3, Lnry;

    invoke-direct {v3}, Lnry;-><init>()V

    aput-object v3, v2, v0

    .line 834
    iget-object v2, p0, Lntt;->a:[Lnry;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 838
    :sswitch_2
    iget-object v0, p0, Lntt;->b:Lnsr;

    if-nez v0, :cond_5

    .line 839
    new-instance v0, Lnsr;

    invoke-direct {v0}, Lnsr;-><init>()V

    iput-object v0, p0, Lntt;->b:Lnsr;

    .line 841
    :cond_5
    iget-object v0, p0, Lntt;->b:Lnsr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 845
    :sswitch_3
    iget-object v0, p0, Lntt;->c:Lnsg;

    if-nez v0, :cond_6

    .line 846
    new-instance v0, Lnsg;

    invoke-direct {v0}, Lnsg;-><init>()V

    iput-object v0, p0, Lntt;->c:Lnsg;

    .line 848
    :cond_6
    iget-object v0, p0, Lntt;->c:Lnsg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 805
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 758
    iget-object v0, p0, Lntt;->a:[Lnry;

    if-eqz v0, :cond_1

    .line 759
    iget-object v1, p0, Lntt;->a:[Lnry;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 760
    if-eqz v3, :cond_0

    .line 761
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 759
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 765
    :cond_1
    iget-object v0, p0, Lntt;->b:Lnsr;

    if-eqz v0, :cond_2

    .line 766
    const/4 v0, 0x2

    iget-object v1, p0, Lntt;->b:Lnsr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 768
    :cond_2
    iget-object v0, p0, Lntt;->c:Lnsg;

    if-eqz v0, :cond_3

    .line 769
    const/4 v0, 0x3

    iget-object v1, p0, Lntt;->c:Lnsg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 771
    :cond_3
    iget-object v0, p0, Lntt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 773
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 740
    invoke-virtual {p0, p1}, Lntt;->a(Loxn;)Lntt;

    move-result-object v0

    return-object v0
.end method
