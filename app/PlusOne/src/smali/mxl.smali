.class public final Lmxl;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmxm;

.field private b:[Lmxn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 133
    sget-object v0, Lmxn;->a:[Lmxn;

    iput-object v0, p0, Lmxl;->b:[Lmxn;

    .line 136
    sget-object v0, Lmxm;->a:[Lmxm;

    iput-object v0, p0, Lmxl;->a:[Lmxm;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 161
    .line 162
    iget-object v0, p0, Lmxl;->b:[Lmxn;

    if-eqz v0, :cond_1

    .line 163
    iget-object v3, p0, Lmxl;->b:[Lmxn;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 164
    if-eqz v5, :cond_0

    .line 165
    const/4 v6, 0x1

    .line 166
    invoke-static {v6}, Loxo;->k(I)I

    move-result v6

    shl-int/lit8 v6, v6, 0x1

    invoke-virtual {v5}, Loxu;->a()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v0, v5

    .line 163
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 170
    :cond_2
    iget-object v2, p0, Lmxl;->a:[Lmxm;

    if-eqz v2, :cond_4

    .line 171
    iget-object v2, p0, Lmxl;->a:[Lmxm;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 172
    if-eqz v4, :cond_3

    .line 173
    const/4 v5, 0x3

    .line 174
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 171
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 178
    :cond_4
    iget-object v1, p0, Lmxl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    iput v0, p0, Lmxl;->ai:I

    .line 180
    return v0
.end method

.method public a(Loxn;)Lmxl;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 188
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 189
    sparse-switch v0, :sswitch_data_0

    .line 193
    iget-object v2, p0, Lmxl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 194
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmxl;->ah:Ljava/util/List;

    .line 197
    :cond_1
    iget-object v2, p0, Lmxl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    :sswitch_0
    return-object p0

    .line 204
    :sswitch_1
    const/16 v0, 0xb

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 205
    iget-object v0, p0, Lmxl;->b:[Lmxn;

    if-nez v0, :cond_3

    move v0, v1

    .line 206
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmxn;

    .line 207
    iget-object v3, p0, Lmxl;->b:[Lmxn;

    if-eqz v3, :cond_2

    .line 208
    iget-object v3, p0, Lmxl;->b:[Lmxn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 210
    :cond_2
    iput-object v2, p0, Lmxl;->b:[Lmxn;

    .line 211
    :goto_2
    iget-object v2, p0, Lmxl;->b:[Lmxn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 212
    iget-object v2, p0, Lmxl;->b:[Lmxn;

    new-instance v3, Lmxn;

    invoke-direct {v3}, Lmxn;-><init>()V

    aput-object v3, v2, v0

    .line 213
    iget-object v2, p0, Lmxl;->b:[Lmxn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2, v4}, Loxn;->a(Loxu;I)V

    .line 214
    invoke-virtual {p1}, Loxn;->a()I

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 205
    :cond_3
    iget-object v0, p0, Lmxl;->b:[Lmxn;

    array-length v0, v0

    goto :goto_1

    .line 217
    :cond_4
    iget-object v2, p0, Lmxl;->b:[Lmxn;

    new-instance v3, Lmxn;

    invoke-direct {v3}, Lmxn;-><init>()V

    aput-object v3, v2, v0

    .line 218
    iget-object v2, p0, Lmxl;->b:[Lmxn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v4}, Loxn;->a(Loxu;I)V

    goto :goto_0

    .line 222
    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 223
    iget-object v0, p0, Lmxl;->a:[Lmxm;

    if-nez v0, :cond_6

    move v0, v1

    .line 224
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmxm;

    .line 225
    iget-object v3, p0, Lmxl;->a:[Lmxm;

    if-eqz v3, :cond_5

    .line 226
    iget-object v3, p0, Lmxl;->a:[Lmxm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 228
    :cond_5
    iput-object v2, p0, Lmxl;->a:[Lmxm;

    .line 229
    :goto_4
    iget-object v2, p0, Lmxl;->a:[Lmxm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 230
    iget-object v2, p0, Lmxl;->a:[Lmxm;

    new-instance v3, Lmxm;

    invoke-direct {v3}, Lmxm;-><init>()V

    aput-object v3, v2, v0

    .line 231
    iget-object v2, p0, Lmxl;->a:[Lmxm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 232
    invoke-virtual {p1}, Loxn;->a()I

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 223
    :cond_6
    iget-object v0, p0, Lmxl;->a:[Lmxm;

    array-length v0, v0

    goto :goto_3

    .line 235
    :cond_7
    iget-object v2, p0, Lmxl;->a:[Lmxm;

    new-instance v3, Lmxm;

    invoke-direct {v3}, Lmxm;-><init>()V

    aput-object v3, v2, v0

    .line 236
    iget-object v2, p0, Lmxl;->a:[Lmxm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 189
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 141
    iget-object v1, p0, Lmxl;->b:[Lmxn;

    if-eqz v1, :cond_1

    .line 142
    iget-object v2, p0, Lmxl;->b:[Lmxn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 143
    if-eqz v4, :cond_0

    .line 144
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILoxu;)V

    .line 142
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 148
    :cond_1
    iget-object v1, p0, Lmxl;->a:[Lmxm;

    if-eqz v1, :cond_3

    .line 149
    iget-object v1, p0, Lmxl;->a:[Lmxm;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 150
    if-eqz v3, :cond_2

    .line 151
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 149
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 155
    :cond_3
    iget-object v0, p0, Lmxl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 157
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmxl;->a(Loxn;)Lmxl;

    move-result-object v0

    return-object v0
.end method
