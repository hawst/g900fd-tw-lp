.class public final Lmbo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojh;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10447
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10450
    iput-object v0, p0, Lmbo;->apiHeader:Llyq;

    .line 10453
    iput-object v0, p0, Lmbo;->a:Lojh;

    .line 10447
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10470
    const/4 v0, 0x0

    .line 10471
    iget-object v1, p0, Lmbo;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 10472
    const/4 v0, 0x1

    iget-object v1, p0, Lmbo;->apiHeader:Llyq;

    .line 10473
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10475
    :cond_0
    iget-object v1, p0, Lmbo;->a:Lojh;

    if-eqz v1, :cond_1

    .line 10476
    const/4 v1, 0x2

    iget-object v2, p0, Lmbo;->a:Lojh;

    .line 10477
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10479
    :cond_1
    iget-object v1, p0, Lmbo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10480
    iput v0, p0, Lmbo;->ai:I

    .line 10481
    return v0
.end method

.method public a(Loxn;)Lmbo;
    .locals 2

    .prologue
    .line 10489
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10490
    sparse-switch v0, :sswitch_data_0

    .line 10494
    iget-object v1, p0, Lmbo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10495
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbo;->ah:Ljava/util/List;

    .line 10498
    :cond_1
    iget-object v1, p0, Lmbo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10500
    :sswitch_0
    return-object p0

    .line 10505
    :sswitch_1
    iget-object v0, p0, Lmbo;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 10506
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmbo;->apiHeader:Llyq;

    .line 10508
    :cond_2
    iget-object v0, p0, Lmbo;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10512
    :sswitch_2
    iget-object v0, p0, Lmbo;->a:Lojh;

    if-nez v0, :cond_3

    .line 10513
    new-instance v0, Lojh;

    invoke-direct {v0}, Lojh;-><init>()V

    iput-object v0, p0, Lmbo;->a:Lojh;

    .line 10515
    :cond_3
    iget-object v0, p0, Lmbo;->a:Lojh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10490
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10458
    iget-object v0, p0, Lmbo;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 10459
    const/4 v0, 0x1

    iget-object v1, p0, Lmbo;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10461
    :cond_0
    iget-object v0, p0, Lmbo;->a:Lojh;

    if-eqz v0, :cond_1

    .line 10462
    const/4 v0, 0x2

    iget-object v1, p0, Lmbo;->a:Lojh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10464
    :cond_1
    iget-object v0, p0, Lmbo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10466
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10443
    invoke-virtual {p0, p1}, Lmbo;->a(Loxn;)Lmbo;

    move-result-object v0

    return-object v0
.end method
