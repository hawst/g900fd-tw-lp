.class public final Lpjo;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lpjk;

.field private c:Lpjk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1185
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1190
    iput-object v0, p0, Lpjo;->b:Lpjk;

    .line 1193
    iput-object v0, p0, Lpjo;->c:Lpjk;

    .line 1185
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1213
    const/4 v0, 0x0

    .line 1214
    iget-object v1, p0, Lpjo;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1215
    const/4 v0, 0x1

    iget-object v1, p0, Lpjo;->a:Ljava/lang/String;

    .line 1216
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1218
    :cond_0
    iget-object v1, p0, Lpjo;->b:Lpjk;

    if-eqz v1, :cond_1

    .line 1219
    const/4 v1, 0x2

    iget-object v2, p0, Lpjo;->b:Lpjk;

    .line 1220
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1222
    :cond_1
    iget-object v1, p0, Lpjo;->c:Lpjk;

    if-eqz v1, :cond_2

    .line 1223
    const/4 v1, 0x3

    iget-object v2, p0, Lpjo;->c:Lpjk;

    .line 1224
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1226
    :cond_2
    iget-object v1, p0, Lpjo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1227
    iput v0, p0, Lpjo;->ai:I

    .line 1228
    return v0
.end method

.method public a(Loxn;)Lpjo;
    .locals 2

    .prologue
    .line 1236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1237
    sparse-switch v0, :sswitch_data_0

    .line 1241
    iget-object v1, p0, Lpjo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1242
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjo;->ah:Ljava/util/List;

    .line 1245
    :cond_1
    iget-object v1, p0, Lpjo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1247
    :sswitch_0
    return-object p0

    .line 1252
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjo;->a:Ljava/lang/String;

    goto :goto_0

    .line 1256
    :sswitch_2
    iget-object v0, p0, Lpjo;->b:Lpjk;

    if-nez v0, :cond_2

    .line 1257
    new-instance v0, Lpjk;

    invoke-direct {v0}, Lpjk;-><init>()V

    iput-object v0, p0, Lpjo;->b:Lpjk;

    .line 1259
    :cond_2
    iget-object v0, p0, Lpjo;->b:Lpjk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1263
    :sswitch_3
    iget-object v0, p0, Lpjo;->c:Lpjk;

    if-nez v0, :cond_3

    .line 1264
    new-instance v0, Lpjk;

    invoke-direct {v0}, Lpjk;-><init>()V

    iput-object v0, p0, Lpjo;->c:Lpjk;

    .line 1266
    :cond_3
    iget-object v0, p0, Lpjo;->c:Lpjk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1237
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1198
    iget-object v0, p0, Lpjo;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1199
    const/4 v0, 0x1

    iget-object v1, p0, Lpjo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1201
    :cond_0
    iget-object v0, p0, Lpjo;->b:Lpjk;

    if-eqz v0, :cond_1

    .line 1202
    const/4 v0, 0x2

    iget-object v1, p0, Lpjo;->b:Lpjk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1204
    :cond_1
    iget-object v0, p0, Lpjo;->c:Lpjk;

    if-eqz v0, :cond_2

    .line 1205
    const/4 v0, 0x3

    iget-object v1, p0, Lpjo;->c:Lpjk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1207
    :cond_2
    iget-object v0, p0, Lpjo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1209
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1181
    invoke-virtual {p0, p1}, Lpjo;->a(Loxn;)Lpjo;

    move-result-object v0

    return-object v0
.end method
