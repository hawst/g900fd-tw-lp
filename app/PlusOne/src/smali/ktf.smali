.class public final Lktf;
.super Lllq;
.source "PG"


# direct methods
.method public static a([B)Lnse;
    .locals 6

    .prologue
    .line 41
    if-nez p0, :cond_1

    .line 42
    const/4 v0, 0x0

    .line 45
    :cond_0
    return-object v0

    .line 44
    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 45
    new-instance v0, Lnse;

    invoke-direct {v0}, Lnse;-><init>()V

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    new-array v1, v3, [Lnsd;

    iput-object v1, v0, Lnse;->a:[Lnsd;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    new-instance v4, Lnsd;

    invoke-direct {v4}, Lnsd;-><init>()V

    invoke-static {v2}, Lktf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lnsd;->b:Ljava/lang/String;

    invoke-static {v2}, Lktf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lnsd;->c:Ljava/lang/String;

    invoke-static {v2}, Lktf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lnsd;->d:Ljava/lang/String;

    iget-object v5, v0, Lnse;->a:[Lnsd;

    aput-object v4, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a(Lnse;)[B
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 20
    if-nez p0, :cond_0

    .line 21
    new-array v0, v0, [B

    .line 37
    :goto_0
    return-object v0

    .line 24
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 25
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 28
    :try_start_0
    iget-object v3, p0, Lnse;->a:[Lnsd;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 29
    iget-object v3, p0, Lnse;->a:[Lnsd;

    array-length v4, v3

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 30
    iget-object v6, v5, Lnsd;->b:Ljava/lang/String;

    invoke-static {v2, v6}, Lktf;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 31
    iget-object v6, v5, Lnsd;->c:Ljava/lang/String;

    invoke-static {v2, v6}, Lktf;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 32
    iget-object v5, v5, Lnsd;->d:Ljava/lang/String;

    invoke-static {v2, v5}, Lktf;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 35
    :cond_1
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 37
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto :goto_0

    .line 35
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    throw v0
.end method
