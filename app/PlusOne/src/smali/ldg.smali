.class public final Lldg;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;


# static fields
.field public static a:Llct;


# instance fields
.field private b:Lkzu;

.field private c:Landroid/text/StaticLayout;

.field private d:Landroid/text/StaticLayout;

.field private e:Landroid/text/StaticLayout;

.field private f:Landroid/text/StaticLayout;

.field private g:Lizu;

.field private h:Lcom/google/android/libraries/social/resources/images/ImageResource;

.field private i:Lizu;

.field private j:Lcom/google/android/libraries/social/resources/images/ImageResource;

.field private k:Landroid/graphics/Rect;

.field private l:Landroid/graphics/Rect;

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 86
    sget-object v0, Lldg;->a:Llct;

    if-nez v0, :cond_0

    .line 87
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lldg;->a:Llct;

    .line 89
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lldg;->l:Landroid/graphics/Rect;

    .line 90
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lldg;->k:Landroid/graphics/Rect;

    .line 91
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 141
    invoke-virtual {p0}, Lldg;->c()V

    .line 143
    iput-object v0, p0, Lldg;->b:Lkzu;

    .line 145
    iput-object v0, p0, Lldg;->c:Landroid/text/StaticLayout;

    .line 146
    iput-object v0, p0, Lldg;->d:Landroid/text/StaticLayout;

    .line 147
    iput-object v0, p0, Lldg;->e:Landroid/text/StaticLayout;

    .line 148
    iput-object v0, p0, Lldg;->f:Landroid/text/StaticLayout;

    .line 150
    iput-object v0, p0, Lldg;->g:Lizu;

    .line 151
    iput-object v0, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 152
    iput-object v0, p0, Lldg;->i:Lizu;

    .line 153
    iput-object v0, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 154
    iget-object v0, p0, Lldg;->l:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 155
    iget-object v0, p0, Lldg;->k:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 157
    iput v1, p0, Lldg;->m:I

    .line 158
    iput v1, p0, Lldg;->n:I

    .line 159
    iput v1, p0, Lldg;->o:I

    .line 160
    iput v1, p0, Lldg;->p:I

    .line 161
    iput v1, p0, Lldg;->q:I

    .line 162
    iput v1, p0, Lldg;->r:I

    .line 163
    iput v1, p0, Lldg;->s:I

    .line 164
    iput v1, p0, Lldg;->t:I

    .line 165
    return-void
.end method

.method public a(Lkda;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 404
    iget-object v0, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 405
    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getStatus()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 406
    invoke-virtual {p0}, Lldg;->invalidate()V

    .line 408
    :cond_0
    iget-object v0, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 409
    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getStatus()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 410
    invoke-virtual {p0}, Lldg;->invalidate()V

    .line 412
    :cond_1
    return-void
.end method

.method public a(Lkzu;I)V
    .locals 4

    .prologue
    .line 94
    if-nez p1, :cond_0

    .line 114
    :goto_0
    return-void

    .line 98
    :cond_0
    div-int/lit8 v0, p2, 0x4

    sget-object v1, Lldg;->a:Llct;

    iget v1, v1, Llct;->aY:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lldg;->o:I

    .line 100
    iput-object p1, p0, Lldg;->b:Lkzu;

    .line 101
    iget-object v0, p0, Lldg;->b:Lkzu;

    invoke-virtual {v0}, Lkzu;->d()Ljava/lang/String;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lldg;->b:Lkzu;

    invoke-virtual {v1}, Lkzu;->c()Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 105
    invoke-virtual {p0}, Lldg;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v0, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lldg;->g:Lizu;

    .line 108
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 109
    invoke-virtual {p0}, Lldg;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v0, v1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lldg;->i:Lizu;

    .line 111
    :cond_2
    invoke-virtual {p0}, Lldg;->requestLayout()V

    .line 113
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 378
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lldg;->g:Lizu;

    if-eqz v0, :cond_0

    .line 380
    sget-object v0, Lldg;->a:Llct;

    iget-object v0, v0, Llct;->d:Lizs;

    iget-object v1, p0, Lldg;->g:Lizu;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p0}, Lizs;->a(Lizu;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 383
    :cond_0
    iget-object v0, p0, Lldg;->i:Lizu;

    if-eqz v0, :cond_1

    .line 384
    sget-object v0, Lldg;->a:Llct;

    iget-object v0, v0, Llct;->d:Lizs;

    iget-object v1, p0, Lldg;->i:Lizu;

    iget v2, p0, Lldg;->o:I

    iget v3, p0, Lldg;->o:I

    invoke-virtual {v0, v1, v2, v3, p0}, Lizs;->b(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 388
    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 392
    iget-object v0, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->unregister(Lkdd;)V

    .line 394
    iput-object v1, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 396
    :cond_0
    iget-object v0, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-eqz v0, :cond_1

    .line 397
    iget-object v0, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->unregister(Lkdd;)V

    .line 398
    iput-object v1, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 400
    :cond_1
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 8
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 119
    iget-object v0, p0, Lldg;->b:Lkzu;

    if-nez v0, :cond_0

    .line 120
    const-string v0, ""

    .line 136
    :goto_0
    return-object v0

    .line 123
    :cond_0
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 124
    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lldg;->b:Lkzu;

    invoke-virtual {v2}, Lkzu;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 125
    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lldg;->b:Lkzu;

    invoke-virtual {v2}, Lkzu;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 127
    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lldg;->b:Lkzu;

    .line 128
    invoke-virtual {v2}, Lkzu;->f()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    .line 127
    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 130
    iget-object v1, p0, Lldg;->b:Lkzu;

    invoke-virtual {v1}, Lkzu;->e()I

    move-result v1

    .line 131
    new-array v2, v5, [Ljava/lang/CharSequence;

    .line 132
    invoke-virtual {p0}, Lldg;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110026

    new-array v5, v5, [Ljava/lang/Object;

    .line 134
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 132
    invoke-virtual {v3, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v7

    .line 131
    invoke-static {v0, v2}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 136
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 366
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 367
    invoke-virtual {p0}, Lldg;->b()V

    .line 368
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 372
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 373
    invoke-virtual {p0}, Lldg;->c()V

    .line 374
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 252
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 254
    iget-object v0, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-eqz v0, :cond_2

    .line 261
    iget-object v0, p0, Lldg;->h:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_2

    .line 263
    iget-object v1, p0, Lldg;->k:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lldg;->l:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 264
    :cond_0
    iget-object v1, p0, Lldg;->k:Landroid/graphics/Rect;

    iget v2, p0, Lldg;->n:I

    add-int/lit8 v2, v2, 0x0

    iget v3, p0, Lldg;->m:I

    add-int/lit8 v3, v3, 0x0

    invoke-virtual {v1, v6, v6, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 265
    iget-object v1, p0, Lldg;->k:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v4, v2

    int-to-float v5, v3

    div-float/2addr v4, v5

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v5, v1

    cmpl-float v4, v4, v1

    if-lez v4, :cond_6

    int-to-float v4, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v4, p0, Lldg;->l:Landroid/graphics/Rect;

    sub-int/2addr v2, v1

    invoke-virtual {v4, v1, v6, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 267
    :cond_1
    :goto_0
    iget-object v1, p0, Lldg;->l:Landroid/graphics/Rect;

    iget-object v2, p0, Lldg;->k:Landroid/graphics/Rect;

    sget-object v3, Lldg;->a:Llct;

    iget-object v3, v3, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 273
    :cond_2
    iget v0, p0, Lldg;->r:I

    add-int/lit8 v0, v0, 0x0

    .line 274
    iget v1, p0, Lldg;->p:I

    add-int/lit8 v4, v1, 0x0

    .line 276
    iget-object v1, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-eqz v1, :cond_e

    .line 277
    iget-object v1, p0, Lldg;->j:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 278
    if-eqz v1, :cond_7

    .line 279
    int-to-float v2, v0

    int-to-float v3, v4

    sget-object v5, Lldg;->a:Llct;

    iget-object v5, v5, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 280
    iget v1, p0, Lldg;->o:I

    add-int/2addr v0, v1

    move v6, v0

    .line 288
    :goto_1
    int-to-float v1, v6

    int-to-float v2, v4

    iget v0, p0, Lldg;->s:I

    add-int/2addr v0, v6

    int-to-float v3, v0

    iget v0, p0, Lldg;->t:I

    add-int/2addr v0, v4

    int-to-float v4, v0

    sget-object v0, Lldg;->a:Llct;

    iget-object v5, v0, Llct;->u:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 293
    sget-object v0, Lldg;->a:Llct;

    iget v0, v0, Llct;->m:I

    add-int v1, v6, v0

    .line 294
    iget v0, p0, Lldg;->q:I

    add-int/lit8 v0, v0, 0x0

    .line 295
    iget-object v2, p0, Lldg;->c:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    .line 296
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 297
    iget-object v2, p0, Lldg;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 298
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 299
    iget-object v2, p0, Lldg;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lldg;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 302
    :cond_3
    iget-object v2, p0, Lldg;->d:Landroid/text/StaticLayout;

    if-eqz v2, :cond_4

    .line 303
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 304
    iget-object v2, p0, Lldg;->d:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 305
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 306
    iget-object v2, p0, Lldg;->d:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lldg;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 309
    :cond_4
    iget-object v2, p0, Lldg;->e:Landroid/text/StaticLayout;

    if-eqz v2, :cond_5

    .line 310
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 311
    iget-object v2, p0, Lldg;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 312
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 313
    sget-object v2, Lldg;->a:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Lldg;->a:Llct;

    iget v3, v3, Llct;->k:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 316
    :cond_5
    iget-object v2, p0, Lldg;->b:Lkzu;

    invoke-virtual {v2}, Lkzu;->f()F

    move-result v4

    .line 317
    const/4 v2, 0x0

    cmpl-float v2, v4, v2

    if-lez v2, :cond_b

    .line 319
    iget-object v2, p0, Lldg;->e:Landroid/text/StaticLayout;

    if-eqz v2, :cond_d

    .line 320
    iget-object v2, p0, Lldg;->e:Landroid/text/StaticLayout;

    .line 321
    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lldg;->a:Llct;

    iget-object v3, v3, Llct;->aM:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    .line 324
    :goto_2
    sget-object v3, Lldg;->a:Llct;

    iget v3, v3, Llct;->aT:I

    sget-object v5, Lldg;->a:Llct;

    iget-object v5, v5, Llct;->aM:Landroid/graphics/Bitmap;

    .line 325
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int/2addr v5, v3

    move v3, v1

    move v1, v7

    .line 326
    :goto_3
    const/high16 v6, 0x40a00000    # 5.0f

    cmpg-float v6, v1, v6

    if-gtz v6, :cond_a

    .line 327
    cmpl-float v6, v4, v1

    if-lez v6, :cond_8

    .line 328
    sget-object v6, Lldg;->a:Llct;

    iget-object v6, v6, Llct;->aM:Landroid/graphics/Bitmap;

    int-to-float v8, v3

    int-to-float v9, v2

    sget-object v10, Lldg;->a:Llct;

    iget-object v10, v10, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 337
    :goto_4
    add-int/2addr v3, v5

    .line 326
    add-float/2addr v1, v7

    goto :goto_3

    .line 265
    :cond_6
    int-to-float v4, v2

    div-float v1, v4, v1

    float-to-int v1, v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v4, p0, Lldg;->l:Landroid/graphics/Rect;

    sub-int/2addr v3, v1

    invoke-virtual {v4, v6, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 285
    :cond_7
    iget v1, p0, Lldg;->o:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    move v6, v0

    goto/16 :goto_1

    .line 330
    :cond_8
    const/high16 v6, 0x3f000000    # 0.5f

    sub-float v6, v1, v6

    cmpl-float v6, v4, v6

    if-lez v6, :cond_9

    .line 331
    sget-object v6, Lldg;->a:Llct;

    iget-object v6, v6, Llct;->aN:Landroid/graphics/Bitmap;

    int-to-float v8, v3

    int-to-float v9, v2

    sget-object v10, Lldg;->a:Llct;

    iget-object v10, v10, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_4

    .line 334
    :cond_9
    sget-object v6, Lldg;->a:Llct;

    iget-object v6, v6, Llct;->aO:Landroid/graphics/Bitmap;

    int-to-float v8, v3

    int-to-float v9, v2

    sget-object v10, Lldg;->a:Llct;

    iget-object v10, v10, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_4

    .line 340
    :cond_a
    sget-object v1, Lldg;->a:Llct;

    iget v1, v1, Llct;->aT:I

    add-int/2addr v1, v3

    .line 342
    :cond_b
    iget-object v2, p0, Lldg;->f:Landroid/text/StaticLayout;

    if-eqz v2, :cond_c

    .line 343
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 344
    iget-object v2, p0, Lldg;->f:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 345
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 346
    iget-object v0, p0, Lldg;->f:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    sget-object v0, Lldg;->a:Llct;

    iget v0, v0, Llct;->l:I

    .line 348
    :cond_c
    return-void

    :cond_d
    move v2, v0

    goto/16 :goto_2

    :cond_e
    move v6, v0

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 170
    invoke-virtual {p0}, Lldg;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 171
    invoke-virtual {p0}, Lldg;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 172
    iget-object v0, p0, Lldg;->b:Lkzu;

    invoke-virtual {v0}, Lkzu;->a()Ljava/lang/String;

    move-result-object v5

    .line 173
    iget-object v0, p0, Lldg;->b:Lkzu;

    invoke-virtual {v0}, Lkzu;->b()Ljava/lang/String;

    move-result-object v6

    .line 174
    iget-object v0, p0, Lldg;->b:Lkzu;

    invoke-virtual {v0}, Lkzu;->e()I

    move-result v7

    .line 175
    iget-object v0, p0, Lldg;->b:Lkzu;

    invoke-virtual {v0}, Lkzu;->f()F

    move-result v8

    .line 180
    sget-object v0, Lldg;->a:Llct;

    iget v0, v0, Llct;->aU:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    .line 181
    iget-object v9, p0, Lldg;->i:Lizu;

    if-eqz v9, :cond_0

    .line 182
    iget v9, p0, Lldg;->o:I

    sub-int/2addr v0, v9

    .line 184
    :cond_0
    sget-object v9, Lldg;->a:Llct;

    iget v9, v9, Llct;->aX:I

    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lldg;->s:I

    .line 185
    iget v0, p0, Lldg;->o:I

    iput v0, p0, Lldg;->t:I

    .line 186
    iget v0, p0, Lldg;->s:I

    sget-object v9, Lldg;->a:Llct;

    iget v9, v9, Llct;->m:I

    mul-int/lit8 v9, v9, 0x2

    sub-int v9, v0, v9

    .line 189
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 190
    const/16 v0, 0x1b

    .line 191
    invoke-static {v3, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    sget-object v10, Lldg;->a:Llct;

    iget v10, v10, Llct;->aP:I

    .line 190
    invoke-static {v0, v5, v9, v10}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lldg;->c:Landroid/text/StaticLayout;

    .line 193
    iget-object v0, p0, Lldg;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v5, Lldg;->a:Llct;

    iget v5, v5, Llct;->l:I

    add-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x0

    .line 196
    :goto_0
    const/16 v5, 0x19

    invoke-static {v3, v5}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    .line 198
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 199
    sget-object v5, Lldg;->a:Llct;

    iget v5, v5, Llct;->aQ:I

    invoke-static {v3, v6, v9, v5}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v5

    iput-object v5, p0, Lldg;->d:Landroid/text/StaticLayout;

    .line 202
    iget-object v5, p0, Lldg;->d:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    sget-object v6, Lldg;->a:Llct;

    iget v6, v6, Llct;->l:I

    add-int/2addr v5, v6

    add-int/2addr v0, v5

    .line 205
    :cond_1
    const/4 v5, 0x0

    cmpl-float v5, v8, v5

    if-lez v5, :cond_2

    .line 207
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v5

    float-to-double v10, v8

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lldg;->a:Llct;

    iget v6, v6, Llct;->aS:I

    .line 206
    invoke-static {v3, v5, v9, v6}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v5

    iput-object v5, p0, Lldg;->e:Landroid/text/StaticLayout;

    .line 211
    :cond_2
    sget-object v5, Lldg;->a:Llct;

    iget v5, v5, Llct;->aT:I

    sget-object v6, Lldg;->a:Llct;

    iget-object v6, v6, Llct;->aM:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    .line 212
    mul-int/lit8 v5, v5, 0x5

    sub-int v5, v9, v5

    .line 214
    if-lez v7, :cond_6

    if-lez v5, :cond_6

    .line 215
    const v6, 0x7f110026

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    .line 218
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v1

    .line 216
    invoke-virtual {v4, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lldg;->a:Llct;

    iget v4, v4, Llct;->aR:I

    .line 215
    invoke-static {v3, v1, v5, v4}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Lldg;->f:Landroid/text/StaticLayout;

    .line 220
    iget-object v1, p0, Lldg;->e:Landroid/text/StaticLayout;

    if-eqz v1, :cond_5

    .line 221
    iget-object v1, p0, Lldg;->e:Landroid/text/StaticLayout;

    .line 222
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    iget-object v3, p0, Lldg;->f:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    .line 221
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_3
    :goto_1
    iget v1, p0, Lldg;->o:I

    sget-object v3, Lldg;->a:Llct;

    iget v3, v3, Llct;->aV:I

    add-int/2addr v1, v3

    sget-object v3, Lldg;->a:Llct;

    iget v3, v3, Llct;->aW:I

    add-int/2addr v1, v3

    .line 235
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lldg;->m:I

    .line 236
    iput v2, p0, Lldg;->n:I

    .line 239
    iget v1, p0, Lldg;->m:I

    iget v3, p0, Lldg;->o:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lldg;->p:I

    .line 240
    iget v1, p0, Lldg;->m:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lldg;->q:I

    .line 241
    iget v0, p0, Lldg;->s:I

    sub-int v0, v2, v0

    .line 242
    iget-object v1, p0, Lldg;->i:Lizu;

    if-eqz v1, :cond_4

    .line 243
    iget v1, p0, Lldg;->o:I

    sub-int/2addr v0, v1

    .line 245
    :cond_4
    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lldg;->r:I

    .line 247
    iget v0, p0, Lldg;->m:I

    invoke-virtual {p0, v2, v0}, Lldg;->setMeasuredDimension(II)V

    .line 248
    return-void

    .line 224
    :cond_5
    iget-object v1, p0, Lldg;->f:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    .line 226
    :cond_6
    iget-object v1, p0, Lldg;->e:Landroid/text/StaticLayout;

    if-eqz v1, :cond_3

    .line 227
    const/4 v1, 0x0

    iput-object v1, p0, Lldg;->f:Landroid/text/StaticLayout;

    .line 228
    iget-object v1, p0, Lldg;->e:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method
