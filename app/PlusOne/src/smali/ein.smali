.class final Lein;
.super Lfhh;
.source "PG"


# instance fields
.field private synthetic a:Lehu;


# direct methods
.method constructor <init>(Lehu;)V
    .locals 0

    .prologue
    .line 2944
    iput-object p1, p0, Lein;->a:Lehu;

    invoke-direct {p0}, Lfhh;-><init>()V

    return-void
.end method

.method private ag(ILfib;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3131
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->as(Lehu;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->as(Lehu;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    move v0, v1

    .line 3238
    :goto_0
    return v0

    .line 3134
    :cond_1
    iget-object v0, p0, Lein;->a:Lehu;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lehu;->a(Lehu;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 3136
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->at(Lehu;)V

    .line 3138
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3140
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->aA(Lehu;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 3217
    const v0, 0x7f0a058e

    .line 3222
    :goto_1
    iget-object v2, p0, Lein;->a:Lehu;

    invoke-static {v2}, Lehu;->aB(Lehu;)Llnl;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 3223
    goto :goto_0

    .line 3142
    :sswitch_0
    const v0, 0x7f0a0797

    .line 3143
    goto :goto_1

    .line 3147
    :sswitch_1
    const v0, 0x7f0a04b3

    .line 3148
    goto :goto_1

    .line 3152
    :sswitch_2
    const v0, 0x7f0a079c

    .line 3153
    goto :goto_1

    .line 3157
    :sswitch_3
    const v0, 0x7f0a079d

    .line 3158
    goto :goto_1

    .line 3162
    :sswitch_4
    const v0, 0x7f0a079e

    .line 3163
    goto :goto_1

    .line 3167
    :sswitch_5
    const v0, 0x7f0a079f

    .line 3168
    goto :goto_1

    .line 3172
    :sswitch_6
    const v0, 0x7f0a07a0

    .line 3173
    goto :goto_1

    .line 3177
    :sswitch_7
    const v0, 0x7f0a07a1

    .line 3178
    goto :goto_1

    .line 3182
    :sswitch_8
    const v0, 0x7f0a0793

    .line 3183
    goto :goto_1

    .line 3187
    :sswitch_9
    const v0, 0x7f0a0795

    .line 3188
    goto :goto_1

    .line 3192
    :sswitch_a
    const v0, 0x7f0a0796

    .line 3193
    goto :goto_1

    .line 3197
    :sswitch_b
    const v0, 0x7f0a07a4

    .line 3198
    goto :goto_1

    .line 3202
    :sswitch_c
    const v0, 0x7f0a0799

    .line 3203
    goto :goto_1

    .line 3207
    :sswitch_d
    const v0, 0x7f0a07af

    .line 3208
    goto :goto_1

    .line 3212
    :sswitch_e
    const v0, 0x7f0a0792

    .line 3213
    goto :goto_1

    .line 3225
    :cond_2
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->aA(Lehu;)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 3238
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 3228
    :sswitch_f
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-virtual {v0}, Lehu;->e()V

    goto :goto_2

    .line 3233
    :sswitch_10
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0, v1}, Lehu;->d(Lehu;Z)Z

    .line 3234
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->ak(Lehu;)Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c()V

    goto :goto_2

    .line 3140
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
        0x12 -> :sswitch_7
        0x13 -> :sswitch_0
        0x14 -> :sswitch_4
        0x15 -> :sswitch_3
        0x16 -> :sswitch_6
        0x17 -> :sswitch_5
        0x20 -> :sswitch_8
        0x21 -> :sswitch_9
        0x22 -> :sswitch_a
        0x2e -> :sswitch_e
        0x30 -> :sswitch_b
        0x31 -> :sswitch_c
        0x41 -> :sswitch_d
    .end sparse-switch

    .line 3225
    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_f
        0x12 -> :sswitch_f
        0x20 -> :sswitch_10
    .end sparse-switch
.end method


# virtual methods
.method public C(ILfib;)V
    .locals 3

    .prologue
    .line 3064
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3065
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->aw(Lehu;)Llnl;

    move-result-object v0

    const v1, 0x7f0a07a2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3067
    :cond_0
    return-void
.end method

.method public D(ILfib;)V
    .locals 3

    .prologue
    .line 3072
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3073
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->ax(Lehu;)Llnl;

    move-result-object v0

    const v1, 0x7f0a07a3

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 3074
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3076
    :cond_0
    return-void
.end method

.method public I(ILfib;)V
    .locals 0

    .prologue
    .line 3036
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 3037
    return-void
.end method

.method public T(ILfib;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2955
    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2956
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->ag(Lehu;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2958
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->ag(Lehu;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2980
    :cond_0
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 2983
    new-instance v0, Lkoe;

    const/16 v1, 0x5c

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lein;->a:Lehu;

    invoke-static {v1}, Lehu;->au(Lehu;)Llnl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 2984
    :goto_0
    return-void

    .line 2961
    :cond_1
    invoke-virtual {p2}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v0

    .line 2962
    const-string v1, "INVALID_ACL_EXPANSION"

    invoke-static {v0, v1}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2963
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->as(Lehu;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 2964
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0, v4}, Lehu;->a(Lehu;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 2965
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->at(Lehu;)V

    .line 2966
    iget-object v0, p0, Lein;->a:Lehu;

    const v1, 0x7f0a02c8

    .line 2967
    invoke-virtual {v0, v1}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lein;->a:Lehu;

    iget-object v0, p0, Lein;->a:Lehu;

    .line 2968
    invoke-static {v0}, Lehu;->p(Lehu;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a02c7

    :goto_1
    invoke-virtual {v2, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lein;->a:Lehu;

    const v3, 0x7f0a0596

    .line 2970
    invoke-virtual {v2, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 2966
    invoke-static {v1, v0, v2, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 2972
    iget-object v1, p0, Lein;->a:Lehu;

    invoke-virtual {v1}, Lehu;->u_()Lu;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lu;I)V

    .line 2973
    iget-object v1, p0, Lein;->a:Lehu;

    invoke-virtual {v1}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 2968
    :cond_2
    const v0, 0x7f0a02c6

    goto :goto_1
.end method

.method public U(ILfib;)V
    .locals 0

    .prologue
    .line 3001
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 3002
    return-void
.end method

.method public V(ILfib;)V
    .locals 0

    .prologue
    .line 3042
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 3043
    return-void
.end method

.method public W(ILfib;)V
    .locals 0

    .prologue
    .line 3048
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 3049
    return-void
.end method

.method public X(ILfib;)V
    .locals 0

    .prologue
    .line 3092
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 3093
    return-void
.end method

.method public Y(ILfib;)V
    .locals 0

    .prologue
    .line 3098
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 3099
    return-void
.end method

.method public a(ILfib;Landroid/text/Spanned;Landroid/text/Spanned;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lfib;",
            "Landroid/text/Spanned;",
            "Landroid/text/Spanned;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/text/Spanned;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3115
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3116
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->i(Lehu;)Lfdr;

    move-result-object v0

    invoke-virtual {v0, p3, p4, p5}, Lfdr;->a(Landroid/text/Spanned;Landroid/text/Spanned;Ljava/util/HashMap;)V

    .line 3118
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->i(Lehu;)Lfdr;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfdr;->b(Z)V

    .line 3120
    :cond_0
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 3121
    return-void
.end method

.method public a(ILhgw;Lfib;)V
    .locals 1

    .prologue
    .line 3081
    invoke-virtual {p3}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 3082
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0, p2}, Lehu;->a(Lehu;Lhgw;)Lhgw;

    .line 3083
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->ay(Lehu;)V

    .line 3086
    :cond_0
    invoke-direct {p0, p1, p3}, Lein;->ag(ILfib;)Z

    .line 3087
    return-void
.end method

.method public a(ILjava/lang/String;ZLfib;)V
    .locals 2

    .prologue
    .line 3104
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3105
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->az(Lehu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p2}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3106
    invoke-direct {p0, p1, p4}, Lein;->ag(ILfib;)Z

    .line 3108
    :cond_0
    return-void
.end method

.method public a(ZLfib;)V
    .locals 3

    .prologue
    .line 3054
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3055
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-static {v0}, Lehu;->av(Lehu;)Llnl;

    move-result-object v1

    if-eqz p1, :cond_1

    const v0, 0x7f0a07a2

    :goto_0
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 3057
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3059
    :cond_0
    return-void

    .line 3055
    :cond_1
    const v0, 0x7f0a07a3

    goto :goto_0
.end method

.method public b(IZLfib;)V
    .locals 3

    .prologue
    .line 3007
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3008
    iget-object v0, p0, Lein;->a:Lehu;

    invoke-virtual {v0}, Lehu;->n()Lz;

    move-result-object v1

    if-eqz p2, :cond_1

    const v0, 0x7f0a079a

    :goto_0
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 3010
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3012
    :cond_0
    invoke-direct {p0, p1, p3}, Lein;->ag(ILfib;)Z

    .line 3013
    return-void

    .line 3008
    :cond_1
    const v0, 0x7f0a079b

    goto :goto_0
.end method

.method public c(IZLfib;)V
    .locals 2

    .prologue
    .line 3018
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3019
    iget-object v1, p0, Lein;->a:Lehu;

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lehu;->j(Lehu;Z)Z

    .line 3021
    :cond_0
    invoke-direct {p0, p1, p3}, Lein;->ag(ILfib;)Z

    .line 3022
    return-void

    .line 3019
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(IZLfib;)V
    .locals 2

    .prologue
    .line 3027
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3028
    iget-object v1, p0, Lein;->a:Lehu;

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lehu;->k(Lehu;Z)Z

    .line 3030
    :cond_0
    invoke-direct {p0, p1, p3}, Lein;->ag(ILfib;)Z

    .line 3031
    return-void

    .line 3028
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p(ILfib;)V
    .locals 0

    .prologue
    .line 2949
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 2950
    return-void
.end method

.method public q(ILfib;)V
    .locals 0

    .prologue
    .line 2989
    invoke-direct {p0, p1, p2}, Lein;->ag(ILfib;)Z

    .line 2990
    return-void
.end method
