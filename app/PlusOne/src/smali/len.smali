.class public final Llen;
.super Llch;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static C:Llct;

.field private static D:I

.field private static E:I

.field private static F:I

.field private static G:I

.field private static H:I

.field private static I:I

.field private static J:I

.field private static K:Landroid/graphics/Bitmap;

.field private static L:Landroid/graphics/Bitmap;

.field private static i:I

.field private static j:I


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private M:I

.field private N:I

.field private O:I

.field private P:Ljava/lang/Runnable;

.field public c:I

.field private d:Landroid/widget/TextView;

.field private e:Llcd;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private y:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Llen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Llch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    new-instance v0, Lleo;

    invoke-direct {v0, p0}, Lleo;-><init>(Llen;)V

    iput-object v0, p0, Llen;->P:Ljava/lang/Runnable;

    .line 96
    sget-object v0, Llen;->C:Llct;

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Llen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 98
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v1

    sput-object v1, Llen;->C:Llct;

    .line 99
    const v1, 0x7f0d01d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Llen;->i:I

    .line 101
    const v1, 0x7f0d01d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Llen;->j:I

    .line 104
    const v1, 0x7f0d01d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Llen;->D:I

    .line 106
    const v1, 0x7f0d01d9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Llen;->E:I

    .line 108
    const v1, 0x7f0d01da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Llen;->F:I

    .line 110
    const v1, 0x7f0d01db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Llen;->G:I

    .line 112
    const v1, 0x7f0d01dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Llen;->H:I

    .line 114
    const v1, 0x7f0d01dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Llen;->I:I

    .line 116
    const v1, 0x7f0d01de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Llen;->J:I

    .line 118
    const v1, 0x7f0205ad

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Llen;->K:Landroid/graphics/Bitmap;

    .line 119
    const v1, 0x7f0205ae

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Llen;->L:Landroid/graphics/Bitmap;

    .line 121
    :cond_0
    return-void
.end method

.method private b(I)I
    .locals 1

    .prologue
    const v0, 0x7f0202b4

    .line 468
    packed-switch p1, :pswitch_data_0

    .line 510
    :goto_0
    :pswitch_0
    return v0

    .line 470
    :pswitch_1
    const v0, 0x7f0202b8

    goto :goto_0

    .line 474
    :pswitch_2
    const v0, 0x7f0202b2

    goto :goto_0

    .line 482
    :pswitch_3
    const v0, 0x7f0202b6

    goto :goto_0

    .line 486
    :pswitch_4
    const v0, 0x7f0202ba

    goto :goto_0

    .line 490
    :pswitch_5
    const v0, 0x7f0202b9

    goto :goto_0

    .line 494
    :pswitch_6
    const v0, 0x7f0202b3

    goto :goto_0

    .line 498
    :pswitch_7
    const v0, 0x7f0202b5

    goto :goto_0

    .line 502
    :pswitch_8
    const v0, 0x7f0202b7

    goto :goto_0

    .line 506
    :pswitch_9
    const v0, 0x7f0202bb

    goto :goto_0

    .line 468
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method protected a(I)I
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 370
    invoke-virtual {p0}, Llen;->m()V

    .line 372
    invoke-virtual {p0}, Llen;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 375
    const/16 v1, 0x1b

    invoke-static {v0, v8, v6, v1}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Llen;->k:Landroid/widget/TextView;

    .line 377
    iget-object v1, p0, Llen;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 378
    iget-object v1, p0, Llen;->k:Landroid/widget/TextView;

    sget-object v2, Llen;->C:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Llen;->C:Llct;

    iget v3, v3, Llct;->m:I

    sget v4, Llen;->D:I

    invoke-virtual {v1, v2, v6, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 380
    iget-object v1, p0, Llen;->k:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Llen;->addView(Landroid/view/View;)V

    .line 382
    const/16 v1, 0xa

    invoke-static {v0, v8, v6, v1}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Llen;->l:Landroid/widget/TextView;

    .line 384
    iget-object v1, p0, Llen;->l:Landroid/widget/TextView;

    sget v2, Llen;->G:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setWidth(I)V

    .line 385
    iget-object v1, p0, Llen;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 386
    iget-object v1, p0, Llen;->l:Landroid/widget/TextView;

    sget-object v2, Llen;->C:Llct;

    iget v2, v2, Llct;->m:I

    sget v3, Llen;->E:I

    sget-object v4, Llen;->C:Llct;

    iget v4, v4, Llct;->m:I

    sget v5, Llen;->F:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 388
    iget-object v1, p0, Llen;->l:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Llen;->addView(Landroid/view/View;)V

    .line 390
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Llen;->y:Landroid/widget/ImageView;

    .line 391
    iget-object v1, p0, Llen;->y:Landroid/widget/ImageView;

    sget-object v2, Llen;->C:Llct;

    iget v2, v2, Llct;->m:I

    sget v3, Llen;->H:I

    sget-object v4, Llen;->C:Llct;

    iget v4, v4, Llct;->m:I

    sget v5, Llen;->I:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 393
    iget-object v1, p0, Llen;->y:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Llen;->addView(Landroid/view/View;)V

    .line 395
    iget-object v1, p0, Llen;->e:Llcd;

    invoke-virtual {v1}, Llcd;->f()I

    move-result v1

    if-ne v1, v7, :cond_1

    .line 396
    const/16 v1, 0xc

    invoke-static {v0, v8, v6, v1}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Llen;->A:Landroid/widget/TextView;

    .line 398
    iget-object v1, p0, Llen;->A:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 399
    iget-object v1, p0, Llen;->A:Landroid/widget/TextView;

    .line 400
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a049e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 399
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    iget-object v1, p0, Llen;->A:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 402
    iget-object v1, p0, Llen;->A:Landroid/widget/TextView;

    sget-object v2, Llen;->C:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Llen;->C:Llct;

    iget v3, v3, Llct;->m:I

    sget v4, Llen;->I:I

    invoke-virtual {v1, v2, v6, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 404
    iget-object v1, p0, Llen;->A:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Llen;->addView(Landroid/view/View;)V

    .line 405
    iget-object v1, p0, Llen;->k:Landroid/widget/TextView;

    .line 406
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a049a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 405
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    iget-object v1, p0, Llen;->l:Landroid/widget/TextView;

    .line 408
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a049b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 407
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    iget-object v0, p0, Llen;->y:Landroid/widget/ImageView;

    sget-object v1, Llen;->K:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 418
    :goto_0
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 420
    iget v0, p0, Llen;->t:I

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 422
    sget v0, Llen;->G:I

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 425
    iget-object v3, p0, Llen;->k:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v1}, Landroid/widget/TextView;->measure(II)V

    .line 426
    iget-object v3, p0, Llen;->k:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, p1

    .line 428
    iget-object v4, p0, Llen;->l:Landroid/widget/TextView;

    invoke-virtual {v4, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 429
    iget-object v0, p0, Llen;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v3

    .line 431
    iget-object v3, p0, Llen;->y:Landroid/widget/ImageView;

    invoke-virtual {v3, v2, v1}, Landroid/widget/ImageView;->measure(II)V

    .line 432
    iget-object v3, p0, Llen;->y:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v0, v3

    .line 433
    iget-object v3, p0, Llen;->e:Llcd;

    invoke-virtual {v3}, Llcd;->f()I

    move-result v3

    if-ne v3, v7, :cond_0

    .line 434
    iget-object v3, p0, Llen;->A:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v1}, Landroid/widget/TextView;->measure(II)V

    .line 435
    iget-object v1, p0, Llen;->A:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 437
    :cond_0
    return v0

    .line 411
    :cond_1
    iget-object v1, p0, Llen;->k:Landroid/widget/TextView;

    .line 412
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a049c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 411
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    iget-object v1, p0, Llen;->l:Landroid/widget/TextView;

    .line 414
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a049d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 413
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    iget-object v0, p0, Llen;->y:Landroid/widget/ImageView;

    sget-object v1, Llen;->L:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected a(II)I
    .locals 9

    .prologue
    .line 252
    invoke-virtual {p0}, Llen;->m()V

    .line 254
    invoke-virtual {p0}, Llen;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 256
    iget-object v0, p0, Llen;->d:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 257
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xd

    invoke-static {v1, v0, v2, v3}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Llen;->d:Landroid/widget/TextView;

    .line 260
    :cond_0
    iget-object v0, p0, Llen;->d:Landroid/widget/TextView;

    iget-object v2, p0, Llen;->e:Llcd;

    invoke-virtual {v2}, Llcd;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    iget-object v0, p0, Llen;->d:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 262
    iget-object v0, p0, Llen;->d:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 263
    iget-object v0, p0, Llen;->d:Landroid/widget/TextView;

    sget-object v2, Llen;->C:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Llen;->C:Llct;

    iget v3, v3, Llct;->m:I

    sget-object v4, Llen;->C:Llct;

    iget v4, v4, Llct;->m:I

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 265
    iget-object v0, p0, Llen;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Llen;->addView(Landroid/view/View;)V

    .line 267
    iget-object v0, p0, Llen;->f:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 268
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-static {v1, v0, v2, v3}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Llen;->f:Landroid/widget/TextView;

    .line 271
    :cond_1
    iget-object v0, p0, Llen;->f:Landroid/widget/TextView;

    iget-object v2, p0, Llen;->e:Llcd;

    invoke-virtual {v2}, Llcd;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v0, p0, Llen;->f:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 273
    iget-object v0, p0, Llen;->f:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 274
    iget-object v0, p0, Llen;->f:Landroid/widget/TextView;

    sget-object v2, Llen;->C:Llct;

    iget v2, v2, Llct;->m:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget v5, Llen;->i:I

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 275
    iget-object v0, p0, Llen;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Llen;->addView(Landroid/view/View;)V

    .line 277
    invoke-virtual {p0}, Llen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 278
    const v2, 0x7f0b011e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 279
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llen;->h:Ljava/util/ArrayList;

    .line 280
    const/4 v0, 0x0

    iget-object v3, p0, Llen;->e:Llcd;

    invoke-virtual {v3}, Llcd;->e()I

    move-result v3

    :goto_0
    if-ge v0, v3, :cond_2

    .line 281
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x13

    invoke-static {v1, v4, v5, v6}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v4

    .line 283
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 284
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 285
    const/16 v5, 0x10

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 286
    sget-object v5, Llen;->C:Llct;

    iget v5, v5, Llct;->m:I

    sget-object v6, Llen;->C:Llct;

    iget v6, v6, Llct;->m:I

    sget-object v7, Llen;->C:Llct;

    iget v7, v7, Llct;->m:I

    sget-object v8, Llen;->C:Llct;

    iget v8, v8, Llct;->m:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 288
    iget-object v5, p0, Llen;->e:Llcd;

    invoke-virtual {v5, v0}, Llcd;->b(I)Llce;

    move-result-object v5

    invoke-virtual {v5}, Llce;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    iget-object v5, p0, Llen;->e:Llcd;

    .line 290
    invoke-virtual {v5, v0}, Llcd;->b(I)Llce;

    move-result-object v5

    invoke-virtual {v5}, Llce;->c()I

    move-result v5

    invoke-direct {p0, v5}, Llen;->b(I)I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 289
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 291
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 292
    sget-object v5, Llen;->C:Llct;

    iget v5, v5, Llct;->m:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 293
    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    iget-object v5, p0, Llen;->h:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    invoke-virtual {p0, v4}, Llen;->addView(Landroid/view/View;)V

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 298
    :cond_2
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    if-nez v0, :cond_3

    .line 299
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-static {v1, v0, v2, v3}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Llen;->g:Landroid/widget/TextView;

    .line 302
    :cond_3
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    iget-object v1, p0, Llen;->e:Llcd;

    invoke-virtual {v1}, Llcd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    sget-object v1, Llen;->C:Llct;

    iget v1, v1, Llct;->m:I

    sget-object v2, Llen;->C:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Llen;->C:Llct;

    iget v3, v3, Llct;->m:I

    sget-object v4, Llen;->C:Llct;

    iget v4, v4, Llct;->m:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 306
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 307
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 308
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Llen;->addView(Landroid/view/View;)V

    .line 310
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 312
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 313
    sget-object v0, Llen;->C:Llct;

    iget v0, v0, Llct;->m:I

    iget-object v1, p0, Llen;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x1

    sub-int v0, p1, v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 316
    iget-object v0, p0, Llen;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v3}, Landroid/widget/TextView;->measure(II)V

    .line 317
    iget-object v0, p0, Llen;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    .line 318
    iget-object v1, p0, Llen;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v3}, Landroid/widget/TextView;->measure(II)V

    .line 319
    iget-object v1, p0, Llen;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    sget v1, Llen;->j:I

    add-int/2addr v1, v0

    .line 321
    const/4 v0, 0x0

    iget-object v2, p0, Llen;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v1, v6, :cond_4

    .line 322
    iget-object v0, p0, Llen;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 323
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 324
    invoke-virtual {v0, v5, v3}, Landroid/widget/TextView;->measure(II)V

    .line 327
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 328
    sget v2, Llen;->j:I

    add-int/2addr v2, v0

    .line 321
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 331
    :cond_4
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v3}, Landroid/widget/TextView;->measure(II)V

    .line 332
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 333
    return v0
.end method

.method protected a(IIII)I
    .locals 2

    .prologue
    .line 134
    iput p2, p0, Llen;->c:I

    .line 135
    iput p1, p0, Llen;->M:I

    .line 139
    iget-object v0, p0, Llen;->e:Llcd;

    invoke-virtual {v0}, Llcd;->f()I

    move-result v0

    if-nez v0, :cond_0

    .line 140
    iget v0, p0, Llen;->c:I

    invoke-virtual {p0, v0}, Llen;->a(I)I

    move-result v0

    iput v0, p0, Llen;->O:I

    .line 141
    invoke-virtual {p0, p1, p2}, Llen;->a(II)I

    move-result v0

    iput v0, p0, Llen;->N:I

    .line 147
    :goto_0
    iget v0, p0, Llen;->N:I

    iget v1, p0, Llen;->O:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    .line 143
    :cond_0
    invoke-virtual {p0, p1, p2}, Llen;->a(II)I

    move-result v0

    iput v0, p0, Llen;->N:I

    .line 144
    iget v0, p0, Llen;->c:I

    invoke-virtual {p0, v0}, Llen;->a(I)I

    move-result v0

    iput v0, p0, Llen;->O:I

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Llen;->e:Llcd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llen;->e:Llcd;

    .line 166
    invoke-virtual {v0}, Llcd;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Llen;->a:Llci;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Llen;->a:Llci;

    iget-object v1, p0, Llen;->e:Llcd;

    iget-object v2, p0, Llen;->r:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Llci;->a(Llcd;Ljava/lang/String;)V

    .line 170
    :cond_0
    invoke-virtual {p0}, Llen;->getHeight()I

    move-result v0

    add-int/2addr v0, p6

    return v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 175
    invoke-super {p0}, Llch;->a()V

    .line 176
    iput-object v0, p0, Llen;->d:Landroid/widget/TextView;

    .line 177
    iput-object v0, p0, Llen;->f:Landroid/widget/TextView;

    .line 178
    iput-object v0, p0, Llen;->g:Landroid/widget/TextView;

    .line 179
    iput-object v0, p0, Llen;->k:Landroid/widget/TextView;

    .line 180
    iput-object v0, p0, Llen;->l:Landroid/widget/TextView;

    .line 181
    iput-object v0, p0, Llen;->y:Landroid/widget/ImageView;

    .line 182
    iput-object v0, p0, Llen;->A:Landroid/widget/TextView;

    .line 183
    iput-object v0, p0, Llen;->B:Landroid/widget/TextView;

    .line 184
    iput-object v0, p0, Llen;->e:Llcd;

    .line 185
    iput-object v0, p0, Llen;->h:Ljava/util/ArrayList;

    .line 186
    iget-object v0, p0, Llen;->P:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 187
    return-void
.end method

.method protected a(Landroid/database/Cursor;Llcr;I)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 125
    invoke-super {p0, p1, p2, p3}, Llch;->a(Landroid/database/Cursor;Llcr;I)V

    .line 127
    const/16 v2, 0x1b

    .line 128
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 127
    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Llen;->e:Llcd;

    .line 129
    return-void

    .line 127
    :cond_0
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    new-instance v2, Llcd;

    invoke-direct {v2}, Llcd;-><init>()V

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    iput v4, v2, Llcd;->a:I

    invoke-static {v3}, Llcd;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Llcd;->b:Ljava/lang/String;

    invoke-static {v3}, Llcd;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Llcd;->c:Ljava/lang/String;

    invoke-static {v3}, Llcd;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Llcd;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    iput v4, v2, Llcd;->e:I

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    if-ne v4, v0, :cond_1

    :goto_1
    iput-boolean v0, v2, Llcd;->f:Z

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, v2, Llcd;->g:Ljava/util/ArrayList;

    :goto_2
    if-ge v1, v0, :cond_2

    iget-object v4, v2, Llcd;->g:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    invoke-static {v3}, Llce;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v8

    new-instance v9, Llce;

    invoke-direct {v9, v5, v6, v7, v8}, Llce;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method protected a(ZIIII)V
    .locals 1

    .prologue
    .line 152
    invoke-super/range {p0 .. p5}, Llch;->a(ZIIII)V

    .line 154
    iget-object v0, p0, Llen;->e:Llcd;

    invoke-virtual {v0}, Llcd;->f()I

    move-result v0

    if-nez v0, :cond_0

    .line 155
    invoke-virtual {p0}, Llen;->f()V

    .line 159
    :goto_0
    return-void

    .line 157
    :cond_0
    invoke-virtual {p0}, Llen;->g()V

    goto :goto_0
.end method

.method protected f()V
    .locals 8

    .prologue
    .line 337
    invoke-virtual {p0}, Llen;->getHeight()I

    move-result v0

    iget v1, p0, Llen;->N:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 338
    iget-object v1, p0, Llen;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 339
    iget-object v2, p0, Llen;->d:Landroid/widget/TextView;

    iget-object v3, p0, Llen;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Llen;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Llen;->d:Landroid/widget/TextView;

    .line 340
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    add-int v5, v0, v1

    .line 339
    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 343
    add-int/2addr v0, v1

    .line 344
    iget-object v1, p0, Llen;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 345
    iget-object v2, p0, Llen;->f:Landroid/widget/TextView;

    iget-object v3, p0, Llen;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Llen;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Llen;->f:Landroid/widget/TextView;

    .line 346
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    add-int v5, v0, v1

    .line 345
    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 349
    add-int/2addr v0, v1

    .line 350
    iget-object v1, p0, Llen;->q:Landroid/graphics/Rect;

    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 351
    sget v1, Llen;->j:I

    add-int/2addr v1, v0

    .line 352
    const/4 v0, 0x0

    iget-object v2, p0, Llen;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 353
    iget-object v0, p0, Llen;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 354
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 355
    sget-object v5, Llen;->C:Llct;

    iget v5, v5, Llct;->m:I

    add-int/2addr v5, v3

    iget v6, p0, Llen;->t:I

    sget-object v7, Llen;->C:Llct;

    iget v7, v7, Llct;->m:I

    add-int/2addr v7, v3

    sub-int/2addr v6, v7

    .line 359
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v2

    .line 357
    invoke-virtual {v0, v5, v2, v6, v7}, Landroid/widget/TextView;->layout(IIII)V

    .line 360
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 361
    sget v2, Llen;->j:I

    add-int/2addr v2, v0

    .line 352
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 364
    :cond_0
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    iget-object v1, p0, Llen;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Llen;->g:Landroid/widget/TextView;

    .line 365
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Llen;->g:Landroid/widget/TextView;

    .line 366
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v2

    .line 364
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 367
    return-void
.end method

.method protected g()V
    .locals 6

    .prologue
    .line 441
    iget-object v0, p0, Llen;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 442
    iget-object v1, p0, Llen;->k:Landroid/widget/TextView;

    iget-object v2, p0, Llen;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sget v3, Llen;->J:I

    iget-object v4, p0, Llen;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Llen;->k:Landroid/widget/TextView;

    .line 443
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    sget v5, Llen;->J:I

    add-int/2addr v5, v0

    .line 442
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 446
    sget v1, Llen;->J:I

    add-int/2addr v0, v1

    .line 447
    iget-object v1, p0, Llen;->l:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 448
    iget-object v2, p0, Llen;->l:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    .line 450
    iget-object v3, p0, Llen;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Llen;->t:I

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 452
    iget-object v4, p0, Llen;->l:Landroid/widget/TextView;

    add-int/2addr v2, v3

    add-int v5, v0, v1

    invoke-virtual {v4, v3, v0, v2, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 455
    add-int/2addr v0, v1

    .line 456
    iget-object v1, p0, Llen;->y:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    .line 457
    iget-object v2, p0, Llen;->y:Landroid/widget/ImageView;

    iget-object v3, p0, Llen;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Llen;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Llen;->y:Landroid/widget/ImageView;

    .line 458
    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    add-int v5, v0, v1

    .line 457
    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 459
    iget-object v2, p0, Llen;->A:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 460
    add-int/2addr v0, v1

    .line 461
    iget-object v1, p0, Llen;->A:Landroid/widget/TextView;

    iget-object v2, p0, Llen;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Llen;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Llen;->A:Landroid/widget/TextView;

    .line 462
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Llen;->A:Landroid/widget/TextView;

    .line 463
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 461
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 465
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 233
    iget-object v0, p0, Llen;->g:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    .line 234
    iget-object v0, p0, Llen;->e:Llcd;

    invoke-virtual {v0, v3}, Llcd;->a(I)V

    .line 235
    iget-object v0, p0, Llen;->a:Llci;

    iget-object v1, p0, Llen;->e:Llcd;

    iget-object v2, p0, Llen;->r:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v5}, Llci;->a(Llcd;Ljava/lang/String;IZ)V

    .line 237
    iget v0, p0, Llen;->c:I

    invoke-virtual {p0, v0}, Llen;->a(I)I

    .line 238
    invoke-virtual {p0}, Llen;->requestLayout()V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    iget-object v0, p0, Llen;->A:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    .line 240
    iget-object v0, p0, Llen;->e:Llcd;

    invoke-virtual {v0, v1}, Llcd;->a(I)V

    .line 241
    iget v0, p0, Llen;->M:I

    iget v2, p0, Llen;->c:I

    invoke-virtual {p0, v0, v2}, Llen;->a(II)I

    .line 243
    iget-object v0, p0, Llen;->a:Llci;

    iget-object v2, p0, Llen;->e:Llcd;

    iget-object v3, p0, Llen;->r:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v1, v1}, Llci;->a(Llcd;Ljava/lang/String;IZ)V

    .line 245
    invoke-virtual {p0}, Llen;->requestLayout()V

    goto :goto_0

    .line 247
    :cond_2
    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Llen;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_5

    iget-object v3, p0, Llen;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne p1, v3, :cond_4

    iget-object v2, p0, Llen;->e:Llcd;

    invoke-virtual {v2, v0}, Llcd;->b(I)Llce;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llen;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Llen;->B:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Llen;->B:Landroid/widget/TextView;

    const v4, 0x7f0b011e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackgroundColor(I)V

    iget-object v3, p0, Llen;->B:Landroid/widget/TextView;

    invoke-virtual {v0}, Llce;->c()I

    move-result v4

    invoke-direct {p0, v4}, Llen;->b(I)I

    move-result v4

    invoke-virtual {v3, v4, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :cond_3
    iput-object p1, p0, Llen;->B:Landroid/widget/TextView;

    const v3, 0x7f0b0116

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    invoke-virtual {v0}, Llce;->d()I

    move-result v2

    invoke-direct {p0, v2}, Llen;->b(I)I

    move-result v2

    const v3, 0x7f0203de

    invoke-virtual {p1, v2, v1, v3, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v1, p0, Llen;->e:Llcd;

    invoke-virtual {v1, v5}, Llcd;->a(I)V

    iget-object v1, p0, Llen;->a:Llci;

    iget-object v2, p0, Llen;->e:Llcd;

    iget-object v3, p0, Llen;->r:Ljava/lang/String;

    invoke-virtual {v0}, Llce;->a()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, v5}, Llci;->a(Llcd;Ljava/lang/String;IZ)V

    iget-object v0, p0, Llen;->P:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Llen;->P:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method
