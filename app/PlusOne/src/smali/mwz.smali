.class public final Lmwz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmwz;


# instance fields
.field private b:Ljava/lang/Long;

.field private c:I

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6803
    const/4 v0, 0x0

    new-array v0, v0, [Lmwz;

    sput-object v0, Lmwz;->a:[Lmwz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6804
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6809
    const/high16 v0, -0x80000000

    iput v0, p0, Lmwz;->c:I

    .line 6804
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 6836
    const/4 v0, 0x0

    .line 6837
    iget-object v1, p0, Lmwz;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 6838
    const/4 v0, 0x1

    iget-object v1, p0, Lmwz;->b:Ljava/lang/Long;

    .line 6839
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6841
    :cond_0
    iget v1, p0, Lmwz;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 6842
    const/4 v1, 0x2

    iget v2, p0, Lmwz;->c:I

    .line 6843
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6845
    :cond_1
    iget-object v1, p0, Lmwz;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 6846
    const/4 v1, 0x3

    iget-object v2, p0, Lmwz;->d:Ljava/lang/Integer;

    .line 6847
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6849
    :cond_2
    iget-object v1, p0, Lmwz;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 6850
    const/4 v1, 0x4

    iget-object v2, p0, Lmwz;->e:Ljava/lang/Integer;

    .line 6851
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6853
    :cond_3
    iget-object v1, p0, Lmwz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6854
    iput v0, p0, Lmwz;->ai:I

    .line 6855
    return v0
.end method

.method public a(Loxn;)Lmwz;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6863
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6864
    sparse-switch v0, :sswitch_data_0

    .line 6868
    iget-object v1, p0, Lmwz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6869
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwz;->ah:Ljava/util/List;

    .line 6872
    :cond_1
    iget-object v1, p0, Lmwz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6874
    :sswitch_0
    return-object p0

    .line 6879
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwz;->b:Ljava/lang/Long;

    goto :goto_0

    .line 6883
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6884
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 6888
    :cond_2
    iput v0, p0, Lmwz;->c:I

    goto :goto_0

    .line 6890
    :cond_3
    iput v2, p0, Lmwz;->c:I

    goto :goto_0

    .line 6895
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwz;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 6899
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwz;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 6864
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 6818
    iget-object v0, p0, Lmwz;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 6819
    const/4 v0, 0x1

    iget-object v1, p0, Lmwz;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 6821
    :cond_0
    iget v0, p0, Lmwz;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 6822
    const/4 v0, 0x2

    iget v1, p0, Lmwz;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6824
    :cond_1
    iget-object v0, p0, Lmwz;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 6825
    const/4 v0, 0x3

    iget-object v1, p0, Lmwz;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6827
    :cond_2
    iget-object v0, p0, Lmwz;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 6828
    const/4 v0, 0x4

    iget-object v1, p0, Lmwz;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6830
    :cond_3
    iget-object v0, p0, Lmwz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6832
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6800
    invoke-virtual {p0, p1}, Lmwz;->a(Loxn;)Lmwz;

    move-result-object v0

    return-object v0
.end method
