.class public final Ljmr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lnif;

.field private b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lnka;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lnjy;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lnjz;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lnjx;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:I

.field private h:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lnif;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385
    const/4 v0, 0x0

    iput v0, p0, Ljmr;->f:I

    .line 390
    if-eqz p1, :cond_1

    .line 391
    iput-object p1, p0, Ljmr;->a:Lnif;

    .line 392
    iput p2, p0, Ljmr;->g:I

    .line 393
    iget-object v0, p1, Lnif;->a:[Lnka;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lnif;->a:[Lnka;

    .line 394
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Ljmr;->b:Ljava/util/Iterator;

    .line 395
    iget-object v0, p1, Lnif;->b:[Lnjy;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lnif;->b:[Lnjy;

    .line 396
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Ljmr;->c:Ljava/util/Iterator;

    .line 397
    iget-object v0, p1, Lnif;->d:[Lnjz;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lnif;->d:[Lnjz;

    .line 398
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Ljmr;->d:Ljava/util/Iterator;

    .line 399
    iget-object v0, p1, Lnif;->c:[Lnjx;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnif;->c:[Lnjx;

    .line 400
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Ljmr;->e:Ljava/util/Iterator;

    .line 401
    invoke-virtual {p0}, Ljmr;->next()Ljava/lang/Object;

    .line 403
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 394
    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 396
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 398
    goto :goto_2
.end method

.method private a(I)Lnka;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 487
    iget-object v0, p0, Ljmr;->b:Ljava/util/Iterator;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 496
    :goto_0
    return-object v0

    .line 490
    :cond_0
    iget-object v0, p0, Ljmr;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Ljmr;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnka;

    .line 492
    iget v2, v0, Lnka;->e:I

    if-ne p1, v2, :cond_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 496
    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v4, 0x7

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 412
    iget-object v6, p0, Ljmr;->h:Ljava/lang/Object;

    .line 415
    iput-object v3, p0, Ljmr;->h:Ljava/lang/Object;

    .line 416
    :cond_0
    :goto_0
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-nez v0, :cond_11

    iget v0, p0, Ljmr;->f:I

    if-eq v0, v4, :cond_11

    .line 417
    iget v0, p0, Ljmr;->f:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 419
    :pswitch_0
    iget v0, p0, Ljmr;->g:I

    if-ne v8, v0, :cond_1

    move v0, v1

    .line 421
    :goto_1
    invoke-direct {p0, v0}, Ljmr;->a(I)Lnka;

    move-result-object v0

    iput-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    .line 422
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 423
    iput v8, p0, Ljmr;->f:I

    .line 424
    iget-object v0, p0, Ljmr;->a:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljmr;->a:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    .line 425
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Ljmr;->b:Ljava/util/Iterator;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 419
    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 425
    goto :goto_2

    .line 429
    :pswitch_1
    iget v0, p0, Ljmr;->g:I

    if-ne v8, v0, :cond_3

    move v0, v4

    .line 431
    :goto_3
    invoke-direct {p0, v0}, Ljmr;->a(I)Lnka;

    move-result-object v0

    iput-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    .line 432
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 433
    iput v1, p0, Ljmr;->f:I

    goto :goto_0

    .line 429
    :cond_3
    const/16 v0, 0x12

    goto :goto_3

    .line 437
    :pswitch_2
    iget v0, p0, Ljmr;->g:I

    if-ne v8, v0, :cond_5

    move v5, v1

    .line 439
    :goto_4
    iget-object v0, p0, Ljmr;->c:Ljava/util/Iterator;

    if-eqz v0, :cond_6

    :cond_4
    iget-object v0, p0, Ljmr;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljmr;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnjy;

    iget-object v7, v0, Lnjy;->c:Lnie;

    if-eqz v7, :cond_4

    iget-object v7, v0, Lnjy;->c:Lnie;

    iget v7, v7, Lnie;->a:I

    if-ne v5, v7, :cond_4

    :goto_5
    iput-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    .line 440
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 441
    iput v2, p0, Ljmr;->f:I

    .line 442
    iget-object v0, p0, Ljmr;->a:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljmr;->a:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    .line 443
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    iput-object v0, p0, Ljmr;->b:Ljava/util/Iterator;

    goto/16 :goto_0

    :cond_5
    move v5, v2

    .line 437
    goto :goto_4

    :cond_6
    move-object v0, v3

    .line 439
    goto :goto_5

    :cond_7
    move-object v0, v3

    .line 443
    goto :goto_6

    .line 447
    :pswitch_3
    iget v0, p0, Ljmr;->g:I

    if-ne v8, v0, :cond_8

    const/4 v0, 0x5

    .line 449
    :goto_7
    invoke-direct {p0, v0}, Ljmr;->a(I)Lnka;

    move-result-object v0

    iput-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    .line 450
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 451
    const/4 v0, 0x4

    iput v0, p0, Ljmr;->f:I

    .line 452
    iget-object v0, p0, Ljmr;->a:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    if-eqz v0, :cond_9

    iget-object v0, p0, Ljmr;->a:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    .line 453
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    iput-object v0, p0, Ljmr;->b:Ljava/util/Iterator;

    goto/16 :goto_0

    .line 447
    :cond_8
    const/4 v0, 0x6

    goto :goto_7

    :cond_9
    move-object v0, v3

    .line 453
    goto :goto_8

    .line 457
    :pswitch_4
    iget v0, p0, Ljmr;->g:I

    if-ne v8, v0, :cond_a

    const/16 v0, 0x8

    .line 459
    :goto_9
    invoke-direct {p0, v0}, Ljmr;->a(I)Lnka;

    move-result-object v0

    iput-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    .line 460
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 461
    const/4 v0, 0x5

    iput v0, p0, Ljmr;->f:I

    goto/16 :goto_0

    .line 457
    :cond_a
    const/16 v0, 0x13

    goto :goto_9

    .line 465
    :pswitch_5
    iget v0, p0, Ljmr;->g:I

    if-ne v8, v0, :cond_c

    move v5, v1

    .line 467
    :goto_a
    iget-object v0, p0, Ljmr;->d:Ljava/util/Iterator;

    if-eqz v0, :cond_d

    :cond_b
    iget-object v0, p0, Ljmr;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljmr;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnjz;

    iget-object v7, v0, Lnjz;->c:Lnie;

    if-eqz v7, :cond_b

    iget-object v7, v0, Lnjz;->c:Lnie;

    iget v7, v7, Lnie;->a:I

    if-ne v5, v7, :cond_b

    :goto_b
    iput-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    .line 468
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 469
    const/4 v0, 0x6

    iput v0, p0, Ljmr;->f:I

    goto/16 :goto_0

    :cond_c
    move v5, v2

    .line 465
    goto :goto_a

    :cond_d
    move-object v0, v3

    .line 467
    goto :goto_b

    .line 473
    :pswitch_6
    iget v0, p0, Ljmr;->g:I

    if-ne v8, v0, :cond_f

    move v5, v1

    .line 475
    :goto_c
    iget-object v0, p0, Ljmr;->e:Ljava/util/Iterator;

    if-eqz v0, :cond_10

    :cond_e
    iget-object v0, p0, Ljmr;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Ljmr;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnjx;

    iget-object v7, v0, Lnjx;->c:Lnie;

    if-eqz v7, :cond_e

    iget-object v7, v0, Lnjx;->c:Lnie;

    iget v7, v7, Lnie;->a:I

    if-ne v5, v7, :cond_e

    :goto_d
    iput-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    .line 476
    iget-object v0, p0, Ljmr;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 477
    iput v4, p0, Ljmr;->f:I

    goto/16 :goto_0

    :cond_f
    move v5, v2

    .line 473
    goto :goto_c

    :cond_10
    move-object v0, v3

    .line 475
    goto :goto_d

    .line 483
    :cond_11
    return-object v6

    .line 417
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 540
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
