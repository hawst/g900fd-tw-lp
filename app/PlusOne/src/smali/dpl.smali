.class public final Ldpl;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ldup;

.field private final c:Z

.field private final d:Lhei;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILdup;Z)V
    .locals 1

    .prologue
    .line 43
    const-string v0, "MovePhotosToTrashTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 44
    iput-object p3, p0, Ldpl;->b:Ldup;

    .line 45
    iput p2, p0, Ldpl;->a:I

    .line 46
    iput-boolean p4, p0, Ldpl;->c:Z

    .line 47
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ldpl;->d:Lhei;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 14

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 52
    invoke-virtual {p0}, Ldpl;->f()Landroid/content/Context;

    move-result-object v1

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Ldpl;->b:Ldup;

    iget v2, p0, Ldpl;->a:I

    invoke-virtual {v0, v1, v2}, Ldup;->a(Landroid/content/Context;I)[Ldwj;

    move-result-object v2

    array-length v7, v2

    move v0, v4

    :goto_0
    if-ge v0, v7, :cond_4

    aget-object v11, v2, v0

    invoke-virtual {v11}, Ldwj;->e()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    iget-boolean v13, p0, Ldpl;->c:Z

    if-eqz v13, :cond_0

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v11}, Ldwj;->c()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_1

    invoke-virtual {v11}, Ldwj;->c()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-boolean v13, p0, Ldpl;->c:Z

    if-eqz v13, :cond_2

    invoke-virtual {v11}, Ldwj;->b()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-virtual {v11}, Ldwj;->a()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    iget v2, p0, Ldpl;->a:I

    const/4 v7, -0x1

    if-eq v2, v7, :cond_e

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget v0, p0, Ldpl;->a:I

    invoke-static {v1, v0, v10, v2}, Ljvd;->a(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;)Ljuy;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p0, Ldpl;->a:I

    invoke-static {v1, v0, v2}, Ljvj;->a(Landroid/content/Context;ILjava/util/ArrayList;)Ljuy;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Ldpl;->d:Lhei;

    iget v7, p0, Ldpl;->a:I

    invoke-interface {v0, v7}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v7, "gaia_id"

    invoke-interface {v0, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v7, Ljvu;

    iget v11, p0, Ldpl;->a:I

    invoke-direct {v7, v1, v11, v0, v6}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v7}, Ljvu;->l()V

    invoke-virtual {v7}, Ljvu;->t()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljvu;->b(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {v7, v0}, Ljvu;->c(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    move-object v6, v2

    :goto_3
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-static {v1, v11}, Lcsa;->a(Landroid/content/Context;Ljava/util/List;)I

    move-result v0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v0, v2, :cond_a

    move v0, v5

    :goto_4
    move v7, v0

    :goto_5
    invoke-virtual {v9}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    new-instance v0, Ldjc;

    iget v2, p0, Ldpl;->a:I

    invoke-virtual {v9}, Ljava/util/HashSet;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/Long;

    invoke-virtual {v9, v3}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Long;

    invoke-direct/range {v0 .. v5}, Ldjc;-><init>(Landroid/content/Context;I[Ljava/lang/Long;ZI)V

    invoke-virtual {v0}, Ldjc;->l()V

    if-eqz v7, :cond_b

    invoke-virtual {v0}, Ldjc;->t()Z

    move-result v0

    if-nez v0, :cond_b

    :goto_6
    new-instance v2, Lhoz;

    invoke-direct {v2, v5}, Lhoz;-><init>(Z)V

    if-eqz v5, :cond_7

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Ldpl;->a:I

    invoke-static {v1, v0, v6, v10, v11}, Ljvd;->a(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "db_rows"

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljuy;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 53
    :cond_7
    invoke-virtual {v2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    const-string v1, "resolver"

    iget-object v3, p0, Ldpl;->b:Ldup;

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 56
    return-object v2

    .line 52
    :cond_8
    const-string v0, "MovePhotosToTrashTask"

    const/4 v6, 0x6

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Error checking if photos exist.  Reason: %s, Error Code: %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v11, v7, Lkff;->j:Ljava/lang/String;

    aput-object v11, v6, v4

    iget v11, v7, Lkff;->i:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v6, v5

    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v6, "MovePhotosToTrashTask"

    iget-object v7, v7, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v6, v0, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_9
    move-object v6, v2

    goto/16 :goto_3

    :cond_a
    move v0, v4

    goto/16 :goto_4

    :cond_b
    move v5, v4

    goto :goto_6

    :cond_c
    move v5, v7

    goto :goto_6

    :cond_d
    move v7, v5

    goto/16 :goto_5

    :cond_e
    move-object v6, v0

    goto/16 :goto_3
.end method
