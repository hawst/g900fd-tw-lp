.class public final Lmam;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlm;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32317
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32320
    iput-object v0, p0, Lmam;->apiHeader:Llyq;

    .line 32323
    iput-object v0, p0, Lmam;->a:Lmlm;

    .line 32317
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32340
    const/4 v0, 0x0

    .line 32341
    iget-object v1, p0, Lmam;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 32342
    const/4 v0, 0x1

    iget-object v1, p0, Lmam;->apiHeader:Llyq;

    .line 32343
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32345
    :cond_0
    iget-object v1, p0, Lmam;->a:Lmlm;

    if-eqz v1, :cond_1

    .line 32346
    const/4 v1, 0x2

    iget-object v2, p0, Lmam;->a:Lmlm;

    .line 32347
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32349
    :cond_1
    iget-object v1, p0, Lmam;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32350
    iput v0, p0, Lmam;->ai:I

    .line 32351
    return v0
.end method

.method public a(Loxn;)Lmam;
    .locals 2

    .prologue
    .line 32359
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32360
    sparse-switch v0, :sswitch_data_0

    .line 32364
    iget-object v1, p0, Lmam;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32365
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmam;->ah:Ljava/util/List;

    .line 32368
    :cond_1
    iget-object v1, p0, Lmam;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32370
    :sswitch_0
    return-object p0

    .line 32375
    :sswitch_1
    iget-object v0, p0, Lmam;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 32376
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmam;->apiHeader:Llyq;

    .line 32378
    :cond_2
    iget-object v0, p0, Lmam;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32382
    :sswitch_2
    iget-object v0, p0, Lmam;->a:Lmlm;

    if-nez v0, :cond_3

    .line 32383
    new-instance v0, Lmlm;

    invoke-direct {v0}, Lmlm;-><init>()V

    iput-object v0, p0, Lmam;->a:Lmlm;

    .line 32385
    :cond_3
    iget-object v0, p0, Lmam;->a:Lmlm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32360
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32328
    iget-object v0, p0, Lmam;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 32329
    const/4 v0, 0x1

    iget-object v1, p0, Lmam;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32331
    :cond_0
    iget-object v0, p0, Lmam;->a:Lmlm;

    if-eqz v0, :cond_1

    .line 32332
    const/4 v0, 0x2

    iget-object v1, p0, Lmam;->a:Lmlm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32334
    :cond_1
    iget-object v0, p0, Lmam;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32336
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32313
    invoke-virtual {p0, p1}, Lmam;->a(Loxn;)Lmam;

    move-result-object v0

    return-object v0
.end method
