.class public final Lpll;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lplm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Loxq;-><init>()V

    .line 91
    const/high16 v0, -0x80000000

    iput v0, p0, Lpll;->a:I

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lpll;->b:Lplm;

    .line 83
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    iget v1, p0, Lpll;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 113
    const/4 v0, 0x1

    iget v1, p0, Lpll;->a:I

    .line 114
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 116
    :cond_0
    iget-object v1, p0, Lpll;->b:Lplm;

    if-eqz v1, :cond_1

    .line 117
    const/4 v1, 0x2

    iget-object v2, p0, Lpll;->b:Lplm;

    .line 118
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_1
    iget-object v1, p0, Lpll;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    iput v0, p0, Lpll;->ai:I

    .line 122
    return v0
.end method

.method public a(Loxn;)Lpll;
    .locals 2

    .prologue
    .line 130
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 131
    sparse-switch v0, :sswitch_data_0

    .line 135
    iget-object v1, p0, Lpll;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpll;->ah:Ljava/util/List;

    .line 139
    :cond_1
    iget-object v1, p0, Lpll;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    :sswitch_0
    return-object p0

    .line 146
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 147
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 149
    :cond_2
    iput v0, p0, Lpll;->a:I

    goto :goto_0

    .line 151
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpll;->a:I

    goto :goto_0

    .line 156
    :sswitch_2
    iget-object v0, p0, Lpll;->b:Lplm;

    if-nez v0, :cond_4

    .line 157
    new-instance v0, Lplm;

    invoke-direct {v0}, Lplm;-><init>()V

    iput-object v0, p0, Lpll;->b:Lplm;

    .line 159
    :cond_4
    iget-object v0, p0, Lpll;->b:Lplm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 131
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 99
    iget v0, p0, Lpll;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 100
    const/4 v0, 0x1

    iget v1, p0, Lpll;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 102
    :cond_0
    iget-object v0, p0, Lpll;->b:Lplm;

    if-eqz v0, :cond_1

    .line 103
    const/4 v0, 0x2

    iget-object v1, p0, Lpll;->b:Lplm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 105
    :cond_1
    iget-object v0, p0, Lpll;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 107
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lpll;->a(Loxn;)Lpll;

    move-result-object v0

    return-object v0
.end method
