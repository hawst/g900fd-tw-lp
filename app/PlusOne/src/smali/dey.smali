.class final Ldey;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic a:Ldex;


# direct methods
.method constructor <init>(Ldex;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Ldey;->a:Ldex;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 219
    iget-object v0, p0, Ldey;->a:Ldex;

    invoke-virtual {v0}, Ldex;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.google.android.apps.photos.SLIDESHOW_STATE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    const-string v0, "slideshow_playing"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 223
    iget-object v0, p0, Ldey;->a:Ldex;

    invoke-static {v0, v1}, Ldex;->a(Ldex;Z)Z

    .line 224
    iget-object v3, p0, Ldey;->a:Ldex;

    iget-object v0, p0, Ldey;->a:Ldex;

    invoke-static {v0}, Ldex;->a(Ldex;)Llnh;

    move-result-object v0

    const-class v4, Ljuk;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    invoke-virtual {v0}, Ljuk;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v3, v0}, Ldex;->b(Ldex;Z)Z

    .line 225
    iget-object v0, p0, Ldey;->a:Ldex;

    invoke-virtual {v0}, Ldex;->X()Z

    move-result v0

    if-nez v0, :cond_2

    .line 226
    iget-object v0, p0, Ldey;->a:Ldex;

    invoke-virtual {v0, v1}, Ldex;->a(Z)V

    .line 228
    :cond_2
    const-string v0, "slideshow_position"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 230
    iget-object v1, p0, Ldey;->a:Ldex;

    invoke-static {v1}, Ldex;->b(Ldex;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_4

    .line 231
    iget-object v0, p0, Ldey;->a:Ldex;

    invoke-static {v0}, Ldex;->c(Ldex;)Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->l()V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 224
    goto :goto_1

    .line 233
    :cond_4
    iget-object v1, p0, Ldey;->a:Ldex;

    invoke-static {v1}, Ldex;->c(Ldex;)Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(I)V

    goto :goto_0

    .line 236
    :cond_5
    iget-object v0, p0, Ldey;->a:Ldex;

    invoke-static {v0, v2}, Ldex;->a(Ldex;Z)Z

    goto :goto_0
.end method
