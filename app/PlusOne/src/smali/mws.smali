.class public final Lmws;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmws;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3087
    const/4 v0, 0x0

    new-array v0, v0, [Lmws;

    sput-object v0, Lmws;->a:[Lmws;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3088
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3098
    const/high16 v0, -0x80000000

    iput v0, p0, Lmws;->c:I

    .line 3088
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3115
    const/4 v0, 0x0

    .line 3116
    iget-object v1, p0, Lmws;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3117
    const/4 v0, 0x1

    iget-object v1, p0, Lmws;->b:Ljava/lang/Integer;

    .line 3118
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3120
    :cond_0
    iget v1, p0, Lmws;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 3121
    const/4 v1, 0x2

    iget v2, p0, Lmws;->c:I

    .line 3122
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3124
    :cond_1
    iget-object v1, p0, Lmws;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3125
    iput v0, p0, Lmws;->ai:I

    .line 3126
    return v0
.end method

.method public a(Loxn;)Lmws;
    .locals 2

    .prologue
    .line 3134
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3135
    sparse-switch v0, :sswitch_data_0

    .line 3139
    iget-object v1, p0, Lmws;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmws;->ah:Ljava/util/List;

    .line 3143
    :cond_1
    iget-object v1, p0, Lmws;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3145
    :sswitch_0
    return-object p0

    .line 3150
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmws;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 3154
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3155
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 3157
    :cond_2
    iput v0, p0, Lmws;->c:I

    goto :goto_0

    .line 3159
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmws;->c:I

    goto :goto_0

    .line 3135
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3103
    iget-object v0, p0, Lmws;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3104
    const/4 v0, 0x1

    iget-object v1, p0, Lmws;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3106
    :cond_0
    iget v0, p0, Lmws;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 3107
    const/4 v0, 0x2

    iget v1, p0, Lmws;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3109
    :cond_1
    iget-object v0, p0, Lmws;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3111
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3084
    invoke-virtual {p0, p1}, Lmws;->a(Loxn;)Lmws;

    move-result-object v0

    return-object v0
.end method
