.class public final Lmby;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmxl;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30049
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30052
    iput-object v0, p0, Lmby;->apiHeader:Llyq;

    .line 30055
    iput-object v0, p0, Lmby;->a:Lmxl;

    .line 30049
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 30072
    const/4 v0, 0x0

    .line 30073
    iget-object v1, p0, Lmby;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 30074
    const/4 v0, 0x1

    iget-object v1, p0, Lmby;->apiHeader:Llyq;

    .line 30075
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 30077
    :cond_0
    iget-object v1, p0, Lmby;->a:Lmxl;

    if-eqz v1, :cond_1

    .line 30078
    const/4 v1, 0x2

    iget-object v2, p0, Lmby;->a:Lmxl;

    .line 30079
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30081
    :cond_1
    iget-object v1, p0, Lmby;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30082
    iput v0, p0, Lmby;->ai:I

    .line 30083
    return v0
.end method

.method public a(Loxn;)Lmby;
    .locals 2

    .prologue
    .line 30091
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30092
    sparse-switch v0, :sswitch_data_0

    .line 30096
    iget-object v1, p0, Lmby;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30097
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmby;->ah:Ljava/util/List;

    .line 30100
    :cond_1
    iget-object v1, p0, Lmby;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30102
    :sswitch_0
    return-object p0

    .line 30107
    :sswitch_1
    iget-object v0, p0, Lmby;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 30108
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmby;->apiHeader:Llyq;

    .line 30110
    :cond_2
    iget-object v0, p0, Lmby;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30114
    :sswitch_2
    iget-object v0, p0, Lmby;->a:Lmxl;

    if-nez v0, :cond_3

    .line 30115
    new-instance v0, Lmxl;

    invoke-direct {v0}, Lmxl;-><init>()V

    iput-object v0, p0, Lmby;->a:Lmxl;

    .line 30117
    :cond_3
    iget-object v0, p0, Lmby;->a:Lmxl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30092
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30060
    iget-object v0, p0, Lmby;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 30061
    const/4 v0, 0x1

    iget-object v1, p0, Lmby;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30063
    :cond_0
    iget-object v0, p0, Lmby;->a:Lmxl;

    if-eqz v0, :cond_1

    .line 30064
    const/4 v0, 0x2

    iget-object v1, p0, Lmby;->a:Lmxl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30066
    :cond_1
    iget-object v0, p0, Lmby;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30068
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 30045
    invoke-virtual {p0, p1}, Lmby;->a(Loxn;)Lmby;

    move-result-object v0

    return-object v0
.end method
