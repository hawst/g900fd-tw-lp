.class public final Lits;
.super Lito;
.source "PG"


# instance fields
.field private N:[Lnhm;

.field private O:Litr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lito;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;Z)Lits;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;Z)",
            "Lits;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 30
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 31
    if-lez v1, :cond_0

    .line 32
    const-string v1, "user_device_locations"

    new-instance v2, Lhyv;

    invoke-direct {v2, p0}, Lhyv;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 35
    :cond_0
    const-string v1, "show_actions"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 36
    const-string v1, "title_res_id"

    const v2, 0x7f0a03ea

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 37
    const-string v1, "positive_button_res_id"

    const v2, 0x7f0a03eb

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 39
    new-instance v1, Lits;

    invoke-direct {v1}, Lits;-><init>()V

    .line 40
    invoke-virtual {v1, v0}, Lits;->f(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method


# virtual methods
.method public U()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0}, Lits;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "user_device_locations"

    new-instance v2, Lhyv;

    invoke-direct {v2, p1}, Lhyv;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 67
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lnhm;

    iput-object v0, p0, Lits;->N:[Lnhm;

    .line 68
    iget-object v0, p0, Lits;->N:[Lnhm;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lits;->O:Litr;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lits;->O:Litr;

    iget-object v1, p0, Lits;->N:[Lnhm;

    invoke-virtual {v0, v1}, Litr;->a([Lnhm;)V

    .line 73
    :cond_0
    return-void
.end method

.method public k(Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 46
    const-string v0, "user_device_locations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyv;

    .line 47
    if-nez v0, :cond_0

    move-object v0, v1

    .line 48
    :goto_0
    iput-object v0, p0, Lits;->N:[Lnhm;

    .line 50
    const-string v0, "show_actions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 52
    invoke-virtual {p0}, Lits;->V()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 53
    const v2, 0x7f0400b2

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 55
    new-instance v0, Litr;

    invoke-virtual {p0}, Lits;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lits;->N:[Lnhm;

    invoke-virtual {p0}, Lits;->W()Litn;

    move-result-object v3

    const/4 v4, 0x1

    .line 56
    invoke-virtual {p0}, Lits;->j()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Litr;-><init>(Landroid/content/Context;[Lnhm;Litn;ZLjava/lang/String;)V

    iput-object v0, p0, Lits;->O:Litr;

    .line 58
    const v0, 0x7f1002b2

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 59
    iget-object v1, p0, Lits;->O:Litr;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    return-object v6

    .line 47
    :cond_0
    const/4 v2, 0x0

    new-array v2, v2, [Lnhm;

    .line 48
    invoke-virtual {v0, v2}, Lhyv;->a([Loxu;)[Loxu;

    move-result-object v0

    check-cast v0, [Lnhm;

    goto :goto_0
.end method
