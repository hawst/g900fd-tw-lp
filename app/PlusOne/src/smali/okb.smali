.class public final Lokb;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lohm;

.field public b:[Lohv;

.field public c:Loia;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 499
    invoke-direct {p0}, Loxq;-><init>()V

    .line 502
    sget-object v0, Lohm;->a:[Lohm;

    iput-object v0, p0, Lokb;->a:[Lohm;

    .line 505
    sget-object v0, Lohv;->a:[Lohv;

    iput-object v0, p0, Lokb;->b:[Lohv;

    .line 508
    const/4 v0, 0x0

    iput-object v0, p0, Lokb;->c:Loia;

    .line 499
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 546
    .line 547
    iget-object v0, p0, Lokb;->a:[Lohm;

    if-eqz v0, :cond_1

    .line 548
    iget-object v3, p0, Lokb;->a:[Lohm;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 549
    if-eqz v5, :cond_0

    .line 550
    const/4 v6, 0x1

    .line 551
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 548
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 555
    :cond_2
    iget-object v2, p0, Lokb;->b:[Lohv;

    if-eqz v2, :cond_4

    .line 556
    iget-object v2, p0, Lokb;->b:[Lohv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 557
    if-eqz v4, :cond_3

    .line 558
    const/4 v5, 0x2

    .line 559
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 556
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 563
    :cond_4
    iget-object v1, p0, Lokb;->c:Loia;

    if-eqz v1, :cond_5

    .line 564
    const/4 v1, 0x3

    iget-object v2, p0, Lokb;->c:Loia;

    .line 565
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_5
    iget-object v1, p0, Lokb;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 568
    const/4 v1, 0x4

    iget-object v2, p0, Lokb;->d:Ljava/lang/Integer;

    .line 569
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_6
    iget-object v1, p0, Lokb;->e:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 572
    const/4 v1, 0x5

    iget-object v2, p0, Lokb;->e:Ljava/lang/Long;

    .line 573
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_7
    iget-object v1, p0, Lokb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 576
    iput v0, p0, Lokb;->ai:I

    .line 577
    return v0
.end method

.method public a(Loxn;)Lokb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 585
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 586
    sparse-switch v0, :sswitch_data_0

    .line 590
    iget-object v2, p0, Lokb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 591
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lokb;->ah:Ljava/util/List;

    .line 594
    :cond_1
    iget-object v2, p0, Lokb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 596
    :sswitch_0
    return-object p0

    .line 601
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 602
    iget-object v0, p0, Lokb;->a:[Lohm;

    if-nez v0, :cond_3

    move v0, v1

    .line 603
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohm;

    .line 604
    iget-object v3, p0, Lokb;->a:[Lohm;

    if-eqz v3, :cond_2

    .line 605
    iget-object v3, p0, Lokb;->a:[Lohm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 607
    :cond_2
    iput-object v2, p0, Lokb;->a:[Lohm;

    .line 608
    :goto_2
    iget-object v2, p0, Lokb;->a:[Lohm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 609
    iget-object v2, p0, Lokb;->a:[Lohm;

    new-instance v3, Lohm;

    invoke-direct {v3}, Lohm;-><init>()V

    aput-object v3, v2, v0

    .line 610
    iget-object v2, p0, Lokb;->a:[Lohm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 611
    invoke-virtual {p1}, Loxn;->a()I

    .line 608
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 602
    :cond_3
    iget-object v0, p0, Lokb;->a:[Lohm;

    array-length v0, v0

    goto :goto_1

    .line 614
    :cond_4
    iget-object v2, p0, Lokb;->a:[Lohm;

    new-instance v3, Lohm;

    invoke-direct {v3}, Lohm;-><init>()V

    aput-object v3, v2, v0

    .line 615
    iget-object v2, p0, Lokb;->a:[Lohm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 619
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 620
    iget-object v0, p0, Lokb;->b:[Lohv;

    if-nez v0, :cond_6

    move v0, v1

    .line 621
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lohv;

    .line 622
    iget-object v3, p0, Lokb;->b:[Lohv;

    if-eqz v3, :cond_5

    .line 623
    iget-object v3, p0, Lokb;->b:[Lohv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 625
    :cond_5
    iput-object v2, p0, Lokb;->b:[Lohv;

    .line 626
    :goto_4
    iget-object v2, p0, Lokb;->b:[Lohv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 627
    iget-object v2, p0, Lokb;->b:[Lohv;

    new-instance v3, Lohv;

    invoke-direct {v3}, Lohv;-><init>()V

    aput-object v3, v2, v0

    .line 628
    iget-object v2, p0, Lokb;->b:[Lohv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 629
    invoke-virtual {p1}, Loxn;->a()I

    .line 626
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 620
    :cond_6
    iget-object v0, p0, Lokb;->b:[Lohv;

    array-length v0, v0

    goto :goto_3

    .line 632
    :cond_7
    iget-object v2, p0, Lokb;->b:[Lohv;

    new-instance v3, Lohv;

    invoke-direct {v3}, Lohv;-><init>()V

    aput-object v3, v2, v0

    .line 633
    iget-object v2, p0, Lokb;->b:[Lohv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 637
    :sswitch_3
    iget-object v0, p0, Lokb;->c:Loia;

    if-nez v0, :cond_8

    .line 638
    new-instance v0, Loia;

    invoke-direct {v0}, Loia;-><init>()V

    iput-object v0, p0, Lokb;->c:Loia;

    .line 640
    :cond_8
    iget-object v0, p0, Lokb;->c:Loia;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 644
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokb;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 648
    :sswitch_5
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lokb;->e:Ljava/lang/Long;

    goto/16 :goto_0

    .line 586
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 517
    iget-object v1, p0, Lokb;->a:[Lohm;

    if-eqz v1, :cond_1

    .line 518
    iget-object v2, p0, Lokb;->a:[Lohm;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 519
    if-eqz v4, :cond_0

    .line 520
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 518
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 524
    :cond_1
    iget-object v1, p0, Lokb;->b:[Lohv;

    if-eqz v1, :cond_3

    .line 525
    iget-object v1, p0, Lokb;->b:[Lohv;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 526
    if-eqz v3, :cond_2

    .line 527
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 525
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 531
    :cond_3
    iget-object v0, p0, Lokb;->c:Loia;

    if-eqz v0, :cond_4

    .line 532
    const/4 v0, 0x3

    iget-object v1, p0, Lokb;->c:Loia;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 534
    :cond_4
    iget-object v0, p0, Lokb;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 535
    const/4 v0, 0x4

    iget-object v1, p0, Lokb;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 537
    :cond_5
    iget-object v0, p0, Lokb;->e:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 538
    const/4 v0, 0x5

    iget-object v1, p0, Lokb;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 540
    :cond_6
    iget-object v0, p0, Lokb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 542
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 495
    invoke-virtual {p0, p1}, Lokb;->a(Loxn;)Lokb;

    move-result-object v0

    return-object v0
.end method
