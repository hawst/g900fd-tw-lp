.class public final Llka;
.super Landroid/view/ViewGroup$LayoutParams;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:J

.field public g:Z

.field public h:I


# direct methods
.method public constructor <init>(II)V
    .locals 4

    .prologue
    const/4 v3, -0x3

    const/4 v0, -0x1

    const/4 v2, 0x1

    .line 2579
    if-ne p1, v2, :cond_1

    if-eq p2, v3, :cond_1

    move v1, p2

    :goto_0
    if-ne p1, v2, :cond_2

    move p2, v0

    :cond_0
    :goto_1
    invoke-direct {p0, v1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2549
    iput v2, p0, Llka;->a:I

    .line 2552
    iput v2, p0, Llka;->b:I

    .line 2564
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Llka;->f:J

    .line 2573
    iput-boolean v2, p0, Llka;->g:Z

    .line 2576
    const/4 v0, 0x2

    iput v0, p0, Llka;->h:I

    .line 2584
    iput p1, p0, Llka;->h:I

    .line 2585
    return-void

    :cond_1
    move v1, v0

    .line 2579
    goto :goto_0

    :cond_2
    if-ne p2, v3, :cond_0

    move p2, v0

    goto :goto_1
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 2588
    invoke-direct {p0, p1, p2}, Llka;-><init>(II)V

    .line 2590
    iput p3, p0, Llka;->b:I

    .line 2591
    iput p4, p0, Llka;->a:I

    .line 2592
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 2595
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2549
    iput v2, p0, Llka;->a:I

    .line 2552
    iput v2, p0, Llka;->b:I

    .line 2564
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Llka;->f:J

    .line 2573
    iput-boolean v2, p0, Llka;->g:Z

    .line 2576
    iput v4, p0, Llka;->h:I

    .line 2597
    sget-object v0, Llkk;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2598
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Llka;->b:I

    .line 2599
    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Llka;->a:I

    .line 2600
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Llka;->h:I

    .line 2602
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2604
    iget v0, p0, Llka;->h:I

    if-ne v0, v2, :cond_1

    .line 2605
    iget v0, p0, Llka;->height:I

    if-eq v0, v3, :cond_0

    .line 2606
    iget v0, p0, Llka;->height:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x4b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Inflation setting LayoutParams height to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - must be MATCH_PARENT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2608
    iput v3, p0, Llka;->height:I

    .line 2617
    :cond_0
    :goto_0
    return-void

    .line 2611
    :cond_1
    iget v0, p0, Llka;->width:I

    if-eq v0, v3, :cond_0

    .line 2612
    iget v0, p0, Llka;->width:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x4a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Inflation setting LayoutParams width to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - must be MATCH_PARENT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614
    iput v3, p0, Llka;->width:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 2620
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2549
    iput v2, p0, Llka;->a:I

    .line 2552
    iput v2, p0, Llka;->b:I

    .line 2564
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Llka;->f:J

    .line 2573
    iput-boolean v2, p0, Llka;->g:Z

    .line 2576
    const/4 v0, 0x2

    iput v0, p0, Llka;->h:I

    .line 2622
    iget v0, p0, Llka;->h:I

    if-ne v0, v2, :cond_1

    .line 2623
    iget v0, p0, Llka;->height:I

    if-eq v0, v3, :cond_0

    .line 2624
    iget v0, p0, Llka;->height:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x48

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Constructing LayoutParams with height "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - must be MATCH_PARENT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2626
    iput v3, p0, Llka;->height:I

    .line 2635
    :cond_0
    :goto_0
    return-void

    .line 2629
    :cond_1
    iget v0, p0, Llka;->width:I

    if-eq v0, v3, :cond_0

    .line 2630
    iget v0, p0, Llka;->width:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x47

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Constructing LayoutParams with width "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - must be MATCH_PARENT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2632
    iput v3, p0, Llka;->width:I

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 11

    .prologue
    .line 2639
    iget-wide v0, p0, Llka;->f:J

    iget v2, p0, Llka;->a:I

    iget v3, p0, Llka;->b:I

    iget v4, p0, Llka;->c:I

    iget v5, p0, Llka;->d:I

    iget v6, p0, Llka;->e:I

    iget-boolean v7, p0, Llka;->g:Z

    iget v8, p0, Llka;->h:I

    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v10, 0xab

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "ColumnGridView.LayoutParams: id="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " major="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " minor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " col="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " boxstart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " orient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
