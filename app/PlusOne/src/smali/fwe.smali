.class public final Lfwe;
.super Lfye;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:I

.field private static d:F

.field private static e:I


# instance fields
.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldrv;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/libraries/social/avatars/ui/AvatarView;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lfxf;

.field private j:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 51
    invoke-direct {p0, p1, p2, p3}, Lfye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    sget-boolean v0, Lfwe;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfwe;->b:I

    invoke-static {p1}, Lhss;->c(Landroid/content/Context;)I

    move-result v1

    sput v1, Lfwe;->c:I

    const v1, 0x7f0d024c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lfwe;->d:F

    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lfwe;->e:I

    sput-boolean v6, Lfwe;->a:Z

    :cond_0
    sget v3, Lfwe;->d:F

    sget v4, Lfwe;->e:I

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Llif;->a(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lfwe;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lfwe;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfwe;->addView(Landroid/view/View;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfwe;->h:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfwe;->f:Ljava/util/ArrayList;

    .line 53
    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;Lfxf;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldrv;",
            ">;",
            "Lfxf;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lfwe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 169
    if-eqz p1, :cond_0

    .line 170
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldrv;

    .line 171
    iget-object v2, p0, Lfwe;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    :cond_0
    iput-object p2, p0, Lfwe;->i:Lfxf;

    .line 176
    iput p3, p0, Lfwe;->g:I

    .line 178
    invoke-virtual {p0}, Lfwe;->requestLayout()V

    .line 179
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 192
    iget-object v0, p0, Lfwe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 194
    iget-object v0, p0, Lfwe;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 196
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 197
    iget-object v0, p0, Lfwe;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 198
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 199
    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d()V

    .line 200
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 201
    invoke-virtual {p0, v0}, Lfwe;->removeView(Landroid/view/View;)V

    .line 196
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 204
    :cond_0
    iget-object v0, p0, Lfwe;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 206
    iput-object v4, p0, Lfwe;->i:Lfxf;

    .line 207
    return-void
.end method

.method protected measureChildren(II)V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/16 v11, 0x8

    const/high16 v10, -0x80000000

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 78
    sget v0, Lfwe;->c:I

    sget v1, Lfwe;->b:I

    add-int v5, v0, v1

    .line 79
    div-int v1, v2, v5

    .line 80
    iget-object v0, p0, Lfwe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 85
    if-lt v1, v0, :cond_0

    iget v1, p0, Lfwe;->g:I

    if-le v1, v0, :cond_3

    .line 87
    :cond_0
    iget v1, p0, Lfwe;->g:I

    sub-int/2addr v1, v0

    .line 92
    invoke-virtual {p0}, Lfwe;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f110060

    new-array v8, v4, [Ljava/lang/Object;

    .line 93
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    .line 92
    invoke-virtual {v6, v7, v1, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 94
    iget-object v6, p0, Lfwe;->j:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v1, p0, Lfwe;->j:Landroid/widget/TextView;

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    sget v7, Lfwe;->c:I

    .line 96
    invoke-static {v7, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 95
    invoke-virtual {v1, v6, v7}, Landroid/widget/TextView;->measure(II)V

    .line 97
    mul-int v1, v0, v5

    sub-int v6, v2, v1

    .line 99
    iget-object v1, p0, Lfwe;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v4, :cond_2

    iget-object v1, p0, Lfwe;->j:Landroid/widget/TextView;

    .line 100
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    if-ge v6, v1, :cond_2

    move v1, v4

    .line 102
    :goto_0
    if-eqz v1, :cond_1

    .line 103
    add-int/lit8 v0, v0, -0x1

    .line 105
    :cond_1
    if-nez v1, :cond_0

    .line 107
    iget-object v1, p0, Lfwe;->j:Landroid/widget/TextView;

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    sget v6, Lfwe;->c:I

    .line 108
    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 107
    invoke-virtual {v1, v2, v6}, Landroid/widget/TextView;->measure(II)V

    .line 109
    iget-object v1, p0, Lfwe;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    iget-object v1, p0, Lfwe;->j:Landroid/widget/TextView;

    mul-int v2, v5, v0

    sget v5, Lfwe;->c:I

    iget-object v6, p0, Lfwe;->j:Landroid/widget/TextView;

    .line 111
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 110
    invoke-static {v1, v2, v5}, Lfwe;->a(Landroid/view/View;II)V

    move v2, v0

    .line 117
    :goto_1
    iget-object v0, p0, Lfwe;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v0, v2, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 119
    :goto_2
    if-lez v0, :cond_4

    .line 120
    new-instance v1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lfwe;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    .line 121
    invoke-virtual {v1, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    invoke-virtual {v1, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 123
    invoke-virtual {v1, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 124
    invoke-virtual {p0, v1}, Lfwe;->addView(Landroid/view/View;)V

    .line 125
    iget-object v5, p0, Lfwe;->h:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_2
    move v1, v3

    .line 100
    goto :goto_0

    .line 113
    :cond_3
    iget-object v1, p0, Lfwe;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    move v2, v0

    goto :goto_1

    .line 129
    :cond_4
    iget-object v0, p0, Lfwe;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v3

    move v4, v3

    .line 131
    :goto_3
    if-ge v5, v6, :cond_8

    .line 132
    iget-object v0, p0, Lfwe;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 134
    if-ge v5, v2, :cond_7

    .line 135
    iget-object v1, p0, Lfwe;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldrv;

    .line 137
    iget-object v7, v1, Ldrv;->gaiaId:Ljava/lang/String;

    iget-object v8, v1, Ldrv;->avatarUrl:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v1, v1, Ldrv;->name:Ljava/lang/String;

    .line 141
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 142
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 145
    :cond_5
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 146
    sget v1, Lfwe;->c:I

    .line 147
    invoke-static {v1, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    sget v7, Lfwe;->c:I

    .line 148
    invoke-static {v7, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 146
    invoke-virtual {v0, v1, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 150
    if-lez v5, :cond_6

    sget v1, Lfwe;->b:I

    :goto_4
    add-int/2addr v1, v4

    .line 151
    invoke-static {v0, v1, v3}, Lfwe;->a(Landroid/view/View;II)V

    .line 152
    sget v0, Lfwe;->c:I

    add-int/2addr v0, v1

    .line 131
    :goto_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v4, v0

    goto :goto_3

    :cond_6
    move v1, v3

    .line 150
    goto :goto_4

    .line 154
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d()V

    .line 155
    invoke-virtual {v0, v11}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    move v0, v4

    goto :goto_5

    .line 158
    :cond_8
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 186
    instance-of v0, p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfwe;->i:Lfxf;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lfwe;->i:Lfxf;

    check-cast p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lfxf;->b(Ljava/lang/String;)V

    .line 189
    :cond_0
    return-void
.end method
