.class public final Lmin;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnwh;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6640
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6643
    iput-object v0, p0, Lmin;->apiHeader:Llyr;

    .line 6646
    iput-object v0, p0, Lmin;->a:Lnwh;

    .line 6640
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6663
    const/4 v0, 0x0

    .line 6664
    iget-object v1, p0, Lmin;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 6665
    const/4 v0, 0x1

    iget-object v1, p0, Lmin;->apiHeader:Llyr;

    .line 6666
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6668
    :cond_0
    iget-object v1, p0, Lmin;->a:Lnwh;

    if-eqz v1, :cond_1

    .line 6669
    const/4 v1, 0x2

    iget-object v2, p0, Lmin;->a:Lnwh;

    .line 6670
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6672
    :cond_1
    iget-object v1, p0, Lmin;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6673
    iput v0, p0, Lmin;->ai:I

    .line 6674
    return v0
.end method

.method public a(Loxn;)Lmin;
    .locals 2

    .prologue
    .line 6682
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6683
    sparse-switch v0, :sswitch_data_0

    .line 6687
    iget-object v1, p0, Lmin;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6688
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmin;->ah:Ljava/util/List;

    .line 6691
    :cond_1
    iget-object v1, p0, Lmin;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6693
    :sswitch_0
    return-object p0

    .line 6698
    :sswitch_1
    iget-object v0, p0, Lmin;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 6699
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmin;->apiHeader:Llyr;

    .line 6701
    :cond_2
    iget-object v0, p0, Lmin;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6705
    :sswitch_2
    iget-object v0, p0, Lmin;->a:Lnwh;

    if-nez v0, :cond_3

    .line 6706
    new-instance v0, Lnwh;

    invoke-direct {v0}, Lnwh;-><init>()V

    iput-object v0, p0, Lmin;->a:Lnwh;

    .line 6708
    :cond_3
    iget-object v0, p0, Lmin;->a:Lnwh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6683
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6651
    iget-object v0, p0, Lmin;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 6652
    const/4 v0, 0x1

    iget-object v1, p0, Lmin;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6654
    :cond_0
    iget-object v0, p0, Lmin;->a:Lnwh;

    if-eqz v0, :cond_1

    .line 6655
    const/4 v0, 0x2

    iget-object v1, p0, Lmin;->a:Lnwh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6657
    :cond_1
    iget-object v0, p0, Lmin;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6659
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6636
    invoke-virtual {p0, p1}, Lmin;->a(Loxn;)Lmin;

    move-result-object v0

    return-object v0
.end method
