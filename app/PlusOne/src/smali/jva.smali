.class public final Ljva;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(I)J
    .locals 4

    .prologue
    .line 20
    const-wide/16 v0, 0x0

    .line 22
    and-int/lit8 v2, p0, 0x1

    if-eqz v2, :cond_0

    .line 23
    const-wide/16 v0, 0x20

    .line 25
    :cond_0
    and-int/lit8 v2, p0, 0x8

    if-eqz v2, :cond_1

    .line 26
    const-wide/16 v2, 0x80

    or-long/2addr v0, v2

    .line 28
    :cond_1
    and-int/lit8 v2, p0, 0x10

    if-eqz v2, :cond_2

    .line 29
    const-wide/32 v2, 0x400000

    or-long/2addr v0, v2

    .line 31
    :cond_2
    and-int/lit8 v2, p0, 0x20

    if-eqz v2, :cond_3

    .line 32
    const-wide/32 v2, 0x1000000

    or-long/2addr v0, v2

    .line 34
    :cond_3
    return-wide v0
.end method

.method public static b(I)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 42
    .line 44
    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_1

    .line 45
    const-wide/16 v0, 0x20

    .line 48
    :goto_0
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 49
    const-wide v0, 0x7fffffffffffffffL

    .line 52
    :cond_0
    return-wide v0

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method public static c(I)Z
    .locals 4

    .prologue
    .line 59
    invoke-static {p0}, Ljva;->b(I)J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
