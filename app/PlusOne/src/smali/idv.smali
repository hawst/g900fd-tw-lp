.class final Lidv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/nio/charset/Charset;

.field private static final s:S

.field private static final t:S

.field private static final u:S

.field private static final v:S

.field private static final w:S

.field private static final x:S

.field private static final y:S


# instance fields
.field private final b:Lido;

.field private final c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Liea;

.field private h:Lidy;

.field private i:Liea;

.field private j:Liea;

.field private k:Z

.field private l:Z

.field private m:I

.field private n:I

.field private o:[B

.field private p:I

.field private q:I

.field private final r:Lidp;

.field private final z:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lidv;->a:Ljava/nio/charset/Charset;

    .line 165
    sget v0, Lidp;->j:I

    .line 166
    invoke-static {v0}, Lidp;->a(I)S

    move-result v0

    sput-short v0, Lidv;->s:S

    .line 167
    sget v0, Lidp;->k:I

    invoke-static {v0}, Lidp;->a(I)S

    move-result v0

    sput-short v0, Lidv;->t:S

    .line 168
    sget v0, Lidp;->r:I

    .line 169
    invoke-static {v0}, Lidp;->a(I)S

    move-result v0

    sput-short v0, Lidv;->u:S

    .line 170
    sget v0, Lidp;->l:I

    .line 171
    invoke-static {v0}, Lidp;->a(I)S

    move-result v0

    sput-short v0, Lidv;->v:S

    .line 172
    sget v0, Lidp;->m:I

    .line 173
    invoke-static {v0}, Lidp;->a(I)S

    move-result v0

    sput-short v0, Lidv;->w:S

    .line 174
    sget v0, Lidp;->e:I

    .line 175
    invoke-static {v0}, Lidp;->a(I)S

    move-result v0

    sput-short v0, Lidv;->x:S

    .line 176
    sget v0, Lidp;->g:I

    .line 177
    invoke-static {v0}, Lidp;->a(I)S

    move-result v0

    sput-short v0, Lidv;->y:S

    .line 176
    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;ILidp;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput v4, p0, Lidv;->d:I

    .line 149
    iput v4, p0, Lidv;->e:I

    .line 157
    iput-boolean v4, p0, Lidv;->l:Z

    .line 159
    iput v4, p0, Lidv;->n:I

    .line 179
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    .line 203
    if-nez p1, :cond_0

    .line 204
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null argument inputStream to ExifParser"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    iput-object p3, p0, Lidv;->r:Lidp;

    .line 210
    invoke-direct {p0, p1}, Lidv;->a(Ljava/io/InputStream;)Z

    move-result v0

    iput-boolean v0, p0, Lidv;->l:Z

    .line 211
    new-instance v0, Lido;

    invoke-direct {v0, p1}, Lido;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lidv;->b:Lido;

    .line 212
    iput p2, p0, Lidv;->c:I

    .line 213
    iget-boolean v0, p0, Lidv;->l:Z

    if-nez v0, :cond_2

    .line 231
    :cond_1
    :goto_0
    return-void

    .line 217
    :cond_2
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->c()S

    move-result v0

    const/16 v1, 0x4949

    if-ne v1, v0, :cond_3

    iget-object v0, p0, Lidv;->b:Lido;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Lido;->a(Ljava/nio/ByteOrder;)V

    :goto_1
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->c()S

    move-result v0

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_5

    new-instance v0, Lidr;

    const-string v1, "Invalid TIFF header"

    invoke-direct {v0, v1}, Lidr;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/16 v1, 0x4d4d

    if-ne v1, v0, :cond_4

    iget-object v0, p0, Lidv;->b:Lido;

    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Lido;->a(Ljava/nio/ByteOrder;)V

    goto :goto_1

    :cond_4
    new-instance v0, Lidr;

    const-string v1, "Invalid TIFF header"

    invoke-direct {v0, v1}, Lidr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_5
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->f()J

    move-result-wide v0

    .line 219
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    .line 220
    new-instance v2, Lidr;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x23

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid offset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lidr;-><init>(Ljava/lang/String;)V

    throw v2

    .line 222
    :cond_6
    long-to-int v2, v0

    iput v2, p0, Lidv;->p:I

    .line 223
    iput v4, p0, Lidv;->f:I

    .line 224
    invoke-direct {p0, v4}, Lidv;->b(I)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-direct {p0}, Lidv;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 225
    :cond_7
    invoke-direct {p0, v4, v0, v1}, Lidv;->a(IJ)V

    .line 226
    const-wide/16 v2, 0x8

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 227
    long-to-int v0, v0

    add-int/lit8 v0, v0, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lidv;->o:[B

    .line 228
    iget-object v0, p0, Lidv;->o:[B

    invoke-virtual {p0, v0}, Lidv;->a([B)I

    goto/16 :goto_0
.end method

.method protected static a(Ljava/io/InputStream;ILidp;)Lidv;
    .locals 1

    .prologue
    .line 241
    new-instance v0, Lidv;

    invoke-direct {v0, p0, p1, p2}, Lidv;-><init>(Ljava/io/InputStream;ILidp;)V

    return-object v0
.end method

.method protected static a(Ljava/io/InputStream;Lidp;)Lidv;
    .locals 2

    .prologue
    .line 254
    new-instance v0, Lidv;

    const/16 v1, 0x3f

    invoke-direct {v0, p0, v1, p1}, Lidv;-><init>(Ljava/io/InputStream;ILidp;)V

    return-object v0
.end method

.method private a(IJ)V
    .locals 4

    .prologue
    .line 522
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lidx;

    invoke-direct {p0, p1}, Lidv;->b(I)Z

    move-result v3

    invoke-direct {v2, p1, v3}, Lidx;-><init>(IZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    return-void
.end method

.method private a(II)Z
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lidv;->r:Lidp;

    invoke-virtual {v0}, Lidp;->d()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 641
    if-nez v0, :cond_0

    .line 642
    const/4 v0, 0x0

    .line 644
    :goto_0
    return v0

    :cond_0
    invoke-static {v0, p1}, Lidp;->k(II)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 756
    new-instance v3, Lido;

    invoke-direct {v3, p1}, Lido;-><init>(Ljava/io/InputStream;)V

    .line 757
    invoke-virtual {v3}, Lido;->c()S

    move-result v1

    const/16 v2, -0x28

    if-eq v1, v2, :cond_0

    .line 758
    new-instance v0, Lidr;

    const-string v1, "Invalid JPEG format"

    invoke-direct {v0, v1}, Lidr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 761
    :cond_0
    invoke-virtual {v3}, Lido;->c()S

    move-result v1

    move v2, v1

    .line 762
    :goto_0
    const/16 v1, -0x27

    if-eq v2, v1, :cond_1

    .line 763
    invoke-static {v2}, Liec;->a(S)Z

    move-result v1

    if-nez v1, :cond_1

    .line 764
    invoke-virtual {v3}, Lido;->d()I

    move-result v1

    .line 767
    const/16 v4, -0x1f

    if-ne v2, v4, :cond_2

    .line 768
    const/16 v2, 0x8

    if-lt v1, v2, :cond_2

    .line 771
    invoke-virtual {v3}, Lido;->e()I

    move-result v2

    .line 772
    invoke-virtual {v3}, Lido;->c()S

    move-result v4

    .line 773
    add-int/lit8 v1, v1, -0x6

    .line 774
    const v5, 0x45786966

    if-ne v2, v5, :cond_2

    if-nez v4, :cond_2

    .line 775
    invoke-virtual {v3}, Lido;->a()I

    move-result v0

    iput v0, p0, Lidv;->q:I

    .line 776
    iput v1, p0, Lidv;->m:I

    .line 777
    iget v0, p0, Lidv;->q:I

    iget v1, p0, Lidv;->m:I

    add-int/2addr v0, v1

    iput v0, p0, Lidv;->n:I

    .line 778
    const/4 v0, 0x1

    .line 788
    :cond_1
    return v0

    .line 782
    :cond_2
    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    add-int/lit8 v2, v1, -0x2

    int-to-long v4, v2

    add-int/lit8 v1, v1, -0x2

    int-to-long v6, v1

    invoke-virtual {v3, v6, v7}, Lido;->skip(J)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    .line 786
    invoke-virtual {v3}, Lido;->c()S

    move-result v1

    move v2, v1

    .line 787
    goto :goto_0
.end method

.method private b(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 182
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 194
    :cond_0
    :goto_0
    return v0

    .line 184
    :pswitch_0
    iget v2, p0, Lidv;->c:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 186
    :pswitch_1
    iget v2, p0, Lidv;->c:I

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 188
    :pswitch_2
    iget v2, p0, Lidv;->c:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 190
    :pswitch_3
    iget v2, p0, Lidv;->c:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 192
    :pswitch_4
    iget v2, p0, Lidv;->c:I

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 498
    iget-object v0, p0, Lidv;->b:Lido;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lido;->b(J)V

    .line 499
    :goto_0
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 500
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    goto :goto_0

    .line 502
    :cond_0
    return-void
.end method

.method private c(Liea;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v0, 0x0

    .line 589
    invoke-virtual {p1}, Liea;->e()I

    move-result v1

    if-nez v1, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    invoke-virtual {p1}, Liea;->b()S

    move-result v1

    .line 593
    invoke-virtual {p1}, Liea;->a()I

    move-result v2

    .line 594
    sget-short v3, Lidv;->s:S

    if-ne v1, v3, :cond_3

    sget v3, Lidp;->j:I

    invoke-direct {p0, v2, v3}, Lidv;->a(II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 595
    invoke-direct {p0, v6}, Lidv;->b(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 596
    invoke-direct {p0, v4}, Lidv;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    :cond_2
    invoke-virtual {p1, v0}, Liea;->e(I)J

    move-result-wide v0

    invoke-direct {p0, v6, v0, v1}, Lidv;->a(IJ)V

    goto :goto_0

    .line 599
    :cond_3
    sget-short v3, Lidv;->t:S

    if-ne v1, v3, :cond_4

    sget v3, Lidp;->k:I

    invoke-direct {p0, v2, v3}, Lidv;->a(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 600
    invoke-direct {p0, v5}, Lidv;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 601
    invoke-virtual {p1, v0}, Liea;->e(I)J

    move-result-wide v0

    invoke-direct {p0, v5, v0, v1}, Lidv;->a(IJ)V

    goto :goto_0

    .line 603
    :cond_4
    sget-short v3, Lidv;->u:S

    if-ne v1, v3, :cond_5

    sget v3, Lidp;->r:I

    .line 604
    invoke-direct {p0, v2, v3}, Lidv;->a(II)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 605
    invoke-direct {p0, v4}, Lidv;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 606
    invoke-virtual {p1, v0}, Liea;->e(I)J

    move-result-wide v0

    invoke-direct {p0, v4, v0, v1}, Lidv;->a(IJ)V

    goto :goto_0

    .line 608
    :cond_5
    sget-short v3, Lidv;->v:S

    if-ne v1, v3, :cond_6

    sget v3, Lidp;->l:I

    .line 609
    invoke-direct {p0, v2, v3}, Lidv;->a(II)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 610
    invoke-direct {p0}, Lidv;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 611
    invoke-virtual {p1, v0}, Liea;->e(I)J

    move-result-wide v0

    iget-object v2, p0, Lidv;->z:Ljava/util/TreeMap;

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lidy;

    invoke-direct {v1, v4}, Lidy;-><init>(I)V

    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 613
    :cond_6
    sget-short v3, Lidv;->w:S

    if-ne v1, v3, :cond_7

    sget v3, Lidp;->m:I

    .line 614
    invoke-direct {p0, v2, v3}, Lidv;->a(II)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 615
    invoke-direct {p0}, Lidv;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 616
    iput-object p1, p0, Lidv;->j:Liea;

    goto/16 :goto_0

    .line 618
    :cond_7
    sget-short v3, Lidv;->x:S

    if-ne v1, v3, :cond_9

    sget v3, Lidp;->e:I

    invoke-direct {p0, v2, v3}, Lidv;->a(II)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 619
    invoke-direct {p0}, Lidv;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 620
    invoke-virtual {p1}, Liea;->f()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 621
    :goto_1
    invoke-virtual {p1}, Liea;->e()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 622
    invoke-virtual {p1}, Liea;->c()S

    .line 623
    invoke-virtual {p1, v0}, Liea;->e(I)J

    move-result-wide v2

    iget-object v1, p0, Lidv;->z:Ljava/util/TreeMap;

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lidy;

    invoke-direct {v3, v5, v0}, Lidy;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 629
    :cond_8
    iget-object v1, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {p1}, Liea;->o()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lidw;

    invoke-direct {v3, p1, v0}, Lidw;-><init>(Liea;Z)V

    invoke-virtual {v1, v2, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 632
    :cond_9
    sget-short v0, Lidv;->y:S

    if-ne v1, v0, :cond_0

    sget v0, Lidp;->g:I

    .line 633
    invoke-direct {p0, v2, v0}, Lidv;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    invoke-direct {p0}, Lidv;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Liea;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    iput-object p1, p0, Lidv;->i:Liea;

    goto/16 :goto_0
.end method

.method private p()Z
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lidv;->c:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 392
    iget v2, p0, Lidv;->f:I

    packed-switch v2, :pswitch_data_0

    .line 403
    :cond_0
    :goto_0
    return v0

    .line 394
    :pswitch_0
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lidv;->b(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lidv;->b(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 395
    invoke-direct {p0, v3}, Lidv;->b(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 396
    invoke-direct {p0, v1}, Lidv;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 398
    :pswitch_1
    invoke-direct {p0}, Lidv;->p()Z

    move-result v0

    goto :goto_0

    .line 401
    :pswitch_2
    invoke-direct {p0, v3}, Lidv;->b(I)Z

    move-result v0

    goto :goto_0

    .line 392
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r()Liea;
    .locals 12

    .prologue
    const-wide/32 v10, 0x7fffffff

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 535
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->c()S

    move-result v1

    .line 536
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->c()S

    move-result v2

    .line 537
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->f()J

    move-result-wide v8

    .line 538
    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    .line 539
    new-instance v0, Lidr;

    const-string v1, "Number of component is larger then Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Lidr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_0
    invoke-static {v2}, Liea;->a(S)Z

    move-result v0

    if-nez v0, :cond_1

    .line 544
    const-string v0, "Tag %04x: Invalid data type %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 545
    iget-object v0, p0, Lidv;->b:Lido;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lido;->skip(J)J

    .line 546
    const/4 v0, 0x0

    .line 580
    :goto_0
    return-object v0

    .line 549
    :cond_1
    new-instance v0, Liea;

    long-to-int v3, v8

    iget v4, p0, Lidv;->f:I

    long-to-int v7, v8

    if-eqz v7, :cond_2

    :goto_1
    invoke-direct/range {v0 .. v5}, Liea;-><init>(SSIIZ)V

    .line 551
    invoke-virtual {v0}, Liea;->d()I

    move-result v1

    .line 552
    const/4 v3, 0x4

    if-le v1, v3, :cond_5

    .line 553
    iget-object v1, p0, Lidv;->b:Lido;

    invoke-virtual {v1}, Lido;->f()J

    move-result-wide v4

    .line 554
    cmp-long v1, v4, v10

    if-lez v1, :cond_3

    .line 555
    new-instance v0, Lidr;

    const-string v1, "offset is larger then Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Lidr;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v5, v6

    .line 549
    goto :goto_1

    .line 560
    :cond_3
    iget-object v1, p0, Lidv;->o:[B

    if-eqz v1, :cond_4

    iget v1, p0, Lidv;->p:I

    int-to-long v10, v1

    cmp-long v1, v4, v10

    if-gez v1, :cond_4

    const/4 v1, 0x7

    if-ne v2, v1, :cond_4

    .line 562
    long-to-int v1, v8

    new-array v1, v1, [B

    .line 563
    iget-object v2, p0, Lidv;->o:[B

    long-to-int v3, v4

    add-int/lit8 v3, v3, -0x8

    long-to-int v4, v8

    invoke-static {v2, v3, v1, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 565
    invoke-virtual {v0, v1}, Liea;->a([B)Z

    goto :goto_0

    .line 567
    :cond_4
    long-to-int v1, v4

    invoke-virtual {v0, v1}, Liea;->g(I)V

    goto :goto_0

    .line 570
    :cond_5
    invoke-virtual {v0}, Liea;->p()Z

    move-result v2

    .line 572
    invoke-virtual {v0, v6}, Liea;->a(Z)V

    .line 574
    invoke-virtual {p0, v0}, Lidv;->b(Liea;)V

    .line 575
    invoke-virtual {v0, v2}, Liea;->a(Z)V

    .line 576
    iget-object v2, p0, Lidv;->b:Lido;

    rsub-int/lit8 v1, v1, 0x4

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lido;->skip(J)J

    .line 578
    iget-object v1, p0, Lidv;->b:Lido;

    invoke-virtual {v1}, Lido;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Liea;->g(I)V

    goto :goto_0
.end method


# virtual methods
.method protected a()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x5

    const/4 v0, 0x1

    .line 272
    iget-boolean v3, p0, Lidv;->l:Z

    if-nez v3, :cond_1

    move v0, v2

    .line 353
    :cond_0
    :goto_0
    return v0

    .line 275
    :cond_1
    iget-object v3, p0, Lidv;->b:Lido;

    invoke-virtual {v3}, Lido;->a()I

    move-result v3

    .line 276
    iget v4, p0, Lidv;->d:I

    add-int/lit8 v4, v4, 0x2

    iget v5, p0, Lidv;->e:I

    mul-int/lit8 v5, v5, 0xc

    add-int/2addr v4, v5

    .line 277
    if-ge v3, v4, :cond_3

    .line 278
    invoke-direct {p0}, Lidv;->r()Liea;

    move-result-object v1

    iput-object v1, p0, Lidv;->g:Liea;

    .line 279
    iget-object v1, p0, Lidv;->g:Liea;

    if-nez v1, :cond_2

    .line 280
    invoke-virtual {p0}, Lidv;->a()I

    move-result v0

    goto :goto_0

    .line 282
    :cond_2
    iget-boolean v1, p0, Lidv;->k:Z

    if-eqz v1, :cond_0

    .line 283
    iget-object v1, p0, Lidv;->g:Liea;

    invoke-direct {p0, v1}, Lidv;->c(Liea;)V

    goto :goto_0

    .line 286
    :cond_3
    if-ne v3, v4, :cond_5

    .line 288
    iget v3, p0, Lidv;->f:I

    if-nez v3, :cond_6

    .line 289
    invoke-virtual {p0}, Lidv;->k()J

    move-result-wide v4

    .line 290
    invoke-direct {p0, v0}, Lidv;->b(I)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lidv;->p()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 291
    :cond_4
    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    .line 292
    invoke-direct {p0, v0, v4, v5}, Lidv;->a(IJ)V

    .line 312
    :cond_5
    :goto_1
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 313
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v3

    .line 314
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 316
    :try_start_0
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lidv;->c(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    instance-of v0, v1, Lidx;

    if-eqz v0, :cond_a

    move-object v0, v1

    .line 323
    check-cast v0, Lidx;

    iget v0, v0, Lidx;->a:I

    iput v0, p0, Lidv;->f:I

    .line 324
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->d()I

    move-result v0

    iput v0, p0, Lidv;->e:I

    .line 325
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lidv;->d:I

    .line 327
    iget v0, p0, Lidv;->e:I

    mul-int/lit8 v0, v0, 0xc

    iget v3, p0, Lidv;->d:I

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x2

    iget v3, p0, Lidv;->m:I

    if-le v0, v3, :cond_8

    .line 328
    iget v0, p0, Lidv;->f:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid size of IFD "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v0, v2

    .line 329
    goto/16 :goto_0

    .line 298
    :cond_6
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 299
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lidv;->b:Lido;

    .line 300
    invoke-virtual {v3}, Lido;->a()I

    move-result v3

    sub-int/2addr v0, v3

    .line 302
    :goto_2
    if-ge v0, v1, :cond_7

    .line 303
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x2d

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid size of link to next IFD: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 305
    :cond_7
    invoke-virtual {p0}, Lidv;->k()J

    move-result-wide v0

    .line 306
    cmp-long v3, v0, v6

    if-eqz v3, :cond_5

    .line 307
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2e

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid link to next IFD: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 318
    :catch_0
    move-exception v0

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 319
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x39

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failed to skip to data at: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " for "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", the file may be broken."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 332
    :cond_8
    invoke-direct {p0}, Lidv;->q()Z

    move-result v0

    iput-boolean v0, p0, Lidv;->k:Z

    .line 333
    check-cast v1, Lidx;

    iget-boolean v0, v1, Lidx;->b:Z

    if-eqz v0, :cond_9

    .line 334
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 336
    :cond_9
    invoke-virtual {p0}, Lidv;->b()V

    goto/16 :goto_1

    .line 338
    :cond_a
    instance-of v0, v1, Lidy;

    if-eqz v0, :cond_b

    .line 339
    check-cast v1, Lidy;

    iput-object v1, p0, Lidv;->h:Lidy;

    .line 340
    iget-object v0, p0, Lidv;->h:Lidy;

    iget v0, v0, Lidy;->b:I

    goto/16 :goto_0

    .line 342
    :cond_b
    check-cast v1, Lidw;

    .line 343
    iget-object v0, v1, Lidw;->a:Liea;

    iput-object v0, p0, Lidv;->g:Liea;

    .line 344
    iget-object v0, p0, Lidv;->g:Liea;

    invoke-virtual {v0}, Liea;->c()S

    move-result v0

    const/4 v3, 0x7

    if-eq v0, v3, :cond_c

    .line 345
    iget-object v0, p0, Lidv;->g:Liea;

    invoke-virtual {p0, v0}, Lidv;->b(Liea;)V

    .line 346
    iget-object v0, p0, Lidv;->g:Liea;

    invoke-direct {p0, v0}, Lidv;->c(Liea;)V

    .line 348
    :cond_c
    iget-boolean v0, v1, Lidw;->b:Z

    if-eqz v0, :cond_5

    .line 349
    const/4 v0, 0x2

    goto/16 :goto_0

    :cond_d
    move v0, v2

    .line 353
    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_2
.end method

.method protected a([B)I
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0, p1}, Lido;->read([B)I

    move-result v0

    return v0
.end method

.method protected a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 819
    sget-object v0, Lidv;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p0, p1, v0}, Lidv;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(ILjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 828
    if-lez p1, :cond_0

    .line 829
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0, p1, p2}, Lido;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    .line 831
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method protected a(Liea;)V
    .locals 4

    .prologue
    .line 514
    invoke-virtual {p1}, Liea;->o()I

    move-result v0

    iget-object v1, p0, Lidv;->b:Lido;

    invoke-virtual {v1}, Lido;->a()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 515
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {p1}, Liea;->o()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lidw;

    const/4 v3, 0x1

    invoke-direct {v2, p1, v3}, Lidw;-><init>(Liea;Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 364
    iget v0, p0, Lidv;->d:I

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, Lidv;->e:I

    mul-int/lit8 v1, v1, 0xc

    add-int/2addr v1, v0

    .line 365
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->a()I

    move-result v0

    .line 366
    if-le v0, v1, :cond_1

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-boolean v2, p0, Lidv;->k:Z

    if-eqz v2, :cond_3

    .line 370
    :cond_2
    :goto_1
    if-ge v0, v1, :cond_4

    .line 371
    invoke-direct {p0}, Lidv;->r()Liea;

    move-result-object v2

    iput-object v2, p0, Lidv;->g:Liea;

    .line 372
    add-int/lit8 v0, v0, 0xc

    .line 373
    iget-object v2, p0, Lidv;->g:Liea;

    if-eqz v2, :cond_2

    .line 374
    iget-object v2, p0, Lidv;->g:Liea;

    invoke-direct {p0, v2}, Lidv;->c(Liea;)V

    goto :goto_1

    .line 379
    :cond_3
    invoke-direct {p0, v1}, Lidv;->c(I)V

    .line 381
    :cond_4
    invoke-virtual {p0}, Lidv;->k()J

    move-result-wide v0

    .line 383
    iget v2, p0, Lidv;->f:I

    if-nez v2, :cond_0

    .line 384
    invoke-direct {p0, v4}, Lidv;->b(I)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0}, Lidv;->p()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    :cond_5
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 386
    invoke-direct {p0, v4, v0, v1}, Lidv;->a(IJ)V

    goto :goto_0
.end method

.method protected b(Liea;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 649
    invoke-virtual {p1}, Liea;->c()S

    move-result v0

    .line 650
    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x7

    if-eq v0, v2, :cond_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 652
    :cond_0
    invoke-virtual {p1}, Liea;->e()I

    move-result v2

    .line 653
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 654
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lidv;->b:Lido;

    invoke-virtual {v3}, Lido;->a()I

    move-result v3

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_1

    .line 656
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 657
    instance-of v2, v0, Lidy;

    if-eqz v2, :cond_3

    .line 659
    const-string v0, "Thumbnail overlaps value for tag: \n"

    invoke-virtual {p1}, Liea;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 660
    :goto_0
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    .line 661
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid thumbnail offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    :cond_1
    :goto_1
    invoke-virtual {p1}, Liea;->c()S

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 736
    :goto_2
    :pswitch_0
    return-void

    .line 659
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 664
    :cond_3
    instance-of v2, v0, Lidx;

    if-eqz v2, :cond_5

    .line 665
    check-cast v0, Lidx;

    iget v0, v0, Lidx;->a:I

    .line 666
    invoke-virtual {p1}, Liea;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x29

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Ifd "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " overlaps value for tag: \n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 668
    :cond_4
    :goto_3
    iget-object v0, p0, Lidv;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lidv;->b:Lido;

    .line 673
    invoke-virtual {v2}, Lido;->a()I

    move-result v2

    sub-int/2addr v0, v2

    .line 674
    invoke-virtual {p1}, Liea;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x34

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid size of tag: \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " setting count to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 676
    invoke-virtual {p1, v0}, Liea;->c(I)V

    goto :goto_1

    .line 667
    :cond_5
    instance-of v2, v0, Lidw;

    if-eqz v2, :cond_4

    .line 668
    check-cast v0, Lidw;

    iget-object v0, v0, Lidw;->a:Liea;

    .line 669
    invoke-virtual {v0}, Liea;->toString()Ljava/lang/String;

    move-result-object v0

    .line 670
    invoke-virtual {p1}, Liea;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2e

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Tag value for tag: \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " overlaps value for tag: \n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 684
    :pswitch_1
    invoke-virtual {p1}, Liea;->e()I

    move-result v0

    new-array v0, v0, [B

    .line 685
    invoke-virtual {p0, v0}, Lidv;->a([B)I

    .line 686
    invoke-virtual {p1, v0}, Liea;->a([B)Z

    goto/16 :goto_2

    .line 690
    :pswitch_2
    invoke-virtual {p1}, Liea;->e()I

    move-result v0

    invoke-virtual {p0, v0}, Lidv;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Liea;->a(Ljava/lang/String;)Z

    goto/16 :goto_2

    .line 693
    :pswitch_3
    invoke-virtual {p1}, Liea;->e()I

    move-result v0

    new-array v0, v0, [J

    .line 694
    array-length v2, v0

    :goto_4
    if-ge v1, v2, :cond_6

    .line 695
    invoke-virtual {p0}, Lidv;->k()J

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 694
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 697
    :cond_6
    invoke-virtual {p1, v0}, Liea;->a([J)Z

    goto/16 :goto_2

    .line 701
    :pswitch_4
    invoke-virtual {p1}, Liea;->e()I

    move-result v0

    new-array v0, v0, [Liee;

    .line 702
    array-length v2, v0

    :goto_5
    if-ge v1, v2, :cond_7

    .line 703
    invoke-virtual {p0}, Lidv;->l()Liee;

    move-result-object v3

    aput-object v3, v0, v1

    .line 702
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 705
    :cond_7
    invoke-virtual {p1, v0}, Liea;->a([Liee;)Z

    goto/16 :goto_2

    .line 709
    :pswitch_5
    invoke-virtual {p1}, Liea;->e()I

    move-result v0

    new-array v0, v0, [I

    .line 710
    array-length v2, v0

    :goto_6
    if-ge v1, v2, :cond_8

    .line 711
    invoke-virtual {p0}, Lidv;->j()I

    move-result v3

    aput v3, v0, v1

    .line 710
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 713
    :cond_8
    invoke-virtual {p1, v0}, Liea;->a([I)Z

    goto/16 :goto_2

    .line 717
    :pswitch_6
    invoke-virtual {p1}, Liea;->e()I

    move-result v0

    new-array v0, v0, [I

    .line 718
    array-length v2, v0

    :goto_7
    if-ge v1, v2, :cond_9

    .line 719
    invoke-virtual {p0}, Lidv;->m()I

    move-result v3

    aput v3, v0, v1

    .line 718
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 721
    :cond_9
    invoke-virtual {p1, v0}, Liea;->a([I)Z

    goto/16 :goto_2

    .line 725
    :pswitch_7
    invoke-virtual {p1}, Liea;->e()I

    move-result v0

    new-array v2, v0, [Liee;

    .line 726
    array-length v3, v2

    move v0, v1

    :goto_8
    if-ge v0, v3, :cond_a

    .line 727
    invoke-virtual {p0}, Lidv;->n()Liee;

    move-result-object v1

    aput-object v1, v2, v0

    .line 726
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 729
    :cond_a
    invoke-virtual {p1, v2}, Liea;->a([Liee;)Z

    goto/16 :goto_2

    .line 681
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected c()Liea;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lidv;->g:Liea;

    return-object v0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lidv;->f:I

    return v0
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lidv;->h:Lidy;

    iget v0, v0, Lidy;->a:I

    return v0
.end method

.method protected f()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 480
    iget-object v1, p0, Lidv;->i:Liea;

    if-nez v1, :cond_0

    .line 483
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lidv;->i:Liea;

    invoke-virtual {v1, v0}, Liea;->e(I)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method protected g()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 491
    iget-object v1, p0, Lidv;->j:Liea;

    if-nez v1, :cond_0

    .line 494
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lidv;->j:Liea;

    invoke-virtual {v1, v0}, Liea;->e(I)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method protected h()I
    .locals 1

    .prologue
    .line 792
    iget v0, p0, Lidv;->n:I

    return v0
.end method

.method protected i()I
    .locals 1

    .prologue
    .line 796
    iget v0, p0, Lidv;->q:I

    return v0
.end method

.method protected j()I
    .locals 2

    .prologue
    .line 840
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->c()S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    return v0
.end method

.method protected k()J
    .locals 4

    .prologue
    .line 848
    invoke-virtual {p0}, Lidv;->m()I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method protected l()Liee;
    .locals 5

    .prologue
    .line 856
    invoke-virtual {p0}, Lidv;->k()J

    move-result-wide v0

    .line 857
    invoke-virtual {p0}, Lidv;->k()J

    move-result-wide v2

    .line 858
    new-instance v4, Liee;

    invoke-direct {v4, v0, v1, v2, v3}, Liee;-><init>(JJ)V

    return-object v4
.end method

.method protected m()I
    .locals 1

    .prologue
    .line 865
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->e()I

    move-result v0

    return v0
.end method

.method protected n()Liee;
    .locals 6

    .prologue
    .line 872
    invoke-virtual {p0}, Lidv;->m()I

    move-result v0

    .line 873
    invoke-virtual {p0}, Lidv;->m()I

    move-result v1

    .line 874
    new-instance v2, Liee;

    int-to-long v4, v0

    int-to-long v0, v1

    invoke-direct {v2, v4, v5, v0, v1}, Liee;-><init>(JJ)V

    return-object v2
.end method

.method protected o()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lidv;->b:Lido;

    invoke-virtual {v0}, Lido;->b()Ljava/nio/ByteOrder;

    move-result-object v0

    return-object v0
.end method
