.class public final Ldqi;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:Z

.field private f:Lhgw;

.field private h:Lhgw;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 34
    const-string v0, "UpdateCollectionTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 36
    iput p2, p0, Ldqi;->a:I

    .line 37
    iput-object p3, p0, Ldqi;->b:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Ldqi;->c:Ljava/lang/String;

    .line 40
    iput-boolean p5, p0, Ldqi;->d:Z

    .line 41
    iput-boolean p6, p0, Ldqi;->e:Z

    .line 42
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 58
    invoke-virtual {p0}, Ldqi;->f()Landroid/content/Context;

    move-result-object v1

    .line 60
    const-class v0, Lkfd;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lkfd;

    .line 63
    iget v0, p0, Ldqi;->a:I

    invoke-static {v1, v0}, Lkgi;->a(Landroid/content/Context;I)Lkfu;

    move-result-object v8

    .line 65
    new-instance v0, Ldoa;

    iget v2, p0, Ldqi;->a:I

    iget-object v3, p0, Ldqi;->b:Ljava/lang/String;

    iget-object v4, p0, Ldqi;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Ldoa;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-boolean v2, p0, Ldqi;->d:Z

    invoke-virtual {v0, v2}, Ldoa;->a(Z)V

    .line 68
    invoke-virtual {v8, v0}, Lkfu;->a(Lkff;)V

    .line 69
    new-instance v0, Ldoc;

    iget v2, p0, Ldqi;->a:I

    iget-object v3, p0, Ldqi;->b:Ljava/lang/String;

    iget-object v4, p0, Ldqi;->c:Ljava/lang/String;

    iget-boolean v5, p0, Ldqi;->e:Z

    invoke-direct/range {v0 .. v5}, Ldoc;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v8, v0}, Lkfu;->a(Lkff;)V

    .line 73
    iget-object v0, p0, Ldqi;->f:Lhgw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqi;->f:Lhgw;

    invoke-virtual {v0}, Lhgw;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Ldqi;->h:Lhgw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldqi;->h:Lhgw;

    invoke-virtual {v0}, Lhgw;->k()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 74
    new-instance v0, Ldnz;

    iget v2, p0, Ldqi;->a:I

    iget-object v3, p0, Ldqi;->b:Ljava/lang/String;

    iget-object v4, p0, Ldqi;->c:Ljava/lang/String;

    iget-object v5, p0, Ldqi;->f:Lhgw;

    iget-object v6, p0, Ldqi;->h:Lhgw;

    invoke-direct/range {v0 .. v6}, Ldnz;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lhgw;Lhgw;)V

    invoke-virtual {v8, v0}, Lkfu;->a(Lkff;)V

    .line 78
    :cond_2
    invoke-interface {v7, v8}, Lkfd;->a(Lkff;)V

    .line 80
    new-instance v0, Lhoz;

    iget v1, v8, Lkff;->i:I

    invoke-direct {v0, v1, v9, v9}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v0

    .line 73
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lhgw;Lhgw;)V
    .locals 1

    .prologue
    .line 52
    invoke-static {p1, p2}, Lhgw;->b(Lhgw;Lhgw;)Lhgw;

    move-result-object v0

    iput-object v0, p0, Ldqi;->h:Lhgw;

    .line 53
    invoke-static {p2, p1}, Lhgw;->b(Lhgw;Lhgw;)Lhgw;

    move-result-object v0

    iput-object v0, p0, Ldqi;->f:Lhgw;

    .line 54
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Ldqi;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0591

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
