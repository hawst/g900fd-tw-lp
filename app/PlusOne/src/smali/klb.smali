.class public Lklb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private a:Lkkz;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lklc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lklb;->b:Ljava/util/ArrayList;

    .line 26
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 28
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lklb;->a:Lkkz;

    .line 39
    iget-object v0, p0, Lklb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lklc;

    .line 40
    invoke-interface {v0}, Lklc;->y()V

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    const-string v0, "selected"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkkz;

    iput-object v0, p0, Lklb;->a:Lkkz;

    .line 61
    :cond_0
    return-void
.end method

.method public a(Lkkz;)V
    .locals 2

    .prologue
    .line 31
    iput-object p1, p0, Lklb;->a:Lkkz;

    .line 32
    iget-object v0, p0, Lklb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lklc;

    .line 33
    invoke-interface {v0}, Lklc;->y()V

    goto :goto_0

    .line 35
    :cond_0
    return-void
.end method

.method public a(Lklc;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lklb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public b()Lkkz;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lklb;->a:Lkkz;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 65
    const-string v0, "selected"

    iget-object v1, p0, Lklb;->a:Lkkz;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 66
    return-void
.end method

.method public b(Lklc;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lklb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method
