.class public final Lopt;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lorh;

.field private b:Lorh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 347
    invoke-direct {p0}, Loxq;-><init>()V

    .line 350
    iput-object v0, p0, Lopt;->a:Lorh;

    .line 353
    iput-object v0, p0, Lopt;->b:Lorh;

    .line 347
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 371
    iget-object v1, p0, Lopt;->a:Lorh;

    if-eqz v1, :cond_0

    .line 372
    const/4 v0, 0x1

    iget-object v1, p0, Lopt;->a:Lorh;

    .line 373
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 375
    :cond_0
    iget-object v1, p0, Lopt;->b:Lorh;

    if-eqz v1, :cond_1

    .line 376
    const/4 v1, 0x2

    iget-object v2, p0, Lopt;->b:Lorh;

    .line 377
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_1
    iget-object v1, p0, Lopt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 380
    iput v0, p0, Lopt;->ai:I

    .line 381
    return v0
.end method

.method public a(Loxn;)Lopt;
    .locals 2

    .prologue
    .line 389
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 390
    sparse-switch v0, :sswitch_data_0

    .line 394
    iget-object v1, p0, Lopt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 395
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lopt;->ah:Ljava/util/List;

    .line 398
    :cond_1
    iget-object v1, p0, Lopt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    :sswitch_0
    return-object p0

    .line 405
    :sswitch_1
    iget-object v0, p0, Lopt;->a:Lorh;

    if-nez v0, :cond_2

    .line 406
    new-instance v0, Lorh;

    invoke-direct {v0}, Lorh;-><init>()V

    iput-object v0, p0, Lopt;->a:Lorh;

    .line 408
    :cond_2
    iget-object v0, p0, Lopt;->a:Lorh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 412
    :sswitch_2
    iget-object v0, p0, Lopt;->b:Lorh;

    if-nez v0, :cond_3

    .line 413
    new-instance v0, Lorh;

    invoke-direct {v0}, Lorh;-><init>()V

    iput-object v0, p0, Lopt;->b:Lorh;

    .line 415
    :cond_3
    iget-object v0, p0, Lopt;->b:Lorh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 390
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lopt;->a:Lorh;

    if-eqz v0, :cond_0

    .line 359
    const/4 v0, 0x1

    iget-object v1, p0, Lopt;->a:Lorh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 361
    :cond_0
    iget-object v0, p0, Lopt;->b:Lorh;

    if-eqz v0, :cond_1

    .line 362
    const/4 v0, 0x2

    iget-object v1, p0, Lopt;->b:Lorh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 364
    :cond_1
    iget-object v0, p0, Lopt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 366
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 343
    invoke-virtual {p0, p1}, Lopt;->a(Loxn;)Lopt;

    move-result-object v0

    return-object v0
.end method
