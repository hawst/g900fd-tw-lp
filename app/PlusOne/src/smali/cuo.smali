.class public final Lcuo;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "PG"


# instance fields
.field private a:Lhei;

.field private b:Ldvz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 255
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lcuo;->a:Lhei;

    .line 256
    return-void
.end method

.method private a(ILdvz;Landroid/content/SyncResult;Z)V
    .locals 6

    .prologue
    .line 510
    :try_start_0
    const-string v0, "PhotoSyncService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x42

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "----> Start highlights metadata down sync for account: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 514
    :cond_0
    if-eqz p4, :cond_2

    sget-object v0, Ldts;->f:Ldts;

    .line 517
    :goto_0
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p2, v0}, Ldtd;->a(Landroid/content/Context;ILdvz;Ldts;)V

    .line 520
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lfit;->b:Lfit;

    .line 521
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 520
    invoke-static {v0, p1, v1, v2, v3}, Ldhv;->a(Landroid/content/Context;ILfit;J)V

    .line 532
    :cond_1
    :goto_1
    return-void

    .line 514
    :cond_2
    sget-object v0, Ldts;->e:Ldts;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 522
    :catch_0
    move-exception v0

    .line 523
    const-string v1, "PhotoSyncService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 524
    const-string v1, "PhotoSyncService"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x41

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "----> doHighlightsMetadataDownSync error for account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :cond_3
    invoke-static {v0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Ljava/lang/Exception;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 528
    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_1
.end method

.method private a(Ljava/util/List;Ldvz;Landroid/content/SyncResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ldvz;",
            "Landroid/content/SyncResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 457
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 458
    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 459
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 460
    sget-object v3, Ldxd;->f:Lief;

    invoke-interface {v0, v3, v1}, Lieh;->b(Lief;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 462
    const/4 v3, 0x0

    :try_start_0
    invoke-direct {p0, v1, p2, p3, v3}, Lcuo;->a(ILdvz;Landroid/content/SyncResult;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 463
    :catch_0
    move-exception v3

    .line 464
    const-string v4, "PhotoSyncService"

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x53

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "----> performUnconditionalHighlightsMetadataDownSync error for account: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 467
    invoke-static {v3}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Ljava/lang/Exception;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 468
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_0

    .line 476
    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Ldvz;Landroid/content/SyncResult;Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ldvz;",
            "Landroid/content/SyncResult;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 401
    const-class v0, Lieh;

    invoke-static {v4, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 402
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 403
    iget-object v1, p0, Lcuo;->a:Lhei;

    invoke-interface {v1, v6}, Lhei;->a(I)Lhej;

    .line 405
    sget-object v1, Ldxd;->f:Lief;

    invoke-interface {v0, v1, v6}, Lieh;->b(Lief;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 407
    :try_start_0
    sget-object v1, Lfit;->b:Lfit;

    invoke-static {v4, v6, v1}, Ldhv;->a(Landroid/content/Context;ILfit;)J

    move-result-wide v2

    .line 409
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v2

    .line 412
    if-eqz p4, :cond_3

    .line 413
    sget-object v1, Lfit;->b:Lfit;

    iget-wide v2, v1, Lfit;->f:J

    .line 422
    :goto_1
    cmp-long v1, v8, v2

    if-lez v1, :cond_4

    .line 423
    const-string v1, "PhotoSyncService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 424
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "----> Highlights sync for account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " starting now"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v6, p2, p3, v1}, Lcuo;->a(ILdvz;Landroid/content/SyncResult;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 435
    :catch_0
    move-exception v1

    .line 436
    const-string v2, "PhotoSyncService"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 437
    const-string v2, "PhotoSyncService"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v7, 0x51

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "----> performConditionalHighlightsMetadataDownSync error for account: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 441
    :cond_2
    invoke-static {v1}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Ljava/lang/Exception;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 442
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_0

    .line 416
    :cond_3
    :try_start_1
    invoke-static {v4, v6}, Ldtu;->b(Landroid/content/Context;I)J

    move-result-wide v2

    const-wide/16 v10, 0x3e8

    mul-long/2addr v2, v10

    sget-object v1, Lfit;->b:Lfit;

    iget-wide v10, v1, Lfit;->f:J

    .line 415
    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto :goto_1

    .line 430
    :cond_4
    const-string v1, "PhotoSyncService"

    const/4 v7, 0x4

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 431
    sub-long/2addr v2, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v2, v8

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v7, 0x53

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "----> Highlights sync for account: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " is due in: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " secs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 449
    :cond_5
    return-void
.end method

.method private a(Ljava/util/List;Ldvz;Landroid/content/SyncResult;ZZ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ldvz;",
            "Landroid/content/SyncResult;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 364
    if-eqz p5, :cond_3

    .line 365
    sget-object v0, Lhmv;->es:Lhmv;

    .line 371
    :goto_0
    const-string v1, "Photo down-sync"

    sget-object v2, Lhmw;->Q:Lhmw;

    invoke-virtual {p2, v1, v2, v0}, Ldvz;->a(Ljava/lang/String;Lhmw;Lhmv;)V

    .line 374
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 375
    iget-object v0, p0, Lcuo;->a:Lhei;

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v0, "PhotoSyncService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x42

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "----> Start all photos metadata down sync for account: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz p4, :cond_5

    sget-object v0, Ldts;->f:Ldts;

    move-object v1, v0

    :goto_2
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lfew;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    invoke-virtual {v0, v3, p2, v1}, Lfew;->a(ILdvz;Ldts;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "PhotoSyncService"

    const/4 v4, 0x6

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "PhotoSyncService"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x40

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "----> doAllPhotosMetadataDownSync error for account: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Ljava/lang/Exception;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v0, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 388
    :catchall_0
    move-exception v0

    move-object v1, v0

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 389
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2, v2, v0}, Ldvz;->a(Landroid/content/Context;I)V

    .line 390
    throw v1

    .line 366
    :cond_3
    if-eqz p4, :cond_4

    .line 367
    sget-object v0, Lhmv;->er:Lhmv;

    goto/16 :goto_0

    .line 369
    :cond_4
    sget-object v0, Lhmv;->et:Lhmv;

    goto/16 :goto_0

    .line 375
    :cond_5
    :try_start_3
    sget-object v0, Ldts;->e:Ldts;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v1, v0

    goto :goto_2

    .line 379
    :cond_6
    if-nez p4, :cond_7

    if-nez p5, :cond_7

    .line 380
    :try_start_4
    invoke-direct {p0, p1, p2, p3}, Lcuo;->a(Ljava/util/List;Ldvz;Landroid/content/SyncResult;)V

    .line 386
    :goto_3
    invoke-direct {p0, p1, p2, p3}, Lcuo;->b(Ljava/util/List;Ldvz;Landroid/content/SyncResult;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 388
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 389
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, Ldvz;->a(Landroid/content/Context;I)V

    .line 390
    return-void

    .line 383
    :cond_7
    :try_start_5
    invoke-direct {p0, p1, p2, p3, p4}, Lcuo;->a(Ljava/util/List;Ldvz;Landroid/content/SyncResult;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method

.method private b(Ljava/util/List;Ldvz;Landroid/content/SyncResult;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ldvz;",
            "Landroid/content/SyncResult;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    .line 540
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 541
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 542
    iget-object v0, p0, Lcuo;->a:Lhei;

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v4

    .line 543
    sget-object v0, Lfit;->d:Lfit;

    invoke-static {v1, v3, v0}, Ldhv;->a(Landroid/content/Context;ILfit;)J

    move-result-wide v6

    .line 545
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v6, v8, v6

    .line 546
    sget-object v0, Lfit;->d:Lfit;

    iget-wide v8, v0, Lfit;->f:J

    .line 547
    cmp-long v0, v6, v8

    if-gez v0, :cond_1

    .line 548
    const-string v0, "PhotoSyncService"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 549
    sub-long v4, v8, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v6, 0x4e

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "----> Media sync for account: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " is due in: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " secs"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 555
    :cond_1
    invoke-static {v1}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    invoke-interface {v0}, Ljdw;->a()V

    .line 557
    :try_start_0
    const-string v0, "PhotoSyncService"

    const/4 v5, 0x4

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 558
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v5, 0x34

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "----> Start media down sync for account: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 561
    :cond_2
    invoke-static {v1}, Ldur;->a(Landroid/content/Context;)Ldur;

    move-result-object v0

    .line 562
    invoke-virtual {v0, v3, p2, p3}, Ldur;->a(ILdvz;Landroid/content/SyncResult;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :goto_1
    invoke-static {v1}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    invoke-interface {v0}, Ljdw;->b()V

    .line 571
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljfb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 572
    const-string v0, "PhotoSyncService"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 573
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v5, 0x2a

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "----> Movie maker for account: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 578
    :cond_3
    :try_start_1
    invoke-static {v1}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    const-string v5, "gaia_id"

    .line 579
    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljem;->a(Ljava/lang/String;)V

    .line 580
    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d(Landroid/content/Context;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 587
    :cond_4
    :goto_2
    sget-object v0, Lfit;->d:Lfit;

    .line 588
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 587
    invoke-static {v1, v3, v0, v4, v5}, Ldhv;->a(Landroid/content/Context;ILfit;J)V

    goto/16 :goto_0

    .line 563
    :catch_0
    move-exception v0

    .line 564
    const-string v5, "PhotoSyncService"

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x39

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "----> performMediaDownSync error for account: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 567
    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_1

    .line 581
    :catch_1
    move-exception v0

    .line 582
    const-string v4, "PhotoSyncService"

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x45

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "----> performMediaDownSync movie maker error for account: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 590
    :cond_5
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 272
    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 273
    if-eqz p2, :cond_1

    const-string v0, "initialize"

    .line 274
    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 276
    :goto_0
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v5

    .line 277
    if-eqz v0, :cond_4

    .line 278
    invoke-static {v4}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 280
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 278
    :goto_1
    invoke-static {v0, v5, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 282
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "googleplus_photos"

    const-string v2, "photossync"

    .line 281
    invoke-static {v0, p1, v5, v1, v2}, Lcun;->a(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v0, "PhotoSyncService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    const-string v0, "====> Performing photos downsync initialization request for account: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 353
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v3

    .line 274
    goto :goto_0

    :cond_2
    move v2, v3

    .line 280
    goto :goto_1

    .line 285
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 295
    :cond_4
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 294
    invoke-static {v0, v1}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 296
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 297
    const-string v0, "PhotoSyncService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    const-string v0, "====> Account is not signed in: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2

    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 304
    :cond_6
    invoke-virtual {p0}, Lcuo;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v6, "googleplus_photos"

    const-string v7, "photossync"

    .line 303
    invoke-static {v0, p1, v5, v6, v7}, Lcun;->a(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    if-eqz p2, :cond_7

    const-string v0, "sync_periodic"

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    move v5, v2

    .line 309
    :goto_3
    monitor-enter p0

    .line 310
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 311
    monitor-exit p0

    goto :goto_2

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_7
    move v5, v3

    .line 307
    goto :goto_3

    .line 313
    :cond_8
    :try_start_1
    invoke-static {v4}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Ljava/lang/String;)Ldvz;

    move-result-object v0

    iput-object v0, p0, Lcuo;->b:Ldvz;

    .line 314
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    iget-object v0, p0, Lcuo;->b:Ldvz;

    const-string v2, "Google+ Photos Sync"

    invoke-virtual {v0, v2}, Ldvz;->b(Ljava/lang/String;)V

    .line 318
    if-eqz p2, :cond_e

    :try_start_2
    const-string v0, "feed"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 319
    const-string v0, "feed"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 320
    const-string v2, "PhotoSyncService"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 321
    const-string v2, "====> Sync specific feed: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 324
    :cond_9
    :goto_4
    const-string v2, "googleplus_photos"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 325
    iget-object v2, p0, Lcuo;->b:Ldvz;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, p5

    invoke-direct/range {v0 .. v5}, Lcuo;->a(Ljava/util/List;Ldvz;Landroid/content/SyncResult;ZZ)V
    :try_end_2
    .catch Lhel; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 345
    :cond_a
    :goto_5
    iget-object v0, p0, Lcuo;->b:Ldvz;

    invoke-virtual {v0}, Ldvz;->g()V

    .line 352
    iput-object v8, p0, Lcuo;->b:Ldvz;

    goto/16 :goto_2

    .line 321
    :cond_b
    :try_start_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Lhel; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    .line 349
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcuo;->b:Ldvz;

    invoke-virtual {v0}, Ldvz;->g()V

    .line 352
    iput-object v8, p0, Lcuo;->b:Ldvz;

    goto/16 :goto_2

    .line 328
    :cond_c
    :try_start_4
    const-string v1, "PhotoSyncService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 329
    const-string v1, "PhotoSyncService"

    const-string v2, "Unexpected feed: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Lhel; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 351
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcuo;->b:Ldvz;

    invoke-virtual {v1}, Ldvz;->g()V

    .line 352
    iput-object v8, p0, Lcuo;->b:Ldvz;

    throw v0

    .line 329
    :cond_d
    :try_start_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 333
    :cond_e
    const-string v0, "PhotoSyncService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 334
    if-nez v5, :cond_f

    .line 335
    const-string v0, "====> Manual or requested down sync account="

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 341
    :cond_f
    :goto_7
    iget-object v2, p0, Lcuo;->b:Ldvz;

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, p5

    invoke-direct/range {v0 .. v5}, Lcuo;->a(Ljava/util/List;Ldvz;Landroid/content/SyncResult;ZZ)V

    goto :goto_5

    .line 335
    :cond_10
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Lhel; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_7
.end method

.method public declared-synchronized onSyncCanceled()V
    .locals 1

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    .line 264
    iget-object v0, p0, Lcuo;->b:Ldvz;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcuo;->b:Ldvz;

    invoke-virtual {v0}, Ldvz;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    :cond_0
    monitor-exit p0

    return-void

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
