.class public final Ldkp;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmdi;",
        "Lmdj;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:I

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lnyz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 57
    const-string v3, "getuserhighlights"

    new-instance v4, Lmdi;

    invoke-direct {v4}, Lmdi;-><init>()V

    new-instance v5, Lmdj;

    invoke-direct {v5}, Lmdj;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 60
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 61
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 62
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    if-eqz p3, :cond_0

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    :cond_0
    iput-object v0, p0, Ldkp;->a:Ljava/lang/String;

    .line 66
    const/4 v0, 0x1

    iput v0, p0, Ldkp;->p:I

    .line 72
    :goto_0
    iput-object p4, p0, Ldkp;->b:Ljava/lang/String;

    .line 73
    return-void

    .line 68
    :cond_1
    iput-object p3, p0, Ldkp;->a:Ljava/lang/String;

    .line 69
    const/4 v0, 0x2

    iput v0, p0, Ldkp;->p:I

    goto :goto_0
.end method

.method private a(Lncb;)[Lnzx;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 121
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move v1, v3

    .line 122
    :goto_0
    iget-object v0, p1, Lncb;->b:[Lnzx;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 123
    iget-object v0, p1, Lncb;->b:[Lnzx;

    aget-object v0, v0, v1

    sget-object v2, Lnzr;->a:Loxr;

    .line 124
    invoke-virtual {v0, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 125
    iget-object v0, v0, Lnzr;->b:Lnxr;

    .line 126
    iget-object v0, v0, Lnxr;->b:Ljava/lang/String;

    iget-object v2, p1, Lncb;->b:[Lnzx;

    aget-object v2, v2, v1

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 130
    :cond_0
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    move v2, v3

    .line 132
    :goto_1
    iget-object v0, p1, Lncb;->c:[Lnzx;

    array-length v0, v0

    if-ge v2, v0, :cond_4

    .line 133
    iget-object v0, p1, Lncb;->c:[Lnzx;

    aget-object v0, v0, v2

    sget-object v1, Lnzu;->a:Loxr;

    .line 134
    invoke-virtual {v0, v1}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 135
    iget-object v7, v0, Lnzu;->b:Lnym;

    .line 136
    iget-object v1, v7, Lnym;->z:[Lnxt;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 137
    iget-object v1, v7, Lnym;->z:[Lnxt;

    aget-object v1, v1, v3

    iget-object v8, v1, Lnxt;->b:Ljava/lang/String;

    .line 139
    invoke-virtual {v6, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 140
    if-nez v1, :cond_1

    .line 141
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 142
    invoke-virtual {v6, v8, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    :cond_1
    iget-object v8, p1, Lncb;->c:[Lnzx;

    aget-object v8, v8, v2

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_2
    iget-object v8, p0, Ldkp;->q:Ljava/util/HashMap;

    iget-object v1, v7, Lnym;->i:Lnzm;

    if-eqz v1, :cond_3

    iget-object v1, v7, Lnym;->i:Lnzm;

    iget-object v1, v1, Lnzm;->b:Ljava/lang/String;

    :goto_2
    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnyz;

    iput-object v1, v7, Lnym;->h:Lnyz;

    .line 149
    iget-object v1, p1, Lncb;->c:[Lnzx;

    aget-object v1, v1, v2

    sget-object v7, Lnzu;->a:Loxr;

    invoke-virtual {v1, v7, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 132
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v1, v4

    .line 148
    goto :goto_2

    .line 154
    :cond_4
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 155
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 156
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzx;

    .line 157
    if-nez v1, :cond_6

    .line 158
    const-string v0, "HttpOperation"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_3

    .line 164
    :cond_6
    invoke-virtual {v6, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 165
    iget-object v2, v1, Lnzx;->f:Lnyl;

    if-nez v2, :cond_7

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 166
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzx;

    iget-object v2, v2, Lnzx;->f:Lnyl;

    iput-object v2, v1, Lnzx;->f:Lnyl;

    .line 168
    :cond_7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lnzx;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnzx;

    iput-object v0, v1, Lnzx;->j:[Lnzx;

    .line 171
    sget-object v0, Lnzr;->a:Loxr;

    .line 172
    invoke-virtual {v1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 173
    iget-object v2, p0, Ldkp;->q:Ljava/util/HashMap;

    iget-object v9, v0, Lnzr;->b:Lnxr;

    iget-object v9, v9, Lnxr;->g:Lnzm;

    iget-object v9, v9, Lnzm;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnyz;

    .line 174
    iget-object v9, v0, Lnzr;->b:Lnxr;

    if-nez v2, :cond_8

    move-object v2, v4

    :goto_4
    iput-object v2, v9, Lnxr;->f:Ljava/lang/String;

    .line 176
    sget-object v2, Lnzr;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 178
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 174
    :cond_8
    iget-object v2, v2, Lnyz;->c:Ljava/lang/String;

    goto :goto_4

    .line 180
    :cond_9
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lnzx;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnzx;

    return-object v0
.end method


# virtual methods
.method protected a(Lmdi;)V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Lnca;

    invoke-direct {v0}, Lnca;-><init>()V

    iput-object v0, p1, Lmdi;->a:Lnca;

    .line 78
    iget-object v0, p1, Lmdi;->a:Lnca;

    .line 79
    iget-object v1, p0, Ldkp;->b:Ljava/lang/String;

    iput-object v1, v0, Lnca;->b:Ljava/lang/String;

    .line 82
    new-instance v1, Lnby;

    invoke-direct {v1}, Lnby;-><init>()V

    iput-object v1, v0, Lnca;->a:Lnby;

    .line 83
    iget v1, p0, Ldkp;->p:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ldkp;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 84
    iget-object v1, v0, Lnca;->a:Lnby;

    new-instance v2, Lota;

    invoke-direct {v2}, Lota;-><init>()V

    iput-object v2, v1, Lnby;->a:Lota;

    .line 85
    iget-object v1, v0, Lnca;->a:Lnby;

    iget-object v1, v1, Lnby;->a:Lota;

    iget-object v2, p0, Ldkp;->a:Ljava/lang/String;

    iput-object v2, v1, Lota;->a:Ljava/lang/String;

    .line 87
    :cond_0
    iget-object v0, v0, Lnca;->a:Lnby;

    iget v1, p0, Ldkp;->p:I

    iput v1, v0, Lnby;->b:I

    .line 88
    return-void
.end method

.method protected a(Lmdj;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 92
    iget-object v1, p1, Lmdj;->a:Lncb;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldkp;->q:Ljava/util/HashMap;

    move v0, v5

    :goto_0
    iget-object v2, v1, Lncb;->d:[Lnyz;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Ldkp;->q:Ljava/util/HashMap;

    iget-object v3, v1, Lncb;->d:[Lnyz;

    aget-object v3, v3, v0

    iget-object v3, v3, Lnyz;->b:Ljava/lang/String;

    iget-object v4, v1, Lncb;->d:[Lnyz;

    aget-object v4, v4, v0

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    iget-object v0, p1, Lmdj;->a:Lncb;

    invoke-direct {p0, v0}, Ldkp;->a(Lncb;)[Lnzx;

    move-result-object v3

    .line 95
    new-array v0, v7, [Ljava/lang/String;

    iget-object v1, p0, Ldkp;->a:Ljava/lang/String;

    aput-object v1, v0, v5

    invoke-static {v5, v0}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    iget-object v1, p0, Ldkp;->f:Landroid/content/Context;

    iget v4, p0, Ldkp;->c:I

    iget-object v0, p1, Lmdj;->a:Lncb;

    iget-object v6, v0, Lncb;->a:Ljava/lang/String;

    iget-object v0, p0, Ldkp;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v7

    :goto_1
    invoke-static {v1, v4, v2, v6, v0}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 98
    iget-object v0, p0, Ldkp;->f:Landroid/content/Context;

    iget v1, p0, Ldkp;->c:I

    iget-object v4, p0, Ldkp;->b:Ljava/lang/String;

    if-nez v4, :cond_2

    move v4, v7

    :goto_2
    const/4 v6, 0x0

    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 101
    return-void

    :cond_1
    move v0, v5

    .line 96
    goto :goto_1

    :cond_2
    move v4, v5

    .line 98
    goto :goto_2
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lmdi;

    invoke-virtual {p0, p1}, Ldkp;->a(Lmdi;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lmdj;

    invoke-virtual {p0, p1}, Ldkp;->a(Lmdj;)V

    return-void
.end method
