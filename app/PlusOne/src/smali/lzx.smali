.class public final Llzx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvx;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7450
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7453
    iput-object v0, p0, Llzx;->apiHeader:Llyr;

    .line 7456
    iput-object v0, p0, Llzx;->a:Lnvx;

    .line 7450
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7473
    const/4 v0, 0x0

    .line 7474
    iget-object v1, p0, Llzx;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 7475
    const/4 v0, 0x1

    iget-object v1, p0, Llzx;->apiHeader:Llyr;

    .line 7476
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7478
    :cond_0
    iget-object v1, p0, Llzx;->a:Lnvx;

    if-eqz v1, :cond_1

    .line 7479
    const/4 v1, 0x2

    iget-object v2, p0, Llzx;->a:Lnvx;

    .line 7480
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7482
    :cond_1
    iget-object v1, p0, Llzx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7483
    iput v0, p0, Llzx;->ai:I

    .line 7484
    return v0
.end method

.method public a(Loxn;)Llzx;
    .locals 2

    .prologue
    .line 7492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7493
    sparse-switch v0, :sswitch_data_0

    .line 7497
    iget-object v1, p0, Llzx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7498
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzx;->ah:Ljava/util/List;

    .line 7501
    :cond_1
    iget-object v1, p0, Llzx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7503
    :sswitch_0
    return-object p0

    .line 7508
    :sswitch_1
    iget-object v0, p0, Llzx;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 7509
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzx;->apiHeader:Llyr;

    .line 7511
    :cond_2
    iget-object v0, p0, Llzx;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7515
    :sswitch_2
    iget-object v0, p0, Llzx;->a:Lnvx;

    if-nez v0, :cond_3

    .line 7516
    new-instance v0, Lnvx;

    invoke-direct {v0}, Lnvx;-><init>()V

    iput-object v0, p0, Llzx;->a:Lnvx;

    .line 7518
    :cond_3
    iget-object v0, p0, Llzx;->a:Lnvx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7493
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7461
    iget-object v0, p0, Llzx;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 7462
    const/4 v0, 0x1

    iget-object v1, p0, Llzx;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7464
    :cond_0
    iget-object v0, p0, Llzx;->a:Lnvx;

    if-eqz v0, :cond_1

    .line 7465
    const/4 v0, 0x2

    iget-object v1, p0, Llzx;->a:Lnvx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7467
    :cond_1
    iget-object v0, p0, Llzx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7469
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7446
    invoke-virtual {p0, p1}, Llzx;->a(Loxn;)Llzx;

    move-result-object v0

    return-object v0
.end method
