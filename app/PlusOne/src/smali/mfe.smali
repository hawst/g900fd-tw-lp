.class public final Lmfe;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojq;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11905
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11908
    iput-object v0, p0, Lmfe;->apiHeader:Llyq;

    .line 11911
    iput-object v0, p0, Lmfe;->a:Lojq;

    .line 11905
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11928
    const/4 v0, 0x0

    .line 11929
    iget-object v1, p0, Lmfe;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 11930
    const/4 v0, 0x1

    iget-object v1, p0, Lmfe;->apiHeader:Llyq;

    .line 11931
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11933
    :cond_0
    iget-object v1, p0, Lmfe;->a:Lojq;

    if-eqz v1, :cond_1

    .line 11934
    const/4 v1, 0x2

    iget-object v2, p0, Lmfe;->a:Lojq;

    .line 11935
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11937
    :cond_1
    iget-object v1, p0, Lmfe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11938
    iput v0, p0, Lmfe;->ai:I

    .line 11939
    return v0
.end method

.method public a(Loxn;)Lmfe;
    .locals 2

    .prologue
    .line 11947
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11948
    sparse-switch v0, :sswitch_data_0

    .line 11952
    iget-object v1, p0, Lmfe;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11953
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfe;->ah:Ljava/util/List;

    .line 11956
    :cond_1
    iget-object v1, p0, Lmfe;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11958
    :sswitch_0
    return-object p0

    .line 11963
    :sswitch_1
    iget-object v0, p0, Lmfe;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 11964
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfe;->apiHeader:Llyq;

    .line 11966
    :cond_2
    iget-object v0, p0, Lmfe;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11970
    :sswitch_2
    iget-object v0, p0, Lmfe;->a:Lojq;

    if-nez v0, :cond_3

    .line 11971
    new-instance v0, Lojq;

    invoke-direct {v0}, Lojq;-><init>()V

    iput-object v0, p0, Lmfe;->a:Lojq;

    .line 11973
    :cond_3
    iget-object v0, p0, Lmfe;->a:Lojq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11948
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11916
    iget-object v0, p0, Lmfe;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 11917
    const/4 v0, 0x1

    iget-object v1, p0, Lmfe;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11919
    :cond_0
    iget-object v0, p0, Lmfe;->a:Lojq;

    if-eqz v0, :cond_1

    .line 11920
    const/4 v0, 0x2

    iget-object v1, p0, Lmfe;->a:Lojq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11922
    :cond_1
    iget-object v0, p0, Lmfe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11924
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11901
    invoke-virtual {p0, p1}, Lmfe;->a(Loxn;)Lmfe;

    move-result-object v0

    return-object v0
.end method
