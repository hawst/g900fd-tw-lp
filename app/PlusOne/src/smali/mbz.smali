.class public final Lmbz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmxo;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30130
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30133
    iput-object v0, p0, Lmbz;->apiHeader:Llyr;

    .line 30136
    iput-object v0, p0, Lmbz;->a:Lmxo;

    .line 30130
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 30153
    const/4 v0, 0x0

    .line 30154
    iget-object v1, p0, Lmbz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 30155
    const/4 v0, 0x1

    iget-object v1, p0, Lmbz;->apiHeader:Llyr;

    .line 30156
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 30158
    :cond_0
    iget-object v1, p0, Lmbz;->a:Lmxo;

    if-eqz v1, :cond_1

    .line 30159
    const/4 v1, 0x2

    iget-object v2, p0, Lmbz;->a:Lmxo;

    .line 30160
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30162
    :cond_1
    iget-object v1, p0, Lmbz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30163
    iput v0, p0, Lmbz;->ai:I

    .line 30164
    return v0
.end method

.method public a(Loxn;)Lmbz;
    .locals 2

    .prologue
    .line 30172
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30173
    sparse-switch v0, :sswitch_data_0

    .line 30177
    iget-object v1, p0, Lmbz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30178
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbz;->ah:Ljava/util/List;

    .line 30181
    :cond_1
    iget-object v1, p0, Lmbz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30183
    :sswitch_0
    return-object p0

    .line 30188
    :sswitch_1
    iget-object v0, p0, Lmbz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 30189
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbz;->apiHeader:Llyr;

    .line 30191
    :cond_2
    iget-object v0, p0, Lmbz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30195
    :sswitch_2
    iget-object v0, p0, Lmbz;->a:Lmxo;

    if-nez v0, :cond_3

    .line 30196
    new-instance v0, Lmxo;

    invoke-direct {v0}, Lmxo;-><init>()V

    iput-object v0, p0, Lmbz;->a:Lmxo;

    .line 30198
    :cond_3
    iget-object v0, p0, Lmbz;->a:Lmxo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30173
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30141
    iget-object v0, p0, Lmbz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 30142
    const/4 v0, 0x1

    iget-object v1, p0, Lmbz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30144
    :cond_0
    iget-object v0, p0, Lmbz;->a:Lmxo;

    if-eqz v0, :cond_1

    .line 30145
    const/4 v0, 0x2

    iget-object v1, p0, Lmbz;->a:Lmxo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30147
    :cond_1
    iget-object v0, p0, Lmbz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30149
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 30126
    invoke-virtual {p0, p1}, Lmbz;->a(Loxn;)Lmbz;

    move-result-object v0

    return-object v0
.end method
