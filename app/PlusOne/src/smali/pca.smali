.class public final Lpca;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpca;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;

.field public e:Loya;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lpdi;

.field private k:Ljava/lang/String;

.field private l:[Loya;

.field private m:Loya;

.field private n:[Loya;

.field private o:Ljava/lang/String;

.field private p:[Loya;

.field private q:Ljava/lang/String;

.field private r:Loya;

.field private s:[Loya;

.field private t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2d18399

    new-instance v1, Lpcb;

    invoke-direct {v1}, Lpcb;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpca;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lpca;->j:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpca;->l:[Loya;

    .line 35
    iput-object v1, p0, Lpca;->m:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpca;->n:[Loya;

    .line 43
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpca;->p:[Loya;

    .line 50
    iput-object v1, p0, Lpca;->r:Loya;

    .line 53
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpca;->s:[Loya;

    .line 60
    iput-object v1, p0, Lpca;->e:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 144
    .line 145
    iget-object v0, p0, Lpca;->f:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 146
    const/4 v0, 0x1

    iget-object v2, p0, Lpca;->f:Ljava/lang/String;

    .line 147
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 149
    :goto_0
    iget-object v2, p0, Lpca;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 150
    const/4 v2, 0x2

    iget-object v3, p0, Lpca;->g:Ljava/lang/String;

    .line 151
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 153
    :cond_0
    iget-object v2, p0, Lpca;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 154
    const/4 v2, 0x3

    iget-object v3, p0, Lpca;->b:Ljava/lang/String;

    .line 155
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 157
    :cond_1
    iget-object v2, p0, Lpca;->h:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 158
    const/4 v2, 0x4

    iget-object v3, p0, Lpca;->h:Ljava/lang/String;

    .line 159
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 161
    :cond_2
    iget-object v2, p0, Lpca;->i:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 162
    const/4 v2, 0x5

    iget-object v3, p0, Lpca;->i:Ljava/lang/String;

    .line 163
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 165
    :cond_3
    iget-object v2, p0, Lpca;->j:Lpdi;

    if-eqz v2, :cond_4

    .line 166
    const/4 v2, 0x6

    iget-object v3, p0, Lpca;->j:Lpdi;

    .line 167
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 169
    :cond_4
    iget-object v2, p0, Lpca;->k:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 170
    const/4 v2, 0x7

    iget-object v3, p0, Lpca;->k:Ljava/lang/String;

    .line 171
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 173
    :cond_5
    iget-object v2, p0, Lpca;->l:[Loya;

    if-eqz v2, :cond_7

    .line 174
    iget-object v3, p0, Lpca;->l:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 175
    if-eqz v5, :cond_6

    .line 176
    const/16 v6, 0x8

    .line 177
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 174
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 181
    :cond_7
    iget-object v2, p0, Lpca;->m:Loya;

    if-eqz v2, :cond_8

    .line 182
    const/16 v2, 0x9

    iget-object v3, p0, Lpca;->m:Loya;

    .line 183
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 185
    :cond_8
    iget-object v2, p0, Lpca;->n:[Loya;

    if-eqz v2, :cond_a

    .line 186
    iget-object v3, p0, Lpca;->n:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    .line 187
    if-eqz v5, :cond_9

    .line 188
    const/16 v6, 0xb

    .line 189
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 186
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 193
    :cond_a
    iget-object v2, p0, Lpca;->o:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 194
    const/16 v2, 0xc

    iget-object v3, p0, Lpca;->o:Ljava/lang/String;

    .line 195
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 197
    :cond_b
    iget-object v2, p0, Lpca;->p:[Loya;

    if-eqz v2, :cond_d

    .line 198
    iget-object v3, p0, Lpca;->p:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_d

    aget-object v5, v3, v2

    .line 199
    if-eqz v5, :cond_c

    .line 200
    const/16 v6, 0x2a

    .line 201
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 198
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 205
    :cond_d
    iget-object v2, p0, Lpca;->q:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 206
    const/16 v2, 0x4b

    iget-object v3, p0, Lpca;->q:Ljava/lang/String;

    .line 207
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 209
    :cond_e
    iget-object v2, p0, Lpca;->c:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 210
    const/16 v2, 0x6a

    iget-object v3, p0, Lpca;->c:Ljava/lang/String;

    .line 211
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 213
    :cond_f
    iget-object v2, p0, Lpca;->r:Loya;

    if-eqz v2, :cond_10

    .line 214
    const/16 v2, 0xb9

    iget-object v3, p0, Lpca;->r:Loya;

    .line 215
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 217
    :cond_10
    iget-object v2, p0, Lpca;->s:[Loya;

    if-eqz v2, :cond_12

    .line 218
    iget-object v2, p0, Lpca;->s:[Loya;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_12

    aget-object v4, v2, v1

    .line 219
    if-eqz v4, :cond_11

    .line 220
    const/16 v5, 0xca

    .line 221
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 218
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 225
    :cond_12
    iget-object v1, p0, Lpca;->d:Ljava/lang/Long;

    if-eqz v1, :cond_13

    .line 226
    const/16 v1, 0xcb

    iget-object v2, p0, Lpca;->d:Ljava/lang/Long;

    .line 227
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_13
    iget-object v1, p0, Lpca;->t:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 230
    const/16 v1, 0xfe

    iget-object v2, p0, Lpca;->t:Ljava/lang/String;

    .line 231
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_14
    iget-object v1, p0, Lpca;->e:Loya;

    if-eqz v1, :cond_15

    .line 234
    const/16 v1, 0x10c

    iget-object v2, p0, Lpca;->e:Loya;

    .line 235
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_15
    iget-object v1, p0, Lpca;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    iput v0, p0, Lpca;->ai:I

    .line 239
    return v0

    :cond_16
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpca;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 248
    sparse-switch v0, :sswitch_data_0

    .line 252
    iget-object v2, p0, Lpca;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 253
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpca;->ah:Ljava/util/List;

    .line 256
    :cond_1
    iget-object v2, p0, Lpca;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    :sswitch_0
    return-object p0

    .line 263
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->f:Ljava/lang/String;

    goto :goto_0

    .line 267
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->g:Ljava/lang/String;

    goto :goto_0

    .line 271
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->b:Ljava/lang/String;

    goto :goto_0

    .line 275
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->h:Ljava/lang/String;

    goto :goto_0

    .line 279
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->i:Ljava/lang/String;

    goto :goto_0

    .line 283
    :sswitch_6
    iget-object v0, p0, Lpca;->j:Lpdi;

    if-nez v0, :cond_2

    .line 284
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lpca;->j:Lpdi;

    .line 286
    :cond_2
    iget-object v0, p0, Lpca;->j:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 290
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->k:Ljava/lang/String;

    goto :goto_0

    .line 294
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 295
    iget-object v0, p0, Lpca;->l:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 296
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 297
    iget-object v3, p0, Lpca;->l:[Loya;

    if-eqz v3, :cond_3

    .line 298
    iget-object v3, p0, Lpca;->l:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 300
    :cond_3
    iput-object v2, p0, Lpca;->l:[Loya;

    .line 301
    :goto_2
    iget-object v2, p0, Lpca;->l:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 302
    iget-object v2, p0, Lpca;->l:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 303
    iget-object v2, p0, Lpca;->l:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 304
    invoke-virtual {p1}, Loxn;->a()I

    .line 301
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 295
    :cond_4
    iget-object v0, p0, Lpca;->l:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 307
    :cond_5
    iget-object v2, p0, Lpca;->l:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 308
    iget-object v2, p0, Lpca;->l:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 312
    :sswitch_9
    iget-object v0, p0, Lpca;->m:Loya;

    if-nez v0, :cond_6

    .line 313
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpca;->m:Loya;

    .line 315
    :cond_6
    iget-object v0, p0, Lpca;->m:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 319
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 320
    iget-object v0, p0, Lpca;->n:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 321
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 322
    iget-object v3, p0, Lpca;->n:[Loya;

    if-eqz v3, :cond_7

    .line 323
    iget-object v3, p0, Lpca;->n:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 325
    :cond_7
    iput-object v2, p0, Lpca;->n:[Loya;

    .line 326
    :goto_4
    iget-object v2, p0, Lpca;->n:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 327
    iget-object v2, p0, Lpca;->n:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 328
    iget-object v2, p0, Lpca;->n:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 329
    invoke-virtual {p1}, Loxn;->a()I

    .line 326
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 320
    :cond_8
    iget-object v0, p0, Lpca;->n:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 332
    :cond_9
    iget-object v2, p0, Lpca;->n:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 333
    iget-object v2, p0, Lpca;->n:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 337
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 341
    :sswitch_c
    const/16 v0, 0x152

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 342
    iget-object v0, p0, Lpca;->p:[Loya;

    if-nez v0, :cond_b

    move v0, v1

    .line 343
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 344
    iget-object v3, p0, Lpca;->p:[Loya;

    if-eqz v3, :cond_a

    .line 345
    iget-object v3, p0, Lpca;->p:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 347
    :cond_a
    iput-object v2, p0, Lpca;->p:[Loya;

    .line 348
    :goto_6
    iget-object v2, p0, Lpca;->p:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 349
    iget-object v2, p0, Lpca;->p:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 350
    iget-object v2, p0, Lpca;->p:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 351
    invoke-virtual {p1}, Loxn;->a()I

    .line 348
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 342
    :cond_b
    iget-object v0, p0, Lpca;->p:[Loya;

    array-length v0, v0

    goto :goto_5

    .line 354
    :cond_c
    iget-object v2, p0, Lpca;->p:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 355
    iget-object v2, p0, Lpca;->p:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 359
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 363
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 367
    :sswitch_f
    iget-object v0, p0, Lpca;->r:Loya;

    if-nez v0, :cond_d

    .line 368
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpca;->r:Loya;

    .line 370
    :cond_d
    iget-object v0, p0, Lpca;->r:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 374
    :sswitch_10
    const/16 v0, 0x652

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 375
    iget-object v0, p0, Lpca;->s:[Loya;

    if-nez v0, :cond_f

    move v0, v1

    .line 376
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 377
    iget-object v3, p0, Lpca;->s:[Loya;

    if-eqz v3, :cond_e

    .line 378
    iget-object v3, p0, Lpca;->s:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 380
    :cond_e
    iput-object v2, p0, Lpca;->s:[Loya;

    .line 381
    :goto_8
    iget-object v2, p0, Lpca;->s:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    .line 382
    iget-object v2, p0, Lpca;->s:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 383
    iget-object v2, p0, Lpca;->s:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 384
    invoke-virtual {p1}, Loxn;->a()I

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 375
    :cond_f
    iget-object v0, p0, Lpca;->s:[Loya;

    array-length v0, v0

    goto :goto_7

    .line 387
    :cond_10
    iget-object v2, p0, Lpca;->s:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 388
    iget-object v2, p0, Lpca;->s:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 392
    :sswitch_11
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpca;->d:Ljava/lang/Long;

    goto/16 :goto_0

    .line 396
    :sswitch_12
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpca;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 400
    :sswitch_13
    iget-object v0, p0, Lpca;->e:Loya;

    if-nez v0, :cond_11

    .line 401
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpca;->e:Loya;

    .line 403
    :cond_11
    iget-object v0, p0, Lpca;->e:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 248
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x152 -> :sswitch_c
        0x25a -> :sswitch_d
        0x352 -> :sswitch_e
        0x5ca -> :sswitch_f
        0x652 -> :sswitch_10
        0x658 -> :sswitch_11
        0x7f2 -> :sswitch_12
        0x862 -> :sswitch_13
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Lpca;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 66
    const/4 v1, 0x1

    iget-object v2, p0, Lpca;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 68
    :cond_0
    iget-object v1, p0, Lpca;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 69
    const/4 v1, 0x2

    iget-object v2, p0, Lpca;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 71
    :cond_1
    iget-object v1, p0, Lpca;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 72
    const/4 v1, 0x3

    iget-object v2, p0, Lpca;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 74
    :cond_2
    iget-object v1, p0, Lpca;->h:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 75
    const/4 v1, 0x4

    iget-object v2, p0, Lpca;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 77
    :cond_3
    iget-object v1, p0, Lpca;->i:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 78
    const/4 v1, 0x5

    iget-object v2, p0, Lpca;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 80
    :cond_4
    iget-object v1, p0, Lpca;->j:Lpdi;

    if-eqz v1, :cond_5

    .line 81
    const/4 v1, 0x6

    iget-object v2, p0, Lpca;->j:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 83
    :cond_5
    iget-object v1, p0, Lpca;->k:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 84
    const/4 v1, 0x7

    iget-object v2, p0, Lpca;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 86
    :cond_6
    iget-object v1, p0, Lpca;->l:[Loya;

    if-eqz v1, :cond_8

    .line 87
    iget-object v2, p0, Lpca;->l:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 88
    if-eqz v4, :cond_7

    .line 89
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 87
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    :cond_8
    iget-object v1, p0, Lpca;->m:Loya;

    if-eqz v1, :cond_9

    .line 94
    const/16 v1, 0x9

    iget-object v2, p0, Lpca;->m:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 96
    :cond_9
    iget-object v1, p0, Lpca;->n:[Loya;

    if-eqz v1, :cond_b

    .line 97
    iget-object v2, p0, Lpca;->n:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 98
    if-eqz v4, :cond_a

    .line 99
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 97
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 103
    :cond_b
    iget-object v1, p0, Lpca;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 104
    const/16 v1, 0xc

    iget-object v2, p0, Lpca;->o:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 106
    :cond_c
    iget-object v1, p0, Lpca;->p:[Loya;

    if-eqz v1, :cond_e

    .line 107
    iget-object v2, p0, Lpca;->p:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_e

    aget-object v4, v2, v1

    .line 108
    if-eqz v4, :cond_d

    .line 109
    const/16 v5, 0x2a

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 107
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 113
    :cond_e
    iget-object v1, p0, Lpca;->q:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 114
    const/16 v1, 0x4b

    iget-object v2, p0, Lpca;->q:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 116
    :cond_f
    iget-object v1, p0, Lpca;->c:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 117
    const/16 v1, 0x6a

    iget-object v2, p0, Lpca;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 119
    :cond_10
    iget-object v1, p0, Lpca;->r:Loya;

    if-eqz v1, :cond_11

    .line 120
    const/16 v1, 0xb9

    iget-object v2, p0, Lpca;->r:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 122
    :cond_11
    iget-object v1, p0, Lpca;->s:[Loya;

    if-eqz v1, :cond_13

    .line 123
    iget-object v1, p0, Lpca;->s:[Loya;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_13

    aget-object v3, v1, v0

    .line 124
    if-eqz v3, :cond_12

    .line 125
    const/16 v4, 0xca

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 123
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 129
    :cond_13
    iget-object v0, p0, Lpca;->d:Ljava/lang/Long;

    if-eqz v0, :cond_14

    .line 130
    const/16 v0, 0xcb

    iget-object v1, p0, Lpca;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 132
    :cond_14
    iget-object v0, p0, Lpca;->t:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 133
    const/16 v0, 0xfe

    iget-object v1, p0, Lpca;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 135
    :cond_15
    iget-object v0, p0, Lpca;->e:Loya;

    if-eqz v0, :cond_16

    .line 136
    const/16 v0, 0x10c

    iget-object v1, p0, Lpca;->e:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 138
    :cond_16
    iget-object v0, p0, Lpca;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 140
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpca;->a(Loxn;)Lpca;

    move-result-object v0

    return-object v0
.end method
