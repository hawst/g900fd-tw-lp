.class public final Lnax;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Long;

.field public c:Lnyp;

.field public d:Ljava/lang/Long;

.field public e:Lnay;

.field private f:Lnyo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 103
    iput-object v0, p0, Lnax;->f:Lnyo;

    .line 106
    iput-object v0, p0, Lnax;->c:Lnyp;

    .line 111
    iput-object v0, p0, Lnax;->e:Lnay;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    iget-object v1, p0, Lnax;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 142
    const/4 v0, 0x1

    iget-object v1, p0, Lnax;->a:Ljava/lang/String;

    .line 143
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 145
    :cond_0
    iget-object v1, p0, Lnax;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 146
    const/4 v1, 0x2

    iget-object v2, p0, Lnax;->b:Ljava/lang/Long;

    .line 147
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_1
    iget-object v1, p0, Lnax;->f:Lnyo;

    if-eqz v1, :cond_2

    .line 150
    const/4 v1, 0x3

    iget-object v2, p0, Lnax;->f:Lnyo;

    .line 151
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_2
    iget-object v1, p0, Lnax;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 154
    const/4 v1, 0x4

    iget-object v2, p0, Lnax;->d:Ljava/lang/Long;

    .line 155
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_3
    iget-object v1, p0, Lnax;->e:Lnay;

    if-eqz v1, :cond_4

    .line 158
    const/4 v1, 0x5

    iget-object v2, p0, Lnax;->e:Lnay;

    .line 159
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_4
    iget-object v1, p0, Lnax;->c:Lnyp;

    if-eqz v1, :cond_5

    .line 162
    const/4 v1, 0x6

    iget-object v2, p0, Lnax;->c:Lnyp;

    .line 163
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_5
    iget-object v1, p0, Lnax;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    iput v0, p0, Lnax;->ai:I

    .line 167
    return v0
.end method

.method public a(Loxn;)Lnax;
    .locals 2

    .prologue
    .line 175
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 176
    sparse-switch v0, :sswitch_data_0

    .line 180
    iget-object v1, p0, Lnax;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 181
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnax;->ah:Ljava/util/List;

    .line 184
    :cond_1
    iget-object v1, p0, Lnax;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    :sswitch_0
    return-object p0

    .line 191
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnax;->a:Ljava/lang/String;

    goto :goto_0

    .line 195
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnax;->b:Ljava/lang/Long;

    goto :goto_0

    .line 199
    :sswitch_3
    iget-object v0, p0, Lnax;->f:Lnyo;

    if-nez v0, :cond_2

    .line 200
    new-instance v0, Lnyo;

    invoke-direct {v0}, Lnyo;-><init>()V

    iput-object v0, p0, Lnax;->f:Lnyo;

    .line 202
    :cond_2
    iget-object v0, p0, Lnax;->f:Lnyo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 206
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnax;->d:Ljava/lang/Long;

    goto :goto_0

    .line 210
    :sswitch_5
    iget-object v0, p0, Lnax;->e:Lnay;

    if-nez v0, :cond_3

    .line 211
    new-instance v0, Lnay;

    invoke-direct {v0}, Lnay;-><init>()V

    iput-object v0, p0, Lnax;->e:Lnay;

    .line 213
    :cond_3
    iget-object v0, p0, Lnax;->e:Lnay;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 217
    :sswitch_6
    iget-object v0, p0, Lnax;->c:Lnyp;

    if-nez v0, :cond_4

    .line 218
    new-instance v0, Lnyp;

    invoke-direct {v0}, Lnyp;-><init>()V

    iput-object v0, p0, Lnax;->c:Lnyp;

    .line 220
    :cond_4
    iget-object v0, p0, Lnax;->c:Lnyp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 176
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lnax;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 117
    const/4 v0, 0x1

    iget-object v1, p0, Lnax;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 119
    :cond_0
    iget-object v0, p0, Lnax;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 120
    const/4 v0, 0x2

    iget-object v1, p0, Lnax;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 122
    :cond_1
    iget-object v0, p0, Lnax;->f:Lnyo;

    if-eqz v0, :cond_2

    .line 123
    const/4 v0, 0x3

    iget-object v1, p0, Lnax;->f:Lnyo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 125
    :cond_2
    iget-object v0, p0, Lnax;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 126
    const/4 v0, 0x4

    iget-object v1, p0, Lnax;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 128
    :cond_3
    iget-object v0, p0, Lnax;->e:Lnay;

    if-eqz v0, :cond_4

    .line 129
    const/4 v0, 0x5

    iget-object v1, p0, Lnax;->e:Lnay;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 131
    :cond_4
    iget-object v0, p0, Lnax;->c:Lnyp;

    if-eqz v0, :cond_5

    .line 132
    const/4 v0, 0x6

    iget-object v1, p0, Lnax;->c:Lnyp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 134
    :cond_5
    iget-object v0, p0, Lnax;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 136
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnax;->a(Loxn;)Lnax;

    move-result-object v0

    return-object v0
.end method
