.class public final Lmem;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11257
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11260
    iput-object v0, p0, Lmem;->apiHeader:Llyq;

    .line 11263
    iput-object v0, p0, Lmem;->a:Lojj;

    .line 11257
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11280
    const/4 v0, 0x0

    .line 11281
    iget-object v1, p0, Lmem;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 11282
    const/4 v0, 0x1

    iget-object v1, p0, Lmem;->apiHeader:Llyq;

    .line 11283
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11285
    :cond_0
    iget-object v1, p0, Lmem;->a:Lojj;

    if-eqz v1, :cond_1

    .line 11286
    const/4 v1, 0x2

    iget-object v2, p0, Lmem;->a:Lojj;

    .line 11287
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11289
    :cond_1
    iget-object v1, p0, Lmem;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11290
    iput v0, p0, Lmem;->ai:I

    .line 11291
    return v0
.end method

.method public a(Loxn;)Lmem;
    .locals 2

    .prologue
    .line 11299
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11300
    sparse-switch v0, :sswitch_data_0

    .line 11304
    iget-object v1, p0, Lmem;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11305
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmem;->ah:Ljava/util/List;

    .line 11308
    :cond_1
    iget-object v1, p0, Lmem;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11310
    :sswitch_0
    return-object p0

    .line 11315
    :sswitch_1
    iget-object v0, p0, Lmem;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 11316
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmem;->apiHeader:Llyq;

    .line 11318
    :cond_2
    iget-object v0, p0, Lmem;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11322
    :sswitch_2
    iget-object v0, p0, Lmem;->a:Lojj;

    if-nez v0, :cond_3

    .line 11323
    new-instance v0, Lojj;

    invoke-direct {v0}, Lojj;-><init>()V

    iput-object v0, p0, Lmem;->a:Lojj;

    .line 11325
    :cond_3
    iget-object v0, p0, Lmem;->a:Lojj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11300
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11268
    iget-object v0, p0, Lmem;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 11269
    const/4 v0, 0x1

    iget-object v1, p0, Lmem;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11271
    :cond_0
    iget-object v0, p0, Lmem;->a:Lojj;

    if-eqz v0, :cond_1

    .line 11272
    const/4 v0, 0x2

    iget-object v1, p0, Lmem;->a:Lojj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11274
    :cond_1
    iget-object v0, p0, Lmem;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11276
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11253
    invoke-virtual {p0, p1}, Lmem;->a(Loxn;)Lmem;

    move-result-object v0

    return-object v0
.end method
