.class public final enum Lcph;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcph;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcph;

.field public static final enum b:Lcph;

.field private static final synthetic c:[Lcph;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcph;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcph;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcph;->a:Lcph;

    new-instance v0, Lcph;

    const-string v1, "WITH_DATA"

    invoke-direct {v0, v1, v3}, Lcph;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcph;->b:Lcph;

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Lcph;

    sget-object v1, Lcph;->a:Lcph;

    aput-object v1, v0, v2

    sget-object v1, Lcph;->b:Lcph;

    aput-object v1, v0, v3

    sput-object v0, Lcph;->c:[Lcph;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcph;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcph;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcph;

    return-object v0
.end method

.method public static values()[Lcph;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcph;->c:[Lcph;

    invoke-virtual {v0}, [Lcph;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcph;

    return-object v0
.end method
