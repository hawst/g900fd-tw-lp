.class public final Ljnc;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static b:Z

.field private static c:I

.field private static d:I

.field private static e:I


# instance fields
.field public a:Ljne;

.field private f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 41
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 60
    invoke-virtual {p0}, Ljnc;->b()V

    .line 49
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 96
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 97
    iget-object v1, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 98
    iget-object v1, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d()V

    .line 96
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Ljnc;->a:Ljne;

    .line 102
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 75
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Ljnc;->g:I

    if-ge v0, v1, :cond_1

    .line 76
    iget-object v1, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    new-instance v2, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v2, p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v0

    .line 78
    iget-object v1, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 79
    iget-object v1, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 80
    iget-object v1, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 81
    iget-object v1, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    new-instance v2, Ljnd;

    invoke-direct {v2, p0}, Ljnd;-><init>(Ljnc;)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_1
    return-void
.end method

.method public a([Loey;Ljne;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-virtual {p0}, Ljnc;->removeAllViews()V

    .line 106
    iput-object p2, p0, Ljnc;->a:Ljne;

    .line 108
    if-nez p1, :cond_0

    move v0, v1

    .line 109
    :goto_0
    const/16 v2, 0xa

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ljnc;->g:I

    .line 111
    invoke-virtual {p0}, Ljnc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljnc;->a(Landroid/content/Context;)V

    .line 112
    sget v0, Ljnc;->e:I

    invoke-virtual {p0, v0}, Ljnc;->setBackgroundColor(I)V

    .line 114
    :goto_1
    iget v0, p0, Ljnc;->g:I

    if-ge v1, v0, :cond_1

    .line 115
    aget-object v0, p1, v1

    .line 116
    iget-object v2, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v1

    iget-object v3, v0, Loey;->b:Ljava/lang/String;

    iget-object v0, v0, Loey;->d:Ljava/lang/String;

    .line 117
    invoke-static {v0}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-virtual {v2, v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Ljnc;->addView(Landroid/view/View;)V

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 108
    :cond_0
    array-length v0, p1

    goto :goto_0

    .line 120
    :cond_1
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 64
    sget-boolean v0, Ljnc;->b:Z

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Ljnc;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 66
    const v1, 0x7f0d0137

    .line 67
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Ljnc;->c:I

    .line 68
    invoke-virtual {p0}, Ljnc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhss;->c(Landroid/content/Context;)I

    move-result v1

    sput v1, Ljnc;->d:I

    .line 69
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Ljnc;->e:I

    .line 70
    const/4 v0, 0x1

    sput-boolean v0, Ljnc;->b:Z

    .line 72
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 147
    sub-int v0, p4, p2

    .line 149
    sget v2, Ljnc;->d:I

    sget v3, Ljnc;->c:I

    add-int/2addr v2, v3

    .line 152
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_0

    invoke-virtual {p0}, Ljnc;->getLayoutDirection()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 153
    sget v3, Ljnc;->d:I

    sub-int/2addr v0, v3

    .line 154
    neg-int v2, v2

    :goto_0
    move v3, v0

    move v0, v1

    .line 159
    :goto_1
    iget v4, p0, Ljnc;->h:I

    if-ge v0, v4, :cond_1

    .line 160
    iget-object v4, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v4, v4, v0

    sget v5, Ljnc;->d:I

    add-int/2addr v5, v3

    sget v6, Ljnc;->d:I

    invoke-virtual {v4, v3, v1, v5, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 161
    add-int/2addr v3, v2

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 156
    goto :goto_0

    .line 163
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 124
    const v0, 0x7fffffff

    invoke-static {v0, p1}, Ljnc;->resolveSize(II)I

    move-result v1

    .line 126
    sget v0, Ljnc;->d:I

    div-int v0, v1, v0

    iput v0, p0, Ljnc;->h:I

    .line 127
    iget v0, p0, Ljnc;->h:I

    sget v2, Ljnc;->d:I

    mul-int/2addr v0, v2

    iget v2, p0, Ljnc;->h:I

    add-int/lit8 v2, v2, -0x1

    sget v3, Ljnc;->c:I

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 129
    if-le v0, v1, :cond_0

    .line 130
    iget v0, p0, Ljnc;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ljnc;->h:I

    .line 133
    :cond_0
    iget v0, p0, Ljnc;->h:I

    iget v2, p0, Ljnc;->g:I

    if-le v0, v2, :cond_1

    .line 134
    iget v0, p0, Ljnc;->g:I

    iput v0, p0, Ljnc;->h:I

    .line 137
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Ljnc;->h:I

    if-ge v0, v2, :cond_2

    .line 138
    iget-object v2, p0, Ljnc;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v0

    sget v3, Ljnc;->d:I

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    sget v4, Ljnc;->d:I

    .line 139
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 138
    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_2
    sget v0, Ljnc;->d:I

    invoke-virtual {p0, v1, v0}, Ljnc;->setMeasuredDimension(II)V

    .line 143
    return-void
.end method
