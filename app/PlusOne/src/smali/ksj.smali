.class public final Lksj;
.super Llol;
.source "PG"


# static fields
.field private static final N:Lhmk;

.field private static final O:Lhmk;

.field private static final P:Lhmk;

.field private static final Q:Lhmk;

.field private static final R:Lhmk;

.field private static final S:Lhmk;


# instance fields
.field private final T:Lksn;

.field private final U:Lksm;

.field private V:Lkrs;

.field private W:Landroid/widget/ImageButton;

.field private X:Landroid/widget/TextView;

.field private Y:Landroid/widget/ImageButton;

.field private Z:Landroid/widget/ImageButton;

.field private aa:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

.field private ab:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private ac:Landroid/widget/TextView;

.field private ad:Landroid/widget/TextView;

.field private ae:Landroid/view/View;

.field private af:Landroid/view/View;

.field private ag:Landroid/view/View;

.field private ah:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lhmk;

    sget-object v1, Lonk;->i:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lksj;->N:Lhmk;

    .line 41
    new-instance v0, Lhmk;

    sget-object v1, Lonk;->h:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lksj;->O:Lhmk;

    .line 43
    new-instance v0, Lhmk;

    sget-object v1, Lonk;->j:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lksj;->P:Lhmk;

    .line 45
    new-instance v0, Lhmk;

    sget-object v1, Lonk;->m:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lksj;->Q:Lhmk;

    .line 47
    new-instance v0, Lhmk;

    sget-object v1, Lonk;->k:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lksj;->R:Lhmk;

    .line 49
    new-instance v0, Lhmk;

    sget-object v1, Lonk;->g:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lksj;->S:Lhmk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Llol;-><init>()V

    .line 53
    new-instance v0, Lksn;

    invoke-direct {v0, p0}, Lksn;-><init>(Lksj;)V

    iput-object v0, p0, Lksj;->T:Lksn;

    .line 54
    new-instance v0, Lksm;

    invoke-direct {v0, p0}, Lksm;-><init>(Lksj;)V

    iput-object v0, p0, Lksj;->U:Lksm;

    .line 285
    return-void
.end method

.method static synthetic a(Lksj;)Lkrs;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lksj;->V:Lkrs;

    return-object v0
.end method

.method static synthetic a(Lksj;Lkrs;)Lkrs;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lksj;->V:Lkrs;

    return-object p1
.end method

.method static synthetic a(Lksj;Z)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lksj;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 224
    iget-object v3, p0, Lksj;->Y:Landroid/widget/ImageButton;

    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 226
    iget-object v3, p0, Lksj;->Z:Landroid/widget/ImageButton;

    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lksj;->W:Landroid/widget/ImageButton;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 229
    return-void

    :cond_0
    move v0, v2

    .line 224
    goto :goto_0

    :cond_1
    move v0, v2

    .line 226
    goto :goto_1

    :cond_2
    move v1, v2

    .line 228
    goto :goto_2
.end method

.method static synthetic b(Lksj;)Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lksj;->aa:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    return-object v0
.end method

.method static synthetic c(Lksj;)Lksm;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lksj;->U:Lksm;

    return-object v0
.end method

.method static synthetic d(Lksj;)V
    .locals 6

    .prologue
    const v5, 0x7f0a0258

    const v4, 0x7f0a0251

    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lksj;->V:Lkrs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->i()Lizu;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lksj;->af:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lksj;->ag:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lksj;->ae:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lksj;->aa:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->setVisibility(I)V

    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->d()Z

    move-result v2

    iget-object v3, p0, Lksj;->W:Landroid/widget/ImageButton;

    if-eqz v2, :cond_4

    sget-object v0, Lksj;->O:Lhmk;

    :goto_1
    invoke-static {v3, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    iget-object v0, p0, Lksj;->W:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    iget-object v3, p0, Lksj;->W:Landroid/widget/ImageButton;

    if-eqz v2, :cond_5

    const v0, 0x7f0a025c

    :goto_2
    invoke-virtual {p0, v0}, Lksj;->i_(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->h()Z

    move-result v2

    iget-object v3, p0, Lksj;->X:Landroid/widget/TextView;

    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lksj;->X:Landroid/widget/TextView;

    if-eqz v2, :cond_7

    invoke-virtual {p0, v4}, Lksj;->i_(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lksj;->X:Landroid/widget/TextView;

    if-eqz v2, :cond_8

    invoke-virtual {p0, v4}, Lksj;->i_(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lksj;->X:Landroid/widget/TextView;

    if-eqz v2, :cond_9

    const v0, 0x7f0200cc

    :goto_6
    invoke-virtual {v3, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v3, p0, Lksj;->X:Landroid/widget/TextView;

    if-eqz v2, :cond_a

    sget-object v0, Lksj;->Q:Lhmk;

    :goto_7
    invoke-static {v3, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lksj;->X:Landroid/widget/TextView;

    invoke-static {v0}, Lhmc;->b(Landroid/view/View;)V

    iget-object v0, p0, Lksj;->X:Landroid/widget/TextView;

    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    :cond_2
    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lksj;->ah:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lksj;->ah:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    :cond_3
    iget-object v0, p0, Lksj;->V:Lkrs;

    invoke-interface {v0}, Lkrs;->h()Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_8
    invoke-direct {p0, v0}, Lksj;->a(Z)V

    iget-object v0, p0, Lksj;->aa:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    iget-object v2, p0, Lksj;->V:Lkrs;

    invoke-interface {v2}, Lkrs;->i()Lizu;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->a(Lizu;)V

    iget-object v0, p0, Lksj;->aa:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    iget-object v2, p0, Lksj;->V:Lkrs;

    invoke-interface {v2}, Lkrs;->k()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lkre;

    sget-object v4, Lonk;->n:Lhmn;

    invoke-direct {v3, v4, v2}, Lkre;-><init>(Lhmn;Ljava/lang/String;)V

    invoke-static {v0, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    iget-object v0, p0, Lksj;->aa:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v0}, Lhmc;->b(Landroid/view/View;)V

    iget-object v0, p0, Lksj;->aa:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    iget-object v0, p0, Lksj;->ab:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v2, 0x0

    iget-object v3, p0, Lksj;->V:Lkrs;

    invoke-interface {v3}, Lkrs;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lksj;->ab:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    iget-object v0, p0, Lksj;->ab:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lksj;->ac:Landroid/widget/TextView;

    iget-object v1, p0, Lksj;->V:Lkrs;

    invoke-interface {v1}, Lkrs;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lksj;->ad:Landroid/widget/TextView;

    iget-object v1, p0, Lksj;->V:Lkrs;

    invoke-interface {v1}, Lkrs;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lksj;->N:Lhmk;

    goto/16 :goto_1

    :cond_5
    const v0, 0x7f0a00e3

    goto/16 :goto_2

    :cond_6
    const/4 v0, 0x4

    goto/16 :goto_3

    :cond_7
    invoke-virtual {p0, v5}, Lksj;->i_(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_4

    :cond_8
    invoke-virtual {p0, v5}, Lksj;->i_(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_5

    :cond_9
    const v0, 0x7f020100

    goto/16 :goto_6

    :cond_a
    sget-object v0, Lksj;->P:Lhmk;

    goto/16 :goto_7

    :cond_b
    move v0, v1

    goto :goto_8
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lksj;->V:Lkrs;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lksj;->V:Lkrs;

    iget-object v1, p0, Lksj;->U:Lksm;

    invoke-interface {v0, v1}, Lkrs;->b(Lkrl;)V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lksj;->V:Lkrs;

    .line 145
    :cond_0
    invoke-virtual {p0}, Lksj;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lksj;->T:Lksn;

    invoke-virtual {v0, v1}, Lz;->unbindService(Landroid/content/ServiceConnection;)V

    .line 147
    iget-object v0, p0, Lksj;->ah:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 148
    invoke-super {p0}, Llol;->A()V

    .line 149
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lksj;->at:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 92
    const v1, 0x7f040215

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 95
    const v0, 0x7f1005f1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lksj;->af:Landroid/view/View;

    .line 96
    const v0, 0x7f1005f2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lksj;->ae:Landroid/view/View;

    .line 97
    const v0, 0x7f1005f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lksj;->ag:Landroid/view/View;

    .line 98
    const v0, 0x7f1005f7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lksj;->W:Landroid/widget/ImageButton;

    .line 99
    const v0, 0x7f1005f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lksj;->X:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f1005f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lksj;->Y:Landroid/widget/ImageButton;

    .line 101
    const v0, 0x7f100508

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lksj;->Z:Landroid/widget/ImageButton;

    .line 102
    const v0, 0x7f100114

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    iput-object v0, p0, Lksj;->aa:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    .line 104
    const v0, 0x7f100412

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lksj;->ab:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 105
    const v0, 0x7f100413

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lksj;->ac:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f1005f3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lksj;->ad:Landroid/widget/TextView;

    .line 109
    new-instance v0, Lksl;

    invoke-direct {v0, p0}, Lksl;-><init>(Lksj;)V

    .line 112
    iget-object v2, p0, Lksj;->Y:Landroid/widget/ImageButton;

    sget-object v3, Lksj;->R:Lhmk;

    invoke-static {v2, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 113
    iget-object v2, p0, Lksj;->Z:Landroid/widget/ImageButton;

    sget-object v3, Lksj;->S:Lhmk;

    invoke-static {v2, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 115
    iget-object v2, p0, Lksj;->W:Landroid/widget/ImageButton;

    sget-object v3, Lksj;->O:Lhmk;

    invoke-static {v2, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 116
    iget-object v2, p0, Lksj;->X:Landroid/widget/TextView;

    sget-object v3, Lksj;->P:Lhmk;

    invoke-static {v2, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 118
    new-instance v2, Lhmi;

    invoke-direct {v2, v0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lksj;->W:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, p0, Lksj;->X:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lksj;->Y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lksj;->Z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    return-object v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 78
    new-instance v0, Lksk;

    invoke-direct {v0, p0}, Lksk;-><init>(Lksj;)V

    iput-object v0, p0, Lksj;->ah:Ljava/lang/Runnable;

    .line 84
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 130
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0}, Llol;->g()V

    .line 135
    invoke-virtual {p0}, Lksj;->n()Lz;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lksj;->n()Lz;

    move-result-object v2

    const-class v3, Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lksj;->T:Lksn;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lz;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 137
    return-void
.end method
