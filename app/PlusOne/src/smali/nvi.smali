.class public final Lnvi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2512
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2519
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnvi;->c:[I

    .line 2512
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2539
    const/4 v0, 0x1

    iget-object v2, p0, Lnvi;->a:Ljava/lang/String;

    .line 2541
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2542
    iget-object v2, p0, Lnvi;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 2543
    const/4 v2, 0x2

    iget-object v3, p0, Lnvi;->b:Ljava/lang/Integer;

    .line 2544
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2546
    :cond_0
    iget-object v2, p0, Lnvi;->c:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnvi;->c:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 2548
    iget-object v3, p0, Lnvi;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    .line 2550
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 2548
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2552
    :cond_1
    add-int/2addr v0, v2

    .line 2553
    iget-object v1, p0, Lnvi;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2555
    :cond_2
    iget-object v1, p0, Lnvi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2556
    iput v0, p0, Lnvi;->ai:I

    .line 2557
    return v0
.end method

.method public a(Loxn;)Lnvi;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2565
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2566
    sparse-switch v0, :sswitch_data_0

    .line 2570
    iget-object v1, p0, Lnvi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2571
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnvi;->ah:Ljava/util/List;

    .line 2574
    :cond_1
    iget-object v1, p0, Lnvi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2576
    :sswitch_0
    return-object p0

    .line 2581
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvi;->a:Ljava/lang/String;

    goto :goto_0

    .line 2585
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnvi;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2589
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 2590
    iget-object v0, p0, Lnvi;->c:[I

    array-length v0, v0

    .line 2591
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 2592
    iget-object v2, p0, Lnvi;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2593
    iput-object v1, p0, Lnvi;->c:[I

    .line 2594
    :goto_1
    iget-object v1, p0, Lnvi;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 2595
    iget-object v1, p0, Lnvi;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 2596
    invoke-virtual {p1}, Loxn;->a()I

    .line 2594
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2599
    :cond_2
    iget-object v1, p0, Lnvi;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 2566
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2524
    const/4 v0, 0x1

    iget-object v1, p0, Lnvi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2525
    iget-object v0, p0, Lnvi;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2526
    const/4 v0, 0x2

    iget-object v1, p0, Lnvi;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2528
    :cond_0
    iget-object v0, p0, Lnvi;->c:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnvi;->c:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 2529
    iget-object v1, p0, Lnvi;->c:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 2530
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 2529
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2533
    :cond_1
    iget-object v0, p0, Lnvi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2535
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2508
    invoke-virtual {p0, p1}, Lnvi;->a(Loxn;)Lnvi;

    move-result-object v0

    return-object v0
.end method
