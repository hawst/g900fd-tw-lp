.class final Lezz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Ligy;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Ligv;

.field private c:Livx;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lezz;->c:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Liwl;

    .line 85
    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 87
    return-void
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lezz;->a:Landroid/app/Activity;

    .line 78
    iput-object p3, p0, Lezz;->b:Ligv;

    .line 79
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lezz;->c:Livx;

    .line 80
    return-void
.end method

.method public a(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 94
    const/4 v0, -0x1

    if-ne p5, v0, :cond_0

    .line 95
    iget-object v0, p0, Lezz;->b:Ligv;

    invoke-interface {v0, v3}, Ligv;->a(I)V

    .line 125
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lezz;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 100
    const-string v1, "version"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 101
    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lezz;->b:Ligv;

    invoke-interface {v0, v4}, Ligv;->a(I)V

    goto :goto_0

    .line 106
    :cond_1
    if-le v0, v3, :cond_2

    .line 107
    iget-object v0, p0, Lezz;->b:Ligv;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Lezz;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 112
    if-nez v0, :cond_3

    .line 113
    iget-object v0, p0, Lezz;->b:Ligv;

    invoke-interface {v0, v4}, Ligv;->a(I)V

    goto :goto_0

    .line 117
    :cond_3
    iget-object v1, p0, Lezz;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lezz;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    .line 119
    iget-object v0, p0, Lezz;->b:Ligv;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_0

    .line 123
    :cond_4
    iget-object v0, p0, Lezz;->a:Landroid/app/Activity;

    invoke-static {v0, p5}, Leyq;->c(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lezz;->b:Ligv;

    invoke-interface {v1, v0}, Ligv;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
