.class public Lcsd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llra;
.implements Llrg;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcsd;->a:Ljava/util/Set;

    .line 44
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 45
    return-void
.end method


# virtual methods
.method public E_()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcsd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 50
    return-void
.end method

.method a(Lbb;I)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcsd;->a:Ljava/util/Set;

    new-instance v1, Lcse;

    invoke-direct {v1, p1, p2}, Lcse;-><init>(Lbb;I)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method b()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcsd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcse;

    .line 62
    invoke-virtual {v0}, Lcse;->a()V

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method b(Lbb;I)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcsd;->a:Ljava/util/Set;

    new-instance v1, Lcse;

    invoke-direct {v1, p1, p2}, Lcse;-><init>(Lbb;I)V

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcsd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcse;

    .line 68
    invoke-virtual {v0}, Lcse;->b()V

    goto :goto_0

    .line 70
    :cond_0
    return-void
.end method
