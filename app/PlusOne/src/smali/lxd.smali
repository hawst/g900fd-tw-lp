.class public final Llxd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxd;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2836
    const/4 v0, 0x0

    new-array v0, v0, [Llxd;

    sput-object v0, Llxd;->a:[Llxd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2837
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2840
    const/4 v0, 0x0

    iput-object v0, p0, Llxd;->b:Llxp;

    .line 2837
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2864
    const/4 v0, 0x0

    .line 2865
    iget-object v1, p0, Llxd;->b:Llxp;

    if-eqz v1, :cond_0

    .line 2866
    const/4 v0, 0x1

    iget-object v1, p0, Llxd;->b:Llxp;

    .line 2867
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2869
    :cond_0
    iget-object v1, p0, Llxd;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2870
    const/4 v1, 0x2

    iget-object v2, p0, Llxd;->c:Ljava/lang/String;

    .line 2871
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2873
    :cond_1
    iget-object v1, p0, Llxd;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2874
    const/4 v1, 0x3

    iget-object v2, p0, Llxd;->d:Ljava/lang/String;

    .line 2875
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2877
    :cond_2
    iget-object v1, p0, Llxd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2878
    iput v0, p0, Llxd;->ai:I

    .line 2879
    return v0
.end method

.method public a(Loxn;)Llxd;
    .locals 2

    .prologue
    .line 2887
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2888
    sparse-switch v0, :sswitch_data_0

    .line 2892
    iget-object v1, p0, Llxd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2893
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxd;->ah:Ljava/util/List;

    .line 2896
    :cond_1
    iget-object v1, p0, Llxd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2898
    :sswitch_0
    return-object p0

    .line 2903
    :sswitch_1
    iget-object v0, p0, Llxd;->b:Llxp;

    if-nez v0, :cond_2

    .line 2904
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxd;->b:Llxp;

    .line 2906
    :cond_2
    iget-object v0, p0, Llxd;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2910
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxd;->c:Ljava/lang/String;

    goto :goto_0

    .line 2914
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxd;->d:Ljava/lang/String;

    goto :goto_0

    .line 2888
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2849
    iget-object v0, p0, Llxd;->b:Llxp;

    if-eqz v0, :cond_0

    .line 2850
    const/4 v0, 0x1

    iget-object v1, p0, Llxd;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2852
    :cond_0
    iget-object v0, p0, Llxd;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2853
    const/4 v0, 0x2

    iget-object v1, p0, Llxd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2855
    :cond_1
    iget-object v0, p0, Llxd;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2856
    const/4 v0, 0x3

    iget-object v1, p0, Llxd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2858
    :cond_2
    iget-object v0, p0, Llxd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2860
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2833
    invoke-virtual {p0, p1}, Llxd;->a(Loxn;)Llxd;

    move-result-object v0

    return-object v0
.end method
