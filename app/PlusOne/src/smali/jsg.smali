.class public final Ljsg;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhio;
.implements Lhyz;
.implements Lhzq;
.implements Ljoz;


# static fields
.field private static N:Landroid/graphics/drawable/Drawable;


# instance fields
.field private O:Lhzb;

.field private P:I

.field private Q:Ljpr;

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field

.field private S:Lhzl;

.field private T:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field

.field private U:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljsw;",
            ">;"
        }
    .end annotation
.end field

.field private V:Ljsb;

.field private W:Lhir;

.field private X:I

.field private Y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Llol;-><init>()V

    .line 56
    sget-object v0, Ljpx;->a:Ljqc;

    iput-object v0, p0, Ljsg;->T:Ljqc;

    .line 57
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ljsg;->U:Landroid/util/SparseArray;

    return-void
.end method

.method private U()V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Ljsg;->R:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljsg;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljsg;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Ljsg;->X:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Ljsg;->W:Lhir;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Ljsg;->W:Lhir;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lhir;->setVisibility(I)V

    .line 310
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p0}, Ljsg;->k()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    .line 294
    :goto_0
    return p2

    :cond_0
    invoke-virtual {p0}, Ljsg;->k()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p2

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 163
    invoke-static {p1}, Lhiu;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public a(I)Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 238
    iget-object v0, p0, Ljsg;->U:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsw;

    .line 239
    if-nez v0, :cond_0

    .line 240
    iget-object v0, p0, Ljsg;->R:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    .line 241
    invoke-interface {v0}, Ljpv;->a()Ljava/lang/String;

    move-result-object v3

    .line 242
    const/4 v2, 0x1

    .line 247
    iget-object v1, p0, Ljsg;->S:Lhzl;

    instance-of v1, v1, Lhzk;

    if-eqz v1, :cond_1

    .line 248
    iget-object v1, p0, Ljsg;->S:Lhzl;

    check-cast v1, Lhzk;

    .line 249
    const/16 v4, 0xaa

    .line 250
    invoke-interface {v1, v4, v3}, Lhzk;->b(ILjava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 251
    instance-of v3, v1, Ljsw;

    if-eqz v3, :cond_1

    .line 252
    check-cast v1, Ljsw;

    invoke-virtual {v1}, Ljsw;->c()Z

    move-result v1

    .line 255
    :goto_0
    invoke-static {}, Ljsw;->a()Ljsy;

    move-result-object v2

    .line 256
    invoke-virtual {v2, v0}, Ljsy;->a(Ljpv;)Ljsy;

    move-result-object v0

    .line 257
    invoke-virtual {v0, v1}, Ljsy;->a(Z)Ljsy;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Ljsy;->a()Ljsw;

    move-result-object v0

    .line 259
    iget-object v1, p0, Ljsg;->U:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 261
    :cond_0
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public a(ILandroid/view/View;)V
    .locals 4

    .prologue
    .line 216
    iget-object v0, p0, Ljsg;->R:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    .line 217
    check-cast p2, Lhiu;

    .line 218
    invoke-interface {v0}, Ljpv;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    invoke-interface {v0}, Ljpv;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljsg;->N:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljsg;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Ljsg;->N:Landroid/graphics/drawable/Drawable;

    :cond_0
    sget-object v2, Ljsg;->N:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v1, v2}, Lhiu;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 223
    :goto_0
    invoke-virtual {p2}, Lhiu;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->a(Ljpv;)Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->a()Lhis;

    move-result-object v0

    invoke-virtual {p0, p1}, Ljsg;->a(I)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhis;->a(Landroid/os/Parcelable;)V

    .line 225
    return-void

    .line 221
    :cond_1
    invoke-interface {v0}, Ljpv;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lhiu;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 73
    const-string v0, "people_people_count per_page"

    const/4 v1, 0x6

    invoke-direct {p0, v0, v1}, Ljsg;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ljsg;->Y:I

    .line 74
    if-eqz p1, :cond_0

    .line 75
    const-string v0, "people_people_count per_page"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ljsg;->X:I

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    iget v0, p0, Ljsg;->Y:I

    iput v0, p0, Ljsg;->X:I

    goto :goto_0
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ljsg;->S:Lhzl;

    if-eqz v0, :cond_0

    instance-of v0, p1, Ljsw;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Ljsg;->S:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->c(Landroid/os/Parcelable;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 127
    check-cast v0, Ljsw;

    invoke-virtual {v0}, Ljsw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Ljsg;->S:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Ljsg;->S:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->a(Landroid/os/Parcelable;)Z

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 229
    check-cast p1, Lhir;

    iput-object p1, p0, Ljsg;->W:Lhir;

    .line 230
    iget-object v0, p0, Ljsg;->W:Lhir;

    const-string v1, "people_footer_title_res_id"

    const v2, 0x7f0a04f0

    invoke-direct {p0, v1, v2}, Ljsg;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lhir;->a(I)Lhir;

    .line 231
    iget-object v0, p0, Ljsg;->W:Lhir;

    invoke-virtual {v0, p0}, Lhir;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    invoke-direct {p0}, Ljsg;->U()V

    .line 233
    return-void
.end method

.method public a(Lhzb;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Ljsg;->O:Lhzb;

    .line 272
    return-void
.end method

.method public a(Ljpi;)V
    .locals 1

    .prologue
    .line 148
    invoke-interface {p1}, Ljpi;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljsg;->R:Ljava/util/List;

    .line 149
    invoke-interface {p1}, Ljpi;->b()V

    .line 150
    iget-object v0, p0, Ljsg;->O:Lhzb;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Ljsg;->O:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 153
    :cond_0
    invoke-direct {p0}, Ljsg;->U()V

    .line 154
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Ljsg;->V:Ljsb;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljsg;->V:Ljsb;

    iget-boolean v0, v0, Ljsb;->b:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    sget-object v0, Ljpx;->d:Ljqc;

    :goto_0
    iput-object v0, p0, Ljsg;->T:Ljqc;

    .line 91
    iget-object v0, p0, Ljsg;->S:Lhzl;

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    .line 92
    iget-object v0, p0, Ljsg;->S:Lhzl;

    invoke-interface {v0}, Lhzl;->c()Ljava/util/List;

    move-result-object v3

    .line 93
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 94
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 98
    instance-of v1, v0, Ljsw;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljsw;

    invoke-virtual {v1}, Ljsw;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    :cond_0
    instance-of v1, v0, Ljsw;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljsw;

    .line 106
    invoke-virtual {v1}, Ljsw;->b()Ljpv;

    move-result-object v1

    .line 105
    invoke-static {v1}, Ljpw;->a(Ljpv;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    iget-object v1, p0, Ljsg;->S:Lhzl;

    invoke-interface {v1, v0}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 93
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 90
    :cond_2
    sget-object v0, Ljpx;->b:Ljqc;

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    sget-object v0, Ljpx;->c:Ljqc;

    goto :goto_0

    :cond_4
    sget-object v0, Ljpx;->a:Ljqc;

    goto :goto_0

    .line 111
    :cond_5
    invoke-virtual {p0}, Ljsg;->e()V

    .line 112
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return v0
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 202
    invoke-super {p0}, Llol;->ae_()V

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Ljsg;->W:Lhir;

    .line 204
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 168
    new-instance v0, Lhiu;

    invoke-direct {v0, p1}, Lhiu;-><init>(Landroid/content/Context;)V

    .line 169
    new-instance v1, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lhiu;->a(Landroid/view/View;)V

    .line 170
    return-object v0
.end method

.method public c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 185
    new-instance v0, Lhir;

    invoke-direct {v0, p1}, Lhir;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljsg;->W:Lhir;

    .line 186
    iget-object v0, p0, Ljsg;->W:Lhir;

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 139
    iget-object v0, p0, Ljsg;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Ljsg;->P:I

    .line 140
    iget-object v0, p0, Ljsg;->au:Llnh;

    const-class v1, Ljpr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpr;

    iput-object v0, p0, Ljsg;->Q:Ljpr;

    .line 141
    iget-object v0, p0, Ljsg;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Ljsg;->S:Lhzl;

    .line 142
    iget-object v0, p0, Ljsg;->au:Llnh;

    const-class v1, Ljsb;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsb;

    iput-object v0, p0, Ljsg;->V:Ljsb;

    .line 143
    iget-object v0, p0, Ljsg;->au:Llnh;

    const-class v1, Lhin;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhin;

    invoke-virtual {v0, p0}, Lhin;->a(Lhio;)V

    .line 144
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Ljsg;->R:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ljsg;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 6

    .prologue
    .line 276
    iget-object v0, p0, Ljsg;->Q:Ljpr;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Ljsg;->U:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 278
    iget-object v0, p0, Ljsg;->Q:Ljpr;

    iget v2, p0, Ljsg;->P:I

    const-string v1, "people_sort_order"

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Ljsg;->a(Ljava/lang/String;I)I

    move-result v3

    iget v4, p0, Ljsg;->X:I

    iget-object v5, p0, Ljsg;->T:Ljqc;

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Ljpr;->a(Ljoz;IIILjqc;)V

    .line 280
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 85
    const-string v0, "people_people_count per_page"

    iget v1, p0, Ljsg;->X:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Llol;->g()V

    .line 67
    invoke-virtual {p0}, Ljsg;->e()V

    .line 68
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Ljsg;->W:Lhir;

    if-ne p1, v0, :cond_0

    .line 209
    iget v0, p0, Ljsg;->X:I

    iget v1, p0, Ljsg;->Y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljsg;->X:I

    .line 210
    invoke-virtual {p0}, Ljsg;->e()V

    .line 212
    :cond_0
    return-void
.end method
