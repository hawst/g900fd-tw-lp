.class public final Llly;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lllt;


# instance fields
.field private final a:Llmc;

.field private final b:Ljava/lang/String;

.field private final c:Lorg/chromium/net/HttpUrlRequestListener;

.field private volatile d:Lorg/chromium/net/HttpUrlRequest;

.field private volatile e:Ljava/util/concurrent/CountDownLatch;

.field private volatile f:Llmb;

.field private volatile g:Z

.field private h:Llmq;

.field private i:J

.field private j:Z


# direct methods
.method public constructor <init>(Llmc;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lllz;

    invoke-direct {v0, p0}, Lllz;-><init>(Llly;)V

    iput-object v0, p0, Llly;->c:Lorg/chromium/net/HttpUrlRequestListener;

    .line 107
    const-string v0, "httpUrlRequestFactory cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmc;

    iput-object v0, p0, Llly;->a:Llmc;

    .line 109
    const-string v0, "url cannot be empty"

    invoke-static {p2, v0}, Llsk;->a(Ljava/lang/CharSequence;Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Llly;->b:Ljava/lang/String;

    .line 110
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Llly;->i:J

    .line 111
    return-void
.end method

.method static synthetic a(Llly;Llmb;)Llmb;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Llly;->f:Llmb;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Llmc;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Llma;

    invoke-direct {v0, p0}, Llma;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Llly;)Lorg/chromium/net/HttpUrlRequest;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Llly;->d:Lorg/chromium/net/HttpUrlRequest;

    return-object v0
.end method

.method private a(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 177
    iget-wide v2, p0, Llly;->i:J

    cmp-long v2, v2, p1

    if-eqz v2, :cond_1

    .line 178
    invoke-direct {p0}, Llly;->b()V

    .line 179
    new-instance v2, Llmp;

    invoke-direct {v2}, Llmp;-><init>()V

    invoke-virtual {v2}, Llmp;->b()Llmq;

    move-result-object v3

    iput-object v3, p0, Llly;->h:Llmq;

    iget-object v3, p0, Llly;->a:Llmc;

    iget-object v4, p0, Llly;->b:Ljava/lang/String;

    invoke-virtual {v2}, Llmp;->a()Ljava/nio/channels/WritableByteChannel;

    move-result-object v2

    iget-object v5, p0, Llly;->c:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v3, v4, v2, v5}, Llmc;->a(Ljava/lang/String;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v2

    iput-object v2, p0, Llly;->d:Lorg/chromium/net/HttpUrlRequest;

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Llly;->d:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v2, p1, p2}, Lorg/chromium/net/HttpUrlRequest;->a(J)V

    :cond_0
    iput-wide p1, p0, Llly;->i:J

    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v2, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v2, p0, Llly;->e:Ljava/util/concurrent/CountDownLatch;

    iput-boolean v1, p0, Llly;->g:Z

    iget-object v1, p0, Llly;->d:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v1}, Lorg/chromium/net/HttpUrlRequest;->g()V

    .line 182
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Llly;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Llly;->g:Z

    return p1
.end method

.method private b(JLjava/nio/ByteBuffer;)I
    .locals 7

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Llly;->a(J)Z

    .line 157
    :try_start_0
    iget-object v0, p0, Llly;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Llly;->c()V

    .line 160
    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 161
    :goto_0
    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    :try_start_1
    iget-object v1, p0, Llly;->h:Llmq;

    invoke-interface {v1, p3}, Llmq;->read(Ljava/nio/ByteBuffer;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 168
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 169
    iget-wide v2, p0, Llly;->i:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Llly;->i:J

    goto :goto_0

    .line 157
    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Interrupted while waiting for request start"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 165
    :catch_1
    move-exception v0

    .line 166
    new-instance v1, Llmd;

    const-string v2, "Exception reading from network request channel"

    invoke-direct {v1, v2, v0}, Llmd;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 173
    :cond_0
    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    sub-int v0, v1, v0

    return v0
.end method

.method static synthetic b(Llly;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Llly;->e:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 209
    iget-object v0, p0, Llly;->d:Lorg/chromium/net/HttpUrlRequest;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Llly;->d:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->h()V

    .line 211
    iput-object v2, p0, Llly;->d:Lorg/chromium/net/HttpUrlRequest;

    .line 213
    :cond_0
    iget-object v0, p0, Llly;->h:Llmq;

    invoke-static {v0}, Llrz;->a(Ljava/io/Closeable;)V

    .line 214
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Llly;->i:J

    .line 215
    iput-object v2, p0, Llly;->f:Llmb;

    .line 216
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 222
    iget-object v0, p0, Llly;->f:Llmb;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Llly;->f:Llmb;

    iget-object v0, v0, Llmb;->b:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 224
    new-instance v0, Llmd;

    iget-object v1, p0, Llly;->f:Llmb;

    iget v1, v1, Llmb;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Exception starting connection, status code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Llly;->f:Llmb;

    iget-object v2, v2, Llmb;->b:Ljava/lang/Exception;

    invoke-direct {v0, v1, v2}, Llmd;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 227
    :cond_0
    new-instance v0, Llmd;

    iget-object v1, p0, Llly;->f:Llmb;

    iget v1, v1, Llmb;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x33

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error starting connection, status code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llmd;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_1
    return-void
.end method


# virtual methods
.method public a(JLjava/nio/ByteBuffer;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 115
    iget-boolean v0, p0, Llly;->j:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "cannot call fetchBytes() after it threw an exception"

    invoke-static {v0, v3}, Llsk;->b(ZLjava/lang/Object;)V

    .line 116
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_0

    move v2, v1

    :cond_0
    const-string v0, "startOffset must be non-negative"

    invoke-static {v2, v0}, Llsk;->a(ZLjava/lang/Object;)V

    .line 117
    const-string v0, "buffer must not be null"

    invoke-static {p3, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Llly;->b(JLjava/nio/ByteBuffer;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 123
    return v0

    :cond_1
    move v0, v2

    .line 115
    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    .line 127
    iput-boolean v1, p0, Llly;->j:Z

    throw v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Llly;->b()V

    .line 140
    return-void
.end method

.method public a(JJ)Z
    .locals 3

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Llly;->a(J)Z

    .line 147
    iget-boolean v0, p0, Llly;->g:Z

    if-eqz v0, :cond_0

    .line 148
    invoke-direct {p0}, Llly;->c()V

    .line 150
    :cond_0
    iget-object v0, p0, Llly;->h:Llmq;

    long-to-int v1, p3

    invoke-interface {v0, v1}, Llmq;->a(I)Z

    move-result v0

    return v0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Llly;->b()V

    .line 135
    return-void
.end method
