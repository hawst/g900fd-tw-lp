.class public final Lptm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lptm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lptl;

.field private c:Lptl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 243
    const v0, 0x2e57524

    new-instance v1, Lptn;

    invoke-direct {v1}, Lptn;-><init>()V

    .line 248
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lptm;->a:Loxr;

    .line 247
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 271
    const/4 v0, 0x0

    .line 272
    iget-object v1, p0, Lptm;->b:Lptl;

    if-eqz v1, :cond_0

    .line 273
    const/4 v0, 0x1

    iget-object v1, p0, Lptm;->b:Lptl;

    .line 274
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 276
    :cond_0
    iget-object v1, p0, Lptm;->c:Lptl;

    if-eqz v1, :cond_1

    .line 277
    const/4 v1, 0x2

    iget-object v2, p0, Lptm;->c:Lptl;

    .line 278
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    :cond_1
    iget-object v1, p0, Lptm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    iput v0, p0, Lptm;->ai:I

    .line 282
    return v0
.end method

.method public a(Loxn;)Lptm;
    .locals 2

    .prologue
    .line 290
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 291
    sparse-switch v0, :sswitch_data_0

    .line 295
    iget-object v1, p0, Lptm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 296
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lptm;->ah:Ljava/util/List;

    .line 299
    :cond_1
    iget-object v1, p0, Lptm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    :sswitch_0
    return-object p0

    .line 306
    :sswitch_1
    iget-object v0, p0, Lptm;->b:Lptl;

    if-nez v0, :cond_2

    .line 307
    new-instance v0, Lptl;

    invoke-direct {v0}, Lptl;-><init>()V

    iput-object v0, p0, Lptm;->b:Lptl;

    .line 309
    :cond_2
    iget-object v0, p0, Lptm;->b:Lptl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 313
    :sswitch_2
    iget-object v0, p0, Lptm;->c:Lptl;

    if-nez v0, :cond_3

    .line 314
    new-instance v0, Lptl;

    invoke-direct {v0}, Lptl;-><init>()V

    iput-object v0, p0, Lptm;->c:Lptl;

    .line 316
    :cond_3
    iget-object v0, p0, Lptm;->c:Lptl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 291
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lptm;->b:Lptl;

    if-eqz v0, :cond_0

    .line 260
    const/4 v0, 0x1

    iget-object v1, p0, Lptm;->b:Lptl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 262
    :cond_0
    iget-object v0, p0, Lptm;->c:Lptl;

    if-eqz v0, :cond_1

    .line 263
    const/4 v0, 0x2

    iget-object v1, p0, Lptm;->c:Lptl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 265
    :cond_1
    iget-object v0, p0, Lptm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 267
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0, p1}, Lptm;->a(Loxn;)Lptm;

    move-result-object v0

    return-object v0
.end method
