.class public final Lobp;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lodo;

.field private b:[Lodo;

.field private c:Lobl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 603
    invoke-direct {p0}, Loxq;-><init>()V

    .line 606
    iput-object v1, p0, Lobp;->a:Lodo;

    .line 609
    sget-object v0, Lodo;->a:[Lodo;

    iput-object v0, p0, Lobp;->b:[Lodo;

    .line 612
    iput-object v1, p0, Lobp;->c:Lobl;

    .line 603
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 636
    .line 637
    iget-object v0, p0, Lobp;->a:Lodo;

    if-eqz v0, :cond_3

    .line 638
    const/4 v0, 0x4

    iget-object v2, p0, Lobp;->a:Lodo;

    .line 639
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 641
    :goto_0
    iget-object v2, p0, Lobp;->b:[Lodo;

    if-eqz v2, :cond_1

    .line 642
    iget-object v2, p0, Lobp;->b:[Lodo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 643
    if-eqz v4, :cond_0

    .line 644
    const/4 v5, 0x5

    .line 645
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 642
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 649
    :cond_1
    iget-object v1, p0, Lobp;->c:Lobl;

    if-eqz v1, :cond_2

    .line 650
    const/4 v1, 0x6

    iget-object v2, p0, Lobp;->c:Lobl;

    .line 651
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 653
    :cond_2
    iget-object v1, p0, Lobp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 654
    iput v0, p0, Lobp;->ai:I

    .line 655
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lobp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 663
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 664
    sparse-switch v0, :sswitch_data_0

    .line 668
    iget-object v2, p0, Lobp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 669
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lobp;->ah:Ljava/util/List;

    .line 672
    :cond_1
    iget-object v2, p0, Lobp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 674
    :sswitch_0
    return-object p0

    .line 679
    :sswitch_1
    iget-object v0, p0, Lobp;->a:Lodo;

    if-nez v0, :cond_2

    .line 680
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobp;->a:Lodo;

    .line 682
    :cond_2
    iget-object v0, p0, Lobp;->a:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 686
    :sswitch_2
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 687
    iget-object v0, p0, Lobp;->b:[Lodo;

    if-nez v0, :cond_4

    move v0, v1

    .line 688
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lodo;

    .line 689
    iget-object v3, p0, Lobp;->b:[Lodo;

    if-eqz v3, :cond_3

    .line 690
    iget-object v3, p0, Lobp;->b:[Lodo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 692
    :cond_3
    iput-object v2, p0, Lobp;->b:[Lodo;

    .line 693
    :goto_2
    iget-object v2, p0, Lobp;->b:[Lodo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 694
    iget-object v2, p0, Lobp;->b:[Lodo;

    new-instance v3, Lodo;

    invoke-direct {v3}, Lodo;-><init>()V

    aput-object v3, v2, v0

    .line 695
    iget-object v2, p0, Lobp;->b:[Lodo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 696
    invoke-virtual {p1}, Loxn;->a()I

    .line 693
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 687
    :cond_4
    iget-object v0, p0, Lobp;->b:[Lodo;

    array-length v0, v0

    goto :goto_1

    .line 699
    :cond_5
    iget-object v2, p0, Lobp;->b:[Lodo;

    new-instance v3, Lodo;

    invoke-direct {v3}, Lodo;-><init>()V

    aput-object v3, v2, v0

    .line 700
    iget-object v2, p0, Lobp;->b:[Lodo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 704
    :sswitch_3
    iget-object v0, p0, Lobp;->c:Lobl;

    if-nez v0, :cond_6

    .line 705
    new-instance v0, Lobl;

    invoke-direct {v0}, Lobl;-><init>()V

    iput-object v0, p0, Lobp;->c:Lobl;

    .line 707
    :cond_6
    iget-object v0, p0, Lobp;->c:Lobl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 664
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x2a -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 617
    iget-object v0, p0, Lobp;->a:Lodo;

    if-eqz v0, :cond_0

    .line 618
    const/4 v0, 0x4

    iget-object v1, p0, Lobp;->a:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 620
    :cond_0
    iget-object v0, p0, Lobp;->b:[Lodo;

    if-eqz v0, :cond_2

    .line 621
    iget-object v1, p0, Lobp;->b:[Lodo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 622
    if-eqz v3, :cond_1

    .line 623
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 621
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 627
    :cond_2
    iget-object v0, p0, Lobp;->c:Lobl;

    if-eqz v0, :cond_3

    .line 628
    const/4 v0, 0x6

    iget-object v1, p0, Lobp;->c:Lobl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 630
    :cond_3
    iget-object v0, p0, Lobp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 632
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 599
    invoke-virtual {p0, p1}, Lobp;->a(Loxn;)Lobp;

    move-result-object v0

    return-object v0
.end method
