.class public final Lqbm;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/Integer;

.field public c:Lqbo;

.field public d:Lqbc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2186
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2189
    const/high16 v0, -0x80000000

    iput v0, p0, Lqbm;->a:I

    .line 2194
    iput-object v1, p0, Lqbm;->c:Lqbo;

    .line 2197
    iput-object v1, p0, Lqbm;->d:Lqbc;

    .line 2186
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2220
    const/4 v0, 0x0

    .line 2221
    iget v1, p0, Lqbm;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 2222
    const/4 v0, 0x1

    iget v1, p0, Lqbm;->a:I

    .line 2223
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2225
    :cond_0
    iget-object v1, p0, Lqbm;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2226
    const/4 v1, 0x2

    iget-object v2, p0, Lqbm;->b:Ljava/lang/Integer;

    .line 2227
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2229
    :cond_1
    iget-object v1, p0, Lqbm;->c:Lqbo;

    if-eqz v1, :cond_2

    .line 2230
    const/4 v1, 0x3

    iget-object v2, p0, Lqbm;->c:Lqbo;

    .line 2231
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2233
    :cond_2
    iget-object v1, p0, Lqbm;->d:Lqbc;

    if-eqz v1, :cond_3

    .line 2234
    const/4 v1, 0x4

    iget-object v2, p0, Lqbm;->d:Lqbc;

    .line 2235
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2237
    :cond_3
    iget-object v1, p0, Lqbm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2238
    iput v0, p0, Lqbm;->ai:I

    .line 2239
    return v0
.end method

.method public a(Loxn;)Lqbm;
    .locals 2

    .prologue
    .line 2247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2248
    sparse-switch v0, :sswitch_data_0

    .line 2252
    iget-object v1, p0, Lqbm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbm;->ah:Ljava/util/List;

    .line 2256
    :cond_1
    iget-object v1, p0, Lqbm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2258
    :sswitch_0
    return-object p0

    .line 2263
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2264
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 2268
    :cond_2
    iput v0, p0, Lqbm;->a:I

    goto :goto_0

    .line 2270
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lqbm;->a:I

    goto :goto_0

    .line 2275
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbm;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2279
    :sswitch_3
    iget-object v0, p0, Lqbm;->c:Lqbo;

    if-nez v0, :cond_4

    .line 2280
    new-instance v0, Lqbo;

    invoke-direct {v0}, Lqbo;-><init>()V

    iput-object v0, p0, Lqbm;->c:Lqbo;

    .line 2282
    :cond_4
    iget-object v0, p0, Lqbm;->c:Lqbo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2286
    :sswitch_4
    iget-object v0, p0, Lqbm;->d:Lqbc;

    if-nez v0, :cond_5

    .line 2287
    new-instance v0, Lqbc;

    invoke-direct {v0}, Lqbc;-><init>()V

    iput-object v0, p0, Lqbm;->d:Lqbc;

    .line 2289
    :cond_5
    iget-object v0, p0, Lqbm;->d:Lqbc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2248
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2202
    iget v0, p0, Lqbm;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2203
    const/4 v0, 0x1

    iget v1, p0, Lqbm;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2205
    :cond_0
    iget-object v0, p0, Lqbm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2206
    const/4 v0, 0x2

    iget-object v1, p0, Lqbm;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2208
    :cond_1
    iget-object v0, p0, Lqbm;->c:Lqbo;

    if-eqz v0, :cond_2

    .line 2209
    const/4 v0, 0x3

    iget-object v1, p0, Lqbm;->c:Lqbo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2211
    :cond_2
    iget-object v0, p0, Lqbm;->d:Lqbc;

    if-eqz v0, :cond_3

    .line 2212
    const/4 v0, 0x4

    iget-object v1, p0, Lqbm;->d:Lqbc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2214
    :cond_3
    iget-object v0, p0, Lqbm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2216
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2182
    invoke-virtual {p0, p1}, Lqbm;->a(Loxn;)Lqbm;

    move-result-object v0

    return-object v0
.end method
