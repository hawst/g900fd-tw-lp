.class public final Lun;
.super Landroid/widget/CheckBox;
.source "PG"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Lur;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010107

    aput v2, v0, v1

    sput-object v0, Lun;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const v0, 0x101006c

    invoke-direct {p0, p1, p2, v0}, Lun;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    sget-object v0, Lun;->a:[I

    invoke-static {p1, p2, v0, p3, v1}, Luw;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Luw;

    move-result-object v0

    .line 49
    invoke-virtual {v0, v1}, Luw;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lun;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 50
    invoke-virtual {v0}, Luw;->b()V

    .line 52
    invoke-virtual {v0}, Luw;->c()Lur;

    move-result-object v0

    iput-object v0, p0, Lun;->b:Lur;

    .line 53
    return-void
.end method


# virtual methods
.method public setButtonDrawable(I)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lun;->b:Lur;

    invoke-virtual {v0, p1}, Lur;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lun;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 58
    return-void
.end method
