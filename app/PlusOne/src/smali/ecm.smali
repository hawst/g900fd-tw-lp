.class public final Lecm;
.super Legi;
.source "PG"

# interfaces
.implements Lbc;
.implements Lctl;
.implements Lcvd;
.implements Lene;
.implements Lfuf;
.implements Lfyl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lctl;",
        "Lcvd",
        "<",
        "Ljava/util/List",
        "<",
        "Lctm;",
        ">;>;",
        "Lene",
        "<",
        "Lizu;",
        "Lcom/google/android/libraries/social/media/MediaResource;",
        ">;",
        "Lfuf;",
        "Lfyl;"
    }
.end annotation


# static fields
.field private static N:Lloz;


# instance fields
.field private ah:Lcom/google/android/apps/plus/views/FastScrollListView;

.field private ai:Lcom/google/android/apps/plus/views/FastScrollContainer;

.field private aj:Lcqx;

.field private ak:Z

.field private al:Z

.field private am:Ljava/lang/Integer;

.field private an:Z

.field private ao:Z

.field private ap:Lizs;

.field private aq:Landroid/net/Uri;

.field private ar:Lend;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lend",
            "<",
            "Lizu;",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field private as:Ljava/lang/Float;

.field private aw:Lcve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcve",
            "<",
            "Landroid/database/Cursor;",
            "Ljava/util/List",
            "<",
            "Lctm;",
            ">;>;"
        }
    .end annotation
.end field

.field private ax:Landroid/graphics/Rect;

.field private final ay:Licq;

.field private final az:Lfhh;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lloz;

    const-string v1, "compilation_list_view"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lecm;->N:Lloz;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Legi;-><init>()V

    .line 106
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lecm;->ax:Landroid/graphics/Rect;

    .line 108
    new-instance v0, Licq;

    iget-object v1, p0, Lecm;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 109
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lecm;->ay:Licq;

    .line 112
    new-instance v0, Lcsc;

    iget-object v1, p0, Lecm;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcsc;-><init>(Lu;Llqr;I)V

    .line 115
    new-instance v0, Lecn;

    invoke-direct {v0, p0}, Lecn;-><init>(Lecm;)V

    iput-object v0, p0, Lecm;->az:Lfhh;

    return-void
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 450
    iget-object v2, p0, Lecm;->am:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lecm;->am:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_1

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lecm;->am:Ljava/lang/Integer;

    .line 454
    iput-boolean v0, p0, Lecm;->al:Z

    .line 456
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lecm;->Z:Z

    .line 457
    iget-boolean v0, p0, Lecm;->Z:Z

    if-eqz v0, :cond_2

    .line 458
    invoke-virtual {p0}, Lecm;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 459
    const v2, 0x7f0a07ad

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 460
    invoke-virtual {p0}, Lecm;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 463
    :cond_2
    invoke-virtual {p0}, Lecm;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lecm;->d(Landroid/view/View;)V

    .line 465
    iget-object v0, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 456
    goto :goto_1
.end method

.method static synthetic a(Lecm;ILfib;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lecm;->a(ILfib;)V

    return-void
.end method

.method public static aa()Z
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lecm;->N:Lloz;

    const/4 v0, 0x0

    return v0
.end method

.method private ac()Ljava/lang/String;
    .locals 4

    .prologue
    .line 200
    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "owner_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 201
    if-nez v0, :cond_0

    iget-object v1, p0, Lecm;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 202
    iget-object v0, p0, Lecm;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    :cond_0
    const/4 v1, 0x2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 422
    if-nez p1, :cond_0

    .line 447
    :goto_0
    return-void

    .line 426
    :cond_0
    invoke-virtual {p0}, Lecm;->U()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 427
    iget-boolean v0, p0, Lecm;->ak:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lecm;->an:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 428
    :cond_1
    iget-object v0, p0, Lecm;->ay:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 436
    :goto_1
    invoke-virtual {p0}, Lecm;->U()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lecm;->as:Ljava/lang/Float;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lecm;->as:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_5

    .line 437
    iget-object v0, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lecm;->aj:Lcqx;

    iget-object v2, p0, Lecm;->as:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Lcqx;->a(F)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    .line 438
    iput-object v4, p0, Lecm;->as:Ljava/lang/Float;

    .line 444
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lecm;->Z_()V

    .line 446
    invoke-virtual {p0}, Lecm;->ag()V

    goto :goto_0

    .line 430
    :cond_3
    iget-object v0, p0, Lecm;->ay:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 433
    :cond_4
    iget-object v0, p0, Lecm;->ay:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 439
    :cond_5
    iget-object v0, p0, Lecm;->aq:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 440
    iget-object v0, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lecm;->aj:Lcqx;

    iget-object v2, p0, Lecm;->aq:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcqx;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    .line 441
    iput-object v4, p0, Lecm;->aq:Landroid/net/Uri;

    goto :goto_2
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lecm;->aw:Lcve;

    invoke-virtual {v0}, Lcve;->a()V

    .line 526
    invoke-super {p0}, Legi;->A()V

    .line 527
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 304
    sget-object v0, Lhmw;->az:Lhmw;

    return-object v0
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 4

    .prologue
    .line 309
    invoke-super {p0}, Legi;->L_()V

    .line 311
    iget-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 322
    :goto_0
    return-void

    .line 314
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lecm;->Z:Z

    .line 316
    iget-object v0, p0, Lecm;->at:Llnl;

    iget-object v1, p0, Lecm;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lecm;->P:Lhee;

    .line 317
    invoke-interface {v2}, Lhee;->g()Lhej;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 316
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    .line 319
    invoke-virtual {p0}, Lecm;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lecm;->d(Landroid/view/View;)V

    .line 320
    iget-object v0, p0, Lecm;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lecm;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 321
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 320
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lecm;->aj:Lcqx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecm;->aj:Lcqx;

    invoke-virtual {v0}, Lcqx;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lecm;->ac:Z

    if-eqz v0, :cond_0

    .line 327
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    .line 329
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    goto :goto_0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Landroid/widget/AbsListView;)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 470
    invoke-super {p0}, Legi;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecm;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 153
    iget-object v0, p0, Lecm;->at:Llnl;

    .line 154
    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400d6

    .line 153
    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 156
    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v2

    .line 158
    const v0, 0x7f100305

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/FastScrollContainer;

    iput-object v0, p0, Lecm;->ai:Lcom/google/android/apps/plus/views/FastScrollContainer;

    .line 159
    iget-object v0, p0, Lecm;->ai:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a(Lfyl;)V

    .line 161
    new-instance v0, Lcqx;

    invoke-direct {v0}, Lcqx;-><init>()V

    iput-object v0, p0, Lecm;->aj:Lcqx;

    .line 163
    const v0, 0x7f100306

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/FastScrollListView;

    iput-object v0, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 164
    iget-object v0, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v3, p0, Lecm;->aj:Lcqx;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/FastScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 166
    new-instance v0, Lend;

    const/4 v3, 0x2

    iget-object v4, p0, Lecm;->aj:Lcqx;

    invoke-direct {v0, v3, v4, p0}, Lend;-><init>(ILenf;Lene;)V

    iput-object v0, p0, Lecm;->ar:Lend;

    .line 168
    invoke-virtual {p0}, Lecm;->as()Lepx;

    move-result-object v0

    .line 170
    iget-object v3, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v4, p0, Lecm;->ai:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0, v3, p0, v4}, Lepx;->a(Landroid/widget/AbsListView;Landroid/widget/AbsListView$OnScrollListener;Lcom/google/android/apps/plus/views/FastScrollContainer;)V

    .line 174
    if-nez p3, :cond_0

    .line 175
    invoke-virtual {p0}, Lecm;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v5}, Lbb;->a(I)V

    .line 178
    :cond_0
    invoke-virtual {p0}, Lecm;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v5, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 181
    const-string v0, "scroll_to_uri"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lecm;->aq:Landroid/net/Uri;

    .line 183
    if-eqz p3, :cond_1

    .line 184
    const-string v0, "first_refresh_finished"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lecm;->al:Z

    .line 185
    const-string v0, "accessibility_enabled"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lecm;->ao:Z

    .line 187
    const-string v0, "scroll_pos"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    const-string v0, "scroll_pos"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lecm;->as:Ljava/lang/Float;

    .line 189
    iput-object v6, p0, Lecm;->aq:Landroid/net/Uri;

    .line 193
    :cond_1
    invoke-direct {p0, v1}, Lecm;->d(Landroid/view/View;)V

    .line 195
    iget-object v0, p0, Lecm;->ai:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a()V

    .line 196
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399
    new-instance v0, Leva;

    iget-object v1, p0, Lecm;->at:Llnl;

    iget-object v2, p0, Lecm;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {p0}, Lecm;->ac()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Leva;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 485
    iget-object v0, p0, Lecm;->ax:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 497
    :goto_0
    return-object v0

    .line 490
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 491
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 492
    iget-object v3, p0, Lecm;->ap:Lizs;

    iget-object v4, p0, Lecm;->ax:Landroid/graphics/Rect;

    .line 493
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v5, p0, Lecm;->ax:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    const/16 v6, 0x40

    .line 492
    invoke-virtual {v3, v0, v4, v5, v6}, Lizs;->a(Lizu;III)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    .line 495
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 497
    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 404
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lecm;->am:Ljava/lang/Integer;

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lecm;->ak:Z

    if-nez v1, :cond_1

    .line 405
    iput-boolean v8, p0, Lecm;->an:Z

    .line 406
    new-instance v1, Lfue;

    iget-object v2, p0, Lecm;->at:Llnl;

    iget-object v3, p0, Lecm;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {p0}, Lecm;->ac()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, -0x1

    move-object v4, p0

    invoke-direct/range {v1 .. v7}, Lfue;-><init>(Landroid/content/Context;ILfuf;Ljava/lang/String;J)V

    new-array v2, v0, [Ljava/lang/Void;

    .line 407
    invoke-virtual {v1, v2}, Lfue;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 409
    :cond_1
    iput-boolean v8, p0, Lecm;->ak:Z

    .line 410
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    move v0, v8

    .line 411
    :cond_3
    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lecm;->al:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lecm;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 412
    invoke-virtual {p0}, Lecm;->L_()V

    .line 414
    :cond_4
    iget-object v0, p0, Lecm;->aw:Lcve;

    invoke-virtual {v0, p1}, Lcve;->a(Ljava/lang/Object;)V

    .line 415
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 125
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 126
    if-eqz p1, :cond_0

    .line 127
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    .line 131
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "collectionlist-fragment"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 133
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 134
    new-instance v1, Lcve;

    .line 135
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Lcrf;

    iget-object v3, p0, Lecm;->at:Llnl;

    invoke-direct {p0}, Lecm;->ac()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lecm;->ax:Landroid/graphics/Rect;

    invoke-direct {v2, v3, v4, v5}, Lcrf;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Rect;)V

    invoke-direct {v1, v0, v2, p0}, Lcve;-><init>(Landroid/os/Looper;Lcvg;Lcvd;)V

    iput-object v1, p0, Lecm;->aw:Lcve;

    .line 136
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 419
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 66
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lecm;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 66
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lecm;->c(Ljava/util/List;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 339
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0x40000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo_picker_crop_mode"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lecm;->at:Llnl;

    iget-object v2, p0, Lecm;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    if-eqz p1, :cond_1

    :goto_1
    invoke-static {v0, v2, p1}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecm;->Y:Lctq;

    invoke-virtual {v2}, Lctq;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v2, p0, Lecm;->aa:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->b(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecm;->X:Lctz;

    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->a(Ljcn;)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecm;->ab:Z

    invoke-virtual {v0, v2}, Ljuj;->a(Z)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecm;->ac:Z

    invoke-virtual {v0, v2}, Ljuj;->b(Z)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecm;->ae:Z

    invoke-virtual {v0, v2}, Ljuj;->c(Z)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecm;->af:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljuj;->c(Ljava/lang/String;)Ljuj;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->c(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "show_autobackup_status"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljuj;->f(Z)Ljuj;

    move-result-object v0

    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lecm;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lecm;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->eb:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    invoke-virtual {p0, v1}, Lecm;->b(Landroid/content/Intent;)V

    .line 340
    return-void

    :cond_0
    move v0, v1

    .line 339
    goto/16 :goto_0

    :cond_1
    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lecm;->n()Lz;

    move-result-object v0

    iget-object v2, p0, Lecm;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-static {v0, v2}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecm;->Y:Lctq;

    invoke-virtual {v2}, Lctq;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v2, p0, Lecm;->aa:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->b(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecm;->X:Lctz;

    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->a(Ljcn;)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecm;->ab:Z

    invoke-virtual {v0, v2}, Ljuj;->a(Z)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecm;->ac:Z

    invoke-virtual {v0, v2}, Ljuj;->b(Z)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecm;->ae:Z

    invoke-virtual {v0, v2}, Ljuj;->c(Z)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecm;->af:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljuj;->c(Ljava/lang/String;)Ljuj;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->c(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v1, p0, Lecm;->aa:I

    invoke-virtual {v0, v1}, Ljuj;->e(I)Ljuj;

    move-result-object v0

    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "button_title_res_id"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->e(Ljava/lang/String;)Ljuj;

    move-result-object v0

    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "max_selection_count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "max_selection_count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljuj;->d(I)Ljuj;

    :cond_3
    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "min_selection_count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lecm;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "min_selection_count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljuj;->c(I)Ljuj;

    :cond_4
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 289
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0}, Legi;->aO_()V

    .line 227
    iget-object v0, p0, Lecm;->az:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 229
    iget-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    invoke-virtual {p0}, Lecm;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lecm;->ay:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 240
    :cond_0
    :goto_0
    iget-object v0, p0, Lecm;->at:Llnl;

    .line 241
    invoke-static {v0}, Llhn;->a(Landroid/content/Context;)Z

    move-result v0

    .line 243
    iget-boolean v1, p0, Lecm;->ao:Z

    if-eq v0, v1, :cond_1

    .line 244
    iput-boolean v0, p0, Lecm;->ao:Z

    .line 245
    iget-object v0, p0, Lecm;->aj:Lcqx;

    invoke-virtual {v0}, Lcqx;->notifyDataSetChanged()V

    .line 248
    :cond_1
    iget-object v0, p0, Lecm;->ai:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a()V

    .line 249
    return-void

    .line 235
    :cond_2
    iget-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lecm;->am:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lecm;->a(ILfib;)V

    goto :goto_0
.end method

.method protected b(Lhjk;)V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 278
    iget-object v0, p0, Lecm;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lecm;->a(Lhjk;I)V

    .line 281
    const v0, 0x7f10067b

    .line 282
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 283
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 285
    :cond_0
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 502
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/MediaResource;

    .line 503
    iget-object v2, p0, Lecm;->ap:Lizs;

    invoke-virtual {v2, v0}, Lizs;->a(Lcom/google/android/libraries/social/media/MediaResource;)V

    goto :goto_0

    .line 505
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0, p1}, Legi;->c(Landroid/os/Bundle;)V

    .line 141
    iget-object v0, p0, Lecm;->au:Llnh;

    const-class v1, Lctl;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 142
    iget-object v0, p0, Lecm;->au:Llnh;

    const-class v1, Lizs;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iput-object v0, p0, Lecm;->ap:Lizs;

    .line 143
    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lctm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 518
    iget-object v0, p0, Lecm;->aj:Lcqx;

    invoke-virtual {v0, p1}, Lcqx;->a(Ljava/util/List;)V

    .line 520
    invoke-virtual {p0}, Lecm;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lecm;->d(Landroid/view/View;)V

    .line 521
    return-void
.end method

.method protected c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 259
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 260
    iget-object v0, p0, Lecm;->am:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 261
    const-string v0, "refresh_request"

    iget-object v1, p0, Lecm;->am:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 264
    :cond_0
    iget-object v0, p0, Lecm;->aj:Lcqx;

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lecm;->aj:Lcqx;

    invoke-virtual {v0}, Lcqx;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 266
    const-string v0, "scroll_pos"

    iget-object v1, p0, Lecm;->aj:Lcqx;

    iget-object v2, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 267
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/FastScrollListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcqx;->a(I)F

    move-result v1

    .line 266
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 271
    :cond_1
    const-string v0, "first_refresh_finished"

    iget-boolean v1, p0, Lecm;->al:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 272
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0}, Legi;->g()V

    .line 220
    iget-object v0, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 221
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 253
    invoke-super {p0}, Legi;->h()V

    .line 254
    iget-object v0, p0, Lecm;->ah:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 255
    return-void
.end method

.method public h_(I)V
    .locals 0

    .prologue
    .line 481
    return-void
.end method

.method public l_(Z)V
    .locals 1

    .prologue
    .line 509
    const/4 v0, 0x0

    iput-boolean v0, p0, Lecm;->an:Z

    .line 510
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lecm;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 511
    invoke-virtual {p0}, Lecm;->L_()V

    .line 513
    :cond_0
    invoke-virtual {p0}, Lecm;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lecm;->d(Landroid/view/View;)V

    .line 514
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lecm;->ar:Lend;

    invoke-virtual {v0, p1, p2, p3, p4}, Lend;->onScroll(Landroid/widget/AbsListView;III)V

    .line 335
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Legi;->z()V

    .line 210
    iget-object v0, p0, Lecm;->ar:Lend;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lecm;->ar:Lend;

    invoke-virtual {v0}, Lend;->a()V

    .line 213
    :cond_0
    iget-object v0, p0, Lecm;->ai:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->b()V

    .line 214
    iget-object v0, p0, Lecm;->az:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 215
    return-void
.end method
