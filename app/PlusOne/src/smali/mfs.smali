.class public final Lmfs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmya;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3157
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3160
    iput-object v0, p0, Lmfs;->apiHeader:Llyq;

    .line 3163
    iput-object v0, p0, Lmfs;->a:Lmya;

    .line 3157
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3180
    const/4 v0, 0x0

    .line 3181
    iget-object v1, p0, Lmfs;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 3182
    const/4 v0, 0x1

    iget-object v1, p0, Lmfs;->apiHeader:Llyq;

    .line 3183
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3185
    :cond_0
    iget-object v1, p0, Lmfs;->a:Lmya;

    if-eqz v1, :cond_1

    .line 3186
    const/4 v1, 0x2

    iget-object v2, p0, Lmfs;->a:Lmya;

    .line 3187
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3189
    :cond_1
    iget-object v1, p0, Lmfs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3190
    iput v0, p0, Lmfs;->ai:I

    .line 3191
    return v0
.end method

.method public a(Loxn;)Lmfs;
    .locals 2

    .prologue
    .line 3199
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3200
    sparse-switch v0, :sswitch_data_0

    .line 3204
    iget-object v1, p0, Lmfs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3205
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfs;->ah:Ljava/util/List;

    .line 3208
    :cond_1
    iget-object v1, p0, Lmfs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3210
    :sswitch_0
    return-object p0

    .line 3215
    :sswitch_1
    iget-object v0, p0, Lmfs;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 3216
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfs;->apiHeader:Llyq;

    .line 3218
    :cond_2
    iget-object v0, p0, Lmfs;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3222
    :sswitch_2
    iget-object v0, p0, Lmfs;->a:Lmya;

    if-nez v0, :cond_3

    .line 3223
    new-instance v0, Lmya;

    invoke-direct {v0}, Lmya;-><init>()V

    iput-object v0, p0, Lmfs;->a:Lmya;

    .line 3225
    :cond_3
    iget-object v0, p0, Lmfs;->a:Lmya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3200
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3168
    iget-object v0, p0, Lmfs;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 3169
    const/4 v0, 0x1

    iget-object v1, p0, Lmfs;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3171
    :cond_0
    iget-object v0, p0, Lmfs;->a:Lmya;

    if-eqz v0, :cond_1

    .line 3172
    const/4 v0, 0x2

    iget-object v1, p0, Lmfs;->a:Lmya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3174
    :cond_1
    iget-object v0, p0, Lmfs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3176
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3153
    invoke-virtual {p0, p1}, Lmfs;->a(Loxn;)Lmfs;

    move-result-object v0

    return-object v0
.end method
