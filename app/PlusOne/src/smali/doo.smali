.class public final Ldoo;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ldiv;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILdiv;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    const-string v0, "CreateShareByLinkTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 27
    iput p2, p0, Ldoo;->a:I

    .line 28
    iput-object p3, p0, Ldoo;->b:Ldiv;

    .line 29
    iput-object p4, p0, Ldoo;->c:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    .line 39
    invoke-virtual {p0}, Ldoo;->f()Landroid/content/Context;

    move-result-object v1

    .line 40
    const-class v0, Lkfd;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    .line 42
    new-instance v2, Ldiu;

    iget v3, p0, Ldoo;->a:I

    iget-object v4, p0, Ldoo;->b:Ldiv;

    invoke-direct {v2, v1, v3, v4}, Ldiu;-><init>(Landroid/content/Context;ILdiv;)V

    .line 43
    invoke-interface {v0, v2}, Lkfd;->a(Lkff;)V

    .line 45
    invoke-virtual {v2}, Ldiu;->t()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 46
    :goto_0
    new-instance v1, Lhoz;

    invoke-direct {v1, v0}, Lhoz;-><init>(Z)V

    .line 47
    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "link"

    invoke-virtual {v2}, Ldiu;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_0
    return-object v1

    .line 45
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldoo;->c:Ljava/lang/String;

    return-object v0
.end method
