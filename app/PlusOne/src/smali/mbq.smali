.class public final Lmbq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnho;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29563
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29566
    iput-object v0, p0, Lmbq;->apiHeader:Llyq;

    .line 29569
    iput-object v0, p0, Lmbq;->a:Lnho;

    .line 29563
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29586
    const/4 v0, 0x0

    .line 29587
    iget-object v1, p0, Lmbq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 29588
    const/4 v0, 0x1

    iget-object v1, p0, Lmbq;->apiHeader:Llyq;

    .line 29589
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29591
    :cond_0
    iget-object v1, p0, Lmbq;->a:Lnho;

    if-eqz v1, :cond_1

    .line 29592
    const/4 v1, 0x2

    iget-object v2, p0, Lmbq;->a:Lnho;

    .line 29593
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29595
    :cond_1
    iget-object v1, p0, Lmbq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29596
    iput v0, p0, Lmbq;->ai:I

    .line 29597
    return v0
.end method

.method public a(Loxn;)Lmbq;
    .locals 2

    .prologue
    .line 29605
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29606
    sparse-switch v0, :sswitch_data_0

    .line 29610
    iget-object v1, p0, Lmbq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29611
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbq;->ah:Ljava/util/List;

    .line 29614
    :cond_1
    iget-object v1, p0, Lmbq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29616
    :sswitch_0
    return-object p0

    .line 29621
    :sswitch_1
    iget-object v0, p0, Lmbq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 29622
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmbq;->apiHeader:Llyq;

    .line 29624
    :cond_2
    iget-object v0, p0, Lmbq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29628
    :sswitch_2
    iget-object v0, p0, Lmbq;->a:Lnho;

    if-nez v0, :cond_3

    .line 29629
    new-instance v0, Lnho;

    invoke-direct {v0}, Lnho;-><init>()V

    iput-object v0, p0, Lmbq;->a:Lnho;

    .line 29631
    :cond_3
    iget-object v0, p0, Lmbq;->a:Lnho;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29606
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29574
    iget-object v0, p0, Lmbq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 29575
    const/4 v0, 0x1

    iget-object v1, p0, Lmbq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29577
    :cond_0
    iget-object v0, p0, Lmbq;->a:Lnho;

    if-eqz v0, :cond_1

    .line 29578
    const/4 v0, 0x2

    iget-object v1, p0, Lmbq;->a:Lnho;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29580
    :cond_1
    iget-object v0, p0, Lmbq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29582
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29559
    invoke-virtual {p0, p1}, Lmbq;->a(Loxn;)Lmbq;

    move-result-object v0

    return-object v0
.end method
