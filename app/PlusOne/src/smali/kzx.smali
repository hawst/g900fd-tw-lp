.class public final Lkzx;
.super Lllq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:J

.field private d:Lkzv;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lllq;-><init>()V

    .line 23
    return-void
.end method

.method public constructor <init>(Lpca;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Lllq;-><init>()V

    .line 26
    iget-object v0, p1, Lpca;->c:Ljava/lang/String;

    iput-object v0, p0, Lkzx;->a:Ljava/lang/String;

    .line 27
    iget-object v0, p1, Lpca;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzx;->b:Ljava/lang/String;

    .line 28
    iget-object v0, p1, Lpca;->d:Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lkzx;->c:J

    .line 29
    iget-object v0, p1, Lpca;->e:Loya;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p1, Lpca;->e:Loya;

    sget-object v1, Lpbu;->a:Loxr;

    .line 31
    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpbu;

    .line 32
    new-instance v1, Lkzv;

    invoke-direct {v1, v0}, Lkzv;-><init>(Lpbu;)V

    iput-object v1, p0, Lkzx;->d:Lkzv;

    .line 33
    iget-object v1, p0, Lkzx;->d:Lkzv;

    iget-object v0, v0, Lpbu;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lkzv;->b(Ljava/lang/String;)V

    .line 35
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lkzx;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 108
    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    :cond_0
    iput-wide p1, p0, Lkzx;->c:J

    .line 109
    return-void
.end method

.method public a(Ljava/io/DataOutputStream;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lkzx;->d:Lkzv;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 53
    iget-object v0, p0, Lkzx;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzx;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lkzx;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzx;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 55
    iget-wide v0, p0, Lkzx;->c:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 56
    iget-object v0, p0, Lkzx;->d:Lkzv;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lkzx;->d:Lkzv;

    invoke-virtual {v0, p1}, Lkzv;->a(Ljava/io/DataOutputStream;)V

    .line 59
    :cond_0
    return-void

    .line 52
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 81
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    if-ne v1, v0, :cond_1

    .line 82
    :goto_0
    invoke-static {p1}, Lkzx;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkzx;->a:Ljava/lang/String;

    .line 83
    invoke-static {p1}, Lkzx;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkzx;->b:Ljava/lang/String;

    .line 84
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    iput-wide v2, p0, Lkzx;->c:J

    .line 85
    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Lkzv;

    invoke-direct {v0}, Lkzv;-><init>()V

    iput-object v0, p0, Lkzx;->d:Lkzv;

    .line 87
    iget-object v0, p0, Lkzx;->d:Lkzv;

    invoke-virtual {v0, p1}, Lkzv;->a(Ljava/nio/ByteBuffer;)V

    .line 89
    :cond_0
    return-void

    .line 81
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lkzx;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 100
    iget-wide v0, p0, Lkzx;->c:J

    return-wide v0
.end method

.method public d()Lkzv;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lkzx;->d:Lkzv;

    return-object v0
.end method
