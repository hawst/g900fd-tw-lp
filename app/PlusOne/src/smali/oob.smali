.class public final Loob;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loob;


# instance fields
.field public b:Loog;

.field private c:[Looc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x0

    new-array v0, v0, [Loob;

    sput-object v0, Loob;->a:[Loob;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 414
    invoke-direct {p0}, Loxq;-><init>()V

    .line 517
    const/4 v0, 0x0

    iput-object v0, p0, Loob;->b:Loog;

    .line 520
    sget-object v0, Looc;->a:[Looc;

    iput-object v0, p0, Loob;->c:[Looc;

    .line 414
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 541
    .line 542
    iget-object v0, p0, Loob;->b:Loog;

    if-eqz v0, :cond_2

    .line 543
    const/4 v0, 0x1

    iget-object v2, p0, Loob;->b:Loog;

    .line 544
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 546
    :goto_0
    iget-object v2, p0, Loob;->c:[Looc;

    if-eqz v2, :cond_1

    .line 547
    iget-object v2, p0, Loob;->c:[Looc;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 548
    if-eqz v4, :cond_0

    .line 549
    const/4 v5, 0x2

    .line 550
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 547
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 554
    :cond_1
    iget-object v1, p0, Loob;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 555
    iput v0, p0, Loob;->ai:I

    .line 556
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loob;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 564
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 565
    sparse-switch v0, :sswitch_data_0

    .line 569
    iget-object v2, p0, Loob;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 570
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loob;->ah:Ljava/util/List;

    .line 573
    :cond_1
    iget-object v2, p0, Loob;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 575
    :sswitch_0
    return-object p0

    .line 580
    :sswitch_1
    iget-object v0, p0, Loob;->b:Loog;

    if-nez v0, :cond_2

    .line 581
    new-instance v0, Loog;

    invoke-direct {v0}, Loog;-><init>()V

    iput-object v0, p0, Loob;->b:Loog;

    .line 583
    :cond_2
    iget-object v0, p0, Loob;->b:Loog;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 587
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 588
    iget-object v0, p0, Loob;->c:[Looc;

    if-nez v0, :cond_4

    move v0, v1

    .line 589
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Looc;

    .line 590
    iget-object v3, p0, Loob;->c:[Looc;

    if-eqz v3, :cond_3

    .line 591
    iget-object v3, p0, Loob;->c:[Looc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 593
    :cond_3
    iput-object v2, p0, Loob;->c:[Looc;

    .line 594
    :goto_2
    iget-object v2, p0, Loob;->c:[Looc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 595
    iget-object v2, p0, Loob;->c:[Looc;

    new-instance v3, Looc;

    invoke-direct {v3}, Looc;-><init>()V

    aput-object v3, v2, v0

    .line 596
    iget-object v2, p0, Loob;->c:[Looc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 597
    invoke-virtual {p1}, Loxn;->a()I

    .line 594
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 588
    :cond_4
    iget-object v0, p0, Loob;->c:[Looc;

    array-length v0, v0

    goto :goto_1

    .line 600
    :cond_5
    iget-object v2, p0, Loob;->c:[Looc;

    new-instance v3, Looc;

    invoke-direct {v3}, Looc;-><init>()V

    aput-object v3, v2, v0

    .line 601
    iget-object v2, p0, Loob;->c:[Looc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 565
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 525
    iget-object v0, p0, Loob;->b:Loog;

    if-eqz v0, :cond_0

    .line 526
    const/4 v0, 0x1

    iget-object v1, p0, Loob;->b:Loog;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 528
    :cond_0
    iget-object v0, p0, Loob;->c:[Looc;

    if-eqz v0, :cond_2

    .line 529
    iget-object v1, p0, Loob;->c:[Looc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 530
    if-eqz v3, :cond_1

    .line 531
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 529
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 535
    :cond_2
    iget-object v0, p0, Loob;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 537
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 410
    invoke-virtual {p0, p1}, Loob;->a(Loxn;)Loob;

    move-result-object v0

    return-object v0
.end method
