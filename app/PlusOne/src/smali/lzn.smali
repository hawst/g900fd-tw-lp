.class public final Llzn;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnwa;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7126
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7129
    iput-object v0, p0, Llzn;->apiHeader:Llyr;

    .line 7132
    iput-object v0, p0, Llzn;->a:Lnwa;

    .line 7126
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7149
    const/4 v0, 0x0

    .line 7150
    iget-object v1, p0, Llzn;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 7151
    const/4 v0, 0x1

    iget-object v1, p0, Llzn;->apiHeader:Llyr;

    .line 7152
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7154
    :cond_0
    iget-object v1, p0, Llzn;->a:Lnwa;

    if-eqz v1, :cond_1

    .line 7155
    const/4 v1, 0x2

    iget-object v2, p0, Llzn;->a:Lnwa;

    .line 7156
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7158
    :cond_1
    iget-object v1, p0, Llzn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7159
    iput v0, p0, Llzn;->ai:I

    .line 7160
    return v0
.end method

.method public a(Loxn;)Llzn;
    .locals 2

    .prologue
    .line 7168
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7169
    sparse-switch v0, :sswitch_data_0

    .line 7173
    iget-object v1, p0, Llzn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7174
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzn;->ah:Ljava/util/List;

    .line 7177
    :cond_1
    iget-object v1, p0, Llzn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7179
    :sswitch_0
    return-object p0

    .line 7184
    :sswitch_1
    iget-object v0, p0, Llzn;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 7185
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzn;->apiHeader:Llyr;

    .line 7187
    :cond_2
    iget-object v0, p0, Llzn;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7191
    :sswitch_2
    iget-object v0, p0, Llzn;->a:Lnwa;

    if-nez v0, :cond_3

    .line 7192
    new-instance v0, Lnwa;

    invoke-direct {v0}, Lnwa;-><init>()V

    iput-object v0, p0, Llzn;->a:Lnwa;

    .line 7194
    :cond_3
    iget-object v0, p0, Llzn;->a:Lnwa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7169
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7137
    iget-object v0, p0, Llzn;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 7138
    const/4 v0, 0x1

    iget-object v1, p0, Llzn;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7140
    :cond_0
    iget-object v0, p0, Llzn;->a:Lnwa;

    if-eqz v0, :cond_1

    .line 7141
    const/4 v0, 0x2

    iget-object v1, p0, Llzn;->a:Lnwa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7143
    :cond_1
    iget-object v0, p0, Llzn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7145
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7122
    invoke-virtual {p0, p1}, Llzn;->a(Loxn;)Llzn;

    move-result-object v0

    return-object v0
.end method
