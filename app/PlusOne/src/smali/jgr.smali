.class public final Ljgr;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 22
    const-string v0, "connectivity"

    .line 23
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 24
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 25
    if-nez v0, :cond_0

    move v0, v1

    .line 71
    :goto_0
    return v0

    .line 28
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 71
    goto :goto_0

    .line 30
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 32
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 34
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 36
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 38
    :pswitch_5
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_6
    move v0, v1

    .line 68
    goto :goto_0

    .line 40
    :pswitch_7
    const/4 v0, 0x5

    goto :goto_0

    .line 42
    :pswitch_8
    const/4 v0, 0x6

    goto :goto_0

    .line 44
    :pswitch_9
    const/4 v0, 0x7

    goto :goto_0

    .line 46
    :pswitch_a
    const/16 v0, 0x8

    goto :goto_0

    .line 48
    :pswitch_b
    const/16 v0, 0xa

    goto :goto_0

    .line 50
    :pswitch_c
    const/16 v0, 0xb

    goto :goto_0

    .line 52
    :pswitch_d
    const/16 v0, 0xc

    goto :goto_0

    .line 54
    :pswitch_e
    const/16 v0, 0xd

    goto :goto_0

    .line 56
    :pswitch_f
    const/16 v0, 0xe

    goto :goto_0

    .line 58
    :pswitch_10
    const/16 v0, 0xf

    goto :goto_0

    .line 60
    :pswitch_11
    const/16 v0, 0x10

    goto :goto_0

    .line 62
    :pswitch_12
    const/16 v0, 0x11

    goto :goto_0

    .line 64
    :pswitch_13
    const/16 v0, 0x12

    goto :goto_0

    .line 66
    :pswitch_14
    const/16 v0, 0x13

    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 38
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_d
        :pswitch_9
        :pswitch_14
        :pswitch_8
        :pswitch_6
        :pswitch_b
        :pswitch_7
        :pswitch_e
        :pswitch_11
        :pswitch_f
        :pswitch_12
        :pswitch_c
        :pswitch_13
        :pswitch_a
        :pswitch_10
    .end packed-switch
.end method
