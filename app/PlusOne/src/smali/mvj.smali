.class public final Lmvj;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10332
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10335
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lmvj;->a:[Ljava/lang/Integer;

    .line 10332
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 10356
    .line 10357
    iget-object v1, p0, Lmvj;->a:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmvj;->a:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 10359
    iget-object v2, p0, Lmvj;->a:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 10361
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 10359
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10364
    :cond_0
    iget-object v0, p0, Lmvj;->a:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 10366
    :cond_1
    iget-object v1, p0, Lmvj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 10367
    const/4 v1, 0x2

    iget-object v2, p0, Lmvj;->b:Ljava/lang/Integer;

    .line 10368
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10370
    :cond_2
    iget-object v1, p0, Lmvj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10371
    iput v0, p0, Lmvj;->ai:I

    .line 10372
    return v0
.end method

.method public a(Loxn;)Lmvj;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 10380
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10381
    sparse-switch v0, :sswitch_data_0

    .line 10385
    iget-object v1, p0, Lmvj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10386
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvj;->ah:Ljava/util/List;

    .line 10389
    :cond_1
    iget-object v1, p0, Lmvj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10391
    :sswitch_0
    return-object p0

    .line 10396
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 10397
    iget-object v0, p0, Lmvj;->a:[Ljava/lang/Integer;

    array-length v0, v0

    .line 10398
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 10399
    iget-object v2, p0, Lmvj;->a:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 10400
    iput-object v1, p0, Lmvj;->a:[Ljava/lang/Integer;

    .line 10401
    :goto_1
    iget-object v1, p0, Lmvj;->a:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 10402
    iget-object v1, p0, Lmvj;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 10403
    invoke-virtual {p1}, Loxn;->a()I

    .line 10401
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 10406
    :cond_2
    iget-object v1, p0, Lmvj;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 10410
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvj;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 10381
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 10342
    iget-object v0, p0, Lmvj;->a:[Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 10343
    iget-object v1, p0, Lmvj;->a:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 10344
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 10343
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10347
    :cond_0
    iget-object v0, p0, Lmvj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 10348
    const/4 v0, 0x2

    iget-object v1, p0, Lmvj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10350
    :cond_1
    iget-object v0, p0, Lmvj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10352
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10328
    invoke-virtual {p0, p1}, Lmvj;->a(Loxn;)Lmvj;

    move-result-object v0

    return-object v0
.end method
