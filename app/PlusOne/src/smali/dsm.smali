.class public final Ldsm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:Lloz;

.field private static final f:[Ljava/lang/String;

.field private static final g:Ljava/lang/Object;

.field private static final h:Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final i:Ljava/lang/Object;

.field private static volatile j:Ljava/util/concurrent/CountDownLatch;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static k:I

.field private static l:[Ljava/lang/String;

.field private static m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 236
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "circle_id"

    aput-object v1, v0, v4

    const-string v1, "circle_name"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "contact_count"

    aput-object v1, v0, v6

    const-string v1, "semantic_hints"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "volume"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "for_sharing"

    aput-object v2, v0, v1

    sput-object v0, Ldsm;->a:[Ljava/lang/String;

    .line 254
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v4

    const-string v1, "last_updated_time"

    aput-object v1, v0, v3

    sput-object v0, Ldsm;->b:[Ljava/lang/String;

    .line 262
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "reviews_data_proto"

    aput-object v1, v0, v4

    sput-object v0, Ldsm;->c:[Ljava/lang/String;

    .line 268
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "profile_state"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "profile_type"

    aput-object v1, v0, v5

    sput-object v0, Ldsm;->d:[Ljava/lang/String;

    .line 304
    new-instance v0, Lloz;

    const-string v1, "enable_social_reviews_on_profiles"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldsm;->e:Lloz;

    .line 430
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "profile_state"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "group_concat(link_circle_id, \'|\') AS packed_circle_ids"

    aput-object v1, v0, v5

    const-string v1, "blocked"

    aput-object v1, v0, v6

    const-string v1, "last_updated_time"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "contact_proto"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "profile_update_time"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_proto"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "people_data_proto"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "videos_data_proto"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "reviews_data_proto"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "local_reviews_data_proto"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "self_local_reviews_data_proto"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "profile_stats_proto"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "profile_squares_proto"

    aput-object v2, v0, v1

    sput-object v0, Ldsm;->f:[Ljava/lang/String;

    .line 465
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldsm;->g:Ljava/lang/Object;

    .line 467
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldsm;->h:Ljava/lang/Object;

    .line 468
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldsm;->i:Ljava/lang/Object;

    .line 476
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Ldsm;->j:Ljava/util/concurrent/CountDownLatch;

    .line 478
    const v0, 0x15f90

    sput v0, Ldsm;->k:I

    .line 766
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "circle_id"

    aput-object v1, v0, v4

    const-string v1, "circle_name"

    aput-object v1, v0, v3

    const-string v1, "semantic_hints"

    aput-object v1, v0, v5

    const-string v1, "contact_count"

    aput-object v1, v0, v6

    sput-object v0, Ldsm;->l:[Ljava/lang/String;

    .line 3976
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ldsm;->m:Ljava/util/HashMap;

    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 2370
    packed-switch p0, :pswitch_data_0

    .line 2387
    :pswitch_0
    const/16 v0, 0x32

    .line 2389
    :goto_0
    return v0

    .line 2372
    :pswitch_1
    const/16 v0, 0x14

    .line 2373
    goto :goto_0

    .line 2375
    :pswitch_2
    const/16 v0, 0x1e

    .line 2376
    goto :goto_0

    .line 2378
    :pswitch_3
    const/16 v0, 0x28

    .line 2379
    goto :goto_0

    .line 2381
    :pswitch_4
    const/16 v0, 0x3c

    .line 2382
    goto :goto_0

    .line 2384
    :pswitch_5
    const/16 v0, 0x3e8

    .line 2385
    goto :goto_0

    .line 2370
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;ILojc;Lojb;)I
    .locals 20
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2118
    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    iget-object v1, v0, Lojc;->a:[Lohm;

    if-nez v1, :cond_1

    .line 2119
    :cond_0
    const/4 v1, 0x0

    .line 2302
    :goto_0
    return v1

    .line 2122
    :cond_1
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 2123
    move-object/from16 v0, p2

    iget-object v2, v0, Lojc;->a:[Lohm;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2124
    iget-object v5, v4, Lohm;->b:Lohn;

    iget-object v5, v5, Lohn;->b:Ljava/lang/String;

    invoke-static {v5}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2127
    :cond_2
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 2128
    move-object/from16 v0, p3

    iget-object v3, v0, Lojb;->a:[Loja;

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 2129
    iget-object v1, v5, Loja;->d:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "0"

    :goto_3
    invoke-virtual {v11, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2128
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2129
    :cond_3
    iget-object v1, v5, Loja;->d:Ljava/lang/String;

    goto :goto_3

    .line 2132
    :cond_4
    const/4 v9, 0x0

    .line 2134
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2135
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2137
    :try_start_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 2138
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 2139
    const-string v2, "circles"

    sget-object v3, Ldsm;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v14

    .line 2142
    const/4 v2, 0x0

    move v8, v2

    .line 2144
    :cond_5
    :goto_4
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2145
    const/4 v2, 0x2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 2148
    const/4 v2, -0x1

    if-ne v15, v2, :cond_6

    .line 2149
    const/4 v2, 0x1

    move v8, v2

    .line 2150
    goto :goto_4

    .line 2153
    :cond_6
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 2154
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lohm;

    .line 2155
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Loja;

    .line 2156
    if-nez v2, :cond_7

    if-nez v3, :cond_7

    .line 2157
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 2207
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2294
    :catchall_1
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 2159
    :cond_7
    const/4 v4, 0x1

    :try_start_3
    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 2160
    const/4 v4, 0x3

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 2161
    const/4 v4, 0x4

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 2169
    if-eqz v2, :cond_9

    .line 2170
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2172
    const/4 v6, 0x1

    .line 2173
    iget-object v3, v2, Lohm;->c:Lohw;

    iget-object v5, v3, Lohw;->a:Ljava/lang/String;

    .line 2174
    iget-object v3, v2, Lohm;->c:Lohw;

    iget-object v4, v3, Lohw;->c:Ljava/lang/String;

    .line 2175
    iget-object v2, v2, Lohm;->c:Lohw;

    iget-object v2, v2, Lohw;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2176
    const/4 v2, 0x0

    .line 2190
    :goto_5
    if-ne v6, v15, :cond_8

    .line 2191
    move-object/from16 v0, v17

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    move/from16 v0, v18

    if-ne v3, v0, :cond_8

    move/from16 v0, v19

    if-eq v2, v0, :cond_5

    .line 2194
    :cond_8
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 2195
    const-string v15, "circle_id"

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2196
    const-string v15, "circle_name"

    invoke-virtual {v7, v15, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197
    const-string v5, "sort_key"

    invoke-virtual {v7, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198
    const-string v4, "type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2199
    const-string v4, "contact_count"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2200
    const-string v3, "semantic_hints"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2201
    const-string v2, "show_order"

    invoke-static {v6}, Ldsm;->a(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2202
    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 2178
    :cond_9
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2180
    iget v2, v3, Loja;->b:I

    invoke-static {v2}, Lhxl;->a(I)I

    move-result v7

    .line 2181
    iget-object v6, v3, Loja;->e:Ljava/lang/String;

    .line 2182
    const/16 v2, 0x9

    if-ne v7, v2, :cond_a

    .line 2183
    const v2, 0x7f0a0822

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2185
    :cond_a
    if-nez v6, :cond_b

    const/4 v5, 0x0

    .line 2186
    :goto_6
    iget-object v2, v3, Loja;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    iget-object v2, v3, Loja;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 2187
    :goto_7
    invoke-static {v3}, Ldsm;->a(Loja;)I

    move-result v2

    move v3, v4

    move-object v4, v5

    move-object v5, v6

    move v6, v7

    goto/16 :goto_5

    .line 2185
    :cond_b
    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v5

    goto :goto_6

    .line 2186
    :cond_c
    const/4 v4, 0x0

    goto :goto_7

    .line 2207
    :cond_d
    :try_start_4
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 2211
    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1c

    .line 2212
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2213
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2214
    const-string v2, "circle_id IN ("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2215
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2216
    const-string v6, "?,"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2217
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 2219
    :cond_e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2220
    const-string v2, ")"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2221
    const-string v5, "circles"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {v1, v5, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2222
    const/4 v2, 0x1

    .line 2226
    :goto_9
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_10

    .line 2227
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 2228
    const-string v5, "circles"

    const-string v6, "circle_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v12, "circle_id"

    .line 2230
    invoke-virtual {v2, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v7, v9

    .line 2228
    invoke-virtual {v1, v5, v2, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2231
    add-int/lit8 v3, v3, 0x1

    .line 2232
    goto :goto_a

    :cond_f
    move v2, v3

    .line 2236
    :cond_10
    invoke-virtual {v10}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_14

    .line 2237
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2238
    invoke-virtual {v10}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lohm;

    .line 2239
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 2240
    const-string v4, "circle_id"

    iget-object v7, v2, Lohm;->b:Lohn;

    iget-object v7, v7, Lohn;->b:Ljava/lang/String;

    .line 2241
    invoke-static {v7}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2240
    invoke-virtual {v5, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2242
    const-string v4, "type"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2243
    const-string v4, "circle_name"

    iget-object v7, v2, Lohm;->c:Lohw;

    iget-object v7, v7, Lohw;->a:Ljava/lang/String;

    invoke-virtual {v5, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2244
    const-string v4, "sort_key"

    iget-object v7, v2, Lohm;->c:Lohw;

    iget-object v7, v7, Lohw;->c:Ljava/lang/String;

    invoke-virtual {v5, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2245
    const-string v7, "contact_count"

    iget-object v4, v2, Lohm;->c:Lohw;

    iget-object v4, v4, Lohw;->b:Ljava/lang/Integer;

    if-eqz v4, :cond_11

    iget-object v4, v2, Lohm;->c:Lohw;

    iget-object v4, v4, Lohw;->b:Ljava/lang/Integer;

    .line 2247
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 2246
    :goto_c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2245
    invoke-virtual {v5, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2248
    const-string v4, "for_sharing"

    iget-object v7, v2, Lohm;->c:Lohw;

    iget-object v7, v7, Lohw;->d:Ljava/lang/Boolean;

    if-eqz v7, :cond_12

    iget-object v2, v2, Lohm;->c:Lohw;

    iget-object v2, v2, Lohw;->d:Ljava/lang/Boolean;

    .line 2250
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    .line 2249
    :goto_d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2248
    invoke-virtual {v5, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2251
    const-string v2, "semantic_hints"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2252
    const-string v2, "show_order"

    const/4 v4, 0x1

    .line 2253
    invoke-static {v4}, Ldsm;->a(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2252
    invoke-virtual {v5, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2255
    const-string v2, "circles"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2256
    add-int/lit8 v3, v3, 0x1

    .line 2257
    goto/16 :goto_b

    .line 2247
    :cond_11
    const/4 v4, 0x0

    goto :goto_c

    .line 2250
    :cond_12
    const/4 v2, 0x0

    goto :goto_d

    :cond_13
    move v2, v3

    .line 2260
    :cond_14
    invoke-virtual {v11}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_19

    .line 2261
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2262
    invoke-virtual {v11}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :cond_15
    :goto_e
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Loja;

    .line 2263
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 2264
    const-string v7, "circle_id"

    iget-object v4, v2, Loja;->d:Ljava/lang/String;

    if-nez v4, :cond_16

    const-string v4, "0"

    :goto_f
    invoke-virtual {v5, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2265
    iget v4, v2, Loja;->b:I

    invoke-static {v4}, Lhxl;->a(I)I

    move-result v4

    .line 2268
    const/16 v7, 0x64

    if-eq v4, v7, :cond_15

    .line 2269
    const-string v7, "type"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2273
    const/16 v7, 0x9

    if-ne v4, v7, :cond_17

    .line 2274
    const-string v7, "circle_name"

    const v9, 0x7f0a0822

    .line 2275
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2274
    invoke-virtual {v5, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2280
    :goto_10
    const-string v7, "semantic_hints"

    invoke-static {v2}, Ldsm;->a(Loja;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2281
    const-string v2, "show_order"

    invoke-static {v4}, Ldsm;->a(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2282
    const-string v2, "circles"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2283
    add-int/lit8 v3, v3, 0x1

    .line 2284
    goto :goto_e

    .line 2264
    :cond_16
    iget-object v4, v2, Loja;->d:Ljava/lang/String;

    goto :goto_f

    .line 2277
    :cond_17
    const-string v7, "circle_name"

    iget-object v9, v2, Loja;->e:Ljava/lang/String;

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    :cond_18
    move v2, v3

    .line 2288
    :cond_19
    if-nez v8, :cond_1a

    .line 2289
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2292
    :cond_1a
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2294
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2297
    if-eqz v2, :cond_1b

    .line 2298
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2299
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    :cond_1b
    move v1, v2

    .line 2302
    goto/16 :goto_0

    :cond_1c
    move v2, v9

    goto/16 :goto_9
.end method

.method private static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;II)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lohv;",
            ">;II)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 2669
    .line 2670
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2672
    :try_start_0
    invoke-static {p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2673
    :goto_0
    if-ge p3, p4, :cond_3

    .line 2674
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lohv;

    .line 2675
    iget-object v1, v0, Lohv;->b:Lohp;

    invoke-static {v1}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "in_same_visibility_group"

    iget-object v1, v0, Lohv;->c:Lohq;

    iget-object v1, v1, Lohq;->f:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "verified"

    iget-object v1, v0, Lohv;->c:Lohq;

    iget-object v1, v1, Lohq;->i:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "contacts"

    const-string v5, "person_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {p1, v1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    const-string v1, "contact_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "contact_proto"

    invoke-static {v0}, Ldsm;->a(Lohv;)Ldsv;

    move-result-object v0

    invoke-static {v0}, Ldsm;->a(Ldsv;)[B

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v0, "profiles"

    const-string v1, "profile_person_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {p1, v0, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "profile_person_id"

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "profiles"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2673
    :cond_0
    add-int/lit8 p3, p3, 0x1

    goto/16 :goto_0

    :cond_1
    move v1, v8

    .line 2675
    goto :goto_1

    :cond_2
    move v1, v8

    goto :goto_2

    :cond_3
    move v2, v8

    .line 2720
    :cond_4
    if-lez v2, :cond_5

    .line 2721
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2724
    :cond_5
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2726
    return v2

    .line 2678
    :cond_6
    :try_start_1
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 2682
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2683
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2684
    const-string v0, "person_id IN ("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, p3

    .line 2685
    :goto_3
    if-ge v1, p4, :cond_7

    .line 2686
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lohv;

    .line 2687
    const-string v2, "?,"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2688
    iget-object v0, v0, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2685
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2690
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2691
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2693
    const-string v1, "contacts"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "person_id"

    aput-object v5, v2, v0

    const/4 v0, 0x1

    const-string v5, "last_updated_time"

    aput-object v5, v2, v0

    .line 2696
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    .line 2693
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 2698
    :goto_4
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2699
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2700
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2701
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 2704
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2724
    :catchall_1
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 2704
    :cond_8
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v2, v8

    .line 2708
    :goto_5
    if-ge p3, p4, :cond_4

    .line 2709
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lohv;

    .line 2710
    invoke-static {v0}, Ldsm;->c(Lohv;)J

    move-result-wide v4

    .line 2711
    iget-object v1, v0, Lohv;->b:Lohp;

    invoke-static {v1}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v3

    .line 2712
    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 2713
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v6, v4

    if-gez v1, :cond_a

    .line 2714
    :cond_9
    add-int/lit8 v1, v2, 0x1

    .line 2715
    invoke-static {p1, v3, v0}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lohv;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move v0, v1

    .line 2708
    :goto_6
    add-int/lit8 p3, p3, 0x1

    move v2, v0

    goto :goto_5

    :cond_a
    move v0, v2

    goto :goto_6
.end method

.method private static a(Loja;)I
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 4843
    .line 4844
    iget-object v2, p0, Loja;->c:Lohx;

    .line 4845
    if-eqz v2, :cond_1

    iget-object v1, v2, Lohx;->a:[I

    if-eqz v1, :cond_1

    move v1, v0

    .line 4846
    :goto_0
    iget-object v3, v2, Lohx;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 4847
    iget-object v3, v2, Lohx;->a:[I

    aget v3, v3, v0

    .line 4848
    packed-switch v3, :pswitch_data_0

    .line 4860
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4850
    :pswitch_1
    or-int/lit8 v1, v1, 0x1

    .line 4851
    goto :goto_1

    .line 4853
    :pswitch_2
    or-int/lit8 v1, v1, 0x2

    .line 4854
    goto :goto_1

    .line 4856
    :pswitch_3
    or-int/lit8 v1, v1, 0x8

    .line 4857
    goto :goto_1

    .line 4859
    :pswitch_4
    or-int/lit8 v1, v1, 0x10

    goto :goto_1

    :cond_0
    move v0, v1

    .line 4865
    :cond_1
    return v0

    .line 4848
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 1007
    .line 1008
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1010
    :try_start_0
    const-string v1, "SELECT circle_fingerprint  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1014
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Liqc;)J
    .locals 12

    .prologue
    const/4 v2, 0x0

    const-wide/16 v10, 0x1f

    .line 167
    invoke-interface {p0}, Liqc;->b()I

    move-result v6

    const-wide/16 v0, 0x11

    move v3, v2

    move-wide v4, v0

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-interface {p0, v3}, Liqc;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liqb;

    mul-long/2addr v4, v10

    invoke-interface {v0}, Liqb;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    int-to-long v8, v1

    add-long/2addr v4, v8

    mul-long/2addr v4, v10

    invoke-interface {v0}, Liqb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    int-to-long v8, v1

    add-long/2addr v4, v8

    mul-long/2addr v4, v10

    invoke-interface {v0}, Liqb;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    int-to-long v8, v1

    add-long/2addr v4, v8

    mul-long/2addr v4, v10

    invoke-interface {v0}, Liqb;->f()J

    move-result-wide v0

    add-long/2addr v4, v0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    return-wide v4
.end method

.method static synthetic a(Liqi;)J
    .locals 10

    .prologue
    .line 167
    invoke-interface {p0}, Liqi;->b()I

    move-result v4

    int-to-long v2, v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-interface {p0, v1}, Liqi;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liqh;

    invoke-interface {v0}, Liqh;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v5

    int-to-long v6, v5

    invoke-interface {v0}, Liqh;->j()J

    move-result-wide v8

    add-long/2addr v6, v8

    add-long/2addr v2, v6

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-wide v2
.end method

.method public static a(Landroid/content/Context;II)Landroid/database/Cursor;
    .locals 12

    .prologue
    .line 802
    invoke-static {p0, p1}, Ldsm;->h(Landroid/content/Context;I)J

    move-result-wide v0

    .line 803
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 806
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, p1, v0}, Ldsm;->b(Landroid/content/Context;IZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 814
    :cond_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 816
    const-string v1, "circles LEFT OUTER JOIN circle_contact ON (circle_id=link_circle_id) LEFT OUTER JOIN contacts ON (link_person_id=person_id)"

    .line 823
    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "circle_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "circle_name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "semantic_hints"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "person_id"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "avatar"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "circles.sort_key AS cir_sort_key"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "contacts.interaction_sort_key AS con_interaction_sort_key"

    aput-object v4, v2, v3

    .line 835
    const-string v3, "semantic_hints&2=0 AND type!=10 AND type!=100"

    .line 839
    const-string v7, "cir_sort_key ASC, con_interaction_sort_key DESC"

    .line 842
    sget-object v4, Ldsm;->l:[Ljava/lang/String;

    mul-int/lit8 v4, p2, 0x2

    add-int/lit8 v4, v4, 0x4

    new-array v5, v4, [Ljava/lang/String;

    .line 844
    const/4 v4, 0x0

    :goto_0
    sget-object v6, Ldsm;->l:[Ljava/lang/String;

    const/4 v6, 0x4

    if-ge v4, v6, :cond_1

    .line 845
    sget-object v6, Ldsm;->l:[Ljava/lang/String;

    aget-object v6, v6, v4

    aput-object v6, v5, v4

    .line 844
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 807
    :catch_0
    move-exception v0

    .line 808
    const-string v1, "EsPeopleData"

    const-string v2, "Error syncing circles"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 809
    const/4 v0, 0x0

    .line 936
    :goto_1
    return-object v0

    .line 847
    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-ge v4, p2, :cond_2

    .line 848
    sget-object v6, Ldsm;->l:[Ljava/lang/String;

    mul-int/lit8 v6, v4, 0x2

    add-int/lit8 v6, v6, 0x4

    .line 849
    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x14

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "personId_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    .line 850
    add-int/lit8 v6, v6, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x12

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "avatar_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    .line 847
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 853
    :cond_2
    new-instance v8, Lhym;

    invoke-direct {v8, v5}, Lhym;-><init>([Ljava/lang/String;)V

    .line 855
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 856
    if-nez v6, :cond_3

    move-object v0, v8

    .line 857
    goto :goto_1

    .line 860
    :cond_3
    new-array v7, p2, [Ljava/lang/String;

    .line 865
    new-array v9, p2, [Ljava/lang/String;

    .line 866
    const/4 v2, 0x0

    .line 867
    const/4 v1, 0x0

    .line 870
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_4

    .line 933
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v0, v8

    goto :goto_1

    .line 875
    :cond_4
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 876
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 877
    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v5, v4

    .line 882
    :goto_3
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 884
    :goto_4
    if-ge v2, p2, :cond_5

    .line 885
    const/4 v10, 0x0

    aput-object v10, v7, v2

    .line 886
    const/4 v10, 0x0

    aput-object v10, v9, v2

    .line 884
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 890
    :cond_5
    mul-int/lit8 v2, p2, 0x2

    add-int/lit8 v2, v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    .line 891
    const/4 v10, 0x0

    aput-object v5, v2, v10

    .line 892
    const/4 v5, 0x1

    aput-object v3, v2, v5

    .line 893
    const/4 v3, 0x2

    aput-object v0, v2, v3

    .line 894
    const/4 v3, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    .line 895
    const/4 v1, 0x0

    :goto_5
    if-ge v1, p2, :cond_6

    .line 896
    shl-int/lit8 v3, v1, 0x1

    add-int/lit8 v3, v3, 0x4

    .line 897
    aget-object v5, v7, v1

    aput-object v5, v2, v3

    .line 898
    add-int/lit8 v3, v3, 0x1

    aget-object v5, v9, v1

    aput-object v5, v2, v3

    .line 895
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 900
    :cond_6
    invoke-virtual {v8, v2}, Lhym;->a([Ljava/lang/Object;)V

    .line 904
    const/4 v3, 0x0

    .line 905
    const/4 v2, 0x0

    .line 906
    const/4 v1, 0x0

    .line 909
    if-eqz v4, :cond_9

    move-object v5, v4

    move v4, v1

    move v1, v2

    move-object v2, v3

    .line 910
    :goto_6
    if-nez v2, :cond_a

    .line 916
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 917
    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v2

    move-object v2, v0

    .line 921
    :goto_7
    if-ge v1, p2, :cond_7

    .line 922
    const/4 v0, 0x4

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v1

    .line 923
    add-int/lit8 v0, v1, 0x1

    const/4 v10, 0x5

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v1

    move v1, v0

    .line 925
    :cond_7
    add-int/lit8 v4, v4, 0x1

    .line 927
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    .line 928
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :goto_8
    move v11, v4

    move-object v4, v0

    move-object v0, v2

    move v2, v1

    move v1, v11

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    goto :goto_8

    .line 933
    :cond_9
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v0, v8

    .line 936
    goto/16 :goto_1

    .line 933
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_a
    move-object v3, v2

    move-object v2, v0

    goto :goto_7

    :cond_b
    move v4, v1

    move v1, v2

    move-object v2, v3

    goto :goto_6
.end method

.method public static a(Landroid/content/Context;II[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 519
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 605
    const-string v5, "show_order ASC, sort_key"

    .line 609
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move-object v3, v4

    .line 731
    :goto_0
    if-eqz p4, :cond_0

    .line 732
    if-nez v3, :cond_4

    move-object v3, p4

    .line 739
    :cond_0
    :goto_1
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 740
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri$Builder;I)Landroid/net/Uri$Builder;

    .line 741
    if-eqz p5, :cond_1

    .line 742
    const-string v0, "limit"

    .line 743
    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 742
    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 746
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 611
    :pswitch_1
    const-string v3, "0"

    goto :goto_0

    .line 615
    :pswitch_2
    const-string v3, "semantic_hints&1=0 AND (type!=10 OR contact_count>0)"

    goto :goto_0

    .line 622
    :pswitch_3
    const-string v3, "semantic_hints&2=0 AND type!=10 AND type!=100"

    goto :goto_0

    .line 629
    :pswitch_4
    const-string v3, "type=1 OR circle_id=\'v.whatshot\'"

    goto :goto_0

    .line 635
    :pswitch_5
    const-string v3, "type IN (1,-1)"

    goto :goto_0

    .line 640
    :pswitch_6
    const-string v3, "semantic_hints&8=0"

    goto :goto_0

    .line 645
    :pswitch_7
    const-string v3, "semantic_hints&8=0 AND type NOT IN (9,8)"

    goto :goto_0

    .line 653
    :pswitch_8
    const-string v3, "semantic_hints&8=0 AND type!=9"

    goto :goto_0

    .line 659
    :pswitch_9
    const-string v3, "semantic_hints&8=0 AND type IN (1,5)"

    goto :goto_0

    .line 667
    :pswitch_a
    const-string v3, "for_sharing!=0 AND type=1"

    goto :goto_0

    .line 672
    :pswitch_b
    invoke-static {p0, p1}, Ldhv;->p(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "circle_id IN ("

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    move v1, v0

    :goto_2
    if-ge v1, v6, :cond_3

    if-eqz v1, :cond_2

    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 676
    :pswitch_c
    const-string v3, "semantic_hints&8=0 AND type=1"

    goto :goto_0

    .line 682
    :pswitch_d
    const-string v3, "semantic_hints&8=0"

    goto :goto_0

    .line 687
    :pswitch_e
    const-string v3, "semantic_hints&1=0"

    goto :goto_0

    .line 692
    :pswitch_f
    const-string v3, "semantic_hints&8=0 AND type IN (9,5,8)"

    goto/16 :goto_0

    .line 701
    :pswitch_10
    const-string v3, "type IN (9,8)"

    goto/16 :goto_0

    .line 706
    :pswitch_11
    const-string v3, "semantic_hints&8=0 AND type=1"

    .line 709
    const-string v5, "for_sharing DESC, circle_name ASC"

    goto/16 :goto_0

    .line 714
    :pswitch_12
    const-string v3, "type = 8"

    goto/16 :goto_0

    .line 718
    :pswitch_13
    const-string v3, "circle_name IN (?, ?)"

    .line 719
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const v1, 0x7f0a09b2

    .line 720
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const v1, 0x7f0a09b1

    .line 721
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    goto/16 :goto_0

    .line 735
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x5

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 609
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_7
        :pswitch_8
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_9
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;IZ)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 567
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    .line 568
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 569
    new-instance v0, Lhym;

    invoke-direct {v0, p3}, Lhym;-><init>([Ljava/lang/String;)V

    .line 586
    :goto_0
    return-object v0

    .line 571
    :cond_0
    invoke-static {p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 572
    if-eqz p6, :cond_1

    new-instance v1, Ljqy;

    invoke-direct {v1, p0}, Ljqy;-><init>(Landroid/content/Context;)V

    invoke-interface {v1}, Ljpr;->c()Ljps;

    move-result-object v0

    new-instance v2, Ldsp;

    invoke-direct {v2, v0, p1}, Ldsp;-><init>(Ljps;I)V

    invoke-interface {v0}, Ljps;->d()V

    const-wide/16 v4, 0x7080

    const/4 v6, 0x0

    move v3, p1

    invoke-interface/range {v1 .. v6}, Ljpr;->a(Ljqg;IJZ)V

    :cond_1
    invoke-static {p0, p1}, Ldsm;->a(Landroid/content/Context;I)J

    move-result-wide v2

    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, p1, v2, v3, v0}, Ldsm;->a(Landroid/content/Context;IJZ)V

    .line 586
    :cond_2
    :goto_2
    invoke-static/range {p0 .. p5}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 572
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 574
    :cond_4
    invoke-static {p0, p1}, Ldsm;->h(Landroid/content/Context;I)J

    move-result-wide v0

    .line 575
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 578
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, p1, v0}, Ldsm;->b(Landroid/content/Context;IZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 579
    :catch_0
    move-exception v0

    .line 580
    const-string v1, "EsPeopleData"

    const-string v2, "Error syncing circles"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 581
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 525
    if-eq p4, v2, :cond_0

    const/4 v0, 0x2

    if-eq p4, v0, :cond_0

    .line 527
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid volume control type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529
    :cond_0
    invoke-static {p0, p1, p2}, Ldsm;->d(Landroid/content/Context;ILjava/lang/String;)J

    move-result-wide v0

    .line 530
    const-wide/16 v6, -0x1

    cmp-long v3, v0, v6

    if-eqz v3, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v0

    const-wide/32 v6, 0xdbba0

    cmp-long v0, v0, v6

    if-lez v0, :cond_2

    .line 532
    :cond_1
    new-instance v0, Ldkt;

    .line 533
    invoke-static {p2}, Lhxe;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p1, p4, v1}, Ldkt;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    .line 534
    invoke-virtual {v0}, Ldkt;->l()V

    .line 535
    invoke-virtual {v0}, Ldkt;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    const/4 v0, 0x0

    .line 545
    :goto_0
    return-object v0

    .line 540
    :cond_2
    if-ne p4, v2, :cond_3

    .line 543
    invoke-static {p2}, Ldsm;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v1, p1

    move-object v3, p3

    .line 541
    invoke-static/range {v0 .. v5}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 545
    :cond_3
    const-string v0, "v.whatshot"

    .line 547
    invoke-static {v0}, Ldsm;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v3, p0

    move v4, p1

    move-object v6, p3

    move v8, v5

    .line 545
    invoke-static/range {v3 .. v8}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 1074
    invoke-static {p0, p1}, Ldsm;->b(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1075
    const/4 v0, 0x0

    .line 1086
    :goto_0
    return-object v0

    .line 1079
    :cond_0
    if-nez p2, :cond_1

    .line 1080
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    .line 1084
    :goto_1
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    .line 1086
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 1082
    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)Landroid/database/Cursor;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I[",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1155
    .line 1156
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 1158
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    .line 1159
    if-eqz p3, :cond_0

    .line 1160
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v4, v0

    .line 1161
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1162
    const-string v0, "blocked=1"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1163
    if-eqz v4, :cond_2

    array-length v0, v4

    if-lez v0, :cond_2

    .line 1164
    const-string v0, " OR person_id IN ("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1165
    const/4 v0, 0x0

    :goto_1
    array-length v3, v4

    if-ge v0, v3, :cond_1

    .line 1166
    const-string v3, "?,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1165
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move-object v4, v5

    .line 1160
    goto :goto_0

    .line 1168
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1169
    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1172
    :cond_2
    invoke-static {p0, p1}, Ldsm;->i(Landroid/content/Context;I)J

    move-result-wide v8

    .line 1173
    const-wide/16 v10, -0x1

    cmp-long v0, v8, v10

    if-nez v0, :cond_4

    .line 1174
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1175
    const-string v3, "blocked_people_sync_time"

    .line 1176
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 1175
    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1177
    const-string v3, "account_status"

    invoke-virtual {v6, v3, v0, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1179
    new-instance v3, Ldjw;

    invoke-direct {v3, p0, p1}, Ldjw;-><init>(Landroid/content/Context;I)V

    .line 1180
    invoke-virtual {v3}, Ldjw;->l()V

    .line 1181
    invoke-virtual {v3}, Ldjw;->t()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1182
    const-string v1, "EsPeopleData"

    invoke-virtual {v3, v1}, Ldjw;->d(Ljava/lang/String;)V

    .line 1184
    const-string v1, "blocked_people_sync_time"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1185
    const-string v1, "account_status"

    invoke-virtual {v6, v1, v0, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1200
    :goto_2
    return-object v5

    .line 1189
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_2

    .line 1191
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1193
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v8

    const-wide/16 v8, 0x2710

    cmp-long v1, v2, v8

    if-lez v1, :cond_5

    .line 1194
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1195
    const-string v2, "blocked_people_sync_time"

    .line 1196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1195
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1197
    const-string v2, "account_status"

    invoke-virtual {v6, v2, v1, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1198
    new-instance v1, Ldso;

    invoke-direct {v1, p0, p1}, Ldso;-><init>(Landroid/content/Context;I)V

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->a(Ljava/lang/Runnable;)V

    :cond_5
    move-object v5, v0

    .line 1200
    goto :goto_2
.end method

.method private static a(Lohv;)Ldsv;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 4602
    new-instance v0, Ldsv;

    invoke-direct {v0}, Ldsv;-><init>()V

    .line 4604
    iget-object v2, p0, Lohv;->c:Lohq;

    if-nez v2, :cond_1

    .line 4650
    :cond_0
    return-object v0

    .line 4608
    :cond_1
    iget-object v2, p0, Lohv;->b:Lohp;

    .line 4609
    iget-object v3, v2, Lohp;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 4610
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Ldsv;->a:Ljava/util/List;

    .line 4611
    new-instance v3, Loib;

    invoke-direct {v3}, Loib;-><init>()V

    .line 4612
    iget-object v4, v2, Lohp;->b:Ljava/lang/String;

    iput-object v4, v3, Loib;->d:Ljava/lang/String;

    .line 4613
    iget-object v4, v0, Ldsv;->a:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4616
    :cond_2
    iget-object v3, v2, Lohp;->e:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 4617
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Ldsv;->b:Ljava/util/List;

    .line 4618
    new-instance v3, Loim;

    invoke-direct {v3}, Loim;-><init>()V

    .line 4619
    iget-object v2, v2, Lohp;->e:Ljava/lang/String;

    iput-object v2, v3, Loim;->d:Ljava/lang/String;

    .line 4620
    iget-object v2, v0, Ldsv;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4623
    :cond_3
    iget-object v2, p0, Lohv;->c:Lohq;

    iget-object v2, v2, Lohq;->p:[Loib;

    if-eqz v2, :cond_5

    .line 4624
    iget-object v2, v0, Ldsv;->a:Ljava/util/List;

    if-nez v2, :cond_4

    .line 4625
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Ldsv;->a:Ljava/util/List;

    .line 4627
    :cond_4
    iget-object v2, p0, Lohv;->c:Lohq;

    iget-object v3, v2, Lohq;->p:[Loib;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    .line 4628
    iget-object v6, v0, Ldsv;->a:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4627
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4632
    :cond_5
    iget-object v2, p0, Lohv;->c:Lohq;

    iget-object v2, v2, Lohq;->q:[Loim;

    if-eqz v2, :cond_7

    .line 4633
    iget-object v2, v0, Ldsv;->b:Ljava/util/List;

    if-nez v2, :cond_6

    .line 4634
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Ldsv;->b:Ljava/util/List;

    .line 4636
    :cond_6
    iget-object v2, p0, Lohv;->c:Lohq;

    iget-object v3, v2, Lohq;->q:[Loim;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 4637
    iget-object v6, v0, Ldsv;->b:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4636
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4641
    :cond_7
    iget-object v2, p0, Lohv;->c:Lohq;

    iget-object v2, v2, Lohq;->r:[Lohr;

    if-eqz v2, :cond_0

    .line 4642
    iget-object v2, v0, Ldsv;->c:Ljava/util/List;

    if-nez v2, :cond_8

    .line 4643
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Ldsv;->c:Ljava/util/List;

    .line 4645
    :cond_8
    iget-object v2, p0, Lohv;->c:Lohq;

    iget-object v2, v2, Lohq;->r:[Lohr;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 4646
    iget-object v5, v0, Ldsv;->c:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4645
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public static a([B)Ldsv;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3927
    if-nez p0, :cond_0

    .line 3928
    const/4 v0, 0x0

    .line 3935
    :goto_0
    return-object v0

    .line 3930
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3931
    array-length v0, p0

    invoke-virtual {v1, p0, v2, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 3932
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 3933
    new-instance v0, Ldsv;

    invoke-direct {v0, v1}, Ldsv;-><init>(Landroid/os/Parcel;)V

    .line 3934
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Z)Ldsx;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3990
    new-instance v0, Ldsx;

    invoke-direct {v0}, Ldsx;-><init>()V

    .line 3993
    sget-object v5, Ldsm;->m:Ljava/util/HashMap;

    monitor-enter v5

    .line 3994
    :try_start_0
    sget-object v3, Ldsm;->m:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 3995
    if-nez v3, :cond_8

    .line 3996
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    .line 3997
    sget-object v4, Ldsm;->m:Ljava/util/HashMap;

    invoke-virtual {v4, p2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v3

    .line 3999
    :goto_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4001
    monitor-enter v4

    .line 4002
    :try_start_1
    invoke-static {p0, p1, p2, v0}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Ldsx;)V

    .line 4003
    iget v3, v0, Ldsx;->a:I

    if-eqz v3, :cond_1

    if-eqz p3, :cond_2

    iget-object v3, v0, Ldsx;->h:Lnjt;

    if-eqz v3, :cond_1

    iget-object v3, v0, Ldsx;->j:Lmcf;

    if-eqz v3, :cond_1

    iget-object v3, v0, Ldsx;->k:Lmdp;

    if-eqz v3, :cond_1

    iget-object v3, v0, Ldsx;->l:Lnlp;

    if-eqz v3, :cond_1

    .line 4007
    invoke-static {p2}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, v3}, Ldsm;->f(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v0, Ldsx;->p:Lnts;

    if-eqz v3, :cond_0

    iget-object v3, v0, Ldsx;->o:Lnso;

    if-eqz v3, :cond_0

    iget-object v3, v0, Ldsx;->n:Lnto;

    if-nez v3, :cond_4

    :cond_0
    move v3, v2

    :goto_1
    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    .line 4008
    :cond_2
    if-eqz v1, :cond_3

    .line 4009
    invoke-static {p2}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 4010
    if-eqz v1, :cond_3

    .line 4012
    :try_start_2
    invoke-static {p0, p1, v1}, Ldsm;->e(Landroid/content/Context;ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4022
    :try_start_3
    invoke-static {p0, p1, p2, v0}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Ldsx;)V

    .line 4025
    :cond_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 4027
    sget-object v1, Ldsm;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 4028
    :try_start_4
    sget-object v2, Ldsm;->m:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4029
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 4031
    :goto_2
    return-object v0

    .line 3999
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    :cond_4
    move v3, v1

    .line 4007
    goto :goto_1

    :cond_5
    :try_start_6
    iget-object v3, v0, Ldsx;->p:Lnts;

    if-nez v3, :cond_6

    move v3, v2

    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_1

    .line 4013
    :catch_0
    move-exception v1

    invoke-static {v1}, Lkff;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 4015
    const/4 v1, 0x1

    iput v1, v0, Ldsx;->a:I

    .line 4019
    :goto_3
    monitor-exit v4

    goto :goto_2

    .line 4025
    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 4017
    :cond_7
    const/4 v1, 0x0

    :try_start_7
    iput v1, v0, Ldsx;->a:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_3

    .line 4029
    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    :cond_8
    move-object v4, v3

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4873
    if-nez p1, :cond_1

    .line 4890
    :cond_0
    :goto_0
    return-object v0

    .line 4877
    :cond_1
    const-string v1, "circle_name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 4878
    const-string v2, "circle_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 4879
    invoke-static {p0, p2}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    .line 4881
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4883
    :cond_2
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 4884
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4885
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 4887
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 4900
    if-eqz p1, :cond_0

    .line 4901
    const v0, 0x7f0a09b2

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4903
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a09b1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 751
    if-eqz p0, :cond_0

    .line 752
    const-string v0, "(circle_name LIKE "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 753
    invoke-static {v1}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 755
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lohp;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4671
    if-nez p0, :cond_1

    .line 4687
    :cond_0
    :goto_0
    return-object v0

    .line 4675
    :cond_1
    iget-object v1, p0, Lohp;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4676
    const-string v1, "g:"

    iget-object v0, p0, Lohp;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 4679
    :cond_3
    iget-object v1, p0, Lohp;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 4680
    const-string v1, "e:"

    iget-object v0, p0, Lohp;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 4683
    :cond_5
    iget-object v1, p0, Lohp;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4684
    const-string v1, "p:"

    iget-object v0, p0, Lohp;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 508
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Ldsm;->j:Ljava/util/concurrent/CountDownLatch;

    .line 509
    return-void
.end method

.method private static a(Landroid/content/Context;IJZ)V
    .locals 12

    .prologue
    .line 5122
    new-instance v0, Ljqy;

    invoke-direct {v0, p0}, Ljqy;-><init>(Landroid/content/Context;)V

    .line 5123
    invoke-interface {v0}, Ljpr;->c()Ljps;

    move-result-object v2

    .line 5124
    new-instance v3, Landroid/os/ConditionVariable;

    invoke-direct {v3}, Landroid/os/ConditionVariable;-><init>()V

    .line 5125
    new-instance v1, Ldsq;

    move-wide v4, p2

    move-object v6, p0

    move v7, p1

    invoke-direct/range {v1 .. v7}, Ldsq;-><init>(Ljps;Landroid/os/ConditionVariable;JLandroid/content/Context;I)V

    .line 5178
    invoke-static {}, Ljpe;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 5179
    const-string v4, "Connecting client %s."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 5181
    :cond_0
    invoke-interface {v2}, Ljps;->d()V

    .line 5183
    const/4 v7, 0x0

    const/16 v8, -0x3e7

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v4, v0

    move-object v5, v1

    move v6, p1

    move/from16 v11, p4

    invoke-interface/range {v4 .. v11}, Ljpr;->a(Ljpc;ILjava/lang/String;ILjava/lang/String;ZZ)V

    .line 5187
    if-eqz p4, :cond_1

    .line 5188
    invoke-static {}, Llsx;->c()V

    .line 5189
    const-wide/16 v0, 0x2710

    invoke-virtual {v3, v0, v1}, Landroid/os/ConditionVariable;->block(J)Z

    .line 5191
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;ILiqc;J)V
    .locals 21

    .prologue
    .line 1919
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 1920
    invoke-interface/range {p2 .. p2}, Liqc;->b()I

    move-result v3

    .line 1921
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1922
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Liqc;->b(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liqb;

    .line 1923
    invoke-static {v1}, Lhxe;->a(Liqb;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1921
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1927
    :cond_0
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1928
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1930
    :try_start_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1931
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1932
    const-string v2, "circles"

    sget-object v3, Ldsm;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 1935
    const/4 v3, 0x0

    .line 1936
    const/4 v2, 0x0

    move v4, v2

    move v5, v3

    .line 1938
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1939
    const/4 v2, 0x2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1942
    const/4 v2, -0x1

    if-ne v7, v2, :cond_2

    .line 1943
    const/4 v2, 0x1

    move v5, v2

    .line 1944
    goto :goto_1

    .line 1948
    :cond_2
    const/16 v2, 0xa

    if-ne v7, v2, :cond_3

    .line 1949
    const/4 v2, 0x1

    move v4, v2

    .line 1950
    goto :goto_1

    .line 1953
    :cond_3
    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1954
    invoke-virtual {v9, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1955
    if-nez v2, :cond_4

    .line 1956
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1996
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2081
    :catchall_1
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 1958
    :cond_4
    :try_start_3
    invoke-virtual {v9, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1960
    const/4 v3, 0x1

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1961
    const/4 v3, 0x3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 1962
    const/4 v3, 0x4

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 1963
    const/4 v3, 0x6

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 1965
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Liqc;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liqb;

    .line 1966
    invoke-static {v2}, Lhxl;->a(Liqb;)I

    move-result v16

    .line 1968
    const/16 v3, 0x9

    move/from16 v0, v16

    if-ne v0, v3, :cond_6

    .line 1969
    const v3, 0x7f0a0822

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1973
    :goto_2
    invoke-interface {v2}, Liqb;->c()Ljava/lang/String;

    move-result-object v17

    .line 1974
    invoke-interface {v2}, Liqb;->e()I

    move-result v18

    .line 1975
    invoke-static/range {v16 .. v16}, Ldsm;->d(I)I

    move-result v19

    .line 1976
    invoke-interface {v2}, Liqb;->g()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    .line 1977
    :goto_3
    move/from16 v0, v16

    if-ne v0, v7, :cond_5

    .line 1978
    invoke-static {v3, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    move/from16 v0, v18

    if-ne v0, v13, :cond_5

    move/from16 v0, v19

    if-ne v0, v14, :cond_5

    if-eq v2, v15, :cond_1

    .line 1982
    :cond_5
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1983
    const-string v12, "circle_id"

    invoke-virtual {v7, v12, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1984
    const-string v8, "circle_name"

    invoke-virtual {v7, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985
    const-string v3, "sort_key"

    move-object/from16 v0, v17

    invoke-virtual {v7, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1986
    const-string v3, "type"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1987
    const-string v3, "contact_count"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1988
    const-string v3, "semantic_hints"

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1989
    const-string v3, "show_order"

    invoke-static/range {v16 .. v16}, Ldsm;->a(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1990
    const-string v3, "for_sharing"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1991
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1971
    :cond_6
    invoke-interface {v2}, Liqb;->b()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    goto :goto_2

    .line 1976
    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    .line 1996
    :cond_8
    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2000
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 2001
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2002
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2003
    const-string v2, "circle_id IN ("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2004
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2005
    const-string v8, "?,"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2006
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2008
    :cond_9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2009
    const-string v2, ")"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2010
    const-string v7, "circles"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {v1, v7, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2014
    :cond_a
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 2015
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 2016
    const-string v6, "circles"

    const-string v7, "circle_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "circle_id"

    .line 2018
    invoke-virtual {v2, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v8, v10

    .line 2016
    invoke-virtual {v1, v6, v2, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_5

    .line 2023
    :cond_b
    invoke-virtual {v9}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 2024
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 2025
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 2026
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Liqc;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liqb;

    .line 2027
    invoke-static {v2}, Lhxe;->a(Liqb;)Ljava/lang/String;

    move-result-object v8

    .line 2028
    invoke-static {v2}, Lhxl;->a(Liqb;)I

    move-result v9

    .line 2030
    const/16 v3, 0x9

    if-ne v9, v3, :cond_c

    .line 2031
    const v3, 0x7f0a0822

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2035
    :goto_7
    invoke-interface {v2}, Liqb;->c()Ljava/lang/String;

    move-result-object v10

    .line 2036
    invoke-interface {v2}, Liqb;->e()I

    move-result v11

    .line 2037
    invoke-static {v9}, Ldsm;->d(I)I

    move-result v12

    .line 2038
    invoke-interface {v2}, Liqb;->g()Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    .line 2040
    :goto_8
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 2041
    const-string v13, "circle_id"

    invoke-virtual {v6, v13, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042
    const-string v8, "circle_name"

    invoke-virtual {v6, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2043
    const-string v3, "sort_key"

    invoke-virtual {v6, v3, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2044
    const-string v3, "type"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2045
    const-string v3, "contact_count"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2046
    const-string v3, "semantic_hints"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2047
    const-string v3, "show_order"

    invoke-static {v9}, Ldsm;->a(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2048
    const-string v3, "for_sharing"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2050
    const-string v2, "circles"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_6

    .line 2033
    :cond_c
    invoke-interface {v2}, Liqb;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 2038
    :cond_d
    const/4 v2, 0x0

    goto :goto_8

    .line 2055
    :cond_e
    if-nez v5, :cond_f

    .line 2056
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2059
    :cond_f
    if-nez v4, :cond_10

    .line 2060
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2061
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 2062
    const-string v3, "circle_id"

    const-string v4, "15"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2063
    const-string v3, "circle_name"

    const v4, 0x7f0a0823

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2064
    const-string v3, "sort_key"

    const-string v4, "BLOCKED"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2065
    const-string v3, "type"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2066
    const-string v3, "semantic_hints"

    const/16 v4, 0x18

    .line 2067
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2066
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2068
    const-string v3, "show_order"

    const/16 v4, 0xa

    .line 2069
    invoke-static {v4}, Ldsm;->a(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2068
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2070
    const-string v3, "show_order"

    const/16 v4, 0xa

    .line 2071
    invoke-static {v4}, Ldsm;->a(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2070
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2073
    const-string v3, "circles"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2076
    :cond_10
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2077
    const-string v3, "circle_fingerprint"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2078
    const-string v3, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2079
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2081
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2084
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2085
    return-void
.end method

.method public static a(Landroid/content/Context;ILiqi;J)V
    .locals 17

    .prologue
    .line 1844
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 1845
    invoke-interface/range {p2 .. p2}, Liqi;->b()I

    move-result v4

    .line 1846
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    .line 1847
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Liqi;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liqh;

    .line 1848
    invoke-interface {v2}, Liqh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v11, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1846
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1851
    :cond_0
    const/4 v10, 0x0

    .line 1852
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    .line 1853
    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1854
    const-string v3, "contacts"

    sget-object v4, Ldsm;->b:[Ljava/lang/String;

    const-string v5, "in_my_circles!=0"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v4, v10

    .line 1857
    :cond_1
    :goto_1
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1858
    const/4 v3, 0x0

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1859
    invoke-virtual {v11, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 1860
    if-nez v3, :cond_2

    .line 1861
    if-nez v4, :cond_d

    .line 1862
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1864
    :goto_2
    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1865
    invoke-virtual {v11, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v3

    goto :goto_1

    .line 1867
    :cond_2
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Liqi;->b(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Liqh;

    .line 1868
    const/4 v7, 0x1

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1869
    invoke-interface {v3}, Liqh;->j()J

    move-result-wide v12

    .line 1870
    cmp-long v3, v8, v12

    if-nez v3, :cond_1

    .line 1871
    invoke-virtual {v11, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1876
    :catchall_0
    move-exception v2

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 1879
    invoke-virtual {v11, v11}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1881
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1883
    if-eqz v4, :cond_4

    .line 1884
    :try_start_1
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1885
    invoke-static {v2, v3}, Ldsm;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    .line 1906
    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 1889
    :cond_4
    :try_start_2
    invoke-virtual {v11}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 1890
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Liqi;->b(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Liqh;

    .line 1891
    invoke-interface {v3}, Liqh;->a()Ljava/lang/String;

    move-result-object v8

    .line 1892
    invoke-interface {v3}, Liqh;->h()[Ljava/lang/String;

    move-result-object v4

    .line 1893
    if-eqz v4, :cond_a

    array-length v4, v4

    if-lez v4, :cond_a

    const/4 v4, 0x1

    move v6, v4

    :goto_5
    const/4 v4, 0x0

    invoke-interface {v3}, Liqh;->a()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v5, 0x0

    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    invoke-interface {v3}, Liqh;->c()Ljava/lang/String;

    move-result-object v12

    if-eqz v6, :cond_5

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_5

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v5, v12}, Ldsm;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_5
    const-string v13, "name"

    invoke-virtual {v11, v13, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "sort_key"

    invoke-interface {v3}, Liqh;->e()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "interaction_sort_key"

    invoke-interface {v3}, Liqh;->f()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "avatar"

    invoke-interface {v3}, Liqh;->d()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "last_updated_time"

    invoke-interface {v3}, Liqh;->j()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v12, "in_my_circles"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v3}, Liqh;->g()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_b

    const-string v12, "profile_type"

    const/4 v13, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_6
    if-eqz v6, :cond_6

    if-nez v10, :cond_6

    const-string v6, "profile_state"

    const/4 v12, 0x2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v6, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_6
    const-string v6, "contacts"

    const-string v12, "person_id=?"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object v9, v13, v14

    invoke-virtual {v2, v6, v11, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_7

    const/4 v4, 0x1

    const-string v6, "person_id"

    invoke-virtual {v11, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "gaia_id"

    invoke-virtual {v11, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "contacts"

    const/4 v10, 0x0

    invoke-virtual {v2, v6, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_7
    if-eqz v5, :cond_8

    invoke-static {v2, v9, v5, v4}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    :cond_8
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    const-string v5, "contact_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v11, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "profiles"

    const-string v6, "profile_person_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v12, 0x0

    aput-object v9, v10, v12

    invoke-virtual {v2, v5, v11, v6, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_9

    const-string v5, "profile_person_id"

    invoke-virtual {v11, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "profiles"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1897
    :cond_9
    invoke-static {v2, v8, v3, v4}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Liqh;Z)V

    goto/16 :goto_4

    .line 1893
    :cond_a
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_5

    :cond_b
    const-string v12, "profile_type"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_6

    .line 1900
    :cond_c
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1901
    const-string v4, "people_fingerprint"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1902
    const-string v4, "account_status"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1904
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1909
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1910
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1911
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1912
    return-void

    :cond_d
    move-object v3, v4

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ldsx;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 4074
    iput v9, p3, Ldsx;->a:I

    .line 4077
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4079
    const-string v1, "contacts LEFT OUTER JOIN profiles ON (contacts.person_id=profiles.profile_person_id) LEFT OUTER JOIN circle_contact ON ( contacts.person_id = circle_contact.link_person_id)"

    sget-object v2, Ldsm;->f:[Ljava/lang/String;

    const-string v3, "profiles.profile_person_id=?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object p2, v4, v9

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4084
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4085
    invoke-static {p2}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Ldsx;->b:Ljava/lang/String;

    .line 4086
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p3, Ldsx;->a:I

    .line 4087
    const/4 v0, 0x3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v8

    :goto_0
    iput-boolean v0, p3, Ldsx;->e:Z

    .line 4088
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Ldsx;->d:Ljava/lang/String;

    .line 4089
    const/4 v0, 0x4

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    .line 4090
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Ldsx;->c:Ljava/lang/String;

    .line 4091
    const/4 v0, 0x4

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p3, Ldsx;->g:J

    .line 4092
    const/4 v0, 0x5

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4093
    if-eqz v0, :cond_0

    .line 4094
    invoke-static {v0}, Ldsm;->a([B)Ldsv;

    move-result-object v0

    iput-object v0, p3, Ldsx;->f:Ldsv;

    .line 4096
    :cond_0
    const/4 v0, 0x6

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p3, Ldsx;->m:J

    .line 4097
    const/4 v0, 0x7

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4098
    if-eqz v0, :cond_1

    .line 4099
    invoke-static {v0}, Ldsm;->b([B)Lnjt;

    move-result-object v0

    iput-object v0, p3, Ldsx;->h:Lnjt;

    .line 4101
    :cond_1
    const/16 v0, 0x8

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4102
    if-eqz v0, :cond_2

    .line 4103
    invoke-static {v0}, Ldsm;->e([B)Lmcf;

    move-result-object v0

    iput-object v0, p3, Ldsx;->j:Lmcf;

    .line 4105
    :cond_2
    const/16 v0, 0x9

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4106
    if-eqz v0, :cond_3

    .line 4107
    invoke-static {v0}, Ldsm;->f([B)Lmdp;

    move-result-object v0

    iput-object v0, p3, Ldsx;->k:Lmdp;

    .line 4109
    :cond_3
    const/16 v0, 0xa

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4110
    if-eqz v0, :cond_4

    .line 4111
    invoke-static {v0}, Ldsm;->h([B)Lnlp;

    move-result-object v0

    iput-object v0, p3, Ldsx;->l:Lnlp;

    .line 4113
    :cond_4
    const/16 v0, 0xb

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4114
    if-eqz v0, :cond_5

    .line 4115
    invoke-static {v0}, Ldsm;->g([B)Lnum;

    .line 4117
    :cond_5
    const/16 v0, 0xc

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4119
    if-eqz v0, :cond_6

    .line 4120
    invoke-static {v0}, Ldsm;->g([B)Lnum;

    .line 4123
    :cond_6
    const/16 v0, 0xd

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4125
    if-eqz v0, :cond_7

    .line 4126
    invoke-static {v0}, Ldsm;->d([B)Lnkm;

    move-result-object v0

    iput-object v0, p3, Ldsx;->i:Lnkm;

    .line 4128
    :cond_7
    const/16 v0, 0xe

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4129
    if-eqz v0, :cond_8

    .line 4130
    invoke-static {v0}, Ldsm;->c([B)Lnts;

    move-result-object v0

    iput-object v0, p3, Ldsx;->p:Lnts;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4134
    :cond_8
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4137
    invoke-static {p2}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Ldsm;->f(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4138
    const-class v0, Lkxp;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkxp;

    .line 4139
    invoke-virtual {v0, p1}, Lkxp;->a(I)Lnto;

    move-result-object v1

    iput-object v1, p3, Ldsx;->n:Lnto;

    .line 4140
    invoke-virtual {v0, p1}, Lkxp;->b(I)Lnso;

    move-result-object v0

    iput-object v0, p3, Ldsx;->o:Lnso;

    .line 4142
    :cond_9
    return-void

    :cond_a
    move v0, v9

    .line 4087
    goto/16 :goto_0

    .line 4134
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2318
    sget-object v2, Ldsm;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 2320
    :try_start_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v3

    invoke-virtual {v3}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2321
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2322
    const-string v5, "circle_id"

    invoke-static {p2}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2323
    const-string v5, "circle_name"

    invoke-virtual {v4, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    const-string v5, "sort_key"

    invoke-virtual {v4, v5, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325
    const-string v5, "type"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2326
    const-string v5, "contact_count"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2327
    const-string v5, "for_sharing"

    if-eqz p5, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2328
    const-string v0, "show_order"

    const/4 v1, 0x1

    .line 2329
    invoke-static {v1}, Ldsm;->a(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2328
    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2331
    const-string v0, "circles"

    const/4 v1, 0x0

    const/4 v5, 0x5

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 2333
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2335
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2337
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    .line 2338
    return-void

    :cond_0
    move v0, v1

    .line 2327
    goto :goto_0

    .line 2333
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2351
    .line 2352
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2353
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2354
    const-string v0, "circle_name"

    invoke-virtual {v4, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2355
    const-string v5, "for_sharing"

    if-eqz p4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2357
    const-string v0, "circles"

    const-string v5, "circle_id=?"

    new-array v2, v2, [Ljava/lang/String;

    .line 2358
    invoke-static {p2}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 2357
    invoke-virtual {v3, v0, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2360
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2362
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    .line 2363
    return-void

    :cond_0
    move v0, v2

    .line 2355
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lmcf;)V
    .locals 7

    .prologue
    .line 3958
    const-string v1, "g:"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3961
    :goto_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3962
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3964
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "people_data_proto"

    invoke-static {p3}, Ldsm;->a(Lmcf;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "profile_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "profiles"

    const-string v4, "profile_person_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3965
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3967
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3970
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 3971
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->e:Landroid/net/Uri;

    .line 3972
    invoke-static {v2, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x0

    .line 3971
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3973
    return-void

    .line 3958
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 3967
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lnjt;Lmcf;Lmdp;Lnlp;Lnum;Lnum;Lnkm;Lnts;)V
    .locals 14

    .prologue
    .line 3278
    const-string v1, "EsPeopleData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3279
    if-eqz p3, :cond_2

    invoke-virtual/range {p3 .. p3}, Lnjt;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Profile for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3282
    :cond_0
    if-nez p3, :cond_3

    .line 3319
    :cond_1
    :goto_1
    return-void

    .line 3279
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 3286
    :cond_3
    const-string v1, "g:"

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3289
    :goto_2
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 3291
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3293
    const/4 v13, 0x1

    move-object v1, p0

    move v2, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    :try_start_0
    invoke-static/range {v1 .. v13}, Ldsm;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lnjt;Lmcf;Lmdp;Lnlp;Lnum;Lnum;Lnkm;Lnts;Z)Z

    move-result v1

    .line 3296
    if-eqz v1, :cond_7

    move-object/from16 v0, p3

    iget-object v2, v0, Lnjt;->c:Lnia;

    if-eqz v2, :cond_7

    move-object/from16 v0, p3

    iget-object v2, v0, Lnjt;->c:Lnia;

    iget-object v2, v2, Lnia;->e:Lnju;

    if-eqz v2, :cond_7

    move-object/from16 v0, p3

    iget-object v2, v0, Lnjt;->c:Lnia;

    iget-object v2, v2, Lnia;->e:Lnju;

    iget-object v2, v2, Lnju;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    .line 3298
    move-object/from16 v0, p3

    iget-object v2, v0, Lnjt;->c:Lnia;

    iget-object v2, v2, Lnia;->e:Lnju;

    iget-object v2, v2, Lnju;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 3299
    move-object/from16 v0, p3

    iget-object v5, v0, Lnjt;->g:Ljava/lang/String;

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "blocked"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    if-eqz v2, :cond_4

    const-string v7, "in_my_circles"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_4
    const-string v7, "contacts"

    const-string v8, "person_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v4, v9, v10

    invoke-virtual {v3, v7, v6, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_5

    if-eqz v2, :cond_5

    const-string v7, "person_id"

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "gaia_id"

    invoke-static {v4}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "name"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "contacts"

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_5
    if-eqz v2, :cond_6

    const-string v5, "circle_contact"

    const-string v6, "link_person_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v3, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v5, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=circle_id) WHERE type=1"

    invoke-virtual {v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3300
    :cond_6
    if-nez v2, :cond_7

    .line 3301
    move-object/from16 v0, p3

    iget-object v2, v0, Lnjt;->c:Lnia;

    iget-object v2, v2, Lnia;->e:Lnju;

    iget-object v2, v2, Lnju;->a:Lohv;

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lohv;Z)V

    .line 3305
    :cond_7
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3307
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3310
    if-eqz v1, :cond_1

    .line 3311
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 3312
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->e:Landroid/net/Uri;

    invoke-static {v2, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3314
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3315
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3317
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 3286
    :cond_8
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3307
    :catchall_0
    move-exception v1

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lnlp;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 3553
    const-string v0, "EsPeopleData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3554
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1c

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Profile for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Adding reviews"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3557
    :cond_0
    if-nez p3, :cond_1

    .line 3631
    :goto_0
    return-void

    .line 3562
    :cond_1
    const-string v1, "g:"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    .line 3564
    :goto_1
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3566
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3568
    :try_start_0
    const-string v1, "profiles"

    sget-object v2, Ldsm;->c:[Ljava/lang/String;

    const-string v3, "profile_person_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v8, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 3572
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3573
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 3574
    invoke-static {v1}, Ldsm;->h([B)Lnlp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 3577
    :goto_2
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 3580
    if-eqz v1, :cond_2

    iget-object v2, v1, Lnlp;->a:Lpvl;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lnlp;->a:Lpvl;

    iget-object v2, v2, Lpvl;->b:[Lpvt;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lnlp;->a:Lpvl;

    iget-object v2, v2, Lpvl;->b:[Lpvt;

    array-length v2, v2

    if-eqz v2, :cond_2

    .line 3585
    iget-object v2, v1, Lnlp;->a:Lpvl;

    iget-object v2, v2, Lpvl;->b:[Lpvt;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    .line 3588
    if-eqz p3, :cond_6

    iget-object v2, p3, Lnlp;->a:Lpvl;

    if-eqz v2, :cond_6

    iget-object v2, p3, Lnlp;->a:Lpvl;

    iget-object v2, v2, Lpvl;->b:[Lpvt;

    if-eqz v2, :cond_6

    iget-object v2, p3, Lnlp;->a:Lpvl;

    iget-object v2, v2, Lpvl;->b:[Lpvt;

    array-length v2, v2

    if-eqz v2, :cond_6

    .line 3592
    iget-object v2, p3, Lnlp;->a:Lpvl;

    iget-object v2, v2, Lpvl;->b:[Lpvt;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    .line 3595
    :goto_3
    if-eqz v2, :cond_4

    .line 3596
    iget-object v4, v2, Lpvt;->e:Ljava/lang/String;

    iput-object v4, v3, Lpvt;->e:Ljava/lang/String;

    .line 3598
    iget-object v4, v2, Lpvt;->d:[Lpvi;

    if-eqz v4, :cond_5

    iget-object v4, v2, Lpvt;->d:[Lpvi;

    array-length v4, v4

    if-eqz v4, :cond_5

    .line 3599
    iget-object v4, v3, Lpvt;->d:[Lpvi;

    array-length v4, v4

    iget-object v5, v2, Lpvt;->d:[Lpvi;

    array-length v5, v5

    add-int/2addr v4, v5

    new-array v4, v4, [Lpvi;

    .line 3601
    iget-object v5, v3, Lpvt;->d:[Lpvi;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v10, v3, Lpvt;->d:[Lpvi;

    array-length v10, v10

    invoke-static {v5, v6, v4, v7, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3602
    iget-object v5, v2, Lpvt;->d:[Lpvi;

    const/4 v6, 0x0

    iget-object v7, v3, Lpvt;->d:[Lpvi;

    array-length v7, v7

    iget-object v2, v2, Lpvt;->d:[Lpvi;

    array-length v2, v2

    invoke-static {v5, v6, v4, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3604
    iput-object v4, v3, Lpvt;->d:[Lpvi;

    move-object p3, v1

    .line 3615
    :cond_2
    :goto_4
    invoke-static {p3}, Ldsm;->a(Lnlp;)[B

    move-result-object v1

    .line 3617
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 3618
    const-string v3, "reviews_data_proto"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3620
    const-string v1, "profiles"

    const-string v3, "profile_person_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v8, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3623
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3625
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3628
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3629
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->e:Landroid/net/Uri;

    invoke-static {v1, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 3562
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v8, v0

    goto/16 :goto_1

    .line 3577
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3625
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 3608
    :cond_4
    const/4 v2, 0x0

    :try_start_4
    iput-object v2, v3, Lpvt;->e:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_5
    move-object p3, v1

    .line 3611
    goto :goto_4

    :cond_6
    move-object v2, v9

    goto :goto_3

    :cond_7
    move-object v1, v9

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lodw;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4410
    .line 4411
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 4412
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 4414
    :try_start_0
    invoke-static {p2}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "notifications_enabled"

    iget v6, p3, Lodw;->b:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "volume"

    iget v1, p3, Lodw;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "last_volume_sync"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "circles"

    const-string v1, "circle_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {v2, v0, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4416
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4418
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 4421
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4422
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 4423
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 4424
    return-void

    :cond_0
    move v0, v1

    .line 4414
    goto :goto_0

    .line 4418
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lohv;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Lohv;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4284
    const/4 v3, 0x0

    .line 4288
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 4289
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 4291
    if-eqz p3, :cond_1

    :try_start_0
    move-object/from16 v0, p3

    iget-object v2, v0, Lohv;->b:Lohp;

    invoke-static {v2}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 4292
    :goto_0
    move-object/from16 v0, p2

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4293
    move-object/from16 v0, p2

    invoke-static {v5, v0}, Ldsm;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    .line 4296
    :cond_0
    invoke-static {v5, v4}, Ldsm;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v6

    .line 4297
    if-nez v6, :cond_2

    if-eqz p3, :cond_2

    .line 4298
    move-object/from16 v0, p3

    invoke-static {v5, v4, v0}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lohv;)V

    .line 4315
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 4316
    if-eqz p4, :cond_5

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 4317
    const-string v2, "INSERT OR IGNORE INTO circle_contact(link_person_id,link_circle_id) SELECT "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 4320
    invoke-static {v4}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "circle_id"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "circles"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "circle_id"

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x12

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " FROM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4317
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4324
    const/4 v2, 0x0

    :goto_2
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 4325
    const-string v3, "?,"

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4324
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move-object/from16 v4, p2

    .line 4291
    goto/16 :goto_0

    .line 4300
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 4304
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 4305
    const-string v7, "contact_update_time"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v2, v7, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4306
    const-string v7, "profiles"

    const-string v10, "profile_person_id=?"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    invoke-virtual {v5, v7, v2, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4309
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 4310
    const-string v7, "last_updated_time"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4311
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    .line 4312
    const-string v8, "contacts"

    const-string v9, "person_id=?"

    invoke-virtual {v5, v8, v2, v9, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 4363
    :catchall_0
    move-exception v2

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 4327
    :cond_3
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 4328
    const-string v2, ")"

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4329
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4330
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-static {v5, v2}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V

    .line 4332
    if-nez v6, :cond_4

    .line 4333
    invoke-static {v5, v4}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 4336
    :cond_4
    const/4 v3, 0x1

    .line 4339
    :cond_5
    if-eqz p5, :cond_8

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_8

    .line 4340
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 4341
    const-string v2, "DELETE FROM circle_contact WHERE link_person_id="

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 4343
    invoke-static {v4}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "link_circle_id"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0xb

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " AND "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " IN  ("

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4341
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4346
    const/4 v2, 0x0

    :goto_3
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v2, v8, :cond_6

    .line 4347
    const-string v8, "?,"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4346
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 4349
    :cond_6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 4350
    const-string v2, ")"

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4351
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v2, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4353
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 4352
    invoke-static {v5, v2}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V

    .line 4355
    invoke-static {v5, v4}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 4357
    if-nez v3, :cond_8

    .line 4358
    invoke-static {v5, v4}, Ldsm;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v2

    .line 4361
    :goto_4
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4363
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 4366
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 4367
    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 4368
    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 4370
    if-eq v6, v2, :cond_7

    .line 4371
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    .line 4373
    :cond_7
    return-void

    :cond_8
    move v2, v3

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2401
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2437
    :cond_0
    :goto_0
    return-void

    .line 2406
    :cond_1
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2407
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2408
    const-string v0, " IN ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2409
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2410
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    .line 2411
    if-lez v0, :cond_2

    .line 2412
    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2414
    :cond_2
    const/16 v4, 0x3f

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2410
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2416
    :cond_3
    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2417
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2419
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 2420
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2422
    :try_start_0
    const-string v4, "circles"

    const-string v1, "circle_id"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v4, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2424
    const-string v4, "circle_contact"

    const-string v1, "link_circle_id"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v5, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v2, v4, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2427
    invoke-static {v2, v0}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V

    .line 2428
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2430
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2433
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2434
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2436
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 2422
    :cond_4
    :try_start_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 2430
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 2424
    :cond_5
    :try_start_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;ILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lohv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2606
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 2608
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 2609
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2611
    const/4 v0, 0x0

    move v2, v0

    .line 2612
    :goto_0
    if-ge v2, v1, :cond_1

    .line 2613
    add-int/lit8 v0, v2, 0x4b

    .line 2614
    if-le v0, v1, :cond_0

    move v0, v1

    .line 2617
    :cond_0
    invoke-static {p0, v3, p2, v2, v0}, Ldsm;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;II)I

    move v2, v0

    .line 2619
    goto :goto_0

    .line 2620
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;ILkfp;Lles;)V
    .locals 20

    .prologue
    .line 1318
    sget-object v17, Ldsm;->i:Ljava/lang/Object;

    monitor-enter v17

    .line 1320
    :try_start_0
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v4

    invoke-virtual {v4}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 1322
    const-class v4, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    .line 1323
    move/from16 v0, p1

    invoke-interface {v4, v0}, Lhei;->a(I)Lhej;

    move-result-object v4

    .line 1324
    const-string v5, "gaia_id"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1325
    invoke-static {v4}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1327
    const-string v5, "MyProfile"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lkfp;->c(Ljava/lang/String;)V

    .line 1328
    new-instance v5, Ldjy;

    new-instance v8, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v8, v0, v1, v2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v5, v0, v8, v1, v4}, Ldjy;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;)V

    .line 1330
    invoke-virtual {v5}, Ldjy;->l()V

    .line 1331
    if-eqz p3, :cond_0

    .line 1332
    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Lles;->a(Lkff;)V

    .line 1334
    :cond_0
    const-string v8, "EsPeopleData"

    invoke-virtual {v5, v8}, Ldjy;->e(Ljava/lang/String;)V

    .line 1336
    invoke-virtual {v5}, Ldjy;->b()Lohv;

    move-result-object v5

    .line 1337
    invoke-static {v5}, Ldsm;->c(Lohv;)J

    move-result-wide v18

    .line 1339
    move-wide/from16 v0, v18

    invoke-static {v6, v7, v0, v1}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1340
    invoke-virtual/range {p2 .. p2}, Lkfp;->f()V

    .line 1341
    monitor-exit v17

    .line 1370
    :goto_0
    return-void

    .line 1344
    :cond_1
    new-instance v5, Lkbv;

    new-instance v8, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v8, v0, v1, v2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v5, v0, v8, v1, v4}, Lkbv;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;)V

    .line 1346
    invoke-virtual {v5}, Lkbv;->l()V

    .line 1347
    if-eqz p3, :cond_2

    .line 1348
    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Lles;->a(Lkff;)V

    .line 1350
    :cond_2
    const-string v4, "EsPeopleData"

    invoke-virtual {v5, v4}, Lkbv;->e(Ljava/lang/String;)V

    .line 1352
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1354
    :try_start_1
    invoke-virtual {v5}, Lkbv;->i()Lnjt;

    move-result-object v8

    .line 1355
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v4, p0

    move/from16 v5, p1

    invoke-static/range {v4 .. v16}, Ldsm;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lnjt;Lmcf;Lmdp;Lnlp;Lnum;Lnum;Lnkm;Lnts;Z)Z

    .line 1356
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "last_updated_time"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "contacts"

    const-string v8, "person_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-virtual {v6, v5, v4, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1357
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1359
    :try_start_2
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1362
    invoke-virtual/range {p2 .. p2}, Lkfp;->f()V

    .line 1364
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 1365
    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->e:Landroid/net/Uri;

    invoke-static {v5, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1367
    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1369
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    .line 1370
    monitor-exit v17

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v17
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 1359
    :catchall_1
    move-exception v4

    :try_start_3
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public static a(Landroid/content/Context;ILkfp;Lles;Z)V
    .locals 23

    .prologue
    .line 1388
    sget-object v16, Ldsm;->h:Ljava/lang/Object;

    monitor-enter v16

    .line 1389
    if-nez p4, :cond_0

    .line 1390
    :try_start_0
    invoke-static/range {p0 .. p1}, Ldsm;->j(Landroid/content/Context;I)J

    move-result-wide v4

    .line 1391
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/32 v6, 0xea60

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 1394
    monitor-exit v16

    .line 1415
    :goto_0
    return-void

    .line 1399
    :cond_0
    sget-object v5, Ldsm;->g:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1400
    :try_start_1
    invoke-static/range {p0 .. p3}, Ldsm;->b(Landroid/content/Context;ILkfp;Lles;)Z

    move-result v17

    .line 1401
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1402
    :try_start_2
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    :goto_1
    and-int v4, v4, v17

    .line 1404
    if-eqz v4, :cond_1

    .line 1405
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1406
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1407
    const-string v5, "circle_sync_time"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1408
    const-string v5, "people_sync_time"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1409
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v5

    .line 1410
    invoke-virtual {v5}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 1411
    const-string v6, "account_status"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1413
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1415
    :cond_1
    monitor-exit v16

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 1401
    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v4

    .line 1402
    :cond_2
    invoke-static/range {p0 .. p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static/range {p0 .. p1}, Ldsm;->m(Landroid/content/Context;I)V

    :cond_3
    :goto_2
    const/4 v4, 0x1

    goto :goto_1

    :cond_4
    const-string v4, "SocialNetwork"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lkfp;->c(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v4

    invoke-virtual {v4}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    invoke-static/range {p0 .. p1}, Ldsm;->k(Landroid/content/Context;I)[B

    move-result-object v11

    const/4 v6, 0x0

    const/4 v5, 0x0

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    sget-object v7, Lfvc;->g:Lfvc;

    invoke-virtual {v7}, Lfvc;->b()Z

    move-result v7

    if-eqz v7, :cond_21

    const/4 v4, 0x1

    move v14, v4

    move v12, v5

    move-object v15, v6

    :goto_3
    if-nez v14, :cond_18

    new-instance v4, Ldky;

    new-instance v6, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v6, v0, v1, v2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/16 v10, 0x12c

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v11}, Ldky;-><init>(Landroid/content/Context;Lkfo;ZZZI[B)V

    invoke-virtual {v4}, Ldky;->l()V

    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lles;->a(Lkff;)V

    :cond_5
    invoke-virtual {v4}, Ldky;->t()Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "EsPeopleData"

    invoke-virtual {v4, v5}, Ldky;->d(Ljava/lang/String;)V

    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v4}, Ldky;->d()Loik;

    move-result-object v9

    iget-object v7, v9, Loik;->b:[I

    if-eqz v7, :cond_9

    array-length v8, v7

    const/4 v4, 0x0

    move v6, v4

    move-object v5, v15

    move v4, v14

    :goto_4
    if-ge v6, v8, :cond_20

    aget v10, v7, v6

    const/4 v14, 0x1

    if-ne v10, v14, :cond_8

    invoke-static/range {v18 .. v18}, Ldsm;->b(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashSet;

    move-result-object v5

    :cond_7
    :goto_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_8
    const/4 v14, 0x2

    if-ne v10, v14, :cond_7

    const/4 v4, 0x1

    goto :goto_5

    :cond_9
    move v7, v14

    move-object v8, v15

    :goto_6
    iget-object v4, v9, Loik;->c:Loiz;

    if-eqz v4, :cond_a

    iget-object v4, v9, Loik;->c:Loiz;

    invoke-static {v4}, Loxu;->a(Loxu;)[B

    move-result-object v11

    :cond_a
    if-nez v11, :cond_b

    const/4 v7, 0x1

    :cond_b
    iget-object v4, v9, Loik;->d:[Lohp;

    if-eqz v4, :cond_1f

    iget-object v4, v9, Loik;->d:[Lohp;

    array-length v4, v4

    if-lez v4, :cond_1f

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, v9, Loik;->d:[Lohp;

    array-length v14, v10

    const/4 v4, 0x0

    move v5, v4

    :goto_7
    if-ge v5, v14, :cond_12

    aget-object v15, v10, v5

    iget-object v4, v15, Lohp;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    const-string v21, "g:"

    iget-object v4, v15, Lohp;->d:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v22

    if-eqz v22, :cond_f

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_8
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    iget-object v4, v15, Lohp;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    const-string v21, "e:"

    iget-object v4, v15, Lohp;->b:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v22

    if-eqz v22, :cond_10

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_9
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    iget-object v4, v15, Lohp;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_e

    const-string v21, "p:"

    iget-object v4, v15, Lohp;->e:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v15

    if-eqz v15, :cond_11

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_a
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_7

    :cond_f
    new-instance v4, Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    :cond_10
    new-instance v4, Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_9

    :cond_11
    new-instance v4, Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_a

    :cond_12
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1f

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v6}, Ldsm;->c(Landroid/content/Context;ILjava/util/List;)Z

    move-result v4

    or-int/2addr v12, v4

    move v5, v12

    :goto_b
    iget-object v4, v9, Loik;->a:[Lohv;

    if-eqz v4, :cond_1e

    iget-object v9, v9, Loik;->a:[Lohv;

    array-length v10, v9

    const/4 v4, 0x0

    move v6, v4

    move v4, v13

    :goto_c
    if-ge v6, v10, :cond_16

    aget-object v12, v9, v6

    iget-object v13, v12, Lohv;->b:Lohp;

    invoke-static {v13}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_14

    invoke-static {v12}, Ldsm;->b(Lohv;)I

    move-result v14

    if-eqz v14, :cond_15

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_d
    if-eqz v8, :cond_13

    invoke-virtual {v8, v13}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :cond_13
    add-int/lit8 v4, v4, 0x1

    :cond_14
    add-int/lit8 v6, v6, 0x1

    goto :goto_c

    :cond_15
    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :cond_16
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v9, 0x7d0

    if-le v6, v9, :cond_17

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Ldsm;->b(Landroid/content/Context;ILjava/util/List;)Z

    move-result v6

    or-int/2addr v5, v6

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_17

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Ldsm;->c(Landroid/content/Context;ILjava/util/List;)Z

    move-result v6

    or-int/2addr v5, v6

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->clear()V

    :cond_17
    move v14, v7

    move v12, v5

    move-object v15, v8

    move v13, v4

    goto/16 :goto_3

    :cond_18
    if-eqz v15, :cond_19

    invoke-virtual {v15}, Ljava/util/HashSet;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_19

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_19
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1d

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Ldsm;->c(Landroid/content/Context;ILjava/util/List;)Z

    move-result v4

    or-int/2addr v4, v12

    :goto_e
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Ldsm;->b(Landroid/content/Context;ILjava/util/List;)Z

    move-result v5

    or-int/2addr v4, v5

    const-string v5, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=circle_id) WHERE type=1"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    if-eqz v4, :cond_1a

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1a
    sget-object v5, Ldsm;->j:Ljava/util/concurrent/CountDownLatch;

    if-eqz v5, :cond_1b

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    const/4 v5, 0x0

    sput-object v5, Ldsm;->j:Ljava/util/concurrent/CountDownLatch;

    :cond_1b
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "people_last_update_token"

    invoke-virtual {v5, v6, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v6, "account_status"

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1c

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1c
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lkfp;->m(I)V

    if-eqz v4, :cond_3

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    :cond_1d
    move v4, v12

    goto :goto_e

    :cond_1e
    move v14, v7

    move v12, v5

    move-object v15, v8

    goto/16 :goto_3

    :cond_1f
    move v5, v12

    goto/16 :goto_b

    :cond_20
    move v7, v4

    move-object v8, v5

    goto/16 :goto_6

    :cond_21
    move v14, v4

    move v12, v5

    move-object v15, v6

    goto/16 :goto_3
.end method

.method public static a(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 1023
    invoke-static {p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1024
    invoke-static {p0, p1}, Ldsm;->a(Landroid/content/Context;I)J

    move-result-wide v0

    .line 1025
    invoke-static {p0, p1, v0, v1, p2}, Ldsm;->a(Landroid/content/Context;IJZ)V

    .line 1027
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;I[Lohv;)V
    .locals 16

    .prologue
    .line 1230
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 1233
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1234
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1236
    :try_start_0
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 1237
    const/4 v3, 0x1

    new-array v12, v3, [Ljava/lang/String;

    .line 1240
    const-string v3, "contacts"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "person_id"

    aput-object v6, v4, v5

    const-string v5, "blocked=1"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 1244
    :goto_0
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1245
    const/4 v3, 0x0

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1248
    :catchall_0
    move-exception v3

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1300
    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 1248
    :cond_0
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1251
    if-nez p2, :cond_1

    const/4 v3, 0x0

    move v4, v3

    .line 1252
    :goto_1
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_2

    .line 1253
    aget-object v5, p2, v3

    iget-object v5, v5, Lohv;->b:Lohp;

    invoke-static {v5}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1252
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1251
    :cond_1
    move-object/from16 v0, p2

    array-length v3, v0

    move v4, v3

    goto :goto_1

    .line 1256
    :cond_2
    invoke-virtual {v10}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1257
    const-string v3, "blocked"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v11, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1258
    const-string v3, "last_updated_time"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v11, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1259
    invoke-virtual {v10}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1260
    const/4 v6, 0x0

    aput-object v3, v12, v6

    .line 1261
    const-string v3, "contacts"

    const-string v6, "person_id=?"

    invoke-virtual {v2, v3, v11, v6, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_3

    .line 1268
    :cond_3
    const/4 v3, 0x0

    move v5, v3

    :goto_4
    if-ge v5, v4, :cond_7

    .line 1269
    aget-object v6, p2, v5

    .line 1270
    aget-object v3, p2, v5

    iget-object v3, v3, Lohv;->b:Lohp;

    invoke-static {v3}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v7

    .line 1272
    iget-object v3, v6, Lohv;->c:Lohq;

    iget v3, v3, Lohq;->g:I

    const/4 v8, 0x3

    if-ne v3, v8, :cond_6

    const/4 v3, 0x2

    .line 1274
    :goto_5
    iget-object v8, v6, Lohv;->c:Lohq;

    iget-object v8, v8, Lohq;->a:Ljava/lang/String;

    .line 1276
    invoke-static {v6}, Ldsm;->c(Lohv;)J

    move-result-wide v14

    .line 1274
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "name"

    invoke-virtual {v6, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "last_updated_time"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "in_my_circles"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "blocked"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "profile_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "avatar"

    const/4 v8, 0x0

    invoke-static {v8}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v7}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "profile_state"

    const/4 v9, 0x6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "contacts"

    const-string v9, "person_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object v7, v10, v13

    invoke-virtual {v2, v8, v6, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "person_id"

    invoke-virtual {v6, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "gaia_id"

    invoke-virtual {v6, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "contacts"

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_4
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    const-string v3, "contact_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "contact_proto"

    const/4 v8, 0x0

    invoke-static {v8}, Ldsm;->a(Ldsv;)[B

    move-result-object v8

    invoke-virtual {v6, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "profiles"

    const-string v8, "profile_person_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-virtual {v2, v3, v6, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "profile_person_id"

    invoke-virtual {v6, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "profiles"

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1279
    :cond_5
    const/4 v3, 0x0

    aput-object v7, v12, v3

    .line 1280
    const-string v3, "circle_contact"

    const-string v6, "link_person_id=?"

    invoke-virtual {v2, v3, v6, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1283
    const-string v3, "contact_search"

    const-string v6, "search_person_id=?"

    invoke-virtual {v2, v3, v6, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1268
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_4

    .line 1272
    :cond_6
    const/4 v3, 0x1

    goto/16 :goto_5

    .line 1287
    :cond_7
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 1288
    const-string v3, "contact_count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1289
    const/4 v3, 0x0

    const-string v4, "15"

    aput-object v4, v12, v3

    .line 1290
    const-string v3, "circles"

    const-string v4, "circle_id=?"

    invoke-virtual {v2, v3, v11, v4, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1293
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 1294
    const-string v3, "blocked_people_sync_time"

    .line 1295
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1294
    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1296
    const-string v3, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v11, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1298
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1300
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1303
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1304
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1305
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1306
    return-void
.end method

.method static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 22

    .prologue
    .line 4543
    const-class v1, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 4544
    move/from16 v0, p2

    invoke-interface {v1, v0}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    .line 4545
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4547
    const-string v2, "contacts"

    const-string v3, "in_my_circles=0  AND blocked=0 AND gaia_id!="

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "gaia_id"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "author_id"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "activities"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "gaia_id"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "author_id"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "activity_comments"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "gaia_id"

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "gaia_id"

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "circled_me_users"

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "gaia_id"

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "gaia_id"

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "event_people"

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "gaia_id"

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    const-string v17, "inviter_gaia_id"

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "squares"

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    add-int/lit16 v0, v0, 0x8c

    move/from16 v20, v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " NOT IN (SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " NOT IN (SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " NOT IN (SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " NOT IN (SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " NOT IN (SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4566
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4480
    const-string v0, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=circle_id) WHERE type=1"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4486
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 4452
    const-string v0, "UPDATE contacts SET in_my_circles=(EXISTS (SELECT 1 FROM circle_contact WHERE link_person_id=?)),last_updated_time=last_updated_time + 1 WHERE person_id=?"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4460
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Liqh;Z)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3147
    invoke-interface {p2}, Liqh;->h()[Ljava/lang/String;

    move-result-object v4

    .line 3148
    if-eqz v4, :cond_0

    array-length v2, v4

    move v3, v2

    .line 3150
    :goto_0
    if-eqz v3, :cond_4

    .line 3151
    add-int/lit8 v2, v3, 0x1

    new-array v5, v2, [Ljava/lang/String;

    .line 3152
    aput-object p1, v5, v0

    .line 3153
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 3155
    :goto_1
    if-ge v0, v3, :cond_1

    .line 3156
    const-string v2, "?,"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3157
    add-int/lit8 v2, v1, 0x1

    aget-object v7, v4, v0

    invoke-static {v7}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v1

    .line 3155
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_1

    :cond_0
    move v3, v0

    .line 3148
    goto :goto_0

    .line 3159
    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3161
    if-nez p3, :cond_2

    .line 3163
    const-string v0, "DELETE FROM circle_contact WHERE link_person_id=? AND link_circle_id NOT IN ("

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3166
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3163
    invoke-virtual {p0, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3171
    :cond_2
    const-string v0, "INSERT OR IGNORE INTO circle_contact(link_person_id,link_circle_id) SELECT ?, circle_id FROM circles WHERE circle_id IN ("

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3176
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3171
    invoke-virtual {p0, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3184
    :cond_3
    :goto_2
    const-string v0, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=circle_id) WHERE type=1"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3185
    return-void

    .line 3178
    :cond_4
    if-nez p3, :cond_3

    .line 3179
    const-string v2, "circle_contact"

    const-string v3, "link_person_id=?"

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v0

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ldsy;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 3227
    if-nez p3, :cond_0

    .line 3228
    const-string v0, "contact_search"

    const-string v1, "search_person_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3232
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3233
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsy;

    .line 3234
    const-string v3, "search_person_id"

    invoke-virtual {v1, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3235
    const-string v3, "search_key_type"

    iget v4, v0, Ldsy;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3236
    const-string v3, "search_key"

    iget-object v0, v0, Ldsy;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3237
    const-string v0, "contact_search"

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 3239
    :cond_1
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2519
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2534
    :cond_0
    return-void

    .line 2524
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 2525
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    .line 2526
    iget-object v3, v0, Ldqv;->c:Ljava/lang/String;

    iget-object v4, v0, Ldqv;->b:Ljava/lang/String;

    iget-object v5, v0, Ldqv;->d:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2528
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 2529
    const-string v3, "notification_key"

    invoke-virtual {v1, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2530
    const-string v3, "gaia_id"

    iget-object v0, v0, Ldqv;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531
    const-string v0, "circled_me_users"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {p0, v0, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lohv;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2831
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2833
    invoke-static {p2}, Ldsm;->b(Lohv;)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2832
    :goto_0
    iget-object v4, p2, Lohv;->b:Lohp;

    invoke-static {v4}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    iget-object v6, p2, Lohv;->c:Lohq;

    iget-object v6, v6, Lohq;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v3, v6}, Ldsm;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_0
    const-string v7, "name"

    invoke-virtual {v5, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "sort_key"

    iget-object v7, p2, Lohv;->c:Lohq;

    iget-object v7, v7, Lohq;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "interaction_sort_key"

    iget-object v7, p2, Lohv;->c:Lohq;

    iget-object v7, v7, Lohq;->d:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p2, Lohv;->c:Lohq;

    iget-object v6, v6, Lohq;->c:Ljava/lang/String;

    invoke-static {v6}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "avatar"

    invoke-virtual {v5, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "last_updated_time"

    invoke-static {p2}, Ldsm;->c(Lohv;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "in_my_circles"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "verified"

    iget-object v7, p2, Lohv;->c:Lohq;

    iget-object v7, v7, Lohq;->i:Ljava/lang/Boolean;

    invoke-static {v7}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v6, p2, Lohv;->c:Lohq;

    iget v6, v6, Lohq;->g:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_4

    const-string v6, "profile_type"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p2, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "profile_state"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    const-string v0, "contacts"

    const-string v6, "person_id=?"

    new-array v7, v1, [Ljava/lang/String;

    aput-object v4, v7, v2

    invoke-virtual {p0, v0, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    const-string v6, "in_same_visibility_group"

    iget-object v0, p2, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->f:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "verified"

    iget-object v0, p2, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->i:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "person_id"

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "gaia_id"

    invoke-static {v4}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "contacts"

    invoke-virtual {p0, v0, v11, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move v0, v1

    :goto_4
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    const-string v6, "contact_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "contact_proto"

    invoke-static {p2}, Ldsm;->a(Lohv;)Ldsv;

    move-result-object v7

    invoke-static {v7}, Ldsm;->a(Ldsv;)[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v6, "profiles"

    const-string v7, "profile_person_id=?"

    new-array v1, v1, [Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-virtual {p0, v6, v5, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "profile_person_id"

    invoke-virtual {v5, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "profiles"

    invoke-virtual {p0, v1, v11, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2835
    :cond_2
    invoke-static {p0, p1, p2, v0}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lohv;Z)V

    .line 2836
    invoke-static {p2, v3}, Ldsm;->a(Lohv;Ljava/util/ArrayList;)V

    .line 2837
    invoke-static {p0, p1, v3, v0}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    .line 2838
    return-void

    :cond_3
    move v0, v2

    .line 2833
    goto/16 :goto_0

    .line 2832
    :cond_4
    const-string v6, "profile_type"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lohv;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3099
    invoke-static {p2}, Ldsm;->b(Lohv;)I

    move-result v2

    .line 3101
    if-eqz v2, :cond_5

    .line 3102
    add-int/lit8 v2, v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    .line 3103
    aput-object p1, v3, v0

    .line 3104
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 3106
    :goto_0
    iget-object v2, p2, Lohv;->d:[Loij;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 3107
    iget-object v2, p2, Lohv;->d:[Loij;

    aget-object v5, v2, v0

    .line 3108
    iget-object v2, v5, Loij;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v2, v5, Loij;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3109
    :cond_0
    const-string v2, "?,"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3110
    add-int/lit8 v2, v1, 0x1

    iget-object v5, v5, Loij;->b:Lohn;

    iget-object v5, v5, Lohn;->b:Ljava/lang/String;

    invoke-static {v5}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    move v1, v2

    .line 3106
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3113
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3115
    if-nez p3, :cond_3

    .line 3117
    const-string v0, "DELETE FROM circle_contact WHERE link_person_id=? AND link_circle_id NOT IN ("

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3120
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3117
    invoke-virtual {p0, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3125
    :cond_3
    const-string v0, "INSERT OR IGNORE INTO circle_contact(link_person_id,link_circle_id) SELECT ?, circle_id FROM circles WHERE circle_id IN ("

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3130
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3125
    invoke-virtual {p0, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3138
    :cond_4
    :goto_1
    const-string v0, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=circle_id) WHERE type=1"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3139
    return-void

    .line 3132
    :cond_5
    if-nez p3, :cond_4

    .line 3133
    const-string v2, "circle_contact"

    const-string v3, "link_person_id=?"

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v0

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4466
    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    .line 4467
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    .line 4468
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 4469
    const/4 v5, 0x1

    aput-object v4, v2, v5

    aput-object v4, v2, v1

    .line 4470
    const-string v4, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=?) WHERE circle_id=?"

    invoke-virtual {p0, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4468
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4477
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldsy;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3070
    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3092
    :cond_0
    :goto_0
    return-void

    .line 3074
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 3076
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    move v1, v2

    move v0, v2

    .line 3077
    :goto_1
    if-ge v1, v4, :cond_4

    .line 3078
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 3079
    invoke-static {v5}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v5

    if-nez v5, :cond_3

    .line 3080
    if-le v1, v0, :cond_2

    .line 3081
    new-instance v5, Ldsy;

    .line 3082
    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v2, v0}, Ldsy;-><init>(ILjava/lang/String;)V

    .line 3081
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3084
    :cond_2
    add-int/lit8 v0, v1, 0x1

    .line 3077
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3088
    :cond_4
    if-le v4, v0, :cond_0

    .line 3089
    new-instance v1, Ldsy;

    .line 3090
    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ldsy;-><init>(ILjava/lang/String;)V

    .line 3089
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2538
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v3

    .line 2539
    :goto_0
    if-ge v5, v6, :cond_3

    .line 2540
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    .line 2541
    iget-object v1, v0, Ldvw;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, v0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    :goto_1
    move v4, v3

    .line 2542
    :goto_2
    if-ge v4, v2, :cond_2

    .line 2544
    iget-object v1, v0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnrp;

    .line 2545
    iget-object v7, v1, Lnrp;->b:Lohv;

    if-eqz v7, :cond_0

    iget-object v7, v1, Lnrp;->b:Lohv;

    iget-object v7, v7, Lohv;->b:Lohp;

    if-eqz v7, :cond_0

    .line 2546
    iget-object v1, v1, Lnrp;->b:Lohv;

    .line 2547
    invoke-static {v1, p1}, Ldsm;->a(Lohv;Ljava/util/HashMap;)V

    .line 2542
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_1
    move v2, v3

    .line 2541
    goto :goto_1

    .line 2539
    :cond_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 2551
    :cond_3
    return-void
.end method

.method private static a(Lohv;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lohv;",
            "Ljava/util/ArrayList",
            "<",
            "Ldsy;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3192
    iget-object v0, p0, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 3194
    :goto_0
    iget-object v3, p0, Lohv;->c:Lohq;

    iget-object v3, v3, Lohq;->p:[Loib;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lohv;->c:Lohq;

    iget-object v3, v3, Lohq;->p:[Loib;

    array-length v3, v3

    if-lez v3, :cond_4

    move v3, v1

    .line 3196
    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_5

    .line 3197
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3198
    if-eqz v0, :cond_1

    .line 3199
    iget-object v0, p0, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->b:Ljava/lang/String;

    invoke-static {v0}, Ldsm;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3200
    if-eqz v0, :cond_1

    .line 3201
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3202
    new-instance v5, Ldsy;

    invoke-direct {v5, v1, v0}, Ldsy;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3207
    :cond_1
    if-eqz v3, :cond_5

    .line 3208
    iget-object v0, p0, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->p:[Loib;

    array-length v3, v0

    :goto_2
    if-ge v2, v3, :cond_5

    aget-object v5, v0, v2

    .line 3209
    iget-object v6, v5, Loib;->d:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 3210
    iget-object v5, v5, Loib;->d:Ljava/lang/String;

    invoke-static {v5}, Ldsm;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3211
    if-eqz v5, :cond_2

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 3212
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3213
    new-instance v6, Ldsy;

    invoke-direct {v6, v1, v5}, Ldsy;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3208
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move v0, v2

    .line 3192
    goto :goto_0

    :cond_4
    move v3, v2

    .line 3194
    goto :goto_1

    .line 3220
    :cond_5
    return-void
.end method

.method public static a(Lohv;Ljava/util/HashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lohv;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2574
    iget-object v0, p0, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->b(Lohp;)Ljava/lang/String;

    move-result-object v0

    .line 2575
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2576
    if-eqz v0, :cond_2

    .line 2577
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2578
    const/4 v1, 0x0

    .line 2579
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2580
    const/16 v2, 0x7c

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 2581
    const/4 v4, -0x1

    if-ne v2, v4, :cond_0

    .line 2582
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 2584
    :cond_0
    new-instance v4, Loij;

    invoke-direct {v4}, Loij;-><init>()V

    .line 2585
    new-instance v5, Lohn;

    invoke-direct {v5}, Lohn;-><init>()V

    iput-object v5, v4, Loij;->b:Lohn;

    .line 2586
    iget-object v5, v4, Loij;->b:Lohn;

    .line 2587
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lohn;->c:Ljava/lang/String;

    .line 2588
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2589
    add-int/lit8 v1, v2, 0x1

    .line 2590
    goto :goto_0

    .line 2591
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Loij;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Loij;

    iput-object v0, p0, Lohv;->d:[Loij;

    .line 2595
    :goto_1
    return-void

    .line 2593
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lohv;->d:[Loij;

    goto :goto_1
.end method

.method public static a([Lohv;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lohv;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2566
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 2567
    aget-object v1, p0, v0

    .line 2568
    invoke-static {v1, p1}, Ldsm;->a(Lohv;Ljava/util/HashMap;)V

    .line 2566
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2570
    :cond_0
    return-void
.end method

.method public static a([Loiu;Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Loiu;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2555
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 2556
    aget-object v1, p0, v0

    .line 2557
    iget-object v2, v1, Loiu;->b:Lohv;

    if-eqz v2, :cond_0

    iget-object v2, v1, Loiu;->b:Lohv;

    iget-object v2, v2, Lohv;->b:Lohp;

    if-eqz v2, :cond_0

    .line 2558
    iget-object v1, v1, Loiu;->b:Lohv;

    .line 2559
    invoke-static {v1, p1}, Ldsm;->a(Lohv;Ljava/util/HashMap;)V

    .line 2555
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2562
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 490
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 491
    const-class v1, Lieh;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lieh;

    .line 492
    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v4

    .line 493
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    .line 494
    :goto_0
    if-ge v3, v5, :cond_1

    .line 495
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 496
    sget-object v6, Ldxd;->q:Lief;

    invoke-interface {v1, v6, v0}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    const/4 v0, 0x1

    .line 500
    :goto_1
    return v0

    .line 494
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 500
    goto :goto_1
.end method

.method private static a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lnjt;Lmcf;Lmdp;Lnlp;Lnum;Lnum;Lnkm;Lnts;Z)Z
    .locals 19

    .prologue
    .line 3372
    move-object/from16 v0, p4

    iget-object v4, v0, Lnjt;->c:Lnia;

    if-eqz v4, :cond_0

    move-object/from16 v0, p4

    iget-object v4, v0, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->a:Lnkk;

    if-eqz v4, :cond_0

    move-object/from16 v0, p4

    iget-object v4, v0, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->a:Lnkk;

    iget v4, v4, Lnkk;->a:I

    packed-switch v4, :pswitch_data_0

    :cond_0
    const/4 v4, 0x0

    move v12, v4

    .line 3373
    :goto_0
    move-object/from16 v0, p4

    iget-object v0, v0, Lnjt;->g:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 3374
    move-object/from16 v0, p4

    iget v4, v0, Lnjt;->b:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_10

    const/4 v4, 0x2

    move v13, v4

    .line 3376
    :goto_1
    const/16 v17, 0x1

    .line 3377
    const/16 v16, -0x1

    .line 3378
    const/4 v15, -0x1

    .line 3379
    const/4 v14, 0x0

    .line 3381
    const-string v5, "contacts"

    sget-object v6, Ldsm;->d:[Ljava/lang/String;

    const-string v7, "person_id=?"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p3, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3384
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 3385
    const/4 v4, 0x0

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 3386
    const/4 v4, 0x1

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 3387
    const/4 v5, 0x2

    invoke-interface {v8, v5}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    .line 3388
    const/4 v7, 0x0

    .line 3391
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3394
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 3396
    const/4 v8, 0x0

    .line 3397
    if-ne v6, v12, :cond_1

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-eq v5, v13, :cond_1b

    .line 3399
    :cond_1
    const/4 v5, 0x1

    .line 3402
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 3403
    :goto_3
    const-string v6, "name"

    invoke-virtual {v15, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3404
    const-string v4, "profile_state"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v15, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3405
    const-string v4, "profile_type"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v15, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3407
    if-eqz v7, :cond_12

    .line 3408
    const-string v4, "person_id"

    move-object/from16 v0, p3

    invoke-virtual {v15, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3409
    const-string v4, "gaia_id"

    invoke-static/range {p3 .. p3}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3411
    const-string v4, "contacts"

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v6, v15}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move v12, v5

    .line 3418
    :goto_4
    const/4 v14, 0x1

    .line 3419
    const/4 v13, 0x0

    .line 3420
    const-string v5, "profiles"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "profile_proto"

    aput-object v7, v6, v4

    const-string v7, "profile_person_id=?"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p3, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3424
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 3425
    const/4 v5, 0x0

    .line 3426
    const/4 v4, 0x0

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 3429
    :goto_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3432
    if-eqz p4, :cond_3

    move-object/from16 v0, p4

    iget-object v6, v0, Lnjt;->c:Lnia;

    if-eqz v6, :cond_2

    move-object/from16 v0, p4

    iget-object v6, v0, Lnjt;->c:Lnia;

    iget-object v6, v6, Lnia;->e:Lnju;

    if-eqz v6, :cond_2

    move-object/from16 v0, p4

    iget-object v6, v0, Lnjt;->c:Lnia;

    iget-object v6, v6, Lnia;->e:Lnju;

    iget-object v6, v6, Lnju;->a:Lohv;

    if-eqz v6, :cond_2

    move-object/from16 v0, p4

    iget-object v6, v0, Lnjt;->c:Lnia;

    iget-object v6, v6, Lnia;->e:Lnju;

    iget-object v6, v6, Lnju;->a:Lohv;

    iget-object v6, v6, Lohv;->c:Lohq;

    if-eqz v6, :cond_2

    move-object/from16 v0, p4

    iget-object v6, v0, Lnjt;->c:Lnia;

    iget-object v6, v6, Lnia;->e:Lnju;

    iget-object v6, v6, Lnju;->a:Lohv;

    iget-object v6, v6, Lohv;->c:Lohq;

    const/4 v7, 0x0

    iput-object v7, v6, Lohq;->e:Ljava/lang/String;

    :cond_2
    move-object/from16 v0, p4

    iget-object v6, v0, Lnjt;->f:Lnjg;

    if-eqz v6, :cond_3

    move-object/from16 v0, p4

    iget-object v6, v0, Lnjt;->f:Lnjg;

    iget-object v6, v6, Lnjg;->c:Loae;

    if-eqz v6, :cond_3

    move-object/from16 v0, p4

    iget-object v6, v0, Lnjt;->f:Lnjg;

    iget-object v6, v6, Lnjg;->c:Loae;

    const/4 v7, 0x0

    iput-object v7, v6, Loae;->d:Ljava/lang/String;

    .line 3433
    :cond_3
    if-nez p4, :cond_13

    const/4 v6, 0x0

    .line 3435
    :goto_6
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 3436
    if-eqz p12, :cond_4

    .line 3437
    const-string v7, "profile_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v15, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3439
    :cond_4
    if-eqz p5, :cond_5

    .line 3440
    invoke-static/range {p5 .. p5}, Ldsm;->a(Lmcf;)[B

    move-result-object v7

    .line 3441
    const-string v8, "people_data_proto"

    invoke-virtual {v15, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3443
    :cond_5
    if-eqz p6, :cond_6

    .line 3444
    invoke-static/range {p6 .. p6}, Loxu;->a(Loxu;)[B

    move-result-object v7

    .line 3445
    const-string v8, "videos_data_proto"

    invoke-virtual {v15, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3447
    :cond_6
    if-eqz p7, :cond_7

    .line 3448
    invoke-static/range {p7 .. p7}, Ldsm;->a(Lnlp;)[B

    move-result-object v7

    .line 3449
    const-string v8, "reviews_data_proto"

    invoke-virtual {v15, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3451
    :cond_7
    if-eqz p8, :cond_8

    .line 3452
    invoke-static/range {p8 .. p8}, Loxu;->a(Loxu;)[B

    move-result-object v7

    .line 3453
    const-string v8, "local_reviews_data_proto"

    invoke-virtual {v15, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3455
    :cond_8
    if-eqz p9, :cond_9

    .line 3456
    invoke-static/range {p9 .. p9}, Loxu;->a(Loxu;)[B

    move-result-object v7

    .line 3457
    const-string v8, "self_local_reviews_data_proto"

    invoke-virtual {v15, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3459
    :cond_9
    if-eqz p10, :cond_a

    .line 3460
    if-nez p10, :cond_14

    const/4 v7, 0x0

    .line 3461
    :goto_7
    const-string v8, "profile_stats_proto"

    invoke-virtual {v15, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3463
    :cond_a
    if-eqz p11, :cond_b

    .line 3464
    if-nez p11, :cond_15

    const/4 v7, 0x0

    .line 3466
    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;)[B

    move-result-object v8

    .line 3465
    invoke-static {v7, v8}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v8

    if-nez v8, :cond_16

    const/4 v8, 0x1

    :goto_9
    or-int/2addr v12, v8

    .line 3467
    const-string v8, "profile_squares_proto"

    invoke-virtual {v15, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_b
    move v7, v12

    .line 3469
    if-eqz v5, :cond_17

    .line 3470
    const-string v4, "profile_person_id"

    move-object/from16 v0, p3

    invoke-virtual {v15, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3471
    const-string v4, "profile_proto"

    invoke-virtual {v15, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3472
    const-string v4, "profiles"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5, v15}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 3473
    const/4 v4, 0x1

    move v10, v4

    .line 3485
    :goto_a
    const-class v4, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    .line 3486
    move/from16 v0, p1

    invoke-interface {v4, v0}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v5, "gaia_id"

    .line 3487
    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3488
    if-eqz v10, :cond_f

    invoke-static/range {p3 .. p3}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 3489
    const/4 v4, 0x0

    .line 3490
    move-object/from16 v0, p4

    iget-object v5, v0, Lnjt;->d:Lnib;

    if-eqz v5, :cond_d

    move-object/from16 v0, p4

    iget-object v5, v0, Lnjt;->d:Lnib;

    iget-object v5, v5, Lnib;->i:Lnjm;

    if-eqz v5, :cond_d

    move-object/from16 v0, p4

    iget-object v5, v0, Lnjt;->d:Lnib;

    iget-object v5, v5, Lnib;->i:Lnjm;

    iget-object v5, v5, Lnjm;->b:Lnjn;

    if-eqz v5, :cond_d

    move-object/from16 v0, p4

    iget-object v5, v0, Lnjt;->d:Lnib;

    iget-object v5, v5, Lnib;->i:Lnjm;

    iget-object v5, v5, Lnjm;->b:Lnjn;

    iget-object v5, v5, Lnjn;->b:Ljava/lang/String;

    if-eqz v5, :cond_d

    move-object/from16 v0, p4

    iget-object v5, v0, Lnjt;->d:Lnib;

    iget-object v5, v5, Lnib;->h:Lnjo;

    if-eqz v5, :cond_d

    move-object/from16 v0, p4

    iget-object v5, v0, Lnjt;->d:Lnib;

    iget-object v5, v5, Lnib;->h:Lnjo;

    iget-object v5, v5, Lnjo;->b:Lnjq;

    if-eqz v5, :cond_d

    move-object/from16 v0, p4

    iget-object v5, v0, Lnjt;->d:Lnib;

    iget-object v5, v5, Lnib;->h:Lnjo;

    iget-object v5, v5, Lnjo;->b:Lnjq;

    iget-object v5, v5, Lnjq;->d:Lnjp;

    if-eqz v5, :cond_d

    .line 3497
    move-object/from16 v0, p4

    iget-object v4, v0, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    iget-object v8, v4, Lnjq;->d:Lnjp;

    .line 3499
    const/4 v9, 0x0

    .line 3500
    move-object/from16 v0, p4

    iget-object v4, v0, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    iget-object v4, v4, Lnjq;->e:Ljava/lang/Integer;

    if-eqz v4, :cond_c

    .line 3501
    move-object/from16 v0, p4

    iget-object v4, v0, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->h:Lnjo;

    iget-object v4, v4, Lnjo;->b:Lnjq;

    iget-object v4, v4, Lnjq;->e:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 3503
    :cond_c
    move-object/from16 v0, p4

    iget-object v4, v0, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->i:Lnjm;

    iget-object v4, v4, Lnjm;->b:Lnjn;

    iget-object v4, v4, Lnjn;->c:Ljava/lang/String;

    iget-object v5, v8, Lnjp;->a:Ljava/lang/Float;

    .line 3505
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iget-object v6, v8, Lnjp;->d:Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget-object v7, v8, Lnjp;->c:Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iget-object v8, v8, Lnjp;->b:Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 3503
    invoke-static/range {v4 .. v9}, Ldqt;->a(Ljava/lang/String;FFFFI)[B

    move-result-object v4

    .line 3508
    :cond_d
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 3509
    const-string v5, "cover_photo_spec"

    invoke-virtual {v15, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3510
    const-string v4, "account_status"

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v15, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3512
    const/4 v4, 0x0

    .line 3513
    move-object/from16 v0, p4

    iget-object v5, v0, Lnjt;->d:Lnib;

    if-eqz v5, :cond_e

    .line 3514
    move-object/from16 v0, p4

    iget-object v4, v0, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->f:Ljava/lang/String;

    invoke-static {v4}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3516
    :cond_e
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 3517
    const-string v5, "avatar"

    invoke-static {v4}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3518
    const-string v5, "contacts"

    const-string v6, "person_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p3, v7, v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v15, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3520
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v4}, Ldhv;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 3522
    :cond_f
    return v10

    .line 3372
    :pswitch_0
    const/4 v4, 0x3

    move v12, v4

    goto/16 :goto_0

    :pswitch_1
    const/4 v4, 0x4

    move v12, v4

    goto/16 :goto_0

    :pswitch_2
    const/4 v4, 0x6

    move v12, v4

    goto/16 :goto_0

    :pswitch_3
    const/4 v4, 0x5

    move v12, v4

    goto/16 :goto_0

    .line 3374
    :cond_10
    const/4 v4, 0x1

    move v13, v4

    goto/16 :goto_1

    .line 3391
    :catchall_0
    move-exception v4

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_11
    move-object/from16 v4, v18

    .line 3402
    goto/16 :goto_3

    .line 3413
    :cond_12
    const-string v4, "contacts"

    const-string v6, "person_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p3, v7, v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v15, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move v12, v5

    goto/16 :goto_4

    .line 3429
    :catchall_1
    move-exception v4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v4

    .line 3433
    :cond_13
    invoke-static/range {p4 .. p4}, Loxu;->a(Loxu;)[B

    move-result-object v6

    goto/16 :goto_6

    .line 3460
    :cond_14
    invoke-static/range {p10 .. p10}, Loxu;->a(Loxu;)[B

    move-result-object v7

    goto/16 :goto_7

    .line 3464
    :cond_15
    invoke-static/range {p11 .. p11}, Loxu;->a(Loxu;)[B

    move-result-object v7

    goto/16 :goto_8

    .line 3465
    :cond_16
    const/4 v8, 0x0

    goto/16 :goto_9

    .line 3475
    :cond_17
    invoke-static {v4, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_19

    .line 3476
    const-string v4, "profile_proto"

    invoke-virtual {v15, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3477
    const/4 v4, 0x1

    .line 3479
    :goto_b
    invoke-virtual {v15}, Landroid/content/ContentValues;->size()I

    move-result v5

    if-lez v5, :cond_18

    .line 3480
    const-string v5, "profiles"

    const-string v6, "profile_person_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p3, v7, v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v15, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_18
    move v10, v4

    goto/16 :goto_a

    :cond_19
    move v4, v7

    goto :goto_b

    :cond_1a
    move-object v4, v13

    move v5, v14

    goto/16 :goto_5

    :cond_1b
    move v12, v8

    goto/16 :goto_4

    :cond_1c
    move-object v4, v14

    move v5, v15

    move/from16 v6, v16

    move/from16 v7, v17

    goto/16 :goto_2

    .line 3372
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;ILkfp;)Z
    .locals 13

    .prologue
    .line 1727
    invoke-virtual {p2}, Lkfp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1728
    const/4 v0, 0x0

    .line 1836
    :goto_0
    return v0

    .line 1731
    :cond_0
    invoke-static {p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1732
    invoke-static {p0, p1}, Ldsm;->m(Landroid/content/Context;I)V

    .line 1733
    const/4 v0, 0x1

    goto :goto_0

    .line 1736
    :cond_1
    const-string v0, "SocialNetwork"

    invoke-virtual {p2, v0}, Lkfp;->c(Ljava/lang/String;)V

    .line 1738
    const/4 v10, 0x0

    .line 1739
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 1740
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 1741
    invoke-static {p0, p1}, Ldsm;->k(Landroid/content/Context;I)[B

    move-result-object v7

    .line 1742
    const/4 v9, 0x0

    .line 1745
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1747
    const/4 v0, 0x0

    .line 1755
    sget-object v1, Lfvc;->g:Lfvc;

    invoke-virtual {v1}, Lfvc;->b()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1756
    const/4 v0, 0x1

    move v8, v0

    .line 1759
    :goto_1
    if-nez v8, :cond_e

    .line 1760
    new-instance v0, Ldky;

    new-instance v2, Lkfo;

    invoke-direct {v2, p0, p1, p2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/16 v6, 0x12c

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Ldky;-><init>(Landroid/content/Context;Lkfo;ZZZI[B)V

    .line 1764
    invoke-virtual {v0}, Ldky;->l()V

    .line 1765
    invoke-virtual {v0}, Ldky;->t()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1766
    const-string v1, "EsPeopleData"

    invoke-virtual {v0, v1}, Ldky;->d(Ljava/lang/String;)V

    .line 1767
    const/4 v0, 0x0

    goto :goto_0

    .line 1770
    :cond_2
    invoke-virtual {v0}, Ldky;->d()Loik;

    move-result-object v3

    .line 1771
    const/4 v0, 0x0

    .line 1773
    :try_start_0
    iget-object v0, v3, Loik;->b:[I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    .line 1780
    :goto_2
    if-eqz v2, :cond_6

    .line 1781
    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    move v0, v8

    :goto_3
    if-ge v1, v4, :cond_7

    aget v5, v2, v1

    .line 1782
    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 1783
    const/4 v0, 0x1

    .line 1781
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1774
    :catch_0
    move-exception v1

    .line 1775
    const-string v2, "EsPeopleData"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1776
    const-string v2, "personList is null: "

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-object v2, v0

    goto :goto_2

    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :cond_5
    move-object v2, v0

    goto :goto_2

    :cond_6
    move v0, v8

    .line 1788
    :cond_7
    iget-object v1, v3, Loik;->c:Loiz;

    if-eqz v1, :cond_8

    .line 1789
    iget-object v1, v3, Loik;->c:Loiz;

    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v7

    .line 1792
    :cond_8
    if-nez v7, :cond_9

    .line 1793
    const/4 v0, 0x1

    .line 1796
    :cond_9
    iget-object v1, v3, Loik;->a:[Lohv;

    if-eqz v1, :cond_10

    .line 1797
    iget-object v4, v3, Loik;->a:[Lohv;

    array-length v5, v4

    const/4 v1, 0x0

    move v3, v1

    move v2, v10

    move v1, v9

    :goto_4
    if-ge v3, v5, :cond_c

    aget-object v6, v4, v3

    .line 1801
    iget-object v8, v6, Lohv;->b:Lohp;

    invoke-static {v8}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v8

    .line 1802
    if-eqz v8, :cond_b

    .line 1803
    invoke-static {v6}, Ldsm;->b(Lohv;)I

    move-result v8

    if-eqz v8, :cond_a

    .line 1807
    const/4 v1, 0x1

    .line 1808
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1811
    :cond_a
    add-int/lit8 v2, v2, 0x1

    .line 1800
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1814
    :cond_c
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v4, 0x7d0

    if-le v3, v4, :cond_d

    .line 1815
    invoke-static {p0, p1, v12}, Ldsm;->a(Landroid/content/Context;ILjava/util/List;)V

    .line 1816
    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    :cond_d
    move v8, v0

    move v9, v1

    move v10, v2

    .line 1818
    goto/16 :goto_1

    .line 1821
    :cond_e
    invoke-static {p0, p1, v12}, Ldsm;->a(Landroid/content/Context;ILjava/util/List;)V

    .line 1823
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1824
    const-string v1, "people_last_update_token"

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1825
    const-string v1, "account_status"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v11, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1827
    invoke-virtual {p2, v10}, Lkfp;->m(I)V

    .line 1829
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1830
    if-eqz v9, :cond_f

    .line 1831
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1832
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1834
    :cond_f
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1836
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_10
    move v8, v0

    goto/16 :goto_1
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2845
    const-string v1, "contacts"

    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "last_updated_time"

    aput-object v0, v2, v9

    const-string v3, "person_id=?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object p1, v4, v9

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2850
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2851
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v2, p2

    if-eqz v0, :cond_0

    move v0, v8

    .line 2854
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2857
    :goto_1
    return v0

    :cond_0
    move v0, v9

    .line 2851
    goto :goto_0

    .line 2854
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 2857
    goto :goto_1

    .line 2854
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v1, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 2482
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2483
    :cond_0
    const-string v0, "EsPeopleData"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x37

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> Person id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; *** Skip. No gaia id or name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    move v0, v8

    .line 2506
    :goto_0
    return v0

    .line 2492
    :cond_2
    const-string v1, "contacts"

    sget-object v2, Ljqp;->a:[Ljava/lang/String;

    const-string v3, "gaia_id = ?"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v8

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2496
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2497
    new-instance v5, Ljqq;

    invoke-direct {v5}, Ljqq;-><init>()V

    .line 2498
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Ljqq;->a:Ljava/lang/String;

    .line 2499
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Ljqq;->b:Ljava/lang/String;

    .line 2500
    const/4 v0, 0x3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v8, v9

    :cond_3
    iput-boolean v8, v5, Ljqq;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2503
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2506
    invoke-static {p0, p1, p2, p3, v5}, Ljqp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljqq;)Z

    move-result v0

    goto :goto_0

    .line 2503
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)[B
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 4036
    .line 4037
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4038
    const-string v1, "profiles"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "profile_squares_proto"

    aput-object v3, v2, v8

    const-string v3, "profile_person_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v8

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 4041
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4042
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 4044
    :cond_0
    return-object v5
.end method

.method private static a(Ldsv;)[B
    .locals 2

    .prologue
    .line 3913
    if-nez p0, :cond_0

    .line 3914
    const/4 v0, 0x0

    .line 3920
    :goto_0
    return-object v0

    .line 3916
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3917
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Ldsv;->writeToParcel(Landroid/os/Parcel;I)V

    .line 3918
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    .line 3919
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0
.end method

.method private static a(Lmcf;)[B
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 3720
    iget-object v0, p0, Lmcf;->a:Lnlm;

    .line 3722
    iget-object v2, p0, Lmcf;->apiHeader:Llyr;

    if-eqz v2, :cond_0

    .line 3723
    iget-object v2, p0, Lmcf;->apiHeader:Llyr;

    iput-object v6, v2, Llyr;->a:Lpwg;

    .line 3724
    iget-object v2, p0, Lmcf;->apiHeader:Llyr;

    iput-object v6, v2, Llyr;->b:Ljava/lang/String;

    .line 3726
    :cond_0
    iget-object v2, v0, Lnlm;->a:Lofz;

    .line 3727
    if-eqz v2, :cond_3

    .line 3729
    iget-object v0, v2, Lofz;->c:Loez;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lofz;->c:Loez;

    iget-object v0, v0, Loez;->b:[Loey;

    if-eqz v0, :cond_1

    .line 3731
    iget-object v0, v2, Lofz;->c:Loez;

    iget-object v3, v0, Loez;->b:[Loey;

    move v0, v1

    .line 3732
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 3733
    aget-object v4, v3, v0

    .line 3734
    iput-object v6, v4, Loey;->e:Ljava/lang/String;

    .line 3735
    iput-object v6, v4, Loey;->f:Ljava/lang/Boolean;

    .line 3736
    iput-object v6, v4, Loey;->c:Ljava/lang/String;

    .line 3737
    iget-object v5, v4, Loey;->d:Ljava/lang/String;

    invoke-static {v5}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Loey;->d:Ljava/lang/String;

    .line 3732
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3740
    :cond_1
    iget-object v0, v2, Lofz;->b:Loez;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lofz;->b:Loez;

    iget-object v0, v0, Loez;->b:[Loey;

    if-eqz v0, :cond_2

    .line 3742
    iget-object v0, v2, Lofz;->b:Loez;

    iget-object v3, v0, Loez;->b:[Loey;

    move v0, v1

    .line 3743
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_2

    .line 3744
    aget-object v4, v3, v0

    .line 3745
    iput-object v6, v4, Loey;->e:Ljava/lang/String;

    .line 3746
    iput-object v6, v4, Loey;->f:Ljava/lang/Boolean;

    .line 3747
    iput-object v6, v4, Loey;->c:Ljava/lang/String;

    .line 3748
    iget-object v5, v4, Loey;->d:Ljava/lang/String;

    invoke-static {v5}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Loey;->d:Ljava/lang/String;

    .line 3743
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3751
    :cond_2
    iget-object v0, v2, Lofz;->a:Loez;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lofz;->a:Loez;

    iget-object v0, v0, Loez;->b:[Loey;

    if-eqz v0, :cond_3

    .line 3753
    iget-object v0, v2, Lofz;->a:Loez;

    iget-object v0, v0, Loez;->b:[Loey;

    .line 3754
    :goto_2
    array-length v2, v0

    if-ge v1, v2, :cond_3

    .line 3755
    aget-object v2, v0, v1

    .line 3756
    iput-object v6, v2, Loey;->e:Ljava/lang/String;

    .line 3757
    iput-object v6, v2, Loey;->f:Ljava/lang/Boolean;

    .line 3758
    iput-object v6, v2, Loey;->c:Ljava/lang/String;

    .line 3759
    iget-object v3, v2, Loey;->d:Ljava/lang/String;

    invoke-static {v3}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Loey;->d:Ljava/lang/String;

    .line 3754
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3764
    :cond_3
    invoke-static {p0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lnjt;)[B
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 4772
    if-nez p0, :cond_0

    move-object v0, v2

    .line 4773
    :goto_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4774
    invoke-virtual {v0, v2, v1}, Lent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 4775
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    .line 4776
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4777
    return-object v0

    .line 4772
    :cond_0
    new-instance v3, Lent;

    iget v0, p0, Lnjt;->b:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v3, v2, v2, v2, v0}, Lent;-><init>(Lnjb;Lnjc;Lnjd;Z)V

    iget-boolean v0, v3, Lent;->d:Z

    if-eqz v0, :cond_3

    new-instance v0, Lnjb;

    invoke-direct {v0}, Lnjb;-><init>()V

    iput-object v0, v3, Lent;->a:Lnjb;

    iget-object v0, v3, Lent;->a:Lnjb;

    iget-object v2, p0, Lnjt;->g:Ljava/lang/String;

    iput-object v2, v0, Lnjb;->c:Ljava/lang/String;

    :cond_1
    :goto_2
    move-object v0, v3

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->a:Lnjb;

    if-nez v0, :cond_5

    :cond_4
    move-object v0, v2

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->a:Lnjb;

    iput-object v0, v3, Lent;->a:Lnjb;

    iget-object v0, p0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->c:Lnjc;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->c:Lnjc;

    iput-object v0, v3, Lent;->b:Lnjc;

    :cond_6
    iget-object v0, p0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->b:Lnjd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->b:Lnjd;

    iput-object v0, v3, Lent;->c:Lnjd;

    goto :goto_2
.end method

.method private static a(Lnlp;)[B
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x0

    .line 3833
    iget-object v0, p0, Lnlp;->a:Lpvl;

    if-eqz v0, :cond_6

    .line 3834
    iget-object v0, p0, Lnlp;->a:Lpvl;

    iget-object v0, v0, Lpvl;->b:[Lpvt;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lnlp;->a:Lpvl;

    iget-object v0, v0, Lpvl;->b:[Lpvt;

    array-length v0, v0

    if-eqz v0, :cond_6

    .line 3836
    iget-object v0, p0, Lnlp;->a:Lpvl;

    iget-object v0, v0, Lpvl;->b:[Lpvt;

    aget-object v0, v0, v3

    .line 3837
    iput-object v12, v0, Lpvt;->f:Lpvs;

    .line 3838
    iput-object v12, v0, Lpvt;->b:Ljava/lang/String;

    .line 3839
    iput-object v12, v0, Lpvt;->c:Lpgx;

    .line 3840
    iput-object v12, v0, Lpvt;->g:Ljava/lang/String;

    .line 3841
    iget-object v1, v0, Lpvt;->d:[Lpvi;

    if-eqz v1, :cond_6

    iget-object v1, v0, Lpvt;->d:[Lpvi;

    array-length v1, v1

    if-eqz v1, :cond_6

    .line 3842
    iget-object v5, v0, Lpvt;->d:[Lpvi;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_6

    aget-object v7, v5, v4

    .line 3843
    iput-object v12, v7, Lpvi;->b:Ljava/lang/String;

    .line 3844
    iput-object v12, v7, Lpvi;->c:Lpgx;

    .line 3845
    iput-object v12, v7, Lpvi;->f:Ljava/lang/String;

    .line 3846
    iget-object v0, v7, Lpvi;->e:[Lpvf;

    if-eqz v0, :cond_2

    iget-object v0, v7, Lpvi;->e:[Lpvf;

    array-length v0, v0

    if-eqz v0, :cond_2

    .line 3847
    iget-object v8, v7, Lpvi;->e:[Lpvf;

    array-length v9, v8

    move v2, v3

    :goto_1
    if-ge v2, v9, :cond_2

    aget-object v10, v8, v2

    .line 3848
    sget-object v0, Lpvd;->a:Loxr;

    invoke-virtual {v10, v0}, Lpvf;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lply;

    .line 3849
    sget-object v1, Lpvg;->a:Loxr;

    .line 3850
    invoke-virtual {v10, v1}, Lpvf;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpmx;

    .line 3851
    if-eqz v0, :cond_0

    .line 3852
    iput-object v12, v0, Lply;->a:Lpgx;

    .line 3853
    sget-object v11, Lpvd;->a:Loxr;

    invoke-virtual {v10, v11, v0}, Lpvf;->a(Loxr;Ljava/lang/Object;)V

    .line 3855
    :cond_0
    if-eqz v1, :cond_1

    .line 3856
    iput-object v12, v1, Lpmx;->b:Lpgx;

    .line 3857
    iput-object v12, v1, Lpmx;->h:Ljava/lang/String;

    .line 3858
    sget-object v0, Lpvg;->a:Loxr;

    invoke-virtual {v10, v0, v1}, Lpvf;->a(Loxr;Ljava/lang/Object;)V

    .line 3847
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 3862
    :cond_2
    iget-object v0, v7, Lpvi;->d:Lpmm;

    if-eqz v0, :cond_5

    .line 3863
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->b:Lpge;

    if-eqz v0, :cond_3

    .line 3864
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->b:Lpge;

    sget-object v1, Loxx;->d:[Ljava/lang/String;

    iput-object v1, v0, Lpge;->c:[Ljava/lang/String;

    .line 3866
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->b:Lpge;

    iput-object v12, v0, Lpge;->b:Ljava/lang/String;

    .line 3868
    :cond_3
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iput-object v12, v0, Lpmm;->g:Lphv;

    .line 3869
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iput-object v12, v0, Lpmm;->c:Lpge;

    .line 3870
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iput-object v12, v0, Lpmm;->q:Lplw;

    .line 3871
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    if-eqz v0, :cond_4

    .line 3872
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->f:Ljava/lang/String;

    .line 3873
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->j:Ljava/lang/String;

    .line 3874
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->h:Ljava/lang/Boolean;

    .line 3875
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->a:Ljava/lang/Integer;

    .line 3876
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->c:Lpow;

    .line 3877
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->b:Ljava/lang/Integer;

    .line 3878
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->g:Ljava/lang/String;

    .line 3879
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->d:Ljava/lang/String;

    .line 3880
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iput-object v12, v0, Lpqx;->e:Ljava/lang/Integer;

    .line 3882
    :cond_4
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iput-object v12, v0, Lpmm;->l:Lpui;

    .line 3883
    iget-object v0, v7, Lpvi;->d:Lpmm;

    iput-object v12, v0, Lpmm;->m:Lpur;

    .line 3842
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 3889
    :cond_6
    invoke-static {p0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    return-object v0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 4813
    packed-switch p0, :pswitch_data_0

    .line 4821
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 4815
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 4817
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 4819
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 4813
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Lohv;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4784
    iget-object v1, p0, Lohv;->d:[Loij;

    if-nez v1, :cond_0

    .line 4795
    :goto_0
    return v0

    :cond_0
    move v1, v0

    .line 4789
    :goto_1
    iget-object v2, p0, Lohv;->d:[Loij;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4790
    iget-object v2, p0, Lohv;->d:[Loij;

    aget-object v2, v2, v0

    .line 4791
    iget-object v2, v2, Loij;->c:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4792
    add-int/lit8 v1, v1, 0x1

    .line 4789
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 4795
    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 759
    if-eqz p0, :cond_0

    .line 760
    const-string v0, "(circle_id = "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 761
    invoke-static {p0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 763
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lohp;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 4694
    if-nez p0, :cond_1

    .line 4702
    :cond_0
    :goto_0
    return-object v0

    .line 4698
    :cond_1
    iget-object v1, p0, Lohp;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4699
    iget-object v0, p0, Lohp;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashSet;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1702
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1703
    const-string v1, "contacts"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "person_id"

    aput-object v0, v2, v3

    const-string v3, "in_my_circles!=0"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1708
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1709
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1712
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1715
    return-object v8
.end method

.method private static b([B)Lnjt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3648
    if-nez p0, :cond_0

    move-object v0, v1

    .line 3657
    :goto_0
    return-object v0

    .line 3653
    :cond_0
    :try_start_0
    new-instance v0, Lnjt;

    invoke-direct {v0}, Lnjt;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjt;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3654
    :catch_0
    move-exception v0

    .line 3656
    const-string v2, "EsPeopleData"

    const-string v3, "Failed to create SimpleProfile from."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 3657
    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5316
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 5317
    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v1

    .line 5319
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 5320
    invoke-interface {v0, v5}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 5322
    const-string v6, "is_plus_page"

    invoke-interface {v1, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 5323
    invoke-static {p0, v5}, Ldhv;->j(Landroid/content/Context;I)Z

    move-result v6

    .line 5326
    invoke-interface {v1}, Lhej;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v6, :cond_1

    move v1, v2

    .line 5327
    :goto_1
    new-array v6, v2, [Ljava/lang/String;

    const-string v7, "$$mycircles$$"

    aput-object v7, v6, v3

    .line 5330
    new-instance v7, Ljql;

    invoke-direct {v7, p0, v5, v1, v6}, Ljql;-><init>(Landroid/content/Context;IZ[Ljava/lang/String;)V

    .line 5332
    invoke-static {v7}, Lhoc;->a(Lhny;)Lhoz;

    goto :goto_0

    :cond_1
    move v1, v3

    .line 5326
    goto :goto_1

    .line 5334
    :cond_2
    return-void
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 4057
    invoke-static {p2}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4058
    if-nez v0, :cond_0

    .line 4063
    :goto_0
    return-void

    .line 4062
    :cond_0
    invoke-static {p0, p1, v0}, Ldsm;->e(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;IZ)V
    .locals 6

    .prologue
    .line 1037
    sget-object v1, Ldsm;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 1038
    :try_start_0
    new-instance v0, Lkfp;

    invoke-direct {v0}, Lkfp;-><init>()V

    .line 1039
    const-string v2, "Circle sync"

    invoke-virtual {v0, v2}, Lkfp;->b(Ljava/lang/String;)V

    .line 1040
    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v2}, Ldsm;->b(Landroid/content/Context;ILkfp;Lles;)Z

    move-result v2

    .line 1041
    invoke-virtual {v0}, Lkfp;->g()V

    .line 1042
    if-eqz v2, :cond_0

    .line 1043
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1044
    const-string v2, "circle_sync_time"

    .line 1045
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1044
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1046
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    .line 1047
    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1048
    const-string v3, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1050
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1052
    if-eqz p2, :cond_0

    .line 1054
    invoke-static {p0, p1}, Ldsm;->l(Landroid/content/Context;I)V

    .line 1057
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 5343
    new-instance v0, Ljqk;

    invoke-direct {v0, p0, p1}, Ljqk;-><init>(Landroid/content/Context;Z)V

    .line 5344
    invoke-static {v0}, Lhoc;->a(Lhny;)Lhoz;

    .line 5345
    return-void
.end method

.method public static b(Landroid/content/Context;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1097
    invoke-static {p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1098
    invoke-static {p0, p1}, Ldsm;->m(Landroid/content/Context;I)V

    .line 1127
    :cond_0
    :goto_0
    return v0

    .line 1102
    :cond_1
    sget-object v1, Ldsm;->j:Ljava/util/concurrent/CountDownLatch;

    .line 1103
    if-eqz v1, :cond_0

    .line 1107
    invoke-static {p0, p1}, Ldsm;->j(Landroid/content/Context;I)J

    move-result-wide v2

    .line 1108
    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1114
    invoke-static {p0, p1}, Ldsm;->l(Landroid/content/Context;I)V

    .line 1117
    :try_start_0
    sget v2, Ldsm;->k:I

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1122
    :goto_1
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1124
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;ILjava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lohv;",
            ">;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2632
    .line 2633
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 2635
    const-string v0, "EsPeopleData"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v2

    .line 2636
    :goto_0
    if-ge v3, v1, :cond_1

    .line 2637
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lohv;

    .line 2638
    const-string v4, ">>>>> Contact id: "

    invoke-virtual {v0}, Lohv;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 2636
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2638
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 2642
    :cond_1
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 2643
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    move v3, v2

    .line 2646
    :goto_2
    if-ge v3, v1, :cond_4

    .line 2647
    add-int/lit8 v0, v3, 0x4b

    .line 2648
    if-le v0, v1, :cond_2

    move v0, v1

    .line 2651
    :cond_2
    invoke-static {p0, v4, p2, v3, v0}, Ldsm;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;II)I

    move-result v3

    .line 2653
    if-lez v3, :cond_3

    .line 2654
    const/4 v2, 0x1

    :cond_3
    move v3, v0

    .line 2657
    goto :goto_2

    .line 2659
    :cond_4
    return v2
.end method

.method private static b(Landroid/content/Context;ILkfp;Lles;)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1488
    invoke-virtual {p2}, Lkfp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1521
    :goto_0
    return v5

    .line 1492
    :cond_0
    invoke-static {p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1493
    invoke-static {p0, p1}, Ldsm;->a(Landroid/content/Context;I)J

    move-result-wide v0

    .line 1494
    invoke-static {p0, p1, v0, v1, v5}, Ldsm;->a(Landroid/content/Context;IJZ)V

    :goto_1
    move v5, v3

    .line 1521
    goto :goto_0

    .line 1496
    :cond_1
    const-string v0, "Circles"

    invoke-virtual {p2, v0}, Lkfp;->c(Ljava/lang/String;)V

    .line 1498
    new-instance v0, Ldky;

    new-instance v2, Lkfo;

    invoke-direct {v2, p0, p1, p2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    const/4 v7, 0x0

    move-object v1, p0

    move v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v7}, Ldky;-><init>(Landroid/content/Context;Lkfo;ZZZI[B)V

    .line 1506
    invoke-virtual {v0}, Ldky;->l()V

    .line 1508
    if-eqz p3, :cond_2

    .line 1509
    invoke-virtual {p3, v0}, Lles;->a(Lkff;)V

    .line 1514
    :cond_2
    const-string v1, "EsPeopleData"

    invoke-virtual {v0, v1}, Ldky;->e(Ljava/lang/String;)V

    .line 1516
    invoke-virtual {v0}, Ldky;->b()Lojc;

    move-result-object v1

    invoke-virtual {v0}, Ldky;->c()Lojb;

    move-result-object v0

    invoke-static {p0, p1, v1, v0}, Ldsm;->a(Landroid/content/Context;ILojc;Lojb;)I

    move-result v0

    .line 1518
    invoke-virtual {p2, v0}, Lkfp;->m(I)V

    goto :goto_1
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2795
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2796
    const-string v3, "in_my_circles"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2800
    const-string v3, "last_updated_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2801
    new-array v3, v0, [Ljava/lang/String;

    aput-object p1, v3, v1

    .line 2802
    const-string v4, "contacts"

    const-string v5, "person_id=?"

    invoke-virtual {p0, v4, v2, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2804
    if-eqz v2, :cond_0

    .line 2805
    const-string v4, "circle_contact"

    const-string v5, "link_person_id=?"

    invoke-virtual {p0, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2808
    :cond_0
    if-lez v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 4826
    packed-switch p0, :pswitch_data_0

    .line 4834
    const/high16 v0, -0x80000000

    :goto_0
    return v0

    .line 4828
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4830
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 4832
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 4826
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static c(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 1443
    .line 1444
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1446
    :try_start_0
    const-string v1, "SELECT people_fingerprint  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1450
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static c(Lohv;)J
    .locals 2

    .prologue
    .line 4803
    iget-object v0, p0, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4805
    iget-object v0, p0, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->e:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    .line 4809
    :goto_0
    return-wide v0

    .line 4807
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 4929
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    .line 4930
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "packed_circle_ids"

    aput-object v3, v2, v6

    const-string v3, "gaia_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4934
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4935
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 4938
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4940
    :goto_0
    return-object v5

    .line 4938
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static c([B)Lnts;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3677
    if-nez p0, :cond_0

    move-object v0, v1

    .line 3685
    :goto_0
    return-object v0

    .line 3682
    :cond_0
    :try_start_0
    new-instance v0, Lnts;

    invoke-direct {v0}, Lnts;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnts;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3683
    :catch_0
    move-exception v0

    .line 3684
    const-string v2, "EsPeopleData"

    const-string v3, "Unable to deserialize ProfileSquares."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 3685
    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Lohp;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 4709
    new-instance v0, Lohp;

    invoke-direct {v0}, Lohp;-><init>()V

    .line 4710
    const-string v1, "g:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4711
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lohp;->d:Ljava/lang/String;

    .line 4717
    :cond_0
    :goto_0
    return-object v0

    .line 4712
    :cond_1
    const-string v1, "e:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4713
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lohp;->b:Ljava/lang/String;

    goto :goto_0

    .line 4714
    :cond_2
    const-string v1, "p:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4715
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lohp;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;ILjava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2777
    const/4 v0, 0x0

    .line 2778
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    .line 2779
    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2781
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2783
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2784
    invoke-static {v2, v0}, Ldsm;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 2785
    goto :goto_0

    .line 2786
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2788
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2791
    return v1

    .line 2788
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4431
    :try_start_0
    const-string v2, "SELECT in_my_circles FROM contacts WHERE person_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 4437
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 4431
    goto :goto_0

    .line 4437
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method private static d(I)I
    .locals 1

    .prologue
    .line 2088
    packed-switch p0, :pswitch_data_0

    .line 2100
    :pswitch_0
    const/16 v0, 0xb

    :goto_0
    return v0

    .line 2090
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2096
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 2088
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private static d(Landroid/content/Context;ILjava/lang/String;)J
    .locals 4

    .prologue
    .line 988
    .line 989
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 991
    :try_start_0
    const-string v0, "SELECT last_volume_sync  FROM circles WHERE     circle_id = "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 994
    invoke-static {p2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    .line 991
    invoke-static {v1, v0, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 996
    :goto_1
    return-wide v0

    .line 994
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 996
    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_1
.end method

.method public static d(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4988
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 4989
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4990
    const/4 v0, 0x0

    .line 4991
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 4992
    const/16 v1, 0x7c

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 4993
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 4994
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 4996
    :cond_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4997
    add-int/lit8 v0, v1, 0x1

    .line 4998
    goto :goto_0

    .line 5001
    :cond_1
    return-object v2
.end method

.method private static d([B)Lnkm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3704
    if-nez p0, :cond_0

    move-object v0, v1

    .line 3712
    :goto_0
    return-object v0

    .line 3709
    :cond_0
    :try_start_0
    new-instance v0, Lnkm;

    invoke-direct {v0}, Lnkm;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnkm;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3710
    :catch_0
    move-exception v0

    .line 3711
    const-string v2, "EsPeopleData"

    const-string v3, "Unable to deserialize ProfileStats."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 3712
    goto :goto_0
.end method

.method public static d(Landroid/content/Context;I)V
    .locals 5

    .prologue
    .line 2446
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2447
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 2449
    const-string v0, "EsPeopleData"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2450
    const-string v0, ">>>> insertSelf: "

    const-string v2, "display_name"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 2454
    :cond_0
    :goto_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2456
    const-string v0, "profile_photo_url"

    invoke-interface {v1, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2457
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2458
    const/4 v0, 0x0

    .line 2461
    :cond_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2463
    :try_start_0
    const-string v3, "gaia_id"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "display_name"

    .line 2464
    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2463
    invoke-static {v2, v3, v1, v0}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2466
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2468
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2469
    return-void

    .line 2450
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2468
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3245
    invoke-static {p0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v1

    .line 3246
    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    .line 3255
    :cond_0
    :goto_0
    return-object v0

    .line 3250
    :cond_1
    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 3251
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3255
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;I)Ljava/util/HashMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 4948
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 4950
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    .line 4952
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "gaia_id"

    aput-object v4, v2, v7

    const-string v4, "packed_circle_ids"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4956
    if-eqz v1, :cond_1

    .line 4958
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4959
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4960
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 4961
    invoke-virtual {v6, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4964
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4968
    :cond_1
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v7

    invoke-static {p0, p1, v0, v3}, Ldsm;->a(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)Landroid/database/Cursor;

    move-result-object v1

    .line 4970
    if-eqz v1, :cond_3

    .line 4972
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4973
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4974
    const-string v2, "15"

    invoke-virtual {v6, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 4977
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4981
    :cond_3
    return-object v6
.end method

.method private static e([B)Lmcf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3771
    if-nez p0, :cond_0

    move-object v0, v1

    .line 3779
    :goto_0
    return-object v0

    .line 3776
    :cond_0
    :try_start_0
    new-instance v0, Lmcf;

    invoke-direct {v0}, Lmcf;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmcf;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3777
    :catch_0
    move-exception v0

    .line 3778
    const-string v2, "EsPeopleData"

    const-string v3, "Unable to deserialize GetPeopleDataResponse."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 3779
    goto :goto_0
.end method

.method private static e(Landroid/content/Context;ILjava/lang/String;)V
    .locals 17

    .prologue
    .line 4154
    .line 4155
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v3

    invoke-virtual {v3}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 4159
    const/4 v12, 0x0

    .line 4161
    const/4 v11, 0x0

    .line 4162
    const-string v4, "profiles"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "videos_data_proto"

    aput-object v7, v5, v6

    const-string v6, "profile_person_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "g:"

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v13

    if-eqz v13, :cond_4

    invoke-virtual {v10, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_0
    aput-object v8, v7, v9

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 4167
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 4168
    const/4 v3, 0x0

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 4171
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 4174
    if-eqz v3, :cond_7

    .line 4175
    invoke-static {v3}, Ldsm;->f([B)Lmdp;

    move-result-object v3

    .line 4176
    invoke-static {v3}, Ldks;->b(Lmdp;)Z

    move-result v3

    .line 4179
    :goto_2
    const-class v4, Lieh;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 4180
    invoke-static/range {p0 .. p2}, Ldsm;->f(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v16

    .line 4182
    new-instance v4, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    .line 4183
    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lkgi;->a(Landroid/content/Context;Lkfo;)Lkfu;

    move-result-object v7

    .line 4184
    new-instance v8, Lkbv;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v8, v0, v4, v1, v2}, Lkbv;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;)V

    .line 4186
    new-instance v9, Ldkf;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v9, v0, v1, v2}, Ldkf;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 4187
    new-instance v10, Ldkn;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v10, v0, v1, v2, v4}, Ldkn;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 4189
    new-instance v6, Ldks;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v6, v0, v1, v2, v3}, Ldks;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 4191
    new-instance v12, Ldkl;

    new-instance v4, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v12, v0, v4, v1}, Ldkl;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    .line 4193
    new-instance v13, Ldkv;

    new-instance v4, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v13, v0, v4, v1}, Ldkv;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    .line 4195
    const/4 v5, 0x0

    .line 4196
    const/4 v4, 0x0

    .line 4198
    invoke-virtual {v7, v8}, Lkfu;->a(Lkff;)V

    .line 4199
    invoke-virtual {v7, v9}, Lkfu;->a(Lkff;)V

    .line 4200
    invoke-virtual {v7, v10}, Lkfu;->a(Lkff;)V

    .line 4201
    invoke-virtual {v7, v6}, Lkfu;->a(Lkff;)V

    .line 4202
    invoke-virtual {v7, v12}, Lkfu;->a(Lkff;)V

    .line 4204
    if-eqz v16, :cond_6

    .line 4205
    new-instance v5, Lktj;

    new-instance v4, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    const/4 v11, 0x1

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4, v11}, Lktj;-><init>(Landroid/content/Context;Lkfo;I)V

    .line 4207
    new-instance v4, Lkwr;

    new-instance v11, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v11, v0, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v11}, Lkwr;-><init>(Landroid/content/Context;Lkfo;)V

    .line 4209
    invoke-virtual {v7, v5}, Lkfu;->a(Lkff;)V

    .line 4210
    invoke-virtual {v7, v4}, Lkfu;->a(Lkff;)V

    move-object v14, v4

    move-object v15, v5

    .line 4213
    :goto_3
    invoke-virtual {v7, v13}, Lkfu;->a(Lkff;)V

    .line 4215
    invoke-virtual {v7}, Lkfu;->l()V

    .line 4217
    const-string v4, "EsPeopleData"

    invoke-virtual {v8, v4}, Lkbv;->e(Ljava/lang/String;)V

    .line 4218
    const-string v4, "EsPeopleData"

    invoke-virtual {v9, v4}, Ldkf;->d(Ljava/lang/String;)V

    .line 4219
    const-string v4, "EsPeopleData"

    invoke-virtual {v10, v4}, Ldkn;->d(Ljava/lang/String;)V

    .line 4220
    const-string v4, "EsPeopleData"

    invoke-virtual {v6, v4}, Ldks;->d(Ljava/lang/String;)V

    .line 4221
    const-string v4, "EsPeopleData"

    invoke-virtual {v12, v4}, Ldkl;->d(Ljava/lang/String;)V

    .line 4223
    if-eqz v16, :cond_0

    .line 4224
    const-string v4, "EsPeopleData"

    invoke-virtual {v15, v4}, Lktj;->d(Ljava/lang/String;)V

    .line 4225
    const-string v4, "EsPeopleData"

    invoke-virtual {v14, v4}, Lkwr;->d(Ljava/lang/String;)V

    .line 4227
    :cond_0
    const-string v4, "EsPeopleData"

    invoke-virtual {v13, v4}, Ldkv;->d(Ljava/lang/String;)V

    .line 4230
    if-nez v3, :cond_5

    invoke-virtual {v6}, Ldks;->c()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 4231
    new-instance v3, Ldks;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v3, v0, v1, v2, v4}, Ldks;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 4232
    invoke-virtual {v3}, Ldks;->l()V

    .line 4233
    const-string v4, "EsPeopleData"

    invoke-virtual {v3, v4}, Ldks;->d(Ljava/lang/String;)V

    .line 4238
    :goto_4
    invoke-virtual {v8}, Lkbv;->i()Lnjt;

    move-result-object v4

    .line 4239
    if-eqz v4, :cond_1

    iget v5, v4, Lnjt;->b:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    iget-object v4, v4, Lnjt;->f:Lnjg;

    iget v4, v4, Lnjg;->a:I

    .line 4245
    :cond_1
    sget-object v4, Ldsm;->e:Lloz;

    .line 4246
    invoke-virtual {v8}, Lkbv;->i()Lnjt;

    move-result-object v6

    invoke-virtual {v9}, Ldkf;->b()Lmcf;

    move-result-object v7

    .line 4264
    invoke-virtual {v3}, Ldks;->b()Lmdp;

    move-result-object v8

    invoke-virtual {v10}, Ldkn;->b()Lnlp;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 4265
    invoke-virtual {v12}, Ldkl;->b()Lnkm;

    move-result-object v12

    invoke-virtual {v13}, Ldkv;->b()Lnts;

    move-result-object v13

    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p2

    .line 4263
    invoke-static/range {v3 .. v13}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Lnjt;Lmcf;Lmdp;Lnlp;Lnum;Lnum;Lnkm;Lnts;)V

    .line 4267
    if-eqz v16, :cond_3

    .line 4268
    const-class v3, Lkxp;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkxp;

    invoke-virtual {v15}, Lktj;->t()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v15}, Lktj;->D()Loxu;

    move-result-object v4

    check-cast v4, Lmcz;

    iget-object v4, v4, Lmcz;->a:Lnto;

    move/from16 v0, p1

    invoke-virtual {v3, v0, v4}, Lkxp;->a(ILnto;)V

    :cond_2
    invoke-virtual {v14}, Lkwr;->t()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v14}, Lkwr;->i()Lnso;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v3, v0, v4}, Lkxp;->a(ILnso;)V

    .line 4271
    :cond_3
    return-void

    .line 4162
    :cond_4
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4171
    :catchall_0
    move-exception v3

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_5
    move-object v3, v6

    goto :goto_4

    :cond_6
    move-object v14, v4

    move-object v15, v5

    goto/16 :goto_3

    :cond_7
    move v3, v12

    goto/16 :goto_2

    :cond_8
    move-object v3, v11

    goto/16 :goto_1
.end method

.method public static f(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lhxc;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5026
    const/16 v1, 0xf

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "circle_id"

    aput-object v3, v2, v4

    const-string v3, "circle_name"

    aput-object v3, v2, v5

    const-string v3, "type"

    aput-object v3, v2, v6

    const-string v3, "contact_count"

    aput-object v3, v2, v7

    invoke-static {p0, p1, v1, v2}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 5034
    if-eqz v1, :cond_1

    .line 5036
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5037
    new-instance v0, Ljava/util/ArrayList;

    .line 5038
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 5040
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 5041
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 5042
    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 5043
    const/4 v5, 0x3

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 5044
    new-instance v6, Lhxc;

    invoke-direct {v6, v2, v4, v3, v5}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5045
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 5050
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5053
    :cond_1
    :goto_0
    return-object v0

    .line 5048
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static f([B)Lmdp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3794
    if-nez p0, :cond_0

    move-object v0, v1

    .line 3802
    :goto_0
    return-object v0

    .line 3799
    :cond_0
    :try_start_0
    new-instance v0, Lmdp;

    invoke-direct {v0}, Lmdp;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmdp;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3800
    :catch_0
    move-exception v0

    .line 3801
    const-string v2, "EsPeopleData"

    const-string v3, "Unable to deserialize cached video data."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 3802
    goto :goto_0
.end method

.method private static f(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 5381
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 5382
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 5383
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5385
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static g([B)Lnum;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3817
    if-nez p0, :cond_0

    move-object v0, v1

    .line 3825
    :goto_0
    return-object v0

    .line 3822
    :cond_0
    :try_start_0
    new-instance v0, Lnum;

    invoke-direct {v0}, Lnum;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnum;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3823
    :catch_0
    move-exception v0

    .line 3824
    const-string v2, "EsPeopleData"

    const-string v3, "Unable to deserialize cached local reviews data."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 3825
    goto :goto_0
.end method

.method public static g(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 5296
    invoke-static {p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5297
    new-instance v0, Ljqy;

    invoke-direct {v0, p0}, Ljqy;-><init>(Landroid/content/Context;)V

    .line 5298
    invoke-interface {v0}, Ljpr;->c()Ljps;

    move-result-object v1

    .line 5299
    new-instance v2, Ldsu;

    invoke-direct {v2, v1}, Ldsu;-><init>(Ljps;)V

    .line 5307
    invoke-interface {v1}, Ljps;->d()V

    .line 5308
    invoke-interface {v0, v2, p1}, Ljpr;->a(Ljqf;I)V

    .line 5310
    :cond_0
    return-void
.end method

.method private static h(Landroid/content/Context;I)J
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 970
    .line 971
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 973
    :try_start_0
    const-string v1, "SELECT circle_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 977
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static h([B)Lnlp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3897
    if-nez p0, :cond_0

    move-object v0, v1

    .line 3905
    :goto_0
    return-object v0

    .line 3902
    :cond_0
    :try_start_0
    new-instance v0, Lnlp;

    invoke-direct {v0}, Lnlp;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnlp;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3903
    :catch_0
    move-exception v0

    .line 3904
    const-string v2, "EsPeopleData"

    const-string v3, "Unable to deserialize GetReviewsDataResponse."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 3905
    goto :goto_0
.end method

.method private static i(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 1211
    .line 1212
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1214
    :try_start_0
    const-string v1, "SELECT blocked_people_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1218
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static j(Landroid/content/Context;I)J
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1426
    .line 1427
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1429
    :try_start_0
    const-string v1, "SELECT people_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1433
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static k(Landroid/content/Context;I)[B
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 1461
    .line 1462
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1463
    const-string v1, "account_status"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "people_last_update_token"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1468
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1469
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1472
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1474
    return-object v3

    .line 1472
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static l(Landroid/content/Context;I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4576
    new-instance v0, Ldsn;

    invoke-direct {v0, p0, p1}, Ldsn;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Ljava/lang/Runnable;)V

    .line 4582
    return-void
.end method

.method private static m(Landroid/content/Context;I)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 5203
    invoke-static {p0, p1}, Ldsm;->a(Landroid/content/Context;I)J

    move-result-wide v0

    .line 5204
    invoke-static {p0, p1, v0, v1, v8}, Ldsm;->a(Landroid/content/Context;IJZ)V

    .line 5206
    invoke-static {p0, p1}, Ldsm;->c(Landroid/content/Context;I)J

    move-result-wide v4

    .line 5208
    const-wide/16 v0, -0x1

    cmp-long v0, v4, v0

    if-nez v0, :cond_2

    move v0, v8

    .line 5209
    :goto_0
    new-instance v10, Ljqy;

    invoke-direct {v10, p0}, Ljqy;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/os/ConditionVariable;

    invoke-direct {v3}, Landroid/os/ConditionVariable;-><init>()V

    invoke-interface {v10}, Ljpr;->c()Ljps;

    move-result-object v2

    new-instance v1, Ldss;

    move-object v6, p0

    move v7, p1

    invoke-direct/range {v1 .. v7}, Ldss;-><init>(Ljps;Landroid/os/ConditionVariable;JLandroid/content/Context;I)V

    invoke-static {}, Ljpe;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "Connecting client %s."

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    :cond_0
    invoke-interface {v2}, Ljps;->d()V

    invoke-interface {v10, v1, p1, v0}, Ljpr;->a(Ljpd;IZ)V

    if-eqz v0, :cond_1

    invoke-static {}, Llsx;->c()V

    const-wide/16 v0, 0x2710

    invoke-virtual {v3, v0, v1}, Landroid/os/ConditionVariable;->block(J)Z

    .line 5210
    :cond_1
    return-void

    :cond_2
    move v0, v9

    .line 5208
    goto :goto_0
.end method
