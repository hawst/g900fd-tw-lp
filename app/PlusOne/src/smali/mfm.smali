.class public final Lmfm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvm;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5749
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5752
    iput-object v0, p0, Lmfm;->apiHeader:Llyq;

    .line 5755
    iput-object v0, p0, Lmfm;->a:Lnvm;

    .line 5749
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5772
    const/4 v0, 0x0

    .line 5773
    iget-object v1, p0, Lmfm;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 5774
    const/4 v0, 0x1

    iget-object v1, p0, Lmfm;->apiHeader:Llyq;

    .line 5775
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5777
    :cond_0
    iget-object v1, p0, Lmfm;->a:Lnvm;

    if-eqz v1, :cond_1

    .line 5778
    const/4 v1, 0x2

    iget-object v2, p0, Lmfm;->a:Lnvm;

    .line 5779
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5781
    :cond_1
    iget-object v1, p0, Lmfm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5782
    iput v0, p0, Lmfm;->ai:I

    .line 5783
    return v0
.end method

.method public a(Loxn;)Lmfm;
    .locals 2

    .prologue
    .line 5791
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5792
    sparse-switch v0, :sswitch_data_0

    .line 5796
    iget-object v1, p0, Lmfm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5797
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfm;->ah:Ljava/util/List;

    .line 5800
    :cond_1
    iget-object v1, p0, Lmfm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5802
    :sswitch_0
    return-object p0

    .line 5807
    :sswitch_1
    iget-object v0, p0, Lmfm;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 5808
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfm;->apiHeader:Llyq;

    .line 5810
    :cond_2
    iget-object v0, p0, Lmfm;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5814
    :sswitch_2
    iget-object v0, p0, Lmfm;->a:Lnvm;

    if-nez v0, :cond_3

    .line 5815
    new-instance v0, Lnvm;

    invoke-direct {v0}, Lnvm;-><init>()V

    iput-object v0, p0, Lmfm;->a:Lnvm;

    .line 5817
    :cond_3
    iget-object v0, p0, Lmfm;->a:Lnvm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5792
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5760
    iget-object v0, p0, Lmfm;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 5761
    const/4 v0, 0x1

    iget-object v1, p0, Lmfm;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5763
    :cond_0
    iget-object v0, p0, Lmfm;->a:Lnvm;

    if-eqz v0, :cond_1

    .line 5764
    const/4 v0, 0x2

    iget-object v1, p0, Lmfm;->a:Lnvm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5766
    :cond_1
    iget-object v0, p0, Lmfm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5768
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5745
    invoke-virtual {p0, p1}, Lmfm;->a(Loxn;)Lmfm;

    move-result-object v0

    return-object v0
.end method
