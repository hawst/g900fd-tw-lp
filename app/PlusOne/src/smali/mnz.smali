.class public final Lmnz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmnz;


# instance fields
.field public b:Lmnu;

.field public c:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6285
    const/4 v0, 0x0

    new-array v0, v0, [Lmnz;

    sput-object v0, Lmnz;->a:[Lmnz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6286
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6289
    const/4 v0, 0x0

    iput-object v0, p0, Lmnz;->b:Lmnu;

    .line 6286
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6308
    const/4 v0, 0x0

    .line 6309
    iget-object v1, p0, Lmnz;->b:Lmnu;

    if-eqz v1, :cond_0

    .line 6310
    const/4 v0, 0x1

    iget-object v1, p0, Lmnz;->b:Lmnu;

    .line 6311
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6313
    :cond_0
    iget-object v1, p0, Lmnz;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 6314
    const/4 v1, 0x2

    iget-object v2, p0, Lmnz;->c:Ljava/lang/Float;

    .line 6315
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6317
    :cond_1
    iget-object v1, p0, Lmnz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6318
    iput v0, p0, Lmnz;->ai:I

    .line 6319
    return v0
.end method

.method public a(Loxn;)Lmnz;
    .locals 2

    .prologue
    .line 6327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6328
    sparse-switch v0, :sswitch_data_0

    .line 6332
    iget-object v1, p0, Lmnz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6333
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnz;->ah:Ljava/util/List;

    .line 6336
    :cond_1
    iget-object v1, p0, Lmnz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6338
    :sswitch_0
    return-object p0

    .line 6343
    :sswitch_1
    iget-object v0, p0, Lmnz;->b:Lmnu;

    if-nez v0, :cond_2

    .line 6344
    new-instance v0, Lmnu;

    invoke-direct {v0}, Lmnu;-><init>()V

    iput-object v0, p0, Lmnz;->b:Lmnu;

    .line 6346
    :cond_2
    iget-object v0, p0, Lmnz;->b:Lmnu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6350
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnz;->c:Ljava/lang/Float;

    goto :goto_0

    .line 6328
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6296
    iget-object v0, p0, Lmnz;->b:Lmnu;

    if-eqz v0, :cond_0

    .line 6297
    const/4 v0, 0x1

    iget-object v1, p0, Lmnz;->b:Lmnu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6299
    :cond_0
    iget-object v0, p0, Lmnz;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 6300
    const/4 v0, 0x2

    iget-object v1, p0, Lmnz;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6302
    :cond_1
    iget-object v0, p0, Lmnz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6304
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6282
    invoke-virtual {p0, p1}, Lmnz;->a(Loxn;)Lmnz;

    move-result-object v0

    return-object v0
.end method
