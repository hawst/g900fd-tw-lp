.class public final Lpbf;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpay;

.field private b:[Lpaf;

.field private c:Lpaf;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1698
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1709
    iput-object v1, p0, Lpbf;->a:Lpay;

    .line 1712
    sget-object v0, Lpaf;->a:[Lpaf;

    iput-object v0, p0, Lpbf;->b:[Lpaf;

    .line 1715
    iput-object v1, p0, Lpbf;->c:Lpaf;

    .line 1718
    const/high16 v0, -0x80000000

    iput v0, p0, Lpbf;->d:I

    .line 1698
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1745
    .line 1746
    iget-object v0, p0, Lpbf;->a:Lpay;

    if-eqz v0, :cond_4

    .line 1747
    const/4 v0, 0x1

    iget-object v2, p0, Lpbf;->a:Lpay;

    .line 1748
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1750
    :goto_0
    iget-object v2, p0, Lpbf;->b:[Lpaf;

    if-eqz v2, :cond_1

    .line 1751
    iget-object v2, p0, Lpbf;->b:[Lpaf;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1752
    if-eqz v4, :cond_0

    .line 1753
    const/4 v5, 0x2

    .line 1754
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1751
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1758
    :cond_1
    iget-object v1, p0, Lpbf;->c:Lpaf;

    if-eqz v1, :cond_2

    .line 1759
    const/4 v1, 0x3

    iget-object v2, p0, Lpbf;->c:Lpaf;

    .line 1760
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1762
    :cond_2
    iget v1, p0, Lpbf;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 1763
    const/4 v1, 0x4

    iget v2, p0, Lpbf;->d:I

    .line 1764
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1766
    :cond_3
    iget-object v1, p0, Lpbf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1767
    iput v0, p0, Lpbf;->ai:I

    .line 1768
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpbf;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1776
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1777
    sparse-switch v0, :sswitch_data_0

    .line 1781
    iget-object v2, p0, Lpbf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1782
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpbf;->ah:Ljava/util/List;

    .line 1785
    :cond_1
    iget-object v2, p0, Lpbf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1787
    :sswitch_0
    return-object p0

    .line 1792
    :sswitch_1
    iget-object v0, p0, Lpbf;->a:Lpay;

    if-nez v0, :cond_2

    .line 1793
    new-instance v0, Lpay;

    invoke-direct {v0}, Lpay;-><init>()V

    iput-object v0, p0, Lpbf;->a:Lpay;

    .line 1795
    :cond_2
    iget-object v0, p0, Lpbf;->a:Lpay;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1799
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1800
    iget-object v0, p0, Lpbf;->b:[Lpaf;

    if-nez v0, :cond_4

    move v0, v1

    .line 1801
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpaf;

    .line 1802
    iget-object v3, p0, Lpbf;->b:[Lpaf;

    if-eqz v3, :cond_3

    .line 1803
    iget-object v3, p0, Lpbf;->b:[Lpaf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1805
    :cond_3
    iput-object v2, p0, Lpbf;->b:[Lpaf;

    .line 1806
    :goto_2
    iget-object v2, p0, Lpbf;->b:[Lpaf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1807
    iget-object v2, p0, Lpbf;->b:[Lpaf;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    aput-object v3, v2, v0

    .line 1808
    iget-object v2, p0, Lpbf;->b:[Lpaf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1809
    invoke-virtual {p1}, Loxn;->a()I

    .line 1806
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1800
    :cond_4
    iget-object v0, p0, Lpbf;->b:[Lpaf;

    array-length v0, v0

    goto :goto_1

    .line 1812
    :cond_5
    iget-object v2, p0, Lpbf;->b:[Lpaf;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    aput-object v3, v2, v0

    .line 1813
    iget-object v2, p0, Lpbf;->b:[Lpaf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1817
    :sswitch_3
    iget-object v0, p0, Lpbf;->c:Lpaf;

    if-nez v0, :cond_6

    .line 1818
    new-instance v0, Lpaf;

    invoke-direct {v0}, Lpaf;-><init>()V

    iput-object v0, p0, Lpbf;->c:Lpaf;

    .line 1820
    :cond_6
    iget-object v0, p0, Lpbf;->c:Lpaf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1824
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1825
    if-eq v0, v4, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    if-eq v0, v2, :cond_7

    const/4 v2, 0x5

    if-ne v0, v2, :cond_8

    .line 1830
    :cond_7
    iput v0, p0, Lpbf;->d:I

    goto/16 :goto_0

    .line 1832
    :cond_8
    iput v4, p0, Lpbf;->d:I

    goto/16 :goto_0

    .line 1777
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1723
    iget-object v0, p0, Lpbf;->a:Lpay;

    if-eqz v0, :cond_0

    .line 1724
    const/4 v0, 0x1

    iget-object v1, p0, Lpbf;->a:Lpay;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1726
    :cond_0
    iget-object v0, p0, Lpbf;->b:[Lpaf;

    if-eqz v0, :cond_2

    .line 1727
    iget-object v1, p0, Lpbf;->b:[Lpaf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1728
    if-eqz v3, :cond_1

    .line 1729
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1727
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1733
    :cond_2
    iget-object v0, p0, Lpbf;->c:Lpaf;

    if-eqz v0, :cond_3

    .line 1734
    const/4 v0, 0x3

    iget-object v1, p0, Lpbf;->c:Lpaf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1736
    :cond_3
    iget v0, p0, Lpbf;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 1737
    const/4 v0, 0x4

    iget v1, p0, Lpbf;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1739
    :cond_4
    iget-object v0, p0, Lpbf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1741
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1694
    invoke-virtual {p0, p1}, Lpbf;->a(Loxn;)Lpbf;

    move-result-object v0

    return-object v0
.end method
