.class final Lids;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/nio/ByteBuffer;

.field private final b:Lidq;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lidt;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lidp;

.field private e:I


# direct methods
.method protected constructor <init>(Ljava/nio/ByteBuffer;Lidp;)V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lids;->c:Ljava/util/List;

    .line 49
    iput-object p1, p0, Lids;->a:Ljava/nio/ByteBuffer;

    .line 50
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iput v0, p0, Lids;->e:I

    .line 51
    iput-object p2, p0, Lids;->d:Lidp;

    .line 52
    const/4 v2, 0x0

    .line 54
    :try_start_0
    new-instance v1, Lidn;

    invoke-direct {v1, p1}, Lidn;-><init>(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :try_start_1
    iget-object v0, p0, Lids;->d:Lidp;

    invoke-static {v1, v0}, Lidv;->a(Ljava/io/InputStream;Lidp;)Lidv;

    move-result-object v0

    .line 57
    new-instance v2, Lidq;

    invoke-virtual {v0}, Lidv;->o()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-direct {v2, v3}, Lidq;-><init>(Ljava/nio/ByteOrder;)V

    iput-object v2, p0, Lids;->b:Lidq;

    .line 58
    iget v2, p0, Lids;->e:I

    invoke-virtual {v0}, Lidv;->i()I

    move-result v0

    add-int/2addr v0, v2

    iput v0, p0, Lids;->e:I

    .line 59
    iget-object v0, p0, Lids;->a:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 61
    invoke-static {v1}, Lidp;->a(Ljava/io/Closeable;)V

    .line 62
    return-void

    .line 61
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-static {v1}, Lidp;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lids;->b:Lidq;

    invoke-virtual {v0}, Lidq;->e()Ljava/nio/ByteOrder;

    move-result-object v0

    return-object v0
.end method

.method public a(Liea;)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lids;->b:Lidq;

    invoke-virtual {v0, p1}, Lidq;->a(Liea;)Liea;

    .line 200
    return-void
.end method

.method protected b()Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 70
    const/4 v4, 0x0

    .line 72
    :try_start_0
    new-instance v3, Lidn;

    iget-object v0, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-direct {v3, v0}, Lidn;-><init>(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 74
    const/4 v0, 0x5

    :try_start_1
    new-array v5, v0, [Lieb;

    const/4 v0, 0x0

    iget-object v4, p0, Lids;->b:Lidq;

    const/4 v6, 0x0

    .line 75
    invoke-virtual {v4, v6}, Lidq;->b(I)Lieb;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x1

    iget-object v4, p0, Lids;->b:Lidq;

    const/4 v6, 0x1

    .line 76
    invoke-virtual {v4, v6}, Lidq;->b(I)Lieb;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x2

    iget-object v4, p0, Lids;->b:Lidq;

    const/4 v6, 0x2

    .line 77
    invoke-virtual {v4, v6}, Lidq;->b(I)Lieb;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x3

    iget-object v4, p0, Lids;->b:Lidq;

    const/4 v6, 0x3

    .line 78
    invoke-virtual {v4, v6}, Lidq;->b(I)Lieb;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x4

    iget-object v4, p0, Lids;->b:Lidq;

    const/4 v6, 0x4

    .line 79
    invoke-virtual {v4, v6}, Lidq;->b(I)Lieb;

    move-result-object v4

    aput-object v4, v5, v0

    .line 82
    const/4 v0, 0x0

    aget-object v0, v5, v0

    if-eqz v0, :cond_d

    move v0, v2

    .line 85
    :goto_0
    const/4 v4, 0x1

    aget-object v4, v5, v4

    if-eqz v4, :cond_0

    .line 86
    or-int/lit8 v0, v0, 0x2

    .line 88
    :cond_0
    const/4 v4, 0x2

    aget-object v4, v5, v4

    if-eqz v4, :cond_1

    .line 89
    or-int/lit8 v0, v0, 0x4

    .line 91
    :cond_1
    const/4 v4, 0x4

    aget-object v4, v5, v4

    if-eqz v4, :cond_2

    .line 92
    or-int/lit8 v0, v0, 0x8

    .line 94
    :cond_2
    const/4 v4, 0x3

    aget-object v4, v5, v4

    if-eqz v4, :cond_3

    .line 95
    or-int/lit8 v0, v0, 0x10

    .line 98
    :cond_3
    iget-object v4, p0, Lids;->d:Lidp;

    invoke-static {v3, v0, v4}, Lidv;->a(Ljava/io/InputStream;ILidp;)Lidv;

    move-result-object v6

    .line 99
    invoke-virtual {v6}, Lidv;->a()I

    move-result v4

    .line 100
    const/4 v0, 0x0

    .line 101
    :goto_1
    const/4 v7, 0x5

    if-eq v4, v7, :cond_7

    .line 102
    packed-switch v4, :pswitch_data_0

    .line 126
    :cond_4
    :goto_2
    invoke-virtual {v6}, Lidv;->a()I

    move-result v4

    goto :goto_1

    .line 104
    :pswitch_0
    invoke-virtual {v6}, Lidv;->d()I

    move-result v0

    aget-object v0, v5, v0

    .line 105
    if-nez v0, :cond_4

    .line 106
    invoke-virtual {v6}, Lidv;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 135
    :catchall_0
    move-exception v0

    move-object v1, v3

    :goto_3
    invoke-static {v1}, Lidp;->a(Ljava/io/Closeable;)V

    throw v0

    .line 110
    :pswitch_1
    :try_start_2
    invoke-virtual {v6}, Lidv;->c()Liea;

    move-result-object v4

    .line 111
    invoke-virtual {v4}, Liea;->b()S

    move-result v7

    invoke-virtual {v0, v7}, Lieb;->a(S)Liea;

    move-result-object v7

    .line 112
    if-eqz v7, :cond_4

    .line 113
    invoke-virtual {v7}, Liea;->e()I

    move-result v8

    invoke-virtual {v4}, Liea;->e()I

    move-result v9

    if-ne v8, v9, :cond_5

    .line 114
    invoke-virtual {v7}, Liea;->c()S

    move-result v8

    invoke-virtual {v4}, Liea;->c()S
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    if-eq v8, v9, :cond_6

    .line 115
    :cond_5
    invoke-static {v3}, Lidp;->a(Ljava/io/Closeable;)V

    move v0, v1

    .line 137
    :goto_4
    return v0

    .line 117
    :cond_6
    :try_start_3
    iget-object v8, p0, Lids;->c:Ljava/util/List;

    new-instance v9, Lidt;

    invoke-virtual {v4}, Liea;->o()I

    move-result v10

    invoke-direct {v9, v7, v10}, Lidt;-><init>(Liea;I)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-virtual {v4}, Liea;->b()S

    move-result v4

    invoke-virtual {v0, v4}, Lieb;->b(S)V

    .line 119
    invoke-virtual {v0}, Lieb;->d()I

    move-result v4

    if-nez v4, :cond_4

    .line 120
    invoke-virtual {v6}, Lidv;->b()V

    goto :goto_2

    :cond_7
    move v0, v1

    .line 128
    :goto_5
    const/4 v4, 0x5

    if-ge v0, v4, :cond_9

    aget-object v4, v5, v0

    .line 129
    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lieb;->d()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v4

    if-lez v4, :cond_8

    .line 130
    invoke-static {v3}, Lidp;->a(Ljava/io/Closeable;)V

    move v0, v1

    goto :goto_4

    .line 128
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 133
    :cond_9
    :try_start_4
    iget-object v0, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Lids;->a()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lids;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidt;

    iget-object v5, v0, Lidt;->b:Liea;

    iget v0, v0, Lidt;->a:I

    invoke-virtual {v5}, Liea;->f()Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lids;->a:Ljava/nio/ByteBuffer;

    iget v7, p0, Lids;->e:I

    add-int/2addr v0, v7

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v5}, Liea;->c()S

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    goto :goto_6

    :pswitch_3
    invoke-virtual {v5}, Liea;->e()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {v5, v0}, Liea;->b([B)V

    iget-object v5, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto :goto_6

    :pswitch_4
    invoke-virtual {v5}, Liea;->n()[B

    move-result-object v0

    array-length v6, v0

    invoke-virtual {v5}, Liea;->e()I

    move-result v5

    if-ne v6, v5, :cond_b

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    const/4 v6, 0x0

    aput-byte v6, v0, v5

    iget-object v5, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto :goto_6

    :cond_b
    iget-object v5, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lids;->a:Ljava/nio/ByteBuffer;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_6

    :pswitch_5
    invoke-virtual {v5}, Liea;->e()I

    move-result v6

    move v0, v1

    :goto_7
    if-ge v0, v6, :cond_a

    iget-object v7, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Liea;->e(I)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :pswitch_6
    invoke-virtual {v5}, Liea;->e()I

    move-result v6

    move v0, v1

    :goto_8
    if-ge v0, v6, :cond_a

    invoke-virtual {v5, v0}, Liea;->f(I)Liee;

    move-result-object v7

    iget-object v8, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Liee;->a()J

    move-result-wide v10

    long-to-int v9, v10

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v8, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Liee;->b()J

    move-result-wide v10

    long-to-int v7, v10

    invoke-virtual {v8, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :pswitch_7
    invoke-virtual {v5}, Liea;->e()I

    move-result v6

    move v0, v1

    :goto_9
    if-ge v0, v6, :cond_a

    iget-object v7, p0, Lids;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Liea;->e(I)J

    move-result-wide v8

    long-to-int v8, v8

    int-to-short v8, v8

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 135
    :cond_c
    invoke-static {v3}, Lidp;->a(Ljava/io/Closeable;)V

    move v0, v2

    .line 137
    goto/16 :goto_4

    .line 135
    :catchall_1
    move-exception v0

    move-object v1, v4

    goto/16 :goto_3

    :cond_d
    move v0, v1

    goto/16 :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 133
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
