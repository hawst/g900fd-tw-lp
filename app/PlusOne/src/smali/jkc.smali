.class public final Ljkc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "com.google.android.libraries.social.notifications.REPORT_ABUSE"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v3

    .line 26
    if-nez v3, :cond_0

    .line 49
    :goto_0
    return-void

    .line 30
    :cond_0
    const-string v0, "notification_to_flag"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 32
    const-string v2, "abuse_report"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    .line 37
    if-nez v0, :cond_1

    move-object v2, v1

    .line 39
    :goto_1
    if-nez v4, :cond_2

    move-object v0, v1

    .line 45
    :goto_2
    new-instance v4, Ljkd;

    invoke-direct {v4, p2, v3, v2, v0}, Ljkd;-><init>(Landroid/content/Context;Lkfo;Lluv;Llwr;)V

    .line 47
    const-class v0, Ljjo;

    invoke-static {p2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    .line 48
    invoke-virtual {v0, v4, p0, p2, v1}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    .line 37
    :cond_1
    :try_start_0
    new-instance v2, Lluv;

    invoke-direct {v2}, Lluv;-><init>()V

    .line 38
    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lluv;

    move-object v2, v0

    goto :goto_1

    .line 39
    :cond_2
    new-instance v0, Llwr;

    invoke-direct {v0}, Llwr;-><init>()V

    .line 40
    invoke-static {v0, v4}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Llwr;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 41
    :catch_0
    move-exception v0

    .line 42
    const-string v1, "NotsReportAbuseHandler"

    const-string v2, "Skipping report abuse - Cannot parse protobuf"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Ljkc;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
