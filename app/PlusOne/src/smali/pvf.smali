.class public final Lpvf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpvf;


# instance fields
.field public b:I

.field private c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1384
    const/4 v0, 0x0

    new-array v0, v0, [Lpvf;

    sput-object v0, Lpvf;->a:[Lpvf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1385
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1395
    const/high16 v0, -0x80000000

    iput v0, p0, Lpvf;->b:I

    .line 1385
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 1414
    const/4 v0, 0x0

    .line 1415
    iget v1, p0, Lpvf;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1416
    const/4 v0, 0x1

    iget v1, p0, Lpvf;->b:I

    .line 1417
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1419
    :cond_0
    iget-object v1, p0, Lpvf;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 1420
    const/4 v1, 0x2

    iget-object v2, p0, Lpvf;->c:Ljava/lang/Long;

    .line 1421
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1423
    :cond_1
    iget-object v1, p0, Lpvf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1424
    iput v0, p0, Lpvf;->ai:I

    .line 1425
    return v0
.end method

.method public a(Loxn;)Lpvf;
    .locals 2

    .prologue
    .line 1433
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1434
    sparse-switch v0, :sswitch_data_0

    .line 1438
    iget-object v1, p0, Lpvf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1439
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpvf;->ah:Ljava/util/List;

    .line 1442
    :cond_1
    iget-object v1, p0, Lpvf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1444
    :sswitch_0
    return-object p0

    .line 1449
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1450
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1454
    :cond_2
    iput v0, p0, Lpvf;->b:I

    goto :goto_0

    .line 1456
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpvf;->b:I

    goto :goto_0

    .line 1461
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpvf;->c:Ljava/lang/Long;

    goto :goto_0

    .line 1434
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 1402
    iget v0, p0, Lpvf;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1403
    const/4 v0, 0x1

    iget v1, p0, Lpvf;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1405
    :cond_0
    iget-object v0, p0, Lpvf;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1406
    const/4 v0, 0x2

    iget-object v1, p0, Lpvf;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 1408
    :cond_1
    iget-object v0, p0, Lpvf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1410
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1381
    invoke-virtual {p0, p1}, Lpvf;->a(Loxn;)Lpvf;

    move-result-object v0

    return-object v0
.end method
