.class public Lizu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljad;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/net/Uri;

.field private final e:Ljac;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lizv;

    invoke-direct {v0}, Lizv;-><init>()V

    sput-object v0, Lizu;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizu;->a:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizu;->c:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lizu;->d:Landroid/net/Uri;

    .line 150
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljac;->a(I)Ljac;

    move-result-object v0

    iput-object v0, p0, Lizu;->e:Ljac;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizu;->f:Ljava/lang/String;

    .line 152
    sget-object v0, Ljad;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljad;

    iput-object v0, p0, Lizu;->b:Ljad;

    .line 153
    return-void

    .line 148
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lizu;->d:Landroid/net/Uri;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    new-instance v0, Ljad;

    invoke-direct {v0, p2, p3, p4}, Ljad;-><init>(Ljava/lang/String;J)V

    iput-object v0, p0, Lizu;->b:Ljad;

    .line 130
    iput-object p1, p0, Lizu;->a:Ljava/lang/String;

    .line 131
    iput-object p5, p0, Lizu;->c:Ljava/lang/String;

    .line 132
    iput-object p6, p0, Lizu;->d:Landroid/net/Uri;

    .line 133
    iput-object p7, p0, Lizu;->e:Ljac;

    .line 134
    iput-object p8, p0, Lizu;->f:Ljava/lang/String;

    .line 136
    invoke-virtual {p0}, Lizu;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lizu;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MediaRef has neither url nor local uri!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 71
    const-wide/16 v4, 0x0

    move-object v1, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, p1

    move-object v8, p2

    move-object v9, v2

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 79
    const-wide/16 v4, 0x0

    move-object v1, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, p1

    move-object v8, p2

    move-object v9, p3

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;
    .locals 10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 89
    const/4 v2, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;
    .locals 10

    .prologue
    .line 99
    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 63
    const-wide/16 v4, 0x0

    move-object v1, p0

    move-object v3, v2

    move-object v6, p1

    move-object v7, v2

    move-object v8, p2

    move-object v9, v2

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;
    .locals 11
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;
    .locals 13

    .prologue
    .line 118
    if-eqz p6, :cond_1

    sget-object v2, Ljac;->a:Ljac;

    move-object/from16 v0, p7

    if-ne v0, v2, :cond_1

    .line 119
    invoke-virtual/range {p6 .. p6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljvs;->a:Ljvs;

    invoke-static {p0, v2, v3}, Ljvq;->a(Landroid/content/Context;Ljava/lang/String;Ljvs;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 120
    invoke-virtual/range {p6 .. p6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljvs;->b:Ljvs;

    invoke-static {p0, v2, v3}, Ljvq;->a(Landroid/content/Context;Ljava/lang/String;Ljvs;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 121
    :cond_0
    sget-object p7, Ljac;->c:Ljac;

    move-object/from16 v10, p7

    .line 124
    :goto_0
    new-instance v3, Lizu;

    move-object v4, p1

    move-object v5, p2

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lizu;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)V

    return-object v3

    :cond_1
    move-object/from16 v10, p7

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljac;)Lizu;
    .locals 10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 38
    const-wide/16 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object v9, v3

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;
    .locals 10

    .prologue
    .line 55
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 46
    const-wide/16 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    move-object v7, v3

    move-object v8, p3

    move-object v9, v3

    invoke-static/range {v1 .. v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 308
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 309
    invoke-virtual {p0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 313
    :goto_0
    return v0

    .line 310
    :cond_0
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 311
    const/4 v0, 0x1

    goto :goto_0

    .line 313
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lizu;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lizu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 289
    if-nez p1, :cond_1

    .line 296
    :cond_0
    :goto_0
    return v0

    .line 291
    :cond_1
    iget-object v1, p0, Lizu;->f:Ljava/lang/String;

    iget-object v2, p1, Lizu;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lizu;->c:Ljava/lang/String;

    iget-object v2, p1, Lizu;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296
    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    iget-object v1, p1, Lizu;->d:Landroid/net/Uri;

    invoke-static {v0, v1}, Lizu;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lizu;->b:Ljad;

    invoke-virtual {v0}, Ljad;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lizu;->b:Ljad;

    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lizu;->c:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 248
    if-ne p0, p1, :cond_1

    .line 270
    :cond_0
    :goto_0
    return v0

    .line 250
    :cond_1
    instance-of v2, p1, Lizu;

    if-eqz v2, :cond_6

    .line 251
    check-cast p1, Lizu;

    .line 256
    iget-object v2, p0, Lizu;->b:Ljad;

    invoke-virtual {v2}, Ljad;->b()J

    move-result-wide v2

    invoke-virtual {p1}, Lizu;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    move v0, v1

    .line 257
    goto :goto_0

    .line 258
    :cond_2
    iget-object v2, p0, Lizu;->c:Ljava/lang/String;

    iget-object v3, p1, Lizu;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 259
    goto :goto_0

    .line 260
    :cond_3
    iget-object v2, p0, Lizu;->f:Ljava/lang/String;

    iget-object v3, p1, Lizu;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 261
    goto :goto_0

    .line 262
    :cond_4
    iget-object v2, p0, Lizu;->d:Landroid/net/Uri;

    iget-object v3, p1, Lizu;->d:Landroid/net/Uri;

    invoke-static {v2, v3}, Lizu;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 263
    goto :goto_0

    .line 264
    :cond_5
    iget-object v2, p0, Lizu;->e:Ljac;

    iget-object v3, p1, Lizu;->e:Ljac;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 265
    goto :goto_0

    :cond_6
    move v0, v1

    .line 270
    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lizu;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljac;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lizu;->e:Ljac;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lizu;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 276
    iget-object v0, p0, Lizu;->b:Ljad;

    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v2

    iget-object v0, p0, Lizu;->b:Ljad;

    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v4

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit8 v0, v0, 0x1f

    .line 278
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lizu;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 279
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lizu;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 280
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 281
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lizu;->e:Ljac;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 282
    return v0

    .line 278
    :cond_0
    iget-object v0, p0, Lizu;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 279
    :cond_1
    iget-object v0, p0, Lizu;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 280
    :cond_2
    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_2

    .line 281
    :cond_3
    iget-object v1, p0, Lizu;->e:Ljac;

    invoke-virtual {v1}, Ljac;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lizu;->b:Ljad;

    invoke-virtual {v0}, Ljad;->c()Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lizu;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 11

    .prologue
    .line 319
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lizu;->b:Ljad;

    .line 320
    invoke-virtual {v0}, Ljad;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lizu;->a:Ljava/lang/String;

    iget-object v5, p0, Lizu;->c:Ljava/lang/String;

    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    .line 321
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lizu;->e:Ljac;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lizu;->f:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x19

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "@"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ti-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", u-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", l-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ty-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", s-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lizu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lizu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lizu;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 164
    :goto_0
    iget-object v0, p0, Lizu;->e:Ljac;

    iget v0, v0, Ljac;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 165
    iget-object v0, p0, Lizu;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lizu;->b:Ljad;

    invoke-virtual {v0, p1, p2}, Ljad;->writeToParcel(Landroid/os/Parcel;I)V

    .line 167
    return-void

    .line 162
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
