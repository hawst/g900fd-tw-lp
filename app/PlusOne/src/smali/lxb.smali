.class public final Llxb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxb;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4191
    const/4 v0, 0x0

    new-array v0, v0, [Llxb;

    sput-object v0, Llxb;->a:[Llxb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4192
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4195
    const/4 v0, 0x0

    iput-object v0, p0, Llxb;->b:Llxp;

    .line 4192
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 4224
    const/4 v0, 0x0

    .line 4225
    iget-object v1, p0, Llxb;->b:Llxp;

    if-eqz v1, :cond_0

    .line 4226
    const/4 v0, 0x1

    iget-object v1, p0, Llxb;->b:Llxp;

    .line 4227
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4229
    :cond_0
    iget-object v1, p0, Llxb;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 4230
    const/4 v1, 0x2

    iget-object v2, p0, Llxb;->c:Ljava/lang/Long;

    .line 4231
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4233
    :cond_1
    iget-object v1, p0, Llxb;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4234
    const/4 v1, 0x3

    iget-object v2, p0, Llxb;->d:Ljava/lang/String;

    .line 4235
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4237
    :cond_2
    iget-object v1, p0, Llxb;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4238
    const/4 v1, 0x4

    iget-object v2, p0, Llxb;->e:Ljava/lang/String;

    .line 4239
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4241
    :cond_3
    iget-object v1, p0, Llxb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4242
    iput v0, p0, Llxb;->ai:I

    .line 4243
    return v0
.end method

.method public a(Loxn;)Llxb;
    .locals 2

    .prologue
    .line 4251
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4252
    sparse-switch v0, :sswitch_data_0

    .line 4256
    iget-object v1, p0, Llxb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4257
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxb;->ah:Ljava/util/List;

    .line 4260
    :cond_1
    iget-object v1, p0, Llxb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4262
    :sswitch_0
    return-object p0

    .line 4267
    :sswitch_1
    iget-object v0, p0, Llxb;->b:Llxp;

    if-nez v0, :cond_2

    .line 4268
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxb;->b:Llxp;

    .line 4270
    :cond_2
    iget-object v0, p0, Llxb;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4274
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llxb;->c:Ljava/lang/Long;

    goto :goto_0

    .line 4278
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxb;->d:Ljava/lang/String;

    goto :goto_0

    .line 4282
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxb;->e:Ljava/lang/String;

    goto :goto_0

    .line 4252
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 4206
    iget-object v0, p0, Llxb;->b:Llxp;

    if-eqz v0, :cond_0

    .line 4207
    const/4 v0, 0x1

    iget-object v1, p0, Llxb;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4209
    :cond_0
    iget-object v0, p0, Llxb;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 4210
    const/4 v0, 0x2

    iget-object v1, p0, Llxb;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 4212
    :cond_1
    iget-object v0, p0, Llxb;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 4213
    const/4 v0, 0x3

    iget-object v1, p0, Llxb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4215
    :cond_2
    iget-object v0, p0, Llxb;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4216
    const/4 v0, 0x4

    iget-object v1, p0, Llxb;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4218
    :cond_3
    iget-object v0, p0, Llxb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4220
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4188
    invoke-virtual {p0, p1}, Llxb;->a(Loxn;)Llxb;

    move-result-object v0

    return-object v0
.end method
