.class public final Lcpu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[Lcpv;

.field private final b:Ljava/lang/Integer;

.field private final c:Ljava/lang/Integer;

.field private final d:Ljava/lang/Boolean;

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lfce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([Lcpv;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcpv;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Landroid/util/SparseArray",
            "<",
            "Lfce;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcpu;->a:[Lcpv;

    .line 27
    iput-object p2, p0, Lcpu;->b:Ljava/lang/Integer;

    .line 28
    iput-object p3, p0, Lcpu;->c:Ljava/lang/Integer;

    .line 29
    iput-object p4, p0, Lcpu;->d:Ljava/lang/Boolean;

    .line 30
    iput-object p5, p0, Lcpu;->e:Landroid/util/SparseArray;

    .line 31
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcpu;->a:[Lcpv;

    array-length v0, v0

    return v0
.end method

.method public a(I)J
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcpu;->a:[Lcpv;

    aget-object v0, v0, p1

    iget-wide v0, v0, Lcpv;->a:J

    return-wide v0
.end method

.method public b(I)J
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcpu;->a:[Lcpv;

    aget-object v0, v0, p1

    iget-wide v0, v0, Lcpv;->b:J

    return-wide v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcpu;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcpu;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method public d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcpu;->d:Ljava/lang/Boolean;

    return-object v0
.end method

.method public e()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lfce;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcpu;->e:Landroid/util/SparseArray;

    return-object v0
.end method
