.class public final Levi;
.super Lfbx;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lenf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lfbx;",
        "Landroid/view/View$OnClickListener;",
        "Lenf",
        "<",
        "Lizu;",
        ">;"
    }
.end annotation


# static fields
.field private static C:Levj;


# instance fields
.field private final A:Leqt;

.field private final B:Ldxq;

.field private D:I

.field private E:Z

.field private F:Landroid/view/View;

.field private G:Z

.field private H:Z

.field private final j:I

.field private final k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private final n:Z

.field private final o:Z

.field private p:I

.field private q:I

.field private r:I

.field private s:Lgu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgu",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:Landroid/util/SparseIntArray;

.field private u:Landroid/util/SparseIntArray;

.field private v:Lgu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgu",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private w:I

.field private x:J

.field private y:Z

.field private z:Levy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;ZLeqt;Ldxq;ZZLcom/google/android/apps/plus/views/FastScrollContainer;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 185
    invoke-direct {p0, p1, p2}, Lfbx;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 113
    const/4 v0, 0x6

    iput v0, p0, Levi;->p:I

    .line 152
    iput v1, p0, Levi;->D:I

    .line 161
    iput-boolean v1, p0, Levi;->E:Z

    .line 164
    iput-boolean v1, p0, Levi;->H:Z

    .line 186
    invoke-virtual {p0, p10}, Levi;->a(Lcom/google/android/apps/plus/views/FastScrollContainer;)V

    .line 187
    iput p3, p0, Levi;->j:I

    .line 188
    iput-object p4, p0, Levi;->k:Ljava/lang/String;

    .line 190
    sget-object v0, Levi;->C:Levj;

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Levj;

    invoke-direct {v0}, Levj;-><init>()V

    sput-object v0, Levi;->C:Levj;

    .line 194
    :cond_0
    iput-boolean p5, p0, Levi;->n:Z

    .line 195
    iput-object p6, p0, Levi;->A:Leqt;

    .line 196
    iput-boolean p9, p0, Levi;->o:Z

    .line 197
    iput-object p7, p0, Levi;->B:Ldxq;

    .line 198
    iput-boolean p8, p0, Levi;->y:Z

    .line 200
    sget-object v0, Lfvc;->k:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    new-instance v0, Levy;

    invoke-direct {v0, p1}, Levy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Levi;->z:Levy;

    .line 203
    :cond_1
    return-void
.end method

.method static synthetic a(Levi;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Levi;->m:Ljava/lang/String;

    return-object p1
.end method

.method private a(ILevj;)V
    .locals 4

    .prologue
    .line 1163
    iget-object v0, p0, Levi;->s:Lgu;

    invoke-virtual {v0}, Lgu;->b()I

    move-result v1

    .line 1164
    iget-object v0, p0, Levi;->s:Lgu;

    invoke-virtual {v0, p1}, Lgu;->g(I)I

    move-result v0

    .line 1166
    if-gez v0, :cond_1

    .line 1171
    xor-int/lit8 v0, v0, -0x1

    .line 1174
    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Levi;->s:Lgu;

    invoke-virtual {v2, v0}, Lgu;->e(I)I

    move-result v2

    if-ge v2, p1, :cond_0

    .line 1175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1177
    :cond_0
    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1180
    :goto_1
    if-ltz v0, :cond_1

    iget-object v1, p0, Levi;->s:Lgu;

    invoke-virtual {v1, v0}, Lgu;->e(I)I

    move-result v1

    if-le v1, p1, :cond_1

    .line 1181
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    move v2, v0

    .line 1185
    if-ltz v2, :cond_3

    .line 1186
    iget-object v0, p0, Levi;->s:Lgu;

    invoke-virtual {v0, v2}, Lgu;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1187
    iget-object v1, p0, Levi;->v:Lgu;

    invoke-virtual {v1, v2}, Lgu;->f(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1188
    iget-object v3, p0, Levi;->u:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseIntArray;->valueAt(I)I

    .line 1189
    iget-object v3, p0, Levi;->s:Lgu;

    invoke-virtual {v3, v2}, Lgu;->e(I)I

    move-result v2

    .line 1191
    sub-int v2, p1, v2

    invoke-virtual {p2, v0, v1, v2}, Levj;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1197
    :cond_2
    :goto_2
    return-void

    .line 1193
    :cond_3
    const-string v0, "EsTile"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1194
    const-string v0, "EsTile"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unable to find cluster ID for photo: cursorPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private a(Landroid/view/View;Landroid/database/Cursor;Lizu;)V
    .locals 8

    .prologue
    const v7, 0x7f10007f

    const/4 v2, 0x3

    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 1549
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1550
    const/16 v4, 0xf

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1551
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    const/4 v0, 0x0

    .line 1553
    :goto_0
    :sswitch_0
    packed-switch v0, :pswitch_data_0

    .line 1571
    :goto_1
    :pswitch_0
    const v1, 0x7f100079

    invoke-virtual {p1, v1, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1572
    const v1, 0x7f10007a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1573
    const v0, 0x7f100093

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1574
    iget-object v0, p0, Levi;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1575
    :pswitch_1
    return-void

    :sswitch_1
    move v0, v1

    .line 1551
    goto :goto_0

    :sswitch_2
    move v0, v2

    goto :goto_0

    .line 1555
    :pswitch_2
    iget-object v1, p0, Levi;->i:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1556
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1557
    const v2, 0x7f100092

    invoke-virtual {p1, v2, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1558
    invoke-virtual {p1, v7, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1

    .line 1562
    :pswitch_3
    const/4 v1, 0x4

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1563
    invoke-virtual {p1, v7, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1

    .line 1551
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x6e -> :sswitch_0
        0x29a -> :sswitch_2
    .end sparse-switch

    .line 1553
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/view/View;Landroid/database/Cursor;ZLizu;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1273
    const v0, 0x7f1001d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 1274
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    sget-object v4, Levi;->C:Levj;

    invoke-direct {p0, v3, v4}, Levi;->a(ILevj;)V

    .line 1276
    const v3, 0x7f100098

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1279
    invoke-static {p2}, Levi;->h(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz p3, :cond_0

    .line 1280
    invoke-virtual {v0, p4}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    .line 1285
    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoTileView;->c(Z)V

    .line 1287
    const v0, 0x7f100197

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1294
    iget-object v3, p0, Levi;->e:Lctz;

    invoke-virtual {v3}, Lctz;->a()Ljcn;

    move-result-object v4

    .line 1300
    sget-object v3, Levi;->C:Levj;

    iget-object v5, v3, Levj;->a:Ljava/lang/String;

    if-nez p3, :cond_1

    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-interface {p2, v1}, Landroid/database/Cursor;->move(I)Z

    iget-object v1, p0, Levi;->e:Lctz;

    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v7

    move v3, v2

    :goto_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p2}, Levi;->g(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p2}, Levi;->j(Landroid/database/Cursor;)Lizu;

    move-result-object v1

    new-instance v8, Ljug;

    invoke-direct {v8, v5}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljue;

    invoke-direct {v9, v1}, Ljue;-><init>(Lizu;)V

    invoke-virtual {v7, v8, v9}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v1

    check-cast v1, Ljuc;

    if-eqz v1, :cond_4

    add-int/lit8 v1, v3, 0x1

    :goto_3
    move v3, v1

    goto :goto_2

    .line 1282
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 1300
    goto :goto_1

    :cond_2
    invoke-interface {p2, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1302
    new-instance v1, Ljug;

    sget-object v5, Levi;->C:Levj;

    iget-object v5, v5, Levj;->a:Ljava/lang/String;

    invoke-direct {v1, v5}, Ljug;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljcn;->a(Ljcj;)I

    move-result v1

    sub-int/2addr v1, v3

    .line 1304
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1305
    if-lez v1, :cond_3

    :goto_4
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1306
    return-void

    .line 1305
    :cond_3
    const/16 v2, 0x8

    goto :goto_4

    :cond_4
    move v1, v3

    goto :goto_3
.end method

.method private a(Landroid/view/ViewGroup;I)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v1, 0x0

    .line 1107
    iget v2, p0, Levi;->r:I

    .line 1108
    iget v5, p0, Levi;->q:I

    .line 1109
    mul-int v0, v5, v2

    sub-int v3, p2, v0

    .line 1110
    if-lez v3, :cond_0

    const/4 v0, 0x1

    .line 1111
    :goto_0
    add-int v6, v5, v0

    .line 1114
    iget-object v4, p0, Levi;->f:Landroid/view/LayoutInflater;

    const v7, 0x7f040045

    invoke-virtual {v4, v7, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 1115
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1116
    iget v8, p0, Levi;->p:I

    iput v8, v7, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1117
    iget v8, p0, Levi;->p:I

    iput v8, v7, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1118
    invoke-virtual {v4, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1119
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1120
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1123
    add-int/lit8 v2, v2, -0x1

    move v4, v3

    move v3, v2

    move v2, v0

    :goto_1
    if-ltz v3, :cond_2

    .line 1124
    iget-object v0, p0, Levi;->f:Landroid/view/LayoutInflater;

    const v7, 0x7f04018e

    invoke-virtual {v0, v7, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 1125
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    add-int v0, v5, v2

    add-int v9, v5, v2

    invoke-direct {v8, v0, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1127
    if-nez v3, :cond_1

    move v0, v1

    :goto_2
    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1128
    iget v0, p0, Levi;->p:I

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1129
    invoke-virtual {v7, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1130
    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1131
    invoke-virtual {p1, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1132
    add-int/lit8 v4, v4, -0x1

    .line 1133
    if-nez v4, :cond_3

    move v0, v1

    .line 1123
    :goto_3
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    .line 1110
    goto :goto_0

    .line 1127
    :cond_1
    iget v0, p0, Levi;->p:I

    goto :goto_2

    .line 1139
    :cond_2
    iget-object v0, p0, Levi;->f:Landroid/view/LayoutInflater;

    const v2, 0x7f040059

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1140
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1141
    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1142
    iget v3, p0, Levi;->p:I

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1143
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1144
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1145
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1148
    iget-object v0, p0, Levi;->f:Landroid/view/LayoutInflater;

    const v2, 0x7f0400f8

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1149
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1150
    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1151
    iget v1, p0, Levi;->p:I

    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1152
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1153
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1154
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1155
    return-void

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method private a(Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 1442
    iget v0, p0, Levi;->r:I

    iget v0, p0, Levi;->D:I

    .line 1444
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 1446
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1447
    invoke-static {p1}, Levi;->g(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1448
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 1452
    const-wide/32 v4, 0x20000000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 1453
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/4 v0, 0x1

    .line 1459
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    throw v0
.end method

.method private c(I)I
    .locals 14

    .prologue
    .line 473
    iget-boolean v0, p0, Levi;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 474
    :cond_0
    const/4 v0, -0x1

    .line 587
    :goto_0
    return v0

    .line 477
    :cond_1
    iget-object v0, p0, Levi;->t:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_2

    .line 478
    iget-object v0, p0, Levi;->t:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    goto :goto_0

    .line 481
    :cond_2
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    .line 484
    iget v0, p0, Levi;->w:I

    add-int/lit8 v0, v0, 0xf

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 485
    iget v8, p0, Levi;->w:I

    .line 486
    iget v0, p0, Levi;->w:I

    add-int/lit8 v3, v0, -0x1

    .line 487
    const/4 v2, -0x1

    .line 489
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    iget-object v1, p0, Levi;->t:Landroid/util/SparseIntArray;

    iget v4, p0, Levi;->w:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 491
    const/4 v1, 0x0

    .line 496
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 497
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v4, 0x3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 499
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 507
    :cond_3
    const/4 v0, 0x0

    .line 515
    :cond_4
    :goto_1
    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_c

    if-lt v3, v7, :cond_5

    iget-boolean v4, p0, Levi;->n:Z

    if-eqz v4, :cond_c

    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v5, 0x3

    .line 517
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 519
    :cond_5
    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    .line 520
    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    invoke-static {v4}, Levi;->d(Landroid/database/Cursor;)Z

    move-result v10

    .line 524
    if-nez v10, :cond_6

    if-lt v9, v2, :cond_4

    .line 526
    :cond_6
    const/4 v5, 0x0

    .line 530
    const/4 v4, 0x0

    .line 532
    if-nez v10, :cond_8

    .line 533
    invoke-direct {p0, v9}, Levi;->d(I)I

    move-result v5

    .line 534
    iget-object v11, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v12, 0x3

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 536
    iget-boolean v12, p0, Levi;->n:Z

    if-eqz v12, :cond_8

    if-eqz v11, :cond_8

    .line 538
    invoke-static {v11}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_8

    .line 539
    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    move-result v12

    .line 542
    if-lez v1, :cond_a

    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    add-int/lit8 v13, v5, -0x1

    .line 543
    invoke-interface {v4, v13}, Landroid/database/Cursor;->move(I)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v13, 0x2

    .line 544
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v13, 0x2

    if-eq v4, v13, :cond_7

    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v13, 0x3

    .line 547
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 546
    invoke-static {v11, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    :cond_7
    const/4 v4, 0x1

    .line 550
    :goto_2
    iget-object v11, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v11, v12}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 554
    :cond_8
    if-nez v4, :cond_9

    .line 555
    const/4 v2, -0x1

    .line 556
    add-int/lit8 v3, v3, 0x1

    .line 557
    add-int/lit8 v1, v1, 0x1

    .line 559
    iget-object v4, p0, Levi;->t:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v3, v9}, Landroid/util/SparseIntArray;->put(II)V

    .line 564
    :cond_9
    if-eqz v10, :cond_b

    .line 565
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 566
    iget-object v1, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v4, 0x5

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 567
    iget-object v4, p0, Levi;->b:Landroid/database/Cursor;

    const/16 v5, 0x9

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 569
    iget-object v5, p0, Levi;->s:Lgu;

    invoke-virtual {v5, v9, v0}, Lgu;->b(ILjava/lang/Object;)V

    .line 570
    iget-object v5, p0, Levi;->u:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 571
    iget-object v4, p0, Levi;->v:Lgu;

    invoke-virtual {v4, v9, v1}, Lgu;->b(ILjava/lang/Object;)V

    .line 572
    const/4 v1, 0x0

    .line 574
    goto/16 :goto_1

    .line 546
    :cond_a
    const/4 v4, 0x0

    goto :goto_2

    .line 575
    :cond_b
    add-int v2, v9, v5

    .line 577
    goto/16 :goto_1

    .line 579
    :cond_c
    const/4 v0, 0x0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Levi;->w:I

    .line 581
    iget v0, p0, Levi;->w:I

    if-eq v8, v0, :cond_d

    .line 582
    invoke-virtual {p0}, Levi;->notifyDataSetChanged()V

    .line 585
    :cond_d
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v0, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 587
    iget-object v0, p0, Levi;->t:Landroid/util/SparseIntArray;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    goto/16 :goto_0
.end method

.method private d(I)I
    .locals 1

    .prologue
    .line 1622
    iget-object v0, p0, Levi;->s:Lgu;

    invoke-virtual {v0, p1}, Lgu;->g(I)I

    move-result v0

    if-ltz v0, :cond_0

    .line 1623
    const/4 v0, 0x0

    .line 1626
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Levi;->r:I

    goto :goto_0
.end method

.method private static d(Landroid/database/Cursor;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 591
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-static {p0}, Levi;->f(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Levi;->i(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method private e(Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 670
    if-nez p1, :cond_0

    .line 671
    invoke-virtual {p0, v3}, Levi;->a(Landroid/util/SparseArray;)V

    .line 709
    :goto_0
    return-void

    .line 677
    :cond_0
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    .line 678
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    .line 680
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 681
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x2

    .line 682
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v6, 0x4

    if-eq v2, v6, :cond_1

    .line 683
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 684
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 690
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    move-object v8, v2

    move v2, v0

    move-object v0, v8

    move-object v9, v3

    move v3, v1

    move-object v1, v9

    .line 692
    :cond_2
    iget v6, p0, Levi;->r:I

    rem-int v6, v3, v6

    if-nez v6, :cond_5

    .line 693
    const/16 v6, 0x10

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 694
    if-eqz v1, :cond_3

    invoke-virtual {v1, v0}, Lfce;->a(Ljava/util/Calendar;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 695
    :cond_3
    new-instance v1, Lfce;

    iget-object v6, p0, Levi;->c:Landroid/content/Context;

    invoke-direct {v1, v6, v0}, Lfce;-><init>(Landroid/content/Context;Ljava/util/Calendar;)V

    .line 696
    invoke-virtual {v4, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 697
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 700
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 702
    :cond_5
    add-int/lit8 v3, v3, 0x1

    .line 703
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 706
    :cond_6
    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 708
    invoke-virtual {p0, v4}, Levi;->a(Landroid/util/SparseArray;)V

    goto :goto_0
.end method

.method private static f(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 1599
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x6e

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static g(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 1604
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static h(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 1609
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static i(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 1614
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x29a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j(Landroid/database/Cursor;)Lizu;
    .locals 7

    .prologue
    const/16 v1, 0x13

    .line 1648
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1649
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1650
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1651
    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    .line 1653
    :goto_0
    invoke-static {v2, v3}, Ljvj;->a(J)Ljac;

    move-result-object v6

    .line 1656
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1658
    const-string v0, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1659
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1664
    :goto_1
    iget-object v1, p0, Levi;->c:Landroid/content/Context;

    invoke-static {v1, v4, v5, v0, v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 1672
    :goto_2
    return-object v0

    .line 1652
    :cond_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1661
    :cond_1
    iget-object v0, p0, Levi;->c:Landroid/content/Context;

    const-class v2, Lkdv;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 1662
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 1665
    :cond_2
    const-wide/32 v0, 0x40000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 1666
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1667
    iget-object v1, p0, Levi;->c:Landroid/content/Context;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2, v6, v0}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    goto :goto_2

    .line 1669
    :cond_3
    iget-object v0, p0, Levi;->c:Landroid/content/Context;

    invoke-static {v0, v4, v5, v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public a([I[Ljava/lang/String;[II)I
    .locals 13

    .prologue
    .line 321
    iget-object v1, p0, Levi;->s:Lgu;

    if-nez v1, :cond_0

    .line 322
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Must set list after cursor is set"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 325
    :cond_0
    iget v1, p0, Levi;->w:I

    if-eqz v1, :cond_1

    .line 326
    iget-object v1, p0, Levi;->s:Lgu;

    invoke-virtual {v1}, Lgu;->c()V

    .line 327
    iget-object v1, p0, Levi;->u:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 328
    iget-object v1, p0, Levi;->v:Lgu;

    invoke-virtual {v1}, Lgu;->c()V

    .line 329
    iget-object v1, p0, Levi;->t:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 330
    const/4 v1, 0x0

    iput v1, p0, Levi;->w:I

    .line 333
    :cond_1
    iget v1, p0, Levi;->D:I

    iget v1, p0, Levi;->r:I

    .line 335
    array-length v8, p1

    .line 336
    iget-object v1, p0, Levi;->b:Landroid/database/Cursor;

    if-nez v1, :cond_4

    const/4 v1, 0x0

    :goto_0
    add-int/lit8 v9, v1, -0x1

    .line 337
    const/4 v3, 0x0

    .line 338
    const/4 v2, 0x0

    .line 341
    iget-object v1, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    .line 344
    const/4 v1, 0x0

    move v7, v1

    move v1, v2

    move v2, v3

    :goto_1
    if-ge v7, v8, :cond_6

    .line 345
    aget v4, p1, v7

    .line 347
    if-gt v4, v9, :cond_6

    .line 348
    move/from16 v0, p4

    if-lt v0, v4, :cond_2

    move v1, v2

    .line 354
    :cond_2
    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 357
    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    invoke-static {v3}, Levi;->d(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v5, 0x4

    .line 358
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aget-object v5, p2, v7

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 360
    iget-object v3, p0, Levi;->s:Lgu;

    aget-object v5, p2, v7

    invoke-virtual {v3, v4, v5}, Lgu;->b(ILjava/lang/Object;)V

    .line 363
    iget-object v3, p0, Levi;->v:Lgu;

    iget-object v5, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v6, 0x5

    .line 364
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 363
    invoke-virtual {v3, v4, v5}, Lgu;->b(ILjava/lang/Object;)V

    .line 366
    add-int/lit8 v3, v8, -0x1

    if-ne v7, v3, :cond_8

    move v3, v4

    .line 378
    :goto_2
    iget-object v5, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v3, v5, :cond_5

    .line 380
    aget-object v5, p2, v7

    iget-object v6, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v11, 0x3

    invoke-interface {v6, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    aget-object v5, p2, v7

    iget-object v6, p0, Levi;->b:Landroid/database/Cursor;

    const/4 v11, 0x4

    .line 382
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 384
    :cond_3
    add-int/lit8 v3, v3, 0x1

    .line 388
    iget-object v5, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    .line 336
    :cond_4
    iget-object v1, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    goto/16 :goto_0

    .line 391
    :cond_5
    add-int/lit8 v3, v3, 0x1

    move v6, v3

    .line 396
    :goto_3
    if-gtz v6, :cond_9

    .line 403
    iput v2, p0, Levi;->w:I

    .line 404
    iget-object v3, p0, Levi;->t:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 435
    :cond_6
    iget v2, p0, Levi;->w:I

    if-eqz v2, :cond_7

    .line 436
    invoke-virtual {p0}, Levi;->notifyDataSetChanged()V

    .line 439
    :cond_7
    iget-object v2, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v2, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 440
    return v1

    .line 393
    :cond_8
    add-int/lit8 v3, v7, 0x1

    aget v3, p1, v3

    move v6, v3

    goto :goto_3

    :cond_9
    move v5, v4

    move v3, v2

    .line 408
    :goto_4
    if-ge v5, v6, :cond_e

    .line 409
    move/from16 v0, p4

    if-lt v0, v5, :cond_f

    move v2, v3

    .line 412
    :goto_5
    if-ne v5, v4, :cond_c

    const/4 v1, 0x1

    .line 416
    :goto_6
    iget-boolean v11, p0, Levi;->n:Z

    if-eqz v11, :cond_a

    add-int/lit8 v11, v4, 0x1

    if-le v5, v11, :cond_a

    add-int v11, v5, v1

    sub-int v11, v6, v11

    if-ltz v11, :cond_d

    .line 419
    :cond_a
    iput v3, p0, Levi;->w:I

    .line 422
    iget-object v11, p0, Levi;->t:Landroid/util/SparseIntArray;

    invoke-virtual {v11, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 424
    if-ne v5, v4, :cond_b

    .line 425
    iget-object v11, p0, Levi;->u:Landroid/util/SparseIntArray;

    aget v12, p3, v7

    invoke-virtual {v11, v3, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 430
    :cond_b
    add-int/2addr v1, v5

    .line 431
    add-int/lit8 v3, v3, 0x1

    move v5, v1

    move v1, v2

    .line 432
    goto :goto_4

    .line 412
    :cond_c
    invoke-direct {p0, v5}, Levi;->d(I)I

    move-result v1

    goto :goto_6

    :cond_d
    move v1, v2

    .line 344
    :cond_e
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v2, v3

    goto/16 :goto_1

    :cond_f
    move v2, v1

    goto :goto_5
.end method

.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1159
    iget-object v0, p0, Levi;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f040058

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(ILjava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<+",
            "Ljuf;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1473
    invoke-virtual {p0, p1}, Levi;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/database/Cursor;

    .line 1474
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1475
    iget v0, p0, Levi;->r:I

    iget v0, p0, Levi;->D:I

    .line 1479
    :cond_0
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1480
    invoke-static {v8}, Levi;->g(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1481
    invoke-static {v8}, Levi;->h(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v9

    .line 1497
    :goto_1
    iget-boolean v1, p0, Levi;->n:Z

    if-eqz v1, :cond_3

    .line 1498
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 1499
    iget v1, p0, Levi;->r:I

    rem-int v1, v0, v1

    .line 1500
    iget v2, p0, Levi;->r:I

    div-int v2, v0, v2

    .line 1501
    if-lez v2, :cond_3

    iget-object v3, p0, Levi;->u:Landroid/util/SparseIntArray;

    .line 1502
    invoke-virtual {v3, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    if-gt v3, v0, :cond_1

    if-eqz v1, :cond_3

    .line 1505
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Levi;->r:I

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    .line 1506
    invoke-virtual {v11, v10, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1510
    :goto_2
    return-object v0

    .line 1487
    :cond_2
    const/16 v0, 0xe

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1488
    const-wide/32 v0, 0x20000000

    and-long/2addr v0, v6

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1489
    const/16 v0, 0xf

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1492
    invoke-direct {p0, v8}, Levi;->j(Landroid/database/Cursor;)Lizu;

    move-result-object v3

    .line 1493
    new-instance v0, Ljuc;

    new-array v1, v9, [Ljava/lang/String;

    iget-object v2, p0, Levi;->k:Ljava/lang/String;

    aput-object v2, v1, v10

    invoke-static {v10, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move-object v0, v11

    .line 1510
    goto :goto_2

    :cond_4
    move v0, v10

    goto :goto_1
.end method

.method public a(II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1070
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1071
    :goto_0
    if-ge p1, p2, :cond_2

    .line 1072
    invoke-virtual {p0, p1}, Levi;->getItemViewType(I)I

    move-result v0

    .line 1073
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1074
    invoke-direct {p0, p1}, Levi;->c(I)I

    move-result v1

    move v0, v1

    .line 1078
    :goto_1
    iget v3, p0, Levi;->r:I

    add-int/2addr v3, v1

    if-ge v0, v3, :cond_1

    .line 1079
    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1080
    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    invoke-static {v3}, Levi;->g(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1081
    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    invoke-direct {p0, v3}, Levi;->j(Landroid/database/Cursor;)Lizu;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1078
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1071
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1085
    :cond_2
    return-object v2
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 271
    iput p1, p0, Levi;->p:I

    .line 272
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1719
    iput-object p1, p0, Levi;->F:Landroid/view/View;

    .line 1720
    const/4 v0, 0x1

    iput-boolean v0, p0, Levi;->G:Z

    .line 1721
    invoke-virtual {p0}, Levi;->notifyDataSetChanged()V

    .line 1722
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 244
    iput-boolean p1, p0, Levi;->H:Z

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 596
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 449
    .line 450
    :goto_0
    if-lez p1, :cond_1

    .line 451
    iget-object v0, p0, Levi;->t:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_0

    .line 452
    iget-object v0, p0, Levi;->t:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 456
    :goto_1
    return v0

    .line 454
    :cond_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 456
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v4, 0x0

    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 615
    if-nez p1, :cond_4

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 616
    :goto_0
    const-string v3, "resume_token"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Levi;->l:Ljava/lang/String;

    .line 617
    const-string v3, "last_refresh_time"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 619
    new-instance v0, Ljvl;

    iget-object v3, p0, Levi;->c:Landroid/content/Context;

    invoke-direct {v0, v3}, Ljvl;-><init>(Landroid/content/Context;)V

    .line 620
    iget v0, v0, Ljvl;->a:I

    iput v0, p0, Levi;->r:I

    .line 621
    iget-wide v8, p0, Levi;->x:J

    cmp-long v0, v8, v6

    if-eqz v0, :cond_5

    move v0, v1

    .line 622
    :goto_1
    iget-boolean v3, p0, Levi;->o:Z

    if-nez v3, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v3, p0, Levi;->a:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    if-eqz v3, :cond_0

    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    .line 626
    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    if-eq p1, v0, :cond_6

    .line 628
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_6

    :cond_0
    move v0, v1

    .line 630
    :goto_2
    iput-wide v6, p0, Levi;->x:J

    .line 631
    iget-boolean v3, p0, Levi;->H:Z

    if-eqz v3, :cond_1

    if-eqz p1, :cond_1

    .line 634
    new-instance v5, Lhym;

    sget-object v3, Levl;->b:[Ljava/lang/String;

    invoke-direct {v5, v3}, Lhym;-><init>([Ljava/lang/String;)V

    sget-object v3, Levl;->b:[Ljava/lang/String;

    const/16 v3, 0x16

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v2

    const/16 v6, 0x6e

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v10

    const-string v6, "~promo"

    aput-object v6, v3, v1

    const-string v6, "~promo"

    aput-object v6, v3, v11

    invoke-virtual {v5, v3}, Lhym;->a([Ljava/lang/Object;)V

    new-instance v3, Landroid/database/MergeCursor;

    new-array v6, v10, [Landroid/database/Cursor;

    aput-object v5, v6, v2

    aput-object p1, v6, v1

    invoke-direct {v3, v6}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    move-object p1, v3

    .line 638
    :cond_1
    iget-boolean v3, p0, Levi;->H:Z

    if-eqz v3, :cond_2

    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 639
    new-instance v5, Lhym;

    sget-object v3, Levl;->b:[Ljava/lang/String;

    invoke-direct {v5, v3}, Lhym;-><init>([Ljava/lang/String;)V

    sget-object v3, Levl;->b:[Ljava/lang/String;

    const/16 v3, 0x16

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v2

    const/16 v6, 0x29a

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v10

    const-string v6, "~autobackup"

    aput-object v6, v3, v1

    const-string v6, "~autobackup"

    aput-object v6, v3, v11

    invoke-virtual {v5, v3}, Lhym;->a([Ljava/lang/Object;)V

    new-instance v3, Landroid/database/MergeCursor;

    new-array v6, v10, [Landroid/database/Cursor;

    aput-object v5, v6, v2

    aput-object p1, v6, v1

    invoke-direct {v3, v6}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    move-object p1, v3

    .line 642
    :cond_2
    invoke-virtual {p0}, Levi;->f()Lcom/google/android/apps/plus/views/FastScrollContainer;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 643
    invoke-direct {p0, p1}, Levi;->e(Landroid/database/Cursor;)V

    .line 646
    :cond_3
    if-eqz v0, :cond_b

    .line 647
    if-nez p1, :cond_7

    move-object v0, v4

    :goto_3
    iput-object v0, p0, Levi;->s:Lgu;

    .line 648
    if-nez p1, :cond_8

    move-object v0, v4

    :goto_4
    iput-object v0, p0, Levi;->t:Landroid/util/SparseIntArray;

    .line 649
    if-nez p1, :cond_9

    move-object v0, v4

    .line 650
    :goto_5
    iput-object v0, p0, Levi;->u:Landroid/util/SparseIntArray;

    .line 651
    if-nez p1, :cond_a

    :goto_6
    iput-object v4, p0, Levi;->v:Lgu;

    .line 652
    iput v2, p0, Levi;->w:I

    .line 654
    invoke-super {p0, p1}, Lfbx;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 662
    :goto_7
    return-object v0

    .line 615
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 621
    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 628
    goto/16 :goto_2

    .line 647
    :cond_7
    new-instance v0, Lgu;

    invoke-direct {v0}, Lgu;-><init>()V

    goto :goto_3

    .line 648
    :cond_8
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    goto :goto_4

    .line 649
    :cond_9
    new-instance v0, Landroid/util/SparseIntArray;

    .line 650
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    goto :goto_5

    .line 651
    :cond_a
    new-instance v4, Lgu;

    invoke-direct {v4}, Lgu;-><init>()V

    goto :goto_6

    .line 662
    :cond_b
    invoke-super {p0, p1}, Lfbx;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_7
.end method

.method public c()Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<[I[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    iget-object v0, p0, Levi;->s:Lgu;

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 289
    :goto_0
    return-object v0

    .line 283
    :cond_0
    iget-object v0, p0, Levi;->s:Lgu;

    invoke-virtual {v0}, Lgu;->b()I

    move-result v0

    new-array v2, v0, [I

    .line 284
    iget-object v0, p0, Levi;->s:Lgu;

    invoke-virtual {v0}, Lgu;->b()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 285
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    .line 286
    iget-object v0, p0, Levi;->s:Lgu;

    invoke-virtual {v0, v1}, Lgu;->e(I)I

    move-result v0

    aput v0, v2, v1

    .line 287
    iget-object v0, p0, Levi;->s:Lgu;

    invoke-virtual {v0, v1}, Lgu;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 285
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 289
    :cond_1
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public d()[I
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Levi;->u:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 297
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 298
    iget-object v2, p0, Levi;->u:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v2

    aput v2, v1, v0

    .line 297
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 301
    :cond_0
    return-object v1
.end method

.method public e()V
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Levi;->z:Levy;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Levi;->z:Levy;

    invoke-virtual {v0}, Levy;->a()V

    .line 464
    :cond_0
    invoke-super {p0}, Lfbx;->e()V

    .line 465
    return-void
.end method

.method public getCount()I
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 742
    iget-boolean v0, p0, Levi;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Levi;->G:Z

    if-nez v0, :cond_1

    .line 783
    :cond_0
    :goto_0
    return v2

    .line 746
    :cond_1
    iget-object v0, p0, Levi;->t:Landroid/util/SparseIntArray;

    iget v1, p0, Levi;->w:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v5

    .line 747
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 749
    iget-boolean v1, p0, Levi;->n:Z

    if-eqz v1, :cond_2

    iget v1, p0, Levi;->w:I

    if-lez v1, :cond_2

    .line 750
    invoke-direct {p0, v5}, Levi;->d(I)I

    move-result v1

    .line 751
    shl-int/lit8 v4, v1, 0x1

    add-int/2addr v4, v5

    iget-object v6, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-le v4, v6, :cond_2

    .line 752
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/2addr v1, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 756
    :cond_2
    iget v1, p0, Levi;->w:I

    if-nez v1, :cond_4

    move v1, v2

    .line 770
    :goto_1
    iget-object v4, p0, Levi;->l:Ljava/lang/String;

    if-nez v4, :cond_6

    move v4, v2

    :goto_2
    add-int/2addr v0, v4

    .line 772
    sub-int v4, v0, v5

    if-le v4, v1, :cond_7

    .line 774
    :goto_3
    if-eqz v3, :cond_3

    sub-int v2, v0, v5

    sub-int/2addr v2, v1

    :cond_3
    add-int/lit8 v1, v2, 0x1

    .line 777
    iget v2, p0, Levi;->w:I

    add-int/2addr v2, v1

    .line 779
    const-string v1, "EsTile"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 780
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x30

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "#getCount; curCnt: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cnt: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 756
    :cond_4
    iget-object v1, p0, Levi;->s:Lgu;

    .line 768
    invoke-virtual {v1, v5}, Lgu;->g(I)I

    move-result v1

    if-ltz v1, :cond_5

    move v1, v3

    goto :goto_1

    .line 769
    :cond_5
    invoke-direct {p0, v5}, Levi;->d(I)I

    move-result v1

    goto :goto_1

    :cond_6
    move v4, v3

    .line 770
    goto :goto_2

    :cond_7
    move v3, v2

    .line 772
    goto :goto_3
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 820
    invoke-direct {p0, p1}, Levi;->c(I)I

    move-result v0

    .line 821
    const-string v1, "EsTile"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 822
    iget-boolean v1, p0, Levi;->a:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Levi;->b:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 823
    iget-object v1, p0, Levi;->b:Landroid/database/Cursor;

    .line 824
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x44

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "#getItem; pos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", curPos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", curCnt: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 830
    :cond_0
    :goto_0
    iget-boolean v1, p0, Levi;->a:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Levi;->b:Landroid/database/Cursor;

    if-eqz v1, :cond_2

    if-ltz v0, :cond_2

    .line 831
    iget-object v1, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 832
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    .line 834
    :goto_1
    return-object v0

    .line 826
    :cond_1
    iget-boolean v1, p0, Levi;->a:Z

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "#getItem; is data valid? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 834
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 840
    invoke-direct {p0, p1}, Levi;->c(I)I

    move-result v2

    .line 841
    iget-boolean v3, p0, Levi;->a:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    if-eqz v3, :cond_0

    if-ltz v2, :cond_0

    .line 842
    iget-object v3, p0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v3, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 843
    iget-object v0, p0, Levi;->b:Landroid/database/Cursor;

    iget v1, p0, Levi;->d:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 848
    :cond_0
    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 5

    .prologue
    const/4 v1, 0x3

    .line 793
    invoke-direct {p0, p1}, Levi;->c(I)I

    move-result v0

    .line 794
    const-string v2, "EsTile"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 795
    iget-boolean v2, p0, Levi;->a:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Levi;->b:Landroid/database/Cursor;

    if-eqz v2, :cond_2

    .line 796
    iget-object v2, p0, Levi;->b:Landroid/database/Cursor;

    .line 797
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x4c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "#getItemViewType; pos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", curPos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", curCnt: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 802
    :cond_0
    :goto_0
    iget-boolean v2, p0, Levi;->a:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Levi;->b:Landroid/database/Cursor;

    if-eqz v2, :cond_1

    if-gez v0, :cond_3

    .line 803
    :cond_1
    const/4 v0, -0x1

    .line 814
    :goto_1
    return v0

    .line 799
    :cond_2
    iget-boolean v2, p0, Levi;->a:Z

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "#getItemViewType; is data valid? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 805
    :cond_3
    invoke-virtual {p0, p1}, Levi;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 807
    invoke-static {v0}, Levi;->g(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v0}, Levi;->h(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 808
    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    .line 809
    :cond_5
    invoke-static {v0}, Levi;->f(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 810
    const/4 v0, 0x2

    goto :goto_1

    .line 811
    :cond_6
    invoke-static {v0}, Levi;->i(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 812
    goto :goto_1

    .line 814
    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 32
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi",
            "NewApi"
        }
    .end annotation

    .prologue
    .line 874
    move-object/from16 v0, p0

    iget-boolean v2, v0, Levi;->a:Z

    if-nez v2, :cond_0

    .line 875
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "this should only be called when the cursor is valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 878
    :cond_0
    if-nez p3, :cond_5

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Levi;->q:I

    if-nez v3, :cond_1

    if-lez v2, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Levi;->p:I

    move-object/from16 v0, p0

    iget v5, v0, Levi;->r:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v4, v5

    sub-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Levi;->r:I

    div-int/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Levi;->q:I

    :cond_1
    const-string v2, "EsTile"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Levi;->r:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x38

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "#calcColumnCount; count: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", width: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 879
    :cond_2
    invoke-direct/range {p0 .. p1}, Levi;->c(I)I

    move-result v22

    .line 880
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v23

    .line 882
    const-string v2, "EsTile"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 883
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "#getView; pos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", curStart: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 886
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Levi;->E:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->l:Ljava/lang/String;

    if-eqz v2, :cond_4

    sub-int v2, v23, v22

    const/16 v3, 0x64

    if-ge v2, v3, :cond_4

    .line 888
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->l:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 889
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Levi;->m:Ljava/lang/String;

    .line 890
    new-instance v2, Levk;

    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->c:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v4, v0, Levi;->j:I

    move-object/from16 v0, p0

    iget-object v5, v0, Levi;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v4, v5, v0}, Levk;-><init>(Landroid/content/Context;ILjava/lang/String;Levi;)V

    .line 892
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_6

    .line 893
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Levi;->l:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 900
    :cond_4
    :goto_1
    invoke-virtual/range {p0 .. p1}, Levi;->getItemViewType(I)I

    move-result v3

    .line 901
    const/4 v2, 0x3

    if-ne v3, v2, :cond_7

    .line 902
    if-nez p2, :cond_3c

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->f:Landroid/view/LayoutInflater;

    const v3, 0x7f040050

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    :goto_2
    check-cast v2, Lcom/google/android/apps/plus/views/AutoBackupBarView;

    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->B:Ldxq;

    move-object/from16 v0, p0

    iget-object v4, v0, Levi;->c:Landroid/content/Context;

    invoke-interface {v3, v4, v2}, Ldxq;->a(Landroid/content/Context;Lcom/google/android/apps/plus/views/AutoBackupBarView;)V

    .line 1058
    :goto_3
    return-object v2

    .line 878
    :cond_5
    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v2

    goto/16 :goto_0

    .line 895
    :cond_6
    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Levi;->l:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 905
    :cond_7
    if-nez v3, :cond_e

    const/4 v2, 0x1

    move v4, v2

    .line 906
    :goto_4
    const/4 v2, 0x2

    if-ne v3, v2, :cond_f

    const/4 v2, 0x1

    move v3, v2

    .line 909
    :goto_5
    if-nez p2, :cond_11

    .line 910
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->c:Landroid/content/Context;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v5, v1}, Levi;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 911
    if-eqz v4, :cond_10

    .line 912
    move-object/from16 v0, p0

    iget-object v5, v0, Levi;->f:Landroid/view/LayoutInflater;

    const v6, 0x7f040056

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object/from16 v18, v2

    .line 920
    :goto_6
    if-eqz v4, :cond_1c

    .line 921
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Levi;->b:Landroid/database/Cursor;

    const v2, 0x7f1001c7

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f100174

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const v4, 0x7f1001c9

    invoke-virtual {v7, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    const v5, 0x7f1001c8

    invoke-virtual {v7, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    const v6, 0x7f1001cb

    invoke-virtual {v7, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    const/4 v9, 0x5

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v9, 0x1

    invoke-direct {v2, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/16 v10, 0x9

    invoke-interface {v8, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-nez v10, :cond_8

    const/16 v10, 0x9

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Levi;->c:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f11003b

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v10, v13}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v11, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v12, 0x21

    invoke-virtual {v9, v2, v11, v10, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_8
    const/16 v2, 0xe

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/4 v2, 0x4

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v12

    invoke-static {v9}, Ljvj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v13, "PLUS_EVENT"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-wide/16 v14, 0x200

    and-long/2addr v10, v14

    const-wide/16 v14, 0x0

    cmp-long v10, v10, v14

    if-eqz v10, :cond_12

    if-nez v2, :cond_12

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Levi;->a(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Levi;->g:Lctq;

    invoke-virtual {v11}, Lctq;->c()I

    move-result v11

    const/4 v13, 0x1

    if-eq v11, v13, :cond_15

    if-nez v11, :cond_13

    if-nez v10, :cond_9

    if-eqz v2, :cond_13

    :cond_9
    if-nez v12, :cond_13

    const/16 v10, 0x9

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v13, v0, Levi;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v13}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Levi;->i:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v4, v13}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const v13, 0x7f100080

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v13, v10}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    const v10, 0x7f10007f

    invoke-virtual {v4, v10, v9}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    const v10, 0x7f100083

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v10, v2}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    const v2, 0x7f100081

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v2, v10}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v2, 0x8

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_8
    if-eqz v12, :cond_c

    invoke-static {v9}, Ljvj;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v11, :cond_16

    const/4 v2, 0x5

    if-eq v11, v2, :cond_16

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p0

    iget-boolean v4, v0, Levi;->y:Z

    if-eqz v4, :cond_1a

    if-nez v2, :cond_1a

    const v2, 0x7f10007e

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_a
    const/4 v2, 0x1

    move v4, v2

    :goto_a
    if-eqz v4, :cond_b

    const v2, 0x7f1001ca

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->clearAnimation()V

    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_b
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f10007e

    invoke-virtual {v6, v2, v5}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    const v2, 0x7f10007f

    invoke-virtual {v6, v2, v9}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {v9}, Ljvj;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    const v2, 0x7f020158

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_c
    :goto_b
    const/16 v2, 0x11

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x12

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1b

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1b

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setClickable(Z)V

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_c
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8, v2}, Levi;->a(Landroid/view/View;Landroid/database/Cursor;Lizu;)V

    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_d
    :goto_d
    move-object/from16 v2, v18

    .line 1058
    goto/16 :goto_3

    .line 905
    :cond_e
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_4

    .line 906
    :cond_f
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_5

    .line 913
    :cond_10
    if-nez v3, :cond_3b

    .line 914
    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Levi;->a(Landroid/view/ViewGroup;I)V

    move-object/from16 v18, v2

    goto/16 :goto_6

    .line 917
    :cond_11
    check-cast p2, Landroid/view/ViewGroup;

    move-object/from16 v18, p2

    goto/16 :goto_6

    .line 921
    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_13
    if-eqz v11, :cond_14

    const/4 v2, 0x2

    if-eq v11, v2, :cond_14

    const/4 v2, 0x4

    if-ne v11, v2, :cond_15

    :cond_14
    if-eqz v10, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f10007f

    invoke-virtual {v5, v2, v9}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    const v2, 0x7f100081

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v5, v2, v10}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v2, 0x8

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_8

    :cond_15
    const/16 v2, 0x8

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v2, 0x8

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_8

    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_17
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_a

    :cond_18
    const/4 v2, 0x0

    invoke-interface {v2, v5}, Lhqo;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    if-eqz v4, :cond_c

    const v2, 0x7f02005a

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_b

    :cond_19
    const v2, 0x7f02004a

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_b

    :cond_1a
    const/16 v2, 0x8

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_b

    :cond_1b
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    goto/16 :goto_c

    .line 922
    :cond_1c
    if-eqz v3, :cond_1f

    .line 923
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 924
    if-eqz v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->F:Landroid/view/View;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    .line 926
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 927
    const/4 v2, 0x0

    .line 929
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->F:Landroid/view/View;

    if-eqz v3, :cond_d

    if-nez v2, :cond_d

    .line 930
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->F:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 931
    if-eqz v2, :cond_1e

    .line 932
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->F:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 934
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->F:Landroid/view/View;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_d

    .line 938
    :cond_1f
    move-object/from16 v0, p0

    iget v0, v0, Levi;->r:I

    move/from16 v24, v0

    .line 939
    invoke-virtual/range {v18 .. v18}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v25, v2, -0x1

    .line 940
    add-int/lit8 v26, v25, -0x1

    .line 942
    const/4 v6, 0x1

    .line 944
    const/4 v4, 0x0

    .line 945
    const/4 v3, 0x0

    .line 950
    move-object/from16 v0, p0

    iget-boolean v2, v0, Levi;->n:Z

    if-eqz v2, :cond_23

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->t:Landroid/util/SparseIntArray;

    .line 952
    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, p1

    if-eq v0, v2, :cond_20

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->s:Lgu;

    move-object/from16 v0, p0

    iget-object v5, v0, Levi;->t:Landroid/util/SparseIntArray;

    add-int/lit8 v7, p1, 0x1

    .line 953
    invoke-virtual {v5, v7}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    invoke-virtual {v2, v5}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_23

    :cond_20
    const/4 v2, 0x1

    move/from16 v21, v2

    .line 957
    :goto_e
    if-eqz v21, :cond_3a

    .line 959
    :goto_f
    if-ltz p1, :cond_3a

    .line 960
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->t:Landroid/util/SparseIntArray;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    .line 961
    move-object/from16 v0, p0

    iget-object v5, v0, Levi;->s:Lgu;

    invoke-virtual {v5, v2}, Lgu;->g(I)I

    move-result v5

    if-ltz v5, :cond_24

    .line 963
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->u:Landroid/util/SparseIntArray;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    move v12, v2

    move v13, v3

    .line 969
    :goto_10
    const/16 v19, 0x0

    .line 970
    const/16 v16, 0x0

    .line 971
    const/4 v15, 0x0

    .line 972
    const/4 v2, 0x0

    move/from16 v20, v2

    :goto_11
    move/from16 v0, v20

    move/from16 v1, v24

    if-ge v0, v1, :cond_38

    .line 973
    add-int v2, v22, v20

    .line 975
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v3, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 977
    move/from16 v0, v23

    if-eq v2, v0, :cond_21

    if-gez v2, :cond_25

    .line 978
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->l:Ljava/lang/String;

    if-eqz v2, :cond_38

    .line 979
    const/4 v2, 0x1

    .line 980
    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1039
    :goto_12
    if-nez v16, :cond_22

    .line 1040
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1044
    :cond_22
    :goto_13
    move/from16 v0, v24

    if-gt v6, v0, :cond_36

    .line 1045
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1044
    add-int/lit8 v6, v6, 0x1

    goto :goto_13

    .line 953
    :cond_23
    const/4 v2, 0x0

    move/from16 v21, v2

    goto :goto_e

    .line 959
    :cond_24
    add-int/lit8 p1, p1, -0x1

    goto :goto_f

    .line 986
    :cond_25
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->b:Landroid/database/Cursor;

    invoke-static {v3}, Levi;->g(Landroid/database/Cursor;)Z

    move-result v3

    .line 987
    move-object/from16 v0, p0

    iget-object v4, v0, Levi;->b:Landroid/database/Cursor;

    invoke-static {v4}, Levi;->h(Landroid/database/Cursor;)Z

    move-result v4

    .line 989
    if-eqz v3, :cond_27

    move-object/from16 v0, p0

    iget-object v5, v0, Levi;->b:Landroid/database/Cursor;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Levi;->j(Landroid/database/Cursor;)Lizu;

    move-result-object v5

    .line 992
    :goto_14
    if-eqz v3, :cond_35

    .line 993
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->b:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    sget-object v4, Levi;->C:Levj;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Levi;->a(ILevj;)V

    .line 995
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->b:Landroid/database/Cursor;

    const/4 v4, 0x1

    .line 996
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "~folders"

    .line 995
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    .line 1002
    move-object/from16 v0, p0

    iget v4, v0, Levi;->r:I

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v20

    if-ne v0, v4, :cond_26

    sget-object v4, Levi;->C:Levj;

    iget-object v4, v4, Levj;->a:Ljava/lang/String;

    .line 1009
    :cond_26
    add-int/lit8 v4, v24, -0x1

    move/from16 v0, v20

    if-ne v0, v4, :cond_28

    if-eqz v21, :cond_28

    sub-int/2addr v2, v12

    if-le v13, v2, :cond_28

    const/4 v2, 0x1

    :goto_15
    or-int/lit8 v2, v2, 0x0

    .line 1012
    if-eqz v3, :cond_2a

    .line 1013
    const/4 v4, 0x1

    .line 1014
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 1015
    const v2, 0x7f100196

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PhotoTileView;

    const v3, 0x7f100099

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v11, v3, v7}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    const v2, 0x7f100197

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->e:Lctz;

    invoke-virtual {v3}, Lctz;->a()Ljcn;

    move-result-object v3

    invoke-virtual {v3}, Ljcn;->l()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-lez v3, :cond_29

    const/4 v3, 0x0

    :goto_16
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    move v2, v15

    move v3, v4

    move v4, v6

    .line 1032
    :goto_17
    move-object/from16 v0, p0

    iget-object v6, v0, Levi;->b:Landroid/database/Cursor;

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v6, v5}, Levi;->a(Landroid/view/View;Landroid/database/Cursor;Lizu;)V

    .line 1033
    const/4 v5, 0x0

    invoke-virtual {v11, v5}, Landroid/view/View;->setVisibility(I)V

    .line 972
    add-int/lit8 v5, v20, 0x1

    move/from16 v20, v5

    move v15, v2

    move/from16 v16, v3

    move v6, v4

    goto/16 :goto_11

    .line 989
    :cond_27
    const/4 v5, 0x0

    goto/16 :goto_14

    .line 1009
    :cond_28
    const/4 v2, 0x0

    goto :goto_15

    .line 1015
    :cond_29
    const/16 v3, 0x8

    goto :goto_16

    .line 1016
    :cond_2a
    if-eqz v2, :cond_2b

    .line 1017
    const/4 v2, 0x1

    .line 1018
    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 1019
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->c:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->b:Landroid/database/Cursor;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v4, v5}, Levi;->a(Landroid/view/View;Landroid/database/Cursor;ZLizu;)V

    move/from16 v3, v16

    move v4, v6

    goto :goto_17

    .line 1021
    :cond_2b
    add-int/lit8 v17, v6, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 1022
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->c:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->b:Landroid/database/Cursor;

    move-object v10, v11

    check-cast v10, Lcom/google/android/apps/plus/views/PhotoTileView;

    const/high16 v2, 0x10000

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->i(I)V

    invoke-virtual {v10, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    const/16 v2, 0xa

    invoke-interface {v3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_2f

    const/4 v2, 0x0

    :goto_18
    if-lez v2, :cond_30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Ljava/lang/Integer;)V

    :goto_19
    const/16 v2, 0xb

    invoke-interface {v3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_31

    const/4 v2, 0x0

    :goto_1a
    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljava/lang/Integer;)V

    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->c(Z)V

    const/16 v2, 0xf

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/16 v2, 0xe

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-wide v28, 0x200000000L

    and-long v28, v28, v8

    const-wide/16 v30, 0x0

    cmp-long v2, v28, v30

    if-nez v2, :cond_2c

    const-wide/32 v28, 0x20000000

    and-long v28, v28, v8

    const-wide/16 v30, 0x0

    cmp-long v2, v28, v30

    if-eqz v2, :cond_32

    const-wide/16 v28, 0x4000

    and-long v28, v28, v6

    const-wide/16 v30, 0x0

    cmp-long v2, v28, v30

    if-eqz v2, :cond_32

    :cond_2c
    const/4 v2, 0x1

    move v14, v2

    :goto_1b
    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    sget-object v3, Levi;->C:Levj;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Levi;->a(ILevj;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->e:Lctz;

    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    new-instance v3, Ljug;

    sget-object v4, Levi;->C:Levj;

    iget-object v4, v4, Levj;->a:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljue;

    invoke-direct {v4, v5}, Ljue;-><init>(Lizu;)V

    invoke-virtual {v2, v3, v4}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v2

    check-cast v2, Ljuc;

    if-nez v2, :cond_39

    new-instance v2, Ljuc;

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Levi;->k:Ljava/lang/String;

    move-object/from16 v28, v0

    aput-object v28, v4, v27

    invoke-static {v3, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Levi;->C:Levj;

    iget-object v4, v4, Levj;->a:Ljava/lang/String;

    invoke-direct/range {v2 .. v9}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    move-object v4, v2

    :goto_1c
    const-wide/16 v2, 0x100

    and-long/2addr v2, v6

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-eqz v2, :cond_33

    const/4 v2, 0x1

    :goto_1d
    invoke-virtual {v4}, Ljuc;->h()Lnzi;

    move-result-object v3

    if-nez v3, :cond_34

    const/4 v3, 0x0

    :goto_1e
    invoke-virtual {v10, v5, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;Lizo;)V

    invoke-virtual {v10, v4}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljcl;)V

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->d(Z)V

    invoke-virtual {v10, v14}, Lcom/google/android/apps/plus/views/PhotoTileView;->n(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->z:Levy;

    if-eqz v2, :cond_2d

    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->z:Levy;

    invoke-virtual {v2, v10, v5}, Levy;->a(Lcom/google/android/apps/plus/views/PhotoTileView;Lizu;)V

    :cond_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->A:Leqt;

    if-eqz v2, :cond_2e

    move-object/from16 v0, p0

    iget-object v2, v0, Levi;->A:Leqt;

    invoke-interface {v2, v5, v10}, Leqt;->a(Lizu;Lcom/google/android/apps/plus/views/PhotoTileView;)V

    :cond_2e
    move v2, v15

    move/from16 v3, v16

    move/from16 v4, v17

    .line 1024
    goto/16 :goto_17

    .line 1022
    :cond_2f
    const/16 v2, 0xa

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    goto/16 :goto_18

    :cond_30
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Ljava/lang/Integer;)V

    goto/16 :goto_19

    :cond_31
    const/16 v2, 0xb

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_1a

    :cond_32
    const/4 v2, 0x0

    move v14, v2

    goto/16 :goto_1b

    :cond_33
    const/4 v2, 0x0

    goto :goto_1d

    :cond_34
    new-instance v3, Ljub;

    invoke-virtual {v4}, Ljuc;->h()Lnzi;

    move-result-object v6

    invoke-direct {v3, v6}, Ljub;-><init>(Lnzi;)V

    goto :goto_1e

    .line 1024
    :cond_35
    if-eqz v4, :cond_38

    .line 1025
    const/4 v2, 0x1

    .line 1027
    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 1028
    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->c:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Levi;->b:Landroid/database/Cursor;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v4, v5}, Levi;->a(Landroid/view/View;Landroid/database/Cursor;ZLizu;)V

    move/from16 v3, v16

    move v4, v6

    goto/16 :goto_17

    .line 1049
    :cond_36
    if-nez v15, :cond_37

    .line 1050
    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1054
    :cond_37
    if-nez v2, :cond_d

    .line 1055
    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_d

    :cond_38
    move/from16 v2, v19

    goto/16 :goto_12

    :cond_39
    move-object v4, v2

    goto/16 :goto_1c

    :cond_3a
    move v12, v3

    move v13, v4

    goto/16 :goto_10

    :cond_3b
    move-object/from16 v18, v2

    goto/16 :goto_6

    :cond_3c
    move-object/from16 v2, p2

    goto/16 :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 788
    const/4 v0, 0x5

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 606
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 601
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1540
    return-void
.end method
