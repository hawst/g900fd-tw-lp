.class public final Leqf;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private Q:Lhee;

.field private R:Landroid/view/ContextThemeWrapper;

.field private S:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Leqh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lloj;-><init>()V

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Leqf;->S:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Leqf;)Landroid/view/ContextThemeWrapper;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Leqf;->R:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Z)Leqf;
    .locals 3

    .prologue
    .line 119
    new-instance v0, Leqf;

    invoke-direct {v0}, Leqf;-><init>()V

    .line 120
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 121
    const-string v2, "domain_name"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v2, "domain_id"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "has_public_circle"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 124
    const-string v2, "title_res_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 125
    invoke-virtual {v0, v1}, Leqf;->f(Landroid/os/Bundle;)V

    .line 126
    return-object v0
.end method

.method static synthetic b(Leqf;)Llnl;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Leqf;->N:Llnl;

    return-object v0
.end method

.method public static c(I)Leqf;
    .locals 4

    .prologue
    .line 106
    new-instance v0, Leqf;

    invoke-direct {v0}, Leqf;-><init>()V

    .line 107
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 108
    const-string v2, "title_res_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 109
    const-string v2, "has_public_circle"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 110
    invoke-virtual {v0, v1}, Leqf;->f(Landroid/os/Bundle;)V

    .line 111
    return-object v0
.end method

.method static synthetic c(Leqf;)Lhee;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Leqf;->Q:Lhee;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0, p1}, Lloj;->a(Landroid/app/Activity;)V

    .line 138
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0901c9

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Leqf;->R:Landroid/view/ContextThemeWrapper;

    .line 139
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10

    .prologue
    .line 143
    invoke-virtual {p0}, Leqf;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 144
    const-string v1, "domain_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 145
    const-string v2, "domain_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 146
    const-string v3, "has_public_circle"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 147
    const-string v4, "title_res_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 149
    iget-object v0, p0, Leqf;->R:Landroid/view/ContextThemeWrapper;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 150
    const v5, 0x7f0401e9

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 153
    iget-object v0, p0, Leqf;->S:Ljava/util/ArrayList;

    new-instance v6, Leqh;

    const-string v7, "1f"

    const/4 v8, 0x7

    const v9, 0x7f0a04a4

    .line 155
    invoke-virtual {p0, v9}, Leqf;->e_(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Leqh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 153
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    if-eqz v1, :cond_0

    .line 157
    iget-object v0, p0, Leqf;->S:Ljava/util/ArrayList;

    new-instance v6, Leqh;

    const/16 v7, 0x8

    invoke-direct {v6, v2, v7, v1}, Leqh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_0
    if-eqz v3, :cond_1

    .line 161
    iget-object v0, p0, Leqf;->S:Ljava/util/ArrayList;

    new-instance v1, Leqh;

    const-string v2, "0"

    const/16 v3, 0x9

    const v6, 0x7f0a04a3

    .line 162
    invoke-virtual {p0, v6}, Leqf;->e_(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v3, v6}, Leqh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 161
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_1
    iget-object v0, p0, Leqf;->S:Ljava/util/ArrayList;

    new-instance v1, Leqh;

    const-string v2, "1c"

    const/4 v3, 0x5

    const v6, 0x7f0a04a5

    .line 166
    invoke-virtual {p0, v6}, Leqf;->e_(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v3, v6}, Leqh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 164
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v0, p0, Leqf;->S:Ljava/util/ArrayList;

    new-instance v1, Leqh;

    const-string v2, "v.private"

    const/16 v3, 0x65

    const v6, 0x7f0a04a1

    .line 169
    invoke-virtual {p0, v6}, Leqf;->e_(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v3, v6}, Leqh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 167
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v0, p0, Leqf;->S:Ljava/util/ArrayList;

    new-instance v1, Leqh;

    const-string v2, "v.custom"

    const/4 v3, -0x1

    const v6, 0x7f0a0790

    .line 171
    invoke-virtual {p0, v6}, Leqf;->e_(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v3, v6}, Leqh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 170
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    const v0, 0x7f1002b2

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 174
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 175
    new-instance v1, Leqg;

    iget-object v2, p0, Leqf;->R:Landroid/view/ContextThemeWrapper;

    const/4 v3, 0x0

    iget-object v6, p0, Leqf;->S:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2, v3, v6}, Leqg;-><init>(Leqf;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 205
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Leqf;->R:Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 206
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 207
    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 208
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 209
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 211
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 131
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 132
    iget-object v0, p0, Leqf;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leqf;->Q:Lhee;

    .line 133
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 216
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 217
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 221
    packed-switch p2, :pswitch_data_0

    .line 227
    :goto_0
    return-void

    .line 223
    :pswitch_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 231
    invoke-virtual {p0}, Leqf;->u_()Lu;

    move-result-object v1

    .line 232
    instance-of v0, v1, Leqi;

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqh;

    .line 234
    if-eqz v0, :cond_0

    .line 235
    check-cast v1, Leqi;

    .line 236
    invoke-virtual {v0}, Leqh;->c()I

    move-result v2

    .line 237
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 238
    invoke-interface {v1}, Leqi;->aj()V

    .line 245
    :cond_0
    :goto_0
    invoke-virtual {p0}, Leqf;->c()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 246
    return-void

    .line 240
    :cond_1
    invoke-virtual {v0}, Leqh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Leqh;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v2, v0}, Leqi;->b(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method
