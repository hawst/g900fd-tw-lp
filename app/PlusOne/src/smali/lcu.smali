.class public final Llcu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static b:Llct;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llja;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/ViewGroup;

.field private d:Ljava/lang/Object;

.field private e:Landroid/graphics/Rect;

.field private f:F

.field private g:Landroid/view/View;

.field private h:F

.field private i:F

.field private j:Z

.field private k:I

.field private l:I

.field private m:[I

.field private n:[I

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private p:Z

.field private final q:I


# direct methods
.method public constructor <init>(Llda;)V
    .locals 1

    .prologue
    .line 94
    const v0, 0x7f0a0482

    invoke-direct {p0, p1, v0}, Llcu;-><init>(Llda;I)V

    .line 95
    return-void
.end method

.method public constructor <init>(Llda;I)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Llcu;->p:Z

    .line 98
    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Llcu;->c:Landroid/view/ViewGroup;

    .line 99
    sget-object v0, Llcu;->b:Llct;

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Llcu;->b:Llct;

    .line 103
    :cond_0
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    new-instance v0, Llcv;

    invoke-direct {v0, p0}, Llcv;-><init>(Llcu;)V

    iput-object v0, p0, Llcu;->d:Ljava/lang/Object;

    .line 139
    :goto_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llcu;->e:Landroid/graphics/Rect;

    .line 140
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Llcu;->f:F

    .line 141
    iput p2, p0, Llcu;->q:I

    .line 142
    return-void

    .line 124
    :cond_1
    new-instance v0, Llcw;

    invoke-direct {v0, p0}, Llcw;-><init>(Llcu;)V

    iput-object v0, p0, Llcu;->d:Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic a(Llcu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    :goto_0
    iget v0, p0, Llcu;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    instance-of v0, v0, Llcz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    check-cast v0, Llcz;

    invoke-interface {v0}, Llcz;->e()V

    :cond_0
    iput-object v2, p0, Llcu;->g:Landroid/view/View;

    const/4 v0, 0x0

    iput v0, p0, Llcu;->k:I

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_3
    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 338
    iget-boolean v0, p0, Llcu;->j:Z

    if-eq v0, p1, :cond_2

    .line 339
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 341
    if-eqz v0, :cond_0

    .line 342
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 344
    :cond_0
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 346
    :cond_1
    iput-boolean p1, p0, Llcu;->j:Z

    .line 347
    invoke-direct {p0}, Llcu;->c()V

    .line 349
    :cond_2
    return-void
.end method

.method private b()V
    .locals 11

    .prologue
    .line 156
    iget-object v0, p0, Llcu;->m:[I

    const/4 v1, 0x0

    iget-object v2, p0, Llcu;->n:[I

    const/4 v3, 0x0

    iget v4, p0, Llcu;->l:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    iget-object v0, p0, Llcu;->m:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 159
    const/4 v1, 0x0

    .line 160
    const/4 v0, 0x0

    iget-object v2, p0, Llcu;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_5

    .line 161
    iget-object v0, p0, Llcu;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llja;

    .line 162
    invoke-interface {v0}, Llja;->k()Z

    move-result v2

    if-nez v2, :cond_13

    .line 163
    iget-object v2, p0, Llcu;->o:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Llcu;->o:Ljava/util/ArrayList;

    .line 167
    invoke-interface {v0}, Llja;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 168
    :cond_0
    const/4 v2, 0x0

    .line 173
    const/4 v0, 0x0

    :goto_1
    iget v5, p0, Llcu;->l:I

    if-ge v0, v5, :cond_12

    .line 174
    iget-object v5, p0, Llcu;->n:[I

    aget v5, v5, v0

    if-ne v5, v3, :cond_2

    .line 175
    iget-object v2, p0, Llcu;->m:[I

    aput v3, v2, v0

    .line 176
    const/4 v0, 0x1

    .line 181
    :goto_2
    if-nez v0, :cond_1

    .line 182
    iget v0, p0, Llcu;->l:I

    if-ge v3, v0, :cond_3

    .line 183
    iget-object v0, p0, Llcu;->m:[I

    aput v3, v0, v3

    .line 194
    :cond_1
    :goto_3
    add-int/lit8 v0, v1, 0x1

    iget v1, p0, Llcu;->l:I

    if-ge v0, v1, :cond_5

    .line 195
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 173
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 185
    :cond_3
    const/4 v0, 0x0

    :goto_5
    iget v2, p0, Llcu;->l:I

    if-ge v0, v2, :cond_1

    .line 186
    iget-object v2, p0, Llcu;->m:[I

    aget v2, v2, v0

    const/4 v5, -0x1

    if-ne v2, v5, :cond_4

    .line 187
    iget-object v2, p0, Llcu;->m:[I

    aput v3, v2, v0

    goto :goto_3

    .line 185
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 198
    :cond_5
    const/4 v1, -0x1

    .line 199
    const/4 v0, 0x0

    :goto_6
    iget v2, p0, Llcu;->l:I

    if-ge v0, v2, :cond_6

    .line 200
    iget-object v2, p0, Llcu;->m:[I

    aget v2, v2, v0

    const/4 v3, -0x1

    if-ne v2, v3, :cond_8

    move v1, v0

    .line 206
    :cond_6
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-static {v0}, Llii;->e(Landroid/view/View;)V

    .line 207
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 209
    const/4 v4, 0x1

    .line 210
    const/4 v3, 0x0

    .line 211
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    check-cast v0, Llda;

    .line 212
    invoke-interface {v0}, Llda;->f()V

    .line 213
    invoke-interface {v0}, Llda;->h()I

    move-result v7

    .line 214
    invoke-interface {v0}, Llda;->i()I

    move-result v8

    .line 215
    const/4 v2, 0x0

    move v6, v2

    move-object v2, v3

    :goto_7
    if-ge v6, v7, :cond_e

    .line 216
    iget-object v3, p0, Llcu;->m:[I

    aget v5, v3, v6

    .line 218
    const/4 v3, -0x1

    if-ne v5, v3, :cond_a

    .line 219
    new-instance v5, Llef;

    iget-object v3, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v9

    if-ne v6, v1, :cond_9

    const/4 v3, 0x1

    :goto_8
    iget v10, p0, Llcu;->q:I

    invoke-direct {v5, v9, v8, v3, v10}, Llef;-><init>(Landroid/content/Context;IZI)V

    move-object v3, v5

    .line 224
    :goto_9
    iget-boolean v5, p0, Llcu;->p:Z

    if-eqz v5, :cond_7

    iget-object v5, p0, Llcu;->m:[I

    aget v5, v5, v6

    const/4 v9, -0x1

    if-eq v5, v9, :cond_c

    iget-object v5, p0, Llcu;->m:[I

    aget v5, v5, v6

    iget-object v9, p0, Llcu;->n:[I

    aget v9, v9, v6

    if-eq v5, v9, :cond_c

    const/4 v5, 0x1

    :goto_a
    if-eqz v5, :cond_7

    .line 225
    if-nez v2, :cond_d

    move-object v2, v3

    .line 231
    :cond_7
    :goto_b
    iget-object v5, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 215
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_7

    .line 199
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 219
    :cond_9
    const/4 v3, 0x0

    goto :goto_8

    .line 222
    :cond_a
    add-int/lit8 v3, v7, -0x1

    if-ne v6, v3, :cond_b

    const/4 v3, 0x1

    :goto_c
    invoke-interface {v0, v5, v3}, Llda;->a(IZ)Landroid/view/View;

    move-result-object v3

    goto :goto_9

    :cond_b
    const/4 v3, 0x0

    goto :goto_c

    .line 224
    :cond_c
    const/4 v5, 0x0

    goto :goto_a

    .line 228
    :cond_d
    const/4 v4, 0x0

    goto :goto_b

    .line 235
    :cond_e
    if-eqz v4, :cond_10

    if-eqz v2, :cond_10

    .line 236
    invoke-static {}, Llsj;->c()Z

    move-result v1

    if-eqz v1, :cond_11

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    sget-object v2, Llcu;->b:Llct;

    iget-object v2, v2, Llct;->b:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-static {}, Llsj;->d()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_f
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 238
    :cond_10
    :goto_d
    invoke-interface {v0}, Llda;->g()V

    .line 240
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 241
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 242
    return-void

    .line 236
    :cond_11
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    sget-object v3, Llcu;->b:Llct;

    iget-object v3, v3, Llct;->b:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v1, v3}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_d

    :cond_12
    move v0, v2

    goto/16 :goto_2

    :cond_13
    move v0, v1

    goto/16 :goto_4
.end method

.method private c()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x96

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 352
    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    if-nez v0, :cond_0

    .line 392
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 357
    iget v0, p0, Llcu;->i:F

    iget v1, p0, Llcu;->h:F

    sub-float v1, v0, v1

    .line 358
    iget-boolean v0, p0, Llcu;->j:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 359
    :goto_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    int-to-float v6, v4

    div-float/2addr v5, v6

    .line 361
    iget-boolean v6, p0, Llcu;->j:Z

    if-eqz v6, :cond_3

    .line 362
    invoke-static {}, Llsj;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 363
    iget-object v1, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 364
    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    sub-float v1, v3, v5

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 358
    goto :goto_1

    .line 366
    :cond_2
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v1, v0, v0, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 367
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    sub-float v2, v3, v5

    sub-float/2addr v3, v5

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 369
    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v7}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 370
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 371
    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 372
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 373
    invoke-virtual {v2, v7}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 374
    invoke-virtual {v2, v7}, Landroid/view/animation/AnimationSet;->setFillEnabled(Z)V

    .line 375
    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 378
    :cond_3
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v6, v4

    div-float/2addr v0, v6

    .line 381
    const/high16 v6, 0x3e800000    # 0.25f

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_6

    .line 382
    cmpl-float v0, v1, v2

    if-lez v0, :cond_5

    int-to-float v0, v4

    .line 383
    :goto_2
    const/4 v4, 0x2

    iput v4, p0, Llcu;->k:I

    move v4, v0

    move v0, v2

    .line 390
    :goto_3
    sub-float/2addr v3, v5

    invoke-static {}, Llsj;->c()Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v1, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Llcu;->b:Llct;

    iget-object v1, v1, Llct;->b:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v0, p0, Llcu;->d:Ljava/lang/Object;

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_4
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_0

    .line 382
    :cond_5
    neg-int v0, v4

    int-to-float v0, v0

    goto :goto_2

    .line 387
    :cond_6
    iput v7, p0, Llcu;->k:I

    move v0, v3

    move v4, v2

    .line 388
    goto :goto_3

    .line 390
    :cond_7
    new-instance v5, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v5, v1, v4, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v3, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v7}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v0, Llcu;->b:Llct;

    iget-object v0, v0, Llct;->b:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    invoke-virtual {v2, v7}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    invoke-virtual {v2, v7}, Landroid/view/animation/AnimationSet;->setFillEnabled(Z)V

    iget-object v0, p0, Llcu;->d:Ljava/lang/Object;

    check-cast v0, Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 422
    invoke-direct {p0, v0}, Llcu;->a(Z)V

    .line 423
    iput-object v1, p0, Llcu;->g:Landroid/view/View;

    .line 424
    iput v2, p0, Llcu;->h:F

    .line 425
    iput v2, p0, Llcu;->i:F

    .line 426
    iput-boolean v0, p0, Llcu;->j:Z

    .line 427
    iput v0, p0, Llcu;->k:I

    .line 429
    iput-object v1, p0, Llcu;->a:Ljava/util/List;

    .line 430
    iput v0, p0, Llcu;->l:I

    .line 431
    iput-object v1, p0, Llcu;->m:[I

    .line 432
    iput-object v1, p0, Llcu;->n:[I

    .line 433
    iput-object v1, p0, Llcu;->o:Ljava/util/ArrayList;

    .line 434
    iput-boolean v0, p0, Llcu;->p:Z

    .line 435
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x12c

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 485
    const/4 v1, 0x0

    .line 486
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 487
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_4

    .line 488
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 489
    instance-of v4, v0, Llcz;

    if-eqz v4, :cond_0

    .line 490
    check-cast v0, Llcz;

    .line 491
    invoke-interface {v0}, Llcz;->f()Llja;

    move-result-object v4

    invoke-interface {v4}, Llja;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 497
    :goto_1
    if-nez v0, :cond_1

    .line 498
    invoke-virtual {p0, p2}, Llcu;->a(Ljava/util/ArrayList;)V

    .line 562
    :goto_2
    return-void

    .line 487
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 500
    :cond_1
    invoke-static {}, Llsj;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 501
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 502
    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Llcu;->b:Llct;

    iget-object v1, v1, Llct;->c:Landroid/view/animation/DecelerateInterpolator;

    .line 503
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 504
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 505
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 507
    :cond_2
    new-instance v1, Llcx;

    invoke-direct {v1, p0, v0, p2}, Llcx;-><init>(Llcu;Landroid/view/ViewPropertyAnimator;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 534
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_2

    .line 536
    :cond_3
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 537
    invoke-virtual {v1, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 538
    sget-object v2, Llcu;->b:Llct;

    iget-object v2, v2, Llct;->c:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v1, v2}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 539
    invoke-virtual {v1, v6}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 540
    invoke-virtual {v1, v6}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 541
    new-instance v2, Llcy;

    invoke-direct {v2, p0, v1, p2}, Llcy;-><init>(Llcu;Landroid/view/animation/AlphaAnimation;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 559
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245
    iput-object p1, p0, Llcu;->o:Ljava/util/ArrayList;

    .line 246
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Llcu;->p:Z

    .line 247
    invoke-direct {p0}, Llcu;->b()V

    .line 248
    return-void

    .line 246
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Llja;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 146
    iput-object p1, p0, Llcu;->a:Ljava/util/List;

    .line 147
    iput p2, p0, Llcu;->l:I

    .line 148
    iget v0, p0, Llcu;->l:I

    new-array v0, v0, [I

    iput-object v0, p0, Llcu;->m:[I

    .line 149
    iget v0, p0, Llcu;->l:I

    new-array v0, v0, [I

    iput-object v0, p0, Llcu;->n:[I

    .line 150
    iget-object v0, p0, Llcu;->m:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 151
    iget-object v0, p0, Llcu;->n:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 152
    invoke-direct {p0}, Llcu;->b()V

    .line 153
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 259
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Llcu;->i:F

    .line 261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 292
    :cond_0
    :goto_0
    iget-boolean v0, p0, Llcu;->j:Z

    :goto_1
    return v0

    .line 263
    :pswitch_0
    iget-boolean v0, p0, Llcu;->j:Z

    if-eqz v0, :cond_0

    .line 264
    invoke-direct {p0}, Llcu;->c()V

    goto :goto_0

    .line 270
    :pswitch_1
    iget-boolean v0, p0, Llcu;->j:Z

    .line 271
    invoke-direct {p0, v2}, Llcu;->a(Z)V

    .line 272
    if-nez v0, :cond_2

    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 273
    iget-object v0, p0, Llcu;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 274
    iget v0, p0, Llcu;->k:I

    if-nez v0, :cond_1

    .line 275
    iput-object v1, p0, Llcu;->g:Landroid/view/View;

    .line 277
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 279
    :cond_2
    iget v0, p0, Llcu;->k:I

    if-nez v0, :cond_0

    .line 280
    iput-object v1, p0, Llcu;->g:Landroid/view/View;

    goto :goto_0

    .line 286
    :pswitch_2
    invoke-direct {p0, v2}, Llcu;->a(Z)V

    .line 287
    iput-object v1, p0, Llcu;->g:Landroid/view/View;

    goto :goto_0

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 296
    iget v0, p0, Llcu;->k:I

    if-eqz v0, :cond_0

    move v0, v2

    .line 334
    :goto_0
    return v0

    .line 300
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Llcu;->i:F

    .line 301
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    iget v0, p0, Llcu;->i:F

    iget-object v1, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getTranslationX()F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Llcu;->i:F

    .line 305
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 307
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    move v0, v2

    .line 334
    goto :goto_0

    .line 309
    :pswitch_1
    iget v0, p0, Llcu;->i:F

    iput v0, p0, Llcu;->h:F

    goto :goto_1

    .line 314
    :pswitch_2
    iget-boolean v0, p0, Llcu;->j:Z

    if-nez v0, :cond_2

    .line 315
    iget v0, p0, Llcu;->h:F

    iget v1, p0, Llcu;->i:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Llcu;->f:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    .line 316
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_2
    if-ltz v4, :cond_2

    .line 317
    iget-object v0, p0, Llcu;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 318
    instance-of v0, v1, Llcz;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Llcz;

    .line 319
    invoke-interface {v0}, Llcz;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    iget-object v0, p0, Llcu;->e:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 321
    iget-object v0, p0, Llcu;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_3

    iget-object v0, p0, Llcu;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_3

    .line 322
    iput-object v1, p0, Llcu;->g:Landroid/view/View;

    .line 323
    invoke-direct {p0, v3}, Llcu;->a(Z)V

    move v0, v3

    .line 324
    goto :goto_0

    .line 316
    :cond_3
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_2

    .line 307
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
