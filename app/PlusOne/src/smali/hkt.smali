.class public final Lhkt;
.super Lkff;
.source "PG"


# static fields
.field private static final a:Llrq;


# instance fields
.field private b:Ljava/net/HttpCookie;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    new-instance v0, Llrq;

    const-string v1, "debug.plus.safe.url"

    const-string v2, "https://googleads.g.doubleclick.net/pagead/drt/m"

    invoke-direct {v0, v1, v2}, Llrq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lhkt;->a:Llrq;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;)V
    .locals 7

    .prologue
    .line 30
    const-string v3, "POST"

    new-instance v4, Lkgc;

    .line 31
    invoke-virtual {p2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, p1, v0}, Lkgc;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sget-object v0, Lhkt;->a:Llrq;

    .line 32
    invoke-virtual {v0}, Llrq;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 30
    invoke-direct/range {v0 .. v6}, Lkff;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lkfj;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 4

    .prologue
    .line 37
    const-string v0, "Set-Cookie"

    invoke-interface {p1, v0}, Lorg/chromium/net/HttpUrlRequest;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    :try_start_0
    invoke-static {v0}, Ljava/net/HttpCookie;->parse(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/HttpCookie;

    .line 41
    invoke-virtual {v0}, Ljava/net/HttpCookie;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "_drt_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    iput-object v0, p0, Lhkt;->b:Ljava/net/HttpCookie;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :cond_1
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    const-string v1, "HttpOperation"

    const-string v2, "Failed to parse cookies"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public i()Ljava/net/HttpCookie;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lhkt;->b:Ljava/net/HttpCookie;

    return-object v0
.end method
