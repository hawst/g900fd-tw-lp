.class final Ldut;
.super Ldvb;
.source "PG"


# direct methods
.method public constructor <init>(Ldur;I)V
    .locals 0

    .prologue
    .line 1809
    invoke-direct {p0, p1, p2}, Ldvb;-><init>(Ldur;I)V

    .line 1811
    return-void
.end method

.method public constructor <init>(Ldur;II)V
    .locals 0

    .prologue
    .line 1813
    invoke-direct {p0, p1, p2, p3}, Ldvb;-><init>(Ldur;II)V

    .line 1815
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1819
    const-string v0, "all_photos LEFT OUTER JOIN media_cache ON (all_photos.image_url=media_cache.image_url)"

    return-object v0
.end method

.method protected b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1824
    invoke-static {}, Ldur;->b()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1829
    const-string v0, "all_photos.image_url IS NOT NULL AND is_primary = 1 AND (has_edit_list = 1 OR local_content_uri IS NULL)"

    return-object v0
.end method

.method protected d()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1834
    const/4 v0, 0x0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1839
    const-string v0, "all_photos.image_url"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1844
    const-string v0, "timestamp DESC"

    return-object v0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 1849
    const/4 v0, 0x2

    return v0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1854
    const-string v0, "AllPhotos"

    return-object v0
.end method
