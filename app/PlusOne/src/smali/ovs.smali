.class public final Lovs;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lovs;


# instance fields
.field private b:I

.field private c:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    new-array v0, v0, [Lovs;

    sput-object v0, Lovs;->a:[Lovs;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Loxq;-><init>()V

    .line 103
    const/high16 v0, -0x80000000

    iput v0, p0, Lovs;->b:I

    .line 93
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    iget v1, p0, Lovs;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 124
    const/4 v0, 0x1

    iget v1, p0, Lovs;->b:I

    .line 125
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 127
    :cond_0
    iget-object v1, p0, Lovs;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 128
    const/4 v1, 0x2

    iget-object v2, p0, Lovs;->c:Ljava/lang/Boolean;

    .line 129
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 131
    :cond_1
    iget-object v1, p0, Lovs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    iput v0, p0, Lovs;->ai:I

    .line 133
    return v0
.end method

.method public a(Loxn;)Lovs;
    .locals 2

    .prologue
    .line 141
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 142
    sparse-switch v0, :sswitch_data_0

    .line 146
    iget-object v1, p0, Lovs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 147
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lovs;->ah:Ljava/util/List;

    .line 150
    :cond_1
    iget-object v1, p0, Lovs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :sswitch_0
    return-object p0

    .line 157
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 158
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 162
    :cond_2
    iput v0, p0, Lovs;->b:I

    goto :goto_0

    .line 164
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lovs;->b:I

    goto :goto_0

    .line 169
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lovs;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 142
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Lovs;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 111
    const/4 v0, 0x1

    iget v1, p0, Lovs;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 113
    :cond_0
    iget-object v0, p0, Lovs;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 114
    const/4 v0, 0x2

    iget-object v1, p0, Lovs;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 116
    :cond_1
    iget-object v0, p0, Lovs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 118
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lovs;->a(Loxn;)Lovs;

    move-result-object v0

    return-object v0
.end method
