.class public final Ljvu;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Llyy;",
        "Llyz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;Z)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v2, Lkfo;

    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    .line 39
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "checkphotosexistence"

    new-instance v4, Llyy;

    invoke-direct {v4}, Llyy;-><init>()V

    new-instance v5, Llyz;

    invoke-direct {v5}, Llyz;-><init>()V

    move-object v0, p0

    move-object v1, p1

    .line 38
    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ljvu;->p:Ljava/util/Map;

    .line 41
    iput-object p3, p0, Ljvu;->a:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Ljvu;->b:Ljava/util/List;

    .line 43
    iput-boolean p5, p0, Ljvu;->c:Z

    .line 44
    return-void
.end method


# virtual methods
.method protected a(Llyy;)V
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lnej;

    invoke-direct {v0}, Lnej;-><init>()V

    iput-object v0, p1, Llyy;->a:Lnej;

    .line 49
    iget-object v1, p1, Llyy;->a:Lnej;

    .line 50
    iget-boolean v0, p0, Ljvu;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnej;->c:Ljava/lang/Boolean;

    .line 51
    iget-object v0, p0, Ljvu;->a:Ljava/lang/String;

    iput-object v0, v1, Lnej;->a:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Ljvu;->b:Ljava/util/List;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lnej;->b:[Ljava/lang/String;

    .line 53
    return-void
.end method

.method protected a(Llyz;)V
    .locals 6

    .prologue
    .line 57
    iget-object v2, p1, Llyz;->a:Lnfl;

    .line 58
    iget-object v0, v2, Lnfl;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lnfl;->a:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 76
    :cond_0
    return-void

    .line 61
    :cond_1
    iget-object v0, v2, Lnfl;->a:[Ljava/lang/String;

    array-length v0, v0

    iget-object v1, p0, Ljvu;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "returned array length doesn\'t match input"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_2
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Ljvu;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 66
    iget-object v4, v2, Lnfl;->a:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 67
    if-eqz v4, :cond_3

    .line 69
    :try_start_0
    iget-object v5, p0, Ljvu;->p:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_3
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 75
    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Llyy;

    invoke-virtual {p0, p1}, Ljvu;->a(Llyy;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Llyz;

    invoke-virtual {p0, p1}, Ljvu;->a(Llyz;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ljvu;->p:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Ljvu;->p:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method
