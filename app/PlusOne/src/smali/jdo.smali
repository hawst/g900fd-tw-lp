.class final Ljdo;
.super Ljdi;
.source "PG"


# instance fields
.field private final h:Landroid/content/Context;

.field private final i:Ljava/lang/String;

.field private j:Lorg/chromium/net/HttpUrlRequest;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfj;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p2}, Ljdi;-><init>(Lkfj;)V

    .line 27
    iput-object p1, p0, Ljdo;->h:Landroid/content/Context;

    .line 28
    iput-object p3, p0, Ljdo;->i:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-virtual {p0}, Ljdo;->j()Lkfj;

    move-result-object v1

    iget-object v2, p0, Ljdo;->i:Ljava/lang/String;

    invoke-interface {v1, v2}, Lkfj;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 37
    const-string v1, "Content-Range"

    const-string v2, "bytes */*"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v1, p0, Ljdo;->h:Landroid/content/Context;

    iget-object v2, p0, Ljdo;->i:Ljava/lang/String;

    const/4 v3, 0x3

    iget-object v4, p0, Ljdo;->g:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-static {v1, v2, v3, v0, v4}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    iput-object v0, p0, Ljdo;->j:Lorg/chromium/net/HttpUrlRequest;

    .line 41
    iget-object v0, p0, Ljdo;->j:Lorg/chromium/net/HttpUrlRequest;

    const-string v1, "PUT"

    invoke-interface {v0, v1}, Lorg/chromium/net/HttpUrlRequest;->a(Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method protected a(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 1

    .prologue
    .line 55
    const-string v0, "Range"

    invoke-interface {p1, v0}, Lorg/chromium/net/HttpUrlRequest;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdo;->k:Ljava/lang/String;

    .line 56
    return-void
.end method

.method protected b()Lorg/chromium/net/HttpUrlRequest;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ljdo;->j:Lorg/chromium/net/HttpUrlRequest;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ljdo;->k:Ljava/lang/String;

    return-object v0
.end method
