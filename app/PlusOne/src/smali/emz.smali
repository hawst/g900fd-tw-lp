.class public final Lemz;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lloj;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Ljava/util/List",
        "<",
        "Lofi;",
        ">;>;"
    }
.end annotation


# instance fields
.field private Q:Lhee;

.field private R:Lena;

.field private S:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lloj;-><init>()V

    .line 195
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    const v0, 0x7f0400f6

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 64
    new-instance v0, Lena;

    invoke-virtual {p0}, Lemz;->n()Lz;

    move-result-object v2

    invoke-direct {v0, v2}, Lena;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lemz;->R:Lena;

    .line 65
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lemz;->S:Landroid/widget/ListView;

    .line 66
    iget-object v0, p0, Lemz;->S:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    iget-object v0, p0, Lemz;->S:Landroid/widget/ListView;

    iget-object v2, p0, Lemz;->R:Lena;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 69
    const v0, 0x7f1001db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v0, 0x7f1001da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 72
    const v0, 0x7f100346

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 73
    const v2, 0x7f0a0af1

    invoke-virtual {p0, v2}, Lemz;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 76
    invoke-virtual {p0}, Lemz;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 77
    const v0, 0x7f100260

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 79
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ljava/util/List",
            "<",
            "Lofi;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 92
    if-nez p1, :cond_0

    iget-object v0, p0, Lemz;->Q:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 93
    :cond_0
    const/4 v0, 0x0

    .line 96
    :goto_0
    return-object v0

    .line 95
    :cond_1
    invoke-virtual {p0}, Lemz;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    new-instance v0, Lenb;

    iget-object v2, p0, Lemz;->N:Llnl;

    iget-object v3, p0, Lemz;->Q:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v0, v2, v3, v1}, Lenb;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Lloj;->a(Landroid/os/Bundle;)V

    .line 56
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lemz;->a(II)V

    .line 57
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ljava/util/List",
            "<",
            "Lofi;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 119
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lemz;->a(Ldo;Ljava/util/List;)V

    return-void
.end method

.method public a(Ldo;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ljava/util/List",
            "<",
            "Lofi;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lofi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    return-void

    .line 105
    :cond_1
    invoke-virtual {p0}, Lemz;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lemz;->R:Lena;

    invoke-virtual {v0}, Lena;->clear()V

    .line 109
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 110
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lofi;

    .line 111
    iget-object v2, p0, Lemz;->R:Lena;

    invoke-virtual {v2, v0}, Lena;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public ae_()V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0}, Lloj;->ae_()V

    .line 85
    iget-object v0, p0, Lemz;->S:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lemz;->S:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 88
    :cond_0
    return-void
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 46
    iget-object v0, p0, Lemz;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lemz;->Q:Lhee;

    .line 47
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 123
    invoke-virtual {p0}, Lemz;->a()V

    .line 124
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lemz;->R:Lena;

    invoke-virtual {v0, p3}, Lena;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lofi;

    .line 129
    if-nez v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v1, p0, Lemz;->Q:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 133
    iget-object v2, p0, Lemz;->N:Llnl;

    const-string v3, "g:"

    iget-object v0, v0, Lofi;->b:Lofv;

    iget-object v0, v0, Lofv;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v3, 0x0

    invoke-static {v2, v1, v0, v3}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 135
    invoke-virtual {p0, v0}, Lemz;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 133
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
