.class public final Lmdt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwd;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7936
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7939
    iput-object v0, p0, Lmdt;->apiHeader:Llyr;

    .line 7942
    iput-object v0, p0, Lmdt;->a:Lnwd;

    .line 7936
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7959
    const/4 v0, 0x0

    .line 7960
    iget-object v1, p0, Lmdt;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 7961
    const/4 v0, 0x1

    iget-object v1, p0, Lmdt;->apiHeader:Llyr;

    .line 7962
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7964
    :cond_0
    iget-object v1, p0, Lmdt;->a:Lnwd;

    if-eqz v1, :cond_1

    .line 7965
    const/4 v1, 0x2

    iget-object v2, p0, Lmdt;->a:Lnwd;

    .line 7966
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7968
    :cond_1
    iget-object v1, p0, Lmdt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7969
    iput v0, p0, Lmdt;->ai:I

    .line 7970
    return v0
.end method

.method public a(Loxn;)Lmdt;
    .locals 2

    .prologue
    .line 7978
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7979
    sparse-switch v0, :sswitch_data_0

    .line 7983
    iget-object v1, p0, Lmdt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7984
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdt;->ah:Ljava/util/List;

    .line 7987
    :cond_1
    iget-object v1, p0, Lmdt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7989
    :sswitch_0
    return-object p0

    .line 7994
    :sswitch_1
    iget-object v0, p0, Lmdt;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 7995
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmdt;->apiHeader:Llyr;

    .line 7997
    :cond_2
    iget-object v0, p0, Lmdt;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8001
    :sswitch_2
    iget-object v0, p0, Lmdt;->a:Lnwd;

    if-nez v0, :cond_3

    .line 8002
    new-instance v0, Lnwd;

    invoke-direct {v0}, Lnwd;-><init>()V

    iput-object v0, p0, Lmdt;->a:Lnwd;

    .line 8004
    :cond_3
    iget-object v0, p0, Lmdt;->a:Lnwd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7979
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7947
    iget-object v0, p0, Lmdt;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 7948
    const/4 v0, 0x1

    iget-object v1, p0, Lmdt;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7950
    :cond_0
    iget-object v0, p0, Lmdt;->a:Lnwd;

    if-eqz v0, :cond_1

    .line 7951
    const/4 v0, 0x2

    iget-object v1, p0, Lmdt;->a:Lnwd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7953
    :cond_1
    iget-object v0, p0, Lmdt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7955
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7932
    invoke-virtual {p0, p1}, Lmdt;->a(Loxn;)Lmdt;

    move-result-object v0

    return-object v0
.end method
