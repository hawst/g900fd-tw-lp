.class public final Llkx;
.super Landroid/view/View$BaseSavedState;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Llkx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Llkt;

.field private final b:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Llky;

    invoke-direct {v0}, Llky;-><init>()V

    sput-object v0, Llkx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llkt;->valueOf(Ljava/lang/String;)Llkt;

    move-result-object v0

    iput-object v0, p0, Llkx;->a:Llkt;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->createFloatArray()[F

    move-result-object v0

    iput-object v0, p0, Llkx;->b:[F

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Llkt;[F)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 118
    iput-object p2, p0, Llkx;->a:Llkt;

    .line 119
    iput-object p3, p0, Llkx;->b:[F

    .line 120
    return-void
.end method

.method public static synthetic a(Llkx;)Llkt;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Llkx;->a:Llkt;

    return-object v0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 125
    iget-object v0, p0, Llkx;->a:Llkt;

    invoke-virtual {v0}, Llkt;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Llkx;->b:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 127
    return-void
.end method
