.class public final Lnjq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lnjr;

.field public c:I

.field public d:Lnjp;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5460
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5465
    iput-object v1, p0, Lnjq;->b:Lnjr;

    .line 5468
    const/high16 v0, -0x80000000

    iput v0, p0, Lnjq;->c:I

    .line 5471
    iput-object v1, p0, Lnjq;->d:Lnjp;

    .line 5460
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5499
    const/4 v0, 0x0

    .line 5500
    iget-object v1, p0, Lnjq;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5501
    const/4 v0, 0x1

    iget-object v1, p0, Lnjq;->a:Ljava/lang/String;

    .line 5502
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5504
    :cond_0
    iget-object v1, p0, Lnjq;->b:Lnjr;

    if-eqz v1, :cond_1

    .line 5505
    const/4 v1, 0x2

    iget-object v2, p0, Lnjq;->b:Lnjr;

    .line 5506
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5508
    :cond_1
    iget v1, p0, Lnjq;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 5509
    const/4 v1, 0x3

    iget v2, p0, Lnjq;->c:I

    .line 5510
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5512
    :cond_2
    iget-object v1, p0, Lnjq;->d:Lnjp;

    if-eqz v1, :cond_3

    .line 5513
    const/4 v1, 0x4

    iget-object v2, p0, Lnjq;->d:Lnjp;

    .line 5514
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5516
    :cond_3
    iget-object v1, p0, Lnjq;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 5517
    const/4 v1, 0x5

    iget-object v2, p0, Lnjq;->e:Ljava/lang/Integer;

    .line 5518
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5520
    :cond_4
    iget-object v1, p0, Lnjq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5521
    iput v0, p0, Lnjq;->ai:I

    .line 5522
    return v0
.end method

.method public a(Loxn;)Lnjq;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5530
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5531
    sparse-switch v0, :sswitch_data_0

    .line 5535
    iget-object v1, p0, Lnjq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5536
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjq;->ah:Ljava/util/List;

    .line 5539
    :cond_1
    iget-object v1, p0, Lnjq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5541
    :sswitch_0
    return-object p0

    .line 5546
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjq;->a:Ljava/lang/String;

    goto :goto_0

    .line 5550
    :sswitch_2
    iget-object v0, p0, Lnjq;->b:Lnjr;

    if-nez v0, :cond_2

    .line 5551
    new-instance v0, Lnjr;

    invoke-direct {v0}, Lnjr;-><init>()V

    iput-object v0, p0, Lnjq;->b:Lnjr;

    .line 5553
    :cond_2
    iget-object v0, p0, Lnjq;->b:Lnjr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5557
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5558
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 5560
    :cond_3
    iput v0, p0, Lnjq;->c:I

    goto :goto_0

    .line 5562
    :cond_4
    iput v2, p0, Lnjq;->c:I

    goto :goto_0

    .line 5567
    :sswitch_4
    iget-object v0, p0, Lnjq;->d:Lnjp;

    if-nez v0, :cond_5

    .line 5568
    new-instance v0, Lnjp;

    invoke-direct {v0}, Lnjp;-><init>()V

    iput-object v0, p0, Lnjq;->d:Lnjp;

    .line 5570
    :cond_5
    iget-object v0, p0, Lnjq;->d:Lnjp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5574
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnjq;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 5531
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5478
    iget-object v0, p0, Lnjq;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5479
    const/4 v0, 0x1

    iget-object v1, p0, Lnjq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5481
    :cond_0
    iget-object v0, p0, Lnjq;->b:Lnjr;

    if-eqz v0, :cond_1

    .line 5482
    const/4 v0, 0x2

    iget-object v1, p0, Lnjq;->b:Lnjr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5484
    :cond_1
    iget v0, p0, Lnjq;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 5485
    const/4 v0, 0x3

    iget v1, p0, Lnjq;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5487
    :cond_2
    iget-object v0, p0, Lnjq;->d:Lnjp;

    if-eqz v0, :cond_3

    .line 5488
    const/4 v0, 0x4

    iget-object v1, p0, Lnjq;->d:Lnjp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5490
    :cond_3
    iget-object v0, p0, Lnjq;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 5491
    const/4 v0, 0x5

    iget-object v1, p0, Lnjq;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5493
    :cond_4
    iget-object v0, p0, Lnjq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5495
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5456
    invoke-virtual {p0, p1}, Lnjq;->a(Loxn;)Lnjq;

    move-result-object v0

    return-object v0
.end method
