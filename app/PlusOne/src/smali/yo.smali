.class public final Lyo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lrm;
.implements Lsb;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lrl;

.field private c:Landroid/view/View;

.field private d:Lry;

.field private e:Lyp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lyo;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;I)V
    .locals 6

    .prologue
    .line 85
    const v4, 0x7f0100d6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lyo;-><init>(Landroid/content/Context;Landroid/view/View;III)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;III)V
    .locals 7

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lyo;->a:Landroid/content/Context;

    .line 108
    new-instance v0, Lrl;

    invoke-direct {v0, p1}, Lrl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lyo;->b:Lrl;

    .line 109
    iget-object v0, p0, Lyo;->b:Lrl;

    invoke-virtual {v0, p0}, Lrl;->a(Lrm;)V

    .line 110
    iput-object p2, p0, Lyo;->c:Landroid/view/View;

    .line 111
    new-instance v0, Lry;

    iget-object v2, p0, Lyo;->b:Lrl;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v3, p2

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lry;-><init>(Landroid/content/Context;Lrl;Landroid/view/View;ZII)V

    iput-object v0, p0, Lyo;->d:Lry;

    .line 112
    iget-object v0, p0, Lyo;->d:Lry;

    invoke-virtual {v0, p3}, Lry;->a(I)V

    .line 113
    iget-object v0, p0, Lyo;->d:Lry;

    invoke-virtual {v0, p0}, Lry;->a(Lsb;)V

    .line 114
    return-void
.end method


# virtual methods
.method public a()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lyo;->b:Lrl;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lyo;->b()Landroid/view/MenuInflater;

    move-result-object v0

    iget-object v1, p0, Lyo;->b:Lrl;

    invoke-virtual {v0, p1, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 186
    return-void
.end method

.method public a(Lrl;Z)V
    .locals 0

    .prologue
    .line 236
    return-void
.end method

.method public a(Lyp;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lyo;->e:Lyp;

    .line 211
    return-void
.end method

.method public a(Lrl;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 245
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 253
    :cond_0
    :goto_0
    return v0

    .line 247
    :cond_1
    invoke-virtual {p1}, Lrl;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    new-instance v1, Lry;

    iget-object v2, p0, Lyo;->a:Landroid/content/Context;

    iget-object v3, p0, Lyo;->c:Landroid/view/View;

    invoke-direct {v1, v2, p1, v3}, Lry;-><init>(Landroid/content/Context;Lrl;Landroid/view/View;)V

    invoke-virtual {v1}, Lry;->a()V

    goto :goto_0
.end method

.method public a(Lrl;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lyo;->e:Lyp;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lyo;->e:Lyp;

    invoke-interface {v0, p2}, Lyp;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 229
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Lqy;

    iget-object v1, p0, Lyo;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lqy;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lyo;->d:Lry;

    invoke-virtual {v0}, Lry;->a()V

    .line 194
    return-void
.end method

.method public m()V
    .locals 0

    .prologue
    .line 266
    return-void
.end method
