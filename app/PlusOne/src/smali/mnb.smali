.class public final Lmnb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmod;

.field public b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4104
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4107
    const/4 v0, 0x0

    iput-object v0, p0, Lmnb;->a:Lmod;

    .line 4104
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4126
    const/4 v0, 0x0

    .line 4127
    iget-object v1, p0, Lmnb;->a:Lmod;

    if-eqz v1, :cond_0

    .line 4128
    const/4 v0, 0x1

    iget-object v1, p0, Lmnb;->a:Lmod;

    .line 4129
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4131
    :cond_0
    iget-object v1, p0, Lmnb;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 4132
    const/4 v1, 0x2

    iget-object v2, p0, Lmnb;->b:Ljava/lang/Float;

    .line 4133
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 4135
    :cond_1
    iget-object v1, p0, Lmnb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4136
    iput v0, p0, Lmnb;->ai:I

    .line 4137
    return v0
.end method

.method public a(Loxn;)Lmnb;
    .locals 2

    .prologue
    .line 4145
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4146
    sparse-switch v0, :sswitch_data_0

    .line 4150
    iget-object v1, p0, Lmnb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4151
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnb;->ah:Ljava/util/List;

    .line 4154
    :cond_1
    iget-object v1, p0, Lmnb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4156
    :sswitch_0
    return-object p0

    .line 4161
    :sswitch_1
    iget-object v0, p0, Lmnb;->a:Lmod;

    if-nez v0, :cond_2

    .line 4162
    new-instance v0, Lmod;

    invoke-direct {v0}, Lmod;-><init>()V

    iput-object v0, p0, Lmnb;->a:Lmod;

    .line 4164
    :cond_2
    iget-object v0, p0, Lmnb;->a:Lmod;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4168
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnb;->b:Ljava/lang/Float;

    goto :goto_0

    .line 4146
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4114
    iget-object v0, p0, Lmnb;->a:Lmod;

    if-eqz v0, :cond_0

    .line 4115
    const/4 v0, 0x1

    iget-object v1, p0, Lmnb;->a:Lmod;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4117
    :cond_0
    iget-object v0, p0, Lmnb;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 4118
    const/4 v0, 0x2

    iget-object v1, p0, Lmnb;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 4120
    :cond_1
    iget-object v0, p0, Lmnb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4122
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4100
    invoke-virtual {p0, p1}, Lmnb;->a(Loxn;)Lmnb;

    move-result-object v0

    return-object v0
.end method
