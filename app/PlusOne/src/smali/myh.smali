.class public final Lmyh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyk;

.field public b:Lmyg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1483
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1486
    iput-object v0, p0, Lmyh;->a:Lmyk;

    .line 1489
    iput-object v0, p0, Lmyh;->b:Lmyg;

    .line 1483
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1506
    const/4 v0, 0x0

    .line 1507
    iget-object v1, p0, Lmyh;->a:Lmyk;

    if-eqz v1, :cond_0

    .line 1508
    const/4 v0, 0x1

    iget-object v1, p0, Lmyh;->a:Lmyk;

    .line 1509
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1511
    :cond_0
    iget-object v1, p0, Lmyh;->b:Lmyg;

    if-eqz v1, :cond_1

    .line 1512
    const/4 v1, 0x2

    iget-object v2, p0, Lmyh;->b:Lmyg;

    .line 1513
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1515
    :cond_1
    iget-object v1, p0, Lmyh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1516
    iput v0, p0, Lmyh;->ai:I

    .line 1517
    return v0
.end method

.method public a(Loxn;)Lmyh;
    .locals 2

    .prologue
    .line 1525
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1526
    sparse-switch v0, :sswitch_data_0

    .line 1530
    iget-object v1, p0, Lmyh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1531
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyh;->ah:Ljava/util/List;

    .line 1534
    :cond_1
    iget-object v1, p0, Lmyh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536
    :sswitch_0
    return-object p0

    .line 1541
    :sswitch_1
    iget-object v0, p0, Lmyh;->a:Lmyk;

    if-nez v0, :cond_2

    .line 1542
    new-instance v0, Lmyk;

    invoke-direct {v0}, Lmyk;-><init>()V

    iput-object v0, p0, Lmyh;->a:Lmyk;

    .line 1544
    :cond_2
    iget-object v0, p0, Lmyh;->a:Lmyk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1548
    :sswitch_2
    iget-object v0, p0, Lmyh;->b:Lmyg;

    if-nez v0, :cond_3

    .line 1549
    new-instance v0, Lmyg;

    invoke-direct {v0}, Lmyg;-><init>()V

    iput-object v0, p0, Lmyh;->b:Lmyg;

    .line 1551
    :cond_3
    iget-object v0, p0, Lmyh;->b:Lmyg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1526
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1494
    iget-object v0, p0, Lmyh;->a:Lmyk;

    if-eqz v0, :cond_0

    .line 1495
    const/4 v0, 0x1

    iget-object v1, p0, Lmyh;->a:Lmyk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1497
    :cond_0
    iget-object v0, p0, Lmyh;->b:Lmyg;

    if-eqz v0, :cond_1

    .line 1498
    const/4 v0, 0x2

    iget-object v1, p0, Lmyh;->b:Lmyg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1500
    :cond_1
    iget-object v0, p0, Lmyh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1502
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1479
    invoke-virtual {p0, p1}, Lmyh;->a(Loxn;)Lmyh;

    move-result-object v0

    return-object v0
.end method
