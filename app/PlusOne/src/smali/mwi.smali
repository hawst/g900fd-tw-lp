.class public final Lmwi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lola;

.field public b:Lmtn;

.field public c:Lmwf;

.field public d:Lmwf;

.field public e:Ljava/lang/Boolean;

.field public f:Lmwg;

.field private g:Ljava/lang/Long;

.field private h:Lmtw;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Integer;

.field private l:Lmtx;

.field private m:Lmvu;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Lmxc;

.field private q:Lmwh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7762
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7767
    iput-object v0, p0, Lmwi;->a:Lola;

    .line 7770
    iput-object v0, p0, Lmwi;->b:Lmtn;

    .line 7773
    iput-object v0, p0, Lmwi;->h:Lmtw;

    .line 7776
    iput-object v0, p0, Lmwi;->c:Lmwf;

    .line 7779
    iput-object v0, p0, Lmwi;->d:Lmwf;

    .line 7788
    iput-object v0, p0, Lmwi;->l:Lmtx;

    .line 7793
    iput-object v0, p0, Lmwi;->m:Lmvu;

    .line 7800
    iput-object v0, p0, Lmwi;->p:Lmxc;

    .line 7803
    iput-object v0, p0, Lmwi;->f:Lmwg;

    .line 7806
    iput-object v0, p0, Lmwi;->q:Lmwh;

    .line 7762
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 7868
    const/4 v0, 0x0

    .line 7869
    iget-object v1, p0, Lmwi;->g:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 7870
    const/4 v0, 0x1

    iget-object v1, p0, Lmwi;->g:Ljava/lang/Long;

    .line 7871
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7873
    :cond_0
    iget-object v1, p0, Lmwi;->a:Lola;

    if-eqz v1, :cond_1

    .line 7874
    const/4 v1, 0x2

    iget-object v2, p0, Lmwi;->a:Lola;

    .line 7875
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7877
    :cond_1
    iget-object v1, p0, Lmwi;->b:Lmtn;

    if-eqz v1, :cond_2

    .line 7878
    const/4 v1, 0x3

    iget-object v2, p0, Lmwi;->b:Lmtn;

    .line 7879
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7881
    :cond_2
    iget-object v1, p0, Lmwi;->h:Lmtw;

    if-eqz v1, :cond_3

    .line 7882
    const/4 v1, 0x4

    iget-object v2, p0, Lmwi;->h:Lmtw;

    .line 7883
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7885
    :cond_3
    iget-object v1, p0, Lmwi;->c:Lmwf;

    if-eqz v1, :cond_4

    .line 7886
    const/4 v1, 0x5

    iget-object v2, p0, Lmwi;->c:Lmwf;

    .line 7887
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7889
    :cond_4
    iget-object v1, p0, Lmwi;->d:Lmwf;

    if-eqz v1, :cond_5

    .line 7890
    const/4 v1, 0x6

    iget-object v2, p0, Lmwi;->d:Lmwf;

    .line 7891
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7893
    :cond_5
    iget-object v1, p0, Lmwi;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 7894
    const/4 v1, 0x7

    iget-object v2, p0, Lmwi;->i:Ljava/lang/Boolean;

    .line 7895
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7897
    :cond_6
    iget-object v1, p0, Lmwi;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 7898
    const/16 v1, 0x8

    iget-object v2, p0, Lmwi;->j:Ljava/lang/Boolean;

    .line 7899
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7901
    :cond_7
    iget-object v1, p0, Lmwi;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 7902
    const/16 v1, 0x9

    iget-object v2, p0, Lmwi;->k:Ljava/lang/Integer;

    .line 7903
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7905
    :cond_8
    iget-object v1, p0, Lmwi;->l:Lmtx;

    if-eqz v1, :cond_9

    .line 7906
    const/16 v1, 0xa

    iget-object v2, p0, Lmwi;->l:Lmtx;

    .line 7907
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7909
    :cond_9
    iget-object v1, p0, Lmwi;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 7910
    const/16 v1, 0xb

    iget-object v2, p0, Lmwi;->e:Ljava/lang/Boolean;

    .line 7911
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7913
    :cond_a
    iget-object v1, p0, Lmwi;->m:Lmvu;

    if-eqz v1, :cond_b

    .line 7914
    const/16 v1, 0xc

    iget-object v2, p0, Lmwi;->m:Lmvu;

    .line 7915
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7917
    :cond_b
    iget-object v1, p0, Lmwi;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 7918
    const/16 v1, 0xd

    iget-object v2, p0, Lmwi;->n:Ljava/lang/String;

    .line 7919
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7921
    :cond_c
    iget-object v1, p0, Lmwi;->o:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 7922
    const/16 v1, 0xe

    iget-object v2, p0, Lmwi;->o:Ljava/lang/String;

    .line 7923
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7925
    :cond_d
    iget-object v1, p0, Lmwi;->p:Lmxc;

    if-eqz v1, :cond_e

    .line 7926
    const/16 v1, 0xf

    iget-object v2, p0, Lmwi;->p:Lmxc;

    .line 7927
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7929
    :cond_e
    iget-object v1, p0, Lmwi;->f:Lmwg;

    if-eqz v1, :cond_f

    .line 7930
    const/16 v1, 0x10

    iget-object v2, p0, Lmwi;->f:Lmwg;

    .line 7931
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7933
    :cond_f
    iget-object v1, p0, Lmwi;->q:Lmwh;

    if-eqz v1, :cond_10

    .line 7934
    const/16 v1, 0x11

    iget-object v2, p0, Lmwi;->q:Lmwh;

    .line 7935
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7937
    :cond_10
    iget-object v1, p0, Lmwi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7938
    iput v0, p0, Lmwi;->ai:I

    .line 7939
    return v0
.end method

.method public a(Loxn;)Lmwi;
    .locals 2

    .prologue
    .line 7947
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7948
    sparse-switch v0, :sswitch_data_0

    .line 7952
    iget-object v1, p0, Lmwi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7953
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwi;->ah:Ljava/util/List;

    .line 7956
    :cond_1
    iget-object v1, p0, Lmwi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7958
    :sswitch_0
    return-object p0

    .line 7963
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwi;->g:Ljava/lang/Long;

    goto :goto_0

    .line 7967
    :sswitch_2
    iget-object v0, p0, Lmwi;->a:Lola;

    if-nez v0, :cond_2

    .line 7968
    new-instance v0, Lola;

    invoke-direct {v0}, Lola;-><init>()V

    iput-object v0, p0, Lmwi;->a:Lola;

    .line 7970
    :cond_2
    iget-object v0, p0, Lmwi;->a:Lola;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7974
    :sswitch_3
    iget-object v0, p0, Lmwi;->b:Lmtn;

    if-nez v0, :cond_3

    .line 7975
    new-instance v0, Lmtn;

    invoke-direct {v0}, Lmtn;-><init>()V

    iput-object v0, p0, Lmwi;->b:Lmtn;

    .line 7977
    :cond_3
    iget-object v0, p0, Lmwi;->b:Lmtn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7981
    :sswitch_4
    iget-object v0, p0, Lmwi;->h:Lmtw;

    if-nez v0, :cond_4

    .line 7982
    new-instance v0, Lmtw;

    invoke-direct {v0}, Lmtw;-><init>()V

    iput-object v0, p0, Lmwi;->h:Lmtw;

    .line 7984
    :cond_4
    iget-object v0, p0, Lmwi;->h:Lmtw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7988
    :sswitch_5
    iget-object v0, p0, Lmwi;->c:Lmwf;

    if-nez v0, :cond_5

    .line 7989
    new-instance v0, Lmwf;

    invoke-direct {v0}, Lmwf;-><init>()V

    iput-object v0, p0, Lmwi;->c:Lmwf;

    .line 7991
    :cond_5
    iget-object v0, p0, Lmwi;->c:Lmwf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7995
    :sswitch_6
    iget-object v0, p0, Lmwi;->d:Lmwf;

    if-nez v0, :cond_6

    .line 7996
    new-instance v0, Lmwf;

    invoke-direct {v0}, Lmwf;-><init>()V

    iput-object v0, p0, Lmwi;->d:Lmwf;

    .line 7998
    :cond_6
    iget-object v0, p0, Lmwi;->d:Lmwf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8002
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwi;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 8006
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwi;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 8010
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwi;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 8014
    :sswitch_a
    iget-object v0, p0, Lmwi;->l:Lmtx;

    if-nez v0, :cond_7

    .line 8015
    new-instance v0, Lmtx;

    invoke-direct {v0}, Lmtx;-><init>()V

    iput-object v0, p0, Lmwi;->l:Lmtx;

    .line 8017
    :cond_7
    iget-object v0, p0, Lmwi;->l:Lmtx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 8021
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwi;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 8025
    :sswitch_c
    iget-object v0, p0, Lmwi;->m:Lmvu;

    if-nez v0, :cond_8

    .line 8026
    new-instance v0, Lmvu;

    invoke-direct {v0}, Lmvu;-><init>()V

    iput-object v0, p0, Lmwi;->m:Lmvu;

    .line 8028
    :cond_8
    iget-object v0, p0, Lmwi;->m:Lmvu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 8032
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwi;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 8036
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwi;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 8040
    :sswitch_f
    iget-object v0, p0, Lmwi;->p:Lmxc;

    if-nez v0, :cond_9

    .line 8041
    new-instance v0, Lmxc;

    invoke-direct {v0}, Lmxc;-><init>()V

    iput-object v0, p0, Lmwi;->p:Lmxc;

    .line 8043
    :cond_9
    iget-object v0, p0, Lmwi;->p:Lmxc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 8047
    :sswitch_10
    iget-object v0, p0, Lmwi;->f:Lmwg;

    if-nez v0, :cond_a

    .line 8048
    new-instance v0, Lmwg;

    invoke-direct {v0}, Lmwg;-><init>()V

    iput-object v0, p0, Lmwi;->f:Lmwg;

    .line 8050
    :cond_a
    iget-object v0, p0, Lmwi;->f:Lmwg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 8054
    :sswitch_11
    iget-object v0, p0, Lmwi;->q:Lmwh;

    if-nez v0, :cond_b

    .line 8055
    new-instance v0, Lmwh;

    invoke-direct {v0}, Lmwh;-><init>()V

    iput-object v0, p0, Lmwi;->q:Lmwh;

    .line 8057
    :cond_b
    iget-object v0, p0, Lmwi;->q:Lmwh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7948
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 7811
    iget-object v0, p0, Lmwi;->g:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 7812
    const/4 v0, 0x1

    iget-object v1, p0, Lmwi;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 7814
    :cond_0
    iget-object v0, p0, Lmwi;->a:Lola;

    if-eqz v0, :cond_1

    .line 7815
    const/4 v0, 0x2

    iget-object v1, p0, Lmwi;->a:Lola;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7817
    :cond_1
    iget-object v0, p0, Lmwi;->b:Lmtn;

    if-eqz v0, :cond_2

    .line 7818
    const/4 v0, 0x3

    iget-object v1, p0, Lmwi;->b:Lmtn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7820
    :cond_2
    iget-object v0, p0, Lmwi;->h:Lmtw;

    if-eqz v0, :cond_3

    .line 7821
    const/4 v0, 0x4

    iget-object v1, p0, Lmwi;->h:Lmtw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7823
    :cond_3
    iget-object v0, p0, Lmwi;->c:Lmwf;

    if-eqz v0, :cond_4

    .line 7824
    const/4 v0, 0x5

    iget-object v1, p0, Lmwi;->c:Lmwf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7826
    :cond_4
    iget-object v0, p0, Lmwi;->d:Lmwf;

    if-eqz v0, :cond_5

    .line 7827
    const/4 v0, 0x6

    iget-object v1, p0, Lmwi;->d:Lmwf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7829
    :cond_5
    iget-object v0, p0, Lmwi;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 7830
    const/4 v0, 0x7

    iget-object v1, p0, Lmwi;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7832
    :cond_6
    iget-object v0, p0, Lmwi;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 7833
    const/16 v0, 0x8

    iget-object v1, p0, Lmwi;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7835
    :cond_7
    iget-object v0, p0, Lmwi;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 7836
    const/16 v0, 0x9

    iget-object v1, p0, Lmwi;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7838
    :cond_8
    iget-object v0, p0, Lmwi;->l:Lmtx;

    if-eqz v0, :cond_9

    .line 7839
    const/16 v0, 0xa

    iget-object v1, p0, Lmwi;->l:Lmtx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7841
    :cond_9
    iget-object v0, p0, Lmwi;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 7842
    const/16 v0, 0xb

    iget-object v1, p0, Lmwi;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7844
    :cond_a
    iget-object v0, p0, Lmwi;->m:Lmvu;

    if-eqz v0, :cond_b

    .line 7845
    const/16 v0, 0xc

    iget-object v1, p0, Lmwi;->m:Lmvu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7847
    :cond_b
    iget-object v0, p0, Lmwi;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 7848
    const/16 v0, 0xd

    iget-object v1, p0, Lmwi;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7850
    :cond_c
    iget-object v0, p0, Lmwi;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 7851
    const/16 v0, 0xe

    iget-object v1, p0, Lmwi;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7853
    :cond_d
    iget-object v0, p0, Lmwi;->p:Lmxc;

    if-eqz v0, :cond_e

    .line 7854
    const/16 v0, 0xf

    iget-object v1, p0, Lmwi;->p:Lmxc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7856
    :cond_e
    iget-object v0, p0, Lmwi;->f:Lmwg;

    if-eqz v0, :cond_f

    .line 7857
    const/16 v0, 0x10

    iget-object v1, p0, Lmwi;->f:Lmwg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7859
    :cond_f
    iget-object v0, p0, Lmwi;->q:Lmwh;

    if-eqz v0, :cond_10

    .line 7860
    const/16 v0, 0x11

    iget-object v1, p0, Lmwi;->q:Lmwh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7862
    :cond_10
    iget-object v0, p0, Lmwi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7864
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7758
    invoke-virtual {p0, p1}, Lmwi;->a(Loxn;)Lmwi;

    move-result-object v0

    return-object v0
.end method
