.class public final Lmjc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnoe;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29077
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29080
    iput-object v0, p0, Lmjc;->apiHeader:Llyq;

    .line 29083
    iput-object v0, p0, Lmjc;->a:Lnoe;

    .line 29077
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29100
    const/4 v0, 0x0

    .line 29101
    iget-object v1, p0, Lmjc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 29102
    const/4 v0, 0x1

    iget-object v1, p0, Lmjc;->apiHeader:Llyq;

    .line 29103
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29105
    :cond_0
    iget-object v1, p0, Lmjc;->a:Lnoe;

    if-eqz v1, :cond_1

    .line 29106
    const/4 v1, 0x2

    iget-object v2, p0, Lmjc;->a:Lnoe;

    .line 29107
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29109
    :cond_1
    iget-object v1, p0, Lmjc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29110
    iput v0, p0, Lmjc;->ai:I

    .line 29111
    return v0
.end method

.method public a(Loxn;)Lmjc;
    .locals 2

    .prologue
    .line 29119
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29120
    sparse-switch v0, :sswitch_data_0

    .line 29124
    iget-object v1, p0, Lmjc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29125
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjc;->ah:Ljava/util/List;

    .line 29128
    :cond_1
    iget-object v1, p0, Lmjc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29130
    :sswitch_0
    return-object p0

    .line 29135
    :sswitch_1
    iget-object v0, p0, Lmjc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 29136
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmjc;->apiHeader:Llyq;

    .line 29138
    :cond_2
    iget-object v0, p0, Lmjc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29142
    :sswitch_2
    iget-object v0, p0, Lmjc;->a:Lnoe;

    if-nez v0, :cond_3

    .line 29143
    new-instance v0, Lnoe;

    invoke-direct {v0}, Lnoe;-><init>()V

    iput-object v0, p0, Lmjc;->a:Lnoe;

    .line 29145
    :cond_3
    iget-object v0, p0, Lmjc;->a:Lnoe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29120
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29088
    iget-object v0, p0, Lmjc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 29089
    const/4 v0, 0x1

    iget-object v1, p0, Lmjc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29091
    :cond_0
    iget-object v0, p0, Lmjc;->a:Lnoe;

    if-eqz v0, :cond_1

    .line 29092
    const/4 v0, 0x2

    iget-object v1, p0, Lmjc;->a:Lnoe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29094
    :cond_1
    iget-object v0, p0, Lmjc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29096
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29073
    invoke-virtual {p0, p1}, Lmjc;->a(Loxn;)Lmjc;

    move-result-object v0

    return-object v0
.end method
