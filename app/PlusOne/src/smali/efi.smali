.class final Lefi;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Lefh;

.field private final d:Leet;

.field private final e:I


# direct methods
.method constructor <init>(Landroid/content/Context;ILefh;Leet;I)V
    .locals 0

    .prologue
    .line 2940
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2941
    iput-object p1, p0, Lefi;->a:Landroid/content/Context;

    .line 2942
    iput p2, p0, Lefi;->b:I

    .line 2943
    iput p5, p0, Lefi;->e:I

    .line 2944
    iput-object p4, p0, Lefi;->d:Leet;

    .line 2945
    iput-object p3, p0, Lefi;->c:Lefh;

    .line 2946
    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 4

    .prologue
    .line 2950
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2951
    iget-object v1, p0, Lefi;->c:Lefh;

    iget-object v1, v1, Lefh;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2952
    iget-object v1, p0, Lefi;->a:Landroid/content/Context;

    iget v2, p0, Lefi;->b:I

    invoke-static {v1, v2, v0}, Ljvj;->a(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2954
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2955
    iget-object v1, p0, Lefi;->c:Lefh;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljad;

    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lefh;->e:Ljava/lang/Long;

    .line 2957
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 2962
    iget-object v0, p0, Lefi;->d:Leet;

    if-eqz v0, :cond_0

    .line 2963
    iget-object v0, p0, Lefi;->d:Leet;

    iget v1, p0, Lefi;->e:I

    iget-object v2, p0, Lefi;->c:Lefh;

    invoke-static {v0, v1, v2}, Leet;->a(Leet;ILefh;)V

    .line 2965
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2932
    invoke-virtual {p0}, Lefi;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2932
    invoke-virtual {p0}, Lefi;->b()V

    return-void
.end method
