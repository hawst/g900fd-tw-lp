.class public final Lokq;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lokr;

.field private b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2565
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2568
    sget-object v0, Lokr;->a:[Lokr;

    iput-object v0, p0, Lokq;->a:[Lokr;

    .line 2565
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2591
    .line 2592
    iget-object v1, p0, Lokq;->a:[Lokr;

    if-eqz v1, :cond_1

    .line 2593
    iget-object v2, p0, Lokq;->a:[Lokr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2594
    if-eqz v4, :cond_0

    .line 2595
    const/4 v5, 0x1

    .line 2596
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2593
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2600
    :cond_1
    iget-object v1, p0, Lokq;->b:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2601
    const/4 v1, 0x2

    iget-object v2, p0, Lokq;->b:Ljava/lang/Long;

    .line 2602
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2604
    :cond_2
    iget-object v1, p0, Lokq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2605
    iput v0, p0, Lokq;->ai:I

    .line 2606
    return v0
.end method

.method public a(Loxn;)Lokq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2614
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2615
    sparse-switch v0, :sswitch_data_0

    .line 2619
    iget-object v2, p0, Lokq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2620
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lokq;->ah:Ljava/util/List;

    .line 2623
    :cond_1
    iget-object v2, p0, Lokq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2625
    :sswitch_0
    return-object p0

    .line 2630
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2631
    iget-object v0, p0, Lokq;->a:[Lokr;

    if-nez v0, :cond_3

    move v0, v1

    .line 2632
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lokr;

    .line 2633
    iget-object v3, p0, Lokq;->a:[Lokr;

    if-eqz v3, :cond_2

    .line 2634
    iget-object v3, p0, Lokq;->a:[Lokr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2636
    :cond_2
    iput-object v2, p0, Lokq;->a:[Lokr;

    .line 2637
    :goto_2
    iget-object v2, p0, Lokq;->a:[Lokr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2638
    iget-object v2, p0, Lokq;->a:[Lokr;

    new-instance v3, Lokr;

    invoke-direct {v3}, Lokr;-><init>()V

    aput-object v3, v2, v0

    .line 2639
    iget-object v2, p0, Lokq;->a:[Lokr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2640
    invoke-virtual {p1}, Loxn;->a()I

    .line 2637
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2631
    :cond_3
    iget-object v0, p0, Lokq;->a:[Lokr;

    array-length v0, v0

    goto :goto_1

    .line 2643
    :cond_4
    iget-object v2, p0, Lokq;->a:[Lokr;

    new-instance v3, Lokr;

    invoke-direct {v3}, Lokr;-><init>()V

    aput-object v3, v2, v0

    .line 2644
    iget-object v2, p0, Lokq;->a:[Lokr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2648
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lokq;->b:Ljava/lang/Long;

    goto :goto_0

    .line 2615
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2575
    iget-object v0, p0, Lokq;->a:[Lokr;

    if-eqz v0, :cond_1

    .line 2576
    iget-object v1, p0, Lokq;->a:[Lokr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2577
    if-eqz v3, :cond_0

    .line 2578
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2576
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2582
    :cond_1
    iget-object v0, p0, Lokq;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2583
    const/4 v0, 0x2

    iget-object v1, p0, Lokq;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2585
    :cond_2
    iget-object v0, p0, Lokq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2587
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2561
    invoke-virtual {p0, p1}, Lokq;->a(Loxn;)Lokq;

    move-result-object v0

    return-object v0
.end method
