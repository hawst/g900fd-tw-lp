.class public Ldco;
.super Llol;
.source "PG"

# interfaces
.implements Ldgl;


# instance fields
.field private N:Lhei;

.field private O:Lhee;

.field private P:Ldgi;

.field private Q:Ldeo;

.field private R:Landroid/view/View;

.field private S:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0}, Llol;-><init>()V

    .line 50
    new-instance v0, Lhmg;

    sget-object v1, Lond;->d:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Ldco;->au:Llnh;

    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 52
    new-instance v0, Ldem;

    iget-object v1, p0, Ldco;->av:Llqm;

    new-instance v2, Ldcp;

    invoke-direct {v2, p0}, Ldcp;-><init>(Ldco;)V

    invoke-direct {v0, v1, v2}, Ldem;-><init>(Llqr;Lden;)V

    .line 61
    new-instance v0, Ldep;

    iget-object v1, p0, Ldco;->av:Llqm;

    new-instance v2, Ldcq;

    invoke-direct {v2, p0}, Ldcq;-><init>(Ldco;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 69
    return-void
.end method

.method private U()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 132
    iget-object v0, p0, Ldco;->O:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    .line 133
    const/4 v0, -0x1

    if-ne v3, v0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p0, Ldco;->P:Ldgi;

    invoke-interface {v0}, Ldgi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Ldco;->Q:Ldeo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldco;->Q:Ldeo;

    .line 145
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldco;->Q:Ldeo;

    .line 146
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Ldco;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v4

    .line 151
    iget-object v0, p0, Ldco;->N:Lhei;

    .line 152
    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v5, "showed_yb_2014_promo"

    .line 153
    invoke-interface {v0, v5, v2}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 155
    if-nez v0, :cond_0

    iget-object v0, p0, Ldco;->Q:Ldeo;

    .line 156
    invoke-virtual {v0}, Ldeo;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    if-eqz v4, :cond_2

    invoke-interface {v4}, Lddl;->e()Lnzi;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Lddl;->U()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    :goto_1
    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Ldco;->N:Lhei;

    invoke-interface {v0, v3}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v3, "showed_yb_2014_promo"

    .line 160
    invoke-interface {v0, v3, v1}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 161
    invoke-interface {v0}, Lhek;->c()I

    .line 163
    invoke-static {p0}, Lhmc;->a(Llol;)V

    .line 165
    invoke-interface {v4}, Lddl;->r()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0a0b5d

    move v1, v0

    .line 167
    :goto_2
    iget-object v0, p0, Ldco;->R:Landroid/view/View;

    const v3, 0x7f10065b

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 168
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 169
    iget-object v0, p0, Ldco;->R:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 157
    :cond_3
    invoke-interface {v4}, Lddl;->e()Lnzi;

    move-result-object v0

    iget-object v0, v0, Lnzi;->d:Lopf;

    iget-object v0, v0, Lopf;->c:Lpxr;

    iget-object v0, v0, Lpxr;->a:Lood;

    iget-object v0, v0, Lood;->h:Looe;

    iget v0, v0, Looe;->a:I

    const/16 v5, 0x12

    if-ne v0, v5, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    .line 165
    :cond_5
    const v0, 0x7f0a0b5e

    move v1, v0

    goto :goto_2
.end method

.method static synthetic a(Ldco;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ldco;->U()V

    return-void
.end method

.method static synthetic b(Ldco;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ldco;->R:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Ldco;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ldco;->S:Landroid/view/animation/Animation;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 105
    const v0, 0x7f04023c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldco;->R:Landroid/view/View;

    .line 107
    iget-object v0, p0, Ldco;->R:Landroid/view/View;

    new-instance v1, Ldcs;

    invoke-direct {v1, p0}, Ldcs;-><init>(Ldco;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Ldco;->R:Landroid/view/View;

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ldco;->U()V

    .line 176
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Llol;->aO_()V

    .line 93
    iget-object v0, p0, Ldco;->P:Ldgi;

    invoke-interface {v0, p0}, Ldgi;->a(Ldgl;)V

    .line 94
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 180
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 74
    iget-object v0, p0, Ldco;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ldco;->N:Lhei;

    .line 75
    iget-object v0, p0, Ldco;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ldco;->O:Lhee;

    .line 77
    iget-object v0, p0, Ldco;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Ldco;->P:Ldgi;

    .line 79
    iget-object v0, p0, Ldco;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Ldco;->Q:Ldeo;

    .line 81
    invoke-virtual {p0}, Ldco;->n()Lz;

    move-result-object v0

    const v1, 0x7f050015

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldco;->S:Landroid/view/animation/Animation;

    .line 82
    iget-object v0, p0, Ldco;->S:Landroid/view/animation/Animation;

    new-instance v1, Ldcr;

    invoke-direct {v1, p0}, Ldcr;-><init>(Ldco;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 88
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x1

    return v0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Llol;->z()V

    .line 99
    iget-object v0, p0, Ldco;->P:Ldgi;

    invoke-interface {v0, p0}, Ldgi;->b(Ldgl;)V

    .line 100
    return-void
.end method
