.class public final Lpie;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpie;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:Lppf;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Double;

.field private h:Ljava/lang/Double;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    new-array v0, v0, [Lpie;

    sput-object v0, Lpie;->a:[Lpie;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Loxq;-><init>()V

    .line 35
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpie;->c:[Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lpie;->d:Lppf;

    .line 41
    const/high16 v0, -0x80000000

    iput v0, p0, Lpie;->e:I

    .line 30
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 96
    .line 97
    iget-object v0, p0, Lpie;->b:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 98
    const/4 v0, 0x1

    iget-object v2, p0, Lpie;->b:Ljava/lang/String;

    .line 99
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 101
    :goto_0
    iget-object v2, p0, Lpie;->d:Lppf;

    if-eqz v2, :cond_0

    .line 102
    const/4 v2, 0x2

    iget-object v3, p0, Lpie;->d:Lppf;

    .line 103
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 105
    :cond_0
    iget v2, p0, Lpie;->e:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_1

    .line 106
    const/4 v2, 0x3

    iget v3, p0, Lpie;->e:I

    .line 107
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 109
    :cond_1
    iget-object v2, p0, Lpie;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 110
    const/4 v2, 0x4

    iget-object v3, p0, Lpie;->f:Ljava/lang/String;

    .line 111
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 113
    :cond_2
    iget-object v2, p0, Lpie;->g:Ljava/lang/Double;

    if-eqz v2, :cond_3

    .line 114
    const/4 v2, 0x5

    iget-object v3, p0, Lpie;->g:Ljava/lang/Double;

    .line 115
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 117
    :cond_3
    iget-object v2, p0, Lpie;->c:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lpie;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 119
    iget-object v3, p0, Lpie;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 121
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 123
    :cond_4
    add-int/2addr v0, v2

    .line 124
    iget-object v1, p0, Lpie;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 126
    :cond_5
    iget-object v1, p0, Lpie;->h:Ljava/lang/Double;

    if-eqz v1, :cond_6

    .line 127
    const/4 v1, 0x7

    iget-object v2, p0, Lpie;->h:Ljava/lang/Double;

    .line 128
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 130
    :cond_6
    iget-object v1, p0, Lpie;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 131
    const/16 v1, 0x8

    iget-object v2, p0, Lpie;->i:Ljava/lang/Integer;

    .line 132
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 134
    :cond_7
    iget-object v1, p0, Lpie;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 135
    const/16 v1, 0x9

    iget-object v2, p0, Lpie;->j:Ljava/lang/Integer;

    .line 136
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 138
    :cond_8
    iget-object v1, p0, Lpie;->k:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 139
    const/16 v1, 0xa

    iget-object v2, p0, Lpie;->k:Ljava/lang/Float;

    .line 140
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 142
    :cond_9
    iget-object v1, p0, Lpie;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    iput v0, p0, Lpie;->ai:I

    .line 144
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpie;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 152
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 153
    sparse-switch v0, :sswitch_data_0

    .line 157
    iget-object v1, p0, Lpie;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 158
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpie;->ah:Ljava/util/List;

    .line 161
    :cond_1
    iget-object v1, p0, Lpie;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :sswitch_0
    return-object p0

    .line 168
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpie;->b:Ljava/lang/String;

    goto :goto_0

    .line 172
    :sswitch_2
    iget-object v0, p0, Lpie;->d:Lppf;

    if-nez v0, :cond_2

    .line 173
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpie;->d:Lppf;

    .line 175
    :cond_2
    iget-object v0, p0, Lpie;->d:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 179
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 180
    if-eq v0, v4, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 183
    :cond_3
    iput v0, p0, Lpie;->e:I

    goto :goto_0

    .line 185
    :cond_4
    iput v4, p0, Lpie;->e:I

    goto :goto_0

    .line 190
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpie;->f:Ljava/lang/String;

    goto :goto_0

    .line 194
    :sswitch_5
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lpie;->g:Ljava/lang/Double;

    goto :goto_0

    .line 198
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 199
    iget-object v0, p0, Lpie;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 200
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 201
    iget-object v2, p0, Lpie;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 202
    iput-object v1, p0, Lpie;->c:[Ljava/lang/String;

    .line 203
    :goto_1
    iget-object v1, p0, Lpie;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    .line 204
    iget-object v1, p0, Lpie;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 205
    invoke-virtual {p1}, Loxn;->a()I

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 208
    :cond_5
    iget-object v1, p0, Lpie;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 212
    :sswitch_7
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lpie;->h:Ljava/lang/Double;

    goto/16 :goto_0

    .line 216
    :sswitch_8
    invoke-virtual {p1}, Loxn;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpie;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 220
    :sswitch_9
    invoke-virtual {p1}, Loxn;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpie;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 224
    :sswitch_a
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpie;->k:Ljava/lang/Float;

    goto/16 :goto_0

    .line 153
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
        0x32 -> :sswitch_6
        0x39 -> :sswitch_7
        0x45 -> :sswitch_8
        0x4d -> :sswitch_9
        0x55 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lpie;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x1

    iget-object v1, p0, Lpie;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lpie;->d:Lppf;

    if-eqz v0, :cond_1

    .line 62
    const/4 v0, 0x2

    iget-object v1, p0, Lpie;->d:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 64
    :cond_1
    iget v0, p0, Lpie;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 65
    const/4 v0, 0x3

    iget v1, p0, Lpie;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 67
    :cond_2
    iget-object v0, p0, Lpie;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 68
    const/4 v0, 0x4

    iget-object v1, p0, Lpie;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 70
    :cond_3
    iget-object v0, p0, Lpie;->g:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 71
    const/4 v0, 0x5

    iget-object v1, p0, Lpie;->g:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 73
    :cond_4
    iget-object v0, p0, Lpie;->c:[Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 74
    iget-object v1, p0, Lpie;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 75
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_5
    iget-object v0, p0, Lpie;->h:Ljava/lang/Double;

    if-eqz v0, :cond_6

    .line 79
    const/4 v0, 0x7

    iget-object v1, p0, Lpie;->h:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 81
    :cond_6
    iget-object v0, p0, Lpie;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 82
    const/16 v0, 0x8

    iget-object v1, p0, Lpie;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->b(II)V

    .line 84
    :cond_7
    iget-object v0, p0, Lpie;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 85
    const/16 v0, 0x9

    iget-object v1, p0, Lpie;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->b(II)V

    .line 87
    :cond_8
    iget-object v0, p0, Lpie;->k:Ljava/lang/Float;

    if-eqz v0, :cond_9

    .line 88
    const/16 v0, 0xa

    iget-object v1, p0, Lpie;->k:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 90
    :cond_9
    iget-object v0, p0, Lpie;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 92
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lpie;->a(Loxn;)Lpie;

    move-result-object v0

    return-object v0
.end method
