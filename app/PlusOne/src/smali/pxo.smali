.class public final Lpxo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpxo;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    new-array v0, v0, [Lpxo;

    sput-object v0, Lpxo;->a:[Lpxo;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 184
    const/4 v0, 0x0

    .line 185
    iget-object v1, p0, Lpxo;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 186
    const/4 v0, 0x1

    iget-object v1, p0, Lpxo;->e:Ljava/lang/String;

    .line 187
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 189
    :cond_0
    iget-object v1, p0, Lpxo;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 190
    const/4 v1, 0x2

    iget-object v2, p0, Lpxo;->b:Ljava/lang/String;

    .line 191
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_1
    iget-object v1, p0, Lpxo;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 194
    const/4 v1, 0x3

    iget-object v2, p0, Lpxo;->c:Ljava/lang/String;

    .line 195
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    :cond_2
    iget-object v1, p0, Lpxo;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 198
    const/4 v1, 0x4

    iget-object v2, p0, Lpxo;->f:Ljava/lang/Boolean;

    .line 199
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 201
    :cond_3
    iget-object v1, p0, Lpxo;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 202
    const/4 v1, 0x5

    iget-object v2, p0, Lpxo;->d:Ljava/lang/String;

    .line 203
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    :cond_4
    iget-object v1, p0, Lpxo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    iput v0, p0, Lpxo;->ai:I

    .line 207
    return v0
.end method

.method public a(Loxn;)Lpxo;
    .locals 2

    .prologue
    .line 215
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 216
    sparse-switch v0, :sswitch_data_0

    .line 220
    iget-object v1, p0, Lpxo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 221
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpxo;->ah:Ljava/util/List;

    .line 224
    :cond_1
    iget-object v1, p0, Lpxo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    :sswitch_0
    return-object p0

    .line 231
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxo;->e:Ljava/lang/String;

    goto :goto_0

    .line 235
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxo;->b:Ljava/lang/String;

    goto :goto_0

    .line 239
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxo;->c:Ljava/lang/String;

    goto :goto_0

    .line 243
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpxo;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 247
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxo;->d:Ljava/lang/String;

    goto :goto_0

    .line 216
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lpxo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x1

    iget-object v1, p0, Lpxo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 166
    :cond_0
    iget-object v0, p0, Lpxo;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 167
    const/4 v0, 0x2

    iget-object v1, p0, Lpxo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 169
    :cond_1
    iget-object v0, p0, Lpxo;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 170
    const/4 v0, 0x3

    iget-object v1, p0, Lpxo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 172
    :cond_2
    iget-object v0, p0, Lpxo;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 173
    const/4 v0, 0x4

    iget-object v1, p0, Lpxo;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 175
    :cond_3
    iget-object v0, p0, Lpxo;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 176
    const/4 v0, 0x5

    iget-object v1, p0, Lpxo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 178
    :cond_4
    iget-object v0, p0, Lpxo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 180
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Lpxo;->a(Loxn;)Lpxo;

    move-result-object v0

    return-object v0
.end method
