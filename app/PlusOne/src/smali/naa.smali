.class public final Lnaa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnaa;


# instance fields
.field private b:[Lows;

.field private c:Lota;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1245
    const/4 v0, 0x0

    new-array v0, v0, [Lnaa;

    sput-object v0, Lnaa;->a:[Lnaa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1246
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1249
    sget-object v0, Lows;->a:[Lows;

    iput-object v0, p0, Lnaa;->b:[Lows;

    .line 1252
    const/4 v0, 0x0

    iput-object v0, p0, Lnaa;->c:Lota;

    .line 1246
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1283
    .line 1284
    iget-object v1, p0, Lnaa;->b:[Lows;

    if-eqz v1, :cond_1

    .line 1285
    iget-object v2, p0, Lnaa;->b:[Lows;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1286
    if-eqz v4, :cond_0

    .line 1287
    const/4 v5, 0x1

    .line 1288
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1285
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1292
    :cond_1
    iget-object v1, p0, Lnaa;->c:Lota;

    if-eqz v1, :cond_2

    .line 1293
    const/4 v1, 0x2

    iget-object v2, p0, Lnaa;->c:Lota;

    .line 1294
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1296
    :cond_2
    iget-object v1, p0, Lnaa;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1297
    const/4 v1, 0x3

    iget-object v2, p0, Lnaa;->d:Ljava/lang/String;

    .line 1298
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1300
    :cond_3
    iget-object v1, p0, Lnaa;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1301
    const/4 v1, 0x4

    iget-object v2, p0, Lnaa;->e:Ljava/lang/String;

    .line 1302
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1304
    :cond_4
    iget-object v1, p0, Lnaa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1305
    iput v0, p0, Lnaa;->ai:I

    .line 1306
    return v0
.end method

.method public a(Loxn;)Lnaa;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1314
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1315
    sparse-switch v0, :sswitch_data_0

    .line 1319
    iget-object v2, p0, Lnaa;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1320
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnaa;->ah:Ljava/util/List;

    .line 1323
    :cond_1
    iget-object v2, p0, Lnaa;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1325
    :sswitch_0
    return-object p0

    .line 1330
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1331
    iget-object v0, p0, Lnaa;->b:[Lows;

    if-nez v0, :cond_3

    move v0, v1

    .line 1332
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lows;

    .line 1333
    iget-object v3, p0, Lnaa;->b:[Lows;

    if-eqz v3, :cond_2

    .line 1334
    iget-object v3, p0, Lnaa;->b:[Lows;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1336
    :cond_2
    iput-object v2, p0, Lnaa;->b:[Lows;

    .line 1337
    :goto_2
    iget-object v2, p0, Lnaa;->b:[Lows;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1338
    iget-object v2, p0, Lnaa;->b:[Lows;

    new-instance v3, Lows;

    invoke-direct {v3}, Lows;-><init>()V

    aput-object v3, v2, v0

    .line 1339
    iget-object v2, p0, Lnaa;->b:[Lows;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1340
    invoke-virtual {p1}, Loxn;->a()I

    .line 1337
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1331
    :cond_3
    iget-object v0, p0, Lnaa;->b:[Lows;

    array-length v0, v0

    goto :goto_1

    .line 1343
    :cond_4
    iget-object v2, p0, Lnaa;->b:[Lows;

    new-instance v3, Lows;

    invoke-direct {v3}, Lows;-><init>()V

    aput-object v3, v2, v0

    .line 1344
    iget-object v2, p0, Lnaa;->b:[Lows;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1348
    :sswitch_2
    iget-object v0, p0, Lnaa;->c:Lota;

    if-nez v0, :cond_5

    .line 1349
    new-instance v0, Lota;

    invoke-direct {v0}, Lota;-><init>()V

    iput-object v0, p0, Lnaa;->c:Lota;

    .line 1351
    :cond_5
    iget-object v0, p0, Lnaa;->c:Lota;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1355
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnaa;->d:Ljava/lang/String;

    goto :goto_0

    .line 1359
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnaa;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 1315
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1261
    iget-object v0, p0, Lnaa;->b:[Lows;

    if-eqz v0, :cond_1

    .line 1262
    iget-object v1, p0, Lnaa;->b:[Lows;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1263
    if-eqz v3, :cond_0

    .line 1264
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1262
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1268
    :cond_1
    iget-object v0, p0, Lnaa;->c:Lota;

    if-eqz v0, :cond_2

    .line 1269
    const/4 v0, 0x2

    iget-object v1, p0, Lnaa;->c:Lota;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1271
    :cond_2
    iget-object v0, p0, Lnaa;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1272
    const/4 v0, 0x3

    iget-object v1, p0, Lnaa;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1274
    :cond_3
    iget-object v0, p0, Lnaa;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1275
    const/4 v0, 0x4

    iget-object v1, p0, Lnaa;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1277
    :cond_4
    iget-object v0, p0, Lnaa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1279
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1242
    invoke-virtual {p0, p1}, Lnaa;->a(Loxn;)Lnaa;

    move-result-object v0

    return-object v0
.end method
