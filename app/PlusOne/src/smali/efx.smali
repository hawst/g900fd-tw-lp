.class final Lefx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ldvw;",
        ">;>;"
    }
.end annotation


# instance fields
.field private synthetic a:Lefj;


# direct methods
.method constructor <init>(Lefj;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lefx;->a:Lefj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 306
    new-instance v6, Lnqx;

    invoke-direct {v6}, Lnqx;-><init>()V

    .line 307
    const-string v2, "list_type"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v6, Lnqx;->b:I

    .line 308
    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v6, Lnqx;->c:Ljava/lang/Integer;

    .line 309
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v6, Lnqx;->e:Ljava/lang/Boolean;

    .line 310
    iget v2, v6, Lnqx;->b:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    iget v2, v6, Lnqx;->b:I

    if-ne v2, v4, :cond_3

    .line 311
    :cond_0
    new-instance v2, Lohl;

    invoke-direct {v2}, Lohl;-><init>()V

    iput-object v2, v6, Lnqx;->d:Lohl;

    .line 312
    iget-object v2, v6, Lnqx;->d:Lohl;

    iget v3, v6, Lnqx;->b:I

    if-ne v3, v4, :cond_2

    :goto_0
    iput v0, v2, Lohl;->b:I

    .line 314
    iget-object v0, v6, Lnqx;->d:Lohl;

    const-string v1, "name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lohl;->a:Ljava/lang/String;

    .line 315
    iget-object v0, v6, Lnqx;->d:Lohl;

    const-string v1, "start_year"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lohl;->c:Ljava/lang/Integer;

    .line 316
    iget-object v0, v6, Lnqx;->d:Lohl;

    const-string v1, "end_year"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lohl;->d:Ljava/lang/Integer;

    .line 323
    :cond_1
    :goto_1
    new-instance v1, Lemi;

    iget-object v0, p0, Lefx;->a:Lefj;

    invoke-static {v0}, Lefj;->c(Lefj;)Llnl;

    move-result-object v2

    iget-object v0, p0, Lefx;->a:Lefj;

    .line 324
    invoke-static {v0}, Lefj;->a(Lefj;)Lhee;

    move-result-object v0

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    const-wide/32 v4, 0x7fffffff

    invoke-direct/range {v1 .. v6}, Lemi;-><init>(Landroid/content/Context;IJLnqx;)V

    .line 325
    return-object v1

    :cond_2
    move v0, v1

    .line 312
    goto :goto_0

    .line 318
    :cond_3
    iget v0, v6, Lnqx;->b:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    .line 319
    new-instance v0, Lnqw;

    invoke-direct {v0}, Lnqw;-><init>()V

    iput-object v0, v6, Lnqx;->f:Lnqw;

    .line 320
    iget-object v0, v6, Lnqx;->f:Lnqw;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnqw;->a:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 337
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 301
    check-cast p2, Ljava/util/ArrayList;

    invoke-virtual {p0, p2}, Lefx;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 331
    iget-object v0, p0, Lefx;->a:Lefj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lefj;->a(Lefj;Z)Z

    .line 332
    iget-object v0, p0, Lefx;->a:Lefj;

    invoke-static {v0, p1}, Lefj;->b(Lefj;Ljava/util/ArrayList;)V

    .line 333
    return-void
.end method
