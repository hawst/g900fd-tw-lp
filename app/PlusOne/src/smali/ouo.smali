.class public final Louo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Louo;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Loun;

.field private g:I

.field private h:I

.field private i:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Louo;

    sput-object v0, Louo;->a:[Louo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput v0, p0, Louo;->g:I

    .line 18
    iput v0, p0, Louo;->h:I

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Louo;->f:Loun;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 64
    const/4 v0, 0x0

    .line 65
    iget v1, p0, Louo;->g:I

    if-eq v1, v3, :cond_0

    .line 66
    const/4 v0, 0x1

    iget v1, p0, Louo;->g:I

    .line 67
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 69
    :cond_0
    iget-object v1, p0, Louo;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 70
    const/4 v1, 0x2

    iget-object v2, p0, Louo;->c:Ljava/lang/String;

    .line 71
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_1
    iget-object v1, p0, Louo;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 74
    const/4 v1, 0x3

    iget-object v2, p0, Louo;->d:Ljava/lang/String;

    .line 75
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_2
    iget-object v1, p0, Louo;->f:Loun;

    if-eqz v1, :cond_3

    .line 78
    const/4 v1, 0x4

    iget-object v2, p0, Louo;->f:Loun;

    .line 79
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_3
    iget-object v1, p0, Louo;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 82
    const/4 v1, 0x5

    iget-object v2, p0, Louo;->e:Ljava/lang/String;

    .line 83
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_4
    iget-object v1, p0, Louo;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 86
    const/4 v1, 0x6

    iget-object v2, p0, Louo;->b:Ljava/lang/Integer;

    .line 87
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_5
    iget-object v1, p0, Louo;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 90
    const/16 v1, 0x8

    iget-object v2, p0, Louo;->i:Ljava/lang/Integer;

    .line 91
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_6
    iget v1, p0, Louo;->h:I

    if-eq v1, v3, :cond_7

    .line 94
    const/16 v1, 0x9

    iget v2, p0, Louo;->h:I

    .line 95
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_7
    iget-object v1, p0, Louo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    iput v0, p0, Louo;->ai:I

    .line 99
    return v0
.end method

.method public a(Loxn;)Louo;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 107
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 108
    sparse-switch v0, :sswitch_data_0

    .line 112
    iget-object v1, p0, Louo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 113
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Louo;->ah:Ljava/util/List;

    .line 116
    :cond_1
    iget-object v1, p0, Louo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    :sswitch_0
    return-object p0

    .line 123
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 124
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-ne v0, v1, :cond_3

    .line 147
    :cond_2
    iput v0, p0, Louo;->g:I

    goto :goto_0

    .line 149
    :cond_3
    iput v2, p0, Louo;->g:I

    goto :goto_0

    .line 154
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Louo;->c:Ljava/lang/String;

    goto :goto_0

    .line 158
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Louo;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 162
    :sswitch_4
    iget-object v0, p0, Louo;->f:Loun;

    if-nez v0, :cond_4

    .line 163
    new-instance v0, Loun;

    invoke-direct {v0}, Loun;-><init>()V

    iput-object v0, p0, Louo;->f:Loun;

    .line 165
    :cond_4
    iget-object v0, p0, Louo;->f:Loun;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 169
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Louo;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 173
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Louo;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 177
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Louo;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 181
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 182
    if-eqz v0, :cond_5

    if-ne v0, v3, :cond_6

    .line 184
    :cond_5
    iput v0, p0, Louo;->h:I

    goto/16 :goto_0

    .line 186
    :cond_6
    iput v2, p0, Louo;->h:I

    goto/16 :goto_0

    .line 108
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 34
    iget v0, p0, Louo;->g:I

    if-eq v0, v2, :cond_0

    .line 35
    const/4 v0, 0x1

    iget v1, p0, Louo;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 37
    :cond_0
    iget-object v0, p0, Louo;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 38
    const/4 v0, 0x2

    iget-object v1, p0, Louo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    :cond_1
    iget-object v0, p0, Louo;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 41
    const/4 v0, 0x3

    iget-object v1, p0, Louo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 43
    :cond_2
    iget-object v0, p0, Louo;->f:Loun;

    if-eqz v0, :cond_3

    .line 44
    const/4 v0, 0x4

    iget-object v1, p0, Louo;->f:Loun;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 46
    :cond_3
    iget-object v0, p0, Louo;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 47
    const/4 v0, 0x5

    iget-object v1, p0, Louo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 49
    :cond_4
    iget-object v0, p0, Louo;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 50
    const/4 v0, 0x6

    iget-object v1, p0, Louo;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 52
    :cond_5
    iget-object v0, p0, Louo;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 53
    const/16 v0, 0x8

    iget-object v1, p0, Louo;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 55
    :cond_6
    iget v0, p0, Louo;->h:I

    if-eq v0, v2, :cond_7

    .line 56
    const/16 v0, 0x9

    iget v1, p0, Louo;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 58
    :cond_7
    iget-object v0, p0, Louo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 60
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Louo;->a(Loxn;)Louo;

    move-result-object v0

    return-object v0
.end method
