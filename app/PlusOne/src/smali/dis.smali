.class public final Ldis;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmhi;",
        "Lmhj;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Lpbl;

.field private final p:Lhgw;

.field private final q:Ljava/lang/String;

.field private final r:Lkzl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ldis;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x194
        0x160
        0x14f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;ILpbl;Lhgw;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 49
    const-string v3, "postactivity"

    new-instance v4, Lmhi;

    invoke-direct {v4}, Lmhi;-><init>()V

    new-instance v5, Lmhj;

    invoke-direct {v5}, Lmhj;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 51
    iget-object v0, p0, Ldis;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldis;->r:Lkzl;

    .line 52
    iput-object p3, p0, Ldis;->b:Lpbl;

    .line 53
    iput-object p4, p0, Ldis;->p:Lhgw;

    .line 54
    iput-object p5, p0, Ldis;->q:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method protected a(Lmhi;)V
    .locals 4

    .prologue
    .line 59
    new-instance v0, Lodg;

    invoke-direct {v0}, Lodg;-><init>()V

    iput-object v0, p1, Lmhi;->a:Lodg;

    .line 60
    iget-object v0, p1, Lmhi;->a:Lodg;

    .line 61
    new-instance v1, Lodh;

    invoke-direct {v1}, Lodh;-><init>()V

    iput-object v1, v0, Lodg;->p:Lodh;

    .line 62
    iget-object v1, v0, Lodg;->p:Lodh;

    const-string v2, "Mobile"

    iput-object v2, v1, Lodh;->a:Ljava/lang/String;

    .line 64
    new-instance v1, Lpee;

    invoke-direct {v1}, Lpee;-><init>()V

    iput-object v1, v0, Lodg;->b:Lpee;

    .line 65
    iget-object v1, v0, Lodg;->b:Lpee;

    new-instance v2, Lpef;

    invoke-direct {v2}, Lpef;-><init>()V

    iput-object v2, v1, Lpee;->a:Lpef;

    .line 66
    iget-object v1, v0, Lodg;->b:Lpee;

    iget-object v1, v1, Lpee;->a:Lpef;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lpef;->a:Ljava/lang/Boolean;

    .line 68
    iget-object v1, p0, Ldis;->q:Ljava/lang/String;

    iput-object v1, v0, Lodg;->c:Ljava/lang/String;

    .line 70
    iget-object v1, p0, Ldis;->p:Lhgw;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lhft;->a(Lhgw;Lhgw;)Lock;

    move-result-object v1

    iput-object v1, v0, Lodg;->j:Lock;

    .line 72
    iget-object v1, p0, Ldis;->b:Lpbl;

    iget-object v1, v1, Lpbl;->c:Ljava/lang/String;

    iput-object v1, v0, Lodg;->a:Ljava/lang/String;

    .line 74
    new-instance v1, Loya;

    invoke-direct {v1}, Loya;-><init>()V

    iput-object v1, v0, Lodg;->q:Loya;

    .line 75
    iget-object v1, v0, Lodg;->q:Loya;

    sget-object v2, Ldis;->a:[I

    iput-object v2, v1, Loya;->b:[I

    .line 76
    iget-object v1, v0, Lodg;->q:Loya;

    sget-object v2, Lpbl;->a:Loxr;

    iget-object v3, p0, Ldis;->b:Lpbl;

    invoke-virtual {v1, v2, v3}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 78
    new-instance v1, Loxz;

    invoke-direct {v1}, Loxz;-><init>()V

    iput-object v1, v0, Lodg;->t:Loxz;

    .line 79
    iget-object v0, v0, Lodg;->t:Loxz;

    iget-object v1, p0, Ldis;->r:Lkzl;

    iget-object v2, p0, Ldis;->f:Landroid/content/Context;

    iget v3, p0, Ldis;->c:I

    invoke-interface {v1, v2, v3}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v1

    iput-object v1, v0, Loxz;->a:[I

    .line 80
    return-void
.end method

.method protected a(Lmhj;)V
    .locals 6

    .prologue
    .line 84
    iget-object v2, p1, Lmhj;->a:Loda;

    .line 85
    if-eqz v2, :cond_0

    iget-object v0, v2, Loda;->a:Logi;

    if-eqz v0, :cond_0

    iget-object v0, v2, Loda;->a:Logi;

    iget-object v0, v0, Logi;->a:[Logr;

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, v2, Loda;->a:Logi;

    iget-object v0, v0, Logi;->a:[Logr;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 89
    iget-object v0, v2, Loda;->a:Logi;

    iget-object v0, v0, Logi;->a:[Logr;

    aget-object v3, v0, v1

    .line 91
    if-eqz v3, :cond_1

    iget-object v0, v3, Logr;->M:Loya;

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, v3, Logr;->M:Loya;

    sget-object v4, Lpbl;->a:Loxr;

    .line 93
    invoke-virtual {v0, v4}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpbl;

    .line 95
    if-eqz v0, :cond_1

    .line 96
    iget-object v1, p0, Ldis;->f:Landroid/content/Context;

    iget v2, p0, Ldis;->c:I

    iget-object v4, v3, Logr;->i:Ljava/lang/String;

    new-instance v5, Lidh;

    invoke-direct {v5, v0}, Lidh;-><init>(Lpbl;)V

    invoke-static {v1, v2, v4, v5, v3}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Lidh;Logr;)V

    .line 103
    :cond_0
    return-void

    .line 88
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lmhi;

    invoke-virtual {p0, p1}, Ldis;->a(Lmhi;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lmhj;

    invoke-virtual {p0, p1}, Ldis;->a(Lmhj;)V

    return-void
.end method
