.class public final Lnwk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwl;

.field public b:Lnwl;

.field public c:[Lnwl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1073
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1076
    iput-object v0, p0, Lnwk;->a:Lnwl;

    .line 1079
    iput-object v0, p0, Lnwk;->b:Lnwl;

    .line 1082
    sget-object v0, Lnwl;->a:[Lnwl;

    iput-object v0, p0, Lnwk;->c:[Lnwl;

    .line 1073
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1106
    .line 1107
    iget-object v0, p0, Lnwk;->a:Lnwl;

    if-eqz v0, :cond_3

    .line 1108
    const/4 v0, 0x1

    iget-object v2, p0, Lnwk;->a:Lnwl;

    .line 1109
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1111
    :goto_0
    iget-object v2, p0, Lnwk;->b:Lnwl;

    if-eqz v2, :cond_0

    .line 1112
    const/4 v2, 0x2

    iget-object v3, p0, Lnwk;->b:Lnwl;

    .line 1113
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1115
    :cond_0
    iget-object v2, p0, Lnwk;->c:[Lnwl;

    if-eqz v2, :cond_2

    .line 1116
    iget-object v2, p0, Lnwk;->c:[Lnwl;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1117
    if-eqz v4, :cond_1

    .line 1118
    const/4 v5, 0x3

    .line 1119
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1116
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1123
    :cond_2
    iget-object v1, p0, Lnwk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1124
    iput v0, p0, Lnwk;->ai:I

    .line 1125
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnwk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1134
    sparse-switch v0, :sswitch_data_0

    .line 1138
    iget-object v2, p0, Lnwk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1139
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnwk;->ah:Ljava/util/List;

    .line 1142
    :cond_1
    iget-object v2, p0, Lnwk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1144
    :sswitch_0
    return-object p0

    .line 1149
    :sswitch_1
    iget-object v0, p0, Lnwk;->a:Lnwl;

    if-nez v0, :cond_2

    .line 1150
    new-instance v0, Lnwl;

    invoke-direct {v0}, Lnwl;-><init>()V

    iput-object v0, p0, Lnwk;->a:Lnwl;

    .line 1152
    :cond_2
    iget-object v0, p0, Lnwk;->a:Lnwl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1156
    :sswitch_2
    iget-object v0, p0, Lnwk;->b:Lnwl;

    if-nez v0, :cond_3

    .line 1157
    new-instance v0, Lnwl;

    invoke-direct {v0}, Lnwl;-><init>()V

    iput-object v0, p0, Lnwk;->b:Lnwl;

    .line 1159
    :cond_3
    iget-object v0, p0, Lnwk;->b:Lnwl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1163
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1164
    iget-object v0, p0, Lnwk;->c:[Lnwl;

    if-nez v0, :cond_5

    move v0, v1

    .line 1165
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnwl;

    .line 1166
    iget-object v3, p0, Lnwk;->c:[Lnwl;

    if-eqz v3, :cond_4

    .line 1167
    iget-object v3, p0, Lnwk;->c:[Lnwl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1169
    :cond_4
    iput-object v2, p0, Lnwk;->c:[Lnwl;

    .line 1170
    :goto_2
    iget-object v2, p0, Lnwk;->c:[Lnwl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1171
    iget-object v2, p0, Lnwk;->c:[Lnwl;

    new-instance v3, Lnwl;

    invoke-direct {v3}, Lnwl;-><init>()V

    aput-object v3, v2, v0

    .line 1172
    iget-object v2, p0, Lnwk;->c:[Lnwl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1173
    invoke-virtual {p1}, Loxn;->a()I

    .line 1170
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1164
    :cond_5
    iget-object v0, p0, Lnwk;->c:[Lnwl;

    array-length v0, v0

    goto :goto_1

    .line 1176
    :cond_6
    iget-object v2, p0, Lnwk;->c:[Lnwl;

    new-instance v3, Lnwl;

    invoke-direct {v3}, Lnwl;-><init>()V

    aput-object v3, v2, v0

    .line 1177
    iget-object v2, p0, Lnwk;->c:[Lnwl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1134
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1087
    iget-object v0, p0, Lnwk;->a:Lnwl;

    if-eqz v0, :cond_0

    .line 1088
    const/4 v0, 0x1

    iget-object v1, p0, Lnwk;->a:Lnwl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1090
    :cond_0
    iget-object v0, p0, Lnwk;->b:Lnwl;

    if-eqz v0, :cond_1

    .line 1091
    const/4 v0, 0x2

    iget-object v1, p0, Lnwk;->b:Lnwl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1093
    :cond_1
    iget-object v0, p0, Lnwk;->c:[Lnwl;

    if-eqz v0, :cond_3

    .line 1094
    iget-object v1, p0, Lnwk;->c:[Lnwl;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1095
    if-eqz v3, :cond_2

    .line 1096
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1094
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1100
    :cond_3
    iget-object v0, p0, Lnwk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1102
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1069
    invoke-virtual {p0, p1}, Lnwk;->a(Loxn;)Lnwk;

    move-result-object v0

    return-object v0
.end method
