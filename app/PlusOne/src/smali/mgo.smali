.class public final Lmgo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnao;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23731
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23734
    iput-object v0, p0, Lmgo;->apiHeader:Llyq;

    .line 23737
    iput-object v0, p0, Lmgo;->a:Lnao;

    .line 23731
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 23754
    const/4 v0, 0x0

    .line 23755
    iget-object v1, p0, Lmgo;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 23756
    const/4 v0, 0x1

    iget-object v1, p0, Lmgo;->apiHeader:Llyq;

    .line 23757
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23759
    :cond_0
    iget-object v1, p0, Lmgo;->a:Lnao;

    if-eqz v1, :cond_1

    .line 23760
    const/4 v1, 0x2

    iget-object v2, p0, Lmgo;->a:Lnao;

    .line 23761
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23763
    :cond_1
    iget-object v1, p0, Lmgo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23764
    iput v0, p0, Lmgo;->ai:I

    .line 23765
    return v0
.end method

.method public a(Loxn;)Lmgo;
    .locals 2

    .prologue
    .line 23773
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 23774
    sparse-switch v0, :sswitch_data_0

    .line 23778
    iget-object v1, p0, Lmgo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 23779
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgo;->ah:Ljava/util/List;

    .line 23782
    :cond_1
    iget-object v1, p0, Lmgo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23784
    :sswitch_0
    return-object p0

    .line 23789
    :sswitch_1
    iget-object v0, p0, Lmgo;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 23790
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmgo;->apiHeader:Llyq;

    .line 23792
    :cond_2
    iget-object v0, p0, Lmgo;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23796
    :sswitch_2
    iget-object v0, p0, Lmgo;->a:Lnao;

    if-nez v0, :cond_3

    .line 23797
    new-instance v0, Lnao;

    invoke-direct {v0}, Lnao;-><init>()V

    iput-object v0, p0, Lmgo;->a:Lnao;

    .line 23799
    :cond_3
    iget-object v0, p0, Lmgo;->a:Lnao;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23774
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23742
    iget-object v0, p0, Lmgo;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 23743
    const/4 v0, 0x1

    iget-object v1, p0, Lmgo;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23745
    :cond_0
    iget-object v0, p0, Lmgo;->a:Lnao;

    if-eqz v0, :cond_1

    .line 23746
    const/4 v0, 0x2

    iget-object v1, p0, Lmgo;->a:Lnao;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23748
    :cond_1
    iget-object v0, p0, Lmgo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23750
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 23727
    invoke-virtual {p0, p1}, Lmgo;->a(Loxn;)Lmgo;

    move-result-object v0

    return-object v0
.end method
