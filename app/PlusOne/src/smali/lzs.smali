.class public final Llzs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlk;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32479
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32482
    iput-object v0, p0, Llzs;->apiHeader:Llyq;

    .line 32485
    iput-object v0, p0, Llzs;->a:Lmlk;

    .line 32479
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32502
    const/4 v0, 0x0

    .line 32503
    iget-object v1, p0, Llzs;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 32504
    const/4 v0, 0x1

    iget-object v1, p0, Llzs;->apiHeader:Llyq;

    .line 32505
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32507
    :cond_0
    iget-object v1, p0, Llzs;->a:Lmlk;

    if-eqz v1, :cond_1

    .line 32508
    const/4 v1, 0x2

    iget-object v2, p0, Llzs;->a:Lmlk;

    .line 32509
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32511
    :cond_1
    iget-object v1, p0, Llzs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32512
    iput v0, p0, Llzs;->ai:I

    .line 32513
    return v0
.end method

.method public a(Loxn;)Llzs;
    .locals 2

    .prologue
    .line 32521
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32522
    sparse-switch v0, :sswitch_data_0

    .line 32526
    iget-object v1, p0, Llzs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32527
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzs;->ah:Ljava/util/List;

    .line 32530
    :cond_1
    iget-object v1, p0, Llzs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32532
    :sswitch_0
    return-object p0

    .line 32537
    :sswitch_1
    iget-object v0, p0, Llzs;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 32538
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzs;->apiHeader:Llyq;

    .line 32540
    :cond_2
    iget-object v0, p0, Llzs;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32544
    :sswitch_2
    iget-object v0, p0, Llzs;->a:Lmlk;

    if-nez v0, :cond_3

    .line 32545
    new-instance v0, Lmlk;

    invoke-direct {v0}, Lmlk;-><init>()V

    iput-object v0, p0, Llzs;->a:Lmlk;

    .line 32547
    :cond_3
    iget-object v0, p0, Llzs;->a:Lmlk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32522
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32490
    iget-object v0, p0, Llzs;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 32491
    const/4 v0, 0x1

    iget-object v1, p0, Llzs;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32493
    :cond_0
    iget-object v0, p0, Llzs;->a:Lmlk;

    if-eqz v0, :cond_1

    .line 32494
    const/4 v0, 0x2

    iget-object v1, p0, Llzs;->a:Lmlk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32496
    :cond_1
    iget-object v0, p0, Llzs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32498
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32475
    invoke-virtual {p0, p1}, Llzs;->a(Loxn;)Llzs;

    move-result-object v0

    return-object v0
.end method
