.class public final Lnet;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Lnyp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3116
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3123
    const/4 v0, 0x0

    iput-object v0, p0, Lnet;->c:Lnyp;

    .line 3116
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3143
    const/4 v0, 0x0

    .line 3144
    iget-object v1, p0, Lnet;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3145
    const/4 v0, 0x1

    iget-object v1, p0, Lnet;->a:Ljava/lang/String;

    .line 3146
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3148
    :cond_0
    iget-object v1, p0, Lnet;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3149
    const/4 v1, 0x2

    iget-object v2, p0, Lnet;->b:Ljava/lang/String;

    .line 3150
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3152
    :cond_1
    iget-object v1, p0, Lnet;->c:Lnyp;

    if-eqz v1, :cond_2

    .line 3153
    const/4 v1, 0x3

    iget-object v2, p0, Lnet;->c:Lnyp;

    .line 3154
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3156
    :cond_2
    iget-object v1, p0, Lnet;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3157
    iput v0, p0, Lnet;->ai:I

    .line 3158
    return v0
.end method

.method public a(Loxn;)Lnet;
    .locals 2

    .prologue
    .line 3166
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3167
    sparse-switch v0, :sswitch_data_0

    .line 3171
    iget-object v1, p0, Lnet;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3172
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnet;->ah:Ljava/util/List;

    .line 3175
    :cond_1
    iget-object v1, p0, Lnet;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3177
    :sswitch_0
    return-object p0

    .line 3182
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet;->a:Ljava/lang/String;

    goto :goto_0

    .line 3186
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet;->b:Ljava/lang/String;

    goto :goto_0

    .line 3190
    :sswitch_3
    iget-object v0, p0, Lnet;->c:Lnyp;

    if-nez v0, :cond_2

    .line 3191
    new-instance v0, Lnyp;

    invoke-direct {v0}, Lnyp;-><init>()V

    iput-object v0, p0, Lnet;->c:Lnyp;

    .line 3193
    :cond_2
    iget-object v0, p0, Lnet;->c:Lnyp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3167
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3128
    iget-object v0, p0, Lnet;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3129
    const/4 v0, 0x1

    iget-object v1, p0, Lnet;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3131
    :cond_0
    iget-object v0, p0, Lnet;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3132
    const/4 v0, 0x2

    iget-object v1, p0, Lnet;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3134
    :cond_1
    iget-object v0, p0, Lnet;->c:Lnyp;

    if-eqz v0, :cond_2

    .line 3135
    const/4 v0, 0x3

    iget-object v1, p0, Lnet;->c:Lnyp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3137
    :cond_2
    iget-object v0, p0, Lnet;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3139
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3112
    invoke-virtual {p0, p1}, Lnet;->a(Loxn;)Lnet;

    move-result-object v0

    return-object v0
.end method
