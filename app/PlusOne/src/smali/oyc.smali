.class public final Loyc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Loyc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Float;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lpdi;

.field private j:Ljava/lang/String;

.field private k:[Loya;

.field private l:Loya;

.field private m:[Loya;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Lpcj;

.field private u:Loya;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/Integer;

.field private y:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x279c1c0

    new-instance v1, Loyd;

    invoke-direct {v1}, Loyd;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Loyc;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Loyc;->i:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyc;->k:[Loya;

    .line 35
    iput-object v1, p0, Loyc;->l:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyc;->m:[Loya;

    .line 53
    iput-object v1, p0, Loyc;->t:Lpcj;

    .line 56
    iput-object v1, p0, Loyc;->u:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 159
    .line 160
    iget-object v0, p0, Loyc;->d:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 161
    const/4 v0, 0x1

    iget-object v2, p0, Loyc;->d:Ljava/lang/String;

    .line 162
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 164
    :goto_0
    iget-object v2, p0, Loyc;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 165
    const/4 v2, 0x2

    iget-object v3, p0, Loyc;->e:Ljava/lang/String;

    .line 166
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 168
    :cond_0
    iget-object v2, p0, Loyc;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 169
    const/4 v2, 0x3

    iget-object v3, p0, Loyc;->f:Ljava/lang/String;

    .line 170
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 172
    :cond_1
    iget-object v2, p0, Loyc;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 173
    const/4 v2, 0x4

    iget-object v3, p0, Loyc;->g:Ljava/lang/String;

    .line 174
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 176
    :cond_2
    iget-object v2, p0, Loyc;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 177
    const/4 v2, 0x5

    iget-object v3, p0, Loyc;->h:Ljava/lang/String;

    .line 178
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 180
    :cond_3
    iget-object v2, p0, Loyc;->i:Lpdi;

    if-eqz v2, :cond_4

    .line 181
    const/4 v2, 0x6

    iget-object v3, p0, Loyc;->i:Lpdi;

    .line 182
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 184
    :cond_4
    iget-object v2, p0, Loyc;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 185
    const/4 v2, 0x7

    iget-object v3, p0, Loyc;->j:Ljava/lang/String;

    .line 186
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 188
    :cond_5
    iget-object v2, p0, Loyc;->k:[Loya;

    if-eqz v2, :cond_7

    .line 189
    iget-object v3, p0, Loyc;->k:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 190
    if-eqz v5, :cond_6

    .line 191
    const/16 v6, 0x8

    .line 192
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 189
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 196
    :cond_7
    iget-object v2, p0, Loyc;->l:Loya;

    if-eqz v2, :cond_8

    .line 197
    const/16 v2, 0x9

    iget-object v3, p0, Loyc;->l:Loya;

    .line 198
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 200
    :cond_8
    iget-object v2, p0, Loyc;->m:[Loya;

    if-eqz v2, :cond_a

    .line 201
    iget-object v2, p0, Loyc;->m:[Loya;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 202
    if-eqz v4, :cond_9

    .line 203
    const/16 v5, 0xb

    .line 204
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 201
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 208
    :cond_a
    iget-object v1, p0, Loyc;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 209
    const/16 v1, 0xc

    iget-object v2, p0, Loyc;->n:Ljava/lang/String;

    .line 210
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_b
    iget-object v1, p0, Loyc;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 213
    const/16 v1, 0x4b

    iget-object v2, p0, Loyc;->o:Ljava/lang/String;

    .line 214
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_c
    iget-object v1, p0, Loyc;->p:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 217
    const/16 v1, 0x5b

    iget-object v2, p0, Loyc;->p:Ljava/lang/String;

    .line 218
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_d
    iget-object v1, p0, Loyc;->q:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 221
    const/16 v1, 0x5c

    iget-object v2, p0, Loyc;->q:Ljava/lang/String;

    .line 222
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_e
    iget-object v1, p0, Loyc;->r:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 225
    const/16 v1, 0x5d

    iget-object v2, p0, Loyc;->r:Ljava/lang/String;

    .line 226
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_f
    iget-object v1, p0, Loyc;->s:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 229
    const/16 v1, 0x5e

    iget-object v2, p0, Loyc;->s:Ljava/lang/String;

    .line 230
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_10
    iget-object v1, p0, Loyc;->t:Lpcj;

    if-eqz v1, :cond_11

    .line 233
    const/16 v1, 0x5f

    iget-object v2, p0, Loyc;->t:Lpcj;

    .line 234
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_11
    iget-object v1, p0, Loyc;->u:Loya;

    if-eqz v1, :cond_12

    .line 237
    const/16 v1, 0xb9

    iget-object v2, p0, Loyc;->u:Loya;

    .line 238
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_12
    iget-object v1, p0, Loyc;->v:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 241
    const/16 v1, 0xe1

    iget-object v2, p0, Loyc;->v:Ljava/lang/String;

    .line 242
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 244
    :cond_13
    iget-object v1, p0, Loyc;->w:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 245
    const/16 v1, 0xe2

    iget-object v2, p0, Loyc;->w:Ljava/lang/String;

    .line 246
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_14
    iget-object v1, p0, Loyc;->x:Ljava/lang/Integer;

    if-eqz v1, :cond_15

    .line 249
    const/16 v1, 0xea

    iget-object v2, p0, Loyc;->x:Ljava/lang/Integer;

    .line 250
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_15
    iget-object v1, p0, Loyc;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_16

    .line 253
    const/16 v1, 0xeb

    iget-object v2, p0, Loyc;->b:Ljava/lang/Integer;

    .line 254
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 256
    :cond_16
    iget-object v1, p0, Loyc;->c:Ljava/lang/Float;

    if-eqz v1, :cond_17

    .line 257
    const/16 v1, 0xec

    iget-object v2, p0, Loyc;->c:Ljava/lang/Float;

    .line 258
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 260
    :cond_17
    iget-object v1, p0, Loyc;->y:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 261
    const/16 v1, 0xfe

    iget-object v2, p0, Loyc;->y:Ljava/lang/String;

    .line 262
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_18
    iget-object v1, p0, Loyc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    iput v0, p0, Loyc;->ai:I

    .line 266
    return v0

    :cond_19
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loyc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 274
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 275
    sparse-switch v0, :sswitch_data_0

    .line 279
    iget-object v2, p0, Loyc;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 280
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loyc;->ah:Ljava/util/List;

    .line 283
    :cond_1
    iget-object v2, p0, Loyc;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    :sswitch_0
    return-object p0

    .line 290
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->d:Ljava/lang/String;

    goto :goto_0

    .line 294
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->e:Ljava/lang/String;

    goto :goto_0

    .line 298
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->f:Ljava/lang/String;

    goto :goto_0

    .line 302
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->g:Ljava/lang/String;

    goto :goto_0

    .line 306
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->h:Ljava/lang/String;

    goto :goto_0

    .line 310
    :sswitch_6
    iget-object v0, p0, Loyc;->i:Lpdi;

    if-nez v0, :cond_2

    .line 311
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Loyc;->i:Lpdi;

    .line 313
    :cond_2
    iget-object v0, p0, Loyc;->i:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 317
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->j:Ljava/lang/String;

    goto :goto_0

    .line 321
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 322
    iget-object v0, p0, Loyc;->k:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 323
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 324
    iget-object v3, p0, Loyc;->k:[Loya;

    if-eqz v3, :cond_3

    .line 325
    iget-object v3, p0, Loyc;->k:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 327
    :cond_3
    iput-object v2, p0, Loyc;->k:[Loya;

    .line 328
    :goto_2
    iget-object v2, p0, Loyc;->k:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 329
    iget-object v2, p0, Loyc;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 330
    iget-object v2, p0, Loyc;->k:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 331
    invoke-virtual {p1}, Loxn;->a()I

    .line 328
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 322
    :cond_4
    iget-object v0, p0, Loyc;->k:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 334
    :cond_5
    iget-object v2, p0, Loyc;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 335
    iget-object v2, p0, Loyc;->k:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 339
    :sswitch_9
    iget-object v0, p0, Loyc;->l:Loya;

    if-nez v0, :cond_6

    .line 340
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyc;->l:Loya;

    .line 342
    :cond_6
    iget-object v0, p0, Loyc;->l:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 346
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 347
    iget-object v0, p0, Loyc;->m:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 348
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 349
    iget-object v3, p0, Loyc;->m:[Loya;

    if-eqz v3, :cond_7

    .line 350
    iget-object v3, p0, Loyc;->m:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 352
    :cond_7
    iput-object v2, p0, Loyc;->m:[Loya;

    .line 353
    :goto_4
    iget-object v2, p0, Loyc;->m:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 354
    iget-object v2, p0, Loyc;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 355
    iget-object v2, p0, Loyc;->m:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 356
    invoke-virtual {p1}, Loxn;->a()I

    .line 353
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 347
    :cond_8
    iget-object v0, p0, Loyc;->m:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 359
    :cond_9
    iget-object v2, p0, Loyc;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 360
    iget-object v2, p0, Loyc;->m:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 364
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 368
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 372
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 376
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 380
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 384
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 388
    :sswitch_11
    iget-object v0, p0, Loyc;->t:Lpcj;

    if-nez v0, :cond_a

    .line 389
    new-instance v0, Lpcj;

    invoke-direct {v0}, Lpcj;-><init>()V

    iput-object v0, p0, Loyc;->t:Lpcj;

    .line 391
    :cond_a
    iget-object v0, p0, Loyc;->t:Lpcj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 395
    :sswitch_12
    iget-object v0, p0, Loyc;->u:Loya;

    if-nez v0, :cond_b

    .line 396
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyc;->u:Loya;

    .line 398
    :cond_b
    iget-object v0, p0, Loyc;->u:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 402
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->v:Ljava/lang/String;

    goto/16 :goto_0

    .line 406
    :sswitch_14
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 410
    :sswitch_15
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loyc;->x:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 414
    :sswitch_16
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loyc;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 418
    :sswitch_17
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loyc;->c:Ljava/lang/Float;

    goto/16 :goto_0

    .line 422
    :sswitch_18
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyc;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 275
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x25a -> :sswitch_c
        0x2da -> :sswitch_d
        0x2e2 -> :sswitch_e
        0x2ea -> :sswitch_f
        0x2f2 -> :sswitch_10
        0x2fa -> :sswitch_11
        0x5ca -> :sswitch_12
        0x70a -> :sswitch_13
        0x712 -> :sswitch_14
        0x750 -> :sswitch_15
        0x758 -> :sswitch_16
        0x765 -> :sswitch_17
        0x7f2 -> :sswitch_18
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Loyc;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 74
    const/4 v1, 0x1

    iget-object v2, p0, Loyc;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 76
    :cond_0
    iget-object v1, p0, Loyc;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 77
    const/4 v1, 0x2

    iget-object v2, p0, Loyc;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 79
    :cond_1
    iget-object v1, p0, Loyc;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 80
    const/4 v1, 0x3

    iget-object v2, p0, Loyc;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 82
    :cond_2
    iget-object v1, p0, Loyc;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 83
    const/4 v1, 0x4

    iget-object v2, p0, Loyc;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 85
    :cond_3
    iget-object v1, p0, Loyc;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 86
    const/4 v1, 0x5

    iget-object v2, p0, Loyc;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 88
    :cond_4
    iget-object v1, p0, Loyc;->i:Lpdi;

    if-eqz v1, :cond_5

    .line 89
    const/4 v1, 0x6

    iget-object v2, p0, Loyc;->i:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 91
    :cond_5
    iget-object v1, p0, Loyc;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 92
    const/4 v1, 0x7

    iget-object v2, p0, Loyc;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 94
    :cond_6
    iget-object v1, p0, Loyc;->k:[Loya;

    if-eqz v1, :cond_8

    .line 95
    iget-object v2, p0, Loyc;->k:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 96
    if-eqz v4, :cond_7

    .line 97
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 95
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 101
    :cond_8
    iget-object v1, p0, Loyc;->l:Loya;

    if-eqz v1, :cond_9

    .line 102
    const/16 v1, 0x9

    iget-object v2, p0, Loyc;->l:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 104
    :cond_9
    iget-object v1, p0, Loyc;->m:[Loya;

    if-eqz v1, :cond_b

    .line 105
    iget-object v1, p0, Loyc;->m:[Loya;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 106
    if-eqz v3, :cond_a

    .line 107
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 105
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 111
    :cond_b
    iget-object v0, p0, Loyc;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 112
    const/16 v0, 0xc

    iget-object v1, p0, Loyc;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 114
    :cond_c
    iget-object v0, p0, Loyc;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 115
    const/16 v0, 0x4b

    iget-object v1, p0, Loyc;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 117
    :cond_d
    iget-object v0, p0, Loyc;->p:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 118
    const/16 v0, 0x5b

    iget-object v1, p0, Loyc;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 120
    :cond_e
    iget-object v0, p0, Loyc;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 121
    const/16 v0, 0x5c

    iget-object v1, p0, Loyc;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 123
    :cond_f
    iget-object v0, p0, Loyc;->r:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 124
    const/16 v0, 0x5d

    iget-object v1, p0, Loyc;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 126
    :cond_10
    iget-object v0, p0, Loyc;->s:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 127
    const/16 v0, 0x5e

    iget-object v1, p0, Loyc;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 129
    :cond_11
    iget-object v0, p0, Loyc;->t:Lpcj;

    if-eqz v0, :cond_12

    .line 130
    const/16 v0, 0x5f

    iget-object v1, p0, Loyc;->t:Lpcj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 132
    :cond_12
    iget-object v0, p0, Loyc;->u:Loya;

    if-eqz v0, :cond_13

    .line 133
    const/16 v0, 0xb9

    iget-object v1, p0, Loyc;->u:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 135
    :cond_13
    iget-object v0, p0, Loyc;->v:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 136
    const/16 v0, 0xe1

    iget-object v1, p0, Loyc;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 138
    :cond_14
    iget-object v0, p0, Loyc;->w:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 139
    const/16 v0, 0xe2

    iget-object v1, p0, Loyc;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 141
    :cond_15
    iget-object v0, p0, Loyc;->x:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    .line 142
    const/16 v0, 0xea

    iget-object v1, p0, Loyc;->x:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 144
    :cond_16
    iget-object v0, p0, Loyc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_17

    .line 145
    const/16 v0, 0xeb

    iget-object v1, p0, Loyc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 147
    :cond_17
    iget-object v0, p0, Loyc;->c:Ljava/lang/Float;

    if-eqz v0, :cond_18

    .line 148
    const/16 v0, 0xec

    iget-object v1, p0, Loyc;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 150
    :cond_18
    iget-object v0, p0, Loyc;->y:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 151
    const/16 v0, 0xfe

    iget-object v1, p0, Loyc;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 153
    :cond_19
    iget-object v0, p0, Loyc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 155
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loyc;->a(Loxn;)Loyc;

    move-result-object v0

    return-object v0
.end method
