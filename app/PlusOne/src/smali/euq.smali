.class public final Leuq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljmb;


# static fields
.field public static final a:Lief;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 23
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_onb_sul"

    const-string v2, "false"

    const-string v3, "b1568032"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Leuq;->a:Lief;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 47
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 48
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "seen_onboarding_sul"

    const/4 v2, 0x1

    .line 49
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 50
    invoke-interface {v0}, Lhek;->c()I

    .line 51
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;IZ)I
    .locals 2

    .prologue
    .line 55
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x3

    .line 62
    :goto_0
    return v0

    .line 58
    :cond_0
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 59
    sget-object v1, Leuq;->a:Lief;

    invoke-interface {v0, v1, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    .line 61
    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 62
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "seen_onboarding_sul"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public a()Lu;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Leup;

    invoke-direct {v0}, Leup;-><init>()V

    return-object v0
.end method
