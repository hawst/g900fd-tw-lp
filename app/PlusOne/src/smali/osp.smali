.class public final Losp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Losp;


# instance fields
.field private b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x0

    new-array v0, v0, [Losp;

    sput-object v0, Losp;->a:[Losp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 373
    invoke-direct {p0}, Loxq;-><init>()V

    .line 389
    iput v0, p0, Losp;->b:I

    .line 392
    iput v0, p0, Losp;->c:I

    .line 373
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 409
    const/4 v0, 0x0

    .line 410
    iget v1, p0, Losp;->b:I

    if-eq v1, v2, :cond_0

    .line 411
    const/4 v0, 0x1

    iget v1, p0, Losp;->b:I

    .line 412
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 414
    :cond_0
    iget v1, p0, Losp;->c:I

    if-eq v1, v2, :cond_1

    .line 415
    const/4 v1, 0x2

    iget v2, p0, Losp;->c:I

    .line 416
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 418
    :cond_1
    iget-object v1, p0, Losp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 419
    iput v0, p0, Losp;->ai:I

    .line 420
    return v0
.end method

.method public a(Loxn;)Losp;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 428
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 429
    sparse-switch v0, :sswitch_data_0

    .line 433
    iget-object v1, p0, Losp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 434
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losp;->ah:Ljava/util/List;

    .line 437
    :cond_1
    iget-object v1, p0, Losp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    :sswitch_0
    return-object p0

    .line 444
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 445
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 449
    :cond_2
    iput v0, p0, Losp;->b:I

    goto :goto_0

    .line 451
    :cond_3
    iput v2, p0, Losp;->b:I

    goto :goto_0

    .line 456
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 457
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_5

    .line 460
    :cond_4
    iput v0, p0, Losp;->c:I

    goto :goto_0

    .line 462
    :cond_5
    iput v2, p0, Losp;->c:I

    goto :goto_0

    .line 429
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 397
    iget v0, p0, Losp;->b:I

    if-eq v0, v2, :cond_0

    .line 398
    const/4 v0, 0x1

    iget v1, p0, Losp;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 400
    :cond_0
    iget v0, p0, Losp;->c:I

    if-eq v0, v2, :cond_1

    .line 401
    const/4 v0, 0x2

    iget v1, p0, Losp;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 403
    :cond_1
    iget-object v0, p0, Losp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 405
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0, p1}, Losp;->a(Loxn;)Losp;

    move-result-object v0

    return-object v0
.end method
