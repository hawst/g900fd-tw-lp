.class public final Lmei;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmls;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31993
    invoke-direct {p0}, Loxq;-><init>()V

    .line 31996
    iput-object v0, p0, Lmei;->apiHeader:Llyq;

    .line 31999
    iput-object v0, p0, Lmei;->a:Lmls;

    .line 31993
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32016
    const/4 v0, 0x0

    .line 32017
    iget-object v1, p0, Lmei;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 32018
    const/4 v0, 0x1

    iget-object v1, p0, Lmei;->apiHeader:Llyq;

    .line 32019
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32021
    :cond_0
    iget-object v1, p0, Lmei;->a:Lmls;

    if-eqz v1, :cond_1

    .line 32022
    const/4 v1, 0x2

    iget-object v2, p0, Lmei;->a:Lmls;

    .line 32023
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32025
    :cond_1
    iget-object v1, p0, Lmei;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32026
    iput v0, p0, Lmei;->ai:I

    .line 32027
    return v0
.end method

.method public a(Loxn;)Lmei;
    .locals 2

    .prologue
    .line 32035
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32036
    sparse-switch v0, :sswitch_data_0

    .line 32040
    iget-object v1, p0, Lmei;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32041
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmei;->ah:Ljava/util/List;

    .line 32044
    :cond_1
    iget-object v1, p0, Lmei;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32046
    :sswitch_0
    return-object p0

    .line 32051
    :sswitch_1
    iget-object v0, p0, Lmei;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 32052
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmei;->apiHeader:Llyq;

    .line 32054
    :cond_2
    iget-object v0, p0, Lmei;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32058
    :sswitch_2
    iget-object v0, p0, Lmei;->a:Lmls;

    if-nez v0, :cond_3

    .line 32059
    new-instance v0, Lmls;

    invoke-direct {v0}, Lmls;-><init>()V

    iput-object v0, p0, Lmei;->a:Lmls;

    .line 32061
    :cond_3
    iget-object v0, p0, Lmei;->a:Lmls;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32036
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32004
    iget-object v0, p0, Lmei;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 32005
    const/4 v0, 0x1

    iget-object v1, p0, Lmei;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32007
    :cond_0
    iget-object v0, p0, Lmei;->a:Lmls;

    if-eqz v0, :cond_1

    .line 32008
    const/4 v0, 0x2

    iget-object v1, p0, Lmei;->a:Lmls;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32010
    :cond_1
    iget-object v0, p0, Lmei;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32012
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 31989
    invoke-virtual {p0, p1}, Lmei;->a(Loxn;)Lmei;

    move-result-object v0

    return-object v0
.end method
