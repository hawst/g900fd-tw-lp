.class public final Lmry;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lmrw;

.field public e:Ljava/lang/Integer;

.field private f:[Lpxo;

.field private g:[Lpxo;

.field private h:Lprw;

.field private i:Lprv;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/Boolean;

.field private m:[I

.field private n:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25
    sget-object v0, Lpxo;->a:[Lpxo;

    iput-object v0, p0, Lmry;->f:[Lpxo;

    .line 28
    sget-object v0, Lpxo;->a:[Lpxo;

    iput-object v0, p0, Lmry;->g:[Lpxo;

    .line 31
    iput-object v1, p0, Lmry;->h:Lprw;

    .line 34
    iput-object v1, p0, Lmry;->i:Lprv;

    .line 47
    iput-object v1, p0, Lmry;->d:Lmrw;

    .line 50
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmry;->m:[I

    .line 16
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 117
    .line 118
    iget-object v0, p0, Lmry;->a:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 119
    const/4 v0, 0x1

    iget-object v2, p0, Lmry;->a:Ljava/lang/String;

    .line 120
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 122
    :goto_0
    iget-object v2, p0, Lmry;->f:[Lpxo;

    if-eqz v2, :cond_1

    .line 123
    iget-object v3, p0, Lmry;->f:[Lpxo;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 124
    if-eqz v5, :cond_0

    .line 125
    const/4 v6, 0x2

    .line 126
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 123
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 130
    :cond_1
    iget-object v2, p0, Lmry;->g:[Lpxo;

    if-eqz v2, :cond_3

    .line 131
    iget-object v3, p0, Lmry;->g:[Lpxo;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 132
    if-eqz v5, :cond_2

    .line 133
    const/4 v6, 0x3

    .line 134
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 131
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 138
    :cond_3
    iget-object v2, p0, Lmry;->h:Lprw;

    if-eqz v2, :cond_4

    .line 139
    const/4 v2, 0x4

    iget-object v3, p0, Lmry;->h:Lprw;

    .line 140
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 142
    :cond_4
    iget-object v2, p0, Lmry;->i:Lprv;

    if-eqz v2, :cond_5

    .line 143
    const/4 v2, 0x5

    iget-object v3, p0, Lmry;->i:Lprv;

    .line 144
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 146
    :cond_5
    iget-object v2, p0, Lmry;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 147
    const/4 v2, 0x6

    iget-object v3, p0, Lmry;->b:Ljava/lang/String;

    .line 148
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 150
    :cond_6
    iget-object v2, p0, Lmry;->j:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 151
    const/4 v2, 0x7

    iget-object v3, p0, Lmry;->j:Ljava/lang/String;

    .line 152
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 154
    :cond_7
    iget-object v2, p0, Lmry;->k:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 155
    const/16 v2, 0x8

    iget-object v3, p0, Lmry;->k:Ljava/lang/String;

    .line 156
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 158
    :cond_8
    iget-object v2, p0, Lmry;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 159
    const/16 v2, 0x9

    iget-object v3, p0, Lmry;->c:Ljava/lang/String;

    .line 160
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 162
    :cond_9
    iget-object v2, p0, Lmry;->l:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    .line 163
    const/16 v2, 0xa

    iget-object v3, p0, Lmry;->l:Ljava/lang/Boolean;

    .line 164
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 166
    :cond_a
    iget-object v2, p0, Lmry;->d:Lmrw;

    if-eqz v2, :cond_b

    .line 167
    const/16 v2, 0xb

    iget-object v3, p0, Lmry;->d:Lmrw;

    .line 168
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 170
    :cond_b
    iget-object v2, p0, Lmry;->m:[I

    if-eqz v2, :cond_d

    iget-object v2, p0, Lmry;->m:[I

    array-length v2, v2

    if-lez v2, :cond_d

    .line 172
    iget-object v3, p0, Lmry;->m:[I

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_c

    aget v5, v3, v1

    .line 174
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 172
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 176
    :cond_c
    add-int/2addr v0, v2

    .line 177
    iget-object v1, p0, Lmry;->m:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 179
    :cond_d
    iget-object v1, p0, Lmry;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 180
    const/16 v1, 0xd

    iget-object v2, p0, Lmry;->e:Ljava/lang/Integer;

    .line 181
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_e
    iget-object v1, p0, Lmry;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 184
    const/16 v1, 0xe

    iget-object v2, p0, Lmry;->n:Ljava/lang/Boolean;

    .line 185
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 187
    :cond_f
    iget-object v1, p0, Lmry;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    iput v0, p0, Lmry;->ai:I

    .line 189
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmry;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 197
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 198
    sparse-switch v0, :sswitch_data_0

    .line 202
    iget-object v2, p0, Lmry;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 203
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmry;->ah:Ljava/util/List;

    .line 206
    :cond_1
    iget-object v2, p0, Lmry;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    :sswitch_0
    return-object p0

    .line 213
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmry;->a:Ljava/lang/String;

    goto :goto_0

    .line 217
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 218
    iget-object v0, p0, Lmry;->f:[Lpxo;

    if-nez v0, :cond_3

    move v0, v1

    .line 219
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpxo;

    .line 220
    iget-object v3, p0, Lmry;->f:[Lpxo;

    if-eqz v3, :cond_2

    .line 221
    iget-object v3, p0, Lmry;->f:[Lpxo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 223
    :cond_2
    iput-object v2, p0, Lmry;->f:[Lpxo;

    .line 224
    :goto_2
    iget-object v2, p0, Lmry;->f:[Lpxo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 225
    iget-object v2, p0, Lmry;->f:[Lpxo;

    new-instance v3, Lpxo;

    invoke-direct {v3}, Lpxo;-><init>()V

    aput-object v3, v2, v0

    .line 226
    iget-object v2, p0, Lmry;->f:[Lpxo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 227
    invoke-virtual {p1}, Loxn;->a()I

    .line 224
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 218
    :cond_3
    iget-object v0, p0, Lmry;->f:[Lpxo;

    array-length v0, v0

    goto :goto_1

    .line 230
    :cond_4
    iget-object v2, p0, Lmry;->f:[Lpxo;

    new-instance v3, Lpxo;

    invoke-direct {v3}, Lpxo;-><init>()V

    aput-object v3, v2, v0

    .line 231
    iget-object v2, p0, Lmry;->f:[Lpxo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 235
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 236
    iget-object v0, p0, Lmry;->g:[Lpxo;

    if-nez v0, :cond_6

    move v0, v1

    .line 237
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpxo;

    .line 238
    iget-object v3, p0, Lmry;->g:[Lpxo;

    if-eqz v3, :cond_5

    .line 239
    iget-object v3, p0, Lmry;->g:[Lpxo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    :cond_5
    iput-object v2, p0, Lmry;->g:[Lpxo;

    .line 242
    :goto_4
    iget-object v2, p0, Lmry;->g:[Lpxo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 243
    iget-object v2, p0, Lmry;->g:[Lpxo;

    new-instance v3, Lpxo;

    invoke-direct {v3}, Lpxo;-><init>()V

    aput-object v3, v2, v0

    .line 244
    iget-object v2, p0, Lmry;->g:[Lpxo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 245
    invoke-virtual {p1}, Loxn;->a()I

    .line 242
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 236
    :cond_6
    iget-object v0, p0, Lmry;->g:[Lpxo;

    array-length v0, v0

    goto :goto_3

    .line 248
    :cond_7
    iget-object v2, p0, Lmry;->g:[Lpxo;

    new-instance v3, Lpxo;

    invoke-direct {v3}, Lpxo;-><init>()V

    aput-object v3, v2, v0

    .line 249
    iget-object v2, p0, Lmry;->g:[Lpxo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 253
    :sswitch_4
    iget-object v0, p0, Lmry;->h:Lprw;

    if-nez v0, :cond_8

    .line 254
    new-instance v0, Lprw;

    invoke-direct {v0}, Lprw;-><init>()V

    iput-object v0, p0, Lmry;->h:Lprw;

    .line 256
    :cond_8
    iget-object v0, p0, Lmry;->h:Lprw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 260
    :sswitch_5
    iget-object v0, p0, Lmry;->i:Lprv;

    if-nez v0, :cond_9

    .line 261
    new-instance v0, Lprv;

    invoke-direct {v0}, Lprv;-><init>()V

    iput-object v0, p0, Lmry;->i:Lprv;

    .line 263
    :cond_9
    iget-object v0, p0, Lmry;->i:Lprv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 267
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmry;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 271
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmry;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 275
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmry;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 279
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmry;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 283
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmry;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 287
    :sswitch_b
    iget-object v0, p0, Lmry;->d:Lmrw;

    if-nez v0, :cond_a

    .line 288
    new-instance v0, Lmrw;

    invoke-direct {v0}, Lmrw;-><init>()V

    iput-object v0, p0, Lmry;->d:Lmrw;

    .line 290
    :cond_a
    iget-object v0, p0, Lmry;->d:Lmrw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 294
    :sswitch_c
    const/16 v0, 0x60

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 295
    iget-object v0, p0, Lmry;->m:[I

    array-length v0, v0

    .line 296
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 297
    iget-object v3, p0, Lmry;->m:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 298
    iput-object v2, p0, Lmry;->m:[I

    .line 299
    :goto_5
    iget-object v2, p0, Lmry;->m:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 300
    iget-object v2, p0, Lmry;->m:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 301
    invoke-virtual {p1}, Loxn;->a()I

    .line 299
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 304
    :cond_b
    iget-object v2, p0, Lmry;->m:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 308
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmry;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 312
    :sswitch_e
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmry;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 198
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 59
    iget-object v1, p0, Lmry;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60
    const/4 v1, 0x1

    iget-object v2, p0, Lmry;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_0
    iget-object v1, p0, Lmry;->f:[Lpxo;

    if-eqz v1, :cond_2

    .line 63
    iget-object v2, p0, Lmry;->f:[Lpxo;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 64
    if-eqz v4, :cond_1

    .line 65
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 63
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    :cond_2
    iget-object v1, p0, Lmry;->g:[Lpxo;

    if-eqz v1, :cond_4

    .line 70
    iget-object v2, p0, Lmry;->g:[Lpxo;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 71
    if-eqz v4, :cond_3

    .line 72
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 70
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 76
    :cond_4
    iget-object v1, p0, Lmry;->h:Lprw;

    if-eqz v1, :cond_5

    .line 77
    const/4 v1, 0x4

    iget-object v2, p0, Lmry;->h:Lprw;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 79
    :cond_5
    iget-object v1, p0, Lmry;->i:Lprv;

    if-eqz v1, :cond_6

    .line 80
    const/4 v1, 0x5

    iget-object v2, p0, Lmry;->i:Lprv;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 82
    :cond_6
    iget-object v1, p0, Lmry;->b:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 83
    const/4 v1, 0x6

    iget-object v2, p0, Lmry;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 85
    :cond_7
    iget-object v1, p0, Lmry;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 86
    const/4 v1, 0x7

    iget-object v2, p0, Lmry;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 88
    :cond_8
    iget-object v1, p0, Lmry;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 89
    const/16 v1, 0x8

    iget-object v2, p0, Lmry;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 91
    :cond_9
    iget-object v1, p0, Lmry;->c:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 92
    const/16 v1, 0x9

    iget-object v2, p0, Lmry;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 94
    :cond_a
    iget-object v1, p0, Lmry;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 95
    const/16 v1, 0xa

    iget-object v2, p0, Lmry;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 97
    :cond_b
    iget-object v1, p0, Lmry;->d:Lmrw;

    if-eqz v1, :cond_c

    .line 98
    const/16 v1, 0xb

    iget-object v2, p0, Lmry;->d:Lmrw;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 100
    :cond_c
    iget-object v1, p0, Lmry;->m:[I

    if-eqz v1, :cond_d

    iget-object v1, p0, Lmry;->m:[I

    array-length v1, v1

    if-lez v1, :cond_d

    .line 101
    iget-object v1, p0, Lmry;->m:[I

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_d

    aget v3, v1, v0

    .line 102
    const/16 v4, 0xc

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 105
    :cond_d
    iget-object v0, p0, Lmry;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 106
    const/16 v0, 0xd

    iget-object v1, p0, Lmry;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 108
    :cond_e
    iget-object v0, p0, Lmry;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 109
    const/16 v0, 0xe

    iget-object v1, p0, Lmry;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 111
    :cond_f
    iget-object v0, p0, Lmry;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 113
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lmry;->a(Loxn;)Lmry;

    move-result-object v0

    return-object v0
.end method
