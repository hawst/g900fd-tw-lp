.class public final Lnvp;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnvo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 906
    invoke-direct {p0}, Loxq;-><init>()V

    .line 909
    sget-object v0, Lnvo;->a:[Lnvo;

    iput-object v0, p0, Lnvp;->a:[Lnvo;

    .line 906
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 927
    .line 928
    iget-object v1, p0, Lnvp;->a:[Lnvo;

    if-eqz v1, :cond_1

    .line 929
    iget-object v2, p0, Lnvp;->a:[Lnvo;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 930
    if-eqz v4, :cond_0

    .line 931
    const/4 v5, 0x1

    .line 932
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 929
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 936
    :cond_1
    iget-object v1, p0, Lnvp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 937
    iput v0, p0, Lnvp;->ai:I

    .line 938
    return v0
.end method

.method public a(Loxn;)Lnvp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 946
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 947
    sparse-switch v0, :sswitch_data_0

    .line 951
    iget-object v2, p0, Lnvp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 952
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnvp;->ah:Ljava/util/List;

    .line 955
    :cond_1
    iget-object v2, p0, Lnvp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 957
    :sswitch_0
    return-object p0

    .line 962
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 963
    iget-object v0, p0, Lnvp;->a:[Lnvo;

    if-nez v0, :cond_3

    move v0, v1

    .line 964
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnvo;

    .line 965
    iget-object v3, p0, Lnvp;->a:[Lnvo;

    if-eqz v3, :cond_2

    .line 966
    iget-object v3, p0, Lnvp;->a:[Lnvo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 968
    :cond_2
    iput-object v2, p0, Lnvp;->a:[Lnvo;

    .line 969
    :goto_2
    iget-object v2, p0, Lnvp;->a:[Lnvo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 970
    iget-object v2, p0, Lnvp;->a:[Lnvo;

    new-instance v3, Lnvo;

    invoke-direct {v3}, Lnvo;-><init>()V

    aput-object v3, v2, v0

    .line 971
    iget-object v2, p0, Lnvp;->a:[Lnvo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 972
    invoke-virtual {p1}, Loxn;->a()I

    .line 969
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 963
    :cond_3
    iget-object v0, p0, Lnvp;->a:[Lnvo;

    array-length v0, v0

    goto :goto_1

    .line 975
    :cond_4
    iget-object v2, p0, Lnvp;->a:[Lnvo;

    new-instance v3, Lnvo;

    invoke-direct {v3}, Lnvo;-><init>()V

    aput-object v3, v2, v0

    .line 976
    iget-object v2, p0, Lnvp;->a:[Lnvo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 947
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 914
    iget-object v0, p0, Lnvp;->a:[Lnvo;

    if-eqz v0, :cond_1

    .line 915
    iget-object v1, p0, Lnvp;->a:[Lnvo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 916
    if-eqz v3, :cond_0

    .line 917
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 915
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 921
    :cond_1
    iget-object v0, p0, Lnvp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 923
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 902
    invoke-virtual {p0, p1}, Lnvp;->a(Loxn;)Lnvp;

    move-result-object v0

    return-object v0
.end method
