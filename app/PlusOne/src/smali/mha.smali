.class public final Lmha;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lngn;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33127
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33130
    iput-object v0, p0, Lmha;->apiHeader:Llyq;

    .line 33133
    iput-object v0, p0, Lmha;->a:Lngn;

    .line 33127
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33150
    const/4 v0, 0x0

    .line 33151
    iget-object v1, p0, Lmha;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 33152
    const/4 v0, 0x1

    iget-object v1, p0, Lmha;->apiHeader:Llyq;

    .line 33153
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33155
    :cond_0
    iget-object v1, p0, Lmha;->a:Lngn;

    if-eqz v1, :cond_1

    .line 33156
    const/4 v1, 0x2

    iget-object v2, p0, Lmha;->a:Lngn;

    .line 33157
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33159
    :cond_1
    iget-object v1, p0, Lmha;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33160
    iput v0, p0, Lmha;->ai:I

    .line 33161
    return v0
.end method

.method public a(Loxn;)Lmha;
    .locals 2

    .prologue
    .line 33169
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33170
    sparse-switch v0, :sswitch_data_0

    .line 33174
    iget-object v1, p0, Lmha;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33175
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmha;->ah:Ljava/util/List;

    .line 33178
    :cond_1
    iget-object v1, p0, Lmha;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33180
    :sswitch_0
    return-object p0

    .line 33185
    :sswitch_1
    iget-object v0, p0, Lmha;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 33186
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmha;->apiHeader:Llyq;

    .line 33188
    :cond_2
    iget-object v0, p0, Lmha;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33192
    :sswitch_2
    iget-object v0, p0, Lmha;->a:Lngn;

    if-nez v0, :cond_3

    .line 33193
    new-instance v0, Lngn;

    invoke-direct {v0}, Lngn;-><init>()V

    iput-object v0, p0, Lmha;->a:Lngn;

    .line 33195
    :cond_3
    iget-object v0, p0, Lmha;->a:Lngn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33170
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33138
    iget-object v0, p0, Lmha;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 33139
    const/4 v0, 0x1

    iget-object v1, p0, Lmha;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33141
    :cond_0
    iget-object v0, p0, Lmha;->a:Lngn;

    if-eqz v0, :cond_1

    .line 33142
    const/4 v0, 0x2

    iget-object v1, p0, Lmha;->a:Lngn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33144
    :cond_1
    iget-object v0, p0, Lmha;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33146
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33123
    invoke-virtual {p0, p1}, Lmha;->a(Loxn;)Lmha;

    move-result-object v0

    return-object v0
.end method
