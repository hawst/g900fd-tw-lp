.class public final Lmwp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lmwq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11680
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11687
    const/4 v0, 0x0

    iput-object v0, p0, Lmwp;->c:Lmwq;

    .line 11680
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11707
    const/4 v0, 0x0

    .line 11708
    iget-object v1, p0, Lmwp;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11709
    const/4 v0, 0x1

    iget-object v1, p0, Lmwp;->a:Ljava/lang/String;

    .line 11710
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11712
    :cond_0
    iget-object v1, p0, Lmwp;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 11713
    const/4 v1, 0x2

    iget-object v2, p0, Lmwp;->b:Ljava/lang/String;

    .line 11714
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11716
    :cond_1
    iget-object v1, p0, Lmwp;->c:Lmwq;

    if-eqz v1, :cond_2

    .line 11717
    const/4 v1, 0x3

    iget-object v2, p0, Lmwp;->c:Lmwq;

    .line 11718
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11720
    :cond_2
    iget-object v1, p0, Lmwp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11721
    iput v0, p0, Lmwp;->ai:I

    .line 11722
    return v0
.end method

.method public a(Loxn;)Lmwp;
    .locals 2

    .prologue
    .line 11730
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11731
    sparse-switch v0, :sswitch_data_0

    .line 11735
    iget-object v1, p0, Lmwp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11736
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwp;->ah:Ljava/util/List;

    .line 11739
    :cond_1
    iget-object v1, p0, Lmwp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11741
    :sswitch_0
    return-object p0

    .line 11746
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwp;->a:Ljava/lang/String;

    goto :goto_0

    .line 11750
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwp;->b:Ljava/lang/String;

    goto :goto_0

    .line 11754
    :sswitch_3
    iget-object v0, p0, Lmwp;->c:Lmwq;

    if-nez v0, :cond_2

    .line 11755
    new-instance v0, Lmwq;

    invoke-direct {v0}, Lmwq;-><init>()V

    iput-object v0, p0, Lmwp;->c:Lmwq;

    .line 11757
    :cond_2
    iget-object v0, p0, Lmwp;->c:Lmwq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11731
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11692
    iget-object v0, p0, Lmwp;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 11693
    const/4 v0, 0x1

    iget-object v1, p0, Lmwp;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 11695
    :cond_0
    iget-object v0, p0, Lmwp;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 11696
    const/4 v0, 0x2

    iget-object v1, p0, Lmwp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 11698
    :cond_1
    iget-object v0, p0, Lmwp;->c:Lmwq;

    if-eqz v0, :cond_2

    .line 11699
    const/4 v0, 0x3

    iget-object v1, p0, Lmwp;->c:Lmwq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11701
    :cond_2
    iget-object v0, p0, Lmwp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11703
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11676
    invoke-virtual {p0, p1}, Lmwp;->a(Loxn;)Lmwp;

    move-result-object v0

    return-object v0
.end method
