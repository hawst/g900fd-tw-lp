.class final Leen;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;


# instance fields
.field private synthetic a:Leef;


# direct methods
.method constructor <init>(Leef;)V
    .locals 0

    .prologue
    .line 1023
    iput-object p1, p0, Leen;->a:Leef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 1027
    const-string v0, "CreateShareByLinkTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1048
    :goto_0
    return-void

    .line 1031
    :cond_0
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1032
    const-string v0, "HostedMiniShareFragment"

    const-string v1, "Error getting sharing link"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1036
    :cond_1
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "link"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1038
    iget-object v1, p0, Leen;->a:Leef;

    invoke-static {v1}, Leef;->r(Leef;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1040
    iget-object v1, p0, Leen;->a:Leef;

    invoke-static {v1}, Leef;->r(Leef;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1043
    :cond_2
    iget-object v1, p0, Leen;->a:Leef;

    invoke-static {v1}, Leef;->r(Leef;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1044
    iget-object v1, p0, Leen;->a:Leef;

    invoke-static {v1}, Leef;->r(Leef;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1045
    iget-object v0, p0, Leen;->a:Leef;

    invoke-static {v0}, Leef;->r(Leef;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1047
    iget-object v0, p0, Leen;->a:Leef;

    invoke-static {v0}, Leef;->c(Leef;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Leen;->a:Leef;

    invoke-static {v1}, Leef;->b(Leef;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method
