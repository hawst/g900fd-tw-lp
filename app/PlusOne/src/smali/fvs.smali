.class public abstract Lfvs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field public final b:Ljava/lang/Runnable;

.field private final c:J

.field private final d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lfvs;-><init>(JLandroid/os/Handler;)V

    .line 34
    return-void
.end method

.method public constructor <init>(JLandroid/os/Handler;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lfvt;

    invoke-direct {v0, p0}, Lfvt;-><init>(Lfvs;)V

    iput-object v0, p0, Lfvs;->b:Ljava/lang/Runnable;

    .line 43
    iput-wide p1, p0, Lfvs;->c:J

    .line 44
    iput-object p3, p0, Lfvs;->d:Landroid/os/Handler;

    .line 45
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 100
    iput-wide p1, p0, Lfvs;->a:J

    .line 101
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lfvs;->d:Landroid/os/Handler;

    iget-object v1, p0, Lfvs;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 79
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lfvs;->a:J

    return-wide v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 57
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lfvs;->a:J

    sub-long/2addr v0, v2

    .line 58
    iget-wide v2, p0, Lfvs;->c:J

    sub-long v0, v2, v0

    .line 60
    iget-object v2, p0, Lfvs;->d:Landroid/os/Handler;

    iget-object v3, p0, Lfvs;->b:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 61
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_2

    .line 62
    iget-object v0, p0, Lfvs;->d:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lfvs;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 72
    :goto_1
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lfvs;->d:Landroid/os/Handler;

    iget-object v1, p0, Lfvs;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 70
    :cond_2
    iget-object v2, p0, Lfvs;->d:Landroid/os/Handler;

    iget-object v3, p0, Lfvs;->b:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
