.class public final Lniz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnja;

.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Lniq;

.field private f:[Lniq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2959
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2962
    iput-object v1, p0, Lniz;->a:Lnja;

    .line 2967
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lniz;->c:[Ljava/lang/String;

    .line 2972
    iput-object v1, p0, Lniz;->e:Lniq;

    .line 2975
    sget-object v0, Lniq;->a:[Lniq;

    iput-object v0, p0, Lniz;->f:[Lniq;

    .line 2959
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3010
    .line 3011
    iget-object v0, p0, Lniz;->a:Lnja;

    if-eqz v0, :cond_7

    .line 3012
    const/4 v0, 0x1

    iget-object v2, p0, Lniz;->a:Lnja;

    .line 3013
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3015
    :goto_0
    iget-object v2, p0, Lniz;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3016
    const/4 v2, 0x2

    iget-object v3, p0, Lniz;->b:Ljava/lang/String;

    .line 3017
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3019
    :cond_0
    iget-object v2, p0, Lniz;->c:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lniz;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 3021
    iget-object v4, p0, Lniz;->c:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 3023
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 3021
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3025
    :cond_1
    add-int/2addr v0, v3

    .line 3026
    iget-object v2, p0, Lniz;->c:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3028
    :cond_2
    iget-object v2, p0, Lniz;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 3029
    const/4 v2, 0x4

    iget-object v3, p0, Lniz;->d:Ljava/lang/String;

    .line 3030
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3032
    :cond_3
    iget-object v2, p0, Lniz;->e:Lniq;

    if-eqz v2, :cond_4

    .line 3033
    const/4 v2, 0x5

    iget-object v3, p0, Lniz;->e:Lniq;

    .line 3034
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3036
    :cond_4
    iget-object v2, p0, Lniz;->f:[Lniq;

    if-eqz v2, :cond_6

    .line 3037
    iget-object v2, p0, Lniz;->f:[Lniq;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 3038
    if-eqz v4, :cond_5

    .line 3039
    const/4 v5, 0x6

    .line 3040
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3037
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3044
    :cond_6
    iget-object v1, p0, Lniz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3045
    iput v0, p0, Lniz;->ai:I

    .line 3046
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lniz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3054
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3055
    sparse-switch v0, :sswitch_data_0

    .line 3059
    iget-object v2, p0, Lniz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 3060
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lniz;->ah:Ljava/util/List;

    .line 3063
    :cond_1
    iget-object v2, p0, Lniz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3065
    :sswitch_0
    return-object p0

    .line 3070
    :sswitch_1
    iget-object v0, p0, Lniz;->a:Lnja;

    if-nez v0, :cond_2

    .line 3071
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lniz;->a:Lnja;

    .line 3073
    :cond_2
    iget-object v0, p0, Lniz;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3077
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lniz;->b:Ljava/lang/String;

    goto :goto_0

    .line 3081
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3082
    iget-object v0, p0, Lniz;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 3083
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 3084
    iget-object v3, p0, Lniz;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3085
    iput-object v2, p0, Lniz;->c:[Ljava/lang/String;

    .line 3086
    :goto_1
    iget-object v2, p0, Lniz;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 3087
    iget-object v2, p0, Lniz;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 3088
    invoke-virtual {p1}, Loxn;->a()I

    .line 3086
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3091
    :cond_3
    iget-object v2, p0, Lniz;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 3095
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lniz;->d:Ljava/lang/String;

    goto :goto_0

    .line 3099
    :sswitch_5
    iget-object v0, p0, Lniz;->e:Lniq;

    if-nez v0, :cond_4

    .line 3100
    new-instance v0, Lniq;

    invoke-direct {v0}, Lniq;-><init>()V

    iput-object v0, p0, Lniz;->e:Lniq;

    .line 3102
    :cond_4
    iget-object v0, p0, Lniz;->e:Lniq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3106
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3107
    iget-object v0, p0, Lniz;->f:[Lniq;

    if-nez v0, :cond_6

    move v0, v1

    .line 3108
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lniq;

    .line 3109
    iget-object v3, p0, Lniz;->f:[Lniq;

    if-eqz v3, :cond_5

    .line 3110
    iget-object v3, p0, Lniz;->f:[Lniq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3112
    :cond_5
    iput-object v2, p0, Lniz;->f:[Lniq;

    .line 3113
    :goto_3
    iget-object v2, p0, Lniz;->f:[Lniq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 3114
    iget-object v2, p0, Lniz;->f:[Lniq;

    new-instance v3, Lniq;

    invoke-direct {v3}, Lniq;-><init>()V

    aput-object v3, v2, v0

    .line 3115
    iget-object v2, p0, Lniz;->f:[Lniq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3116
    invoke-virtual {p1}, Loxn;->a()I

    .line 3113
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 3107
    :cond_6
    iget-object v0, p0, Lniz;->f:[Lniq;

    array-length v0, v0

    goto :goto_2

    .line 3119
    :cond_7
    iget-object v2, p0, Lniz;->f:[Lniq;

    new-instance v3, Lniq;

    invoke-direct {v3}, Lniq;-><init>()V

    aput-object v3, v2, v0

    .line 3120
    iget-object v2, p0, Lniz;->f:[Lniq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 3055
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2980
    iget-object v1, p0, Lniz;->a:Lnja;

    if-eqz v1, :cond_0

    .line 2981
    const/4 v1, 0x1

    iget-object v2, p0, Lniz;->a:Lnja;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2983
    :cond_0
    iget-object v1, p0, Lniz;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2984
    const/4 v1, 0x2

    iget-object v2, p0, Lniz;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 2986
    :cond_1
    iget-object v1, p0, Lniz;->c:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2987
    iget-object v2, p0, Lniz;->c:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2988
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 2987
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2991
    :cond_2
    iget-object v1, p0, Lniz;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2992
    const/4 v1, 0x4

    iget-object v2, p0, Lniz;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 2994
    :cond_3
    iget-object v1, p0, Lniz;->e:Lniq;

    if-eqz v1, :cond_4

    .line 2995
    const/4 v1, 0x5

    iget-object v2, p0, Lniz;->e:Lniq;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2997
    :cond_4
    iget-object v1, p0, Lniz;->f:[Lniq;

    if-eqz v1, :cond_6

    .line 2998
    iget-object v1, p0, Lniz;->f:[Lniq;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 2999
    if-eqz v3, :cond_5

    .line 3000
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2998
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3004
    :cond_6
    iget-object v0, p0, Lniz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3006
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2955
    invoke-virtual {p0, p1}, Lniz;->a(Loxn;)Lniz;

    move-result-object v0

    return-object v0
.end method
