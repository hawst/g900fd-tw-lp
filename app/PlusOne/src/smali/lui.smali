.class public final Llui;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llui;


# instance fields
.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:Llur;

.field public f:[Lluj;

.field public g:[Llum;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Boolean;

.field public k:Llux;

.field public l:Llua;

.field private m:Ljava/lang/Long;

.field private n:[Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Llui;

    sput-object v0, Llui;->a:[Llui;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v0, -0x80000000

    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 104
    iput v0, p0, Llui;->c:I

    .line 107
    iput v0, p0, Llui;->d:I

    .line 112
    iput-object v1, p0, Llui;->e:Llur;

    .line 115
    sget-object v0, Lluj;->a:[Lluj;

    iput-object v0, p0, Llui;->f:[Lluj;

    .line 118
    sget-object v0, Llum;->a:[Llum;

    iput-object v0, p0, Llui;->g:[Llum;

    .line 123
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Llui;->n:[Ljava/lang/String;

    .line 132
    iput-object v1, p0, Llui;->k:Llux;

    .line 135
    iput-object v1, p0, Llui;->l:Llua;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/high16 v4, -0x80000000

    const/4 v1, 0x0

    .line 198
    .line 199
    iget-object v0, p0, Llui;->b:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 200
    const/4 v0, 0x1

    iget-object v2, p0, Llui;->b:Ljava/lang/String;

    .line 201
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 203
    :goto_0
    iget v2, p0, Llui;->c:I

    if-eq v2, v4, :cond_0

    .line 204
    const/4 v2, 0x2

    iget v3, p0, Llui;->c:I

    .line 205
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 207
    :cond_0
    iget v2, p0, Llui;->d:I

    if-eq v2, v4, :cond_1

    .line 208
    const/4 v2, 0x3

    iget v3, p0, Llui;->d:I

    .line 209
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 211
    :cond_1
    iget-object v2, p0, Llui;->m:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 212
    const/4 v2, 0x4

    iget-object v3, p0, Llui;->m:Ljava/lang/Long;

    .line 213
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 215
    :cond_2
    iget-object v2, p0, Llui;->e:Llur;

    if-eqz v2, :cond_3

    .line 216
    const/4 v2, 0x5

    iget-object v3, p0, Llui;->e:Llur;

    .line 217
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 219
    :cond_3
    iget-object v2, p0, Llui;->f:[Lluj;

    if-eqz v2, :cond_5

    .line 220
    iget-object v3, p0, Llui;->f:[Lluj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    .line 221
    if-eqz v5, :cond_4

    .line 222
    const/4 v6, 0x6

    .line 223
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 220
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 227
    :cond_5
    iget-object v2, p0, Llui;->g:[Llum;

    if-eqz v2, :cond_7

    .line 228
    iget-object v3, p0, Llui;->g:[Llum;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 229
    if-eqz v5, :cond_6

    .line 230
    const/4 v6, 0x7

    .line 231
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 228
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 235
    :cond_7
    iget-object v2, p0, Llui;->h:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 236
    const/16 v2, 0x8

    iget-object v3, p0, Llui;->h:Ljava/lang/String;

    .line 237
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 239
    :cond_8
    iget-object v2, p0, Llui;->n:[Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Llui;->n:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 241
    iget-object v3, p0, Llui;->n:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_9

    aget-object v5, v3, v1

    .line 243
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 245
    :cond_9
    add-int/2addr v0, v2

    .line 246
    iget-object v1, p0, Llui;->n:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 248
    :cond_a
    iget-object v1, p0, Llui;->i:Ljava/lang/Long;

    if-eqz v1, :cond_b

    .line 249
    const/16 v1, 0xa

    iget-object v2, p0, Llui;->i:Ljava/lang/Long;

    .line 250
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_b
    iget-object v1, p0, Llui;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 253
    const/16 v1, 0xb

    iget-object v2, p0, Llui;->j:Ljava/lang/Boolean;

    .line 254
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 256
    :cond_c
    iget-object v1, p0, Llui;->o:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 257
    const/16 v1, 0xc

    iget-object v2, p0, Llui;->o:Ljava/lang/String;

    .line 258
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_d
    iget-object v1, p0, Llui;->k:Llux;

    if-eqz v1, :cond_e

    .line 261
    const/16 v1, 0xd

    iget-object v2, p0, Llui;->k:Llux;

    .line 262
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_e
    iget-object v1, p0, Llui;->l:Llua;

    if-eqz v1, :cond_f

    .line 265
    const/16 v1, 0xe

    iget-object v2, p0, Llui;->l:Llua;

    .line 266
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_f
    iget-object v1, p0, Llui;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    iput v0, p0, Llui;->ai:I

    .line 270
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Llui;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 278
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 279
    sparse-switch v0, :sswitch_data_0

    .line 283
    iget-object v2, p0, Llui;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 284
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llui;->ah:Ljava/util/List;

    .line 287
    :cond_1
    iget-object v2, p0, Llui;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    :sswitch_0
    return-object p0

    .line 294
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llui;->b:Ljava/lang/String;

    goto :goto_0

    .line 298
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 299
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-ne v0, v7, :cond_3

    .line 304
    :cond_2
    iput v0, p0, Llui;->c:I

    goto :goto_0

    .line 306
    :cond_3
    iput v1, p0, Llui;->c:I

    goto :goto_0

    .line 311
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 312
    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    if-eq v0, v7, :cond_4

    const/4 v2, 0x5

    if-ne v0, v2, :cond_5

    .line 318
    :cond_4
    iput v0, p0, Llui;->d:I

    goto :goto_0

    .line 320
    :cond_5
    iput v1, p0, Llui;->d:I

    goto :goto_0

    .line 325
    :sswitch_4
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llui;->m:Ljava/lang/Long;

    goto :goto_0

    .line 329
    :sswitch_5
    iget-object v0, p0, Llui;->e:Llur;

    if-nez v0, :cond_6

    .line 330
    new-instance v0, Llur;

    invoke-direct {v0}, Llur;-><init>()V

    iput-object v0, p0, Llui;->e:Llur;

    .line 332
    :cond_6
    iget-object v0, p0, Llui;->e:Llur;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 336
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 337
    iget-object v0, p0, Llui;->f:[Lluj;

    if-nez v0, :cond_8

    move v0, v1

    .line 338
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lluj;

    .line 339
    iget-object v3, p0, Llui;->f:[Lluj;

    if-eqz v3, :cond_7

    .line 340
    iget-object v3, p0, Llui;->f:[Lluj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 342
    :cond_7
    iput-object v2, p0, Llui;->f:[Lluj;

    .line 343
    :goto_2
    iget-object v2, p0, Llui;->f:[Lluj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 344
    iget-object v2, p0, Llui;->f:[Lluj;

    new-instance v3, Lluj;

    invoke-direct {v3}, Lluj;-><init>()V

    aput-object v3, v2, v0

    .line 345
    iget-object v2, p0, Llui;->f:[Lluj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 346
    invoke-virtual {p1}, Loxn;->a()I

    .line 343
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 337
    :cond_8
    iget-object v0, p0, Llui;->f:[Lluj;

    array-length v0, v0

    goto :goto_1

    .line 349
    :cond_9
    iget-object v2, p0, Llui;->f:[Lluj;

    new-instance v3, Lluj;

    invoke-direct {v3}, Lluj;-><init>()V

    aput-object v3, v2, v0

    .line 350
    iget-object v2, p0, Llui;->f:[Lluj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 354
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 355
    iget-object v0, p0, Llui;->g:[Llum;

    if-nez v0, :cond_b

    move v0, v1

    .line 356
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Llum;

    .line 357
    iget-object v3, p0, Llui;->g:[Llum;

    if-eqz v3, :cond_a

    .line 358
    iget-object v3, p0, Llui;->g:[Llum;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 360
    :cond_a
    iput-object v2, p0, Llui;->g:[Llum;

    .line 361
    :goto_4
    iget-object v2, p0, Llui;->g:[Llum;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 362
    iget-object v2, p0, Llui;->g:[Llum;

    new-instance v3, Llum;

    invoke-direct {v3}, Llum;-><init>()V

    aput-object v3, v2, v0

    .line 363
    iget-object v2, p0, Llui;->g:[Llum;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 364
    invoke-virtual {p1}, Loxn;->a()I

    .line 361
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 355
    :cond_b
    iget-object v0, p0, Llui;->g:[Llum;

    array-length v0, v0

    goto :goto_3

    .line 367
    :cond_c
    iget-object v2, p0, Llui;->g:[Llum;

    new-instance v3, Llum;

    invoke-direct {v3}, Llum;-><init>()V

    aput-object v3, v2, v0

    .line 368
    iget-object v2, p0, Llui;->g:[Llum;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 372
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llui;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 376
    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 377
    iget-object v0, p0, Llui;->n:[Ljava/lang/String;

    array-length v0, v0

    .line 378
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 379
    iget-object v3, p0, Llui;->n:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 380
    iput-object v2, p0, Llui;->n:[Ljava/lang/String;

    .line 381
    :goto_5
    iget-object v2, p0, Llui;->n:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 382
    iget-object v2, p0, Llui;->n:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 383
    invoke-virtual {p1}, Loxn;->a()I

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 386
    :cond_d
    iget-object v2, p0, Llui;->n:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 390
    :sswitch_a
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llui;->i:Ljava/lang/Long;

    goto/16 :goto_0

    .line 394
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llui;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 398
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llui;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 402
    :sswitch_d
    iget-object v0, p0, Llui;->k:Llux;

    if-nez v0, :cond_e

    .line 403
    new-instance v0, Llux;

    invoke-direct {v0}, Llux;-><init>()V

    iput-object v0, p0, Llui;->k:Llux;

    .line 405
    :cond_e
    iget-object v0, p0, Llui;->k:Llux;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 409
    :sswitch_e
    iget-object v0, p0, Llui;->l:Llua;

    if-nez v0, :cond_f

    .line 410
    new-instance v0, Llua;

    invoke-direct {v0}, Llua;-><init>()V

    iput-object v0, p0, Llui;->l:Llua;

    .line 412
    :cond_f
    iget-object v0, p0, Llui;->l:Llua;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 279
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v3, -0x80000000

    const/4 v0, 0x0

    .line 140
    iget-object v1, p0, Llui;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 141
    const/4 v1, 0x1

    iget-object v2, p0, Llui;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 143
    :cond_0
    iget v1, p0, Llui;->c:I

    if-eq v1, v3, :cond_1

    .line 144
    const/4 v1, 0x2

    iget v2, p0, Llui;->c:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 146
    :cond_1
    iget v1, p0, Llui;->d:I

    if-eq v1, v3, :cond_2

    .line 147
    const/4 v1, 0x3

    iget v2, p0, Llui;->d:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 149
    :cond_2
    iget-object v1, p0, Llui;->m:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 150
    const/4 v1, 0x4

    iget-object v2, p0, Llui;->m:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(IJ)V

    .line 152
    :cond_3
    iget-object v1, p0, Llui;->e:Llur;

    if-eqz v1, :cond_4

    .line 153
    const/4 v1, 0x5

    iget-object v2, p0, Llui;->e:Llur;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 155
    :cond_4
    iget-object v1, p0, Llui;->f:[Lluj;

    if-eqz v1, :cond_6

    .line 156
    iget-object v2, p0, Llui;->f:[Lluj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 157
    if-eqz v4, :cond_5

    .line 158
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 156
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    :cond_6
    iget-object v1, p0, Llui;->g:[Llum;

    if-eqz v1, :cond_8

    .line 163
    iget-object v2, p0, Llui;->g:[Llum;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 164
    if-eqz v4, :cond_7

    .line 165
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 163
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 169
    :cond_8
    iget-object v1, p0, Llui;->h:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 170
    const/16 v1, 0x8

    iget-object v2, p0, Llui;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 172
    :cond_9
    iget-object v1, p0, Llui;->n:[Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 173
    iget-object v1, p0, Llui;->n:[Ljava/lang/String;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 174
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 177
    :cond_a
    iget-object v0, p0, Llui;->i:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 178
    const/16 v0, 0xa

    iget-object v1, p0, Llui;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 180
    :cond_b
    iget-object v0, p0, Llui;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 181
    const/16 v0, 0xb

    iget-object v1, p0, Llui;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 183
    :cond_c
    iget-object v0, p0, Llui;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 184
    const/16 v0, 0xc

    iget-object v1, p0, Llui;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 186
    :cond_d
    iget-object v0, p0, Llui;->k:Llux;

    if-eqz v0, :cond_e

    .line 187
    const/16 v0, 0xd

    iget-object v1, p0, Llui;->k:Llux;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 189
    :cond_e
    iget-object v0, p0, Llui;->l:Llua;

    if-eqz v0, :cond_f

    .line 190
    const/16 v0, 0xe

    iget-object v1, p0, Llui;->l:Llua;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 192
    :cond_f
    iget-object v0, p0, Llui;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 194
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llui;->a(Loxn;)Llui;

    move-result-object v0

    return-object v0
.end method
