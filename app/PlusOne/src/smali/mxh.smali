.class public final Lmxh;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmxh;


# instance fields
.field public b:I

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lmxh;

    sput-object v0, Lmxh;->a:[Lmxh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    const/high16 v0, -0x80000000

    iput v0, p0, Lmxh;->b:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    iget v1, p0, Lmxh;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 44
    const/4 v0, 0x1

    iget v1, p0, Lmxh;->b:I

    .line 45
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 47
    :cond_0
    iget-object v1, p0, Lmxh;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x2

    iget-object v2, p0, Lmxh;->c:Ljava/lang/Boolean;

    .line 49
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 51
    :cond_1
    iget-object v1, p0, Lmxh;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 52
    const/4 v1, 0x3

    iget-object v2, p0, Lmxh;->d:Ljava/lang/Boolean;

    .line 53
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 55
    :cond_2
    iget-object v1, p0, Lmxh;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 56
    const/4 v1, 0x4

    iget-object v2, p0, Lmxh;->e:Ljava/lang/Boolean;

    .line 57
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 59
    :cond_3
    iget-object v1, p0, Lmxh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    iput v0, p0, Lmxh;->ai:I

    .line 61
    return v0
.end method

.method public a(Loxn;)Lmxh;
    .locals 3

    .prologue
    const/16 v2, 0x163

    .line 69
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 70
    sparse-switch v0, :sswitch_data_0

    .line 74
    iget-object v1, p0, Lmxh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 75
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmxh;->ah:Ljava/util/List;

    .line 78
    :cond_1
    iget-object v1, p0, Lmxh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    :sswitch_0
    return-object p0

    .line 85
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 86
    if-eq v0, v2, :cond_2

    const/16 v1, 0x18d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e5

    if-ne v0, v1, :cond_3

    .line 89
    :cond_2
    iput v0, p0, Lmxh;->b:I

    goto :goto_0

    .line 91
    :cond_3
    iput v2, p0, Lmxh;->b:I

    goto :goto_0

    .line 96
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmxh;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 100
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmxh;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 104
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmxh;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 70
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24
    iget v0, p0, Lmxh;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 25
    const/4 v0, 0x1

    iget v1, p0, Lmxh;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 27
    :cond_0
    iget-object v0, p0, Lmxh;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x2

    iget-object v1, p0, Lmxh;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 30
    :cond_1
    iget-object v0, p0, Lmxh;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 31
    const/4 v0, 0x3

    iget-object v1, p0, Lmxh;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 33
    :cond_2
    iget-object v0, p0, Lmxh;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 34
    const/4 v0, 0x4

    iget-object v1, p0, Lmxh;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 36
    :cond_3
    iget-object v0, p0, Lmxh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 38
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmxh;->a(Loxn;)Lmxh;

    move-result-object v0

    return-object v0
.end method
