.class public final Lcve;
.super Landroid/os/Handler;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DataType:",
        "Ljava/lang/Object;",
        "TransformedType:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/Handler;"
    }
.end annotation


# instance fields
.field private a:Lcvd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcvd",
            "<TTransformedType;>;"
        }
    .end annotation
.end field

.field private b:Lcvg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcvg",
            "<TDataType;TTransformedType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lcvg;Lcvd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            "Lcvg",
            "<TDataType;TTransformedType;>;",
            "Lcvd",
            "<TTransformedType;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 27
    iput-object p3, p0, Lcve;->a:Lcvd;

    .line 28
    iput-object p2, p0, Lcve;->b:Lcvg;

    .line 29
    return-void
.end method

.method static synthetic a(Lcve;)Lcvd;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcve;->a:Lcvd;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcve;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcve;->a:Lcvd;

    .line 71
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDataType;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p0}, Lcve;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 36
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 37
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 39
    invoke-virtual {p0, v0}, Lcve;->sendMessage(Landroid/os/Message;)Z

    .line 40
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 44
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 46
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcve;->a:Lcvd;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcve;->b:Lcvg;

    invoke-interface {v1, v0}, Lcvg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Lcvf;

    invoke-direct {v1, p0, v0}, Lcvf;-><init>(Lcve;Ljava/lang/Object;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
