.class public final Lols;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/Integer;

.field private b:[Ljava/lang/Float;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lols;->a:[Ljava/lang/Integer;

    .line 16
    sget-object v0, Loxx;->i:[Ljava/lang/Float;

    iput-object v0, p0, Lols;->b:[Ljava/lang/Float;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 57
    .line 58
    iget-object v1, p0, Lols;->a:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lols;->a:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 60
    iget-object v2, p0, Lols;->a:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 62
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    iget-object v0, p0, Lols;->a:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 67
    :cond_1
    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 68
    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 69
    add-int/2addr v0, v1

    .line 70
    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 72
    :cond_2
    iget-object v1, p0, Lols;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 73
    const/4 v1, 0x3

    iget-object v2, p0, Lols;->d:Ljava/lang/Boolean;

    .line 74
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 76
    :cond_3
    iget-object v1, p0, Lols;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 77
    const/4 v1, 0x4

    iget-object v2, p0, Lols;->e:Ljava/lang/String;

    .line 78
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_4
    iget-object v1, p0, Lols;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 81
    const/4 v1, 0x5

    iget-object v2, p0, Lols;->f:Ljava/lang/String;

    .line 82
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_5
    iget-object v1, p0, Lols;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 85
    const/4 v1, 0x6

    iget-object v2, p0, Lols;->c:Ljava/lang/Boolean;

    .line 86
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 88
    :cond_6
    iget-object v1, p0, Lols;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    iput v0, p0, Lols;->ai:I

    .line 90
    return v0
.end method

.method public a(Loxn;)Lols;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 98
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 99
    sparse-switch v0, :sswitch_data_0

    .line 103
    iget-object v1, p0, Lols;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lols;->ah:Ljava/util/List;

    .line 107
    :cond_1
    iget-object v1, p0, Lols;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    :sswitch_0
    return-object p0

    .line 114
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 115
    iget-object v0, p0, Lols;->a:[Ljava/lang/Integer;

    array-length v0, v0

    .line 116
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 117
    iget-object v2, p0, Lols;->a:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    iput-object v1, p0, Lols;->a:[Ljava/lang/Integer;

    .line 119
    :goto_1
    iget-object v1, p0, Lols;->a:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 120
    iget-object v1, p0, Lols;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 121
    invoke-virtual {p1}, Loxn;->a()I

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 124
    :cond_2
    iget-object v1, p0, Lols;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 128
    :sswitch_2
    const/16 v0, 0x15

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 129
    iget-object v0, p0, Lols;->b:[Ljava/lang/Float;

    array-length v0, v0

    .line 130
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Float;

    .line 131
    iget-object v2, p0, Lols;->b:[Ljava/lang/Float;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    iput-object v1, p0, Lols;->b:[Ljava/lang/Float;

    .line 133
    :goto_2
    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 134
    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    invoke-virtual {p1}, Loxn;->d()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    .line 135
    invoke-virtual {p1}, Loxn;->a()I

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 138
    :cond_3
    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    invoke-virtual {p1}, Loxn;->d()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 142
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lols;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 146
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lols;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 150
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lols;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 154
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lols;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 99
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 29
    iget-object v1, p0, Lols;->a:[Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 30
    iget-object v2, p0, Lols;->a:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 31
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 30
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 34
    :cond_0
    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 35
    iget-object v1, p0, Lols;->b:[Ljava/lang/Float;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 36
    const/4 v4, 0x2

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(IF)V

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 39
    :cond_1
    iget-object v0, p0, Lols;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 40
    const/4 v0, 0x3

    iget-object v1, p0, Lols;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 42
    :cond_2
    iget-object v0, p0, Lols;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 43
    const/4 v0, 0x4

    iget-object v1, p0, Lols;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 45
    :cond_3
    iget-object v0, p0, Lols;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 46
    const/4 v0, 0x5

    iget-object v1, p0, Lols;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 48
    :cond_4
    iget-object v0, p0, Lols;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 49
    const/4 v0, 0x6

    iget-object v1, p0, Lols;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 51
    :cond_5
    iget-object v0, p0, Lols;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 53
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lols;->a(Loxn;)Lols;

    move-result-object v0

    return-object v0
.end method
