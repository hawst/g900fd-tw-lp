.class public final Lcpw;
.super Lhxz;
.source "PG"

# interfaces
.implements Lfdi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lcpu;",
        ">;",
        "Lfdi",
        "<",
        "Lcpu;",
        ">;"
    }
.end annotation


# static fields
.field private static final m:[Ljava/lang/String;

.field private static final n:[Ljava/lang/String;


# instance fields
.field private final b:I

.field private c:Landroid/net/Uri;

.field private d:Ljava/lang/Long;

.field private e:I

.field private final f:I

.field private final g:Lfew;

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private k:Z

.field private l:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "timestamp"

    aput-object v1, v0, v3

    sput-object v0, Lcpw;->m:[Ljava/lang/String;

    .line 55
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcpw;->n:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/Long;III)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lcpw;->l:Landroid/database/ContentObserver;

    .line 71
    iput p2, p0, Lcpw;->b:I

    .line 72
    const-class v0, Lfew;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    iput-object v0, p0, Lcpw;->g:Lfew;

    .line 73
    iput-object p3, p0, Lcpw;->c:Landroid/net/Uri;

    .line 74
    iput-object p4, p0, Lcpw;->d:Ljava/lang/Long;

    .line 75
    iput p5, p0, Lcpw;->e:I

    .line 76
    iput p6, p0, Lcpw;->f:I

    .line 77
    iput p7, p0, Lcpw;->h:I

    .line 78
    invoke-static {p7}, Ljva;->b(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcpw;->i:Ljava/lang/String;

    .line 79
    invoke-static {p7}, Ljva;->a(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcpw;->j:Ljava/lang/String;

    .line 80
    return-void
.end method

.method private l()Ljava/lang/Long;
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 193
    invoke-virtual {p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v0

    .line 194
    iget v1, p0, Lcpw;->b:I

    .line 195
    invoke-static {v0, v1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 197
    iget v1, p0, Lcpw;->h:I

    invoke-static {v1}, Ljva;->c(I)Z

    move-result v8

    .line 198
    const-string v1, "all_photos"

    sget-object v2, Lcpw;->n:[Ljava/lang/String;

    const-string v3, "is_primary = 1 AND (media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND local_content_uri = ?"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    iget-object v9, p0, Lcpw;->i:Ljava/lang/String;

    aput-object v9, v4, v6

    if-eqz v8, :cond_0

    move v6, v7

    .line 200
    :cond_0
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    const/4 v6, 0x2

    iget-object v7, p0, Lcpw;->j:Ljava/lang/String;

    aput-object v7, v4, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lcpw;->c:Landroid/net/Uri;

    .line 201
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const-string v8, "1"

    move-object v6, v5

    move-object v7, v5

    .line 198
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 204
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 208
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 210
    :goto_0
    return-object v5

    .line 208
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 235
    iget v0, p0, Lcpw;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcpw;->g:Lfew;

    iget v1, p0, Lcpw;->b:I

    .line 236
    invoke-virtual {v0, v1}, Lfew;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcpu;I)V
    .locals 2

    .prologue
    .line 321
    invoke-virtual {p1, p2}, Lcpu;->a(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcpw;->d:Ljava/lang/Long;

    .line 322
    const/4 v0, 0x0

    iput-object v0, p0, Lcpw;->c:Landroid/net/Uri;

    .line 323
    return-void
.end method

.method public a(Lizu;)V
    .locals 1

    .prologue
    .line 327
    invoke-virtual {p1}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcpw;->c:Landroid/net/Uri;

    .line 329
    const/4 v0, 0x0

    iput-object v0, p0, Lcpw;->d:Ljava/lang/Long;

    .line 331
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcpu;

    invoke-virtual {p0, p1, p2}, Lcpw;->a(Lcpu;I)V

    return-void
.end method

.method public f()Lcpu;
    .locals 26

    .prologue
    .line 104
    invoke-virtual/range {p0 .. p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->b:I

    invoke-static {v6, v7}, Ljvd;->a(Landroid/content/Context;I)J

    move-result-wide v6

    long-to-int v0, v6

    move/from16 v17, v0

    .line 108
    move-object/from16 v0, p0

    iget v6, v0, Lcpw;->e:I

    .line 109
    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->e:I

    move/from16 v0, v17

    if-le v7, v0, :cond_17

    .line 110
    move-object/from16 v0, p0

    iget v6, v0, Lcpw;->f:I

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->f:I

    div-int v7, v17, v7

    mul-int/2addr v6, v7

    .line 111
    move/from16 v0, v17

    if-ne v6, v0, :cond_17

    .line 112
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcpw;->f:I

    sub-int/2addr v6, v8

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    move v15, v6

    .line 116
    :goto_0
    move-object/from16 v0, p0

    iget v6, v0, Lcpw;->h:I

    and-int/lit8 v6, v6, 0x4

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    :goto_1
    invoke-direct/range {p0 .. p0}, Lcpw;->m()Z

    move-result v7

    if-eqz v7, :cond_6

    if-eqz v6, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->b:I

    invoke-static {v6, v7}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v6

    invoke-virtual {v6}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->h:I

    invoke-static {v7}, Ljva;->c(I)Z

    move-result v11

    const-string v7, "all_photos"

    sget-object v8, Lcpw;->m:[Ljava/lang/String;

    const-string v9, "is_primary = 1 AND (media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND photo_id IS NOT NULL"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcpw;->i:Ljava/lang/String;

    aput-object v13, v10, v12

    const/4 v12, 0x1

    if-eqz v11, :cond_2

    const/4 v11, 0x1

    :goto_2
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    const/4 v11, 0x2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcpw;->j:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v13, "timestamp DESC"

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const-string v7, "AllPhotosListLoader"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->getColumnCount()I

    move-result v8

    invoke-static/range {v18 .. v19}, Llse;->b(J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lcpw;->b:I

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x7a

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Ran all photos query on remote content, got "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, " results with "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " columns duration: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " accountId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    move-object/from16 v16, v6

    .line 117
    :goto_3
    if-nez v16, :cond_d

    .line 118
    const/4 v6, 0x0

    .line 189
    :goto_4
    return-object v6

    .line 116
    :cond_1
    const/4 v6, 0x0

    goto/16 :goto_1

    :cond_2
    const/4 v11, 0x0

    goto :goto_2

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->b:I

    invoke-static {v6, v7}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v6

    invoke-virtual {v6}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->h:I

    invoke-static {v7}, Ljva;->c(I)Z

    move-result v11

    const-string v7, "all_photos"

    sget-object v8, Lcpw;->m:[Ljava/lang/String;

    const-string v9, "is_primary = 1 AND (media_attr & ? != 0 OR ?) AND media_attr & ? == 0"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcpw;->i:Ljava/lang/String;

    aput-object v13, v10, v12

    const/4 v12, 0x1

    if-eqz v11, :cond_5

    const/4 v11, 0x1

    :goto_5
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    const/4 v11, 0x2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcpw;->j:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v13, "timestamp DESC"

    move-object/from16 v0, p0

    iget v14, v0, Lcpw;->f:I

    new-instance v16, Ljava/lang/StringBuilder;

    const/16 v20, 0x18

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v20, ", "

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {v6 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const-string v7, "AllPhotosListLoader"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->getColumnCount()I

    move-result v8

    invoke-static/range {v18 .. v19}, Llse;->a(J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lcpw;->b:I

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x77

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Ran all photos query on all content, got "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, " results with "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " columns duration: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " accountId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_4
    move-object/from16 v16, v6

    goto/16 :goto_3

    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_5

    :cond_6
    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->b:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_9

    move-object/from16 v0, p0

    iget-object v7, v0, Lcpw;->g:Lfew;

    move-object/from16 v0, p0

    iget v8, v0, Lcpw;->b:I

    invoke-virtual {v7, v8}, Lfew;->c(I)Z

    move-result v7

    if-eqz v7, :cond_9

    const/4 v7, 0x1

    :goto_6
    if-eqz v7, :cond_b

    if-nez v6, :cond_b

    const-string v6, "AllPhotosListLoader"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "Fingerprints not yet generated or remote photos not yet synced, returning media store only accountId: "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->b:I

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0xb

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->b:I

    invoke-static {v6, v7}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v6

    invoke-virtual {v6}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->h:I

    invoke-static {v7}, Ljva;->c(I)Z

    move-result v11

    const-string v7, "all_photos"

    sget-object v8, Lcpw;->m:[Ljava/lang/String;

    const-string v9, "is_primary = 1 AND (media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND local_content_uri IS NOT NULL"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcpw;->i:Ljava/lang/String;

    aput-object v13, v10, v12

    const/4 v12, 0x1

    if-eqz v11, :cond_a

    const/4 v11, 0x1

    :goto_7
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    const/4 v11, 0x2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcpw;->j:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v13, "timestamp DESC"

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const-string v7, "AllPhotosListLoader"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->getColumnCount()I

    move-result v8

    invoke-static/range {v18 .. v19}, Llse;->b(J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lcpw;->b:I

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x79

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Ran all photos query on local content, got "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, " results with "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " columns duration: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " accountId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_8
    move-object/from16 v16, v6

    goto/16 :goto_3

    :cond_9
    const/4 v7, 0x0

    goto/16 :goto_6

    :cond_a
    const/4 v11, 0x0

    goto :goto_7

    :cond_b
    const-string v6, "AllPhotosListLoader"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_c

    const-string v6, "Fingerprints not generated or remote photos not yet synced and local content not yet copied, returning null accountId: "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->b:I

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0xb

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v6, 0x0

    move-object/from16 v16, v6

    goto/16 :goto_3

    .line 121
    :cond_d
    const/4 v14, 0x0

    .line 122
    const/4 v9, 0x0

    .line 123
    const/4 v10, 0x0

    .line 130
    move-object/from16 v0, p0

    iget v6, v0, Lcpw;->e:I

    if-eq v15, v6, :cond_e

    .line 131
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 134
    move-object/from16 v0, p0

    iput v15, v0, Lcpw;->e:I

    .line 137
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcpw;->m()Z

    move-result v6

    if-eqz v6, :cond_10

    move-object/from16 v0, p0

    iget v6, v0, Lcpw;->f:I

    add-int/2addr v6, v15

    move/from16 v0, v17

    if-lt v6, v0, :cond_f

    .line 139
    invoke-virtual/range {p0 .. p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcpw;->b:I

    invoke-static {v6, v7}, Ldtd;->c(Landroid/content/Context;I)Z

    move-result v6

    if-nez v6, :cond_10

    .line 140
    :cond_f
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 144
    :cond_10
    move-object/from16 v0, p0

    iget-object v6, v0, Lcpw;->c:Landroid/net/Uri;

    if-eqz v6, :cond_14

    .line 145
    invoke-direct/range {p0 .. p0}, Lcpw;->l()Ljava/lang/Long;

    move-result-object v6

    .line 150
    :goto_8
    const-string v7, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 151
    const-string v7, "timestamp"

    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 152
    new-instance v11, Landroid/util/SparseArray;

    invoke-direct {v11}, Landroid/util/SparseArray;-><init>()V

    .line 153
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v13

    .line 154
    const/4 v12, 0x0

    .line 156
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v7

    new-array v7, v7, [Lcpv;

    .line 157
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v18

    .line 158
    const/4 v8, 0x0

    move/from16 v25, v8

    move-object v8, v14

    move-object v14, v13

    move-object v13, v12

    move/from16 v12, v25

    :goto_9
    array-length v0, v7

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_15

    .line 159
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    .line 162
    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 163
    invoke-interface/range {v16 .. v17}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 164
    new-instance v24, Lcpv;

    move-object/from16 v0, v24

    move-wide/from16 v1, v20

    move-wide/from16 v3, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcpv;-><init>(JJ)V

    .line 165
    aput-object v24, v7, v12

    .line 168
    move-wide/from16 v0, v22

    invoke-virtual {v14, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 169
    if-eqz v13, :cond_11

    invoke-virtual {v13, v14}, Lfce;->a(Ljava/util/Calendar;)Z

    move-result v22

    if-nez v22, :cond_12

    .line 170
    :cond_11
    new-instance v13, Lfce;

    invoke-virtual/range {p0 .. p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v13, v0, v14}, Lfce;-><init>(Landroid/content/Context;Ljava/util/Calendar;)V

    .line 171
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 172
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v14

    .line 177
    :cond_12
    if-eqz v6, :cond_13

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    cmp-long v20, v20, v22

    if-nez v20, :cond_13

    .line 178
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 158
    :cond_13
    add-int/lit8 v12, v12, 0x1

    goto :goto_9

    .line 147
    :cond_14
    move-object/from16 v0, p0

    iget-object v6, v0, Lcpw;->d:Ljava/lang/Long;

    goto/16 :goto_8

    .line 182
    :cond_15
    const-string v6, "AllPhotosListLoader"

    const/4 v12, 0x3

    invoke-static {v6, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 183
    invoke-static/range {v18 .. v19}, Llse;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    array-length v12, v7

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, 0x2e

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v14, "Cursor iteration took: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v13, " for "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v12, " items."

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    :cond_16
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 189
    new-instance v6, Lcpu;

    invoke-direct/range {v6 .. v11}, Lcpu;-><init>([Lcpv;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Landroid/util/SparseArray;)V

    goto/16 :goto_4

    :cond_17
    move v15, v6

    goto/16 :goto_0
.end method

.method protected g()V
    .locals 4

    .prologue
    .line 84
    invoke-super {p0}, Lhxz;->g()V

    .line 85
    iget-boolean v0, p0, Lcpw;->k:Z

    if-nez v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 87
    iget v1, p0, Lcpw;->b:I

    invoke-static {v1}, Ljvd;->a(I)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcpw;->l:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcpw;->k:Z

    .line 91
    :cond_0
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcpw;->f()Lcpu;

    move-result-object v0

    return-object v0
.end method

.method protected w()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Lhxz;->w()V

    .line 96
    iget-boolean v0, p0, Lcpw;->k:Z

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcpw;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcpw;->l:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcpw;->k:Z

    .line 100
    :cond_0
    return-void
.end method
