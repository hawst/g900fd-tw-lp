.class public final Lequ;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;
.implements Ldxq;
.implements Leqt;
.implements Lfwd;
.implements Llgs;
.implements Llnx;
.implements Llrg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Lhrh;",
        ">;",
        "Ldxq;",
        "Leqt;",
        "Lfwd;",
        "Llgs;",
        "Llnx;",
        "Llrg;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Runnable;

.field private b:Landroid/content/Context;

.field private c:Lhee;

.field private d:I

.field private final e:Lu;

.field private f:Z

.field private g:I

.field private h:Lhei;

.field private i:Leqz;

.field private j:Lhrf;

.field private k:Lhrh;


# direct methods
.method public constructor <init>(Lu;Llqr;Leqz;I)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Leqv;

    invoke-direct {v0, p0}, Leqv;-><init>(Lequ;)V

    iput-object v0, p0, Lequ;->a:Ljava/lang/Runnable;

    .line 96
    iput-object p1, p0, Lequ;->e:Lu;

    .line 97
    iput p4, p0, Lequ;->g:I

    .line 98
    iput-object p3, p0, Lequ;->i:Leqz;

    .line 99
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 100
    return-void
.end method

.method static synthetic a(Lequ;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lequ;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lhmv;)V
    .locals 4

    .prologue
    .line 401
    iget v1, p0, Lequ;->d:I

    .line 402
    iget-object v0, p0, Lequ;->b:Landroid/content/Context;

    const-class v2, Lhms;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lequ;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 404
    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 402
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 406
    return-void
.end method

.method static synthetic b(Lequ;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lequ;->d:I

    return v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lhrh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353
    new-instance v0, Lhrf;

    iget-object v1, p0, Lequ;->b:Landroid/content/Context;

    iget v2, p0, Lequ;->d:I

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lhrf;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    return-object v0
.end method

.method public a(Z)Lequ;
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lequ;->f:Z

    .line 104
    return-object p0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lequ;->j:Lhrf;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lequ;->j:Lhrf;

    invoke-virtual {v0}, Lhrf;->t()V

    .line 145
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 198
    packed-switch p1, :pswitch_data_0

    .line 217
    :goto_0
    return-void

    .line 200
    :pswitch_0
    iget-boolean v0, p0, Lequ;->f:Z

    if-eqz v0, :cond_2

    .line 201
    sget-object v0, Lhmv;->k:Lhmv;

    invoke-direct {p0, v0}, Lequ;->a(Lhmv;)V

    invoke-static {}, Lhqd;->a()Z

    move-result v0

    iget v1, p0, Lequ;->d:I

    if-nez v0, :cond_0

    iget-object v2, p0, Lequ;->e:Lu;

    invoke-virtual {v2}, Lu;->p()Lae;

    move-result-object v2

    const-string v3, "dialog_sync_disabled"

    invoke-virtual {v2, v3}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lequ;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lequ;->h:Lhei;

    invoke-interface {v3, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v3, "account_name"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0a0440

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-nez v0, :cond_1

    const v0, 0x7f0a06c2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const v1, 0x7f0a06c0

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0a06c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a07f8

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v3, v2}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0, p0}, Llgr;->a(Llgs;)V

    iget-object v1, p0, Lequ;->e:Lu;

    invoke-virtual {v1}, Lu;->p()Lae;

    move-result-object v1

    const-string v2, "dialog_sync_disabled"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Leqx;

    invoke-direct {v0, p0}, Leqx;-><init>(Lequ;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leqx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_1
    const v0, 0x7f0a06c3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v2, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 203
    :cond_2
    invoke-virtual {p0, v6}, Lequ;->c(Z)V

    goto/16 :goto_0

    .line 208
    :pswitch_1
    iget v0, p0, Lequ;->d:I

    .line 209
    iget-object v1, p0, Lequ;->b:Landroid/content/Context;

    iget-object v2, p0, Lequ;->b:Landroid/content/Context;

    .line 210
    invoke-static {v2, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsLauncherActivity;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 209
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 214
    :pswitch_2
    iget-object v0, p0, Lequ;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Leqy;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 198
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 394
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 398
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/google/android/apps/plus/views/AutoBackupBarView;)V
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x4

    const/4 v7, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 149
    const-class v0, Lfew;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    .line 151
    iget-object v1, p0, Lequ;->k:Lhrh;

    if-nez v1, :cond_1

    move v1, v3

    .line 158
    :goto_0
    iget-object v2, p0, Lequ;->k:Lhrh;

    if-eqz v2, :cond_18

    .line 159
    iget-object v2, p0, Lequ;->k:Lhrh;

    iget v2, v2, Lhrh;->g:I

    iget-object v3, p0, Lequ;->k:Lhrh;

    iget v3, v3, Lhrh;->h:I

    add-int v5, v2, v3

    .line 160
    iget-object v2, p0, Lequ;->k:Lhrh;

    iget v4, v2, Lhrh;->j:I

    .line 161
    iget-object v2, p0, Lequ;->k:Lhrh;

    iget v3, v2, Lhrh;->d:I

    .line 162
    iget-object v2, p0, Lequ;->k:Lhrh;

    iget-object v2, v2, Lhrh;->b:Ljava/util/Map;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lequ;->k:Lhrh;

    iget-object v2, v2, Lhrh;->n:Ljava/lang/String;

    if-eqz v2, :cond_17

    .line 163
    iget-object v2, p0, Lequ;->k:Lhrh;

    iget-object v2, v2, Lhrh;->b:Ljava/util/Map;

    iget-object v7, p0, Lequ;->k:Lhrh;

    iget-object v7, v7, Lhrh;->n:Ljava/lang/String;

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    move-object v7, v2

    .line 166
    :goto_1
    if-ne v1, v6, :cond_0

    iget-object v2, p0, Lequ;->k:Lhrh;

    iget-object v2, v2, Lhrh;->n:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 167
    iget-object v2, p0, Lequ;->b:Landroid/content/Context;

    iget-object v6, p0, Lequ;->k:Lhrh;

    iget-object v6, v6, Lhrh;->n:Ljava/lang/String;

    .line 168
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    sget-object v8, Ljac;->a:Ljac;

    invoke-static {v2, v6, v8}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v8

    .line 172
    :cond_0
    :goto_2
    iget-boolean v2, p0, Lequ;->f:Z

    iget v6, p0, Lequ;->d:I

    .line 173
    invoke-virtual {v0, v6}, Lfew;->a(I)Ljava/lang/Long;

    move-result-object v6

    move-object v0, p2

    .line 172
    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a(IZIIILjava/lang/Long;Ljava/lang/Float;Lizu;)V

    .line 174
    invoke-virtual {p2, p0}, Lcom/google/android/apps/plus/views/AutoBackupBarView;->a(Lfwd;)V

    .line 175
    return-void

    .line 151
    :cond_1
    iget-boolean v1, p0, Lequ;->f:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget-boolean v1, v1, Lhrh;->e:Z

    if-eqz v1, :cond_8

    :cond_2
    move v2, v4

    :goto_3
    iget-object v1, p0, Lequ;->k:Lhrh;

    iget-boolean v1, v1, Lhrh;->e:Z

    if-eqz v1, :cond_9

    iget-boolean v1, p0, Lequ;->f:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget-boolean v1, v1, Lhrh;->f:Z

    if-eqz v1, :cond_9

    :cond_3
    move v5, v4

    :goto_4
    iget-object v1, p0, Lequ;->b:Landroid/content/Context;

    const-class v9, Ljgn;

    invoke-static {v1, v9}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljgn;

    invoke-interface {v1}, Ljgn;->a()Z

    move-result v9

    invoke-interface {v1}, Ljgn;->d()Z

    move-result v10

    iget-object v1, p0, Lequ;->b:Landroid/content/Context;

    invoke-static {v1}, Lhre;->a(Landroid/content/Context;)Lhre;

    move-result-object v11

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->h:I

    if-nez v1, :cond_a

    if-nez v9, :cond_a

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->k:I

    if-lez v1, :cond_a

    invoke-interface {v11}, Lhrd;->c()Z

    move-result v1

    if-nez v1, :cond_a

    move v1, v4

    :goto_5
    iget-object v12, p0, Lequ;->k:Lhrh;

    iget v12, v12, Lhrh;->h:I

    if-nez v12, :cond_b

    if-nez v9, :cond_b

    iget-object v9, p0, Lequ;->k:Lhrh;

    iget v9, v9, Lhrh;->l:I

    if-lez v9, :cond_b

    invoke-interface {v11}, Lhrd;->b()Z

    move-result v9

    if-nez v9, :cond_b

    move v9, v4

    :goto_6
    if-nez v1, :cond_4

    if-eqz v9, :cond_c

    :cond_4
    move v9, v4

    :goto_7
    if-nez v9, :cond_d

    if-nez v10, :cond_d

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->h:I

    if-nez v1, :cond_d

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->k:I

    if-lez v1, :cond_5

    invoke-interface {v11}, Lhrd;->c()Z

    move-result v1

    if-nez v1, :cond_6

    :cond_5
    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->l:I

    if-lez v1, :cond_d

    invoke-interface {v11}, Lhrd;->b()Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_6
    move v1, v4

    :goto_8
    iget v10, p0, Lequ;->d:I

    if-eq v10, v7, :cond_7

    iget-object v10, p0, Lequ;->i:Leqz;

    invoke-interface {v10}, Leqz;->aa()Z

    move-result v10

    if-nez v10, :cond_e

    :cond_7
    move v1, v3

    goto/16 :goto_0

    :cond_8
    move v2, v3

    goto/16 :goto_3

    :cond_9
    move v5, v3

    goto :goto_4

    :cond_a
    move v1, v3

    goto :goto_5

    :cond_b
    move v9, v3

    goto :goto_6

    :cond_c
    move v9, v3

    goto :goto_7

    :cond_d
    move v1, v3

    goto :goto_8

    :cond_e
    if-eqz v2, :cond_f

    if-nez v5, :cond_f

    move v1, v4

    goto/16 :goto_0

    :cond_f
    if-eqz v5, :cond_10

    iget-object v2, p0, Lequ;->k:Lhrh;

    iget v2, v2, Lhrh;->d:I

    if-eq v2, v7, :cond_10

    iget-object v2, p0, Lequ;->k:Lhrh;

    iget v2, v2, Lhrh;->d:I

    iget v4, p0, Lequ;->d:I

    if-eq v2, v4, :cond_10

    const/4 v1, 0x2

    goto/16 :goto_0

    :cond_10
    if-eqz v5, :cond_11

    if-eqz v9, :cond_11

    const/4 v1, 0x6

    goto/16 :goto_0

    :cond_11
    if-eqz v5, :cond_12

    if-eqz v1, :cond_12

    const/4 v1, 0x5

    goto/16 :goto_0

    :cond_12
    if-eqz v5, :cond_13

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->i:I

    if-lez v1, :cond_13

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->h:I

    if-nez v1, :cond_13

    const/4 v1, 0x3

    goto/16 :goto_0

    :cond_13
    if-eqz v5, :cond_14

    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->h:I

    if-gtz v1, :cond_15

    :cond_14
    iget-object v1, p0, Lequ;->k:Lhrh;

    iget v1, v1, Lhrh;->g:I

    if-lez v1, :cond_16

    :cond_15
    move v1, v6

    goto/16 :goto_0

    :cond_16
    const/4 v1, 0x7

    goto/16 :goto_0

    :cond_17
    move-object v7, v8

    goto/16 :goto_1

    :cond_18
    move v4, v3

    move v5, v3

    move v3, v7

    move-object v7, v8

    goto/16 :goto_2
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 114
    iput-object p1, p0, Lequ;->b:Landroid/content/Context;

    .line 115
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lequ;->c:Lhee;

    .line 116
    iget-object v0, p0, Lequ;->c:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lequ;->d:I

    .line 117
    const-class v0, Lhei;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lequ;->h:Lhei;

    .line 119
    iget-object v0, p0, Lequ;->e:Lu;

    invoke-virtual {v0}, Lu;->p()Lae;

    move-result-object v0

    const-string v1, "dialog_sync_disabled"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Llgr;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Llgr;->a(Llgs;)V

    .line 120
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 379
    const-string v0, "dialog_sync_disabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    invoke-static {}, Lhqd;->b()V

    .line 382
    :cond_0
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lhrh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 375
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 52
    check-cast p2, Lhrh;

    invoke-virtual {p0, p2}, Lequ;->a(Lhrh;)V

    return-void
.end method

.method public a(Lhrh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhrh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 359
    iput-object p1, p0, Lequ;->k:Lhrh;

    .line 360
    const-string v0, "UploadStatusHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 361
    invoke-virtual {p1}, Lhrh;->toString()Ljava/lang/String;

    .line 363
    :cond_0
    iget-object v0, p0, Lequ;->i:Leqz;

    invoke-interface {v0}, Leqz;->ab()V

    .line 365
    iget-object v0, p0, Lequ;->k:Lhrh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lequ;->k:Lhrh;

    iget-boolean v0, v0, Lhrh;->m:Z

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lequ;->a:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 368
    iget-object v0, p0, Lequ;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 371
    :cond_1
    return-void
.end method

.method public a(Lizu;Lcom/google/android/apps/plus/views/PhotoTileView;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 179
    iget-object v0, p0, Lequ;->k:Lhrh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lequ;->k:Lhrh;

    iget-boolean v0, v0, Lhrh;->e:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lequ;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lequ;->k:Lhrh;

    iget-boolean v0, v0, Lhrh;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    .line 181
    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    .line 182
    :cond_1
    sget-object v0, Lhri;->a:Lhri;

    invoke-virtual {p2, v0, v3, v4, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lhri;FJ)V

    .line 194
    :goto_0
    return-void

    .line 185
    :cond_2
    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 186
    iget-object v0, p0, Lequ;->k:Lhrh;

    iget-object v0, v0, Lhrh;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 187
    sget-object v0, Lhri;->a:Lhri;

    invoke-virtual {p2, v0, v3, v4, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lhri;FJ)V

    goto :goto_0

    .line 190
    :cond_3
    iget-object v0, p0, Lequ;->k:Lhrh;

    iget-object v0, v0, Lhrh;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhri;

    .line 191
    iget-object v1, p0, Lequ;->k:Lhrh;

    iget-object v1, v1, Lhrh;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-static {v1, v3}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v3

    .line 192
    iget-object v1, p0, Lequ;->k:Lhrh;

    iget-object v1, v1, Lhrh;->c:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 193
    invoke-virtual {p2, v0, v3, v4, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lhri;FJ)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 386
    return-void
.end method

.method public b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126
    if-eqz p1, :cond_0

    iget-object v0, p0, Lequ;->b:Landroid/content/Context;

    invoke-static {v0}, Lhqd;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lequ;->e:Lu;

    invoke-virtual {v0}, Lu;->w()Lbb;

    move-result-object v0

    iget v1, p0, Lequ;->g:I

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    move-result-object v0

    check-cast v0, Lhrf;

    iput-object v0, p0, Lequ;->j:Lhrf;

    .line 134
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lequ;->e:Lu;

    invoke-virtual {v0}, Lu;->w()Lbb;

    move-result-object v0

    iget v1, p0, Lequ;->g:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 131
    iput-object v2, p0, Lequ;->j:Lhrf;

    .line 132
    iput-object v2, p0, Lequ;->k:Lhrh;

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 390
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 223
    sget-object v0, Lhmv;->eH:Lhmv;

    invoke-direct {p0, v0}, Lequ;->a(Lhmv;)V

    .line 224
    iget-object v0, p0, Lequ;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 225
    new-instance v1, Leqw;

    invoke-direct {v1, p0, v0, p1}, Leqw;-><init>(Lequ;Landroid/content/Context;Z)V

    const/4 v0, 0x0

    .line 236
    invoke-virtual {v1, v0}, Leqw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 237
    return-void
.end method
