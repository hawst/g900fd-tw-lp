.class public final Lnth;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lnrz;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1150
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1155
    sget-object v0, Lnrz;->a:[Lnrz;

    iput-object v0, p0, Lnth;->b:[Lnrz;

    .line 1150
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1191
    .line 1192
    iget-object v0, p0, Lnth;->a:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1193
    const/4 v0, 0x1

    iget-object v2, p0, Lnth;->a:Ljava/lang/String;

    .line 1194
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1196
    :goto_0
    iget-object v2, p0, Lnth;->b:[Lnrz;

    if-eqz v2, :cond_1

    .line 1197
    iget-object v2, p0, Lnth;->b:[Lnrz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1198
    if-eqz v4, :cond_0

    .line 1199
    const/4 v5, 0x2

    .line 1200
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1197
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1204
    :cond_1
    iget-object v1, p0, Lnth;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1205
    const/4 v1, 0x3

    iget-object v2, p0, Lnth;->c:Ljava/lang/Boolean;

    .line 1206
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1208
    :cond_2
    iget-object v1, p0, Lnth;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1209
    const/4 v1, 0x4

    iget-object v2, p0, Lnth;->d:Ljava/lang/Boolean;

    .line 1210
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1212
    :cond_3
    iget-object v1, p0, Lnth;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1213
    const/4 v1, 0x5

    iget-object v2, p0, Lnth;->e:Ljava/lang/String;

    .line 1214
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1216
    :cond_4
    iget-object v1, p0, Lnth;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1217
    iput v0, p0, Lnth;->ai:I

    .line 1218
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnth;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1226
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1227
    sparse-switch v0, :sswitch_data_0

    .line 1231
    iget-object v2, p0, Lnth;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1232
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnth;->ah:Ljava/util/List;

    .line 1235
    :cond_1
    iget-object v2, p0, Lnth;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1237
    :sswitch_0
    return-object p0

    .line 1242
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnth;->a:Ljava/lang/String;

    goto :goto_0

    .line 1246
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1247
    iget-object v0, p0, Lnth;->b:[Lnrz;

    if-nez v0, :cond_3

    move v0, v1

    .line 1248
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnrz;

    .line 1249
    iget-object v3, p0, Lnth;->b:[Lnrz;

    if-eqz v3, :cond_2

    .line 1250
    iget-object v3, p0, Lnth;->b:[Lnrz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1252
    :cond_2
    iput-object v2, p0, Lnth;->b:[Lnrz;

    .line 1253
    :goto_2
    iget-object v2, p0, Lnth;->b:[Lnrz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1254
    iget-object v2, p0, Lnth;->b:[Lnrz;

    new-instance v3, Lnrz;

    invoke-direct {v3}, Lnrz;-><init>()V

    aput-object v3, v2, v0

    .line 1255
    iget-object v2, p0, Lnth;->b:[Lnrz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1256
    invoke-virtual {p1}, Loxn;->a()I

    .line 1253
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1247
    :cond_3
    iget-object v0, p0, Lnth;->b:[Lnrz;

    array-length v0, v0

    goto :goto_1

    .line 1259
    :cond_4
    iget-object v2, p0, Lnth;->b:[Lnrz;

    new-instance v3, Lnrz;

    invoke-direct {v3}, Lnrz;-><init>()V

    aput-object v3, v2, v0

    .line 1260
    iget-object v2, p0, Lnth;->b:[Lnrz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1264
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnth;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1268
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnth;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1272
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnth;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 1227
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1166
    iget-object v0, p0, Lnth;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1167
    const/4 v0, 0x1

    iget-object v1, p0, Lnth;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1169
    :cond_0
    iget-object v0, p0, Lnth;->b:[Lnrz;

    if-eqz v0, :cond_2

    .line 1170
    iget-object v1, p0, Lnth;->b:[Lnrz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1171
    if-eqz v3, :cond_1

    .line 1172
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1170
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1176
    :cond_2
    iget-object v0, p0, Lnth;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1177
    const/4 v0, 0x3

    iget-object v1, p0, Lnth;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1179
    :cond_3
    iget-object v0, p0, Lnth;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1180
    const/4 v0, 0x4

    iget-object v1, p0, Lnth;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1182
    :cond_4
    iget-object v0, p0, Lnth;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1183
    const/4 v0, 0x5

    iget-object v1, p0, Lnth;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1185
    :cond_5
    iget-object v0, p0, Lnth;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1187
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1146
    invoke-virtual {p0, p1}, Lnth;->a(Loxn;)Lnth;

    move-result-object v0

    return-object v0
.end method
