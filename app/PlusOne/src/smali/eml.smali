.class public final Leml;
.super Leku;
.source "PG"


# instance fields
.field private N:Leky;

.field private O:I

.field private final ad:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Leku;-><init>()V

    .line 38
    new-instance v0, Lemm;

    invoke-direct {v0, p0}, Lemm;-><init>(Leml;)V

    iput-object v0, p0, Leml;->ad:Lbc;

    .line 68
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Leku;-><init>()V

    .line 38
    new-instance v0, Lemm;

    invoke-direct {v0, p0}, Lemm;-><init>(Leml;)V

    iput-object v0, p0, Leml;->ad:Lbc;

    .line 71
    iput-boolean p1, p0, Leml;->ac:Z

    .line 72
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lhmw;->r:Lhmw;

    return-object v0
.end method

.method public a()I
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Leml;->O:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 125
    const/16 v0, 0x3f

    .line 129
    :goto_0
    return v0

    .line 126
    :cond_0
    iget v0, p0, Leml;->O:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_1

    .line 127
    const/16 v0, 0x5d

    goto :goto_0

    .line 129
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 87
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 88
    iget-object v1, p0, Leml;->N:Leky;

    invoke-virtual {p0, v1}, Leml;->a(Landroid/widget/ListAdapter;)V

    .line 89
    return-object v0
.end method

.method protected a(II)Ldid;
    .locals 3

    .prologue
    .line 135
    invoke-virtual {p0}, Leml;->a()I

    move-result v0

    .line 136
    invoke-virtual {p0}, Leml;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 135
    invoke-static {v0, v1, v2}, Ldib;->a(ILjava/lang/Integer;Ljava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Leml;->X:Z

    .line 79
    new-instance v0, Leky;

    invoke-direct {v0, p0}, Leky;-><init>(Leku;)V

    iput-object v0, p0, Leml;->N:Leky;

    .line 80
    invoke-virtual {p0}, Leml;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "people_view_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leml;->O:I

    .line 82
    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 102
    const v0, 0x7f0a09b5

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 103
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Leml;->N:Leky;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Leky;->a(Ljava/util/ArrayList;I)V

    .line 95
    iget-object v0, p0, Leml;->N:Leky;

    invoke-virtual {v0}, Leky;->notifyDataSetChanged()V

    .line 96
    invoke-virtual {p0}, Leml;->x()Landroid/view/View;

    invoke-virtual {p0}, Leml;->aa()V

    .line 97
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 141
    const/16 v0, 0x67

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Leml;->R:Z

    .line 113
    invoke-virtual {p0}, Leml;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Leml;->ad:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 114
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Leml;->R:Z

    .line 119
    invoke-virtual {p0}, Leml;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Leml;->ad:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 120
    return-void
.end method
