.class public final Lpky;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Lppf;

.field private d:Ljava/lang/Boolean;

.field private e:Lppf;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    iput-object v0, p0, Lpky;->c:Lppf;

    .line 26
    iput-object v0, p0, Lpky;->e:Lppf;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 58
    iget-object v1, p0, Lpky;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 59
    const/4 v0, 0x1

    iget-object v1, p0, Lpky;->a:Ljava/lang/Boolean;

    .line 60
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 62
    :cond_0
    iget-object v1, p0, Lpky;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 63
    const/4 v1, 0x2

    iget-object v2, p0, Lpky;->b:Ljava/lang/Boolean;

    .line 64
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 66
    :cond_1
    iget-object v1, p0, Lpky;->c:Lppf;

    if-eqz v1, :cond_2

    .line 67
    const/4 v1, 0x3

    iget-object v2, p0, Lpky;->c:Lppf;

    .line 68
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_2
    iget-object v1, p0, Lpky;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 71
    const/4 v1, 0x4

    iget-object v2, p0, Lpky;->d:Ljava/lang/Boolean;

    .line 72
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 74
    :cond_3
    iget-object v1, p0, Lpky;->e:Lppf;

    if-eqz v1, :cond_4

    .line 75
    const/4 v1, 0x5

    iget-object v2, p0, Lpky;->e:Lppf;

    .line 76
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_4
    iget-object v1, p0, Lpky;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 79
    const/4 v1, 0x6

    iget-object v2, p0, Lpky;->f:Ljava/lang/Boolean;

    .line 80
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 82
    :cond_5
    iget-object v1, p0, Lpky;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    iput v0, p0, Lpky;->ai:I

    .line 84
    return v0
.end method

.method public a(Loxn;)Lpky;
    .locals 2

    .prologue
    .line 92
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 93
    sparse-switch v0, :sswitch_data_0

    .line 97
    iget-object v1, p0, Lpky;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpky;->ah:Ljava/util/List;

    .line 101
    :cond_1
    iget-object v1, p0, Lpky;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    :sswitch_0
    return-object p0

    .line 108
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpky;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 112
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpky;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 116
    :sswitch_3
    iget-object v0, p0, Lpky;->c:Lppf;

    if-nez v0, :cond_2

    .line 117
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpky;->c:Lppf;

    .line 119
    :cond_2
    iget-object v0, p0, Lpky;->c:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 123
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpky;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 127
    :sswitch_5
    iget-object v0, p0, Lpky;->e:Lppf;

    if-nez v0, :cond_3

    .line 128
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpky;->e:Lppf;

    .line 130
    :cond_3
    iget-object v0, p0, Lpky;->e:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 134
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpky;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 93
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lpky;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x1

    iget-object v1, p0, Lpky;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 36
    :cond_0
    iget-object v0, p0, Lpky;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 37
    const/4 v0, 0x2

    iget-object v1, p0, Lpky;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 39
    :cond_1
    iget-object v0, p0, Lpky;->c:Lppf;

    if-eqz v0, :cond_2

    .line 40
    const/4 v0, 0x3

    iget-object v1, p0, Lpky;->c:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_2
    iget-object v0, p0, Lpky;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 43
    const/4 v0, 0x4

    iget-object v1, p0, Lpky;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 45
    :cond_3
    iget-object v0, p0, Lpky;->e:Lppf;

    if-eqz v0, :cond_4

    .line 46
    const/4 v0, 0x5

    iget-object v1, p0, Lpky;->e:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 48
    :cond_4
    iget-object v0, p0, Lpky;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 49
    const/4 v0, 0x6

    iget-object v1, p0, Lpky;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 51
    :cond_5
    iget-object v0, p0, Lpky;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 53
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpky;->a(Loxn;)Lpky;

    move-result-object v0

    return-object v0
.end method
