.class public final Lphc;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Lptt;

.field private c:Loxb;

.field private d:Lopk;

.field private e:Lomm;

.field private f:Ljava/lang/Integer;

.field private g:Lpvb;

.field private h:Ljava/lang/Float;

.field private i:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v1, p0, Lphc;->b:Lptt;

    .line 16
    iput-object v1, p0, Lphc;->c:Loxb;

    .line 19
    iput-object v1, p0, Lphc;->d:Lopk;

    .line 22
    iput-object v1, p0, Lphc;->e:Lomm;

    .line 27
    const/high16 v0, -0x80000000

    iput v0, p0, Lphc;->a:I

    .line 30
    iput-object v1, p0, Lphc;->g:Lpvb;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Lphc;->b:Lptt;

    if-eqz v1, :cond_0

    .line 74
    const/4 v0, 0x1

    iget-object v1, p0, Lphc;->b:Lptt;

    .line 75
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 77
    :cond_0
    iget-object v1, p0, Lphc;->c:Loxb;

    if-eqz v1, :cond_1

    .line 78
    const/4 v1, 0x2

    iget-object v2, p0, Lphc;->c:Loxb;

    .line 79
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_1
    iget-object v1, p0, Lphc;->d:Lopk;

    if-eqz v1, :cond_2

    .line 82
    const/4 v1, 0x3

    iget-object v2, p0, Lphc;->d:Lopk;

    .line 83
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_2
    iget-object v1, p0, Lphc;->e:Lomm;

    if-eqz v1, :cond_3

    .line 86
    const/4 v1, 0x4

    iget-object v2, p0, Lphc;->e:Lomm;

    .line 87
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_3
    iget-object v1, p0, Lphc;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 90
    const/4 v1, 0x5

    iget-object v2, p0, Lphc;->f:Ljava/lang/Integer;

    .line 91
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_4
    iget v1, p0, Lphc;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_5

    .line 94
    const/4 v1, 0x6

    iget v2, p0, Lphc;->a:I

    .line 95
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_5
    iget-object v1, p0, Lphc;->g:Lpvb;

    if-eqz v1, :cond_6

    .line 98
    const/4 v1, 0x7

    iget-object v2, p0, Lphc;->g:Lpvb;

    .line 99
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_6
    iget-object v1, p0, Lphc;->h:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 102
    const/16 v1, 0xa

    iget-object v2, p0, Lphc;->h:Ljava/lang/Float;

    .line 103
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 105
    :cond_7
    iget-object v1, p0, Lphc;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 106
    const/16 v1, 0xb

    iget-object v2, p0, Lphc;->i:Ljava/lang/Boolean;

    .line 107
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 109
    :cond_8
    iget-object v1, p0, Lphc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    iput v0, p0, Lphc;->ai:I

    .line 111
    return v0
.end method

.method public a(Loxn;)Lphc;
    .locals 2

    .prologue
    .line 119
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 120
    sparse-switch v0, :sswitch_data_0

    .line 124
    iget-object v1, p0, Lphc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 125
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lphc;->ah:Ljava/util/List;

    .line 128
    :cond_1
    iget-object v1, p0, Lphc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    :sswitch_0
    return-object p0

    .line 135
    :sswitch_1
    iget-object v0, p0, Lphc;->b:Lptt;

    if-nez v0, :cond_2

    .line 136
    new-instance v0, Lptt;

    invoke-direct {v0}, Lptt;-><init>()V

    iput-object v0, p0, Lphc;->b:Lptt;

    .line 138
    :cond_2
    iget-object v0, p0, Lphc;->b:Lptt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 142
    :sswitch_2
    iget-object v0, p0, Lphc;->c:Loxb;

    if-nez v0, :cond_3

    .line 143
    new-instance v0, Loxb;

    invoke-direct {v0}, Loxb;-><init>()V

    iput-object v0, p0, Lphc;->c:Loxb;

    .line 145
    :cond_3
    iget-object v0, p0, Lphc;->c:Loxb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 149
    :sswitch_3
    iget-object v0, p0, Lphc;->d:Lopk;

    if-nez v0, :cond_4

    .line 150
    new-instance v0, Lopk;

    invoke-direct {v0}, Lopk;-><init>()V

    iput-object v0, p0, Lphc;->d:Lopk;

    .line 152
    :cond_4
    iget-object v0, p0, Lphc;->d:Lopk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 156
    :sswitch_4
    iget-object v0, p0, Lphc;->e:Lomm;

    if-nez v0, :cond_5

    .line 157
    new-instance v0, Lomm;

    invoke-direct {v0}, Lomm;-><init>()V

    iput-object v0, p0, Lphc;->e:Lomm;

    .line 159
    :cond_5
    iget-object v0, p0, Lphc;->e:Lomm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 163
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lphc;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 167
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 168
    if-eqz v0, :cond_6

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 171
    :cond_6
    iput v0, p0, Lphc;->a:I

    goto :goto_0

    .line 173
    :cond_7
    const/4 v0, 0x0

    iput v0, p0, Lphc;->a:I

    goto :goto_0

    .line 178
    :sswitch_7
    iget-object v0, p0, Lphc;->g:Lpvb;

    if-nez v0, :cond_8

    .line 179
    new-instance v0, Lpvb;

    invoke-direct {v0}, Lpvb;-><init>()V

    iput-object v0, p0, Lphc;->g:Lpvb;

    .line 181
    :cond_8
    iget-object v0, p0, Lphc;->g:Lpvb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 185
    :sswitch_8
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lphc;->h:Ljava/lang/Float;

    goto/16 :goto_0

    .line 189
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lphc;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x55 -> :sswitch_8
        0x58 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lphc;->b:Lptt;

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x1

    iget-object v1, p0, Lphc;->b:Lptt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_0
    iget-object v0, p0, Lphc;->c:Loxb;

    if-eqz v0, :cond_1

    .line 43
    const/4 v0, 0x2

    iget-object v1, p0, Lphc;->c:Loxb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 45
    :cond_1
    iget-object v0, p0, Lphc;->d:Lopk;

    if-eqz v0, :cond_2

    .line 46
    const/4 v0, 0x3

    iget-object v1, p0, Lphc;->d:Lopk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 48
    :cond_2
    iget-object v0, p0, Lphc;->e:Lomm;

    if-eqz v0, :cond_3

    .line 49
    const/4 v0, 0x4

    iget-object v1, p0, Lphc;->e:Lomm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 51
    :cond_3
    iget-object v0, p0, Lphc;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 52
    const/4 v0, 0x5

    iget-object v1, p0, Lphc;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 54
    :cond_4
    iget v0, p0, Lphc;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_5

    .line 55
    const/4 v0, 0x6

    iget v1, p0, Lphc;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 57
    :cond_5
    iget-object v0, p0, Lphc;->g:Lpvb;

    if-eqz v0, :cond_6

    .line 58
    const/4 v0, 0x7

    iget-object v1, p0, Lphc;->g:Lpvb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 60
    :cond_6
    iget-object v0, p0, Lphc;->h:Ljava/lang/Float;

    if-eqz v0, :cond_7

    .line 61
    const/16 v0, 0xa

    iget-object v1, p0, Lphc;->h:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 63
    :cond_7
    iget-object v0, p0, Lphc;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 64
    const/16 v0, 0xb

    iget-object v1, p0, Lphc;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 66
    :cond_8
    iget-object v0, p0, Lphc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 68
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lphc;->a(Loxn;)Lphc;

    move-result-object v0

    return-object v0
.end method
