.class public final Lflc;
.super Llol;
.source "PG"

# interfaces
.implements Leqi;
.implements Lhob;
.implements Lkgr;


# instance fields
.field private N:Ldvq;

.field private O:Ldvq;

.field private final P:Lhkd;

.field private final Q:Lhke;

.field private R:Lkgp;

.field private final S:Lhoc;

.field private T:Lkhr;

.field private U:Lhee;

.field private V:I

.field private W:Lkhg;

.field private X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private Y:Lfjm;

.field private Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private aa:Lkhl;

.field private ab:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkhl;",
            ">;"
        }
    .end annotation
.end field

.field private ac:Lkhl;

.field private ad:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ldvq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 129
    invoke-direct {p0}, Llol;-><init>()V

    .line 64
    new-instance v0, Lfld;

    invoke-direct {v0, p0}, Lfld;-><init>(Lflc;)V

    iput-object v0, p0, Lflc;->P:Lhkd;

    .line 77
    new-instance v0, Lhke;

    iget-object v1, p0, Lflc;->av:Llqm;

    invoke-direct {v0, v1}, Lhke;-><init>(Llqr;)V

    iget-object v1, p0, Lflc;->au:Llnh;

    .line 79
    invoke-virtual {v0, v1}, Lhke;->a(Llnh;)Lhke;

    move-result-object v0

    const v1, 0x7f1000c6

    iget-object v2, p0, Lflc;->P:Lhkd;

    .line 80
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    iput-object v0, p0, Lflc;->Q:Lhke;

    .line 83
    new-instance v0, Lkgp;

    iget-object v1, p0, Lflc;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkgp;-><init>(Lkgr;Llqr;)V

    iput-object v0, p0, Lflc;->R:Lkgp;

    .line 85
    new-instance v0, Lhoc;

    iget-object v1, p0, Lflc;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 86
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lflc;->S:Lhoc;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflc;->ab:Ljava/util/List;

    .line 101
    new-instance v0, Lfle;

    iget-object v1, p0, Lflc;->R:Lkgp;

    iget-object v2, p0, Lflc;->av:Llqm;

    invoke-direct {v0, p0, p0, v1, v2}, Lfle;-><init>(Lflc;Lkgr;Lkgp;Llqr;)V

    iput-object v0, p0, Lflc;->ad:Lbc;

    .line 129
    return-void
.end method

.method private U()V
    .locals 7

    .prologue
    const v6, 0x7f0a04a6

    const v5, 0x7f0a04a1

    .line 323
    iget-object v0, p0, Lflc;->N:Ldvq;

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lflc;->N:Ldvq;

    .line 326
    invoke-virtual {v0}, Ldvq;->e()Lhgw;

    move-result-object v1

    .line 327
    if-eqz v1, :cond_1

    iget-object v2, p0, Lflc;->aa:Lkhl;

    if-eqz v2, :cond_1

    .line 328
    iget-object v2, p0, Lflc;->aa:Lkhl;

    .line 329
    invoke-virtual {v2}, Lkhl;->B()Landroid/content/Context;

    move-result-object v2

    .line 330
    invoke-virtual {v0}, Ldvq;->g()Ljava/lang/String;

    move-result-object v0

    .line 328
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lhgw;->k()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Lhgw;->b()[Lhxc;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v3, v1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lhxc;->c()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 331
    :goto_0
    :sswitch_0
    iget-object v1, p0, Lflc;->aa:Lkhl;

    invoke-virtual {v1, v0}, Lkhl;->d(Ljava/lang/CharSequence;)V

    .line 334
    :cond_1
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d()Z

    move-result v0

    invoke-direct {p0, v0}, Lflc;->a(Z)V

    .line 335
    return-void

    .line 328
    :sswitch_1
    const v0, 0x7f0a04a5

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    const v0, 0x7f0a04a4

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    const v0, 0x7f0a04a3

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_5
    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x5 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_0
        0x9 -> :sswitch_3
        0x65 -> :sswitch_4
    .end sparse-switch
.end method

.method static synthetic a(Lflc;)Ldvq;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->N:Ldvq;

    return-object v0
.end method

.method static synthetic a(Lflc;Ldvq;)Ldvq;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lflc;->N:Ldvq;

    return-object p1
.end method

.method static synthetic a(Lflc;Lkhl;)Lkhl;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lflc;->ac:Lkhl;

    return-object p1
.end method

.method static synthetic a(Lflc;Lhgw;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lflc;->a(Lhgw;)V

    return-void
.end method

.method static synthetic a(Lflc;Z)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lflc;->a(Z)V

    return-void
.end method

.method private a(Lhgw;)V
    .locals 5

    .prologue
    .line 361
    invoke-static {}, Ldvs;->a()Ldvs;

    move-result-object v0

    .line 362
    iget-object v1, p0, Lflc;->N:Ldvq;

    .line 363
    invoke-virtual {v1}, Ldvq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldvs;->a(Ljava/lang/String;)Ldvs;

    move-result-object v1

    iget-object v2, p0, Lflc;->N:Ldvq;

    .line 364
    invoke-virtual {v2}, Ldvq;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ldvs;->a(I)Ldvs;

    move-result-object v1

    .line 365
    invoke-virtual {v1, p1}, Ldvs;->a(Lhgw;)Ldvs;

    move-result-object v1

    .line 366
    invoke-virtual {v1}, Ldvs;->b()Ldvq;

    move-result-object v1

    .line 368
    new-instance v2, Lfjb;

    iget-object v3, p0, Lflc;->at:Llnl;

    iget v4, p0, Lflc;->V:I

    invoke-direct {v2, v3, v4, v1}, Lfjb;-><init>(Landroid/content/Context;ILdvq;)V

    .line 371
    iget-object v1, p0, Lflc;->N:Ldvq;

    .line 372
    invoke-virtual {v1}, Ldvq;->d()[Ldvo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldvs;->a([Ldvo;)Ldvs;

    move-result-object v0

    iget-object v1, p0, Lflc;->N:Ldvq;

    .line 373
    invoke-virtual {v1}, Ldvq;->f()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Ldvs;->a([I)Ldvs;

    move-result-object v0

    iget-object v1, p0, Lflc;->N:Ldvq;

    .line 374
    invoke-virtual {v1}, Ldvq;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldvs;->b(Ljava/lang/String;)Ldvs;

    move-result-object v0

    iget-object v1, p0, Lflc;->N:Ldvq;

    .line 375
    invoke-virtual {v1}, Ldvq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldvs;->a(Ljava/lang/String;)Ldvs;

    move-result-object v0

    iget-object v1, p0, Lflc;->N:Ldvq;

    .line 376
    invoke-virtual {v1}, Ldvq;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ldvs;->a(I)Ldvs;

    move-result-object v0

    .line 377
    invoke-virtual {v0, p1}, Ldvs;->a(Lhgw;)Ldvs;

    move-result-object v0

    .line 378
    invoke-virtual {v0}, Ldvs;->b()Ldvq;

    move-result-object v0

    iput-object v0, p0, Lflc;->O:Ldvq;

    .line 380
    iget-object v0, p0, Lflc;->S:Lhoc;

    invoke-virtual {v0, v2}, Lhoc;->c(Lhny;)V

    .line 381
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lflc;->Y:Lfjm;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lflc;->Y:Lfjm;

    invoke-virtual {v0, p1}, Lfjm;->b(Z)V

    .line 342
    :cond_0
    iget-object v0, p0, Lflc;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 343
    iget-object v0, p0, Lflc;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->b(Z)V

    .line 346
    :cond_1
    iget-object v0, p0, Lflc;->aa:Lkhl;

    if-eqz v0, :cond_2

    .line 347
    iget-object v0, p0, Lflc;->aa:Lkhl;

    invoke-virtual {v0, p1}, Lkhl;->b(Z)V

    .line 350
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lflc;->ab:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 351
    iget-object v0, p0, Lflc;->ab:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkhl;

    invoke-virtual {v0, p1}, Lkhl;->b(Z)V

    .line 350
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 355
    :cond_3
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_4

    .line 356
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->b(Z)V

    .line 358
    :cond_4
    return-void
.end method

.method static synthetic b(Lflc;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lflc;->V:I

    return v0
.end method

.method static synthetic c(Lflc;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lflc;->U()V

    return-void
.end method

.method static synthetic d(Lflc;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lflc;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->at:Llnl;

    return-object v0
.end method

.method static synthetic f(Lflc;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Lflc;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Lflc;)Lfjm;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->Y:Lfjm;

    return-object v0
.end method

.method static synthetic i(Lflc;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->at:Llnl;

    return-object v0
.end method

.method static synthetic j(Lflc;)Lkhl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->aa:Lkhl;

    return-object v0
.end method

.method static synthetic k(Lflc;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->at:Llnl;

    return-object v0
.end method

.method static synthetic l(Lflc;)Lhoc;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflc;->S:Lhoc;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 167
    invoke-virtual {p0}, Lflc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lflc;->ad:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 168
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 134
    new-instance v0, Lkhg;

    check-cast p1, Los;

    invoke-direct {v0, p1}, Lkhg;-><init>(Los;)V

    iput-object v0, p0, Lflc;->W:Lkhg;

    .line 136
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 149
    if-eqz p1, :cond_0

    .line 150
    const-string v0, "notification_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const-string v0, "notification_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldvq;

    iput-object v0, p0, Lflc;->N:Ldvq;

    .line 154
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 386
    const-string v0, "ChangeNotificationSettingsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflc;->ac:Lkhl;

    if-eqz v0, :cond_3

    .line 389
    iget-object v0, p0, Lflc;->ac:Lkhl;

    instance-of v0, v0, Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lflc;->ac:Lkhl;

    check-cast v0, Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Lflc;->ac:Lkhl;

    check-cast v1, Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 391
    invoke-virtual {v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 392
    iput-object v3, p0, Lflc;->ac:Lkhl;

    .line 394
    :cond_0
    invoke-virtual {p0}, Lflc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lflc;->ad:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 403
    :cond_1
    :goto_1
    return-void

    .line 391
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 396
    :cond_3
    iget-object v0, p0, Lflc;->ac:Lkhl;

    iget-object v1, p0, Lflc;->aa:Lkhl;

    if-ne v0, v1, :cond_1

    .line 397
    iput-object v3, p0, Lflc;->ac:Lkhl;

    .line 398
    iget-object v0, p0, Lflc;->O:Ldvq;

    iput-object v0, p0, Lflc;->N:Ldvq;

    .line 399
    invoke-direct {p0}, Lflc;->U()V

    goto :goto_1
.end method

.method public aj()V
    .locals 4

    .prologue
    .line 417
    invoke-virtual {p0}, Lflc;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lflc;->U:Lhee;

    .line 418
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const v2, 0x7f0a0377

    iget-object v3, p0, Lflc;->N:Ldvq;

    .line 420
    invoke-virtual {v3}, Ldvq;->e()Lhgw;

    move-result-object v3

    .line 417
    invoke-static {v0, v1, v2, v3}, Lfuk;->a(Landroid/app/Activity;IILhgw;)Landroid/content/Intent;

    move-result-object v0

    .line 421
    iget-object v1, p0, Lflc;->Q:Lhke;

    const v2, 0x7f1000c6

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 422
    return-void
.end method

.method public b(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 407
    new-instance v0, Lhgw;

    new-instance v1, Lhxc;

    const/4 v2, 0x1

    invoke-direct {v1, p1, p2, p3, v2}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v0, v1}, Lhgw;-><init>(Lhxc;)V

    .line 409
    iget-object v1, p0, Lflc;->at:Llnl;

    iget-object v2, p0, Lflc;->N:Ldvq;

    .line 410
    invoke-virtual {v2}, Ldvq;->e()Lhgw;

    move-result-object v2

    .line 409
    invoke-static {v1, v0, v2}, Lfuk;->a(Landroid/content/Context;Lhgw;Lhgw;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 411
    invoke-direct {p0, v0}, Lflc;->a(Lhgw;)V

    .line 413
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 141
    iget-object v0, p0, Lflc;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lflc;->U:Lhee;

    .line 142
    iget-object v0, p0, Lflc;->U:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lflc;->V:I

    .line 143
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 172
    invoke-virtual {p0}, Lflc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lflc;->ad:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 173
    return-void
.end method

.method public e()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 177
    new-instance v0, Lkhr;

    iget-object v3, p0, Lflc;->at:Llnl;

    invoke-direct {v0, v3}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflc;->T:Lkhr;

    .line 181
    iget-object v0, p0, Lflc;->T:Lkhr;

    const v3, 0x7f0a069c

    .line 182
    invoke-virtual {p0, v3}, Lflc;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a069d

    .line 183
    invoke-virtual {p0, v4}, Lflc;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 181
    invoke-virtual {v0, v3, v4}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 184
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v3, "notifications_enabled"

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 186
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Z)V

    .line 187
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v3, p0, Lflc;->U:Lhee;

    invoke-interface {v3}, Lhee;->f()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->b(Z)V

    .line 188
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v3, p0, Lflc;->at:Llnl;

    iget v4, p0, Lflc;->V:I

    invoke-static {v3, v4}, Leum;->a(Landroid/content/Context;I)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 191
    iget-object v0, p0, Lflc;->W:Lkhg;

    iget-object v3, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v3, v2}, Lkhg;->a(Lcom/google/android/libraries/social/settings/CheckBoxPreference;Z)V

    .line 193
    iget-object v0, p0, Lflc;->X:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    new-instance v3, Lflf;

    invoke-direct {v3, p0}, Lflf;-><init>(Lflc;)V

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 203
    new-instance v0, Lfjm;

    iget-object v3, p0, Lflc;->at:Llnl;

    invoke-direct {v0, v3}, Lfjm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflc;->Y:Lfjm;

    .line 204
    iget-object v0, p0, Lflc;->Y:Lfjm;

    const v3, 0x7f0a069f

    invoke-virtual {p0, v3}, Lflc;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lfjm;->e(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v0, p0, Lflc;->Y:Lfjm;

    invoke-virtual {v0, v2}, Lfjm;->d(Z)V

    .line 206
    iget-object v0, p0, Lflc;->Y:Lfjm;

    const-string v3, "notifications_ringtone"

    invoke-virtual {v0, v3}, Lfjm;->d(Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lflc;->Y:Lfjm;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lfjm;->a(I)V

    .line 209
    iget-object v0, p0, Lflc;->at:Llnl;

    iget v3, p0, Lflc;->V:I

    invoke-static {v0, v3}, Leum;->d(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lflc;->at:Llnl;

    iget v3, p0, Lflc;->V:I

    invoke-static {v0, v3}, Leum;->c(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v3

    .line 211
    iget-object v0, p0, Lflc;->at:Llnl;

    invoke-static {v0, v3}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    .line 212
    if-nez v0, :cond_0

    move-object v0, v1

    .line 213
    :goto_0
    iget-object v4, p0, Lflc;->Y:Lfjm;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lfjm;->a(Ljava/lang/Object;)V

    .line 214
    iget-object v3, p0, Lflc;->Y:Lfjm;

    invoke-virtual {v3, v0}, Lfjm;->d(Ljava/lang/CharSequence;)V

    .line 220
    :goto_1
    iget-object v0, p0, Lflc;->Y:Lfjm;

    new-instance v3, Lflg;

    invoke-direct {v3, p0}, Lflg;-><init>(Lflc;)V

    invoke-virtual {v0, v3}, Lfjm;->a(Lkhp;)V

    .line 240
    iget-object v0, p0, Lflc;->R:Lkgp;

    iget-object v3, p0, Lflc;->Y:Lfjm;

    invoke-virtual {v0, v3}, Lkgp;->a(Lkhl;)Lkhl;

    .line 242
    iget-object v0, p0, Lflc;->T:Lkhr;

    const v3, 0x7f0a069e

    .line 243
    invoke-virtual {p0, v3}, Lflc;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 242
    invoke-virtual {v0, v3, v1}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lflc;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 244
    iget-object v0, p0, Lflc;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v3, "notifications_vibrate"

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lflc;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Z)V

    .line 246
    iget-object v0, p0, Lflc;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v3, p0, Lflc;->at:Llnl;

    iget v4, p0, Lflc;->V:I

    invoke-static {v3, v4}, Leum;->b(Landroid/content/Context;I)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 247
    iget-object v0, p0, Lflc;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    new-instance v3, Lflh;

    invoke-direct {v3, p0}, Lflh;-><init>(Lflc;)V

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 256
    iget-object v0, p0, Lflc;->R:Lkgp;

    iget-object v3, p0, Lflc;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v3}, Lkgp;->a(Lkhl;)Lkhl;

    .line 258
    iget-object v0, p0, Lflc;->T:Lkhr;

    const v3, 0x7f0a06a3

    .line 259
    invoke-virtual {p0, v3}, Lflc;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 258
    invoke-virtual {v0, v3, v1}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    iput-object v0, p0, Lflc;->aa:Lkhl;

    .line 260
    iget-object v0, p0, Lflc;->aa:Lkhl;

    const-string v3, "notifications_who_can_notify_me"

    invoke-virtual {v0, v3}, Lkhl;->d(Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lflc;->aa:Lkhl;

    invoke-virtual {v0, v2}, Lkhl;->d(Z)V

    .line 262
    iget-object v0, p0, Lflc;->aa:Lkhl;

    new-instance v3, Lfli;

    invoke-direct {v3, p0}, Lfli;-><init>(Lflc;)V

    invoke-virtual {v0, v3}, Lkhl;->a(Lkhq;)V

    .line 273
    iget-object v0, p0, Lflc;->R:Lkgp;

    iget-object v3, p0, Lflc;->aa:Lkhl;

    invoke-virtual {v0, v3}, Lkgp;->a(Lkhl;)Lkhl;

    .line 275
    iget-object v0, p0, Lflc;->ab:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 276
    iget-object v0, p0, Lflc;->N:Ldvq;

    invoke-virtual {v0}, Ldvq;->c()I

    move-result v4

    move v3, v2

    .line 277
    :goto_2
    if-ge v3, v4, :cond_3

    .line 278
    iget-object v0, p0, Lflc;->N:Ldvq;

    invoke-virtual {v0, v3}, Ldvq;->a(I)Ldvo;

    move-result-object v5

    .line 279
    iget-object v0, p0, Lflc;->T:Lkhr;

    .line 280
    invoke-virtual {v5}, Ldvo;->a()Ljava/lang/String;

    move-result-object v6

    .line 279
    invoke-virtual {v0, v6}, Lkhr;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v6

    .line 282
    iget-object v0, p0, Lflc;->R:Lkgp;

    invoke-virtual {v0, v6}, Lkgp;->a(Lkhl;)Lkhl;

    .line 284
    invoke-virtual {v5}, Ldvo;->b()I

    move-result v7

    move v0, v2

    :goto_3
    if-ge v0, v7, :cond_2

    .line 285
    invoke-virtual {v5, v0}, Ldvo;->a(I)Lnoy;

    move-result-object v8

    .line 286
    iget-object v9, p0, Lflc;->T:Lkhr;

    iget-object v10, v8, Lnoy;->b:Ljava/lang/String;

    invoke-virtual {v9, v10, v1}, Lkhr;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v9

    .line 288
    iget-object v10, v8, Lnoy;->d:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    invoke-virtual {v9, v10}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 289
    new-instance v10, Lflj;

    invoke-direct {v10, p0, v8}, Lflj;-><init>(Lflc;Lnoy;)V

    invoke-virtual {v9, v10}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 316
    invoke-virtual {v6, v9}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 317
    iget-object v8, p0, Lflc;->ab:Ljava/util/List;

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 212
    :cond_0
    iget-object v4, p0, Lflc;->at:Llnl;

    invoke-virtual {v0, v4}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lflc;->Y:Lfjm;

    const v3, 0x7f0a06f5

    invoke-virtual {v0, v3}, Lfjm;->i(I)V

    .line 217
    iget-object v0, p0, Lflc;->Y:Lfjm;

    invoke-virtual {v0, v1}, Lfjm;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 277
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 320
    :cond_3
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 160
    iget-object v0, p0, Lflc;->N:Ldvq;

    if-eqz v0, :cond_0

    .line 161
    const-string v0, "notification_settings"

    iget-object v1, p0, Lflc;->N:Ldvq;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 163
    :cond_0
    return-void
.end method
