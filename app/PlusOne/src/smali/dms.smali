.class public final Ldms;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmim;",
        "Lmin;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:I

.field private final r:Z

.field private final s:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 81
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    move-object v8, v5

    invoke-direct/range {v0 .. v8}, Ldms;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZLjava/lang/String;)V
    .locals 9

    .prologue
    .line 66
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Ldms;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;)V

    .line 68
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;)V
    .locals 6

    .prologue
    .line 40
    const-string v3, "reportabuseactivity"

    new-instance v4, Lmim;

    invoke-direct {v4}, Lmim;-><init>()V

    new-instance v5, Lmin;

    invoke-direct {v5}, Lmin;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 46
    iput-object p3, p0, Ldms;->a:Ljava/lang/String;

    .line 47
    iput-object p4, p0, Ldms;->b:Ljava/lang/String;

    .line 48
    iput-object p5, p0, Ldms;->p:Ljava/lang/String;

    .line 49
    iput p6, p0, Ldms;->q:I

    .line 50
    iput-boolean p7, p0, Ldms;->r:Z

    .line 51
    iput-object p8, p0, Ldms;->s:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method protected a(Lmim;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    iget-object v0, p0, Ldms;->p:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 90
    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Ldms;->a:Ljava/lang/String;

    aput-object v1, v0, v2

    .line 94
    :goto_0
    new-instance v1, Lnvs;

    invoke-direct {v1}, Lnvs;-><init>()V

    iput-object v1, p1, Lmim;->a:Lnvs;

    .line 95
    iget-object v1, p1, Lmim;->a:Lnvs;

    .line 96
    iput-object v0, v1, Lnvs;->a:[Ljava/lang/String;

    .line 97
    iget-boolean v0, p0, Ldms;->r:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnvs;->c:Ljava/lang/Boolean;

    .line 98
    new-instance v0, Llwr;

    invoke-direct {v0}, Llwr;-><init>()V

    iput-object v0, v1, Lnvs;->b:Llwr;

    .line 99
    iget-object v0, v1, Lnvs;->b:Llwr;

    iget v2, p0, Ldms;->q:I

    iput v2, v0, Llwr;->a:I

    .line 100
    iget-object v0, v1, Lnvs;->b:Llwr;

    iget-object v1, p0, Ldms;->b:Ljava/lang/String;

    iput-object v1, v0, Llwr;->b:Ljava/lang/String;

    .line 101
    return-void

    .line 92
    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Ldms;->p:Ljava/lang/String;

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lmim;

    invoke-virtual {p0, p1}, Ldms;->a(Lmim;)V

    return-void
.end method

.method protected b()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    iget-object v0, p0, Ldms;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 108
    iget-object v0, p0, Ldms;->f:Landroid/content/Context;

    iget v1, p0, Ldms;->c:I

    iget-object v2, p0, Ldms;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Llap;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v3, p0, Ldms;->f:Landroid/content/Context;

    iget v4, p0, Ldms;->c:I

    iget-object v5, p0, Ldms;->p:Ljava/lang/String;

    iget-boolean v0, p0, Ldms;->r:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v3, v4, v5, v0}, Llap;->g(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 112
    iget-object v0, p0, Ldms;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Ldms;->f:Landroid/content/Context;

    iget v3, p0, Ldms;->c:I

    iget-object v4, p0, Ldms;->s:Ljava/lang/String;

    iget-object v5, p0, Ldms;->p:Ljava/lang/String;

    iget-boolean v6, p0, Ldms;->r:Z

    if-nez v6, :cond_3

    :goto_2
    invoke-static {v0, v3, v4, v5, v1}, Ljvd;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 110
    goto :goto_1

    :cond_3
    move v1, v2

    .line 113
    goto :goto_2
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0}, Ldms;->b()V

    return-void
.end method
