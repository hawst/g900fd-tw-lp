.class public final Lnxj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnxj;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lnxj;

    sput-object v0, Lnxj;->a:[Lnxj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25
    const/high16 v0, -0x80000000

    iput v0, p0, Lnxj;->b:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    iget v1, p0, Lnxj;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 51
    const/4 v0, 0x1

    iget v1, p0, Lnxj;->b:I

    .line 52
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 54
    :cond_0
    iget-object v1, p0, Lnxj;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 55
    const/4 v1, 0x2

    iget-object v2, p0, Lnxj;->d:Ljava/lang/String;

    .line 56
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_1
    iget-object v1, p0, Lnxj;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 59
    const/4 v1, 0x3

    iget-object v2, p0, Lnxj;->c:Ljava/lang/String;

    .line 60
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_2
    iget-object v1, p0, Lnxj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    iput v0, p0, Lnxj;->ai:I

    .line 64
    return v0
.end method

.method public a(Loxn;)Lnxj;
    .locals 2

    .prologue
    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 73
    sparse-switch v0, :sswitch_data_0

    .line 77
    iget-object v1, p0, Lnxj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnxj;->ah:Ljava/util/List;

    .line 81
    :cond_1
    iget-object v1, p0, Lnxj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    :sswitch_0
    return-object p0

    .line 88
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 89
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 98
    :cond_2
    iput v0, p0, Lnxj;->b:I

    goto :goto_0

    .line 100
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnxj;->b:I

    goto :goto_0

    .line 105
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxj;->d:Ljava/lang/String;

    goto :goto_0

    .line 109
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxj;->c:Ljava/lang/String;

    goto :goto_0

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 34
    iget v0, p0, Lnxj;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 35
    const/4 v0, 0x1

    iget v1, p0, Lnxj;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 37
    :cond_0
    iget-object v0, p0, Lnxj;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 38
    const/4 v0, 0x2

    iget-object v1, p0, Lnxj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    :cond_1
    iget-object v0, p0, Lnxj;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 41
    const/4 v0, 0x3

    iget-object v1, p0, Lnxj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 43
    :cond_2
    iget-object v0, p0, Lnxj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 45
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnxj;->a(Loxn;)Lnxj;

    move-result-object v0

    return-object v0
.end method
