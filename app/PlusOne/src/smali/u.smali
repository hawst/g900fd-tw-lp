.class public Lu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnCreateContextMenuListener;


# static fields
.field private static final N:Lgt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgt",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static O:Ljava/lang/Object;


# instance fields
.field A:Z

.field B:Z

.field C:Z

.field D:Z

.field E:Z

.field F:Z

.field G:I

.field H:Landroid/view/ViewGroup;

.field I:Landroid/view/View;

.field J:Landroid/view/View;

.field K:Z

.field L:Z

.field M:Lbd;

.field private P:Z

.field private Q:Z

.field private R:Ljava/lang/Object;

.field private S:Ljava/lang/Object;

.field private T:Ljava/lang/Object;

.field a:I

.field b:Landroid/view/View;

.field c:I

.field d:Landroid/os/Bundle;

.field e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field f:I

.field g:Ljava/lang/String;

.field public h:Landroid/os/Bundle;

.field i:Lu;

.field j:I

.field k:I

.field l:Z

.field public m:Z

.field n:Z

.field o:Z

.field p:Z

.field q:Z

.field r:I

.field s:Lah;

.field public t:Lz;

.field u:Lah;

.field v:Lu;

.field w:I

.field x:I

.field y:Ljava/lang/String;

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lgt;

    invoke-direct {v0}, Lgt;-><init>()V

    sput-object v0, Lu;->N:Lgt;

    .line 171
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lu;->O:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    const/4 v0, 0x0

    iput v0, p0, Lu;->a:I

    .line 196
    iput v1, p0, Lu;->f:I

    .line 208
    iput v1, p0, Lu;->j:I

    .line 279
    iput-boolean v2, p0, Lu;->E:Z

    .line 301
    iput-boolean v2, p0, Lu;->L:Z

    .line 307
    sget-object v0, Lu;->O:Ljava/lang/Object;

    iput-object v0, p0, Lu;->R:Ljava/lang/Object;

    .line 309
    sget-object v0, Lu;->O:Ljava/lang/Object;

    iput-object v0, p0, Lu;->S:Ljava/lang/Object;

    .line 311
    sget-object v0, Lu;->O:Ljava/lang/Object;

    iput-object v0, p0, Lu;->T:Ljava/lang/Object;

    .line 316
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lu;
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lu;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lu;
    .locals 4

    .prologue
    .line 414
    :try_start_0
    sget-object v0, Lu;->N:Lgt;

    invoke-virtual {v0, p1}, Lgt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 415
    if-nez v0, :cond_0

    .line 417
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 418
    sget-object v1, Lu;->N:Lgt;

    invoke-virtual {v1, p1, v0}, Lgt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 421
    if-eqz p2, :cond_1

    .line 422
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 423
    iput-object p2, v0, Lu;->h:Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 425
    :cond_1
    return-object v0

    .line 426
    :catch_0
    move-exception v0

    .line 427
    new-instance v1, Lw;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lw;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 430
    :catch_1
    move-exception v0

    .line 431
    new-instance v1, Lw;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lw;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 434
    :catch_2
    move-exception v0

    .line 435
    new-instance v1, Lw;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lw;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 451
    :try_start_0
    sget-object v0, Lu;->N:Lgt;

    invoke-virtual {v0, p1}, Lgt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 452
    if-nez v0, :cond_0

    .line 454
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 455
    sget-object v1, Lu;->N:Lgt;

    invoke-virtual {v1, p1, v0}, Lgt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    :cond_0
    const-class v1, Lu;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 459
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1194
    iput-boolean v1, p0, Lu;->F:Z

    .line 1197
    iget-boolean v0, p0, Lu;->Q:Z

    if-nez v0, :cond_0

    .line 1198
    iput-boolean v1, p0, Lu;->Q:Z

    .line 1199
    iget-object v0, p0, Lu;->t:Lz;

    iget-object v1, p0, Lu;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lu;->P:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(Ljava/lang/String;ZZ)Lbd;

    move-result-object v0

    iput-object v0, p0, Lu;->M:Lbd;

    .line 1201
    :cond_0
    iget-object v0, p0, Lu;->M:Lbd;

    if-eqz v0, :cond_1

    .line 1202
    iget-object v0, p0, Lu;->M:Lbd;

    invoke-virtual {v0}, Lbd;->h()V

    .line 1204
    :cond_1
    return-void
.end method

.method B()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1213
    const/4 v0, -0x1

    iput v0, p0, Lu;->f:I

    .line 1214
    iput-object v2, p0, Lu;->g:Ljava/lang/String;

    .line 1215
    iput-boolean v1, p0, Lu;->l:Z

    .line 1216
    iput-boolean v1, p0, Lu;->m:Z

    .line 1217
    iput-boolean v1, p0, Lu;->n:Z

    .line 1218
    iput-boolean v1, p0, Lu;->o:Z

    .line 1219
    iput-boolean v1, p0, Lu;->p:Z

    .line 1220
    iput-boolean v1, p0, Lu;->q:Z

    .line 1221
    iput v1, p0, Lu;->r:I

    .line 1222
    iput-object v2, p0, Lu;->s:Lah;

    .line 1223
    iput-object v2, p0, Lu;->u:Lah;

    .line 1224
    iput-object v2, p0, Lu;->t:Lz;

    .line 1225
    iput v1, p0, Lu;->w:I

    .line 1226
    iput v1, p0, Lu;->x:I

    .line 1227
    iput-object v2, p0, Lu;->y:Ljava/lang/String;

    .line 1228
    iput-boolean v1, p0, Lu;->z:Z

    .line 1229
    iput-boolean v1, p0, Lu;->A:Z

    .line 1230
    iput-boolean v1, p0, Lu;->C:Z

    .line 1231
    iput-object v2, p0, Lu;->M:Lbd;

    .line 1232
    iput-boolean v1, p0, Lu;->P:Z

    .line 1233
    iput-boolean v1, p0, Lu;->Q:Z

    .line 1234
    return-void
.end method

.method public C()V
    .locals 0

    .prologue
    .line 1285
    return-void
.end method

.method public D()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1432
    const/4 v0, 0x0

    return-object v0
.end method

.method public E()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1466
    iget-object v0, p0, Lu;->R:Ljava/lang/Object;

    sget-object v1, Lu;->O:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lu;->D()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lu;->R:Ljava/lang/Object;

    goto :goto_0
.end method

.method public F()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1500
    const/4 v0, 0x0

    return-object v0
.end method

.method public G()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1533
    iget-object v0, p0, Lu;->S:Ljava/lang/Object;

    sget-object v1, Lu;->O:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lu;->F()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lu;->S:Ljava/lang/Object;

    goto :goto_0
.end method

.method public H()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1560
    const/4 v0, 0x0

    return-object v0
.end method

.method public I()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1592
    iget-object v0, p0, Lu;->T:Ljava/lang/Object;

    sget-object v1, Lu;->O:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lu;->H()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lu;->T:Ljava/lang/Object;

    goto :goto_0
.end method

.method public J()Z
    .locals 1

    .prologue
    .line 1617
    const/4 v0, 0x1

    return v0
.end method

.method public K()Z
    .locals 1

    .prologue
    .line 1641
    const/4 v0, 0x1

    return v0
.end method

.method L()V
    .locals 3

    .prologue
    .line 1741
    new-instance v0, Lah;

    invoke-direct {v0}, Lah;-><init>()V

    iput-object v0, p0, Lu;->u:Lah;

    .line 1742
    iget-object v0, p0, Lu;->u:Lah;

    iget-object v1, p0, Lu;->t:Lz;

    new-instance v2, Lv;

    invoke-direct {v2, p0}, Lv;-><init>(Lu;)V

    invoke-virtual {v0, v1, v2, p0}, Lah;->a(Lz;Lad;Lu;)V

    .line 1756
    return-void
.end method

.method M()V
    .locals 3

    .prologue
    .line 1805
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1806
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->noteStateNotSaved()V

    .line 1807
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->h()Z

    .line 1809
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lu;->F:Z

    .line 1810
    invoke-virtual {p0}, Lu;->g()V

    .line 1811
    iget-boolean v0, p0, Lu;->F:Z

    if-nez v0, :cond_1

    .line 1812
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1815
    :cond_1
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_2

    .line 1816
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->n()V

    .line 1818
    :cond_2
    iget-object v0, p0, Lu;->M:Lbd;

    if-eqz v0, :cond_3

    .line 1819
    iget-object v0, p0, Lu;->M:Lbd;

    invoke-virtual {v0}, Lbd;->g()V

    .line 1821
    :cond_3
    return-void
.end method

.method N()V
    .locals 3

    .prologue
    .line 1824
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1825
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->noteStateNotSaved()V

    .line 1826
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->h()Z

    .line 1828
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lu;->F:Z

    .line 1829
    invoke-virtual {p0}, Lu;->aO_()V

    .line 1830
    iget-boolean v0, p0, Lu;->F:Z

    if-nez v0, :cond_1

    .line 1831
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1834
    :cond_1
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_2

    .line 1835
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->o()V

    .line 1836
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->h()Z

    .line 1838
    :cond_2
    return-void
.end method

.method O()V
    .locals 1

    .prologue
    .line 1848
    invoke-virtual {p0}, Lu;->onLowMemory()V

    .line 1849
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1850
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->u()V

    .line 1852
    :cond_0
    return-void
.end method

.method P()V
    .locals 3

    .prologue
    .line 1943
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1944
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->p()V

    .line 1946
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lu;->F:Z

    .line 1947
    invoke-virtual {p0}, Lu;->z()V

    .line 1948
    iget-boolean v0, p0, Lu;->F:Z

    if-nez v0, :cond_1

    .line 1949
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1952
    :cond_1
    return-void
.end method

.method Q()V
    .locals 3

    .prologue
    .line 1955
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1956
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->q()V

    .line 1958
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lu;->F:Z

    .line 1959
    invoke-virtual {p0}, Lu;->h()V

    .line 1960
    iget-boolean v0, p0, Lu;->F:Z

    if-nez v0, :cond_1

    .line 1961
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1964
    :cond_1
    return-void
.end method

.method R()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1967
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1968
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->r()V

    .line 1970
    :cond_0
    iget-boolean v0, p0, Lu;->P:Z

    if-eqz v0, :cond_2

    .line 1971
    iput-boolean v3, p0, Lu;->P:Z

    .line 1972
    iget-boolean v0, p0, Lu;->Q:Z

    if-nez v0, :cond_1

    .line 1973
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->Q:Z

    .line 1974
    iget-object v0, p0, Lu;->t:Lz;

    iget-object v1, p0, Lu;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lu;->P:Z

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(Ljava/lang/String;ZZ)Lbd;

    move-result-object v0

    iput-object v0, p0, Lu;->M:Lbd;

    .line 1976
    :cond_1
    iget-object v0, p0, Lu;->M:Lbd;

    if-eqz v0, :cond_2

    .line 1977
    iget-object v0, p0, Lu;->t:Lz;

    iget-boolean v0, v0, Lz;->d:Z

    if-nez v0, :cond_3

    .line 1978
    iget-object v0, p0, Lu;->M:Lbd;

    invoke-virtual {v0}, Lbd;->c()V

    .line 1984
    :cond_2
    :goto_0
    return-void

    .line 1980
    :cond_3
    iget-object v0, p0, Lu;->M:Lbd;

    invoke-virtual {v0}, Lbd;->d()V

    goto :goto_0
.end method

.method S()V
    .locals 3

    .prologue
    .line 1987
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1988
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->s()V

    .line 1990
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lu;->F:Z

    .line 1991
    invoke-virtual {p0}, Lu;->ae_()V

    .line 1992
    iget-boolean v0, p0, Lu;->F:Z

    if-nez v0, :cond_1

    .line 1993
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1996
    :cond_1
    iget-object v0, p0, Lu;->M:Lbd;

    if-eqz v0, :cond_2

    .line 1997
    iget-object v0, p0, Lu;->M:Lbd;

    invoke-virtual {v0}, Lbd;->f()V

    .line 1999
    :cond_2
    return-void
.end method

.method T()V
    .locals 3

    .prologue
    .line 2002
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 2003
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->t()V

    .line 2005
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lu;->F:Z

    .line 2006
    invoke-virtual {p0}, Lu;->A()V

    .line 2007
    iget-boolean v0, p0, Lu;->F:Z

    if-nez v0, :cond_1

    .line 2008
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2011
    :cond_1
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1039
    const/4 v0, 0x0

    return-object v0
.end method

.method public final varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 925
    return-void
.end method

.method final a(ILu;)V
    .locals 2

    .prologue
    .line 477
    iput p1, p0, Lu;->f:I

    .line 478
    if-eqz p2, :cond_0

    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lu;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lu;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lu;->g:Ljava/lang/String;

    .line 483
    :goto_0
    return-void

    .line 481
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:fragment:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lu;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lu;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 990
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 991
    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 982
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 983
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 893
    iget-object v0, p0, Lu;->t:Lz;

    if-nez v0, :cond_0

    .line 894
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 896
    :cond_0
    iget-object v0, p0, Lu;->t:Lz;

    const/4 v1, -0x1

    invoke-virtual {v0, p0, p1, v1}, Lz;->a(Lu;Landroid/content/Intent;I)V

    .line 897
    return-void
.end method

.method public a(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 904
    iget-object v0, p0, Lu;->t:Lz;

    if-nez v0, :cond_0

    .line 905
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 907
    :cond_0
    iget-object v0, p0, Lu;->t:Lz;

    invoke-virtual {v0, p0, p1, p2}, Lz;->a(Lu;Landroid/content/Intent;I)V

    .line 908
    return-void
.end method

.method a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1841
    invoke-virtual {p0, p1}, Lu;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1842
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1843
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0, p1}, Lah;->a(Landroid/content/res/Configuration;)V

    .line 1845
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1015
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1016
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 1275
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 1258
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1053
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1654
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1655
    iget v0, p0, Lu;->w:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1656
    const-string v0, " mContainerId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1657
    iget v0, p0, Lu;->x:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1658
    const-string v0, " mTag="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lu;->y:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1659
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lu;->a:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1660
    const-string v0, " mIndex="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lu;->f:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1661
    const-string v0, " mWho="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lu;->g:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1662
    const-string v0, " mBackStackNesting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lu;->r:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1663
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAdded="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->l:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1664
    const-string v0, " mRemoving="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->m:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1665
    const-string v0, " mResumed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->n:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1666
    const-string v0, " mFromLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->o:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1667
    const-string v0, " mInLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->p:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1668
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHidden="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->z:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1669
    const-string v0, " mDetached="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->A:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1670
    const-string v0, " mMenuVisible="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->E:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1671
    const-string v0, " mHasMenu="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->D:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1672
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetainInstance="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->B:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1673
    const-string v0, " mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->C:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1674
    const-string v0, " mUserVisibleHint="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lu;->L:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1675
    iget-object v0, p0, Lu;->s:Lah;

    if-eqz v0, :cond_0

    .line 1676
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentManager="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1677
    iget-object v0, p0, Lu;->s:Lah;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1679
    :cond_0
    iget-object v0, p0, Lu;->t:Lz;

    if-eqz v0, :cond_1

    .line 1680
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1681
    iget-object v0, p0, Lu;->t:Lz;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1683
    :cond_1
    iget-object v0, p0, Lu;->v:Lu;

    if-eqz v0, :cond_2

    .line 1684
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mParentFragment="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1685
    iget-object v0, p0, Lu;->v:Lu;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1687
    :cond_2
    iget-object v0, p0, Lu;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    .line 1688
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mArguments="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lu;->h:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1690
    :cond_3
    iget-object v0, p0, Lu;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    .line 1691
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedFragmentState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1692
    iget-object v0, p0, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1694
    :cond_4
    iget-object v0, p0, Lu;->e:Landroid/util/SparseArray;

    if-eqz v0, :cond_5

    .line 1695
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedViewState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1696
    iget-object v0, p0, Lu;->e:Landroid/util/SparseArray;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1698
    :cond_5
    iget-object v0, p0, Lu;->i:Lu;

    if-eqz v0, :cond_6

    .line 1699
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTarget="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lu;->i:Lu;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 1700
    const-string v0, " mTargetRequestCode="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1701
    iget v0, p0, Lu;->k:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1703
    :cond_6
    iget v0, p0, Lu;->G:I

    if-eqz v0, :cond_7

    .line 1704
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mNextAnim="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lu;->G:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1706
    :cond_7
    iget-object v0, p0, Lu;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_8

    .line 1707
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lu;->H:Landroid/view/ViewGroup;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1709
    :cond_8
    iget-object v0, p0, Lu;->I:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 1710
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lu;->I:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1712
    :cond_9
    iget-object v0, p0, Lu;->J:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 1713
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mInnerView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lu;->I:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1715
    :cond_a
    iget-object v0, p0, Lu;->b:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 1716
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAnimatingAway="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lu;->b:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1717
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStateAfterAnimating="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1718
    iget v0, p0, Lu;->c:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1720
    :cond_b
    iget-object v0, p0, Lu;->M:Lbd;

    if-eqz v0, :cond_c

    .line 1721
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Loader Manager:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1722
    iget-object v0, p0, Lu;->M:Lbd;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lbd;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1724
    :cond_c
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_d

    .line 1725
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Child "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lu;->u:Lah;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1726
    iget-object v0, p0, Lu;->u:Lah;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lah;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1728
    :cond_d
    return-void
.end method

.method public a(Lu;I)V
    .locals 0

    .prologue
    .line 589
    iput-object p1, p0, Lu;->i:Lu;

    .line 590
    iput p2, p0, Lu;->k:I

    .line 591
    return-void
.end method

.method public a(Lx;)V
    .locals 2

    .prologue
    .line 570
    iget v0, p0, Lu;->f:I

    if-ltz v0, :cond_0

    .line 571
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 573
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lx;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lx;->a:Landroid/os/Bundle;

    :goto_0
    iput-object v0, p0, Lu;->d:Landroid/os/Bundle;

    .line 575
    return-void

    .line 573
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 1125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1126
    return-void
.end method

.method public a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 2

    .prologue
    .line 933
    iget-object v0, p0, Lu;->t:Lz;

    invoke-virtual {v0}, Lz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lu;->t:Lz;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 934
    invoke-virtual {p0}, Lu;->q()Lae;

    .line 935
    iget-object v1, p0, Lu;->u:Lah;

    invoke-virtual {v1}, Lah;->v()Landroid/view/LayoutInflater$Factory;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->setFactory(Landroid/view/LayoutInflater$Factory;)V

    .line 936
    return-object v0
.end method

.method public a_(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1306
    const/4 v0, 0x0

    return v0
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 1186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1187
    return-void
.end method

.method b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1783
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1784
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->noteStateNotSaved()V

    .line 1786
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lu;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1362
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 1363
    return-void
.end method

.method b(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1878
    const/4 v0, 0x0

    .line 1879
    iget-boolean v1, p0, Lu;->z:Z

    if-nez v1, :cond_1

    .line 1880
    iget-boolean v1, p0, Lu;->D:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lu;->E:Z

    if-eqz v1, :cond_0

    .line 1881
    const/4 v0, 0x1

    .line 1882
    invoke-virtual {p0, p1}, Lu;->a(Landroid/view/Menu;)V

    .line 1884
    :cond_0
    iget-object v1, p0, Lu;->u:Lah;

    if-eqz v1, :cond_1

    .line 1885
    iget-object v1, p0, Lu;->u:Lah;

    invoke-virtual {v1, p1}, Lah;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1888
    :cond_1
    return v0
.end method

.method b(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 2

    .prologue
    .line 1864
    const/4 v0, 0x0

    .line 1865
    iget-boolean v1, p0, Lu;->z:Z

    if-nez v1, :cond_1

    .line 1866
    iget-boolean v1, p0, Lu;->D:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lu;->E:Z

    if-eqz v1, :cond_0

    .line 1867
    const/4 v0, 0x1

    .line 1868
    invoke-virtual {p0, p1, p2}, Lu;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1870
    :cond_0
    iget-object v1, p0, Lu;->u:Lah;

    if-eqz v1, :cond_1

    .line 1871
    iget-object v1, p0, Lu;->u:Lah;

    invoke-virtual {v1, p1, p2}, Lah;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1874
    :cond_1
    return v0
.end method

.method public b(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1383
    const/4 v0, 0x0

    return v0
.end method

.method public b_(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1351
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 1352
    return-void
.end method

.method c(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 1922
    iget-boolean v0, p0, Lu;->z:Z

    if-nez v0, :cond_1

    .line 1923
    iget-boolean v0, p0, Lu;->D:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lu;->E:Z

    .line 1924
    :cond_0
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_1

    .line 1927
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0, p1}, Lah;->b(Landroid/view/Menu;)V

    .line 1930
    :cond_1
    return-void
.end method

.method c(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1892
    iget-boolean v1, p0, Lu;->z:Z

    if-nez v1, :cond_2

    .line 1893
    iget-boolean v1, p0, Lu;->D:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lu;->E:Z

    if-eqz v1, :cond_1

    .line 1894
    invoke-virtual {p0, p1}, Lu;->a_(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1904
    :cond_0
    :goto_0
    return v0

    .line 1898
    :cond_1
    iget-object v1, p0, Lu;->u:Lah;

    if-eqz v1, :cond_2

    .line 1899
    iget-object v1, p0, Lu;->u:Lah;

    invoke-virtual {v1, p1}, Lah;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1904
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1080
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1081
    return-void
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 799
    if-eqz p1, :cond_0

    iget-object v0, p0, Lu;->v:Lu;

    if-eqz v0, :cond_0

    .line 800
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t retain fragements that are nested in other fragments"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 803
    :cond_0
    iput-boolean p1, p0, Lu;->B:Z

    .line 804
    return-void
.end method

.method d(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1908
    iget-boolean v1, p0, Lu;->z:Z

    if-nez v1, :cond_2

    .line 1909
    invoke-virtual {p0, p1}, Lu;->b(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1918
    :cond_0
    :goto_0
    return v0

    .line 1912
    :cond_1
    iget-object v1, p0, Lu;->u:Lah;

    if-eqz v1, :cond_2

    .line 1913
    iget-object v1, p0, Lu;->u:Lah;

    invoke-virtual {v1, p1}, Lah;->b(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1918
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1148
    return-void
.end method

.method public e(Z)V
    .locals 1

    .prologue
    .line 818
    iget-boolean v0, p0, Lu;->D:Z

    if-eq v0, p1, :cond_0

    .line 819
    iput-boolean p1, p0, Lu;->D:Z

    .line 820
    invoke-virtual {p0}, Lu;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lu;->z:Z

    if-nez v0, :cond_0

    .line 821
    iget-object v0, p0, Lu;->t:Lz;

    invoke-virtual {v0}, Lz;->az_()V

    .line 824
    :cond_0
    return-void
.end method

.method public final e_(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 641
    invoke-virtual {p0}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 493
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 1241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1242
    return-void
.end method

.method public f(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 547
    iget v0, p0, Lu;->f:I

    if-ltz v0, :cond_0

    .line 548
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 550
    :cond_0
    iput-object p1, p0, Lu;->h:Landroid/os/Bundle;

    .line 551
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1104
    iput-boolean v1, p0, Lu;->F:Z

    .line 1106
    iget-boolean v0, p0, Lu;->P:Z

    if-nez v0, :cond_1

    .line 1107
    iput-boolean v1, p0, Lu;->P:Z

    .line 1108
    iget-boolean v0, p0, Lu;->Q:Z

    if-nez v0, :cond_0

    .line 1109
    iput-boolean v1, p0, Lu;->Q:Z

    .line 1110
    iget-object v0, p0, Lu;->t:Lz;

    iget-object v1, p0, Lu;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lu;->P:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(Ljava/lang/String;ZZ)Lbd;

    move-result-object v0

    iput-object v0, p0, Lu;->M:Lbd;

    .line 1112
    :cond_0
    iget-object v0, p0, Lu;->M:Lbd;

    if-eqz v0, :cond_1

    .line 1113
    iget-object v0, p0, Lu;->M:Lbd;

    invoke-virtual {v0}, Lbd;->b()V

    .line 1116
    :cond_1
    return-void
.end method

.method public g(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1095
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1096
    return-void
.end method

.method public g(Z)V
    .locals 2

    .prologue
    .line 858
    iget-boolean v0, p0, Lu;->L:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lu;->a:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 859
    iget-object v0, p0, Lu;->s:Lah;

    invoke-virtual {v0, p0}, Lah;->b(Lu;)V

    .line 861
    :cond_0
    iput-boolean p1, p0, Lu;->L:Z

    .line 862
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lu;->K:Z

    .line 863
    return-void

    .line 862
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(Z)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 997
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1170
    return-void
.end method

.method h(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1759
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1760
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->noteStateNotSaved()V

    .line 1762
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lu;->F:Z

    .line 1763
    invoke-virtual {p0, p1}, Lu;->a(Landroid/os/Bundle;)V

    .line 1764
    iget-boolean v0, p0, Lu;->F:Z

    if-nez v0, :cond_1

    .line 1765
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1768
    :cond_1
    if-eqz p1, :cond_3

    .line 1769
    const-string v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1771
    if-eqz v0, :cond_3

    .line 1772
    iget-object v1, p0, Lu;->u:Lah;

    if-nez v1, :cond_2

    .line 1773
    invoke-virtual {p0}, Lu;->L()V

    .line 1775
    :cond_2
    iget-object v1, p0, Lu;->u:Lah;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lah;->a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    .line 1776
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->l()V

    .line 1779
    :cond_3
    return-void
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 500
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method i(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1790
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1791
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->noteStateNotSaved()V

    .line 1793
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lu;->F:Z

    .line 1794
    invoke-virtual {p0, p1}, Lu;->d(Landroid/os/Bundle;)V

    .line 1795
    iget-boolean v0, p0, Lu;->F:Z

    if-nez v0, :cond_1

    .line 1796
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1799
    :cond_1
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_2

    .line 1800
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->m()V

    .line 1802
    :cond_2
    return-void
.end method

.method public final i_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 631
    invoke-virtual {p0}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public i_(Z)V
    .locals 1

    .prologue
    .line 836
    iget-boolean v0, p0, Lu;->E:Z

    if-eq v0, p1, :cond_0

    .line 837
    iput-boolean p1, p0, Lu;->E:Z

    .line 838
    iget-boolean v0, p0, Lu;->D:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lu;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lu;->z:Z

    if-nez v0, :cond_0

    .line 839
    iget-object v0, p0, Lu;->t:Lz;

    invoke-virtual {v0}, Lz;->az_()V

    .line 842
    :cond_0
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lu;->y:Ljava/lang/String;

    return-object v0
.end method

.method j(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1933
    invoke-virtual {p0, p1}, Lu;->e(Landroid/os/Bundle;)V

    .line 1934
    iget-object v0, p0, Lu;->u:Lah;

    if-eqz v0, :cond_0

    .line 1935
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->k()Landroid/os/Parcelable;

    move-result-object v0

    .line 1936
    if-eqz v0, :cond_0

    .line 1937
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1940
    :cond_0
    return-void
.end method

.method public final k()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lu;->h:Landroid/os/Bundle;

    return-object v0
.end method

.method public final n()Lz;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lu;->t:Lz;

    return-object v0
.end method

.method public final o()Landroid/content/res/Resources;
    .locals 3

    .prologue
    .line 618
    iget-object v0, p0, Lu;->t:Lz;

    if-nez v0, :cond_0

    .line 619
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 621
    :cond_0
    iget-object v0, p0, Lu;->t:Lz;

    invoke-virtual {v0}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1152
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 1337
    iget-object v0, p0, Lu;->t:Lz;

    invoke-virtual {v0, p1, p2, p3}, Lz;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1338
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 1173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1174
    return-void
.end method

.method public final p()Lae;
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lu;->s:Lah;

    return-object v0
.end method

.method public final q()Lae;
    .locals 2

    .prologue
    .line 676
    iget-object v0, p0, Lu;->u:Lah;

    if-nez v0, :cond_0

    .line 677
    invoke-virtual {p0}, Lu;->L()V

    .line 678
    iget v0, p0, Lu;->a:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    .line 679
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->o()V

    .line 688
    :cond_0
    :goto_0
    iget-object v0, p0, Lu;->u:Lah;

    return-object v0

    .line 680
    :cond_1
    iget v0, p0, Lu;->a:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    .line 681
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->n()V

    goto :goto_0

    .line 682
    :cond_2
    iget v0, p0, Lu;->a:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_3

    .line 683
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->m()V

    goto :goto_0

    .line 684
    :cond_3
    iget v0, p0, Lu;->a:I

    if-lez v0, :cond_0

    .line 685
    iget-object v0, p0, Lu;->u:Lah;

    invoke-virtual {v0}, Lah;->l()V

    goto :goto_0
.end method

.method public final r()Lu;
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lu;->v:Lu;

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lu;->t:Lz;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lu;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 712
    iget-boolean v0, p0, Lu;->A:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 505
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 506
    invoke-static {p0, v0}, Lgi;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 507
    iget v1, p0, Lu;->f:I

    if-ltz v1, :cond_0

    .line 508
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    iget v1, p0, Lu;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 511
    :cond_0
    iget v1, p0, Lu;->w:I

    if-eqz v1, :cond_1

    .line 512
    const-string v1, " id=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    iget v1, p0, Lu;->w:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    :cond_1
    iget-object v1, p0, Lu;->y:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 516
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    iget-object v1, p0, Lu;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    :cond_2
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 520
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 740
    iget-boolean v0, p0, Lu;->n:Z

    return v0
.end method

.method public final u_()Lu;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lu;->i:Lu;

    return-object v0
.end method

.method public v()V
    .locals 0

    .prologue
    .line 782
    return-void
.end method

.method public final v_()I
    .locals 1

    .prologue
    .line 604
    iget v0, p0, Lu;->k:I

    return v0
.end method

.method public w()Lbb;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 877
    iget-object v0, p0, Lu;->M:Lbd;

    if-eqz v0, :cond_0

    .line 878
    iget-object v0, p0, Lu;->M:Lbd;

    .line 885
    :goto_0
    return-object v0

    .line 880
    :cond_0
    iget-object v0, p0, Lu;->t:Lz;

    if-nez v0, :cond_1

    .line 881
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 883
    :cond_1
    iput-boolean v3, p0, Lu;->Q:Z

    .line 884
    iget-object v0, p0, Lu;->t:Lz;

    iget-object v1, p0, Lu;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lu;->P:Z

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(Ljava/lang/String;ZZ)Lbd;

    move-result-object v0

    iput-object v0, p0, Lu;->M:Lbd;

    .line 885
    iget-object v0, p0, Lu;->M:Lbd;

    goto :goto_0
.end method

.method public x()Landroid/view/View;
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lu;->I:Landroid/view/View;

    return-object v0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 1160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lu;->F:Z

    .line 1161
    return-void
.end method
