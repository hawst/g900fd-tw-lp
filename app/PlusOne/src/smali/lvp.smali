.class public final Llvp;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Llvl;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Llvm;

.field private e:[Llvr;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16
    sget-object v0, Llvl;->a:[Llvl;

    iput-object v0, p0, Llvp;->a:[Llvl;

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Llvp;->d:Llvm;

    .line 22
    sget-object v0, Llvr;->a:[Llvr;

    iput-object v0, p0, Llvp;->e:[Llvr;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 61
    .line 62
    iget-object v0, p0, Llvp;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 63
    const/4 v0, 0x1

    iget-object v2, p0, Llvp;->b:Ljava/lang/String;

    .line 64
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 66
    :goto_0
    iget-object v2, p0, Llvp;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 67
    const/4 v2, 0x2

    iget-object v3, p0, Llvp;->c:Ljava/lang/String;

    .line 68
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 70
    :cond_0
    iget-object v2, p0, Llvp;->a:[Llvl;

    if-eqz v2, :cond_2

    .line 71
    iget-object v3, p0, Llvp;->a:[Llvl;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 72
    if-eqz v5, :cond_1

    .line 73
    const/4 v6, 0x3

    .line 74
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 71
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 78
    :cond_2
    iget-object v2, p0, Llvp;->d:Llvm;

    if-eqz v2, :cond_3

    .line 79
    const/4 v2, 0x4

    iget-object v3, p0, Llvp;->d:Llvm;

    .line 80
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_3
    iget-object v2, p0, Llvp;->e:[Llvr;

    if-eqz v2, :cond_5

    .line 83
    iget-object v2, p0, Llvp;->e:[Llvr;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 84
    if-eqz v4, :cond_4

    .line 85
    const/4 v5, 0x5

    .line 86
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 83
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 90
    :cond_5
    iget-object v1, p0, Llvp;->f:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 91
    const/4 v1, 0x6

    iget-object v2, p0, Llvp;->f:Ljava/lang/String;

    .line 92
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_6
    iget-object v1, p0, Llvp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    iput v0, p0, Llvp;->ai:I

    .line 96
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llvp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 104
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 105
    sparse-switch v0, :sswitch_data_0

    .line 109
    iget-object v2, p0, Llvp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 110
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llvp;->ah:Ljava/util/List;

    .line 113
    :cond_1
    iget-object v2, p0, Llvp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    :sswitch_0
    return-object p0

    .line 120
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llvp;->b:Ljava/lang/String;

    goto :goto_0

    .line 124
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llvp;->c:Ljava/lang/String;

    goto :goto_0

    .line 128
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 129
    iget-object v0, p0, Llvp;->a:[Llvl;

    if-nez v0, :cond_3

    move v0, v1

    .line 130
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llvl;

    .line 131
    iget-object v3, p0, Llvp;->a:[Llvl;

    if-eqz v3, :cond_2

    .line 132
    iget-object v3, p0, Llvp;->a:[Llvl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 134
    :cond_2
    iput-object v2, p0, Llvp;->a:[Llvl;

    .line 135
    :goto_2
    iget-object v2, p0, Llvp;->a:[Llvl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 136
    iget-object v2, p0, Llvp;->a:[Llvl;

    new-instance v3, Llvl;

    invoke-direct {v3}, Llvl;-><init>()V

    aput-object v3, v2, v0

    .line 137
    iget-object v2, p0, Llvp;->a:[Llvl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 138
    invoke-virtual {p1}, Loxn;->a()I

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 129
    :cond_3
    iget-object v0, p0, Llvp;->a:[Llvl;

    array-length v0, v0

    goto :goto_1

    .line 141
    :cond_4
    iget-object v2, p0, Llvp;->a:[Llvl;

    new-instance v3, Llvl;

    invoke-direct {v3}, Llvl;-><init>()V

    aput-object v3, v2, v0

    .line 142
    iget-object v2, p0, Llvp;->a:[Llvl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 146
    :sswitch_4
    iget-object v0, p0, Llvp;->d:Llvm;

    if-nez v0, :cond_5

    .line 147
    new-instance v0, Llvm;

    invoke-direct {v0}, Llvm;-><init>()V

    iput-object v0, p0, Llvp;->d:Llvm;

    .line 149
    :cond_5
    iget-object v0, p0, Llvp;->d:Llvm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 153
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 154
    iget-object v0, p0, Llvp;->e:[Llvr;

    if-nez v0, :cond_7

    move v0, v1

    .line 155
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Llvr;

    .line 156
    iget-object v3, p0, Llvp;->e:[Llvr;

    if-eqz v3, :cond_6

    .line 157
    iget-object v3, p0, Llvp;->e:[Llvr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 159
    :cond_6
    iput-object v2, p0, Llvp;->e:[Llvr;

    .line 160
    :goto_4
    iget-object v2, p0, Llvp;->e:[Llvr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 161
    iget-object v2, p0, Llvp;->e:[Llvr;

    new-instance v3, Llvr;

    invoke-direct {v3}, Llvr;-><init>()V

    aput-object v3, v2, v0

    .line 162
    iget-object v2, p0, Llvp;->e:[Llvr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 163
    invoke-virtual {p1}, Loxn;->a()I

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 154
    :cond_7
    iget-object v0, p0, Llvp;->e:[Llvr;

    array-length v0, v0

    goto :goto_3

    .line 166
    :cond_8
    iget-object v2, p0, Llvp;->e:[Llvr;

    new-instance v3, Llvr;

    invoke-direct {v3}, Llvr;-><init>()V

    aput-object v3, v2, v0

    .line 167
    iget-object v2, p0, Llvp;->e:[Llvr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 171
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llvp;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 105
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 29
    iget-object v1, p0, Llvp;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30
    const/4 v1, 0x1

    iget-object v2, p0, Llvp;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v1, p0, Llvp;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 33
    const/4 v1, 0x2

    iget-object v2, p0, Llvp;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 35
    :cond_1
    iget-object v1, p0, Llvp;->a:[Llvl;

    if-eqz v1, :cond_3

    .line 36
    iget-object v2, p0, Llvp;->a:[Llvl;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 37
    if-eqz v4, :cond_2

    .line 38
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 36
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 42
    :cond_3
    iget-object v1, p0, Llvp;->d:Llvm;

    if-eqz v1, :cond_4

    .line 43
    const/4 v1, 0x4

    iget-object v2, p0, Llvp;->d:Llvm;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 45
    :cond_4
    iget-object v1, p0, Llvp;->e:[Llvr;

    if-eqz v1, :cond_6

    .line 46
    iget-object v1, p0, Llvp;->e:[Llvr;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 47
    if-eqz v3, :cond_5

    .line 48
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 46
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 52
    :cond_6
    iget-object v0, p0, Llvp;->f:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 53
    const/4 v0, 0x6

    iget-object v1, p0, Llvp;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 55
    :cond_7
    iget-object v0, p0, Llvp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 57
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llvp;->a(Loxn;)Llvp;

    move-result-object v0

    return-object v0
.end method
