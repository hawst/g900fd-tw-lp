.class public final Lapr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lchb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lchb",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcgy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgy",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    new-instance v0, Laps;

    invoke-direct {v0}, Laps;-><init>()V

    sput-object v0, Lapr;->a:Lchb;

    .line 42
    new-instance v0, Lcgz;

    invoke-direct {v0}, Lcgz;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x1

    sget-object v3, Lapr;->a:Lchb;

    .line 43
    invoke-virtual {v0, v1, v2, v3}, Lcgz;->a(IILchb;)Lcgz;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcgz;->a()Lcgy;

    move-result-object v0

    sput-object v0, Lapr;->b:Lcgy;

    .line 42
    return-void
.end method

.method static synthetic a()Lcgy;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lapr;->b:Lcgy;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/concurrent/Executor;Laqx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    const-string v0, "context"

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 59
    const-string v0, "backgroundExecutor"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 60
    const-string v0, "settings"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 62
    new-instance v0, Lapt;

    invoke-direct {v0, p2, p0}, Lapt;-><init>(Laqx;Landroid/content/Context;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 71
    return-void
.end method
