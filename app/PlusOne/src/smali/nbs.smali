.class public final Lnbs;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lnbu;

.field private c:[Lnbt;

.field private d:Lnbw;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 625
    invoke-direct {p0}, Loxq;-><init>()V

    .line 634
    const/high16 v0, -0x80000000

    iput v0, p0, Lnbs;->a:I

    .line 637
    sget-object v0, Lnbt;->a:[Lnbt;

    iput-object v0, p0, Lnbs;->c:[Lnbt;

    .line 640
    iput-object v1, p0, Lnbs;->b:Lnbu;

    .line 643
    iput-object v1, p0, Lnbs;->d:Lnbw;

    .line 625
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 670
    .line 671
    iget v0, p0, Lnbs;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_4

    .line 672
    const/4 v0, 0x1

    iget v2, p0, Lnbs;->a:I

    .line 673
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 675
    :goto_0
    iget-object v2, p0, Lnbs;->b:Lnbu;

    if-eqz v2, :cond_0

    .line 676
    const/4 v2, 0x2

    iget-object v3, p0, Lnbs;->b:Lnbu;

    .line 677
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 679
    :cond_0
    iget-object v2, p0, Lnbs;->d:Lnbw;

    if-eqz v2, :cond_1

    .line 680
    const/4 v2, 0x3

    iget-object v3, p0, Lnbs;->d:Lnbw;

    .line 681
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 683
    :cond_1
    iget-object v2, p0, Lnbs;->c:[Lnbt;

    if-eqz v2, :cond_3

    .line 684
    iget-object v2, p0, Lnbs;->c:[Lnbt;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 685
    if-eqz v4, :cond_2

    .line 686
    const/4 v5, 0x4

    .line 687
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 684
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 691
    :cond_3
    iget-object v1, p0, Lnbs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 692
    iput v0, p0, Lnbs;->ai:I

    .line 693
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnbs;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 701
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 702
    sparse-switch v0, :sswitch_data_0

    .line 706
    iget-object v2, p0, Lnbs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 707
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnbs;->ah:Ljava/util/List;

    .line 710
    :cond_1
    iget-object v2, p0, Lnbs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 712
    :sswitch_0
    return-object p0

    .line 717
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 718
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 721
    :cond_2
    iput v0, p0, Lnbs;->a:I

    goto :goto_0

    .line 723
    :cond_3
    iput v1, p0, Lnbs;->a:I

    goto :goto_0

    .line 728
    :sswitch_2
    iget-object v0, p0, Lnbs;->b:Lnbu;

    if-nez v0, :cond_4

    .line 729
    new-instance v0, Lnbu;

    invoke-direct {v0}, Lnbu;-><init>()V

    iput-object v0, p0, Lnbs;->b:Lnbu;

    .line 731
    :cond_4
    iget-object v0, p0, Lnbs;->b:Lnbu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 735
    :sswitch_3
    iget-object v0, p0, Lnbs;->d:Lnbw;

    if-nez v0, :cond_5

    .line 736
    new-instance v0, Lnbw;

    invoke-direct {v0}, Lnbw;-><init>()V

    iput-object v0, p0, Lnbs;->d:Lnbw;

    .line 738
    :cond_5
    iget-object v0, p0, Lnbs;->d:Lnbw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 742
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 743
    iget-object v0, p0, Lnbs;->c:[Lnbt;

    if-nez v0, :cond_7

    move v0, v1

    .line 744
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnbt;

    .line 745
    iget-object v3, p0, Lnbs;->c:[Lnbt;

    if-eqz v3, :cond_6

    .line 746
    iget-object v3, p0, Lnbs;->c:[Lnbt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 748
    :cond_6
    iput-object v2, p0, Lnbs;->c:[Lnbt;

    .line 749
    :goto_2
    iget-object v2, p0, Lnbs;->c:[Lnbt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 750
    iget-object v2, p0, Lnbs;->c:[Lnbt;

    new-instance v3, Lnbt;

    invoke-direct {v3}, Lnbt;-><init>()V

    aput-object v3, v2, v0

    .line 751
    iget-object v2, p0, Lnbs;->c:[Lnbt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 752
    invoke-virtual {p1}, Loxn;->a()I

    .line 749
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 743
    :cond_7
    iget-object v0, p0, Lnbs;->c:[Lnbt;

    array-length v0, v0

    goto :goto_1

    .line 755
    :cond_8
    iget-object v2, p0, Lnbs;->c:[Lnbt;

    new-instance v3, Lnbt;

    invoke-direct {v3}, Lnbt;-><init>()V

    aput-object v3, v2, v0

    .line 756
    iget-object v2, p0, Lnbs;->c:[Lnbt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 702
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 648
    iget v0, p0, Lnbs;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 649
    const/4 v0, 0x1

    iget v1, p0, Lnbs;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 651
    :cond_0
    iget-object v0, p0, Lnbs;->b:Lnbu;

    if-eqz v0, :cond_1

    .line 652
    const/4 v0, 0x2

    iget-object v1, p0, Lnbs;->b:Lnbu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 654
    :cond_1
    iget-object v0, p0, Lnbs;->d:Lnbw;

    if-eqz v0, :cond_2

    .line 655
    const/4 v0, 0x3

    iget-object v1, p0, Lnbs;->d:Lnbw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 657
    :cond_2
    iget-object v0, p0, Lnbs;->c:[Lnbt;

    if-eqz v0, :cond_4

    .line 658
    iget-object v1, p0, Lnbs;->c:[Lnbt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 659
    if-eqz v3, :cond_3

    .line 660
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 658
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 664
    :cond_4
    iget-object v0, p0, Lnbs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 666
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 621
    invoke-virtual {p0, p1}, Lnbs;->a(Loxn;)Lnbs;

    move-result-object v0

    return-object v0
.end method
