.class public final Lbm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lbv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 815
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 816
    new-instance v0, Lbx;

    invoke-direct {v0}, Lbx;-><init>()V

    sput-object v0, Lbm;->a:Lbv;

    .line 832
    :goto_0
    return-void

    .line 817
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    .line 818
    new-instance v0, Lbw;

    invoke-direct {v0}, Lbw;-><init>()V

    sput-object v0, Lbm;->a:Lbv;

    goto :goto_0

    .line 819
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 820
    new-instance v0, Lcd;

    invoke-direct {v0}, Lcd;-><init>()V

    sput-object v0, Lbm;->a:Lbv;

    goto :goto_0

    .line 821
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 822
    new-instance v0, Lcc;

    invoke-direct {v0}, Lcc;-><init>()V

    sput-object v0, Lbm;->a:Lbv;

    goto :goto_0

    .line 823
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 824
    new-instance v0, Lcb;

    invoke-direct {v0}, Lcb;-><init>()V

    sput-object v0, Lbm;->a:Lbv;

    goto :goto_0

    .line 825
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 826
    new-instance v0, Lca;

    invoke-direct {v0}, Lca;-><init>()V

    sput-object v0, Lbm;->a:Lbv;

    goto :goto_0

    .line 827
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    .line 828
    new-instance v0, Lbz;

    invoke-direct {v0}, Lbz;-><init>()V

    sput-object v0, Lbm;->a:Lbv;

    goto :goto_0

    .line 830
    :cond_6
    new-instance v0, Lby;

    invoke-direct {v0}, Lby;-><init>()V

    sput-object v0, Lbm;->a:Lbv;

    goto :goto_0
.end method

.method static synthetic a()Lbv;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lbm;->a:Lbv;

    return-object v0
.end method

.method static synthetic a(Lbk;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbn;

    invoke-interface {p0, v0}, Lbk;->a(Lcl;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lbl;Lce;)V
    .locals 7

    .prologue
    .line 41
    if-eqz p1, :cond_0

    instance-of v0, p1, Lbr;

    if-eqz v0, :cond_1

    check-cast p1, Lbr;

    iget-object v0, p1, Lbr;->d:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Lbr;->f:Z

    iget-object v2, p1, Lbr;->e:Ljava/lang/CharSequence;

    iget-object v3, p1, Lbr;->a:Ljava/lang/CharSequence;

    invoke-static {p0, v0, v1, v2, v3}, Lcm;->a(Lbl;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lbu;

    if-eqz v0, :cond_2

    check-cast p1, Lbu;

    iget-object v0, p1, Lbu;->d:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Lbu;->f:Z

    iget-object v2, p1, Lbu;->e:Ljava/lang/CharSequence;

    iget-object v3, p1, Lbu;->a:Ljava/util/ArrayList;

    invoke-static {p0, v0, v1, v2, v3}, Lcm;->a(Lbl;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lbq;

    if-eqz v0, :cond_0

    check-cast p1, Lbq;

    iget-object v1, p1, Lbq;->d:Ljava/lang/CharSequence;

    iget-boolean v2, p1, Lbq;->f:Z

    iget-object v3, p1, Lbq;->e:Ljava/lang/CharSequence;

    iget-object v4, p1, Lbq;->a:Landroid/graphics/Bitmap;

    iget-object v5, p1, Lbq;->b:Landroid/graphics/Bitmap;

    iget-boolean v6, p1, Lbq;->c:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcm;->a(Lbl;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    goto :goto_0
.end method
