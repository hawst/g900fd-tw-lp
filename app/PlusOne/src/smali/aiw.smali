.class public final Laiw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Laix;

.field private final b:Lahx;


# direct methods
.method public constructor <init>(Lahx;)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput-object p1, p0, Laiw;->b:Lahx;

    .line 151
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 260
    iget-object v0, p0, Laiw;->a:Laix;

    const-string v1, "analysisState"

    invoke-static {v0, v1, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laix;

    .line 261
    iget-object v1, v0, Laix;->f:Laiz;

    iget-object v2, v0, Laix;->d:Ljava/util/List;

    iget-object v3, v0, Laix;->e:Ljava/util/List;

    iget-object v0, v0, Laix;->a:Laic;

    invoke-interface {v1, v2, v3}, Laiz;->a(Ljava/util/List;Ljava/util/List;)V

    .line 265
    iput-object v4, p0, Laiw;->a:Laix;

    .line 266
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 184
    iget-object v0, p0, Laiw;->a:Laix;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->j:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-object v1, p0, Laiw;->a:Laix;

    .line 190
    iget-object v0, v1, Laix;->i:Ljava/util/List;

    new-instance v2, Lajc;

    iget-object v3, v1, Laix;->j:Landroid/net/Uri;

    new-instance v4, Laja;

    invoke-direct {v4}, Laja;-><init>()V

    invoke-direct {v2, v3, v6, v4}, Lajc;-><init>(Landroid/net/Uri;Lbkr;Ljava/lang/Exception;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v0, p0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 195
    iget-object v3, v1, Laix;->d:Ljava/util/List;

    new-instance v4, Lajc;

    new-instance v5, Laja;

    invoke-direct {v5}, Laja;-><init>()V

    invoke-direct {v4, v0, v6, v5}, Lajc;-><init>(Landroid/net/Uri;Lbkr;Ljava/lang/Exception;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 197
    :cond_2
    iget-object v0, p0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 198
    iget-object v3, v1, Laix;->e:Ljava/util/List;

    new-instance v4, Lajc;

    new-instance v5, Laja;

    invoke-direct {v5}, Laja;-><init>()V

    invoke-direct {v4, v0, v6, v5}, Lajc;-><init>(Landroid/net/Uri;Lbkr;Ljava/lang/Exception;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 201
    :cond_3
    iget-object v0, v1, Laix;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 202
    iget-object v0, v1, Laix;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 205
    iput-object v6, v1, Laix;->j:Landroid/net/Uri;

    .line 207
    iget-object v0, p0, Laiw;->b:Lahx;

    invoke-interface {v0}, Lahx;->a()V

    .line 209
    invoke-direct {p0}, Laiw;->c()V

    goto :goto_0
.end method

.method public a(Ljava/util/List;Ljava/util/List;Laic;Laiz;Lajb;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Laic;",
            "Laiz;",
            "Lajb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Laiw;->a:Laix;

    const-string v1, "mAnalysisState"

    const-string v2, "analysis already in progress"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 170
    new-instance v0, Laix;

    new-instance v6, Laiy;

    invoke-direct {v6, p0}, Laiy;-><init>(Laiw;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Laix;-><init>(Ljava/util/List;Ljava/util/List;Laic;Laiz;Lajb;Laiy;)V

    iput-object v0, p0, Laiw;->a:Laix;

    .line 172
    invoke-virtual {p0}, Laiw;->b()V

    .line 173
    return-void
.end method

.method b()V
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Laiw;->a:Laix;

    const-string v1, "analysisState"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laix;

    .line 230
    iget-object v1, v0, Laix;->j:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 231
    iget-object v1, v0, Laix;->g:Lajb;

    iget-object v2, v0, Laix;->j:Landroid/net/Uri;

    invoke-interface {v1, v2}, Lajb;->a(Landroid/net/Uri;)V

    .line 234
    :cond_0
    iget-object v1, v0, Laix;->b:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 235
    iget-object v1, v0, Laix;->b:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 236
    iput-object v1, v0, Laix;->j:Landroid/net/Uri;

    .line 237
    iget-object v2, v0, Laix;->g:Lajb;

    .line 238
    iget-object v2, p0, Laiw;->b:Lahx;

    iget-object v3, v0, Laix;->a:Laic;

    iget-object v0, v0, Laix;->h:Lahy;

    invoke-interface {v2, v1, v3, v0}, Lahx;->a(Landroid/net/Uri;Laic;Lahy;)V

    .line 254
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v1, v0, Laix;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 243
    iget-object v1, v0, Laix;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 244
    iget-object v2, p0, Laiw;->a:Laix;

    iget-object v2, v2, Laix;->e:Ljava/util/List;

    iput-object v2, v0, Laix;->i:Ljava/util/List;

    .line 245
    iput-object v1, v0, Laix;->j:Landroid/net/Uri;

    .line 246
    iget-object v0, v0, Laix;->g:Lajb;

    .line 247
    iget-object v0, p0, Laiw;->b:Lahx;

    iget-object v2, p0, Laiw;->a:Laix;

    iget-object v2, v2, Laix;->a:Laic;

    iget-object v3, p0, Laiw;->a:Laix;

    iget-object v3, v3, Laix;->h:Lahy;

    invoke-interface {v0, v1, v2, v3}, Lahx;->b(Landroid/net/Uri;Laic;Lahy;)V

    goto :goto_0

    .line 252
    :cond_2
    invoke-direct {p0}, Laiw;->c()V

    goto :goto_0
.end method
