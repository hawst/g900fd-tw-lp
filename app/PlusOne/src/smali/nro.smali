.class public final Lnro;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 679
    invoke-direct {p0}, Loxq;-><init>()V

    .line 684
    const/high16 v0, -0x80000000

    iput v0, p0, Lnro;->b:I

    .line 679
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 701
    const/4 v0, 0x0

    .line 702
    iget-object v1, p0, Lnro;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 703
    const/4 v0, 0x1

    iget-object v1, p0, Lnro;->a:Ljava/lang/String;

    .line 704
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 706
    :cond_0
    iget v1, p0, Lnro;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 707
    const/4 v1, 0x2

    iget v2, p0, Lnro;->b:I

    .line 708
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 710
    :cond_1
    iget-object v1, p0, Lnro;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 711
    iput v0, p0, Lnro;->ai:I

    .line 712
    return v0
.end method

.method public a(Loxn;)Lnro;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 720
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 721
    sparse-switch v0, :sswitch_data_0

    .line 725
    iget-object v1, p0, Lnro;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 726
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnro;->ah:Ljava/util/List;

    .line 729
    :cond_1
    iget-object v1, p0, Lnro;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 731
    :sswitch_0
    return-object p0

    .line 736
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnro;->a:Ljava/lang/String;

    goto :goto_0

    .line 740
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 741
    if-eq v0, v2, :cond_2

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-ne v0, v1, :cond_3

    .line 758
    :cond_2
    iput v0, p0, Lnro;->b:I

    goto :goto_0

    .line 760
    :cond_3
    iput v2, p0, Lnro;->b:I

    goto :goto_0

    .line 721
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 689
    iget-object v0, p0, Lnro;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 690
    const/4 v0, 0x1

    iget-object v1, p0, Lnro;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 692
    :cond_0
    iget v0, p0, Lnro;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 693
    const/4 v0, 0x2

    iget v1, p0, Lnro;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 695
    :cond_1
    iget-object v0, p0, Lnro;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 697
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 675
    invoke-virtual {p0, p1}, Lnro;->a(Loxn;)Lnro;

    move-result-object v0

    return-object v0
.end method
