.class public abstract Lbvt;
.super Landroid/app/IntentService;
.source "PG"


# instance fields
.field private a:Lbwe;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method


# virtual methods
.method public abstract a()Lbwe;
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lbvt;->a()Lbwe;

    move-result-object v0

    iput-object v0, p0, Lbvt;->a:Lbwe;

    .line 23
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 24
    iget-object v0, p0, Lbvt;->a:Lbwe;

    invoke-interface {v0}, Lbwe;->a()V

    .line 25
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lbvt;->a:Lbwe;

    invoke-interface {v0}, Lbwe;->b()V

    .line 30
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 31
    return-void
.end method

.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 36
    if-eqz p1, :cond_0

    .line 37
    iget-object v0, p0, Lbvt;->a:Lbwe;

    invoke-interface {v0, p1}, Lbwe;->a(Landroid/content/Intent;)V

    .line 39
    :cond_0
    return-void
.end method
