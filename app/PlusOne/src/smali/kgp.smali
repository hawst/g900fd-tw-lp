.class public final Lkgp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llqj;
.implements Llre;
.implements Llrg;


# instance fields
.field final a:Lkgr;

.field private b:Lkij;

.field private c:Lkhr;

.field private d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lkgr;Llqr;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lkgp;->a:Lkgr;

    .line 105
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 106
    return-void
.end method


# virtual methods
.method public a(Lkhl;)Lkhl;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lkgp;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->c(Lkhl;)Z

    .line 139
    return-object p1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lkgp;->a:Lkgr;

    invoke-interface {v0}, Lkgr;->a()V

    .line 132
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 110
    const-class v0, Lkij;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkij;

    iput-object v0, p0, Lkgp;->b:Lkij;

    .line 111
    iput-object p1, p0, Lkgp;->e:Landroid/content/Context;

    .line 112
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lkgp;->b:Lkij;

    invoke-interface {v0}, Lkij;->a()Lcom/google/android/libraries/social/settings/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lkgp;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    .line 122
    new-instance v0, Lkhr;

    iget-object v1, p0, Lkgp;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkgp;->c:Lkhr;

    .line 123
    return-void
.end method

.method public b(Lkhl;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkgp;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->d(Lkhl;)Z

    .line 147
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lkgp;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    const-string v1, "CloudPreferencesManager.on_load_error"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->a(Ljava/lang/CharSequence;)Lkhl;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_0

    .line 167
    iget-object v1, p0, Lkgp;->b:Lkij;

    invoke-interface {v1}, Lkij;->a()Lcom/google/android/libraries/social/settings/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->d(Lkhl;)Z

    .line 169
    :cond_0
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 177
    iget-object v0, p0, Lkgp;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    const-string v1, "CloudPreferencesManager.on_load_error"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->a(Ljava/lang/CharSequence;)Lkhl;

    move-result-object v0

    .line 178
    if-nez v0, :cond_0

    .line 179
    iget-object v1, p0, Lkgp;->c:Lkhr;

    const/4 v2, 0x0

    iget-object v0, p0, Lkgp;->a:Lkgr;

    check-cast v0, Lu;

    const v3, 0x7f0a056c

    .line 180
    invoke-virtual {v0, v3}, Lu;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 179
    invoke-virtual {v1, v2, v0}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 181
    const-string v1, "CloudPreferencesManager.on_load_error"

    invoke-virtual {v0, v1}, Lkhl;->d(Ljava/lang/String;)V

    .line 182
    new-instance v1, Lkgq;

    invoke-direct {v1, p0}, Lkgq;-><init>(Lkgp;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 189
    iget-object v1, p0, Lkgp;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->c(Lkhl;)Z

    .line 191
    :cond_0
    return-void
.end method
