.class public final Lamo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lamm;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private A:Lbgf;

.field private B:Lbqo;

.field private C:Lbqk;

.field private D:Lbig;

.field private E:Lbks;

.field private F:Laro;

.field private G:Laqa;

.field private H:Laxu;

.field private I:Lavj;

.field private J:Laxp;

.field private K:Lced;

.field private L:[Lajw;

.field private M:[Lajw;

.field private N:Ljdw;

.field private O:[Lajj;

.field private P:Ljfb;

.field private Q:Lbyf;

.field private R:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

.field private S:Lahw;

.field private T:Lali;

.field private U:Lbjf;

.field private V:Lbjf;

.field private W:Lbrh;

.field private X:Llmg;

.field private Y:Llmg;

.field private final c:Landroid/content/Context;

.field private final d:Lald;

.field private final e:Lamy;

.field private final f:Lakq;

.field private final g:Lanh;

.field private final h:Lasy;

.field private final i:Lcdu;

.field private final j:Lawy;

.field private final k:Ljava/io/File;

.field private l:Lchk;

.field private m:Ljava/util/concurrent/Executor;

.field private n:Ljava/util/concurrent/Executor;

.field private o:Ljava/util/concurrent/Executor;

.field private p:Ljava/util/concurrent/Executor;

.field private q:Lalg;

.field private r:Ljava/util/concurrent/Executor;

.field private s:Lalg;

.field private t:Laqx;

.field private u:Lajt;

.field private v:Lajl;

.field private w:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lbvq;",
            ">;"
        }
    .end annotation
.end field

.field private x:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lbjl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjl",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    const/high16 v0, 0x1000000

    sput v0, Lamo;->a:I

    .line 148
    const/high16 v0, 0x400000

    sput v0, Lamo;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    new-instance v0, Lamv;

    invoke-direct {v0, p0}, Lamv;-><init>(Lamo;)V

    iput-object v0, p0, Lamo;->j:Lawy;

    .line 263
    const-string v0, "context"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 265
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lamo;->c:Landroid/content/Context;

    .line 266
    invoke-static {p1}, Lald;->a(Landroid/content/Context;)Lald;

    move-result-object v0

    iput-object v0, p0, Lamo;->d:Lald;

    .line 267
    new-instance v1, Lamy;

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sget-object v3, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v5, v4, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v0, v5

    iget v5, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v5, v5

    iget v4, v4, Landroid/util/DisplayMetrics;->ydpi:F

    div-float v4, v5, v4

    mul-float/2addr v0, v0

    mul-float/2addr v4, v4

    add-float/2addr v0, v4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lamy;-><init>(Ljava/lang/String;Ljava/lang/String;D)V

    iput-object v1, p0, Lamo;->e:Lamy;

    .line 268
    new-instance v0, Lakq;

    new-instance v1, Ljava/io/File;

    .line 269
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "aps"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lakq;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lamo;->f:Lakq;

    .line 271
    new-instance v0, Lanh;

    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    .line 272
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lamo;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lanh;-><init>(Landroid/content/ContentResolver;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lamo;->g:Lanh;

    .line 274
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "aamlogs"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lamo;->k:Ljava/io/File;

    .line 275
    iget-object v0, p0, Lamo;->k:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 277
    new-instance v0, Lasy;

    iget-object v1, p0, Lamo;->f:Lakq;

    iget-object v2, p0, Lamo;->g:Lanh;

    invoke-direct {v0, v1, v2}, Lasy;-><init>(Livo;Latd;)V

    iput-object v0, p0, Lamo;->h:Lasy;

    .line 280
    iget-object v0, p0, Lamo;->h:Lasy;

    new-instance v1, Livf;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lamo;->k:Ljava/io/File;

    const-string v4, "PluggedInLog"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Livf;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lasy;->a(Livf;)V

    .line 282
    iget-object v0, p0, Lamo;->h:Lasy;

    new-instance v1, Livf;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lamo;->k:Ljava/io/File;

    const-string v4, "PostCaptureLog"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Livf;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lasy;->a(Livf;)V

    .line 285
    iget-object v0, p0, Lamo;->h:Lasy;

    new-instance v1, Livf;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lamo;->k:Ljava/io/File;

    const-string v4, "PostSyncLog"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Livf;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lasy;->a(Livf;)V

    .line 287
    iget-object v0, p0, Lamo;->h:Lasy;

    new-instance v1, Livf;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lamo;->k:Ljava/io/File;

    const-string v4, "ClusteringLog"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Livf;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lasy;->a(Livf;)V

    .line 289
    iget-object v0, p0, Lamo;->h:Lasy;

    new-instance v1, Livf;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lamo;->k:Ljava/io/File;

    const-string v4, "AamEventsLog"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Livf;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lasy;->a(Livf;)V

    .line 292
    const-string v0, "sessionStats"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 294
    new-instance v1, Lcdu;

    iget-object v2, p0, Lamo;->e:Lamy;

    new-instance v3, Lcfa;

    invoke-direct {v3, p1}, Lcfa;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v0, v2, v3}, Lcdu;-><init>(Landroid/content/SharedPreferences;Lamy;Lcdt;)V

    iput-object v1, p0, Lamo;->i:Lcdu;

    .line 298
    invoke-virtual {p0}, Lamo;->B()Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->a()V

    .line 299
    return-void
.end method

.method static synthetic a(Lamo;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public A()Lbzf;
    .locals 4

    .prologue
    .line 738
    new-instance v0, Lbzj;

    new-instance v1, Lbzl;

    .line 739
    invoke-virtual {p0}, Lamo;->W()Ljava/util/concurrent/Future;

    move-result-object v2

    sget-object v3, Lbzc;->c:Lbzc;

    invoke-direct {v1, v2, v3}, Lbzl;-><init>(Ljava/util/concurrent/Future;Lbzc;)V

    invoke-direct {v0, v1}, Lbzj;-><init>(Lbzf;)V

    return-object v0
.end method

.method public B()Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;
    .locals 6

    .prologue
    .line 814
    iget-object v0, p0, Lamo;->R:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    if-nez v0, :cond_0

    .line 815
    new-instance v0, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    iget-object v2, p0, Lamo;->c:Landroid/content/Context;

    .line 817
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 818
    invoke-virtual {p0}, Lamo;->p()Lanh;

    move-result-object v3

    .line 819
    invoke-virtual {p0}, Lamo;->L()Ljfb;

    move-result-object v4

    .line 820
    invoke-virtual {p0}, Lamo;->g()Ljava/util/concurrent/Executor;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Lanh;Ljfb;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lamo;->R:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    .line 822
    :cond_0
    iget-object v0, p0, Lamo;->R:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    return-object v0
.end method

.method public C()Laro;
    .locals 5

    .prologue
    .line 835
    iget-object v0, p0, Lamo;->F:Laro;

    if-nez v0, :cond_0

    .line 836
    new-instance v0, Lamx;

    new-instance v1, Ljava/io/File;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    .line 838
    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lamo;->c:Landroid/content/Context;

    const v4, 0x7f0a00d2

    .line 840
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iget-object v2, p0, Lamo;->c:Landroid/content/Context;

    const v3, 0x7f0a00d0

    .line 841
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lamo;->c:Landroid/content/Context;

    const v4, 0x7f0a00d1

    .line 842
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lamx;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lamo;->F:Laro;

    .line 844
    :cond_0
    iget-object v0, p0, Lamo;->F:Laro;

    return-object v0
.end method

.method public D()Laqa;
    .locals 3

    .prologue
    .line 849
    iget-object v0, p0, Lamo;->G:Laqa;

    if-nez v0, :cond_0

    .line 850
    new-instance v1, Lamw;

    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    const-string v2, "notification"

    .line 851
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lamw;-><init>(Landroid/app/NotificationManager;)V

    iput-object v1, p0, Lamo;->G:Laqa;

    .line 853
    :cond_0
    iget-object v0, p0, Lamo;->G:Laqa;

    return-object v0
.end method

.method public E()Laxu;
    .locals 3

    .prologue
    .line 858
    iget-object v0, p0, Lamo;->H:Laxu;

    if-nez v0, :cond_0

    .line 859
    new-instance v1, Laxu;

    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    const-string v2, "connectivity"

    .line 860
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-direct {v1, v0}, Laxu;-><init>(Landroid/net/ConnectivityManager;)V

    iput-object v1, p0, Lamo;->H:Laxu;

    .line 862
    :cond_0
    iget-object v0, p0, Lamo;->H:Laxu;

    return-object v0
.end method

.method public F()Lced;
    .locals 3

    .prologue
    .line 867
    iget-object v0, p0, Lamo;->K:Lced;

    if-nez v0, :cond_0

    .line 868
    new-instance v1, Lced;

    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    const-string v2, "audio"

    .line 869
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-direct {v1, v0}, Lced;-><init>(Landroid/media/AudioManager;)V

    iput-object v1, p0, Lamo;->K:Lced;

    .line 871
    :cond_0
    iget-object v0, p0, Lamo;->K:Lced;

    return-object v0
.end method

.method public G()[Lajj;
    .locals 3

    .prologue
    .line 876
    iget-object v0, p0, Lamo;->O:[Lajj;

    if-nez v0, :cond_0

    .line 877
    const/4 v0, 0x1

    new-array v0, v0, [Lajj;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/moviemaker/analyzer/postprocess/StabilizationPostProcessor;

    invoke-direct {v2}, Lcom/google/android/apps/moviemaker/analyzer/postprocess/StabilizationPostProcessor;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lamo;->O:[Lajj;

    .line 881
    :cond_0
    iget-object v0, p0, Lamo;->O:[Lajj;

    return-object v0
.end method

.method public H()[Lajj;
    .locals 1

    .prologue
    .line 886
    sget-object v0, Lajj;->a:[Lajj;

    return-object v0
.end method

.method public I()[Lajw;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 891
    iget-object v0, p0, Lamo;->L:[Lajw;

    if-nez v0, :cond_0

    .line 892
    const/4 v0, 0x4

    new-array v0, v0, [Lajw;

    const/4 v1, 0x0

    new-instance v2, Lakw;

    invoke-direct {v2}, Lakw;-><init>()V

    aput-object v2, v0, v1

    new-instance v1, Lakx;

    invoke-direct {v1}, Lakx;-><init>()V

    aput-object v1, v0, v3

    const/4 v1, 0x2

    new-instance v2, Lala;

    invoke-direct {v2}, Lala;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Laky;

    invoke-direct {v2, v3}, Laky;-><init>(Z)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lamo;->L:[Lajw;

    .line 899
    :cond_0
    iget-object v0, p0, Lamo;->L:[Lajw;

    return-object v0
.end method

.method public J()[Lajw;
    .locals 3

    .prologue
    .line 904
    iget-object v0, p0, Lamo;->M:[Lajw;

    if-nez v0, :cond_0

    .line 905
    const/4 v0, 0x1

    new-array v0, v0, [Lajw;

    const/4 v1, 0x0

    new-instance v2, Lalc;

    invoke-direct {v2}, Lalc;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lamo;->M:[Lajw;

    .line 907
    :cond_0
    iget-object v0, p0, Lamo;->M:[Lajw;

    return-object v0
.end method

.method public K()Ljdw;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 912
    iget-object v0, p0, Lamo;->N:Ljdw;

    if-nez v0, :cond_0

    .line 913
    iget-object v0, p0, Lamo;->g:Lanh;

    invoke-virtual {v0}, Lanh;->V()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 914
    iget-object v0, p0, Lamo;->g:Lanh;

    invoke-virtual {v0}, Lanh;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 915
    new-instance v0, Lasw;

    const/4 v1, 0x2

    new-array v1, v1, [Livc;

    new-instance v2, Livb;

    const-string v3, "AamEvents"

    invoke-direct {v2, v3}, Livb;-><init>(Ljava/lang/String;)V

    aput-object v2, v1, v4

    const-string v2, "AamEventsLog"

    .line 917
    invoke-virtual {p0, v2}, Lamo;->a(Ljava/lang/String;)Livc;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-direct {v0, v1}, Lasw;-><init>([Livc;)V

    iput-object v0, p0, Lamo;->N:Ljdw;

    .line 928
    :cond_0
    :goto_0
    iget-object v0, p0, Lamo;->N:Ljdw;

    return-object v0

    .line 920
    :cond_1
    new-instance v0, Lasw;

    new-array v1, v5, [Livc;

    const-string v2, "AamEventsLog"

    .line 921
    invoke-virtual {p0, v2}, Lamo;->a(Ljava/lang/String;)Livc;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lasw;-><init>([Livc;)V

    iput-object v0, p0, Lamo;->N:Ljdw;

    goto :goto_0

    .line 925
    :cond_2
    new-instance v0, Ljef;

    invoke-direct {v0}, Ljef;-><init>()V

    iput-object v0, p0, Lamo;->N:Ljdw;

    goto :goto_0
.end method

.method public L()Ljfb;
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lamo;->P:Ljfb;

    if-nez v0, :cond_0

    .line 943
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    iput-object v0, p0, Lamo;->P:Ljfb;

    .line 945
    :cond_0
    iget-object v0, p0, Lamo;->P:Ljfb;

    return-object v0
.end method

.method public M()Lbrh;
    .locals 6

    .prologue
    .line 989
    iget-object v0, p0, Lamo;->W:Lbrh;

    if-nez v0, :cond_0

    .line 990
    new-instance v0, Lbrh;

    invoke-virtual {p0}, Lamo;->p()Lanh;

    move-result-object v1

    new-instance v2, Lbrb;

    const v3, 0x989680

    iget-object v4, p0, Lamo;->c:Landroid/content/Context;

    const-string v5, "MusicCache"

    invoke-direct {v2, v3, v4, v5}, Lbrb;-><init>(ILandroid/content/Context;Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lamo;->c:Landroid/content/Context;

    .line 993
    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "soundtrack_list"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lbrh;-><init>(Lbri;Lbrb;Ljava/io/File;)V

    iput-object v0, p0, Lamo;->W:Lbrh;

    .line 995
    :cond_0
    iget-object v0, p0, Lamo;->W:Lbrh;

    return-object v0
.end method

.method public N()Ljava/io/File;
    .locals 3

    .prologue
    .line 1000
    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    const-string v1, "AssetCache"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public O()Lahw;
    .locals 4

    .prologue
    .line 950
    iget-object v0, p0, Lamo;->S:Lahw;

    if-nez v0, :cond_0

    .line 951
    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    const-string v1, "batteryAnalysis"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 953
    new-instance v1, Lahu;

    invoke-direct {v1, v0}, Lahu;-><init>(Landroid/content/SharedPreferences;)V

    .line 955
    new-instance v0, Laht;

    .line 956
    invoke-virtual {p0}, Lamo;->p()Lanh;

    move-result-object v2

    invoke-static {}, Lcgt;->a()Lcgt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Laht;-><init>(Lahv;Lanh;Lcgt;)V

    iput-object v0, p0, Lamo;->S:Lahw;

    .line 958
    :cond_0
    iget-object v0, p0, Lamo;->S:Lahw;

    return-object v0
.end method

.method public P()Lbjf;
    .locals 1

    .prologue
    .line 971
    iget-object v0, p0, Lamo;->U:Lbjf;

    if-nez v0, :cond_0

    .line 972
    new-instance v0, Lbjf;

    invoke-direct {v0}, Lbjf;-><init>()V

    iput-object v0, p0, Lamo;->U:Lbjf;

    .line 974
    :cond_0
    iget-object v0, p0, Lamo;->U:Lbjf;

    return-object v0
.end method

.method public Q()Lbjf;
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lamo;->V:Lbjf;

    if-nez v0, :cond_0

    .line 982
    new-instance v0, Lbjf;

    invoke-direct {v0}, Lbjf;-><init>()V

    iput-object v0, p0, Lamo;->V:Lbjf;

    .line 984
    :cond_0
    iget-object v0, p0, Lamo;->V:Lbjf;

    return-object v0
.end method

.method public R()Lali;
    .locals 2

    .prologue
    .line 963
    iget-object v0, p0, Lamo;->T:Lali;

    if-nez v0, :cond_0

    .line 964
    new-instance v0, Lali;

    invoke-virtual {p0}, Lamo;->L()Ljfb;

    move-result-object v1

    invoke-direct {v0, v1}, Lali;-><init>(Ljfb;)V

    iput-object v0, p0, Lamo;->T:Lali;

    .line 966
    :cond_0
    iget-object v0, p0, Lamo;->T:Lali;

    return-object v0
.end method

.method public S()Llmg;
    .locals 8

    .prologue
    .line 1010
    iget-object v0, p0, Lamo;->X:Llmg;

    if-nez v0, :cond_0

    .line 1011
    new-instance v1, Llmg;

    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    .line 1012
    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v3, "MovieMakerChunkCache"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/high16 v3, 0x40000

    sget-wide v4, Larp;->a:J

    .line 1015
    invoke-static {}, Llmg;->d()Llmm;

    move-result-object v6

    .line 1016
    invoke-static {}, Llmg;->c()Llml;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Llmg;-><init>(Ljava/io/File;IJLlmm;Llml;)V

    iput-object v1, p0, Lamo;->X:Llmg;

    .line 1018
    :cond_0
    iget-object v0, p0, Lamo;->X:Llmg;

    return-object v0
.end method

.method public T()Llmg;
    .locals 8

    .prologue
    .line 1023
    iget-object v0, p0, Lamo;->Y:Llmg;

    if-nez v0, :cond_0

    .line 1024
    new-instance v1, Llmg;

    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    .line 1025
    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v3, "MovieMakerFirstChunkCache"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/high16 v3, 0x40000

    sget-wide v4, Larp;->b:J

    .line 1028
    invoke-static {}, Llmg;->d()Llmm;

    move-result-object v6

    .line 1029
    invoke-static {}, Llmg;->c()Llml;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Llmg;-><init>(Ljava/io/File;IJLlmm;Llml;)V

    iput-object v1, p0, Lamo;->Y:Llmg;

    .line 1031
    :cond_0
    iget-object v0, p0, Lamo;->Y:Llmg;

    return-object v0
.end method

.method public U()Lcfg;
    .locals 1

    .prologue
    .line 1005
    sget-object v0, Lcer;->a:Lcfg;

    return-object v0
.end method

.method public V()Lbqk;
    .locals 4

    .prologue
    .line 549
    iget-object v0, p0, Lamo;->C:Lbqk;

    if-nez v0, :cond_0

    .line 550
    new-instance v0, Lbqk;

    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    .line 552
    invoke-virtual {p0}, Lamo;->y()Lbjp;

    move-result-object v2

    .line 553
    invoke-virtual {p0}, Lamo;->v()Lbig;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lbqk;-><init>(Landroid/content/Context;Lbjp;Lbig;)V

    iput-object v0, p0, Lamo;->C:Lbqk;

    .line 555
    :cond_0
    iget-object v0, p0, Lamo;->C:Lbqk;

    return-object v0
.end method

.method public W()Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lbvq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 721
    iget-object v0, p0, Lamo;->w:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    .line 722
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lams;

    invoke-direct {v1, p0}, Lams;-><init>(Lamo;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 730
    invoke-virtual {p0}, Lamo;->h()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 731
    iput-object v0, p0, Lamo;->w:Ljava/util/concurrent/Future;

    .line 733
    :cond_0
    iget-object v0, p0, Lamo;->w:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public a()Lald;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lamo;->d:Lald;

    return-object v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/String;)Lchg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Lchg;"
        }
    .end annotation

    .prologue
    .line 711
    new-instance v0, Lchg;

    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lchg;-><init>(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/Class;)Lchl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lchl;"
        }
    .end annotation

    .prologue
    .line 716
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lamo;->a(Ljava/lang/Class;Ljava/lang/String;)Lchg;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Livc;
    .locals 5

    .prologue
    .line 933
    new-instance v0, Livg;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lamo;->k:Ljava/io/File;

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/16 v2, 0x64

    const/16 v3, 0xa

    .line 937
    invoke-virtual {p0}, Lamo;->g()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Livg;-><init>(Ljava/io/File;IILjava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public b()Lamy;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lamo;->e:Lamy;

    return-object v0
.end method

.method public c()Lasy;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lamo;->h:Lasy;

    return-object v0
.end method

.method public d()Lakq;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lamo;->f:Lakq;

    return-object v0
.end method

.method public e()Lchk;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lamo;->l:Lchk;

    if-nez v0, :cond_0

    .line 324
    new-instance v0, Lamp;

    invoke-direct {v0, p0}, Lamp;-><init>(Lamo;)V

    iput-object v0, p0, Lamo;->l:Lchk;

    .line 346
    :cond_0
    iget-object v0, p0, Lamo;->l:Lchk;

    return-object v0
.end method

.method public f()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lamo;->m:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    .line 352
    new-instance v0, Lchj;

    invoke-direct {v0}, Lchj;-><init>()V

    iput-object v0, p0, Lamo;->m:Ljava/util/concurrent/Executor;

    .line 354
    :cond_0
    iget-object v0, p0, Lamo;->m:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public g()Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    .line 359
    iget-object v0, p0, Lamo;->n:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    .line 360
    invoke-virtual {p0}, Lamo;->e()Lchk;

    move-result-object v0

    const-class v1, Lamo;

    const-string v2, "BackgroundThreadExecutor"

    invoke-interface {v0, v1, v2}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lamo;->n:Ljava/util/concurrent/Executor;

    .line 363
    :cond_0
    iget-object v0, p0, Lamo;->n:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public h()Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    .line 368
    iget-object v0, p0, Lamo;->o:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    .line 369
    invoke-virtual {p0}, Lamo;->e()Lchk;

    move-result-object v0

    const-class v1, Lamo;

    const-string v2, "BackgroundThreadPoolExecutor"

    invoke-interface {v0, v1, v2}, Lchk;->b(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lamo;->o:Ljava/util/concurrent/Executor;

    .line 372
    :cond_0
    iget-object v0, p0, Lamo;->o:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public i()Lalg;
    .locals 4

    .prologue
    .line 385
    iget-object v0, p0, Lamo;->q:Lalg;

    if-nez v0, :cond_1

    .line 386
    new-instance v0, Lang;

    iget-object v1, p0, Lamo;->p:Ljava/util/concurrent/Executor;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lamo;->e()Lchk;

    move-result-object v1

    const-class v2, Lamo;

    const-string v3, "SerialAsyncTaskExecutor"

    invoke-interface {v1, v2, v3}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lamo;->p:Ljava/util/concurrent/Executor;

    :cond_0
    iget-object v1, p0, Lamo;->p:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1}, Lang;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lamo;->q:Lalg;

    .line 388
    :cond_1
    iget-object v0, p0, Lamo;->q:Lalg;

    return-object v0
.end method

.method public j()Lalg;
    .locals 4

    .prologue
    .line 401
    iget-object v0, p0, Lamo;->s:Lalg;

    if-nez v0, :cond_1

    .line 402
    new-instance v0, Lang;

    iget-object v1, p0, Lamo;->r:Ljava/util/concurrent/Executor;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lamo;->e()Lchk;

    move-result-object v1

    const-class v2, Lamo;

    const-string v3, "NetworkBackgroundThreadExecutor"

    invoke-interface {v1, v2, v3}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lamo;->r:Ljava/util/concurrent/Executor;

    :cond_0
    iget-object v1, p0, Lamo;->r:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1}, Lang;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lamo;->s:Lalg;

    .line 404
    :cond_1
    iget-object v0, p0, Lamo;->s:Lalg;

    return-object v0
.end method

.method public k()Lcdu;
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lamo;->i:Lcdu;

    return-object v0
.end method

.method public l()Lavj;
    .locals 3

    .prologue
    .line 749
    iget-object v0, p0, Lamo;->I:Lavj;

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lamo;->I:Lavj;

    .line 774
    :goto_0
    return-object v0

    .line 752
    :cond_0
    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    const-string v1, "soundtrackUsage"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 754
    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    .line 755
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 757
    new-instance v2, Lamt;

    invoke-direct {v2, v0, v1}, Lamt;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 773
    new-instance v0, Lavj;

    invoke-direct {v0, v2}, Lavj;-><init>(Lavl;)V

    iput-object v0, p0, Lamo;->I:Lavj;

    .line 774
    iget-object v0, p0, Lamo;->I:Lavj;

    goto :goto_0
.end method

.method public m()Laxp;
    .locals 3

    .prologue
    .line 779
    iget-object v0, p0, Lamo;->J:Laxp;

    if-eqz v0, :cond_0

    .line 780
    iget-object v0, p0, Lamo;->J:Laxp;

    .line 801
    :goto_0
    return-object v0

    .line 782
    :cond_0
    iget-object v0, p0, Lamo;->c:Landroid/content/Context;

    const-string v1, "userHints"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 784
    new-instance v1, Lamu;

    invoke-direct {v1, v0}, Lamu;-><init>(Landroid/content/SharedPreferences;)V

    .line 800
    new-instance v0, Laxp;

    invoke-static {}, Lcgt;->a()Lcgt;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Laxp;-><init>(Laxr;Lcgt;)V

    iput-object v0, p0, Lamo;->J:Laxp;

    .line 801
    iget-object v0, p0, Lamo;->J:Laxp;

    goto :goto_0
.end method

.method public n()Lbyf;
    .locals 2

    .prologue
    .line 806
    iget-object v0, p0, Lamo;->Q:Lbyf;

    if-nez v0, :cond_0

    .line 807
    new-instance v0, Lbyf;

    new-instance v1, Lbyl;

    invoke-direct {v1}, Lbyl;-><init>()V

    invoke-direct {v0, v1}, Lbyf;-><init>(Lbyg;)V

    iput-object v0, p0, Lamo;->Q:Lbyf;

    .line 809
    :cond_0
    iget-object v0, p0, Lamo;->Q:Lbyf;

    return-object v0
.end method

.method public o()Laqx;
    .locals 3

    .prologue
    .line 417
    iget-object v0, p0, Lamo;->t:Laqx;

    if-nez v0, :cond_0

    .line 418
    new-instance v0, Laqx;

    .line 419
    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lamo;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Laqx;-><init>(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lamo;->t:Laqx;

    .line 421
    :cond_0
    iget-object v0, p0, Lamo;->t:Laqx;

    return-object v0
.end method

.method public p()Lanh;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lamo;->g:Lanh;

    return-object v0
.end method

.method public q()Lajt;
    .locals 5

    .prologue
    .line 431
    iget-object v0, p0, Lamo;->u:Lajt;

    if-nez v0, :cond_0

    .line 432
    new-instance v0, Lajf;

    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    const-string v2, "metrics.db"

    iget-object v3, p0, Lamo;->c:Landroid/content/Context;

    .line 435
    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const/16 v4, 0x14

    invoke-direct {v0, v1, v2, v3, v4}, Lajf;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;I)V

    iput-object v0, p0, Lamo;->u:Lajt;

    .line 438
    :cond_0
    iget-object v0, p0, Lamo;->u:Lajt;

    return-object v0
.end method

.method public r()Lajl;
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lamo;->v:Lajl;

    if-nez v0, :cond_0

    .line 444
    new-instance v0, Lbkj;

    .line 445
    invoke-virtual {p0}, Lamo;->q()Lajt;

    move-result-object v1

    invoke-direct {v0, v1}, Lbkj;-><init>(Lajl;)V

    iput-object v0, p0, Lamo;->v:Lajl;

    .line 447
    :cond_0
    iget-object v0, p0, Lamo;->v:Lajl;

    return-object v0
.end method

.method public s()Lbks;
    .locals 1

    .prologue
    .line 827
    iget-object v0, p0, Lamo;->E:Lbks;

    if-nez v0, :cond_0

    .line 828
    new-instance v0, Lbks;

    invoke-direct {v0}, Lbks;-><init>()V

    iput-object v0, p0, Lamo;->E:Lbks;

    .line 830
    :cond_0
    iget-object v0, p0, Lamo;->E:Lbks;

    return-object v0
.end method

.method public t()Lbgf;
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lamo;->A:Lbgf;

    if-nez v0, :cond_0

    .line 529
    new-instance v0, Lbgf;

    invoke-virtual {p0}, Lamo;->e()Lchk;

    move-result-object v1

    invoke-direct {v0, v1}, Lbgf;-><init>(Lchk;)V

    iput-object v0, p0, Lamo;->A:Lbgf;

    .line 531
    :cond_0
    iget-object v0, p0, Lamo;->A:Lbgf;

    return-object v0
.end method

.method public u()Lbqo;
    .locals 4

    .prologue
    .line 536
    iget-object v0, p0, Lamo;->B:Lbqo;

    if-nez v0, :cond_0

    .line 537
    sget v0, Lamo;->a:I

    .line 538
    invoke-static {v0}, Lbpz;->a(I)Lbpz;

    move-result-object v0

    .line 539
    sget v1, Lamo;->b:I

    .line 540
    invoke-static {v1}, Lbpz;->a(I)Lbpz;

    move-result-object v1

    .line 542
    new-instance v2, Lbqo;

    invoke-virtual {p0}, Lamo;->V()Lbqk;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, Lbqo;-><init>(Lbqk;Lbpz;Lbpz;)V

    iput-object v2, p0, Lamo;->B:Lbqo;

    .line 544
    :cond_0
    iget-object v0, p0, Lamo;->B:Lbqo;

    return-object v0
.end method

.method public v()Lbig;
    .locals 6

    .prologue
    .line 560
    iget-object v0, p0, Lamo;->D:Lbig;

    if-nez v0, :cond_0

    .line 561
    new-instance v0, Lbif;

    iget-object v1, p0, Lamo;->c:Landroid/content/Context;

    .line 563
    invoke-virtual {p0}, Lamo;->L()Ljfb;

    move-result-object v2

    new-instance v3, Llmo;

    .line 565
    invoke-virtual {p0}, Lamo;->T()Llmg;

    move-result-object v4

    invoke-virtual {p0}, Lamo;->S()Llmg;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Llmo;-><init>(Lllx;Lllx;)V

    invoke-direct {v0, v1, v2, v3}, Lbif;-><init>(Landroid/content/Context;Ljfb;Lllx;)V

    iput-object v0, p0, Lamo;->D:Lbig;

    .line 567
    :cond_0
    iget-object v0, p0, Lamo;->D:Lbig;

    return-object v0
.end method

.method public w()Lawy;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lamo;->j:Lawy;

    return-object v0
.end method

.method public x()Lbjp;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbjp",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 462
    iget-object v0, p0, Lamo;->y:Lbjp;

    if-nez v0, :cond_0

    .line 463
    invoke-virtual {p0}, Lamo;->p()Lanh;

    move-result-object v0

    .line 464
    invoke-virtual {p0}, Lamo;->e()Lchk;

    move-result-object v1

    const-class v2, Lbjm;

    const-string v3, "AudioDecoderBackgroundExecutor"

    invoke-interface {v1, v2, v3}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 466
    new-instance v2, Lbjp;

    new-instance v3, Lamq;

    invoke-direct {v3, v0}, Lamq;-><init>(Lanh;)V

    new-instance v0, Lbjm;

    .line 480
    invoke-virtual {p0}, Lamo;->p()Lanh;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lbjm;-><init>(Lbiw;Ljava/util/concurrent/Executor;)V

    invoke-direct {v2, v3, v0}, Lbjp;-><init>(Lbjq;Lbjl;)V

    iput-object v2, p0, Lamo;->y:Lbjp;

    .line 482
    :cond_0
    iget-object v0, p0, Lamo;->y:Lbjp;

    return-object v0
.end method

.method public y()Lbjp;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 487
    iget-object v0, p0, Lamo;->x:Lbjp;

    if-nez v0, :cond_0

    .line 488
    invoke-virtual {p0}, Lamo;->p()Lanh;

    move-result-object v0

    .line 489
    invoke-virtual {p0}, Lamo;->e()Lchk;

    move-result-object v1

    const-class v2, Lbjo;

    const-string v3, "VideoDecoderBackgroundExecutor"

    invoke-interface {v1, v2, v3}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 491
    new-instance v2, Lbjp;

    new-instance v3, Lamr;

    invoke-direct {v3, v0}, Lamr;-><init>(Lanh;)V

    new-instance v4, Lbjo;

    .line 510
    invoke-virtual {p0}, Lamo;->t()Lbgf;

    move-result-object v5

    invoke-direct {v4, v5, v0, v1}, Lbjo;-><init>(Lbgf;Lbiw;Ljava/util/concurrent/Executor;)V

    invoke-direct {v2, v3, v4}, Lbjp;-><init>(Lbjq;Lbjl;)V

    iput-object v2, p0, Lamo;->x:Lbjp;

    .line 512
    :cond_0
    iget-object v0, p0, Lamo;->x:Lbjp;

    return-object v0
.end method

.method public z()Lbjl;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbjl",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 517
    iget-object v0, p0, Lamo;->z:Lbjl;

    if-nez v0, :cond_0

    .line 518
    invoke-virtual {p0}, Lamo;->e()Lchk;

    move-result-object v0

    const-class v1, Lbjo;

    const-string v2, "ClipEditorDecoderBackgroundExecutor"

    invoke-interface {v0, v1, v2}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 520
    new-instance v1, Lbjn;

    .line 521
    invoke-virtual {p0}, Lamo;->t()Lbgf;

    move-result-object v2

    invoke-virtual {p0}, Lamo;->p()Lanh;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lbjn;-><init>(Lbgf;Lbiw;Ljava/util/concurrent/Executor;)V

    iput-object v1, p0, Lamo;->z:Lbjl;

    .line 523
    :cond_0
    iget-object v0, p0, Lamo;->z:Lbjl;

    return-object v0
.end method
