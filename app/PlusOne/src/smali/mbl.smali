.class public final Lmbl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvy;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7612
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7615
    iput-object v0, p0, Lmbl;->apiHeader:Llyr;

    .line 7618
    iput-object v0, p0, Lmbl;->a:Lnvy;

    .line 7612
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7635
    const/4 v0, 0x0

    .line 7636
    iget-object v1, p0, Lmbl;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 7637
    const/4 v0, 0x1

    iget-object v1, p0, Lmbl;->apiHeader:Llyr;

    .line 7638
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7640
    :cond_0
    iget-object v1, p0, Lmbl;->a:Lnvy;

    if-eqz v1, :cond_1

    .line 7641
    const/4 v1, 0x2

    iget-object v2, p0, Lmbl;->a:Lnvy;

    .line 7642
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7644
    :cond_1
    iget-object v1, p0, Lmbl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7645
    iput v0, p0, Lmbl;->ai:I

    .line 7646
    return v0
.end method

.method public a(Loxn;)Lmbl;
    .locals 2

    .prologue
    .line 7654
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7655
    sparse-switch v0, :sswitch_data_0

    .line 7659
    iget-object v1, p0, Lmbl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7660
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbl;->ah:Ljava/util/List;

    .line 7663
    :cond_1
    iget-object v1, p0, Lmbl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7665
    :sswitch_0
    return-object p0

    .line 7670
    :sswitch_1
    iget-object v0, p0, Lmbl;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 7671
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbl;->apiHeader:Llyr;

    .line 7673
    :cond_2
    iget-object v0, p0, Lmbl;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7677
    :sswitch_2
    iget-object v0, p0, Lmbl;->a:Lnvy;

    if-nez v0, :cond_3

    .line 7678
    new-instance v0, Lnvy;

    invoke-direct {v0}, Lnvy;-><init>()V

    iput-object v0, p0, Lmbl;->a:Lnvy;

    .line 7680
    :cond_3
    iget-object v0, p0, Lmbl;->a:Lnvy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7655
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7623
    iget-object v0, p0, Lmbl;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 7624
    const/4 v0, 0x1

    iget-object v1, p0, Lmbl;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7626
    :cond_0
    iget-object v0, p0, Lmbl;->a:Lnvy;

    if-eqz v0, :cond_1

    .line 7627
    const/4 v0, 0x2

    iget-object v1, p0, Lmbl;->a:Lnvy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7629
    :cond_1
    iget-object v0, p0, Lmbl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7631
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7608
    invoke-virtual {p0, p1}, Lmbl;->a(Loxn;)Lmbl;

    move-result-object v0

    return-object v0
.end method
