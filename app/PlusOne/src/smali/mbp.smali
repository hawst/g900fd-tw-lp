.class public final Lmbp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojw;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10528
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10531
    iput-object v0, p0, Lmbp;->apiHeader:Llyr;

    .line 10534
    iput-object v0, p0, Lmbp;->a:Lojw;

    .line 10528
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10551
    const/4 v0, 0x0

    .line 10552
    iget-object v1, p0, Lmbp;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 10553
    const/4 v0, 0x1

    iget-object v1, p0, Lmbp;->apiHeader:Llyr;

    .line 10554
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10556
    :cond_0
    iget-object v1, p0, Lmbp;->a:Lojw;

    if-eqz v1, :cond_1

    .line 10557
    const/4 v1, 0x2

    iget-object v2, p0, Lmbp;->a:Lojw;

    .line 10558
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10560
    :cond_1
    iget-object v1, p0, Lmbp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10561
    iput v0, p0, Lmbp;->ai:I

    .line 10562
    return v0
.end method

.method public a(Loxn;)Lmbp;
    .locals 2

    .prologue
    .line 10570
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10571
    sparse-switch v0, :sswitch_data_0

    .line 10575
    iget-object v1, p0, Lmbp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10576
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbp;->ah:Ljava/util/List;

    .line 10579
    :cond_1
    iget-object v1, p0, Lmbp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10581
    :sswitch_0
    return-object p0

    .line 10586
    :sswitch_1
    iget-object v0, p0, Lmbp;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 10587
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbp;->apiHeader:Llyr;

    .line 10589
    :cond_2
    iget-object v0, p0, Lmbp;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10593
    :sswitch_2
    iget-object v0, p0, Lmbp;->a:Lojw;

    if-nez v0, :cond_3

    .line 10594
    new-instance v0, Lojw;

    invoke-direct {v0}, Lojw;-><init>()V

    iput-object v0, p0, Lmbp;->a:Lojw;

    .line 10596
    :cond_3
    iget-object v0, p0, Lmbp;->a:Lojw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10571
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10539
    iget-object v0, p0, Lmbp;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 10540
    const/4 v0, 0x1

    iget-object v1, p0, Lmbp;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10542
    :cond_0
    iget-object v0, p0, Lmbp;->a:Lojw;

    if-eqz v0, :cond_1

    .line 10543
    const/4 v0, 0x2

    iget-object v1, p0, Lmbp;->a:Lojw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10545
    :cond_1
    iget-object v0, p0, Lmbp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10547
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10524
    invoke-virtual {p0, p1}, Lmbp;->a(Loxn;)Lmbp;

    move-result-object v0

    return-object v0
.end method
