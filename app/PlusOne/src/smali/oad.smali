.class public final Load;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Load;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    new-array v0, v0, [Load;

    sput-object v0, Load;->a:[Load;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 350
    const/4 v0, 0x1

    iget-object v1, p0, Load;->e:Ljava/lang/String;

    .line 352
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 353
    const/4 v1, 0x2

    iget-object v2, p0, Load;->b:Ljava/lang/String;

    .line 354
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    iget-object v1, p0, Load;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 356
    const/4 v1, 0x3

    iget-object v2, p0, Load;->f:Ljava/lang/String;

    .line 357
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_0
    iget-object v1, p0, Load;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 360
    const/4 v1, 0x4

    iget-object v2, p0, Load;->c:Ljava/lang/String;

    .line 361
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_1
    iget-object v1, p0, Load;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 364
    const/4 v1, 0x5

    iget-object v2, p0, Load;->d:Ljava/lang/String;

    .line 365
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_2
    iget-object v1, p0, Load;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 368
    iput v0, p0, Load;->ai:I

    .line 369
    return v0
.end method

.method public a(Loxn;)Load;
    .locals 2

    .prologue
    .line 377
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 378
    sparse-switch v0, :sswitch_data_0

    .line 382
    iget-object v1, p0, Load;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 383
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Load;->ah:Ljava/util/List;

    .line 386
    :cond_1
    iget-object v1, p0, Load;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    :sswitch_0
    return-object p0

    .line 393
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Load;->e:Ljava/lang/String;

    goto :goto_0

    .line 397
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Load;->b:Ljava/lang/String;

    goto :goto_0

    .line 401
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Load;->f:Ljava/lang/String;

    goto :goto_0

    .line 405
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Load;->c:Ljava/lang/String;

    goto :goto_0

    .line 409
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Load;->d:Ljava/lang/String;

    goto :goto_0

    .line 378
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 333
    const/4 v0, 0x1

    iget-object v1, p0, Load;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 334
    const/4 v0, 0x2

    iget-object v1, p0, Load;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 335
    iget-object v0, p0, Load;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 336
    const/4 v0, 0x3

    iget-object v1, p0, Load;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 338
    :cond_0
    iget-object v0, p0, Load;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 339
    const/4 v0, 0x4

    iget-object v1, p0, Load;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 341
    :cond_1
    iget-object v0, p0, Load;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 342
    const/4 v0, 0x5

    iget-object v1, p0, Load;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 344
    :cond_2
    iget-object v0, p0, Load;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 346
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 314
    invoke-virtual {p0, p1}, Load;->a(Loxn;)Load;

    move-result-object v0

    return-object v0
.end method
