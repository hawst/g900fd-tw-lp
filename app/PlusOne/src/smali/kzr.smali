.class public final Lkzr;
.super Lllq;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[Lkzv;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lllq;-><init>()V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lkzv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Lllq;-><init>()V

    .line 48
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lkzr;->a:I

    .line 49
    iget v0, p0, Lkzr;->a:I

    iput v0, p0, Lkzr;->b:I

    .line 50
    iget v0, p0, Lkzr;->a:I

    new-array v0, v0, [Lkzv;

    iput-object v0, p0, Lkzr;->f:[Lkzv;

    .line 51
    iget-object v0, p0, Lkzr;->f:[Lkzv;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public constructor <init>(Lpbo;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lllq;-><init>()V

    .line 33
    iget-object v0, p1, Lpbo;->e:[Loya;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lkzr;->a:I

    .line 35
    iget v0, p0, Lkzr;->a:I

    iget-object v2, p1, Lpbo;->f:Ljava/lang/Integer;

    .line 36
    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    .line 35
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lkzr;->b:I

    .line 37
    iget-object v0, p1, Lpbo;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzr;->e:Ljava/lang/String;

    .line 38
    iget-object v0, p1, Lpbo;->d:Ljava/lang/String;

    iput-object v0, p0, Lkzr;->d:Ljava/lang/String;

    .line 39
    iget-object v0, p1, Lpbo;->c:Ljava/lang/String;

    iput-object v0, p0, Lkzr;->c:Ljava/lang/String;

    .line 40
    iget v0, p0, Lkzr;->a:I

    new-array v0, v0, [Lkzv;

    iput-object v0, p0, Lkzr;->f:[Lkzv;

    .line 41
    :goto_1
    iget v0, p0, Lkzr;->a:I

    if-ge v1, v0, :cond_1

    .line 42
    iget-object v2, p0, Lkzr;->f:[Lkzv;

    new-instance v3, Lkzv;

    iget-object v0, p1, Lpbo;->e:[Loya;

    aget-object v0, v0, v1

    sget-object v4, Lpbu;->a:Loxr;

    invoke-virtual {v0, v4}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpbu;

    invoke-direct {v3, v0}, Lkzv;-><init>(Lpbu;)V

    aput-object v3, v2, v1

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 33
    :cond_0
    iget-object v0, p1, Lpbo;->e:[Loya;

    array-length v0, v0

    goto :goto_0

    .line 45
    :cond_1
    return-void
.end method

.method public static a([B)Lkzr;
    .locals 5

    .prologue
    .line 78
    if-nez p0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 96
    :goto_0
    return-object v0

    .line 82
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 83
    new-instance v1, Lkzr;

    invoke-direct {v1}, Lkzr;-><init>()V

    .line 85
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, v1, Lkzr;->a:I

    .line 86
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, v1, Lkzr;->b:I

    .line 87
    invoke-static {v2}, Lkzr;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lkzr;->d:Ljava/lang/String;

    .line 88
    invoke-static {v2}, Lkzr;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lkzr;->c:Ljava/lang/String;

    .line 89
    invoke-static {v2}, Lkzr;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lkzr;->e:Ljava/lang/String;

    .line 90
    iget v0, v1, Lkzr;->a:I

    new-array v0, v0, [Lkzv;

    iput-object v0, v1, Lkzr;->f:[Lkzv;

    .line 91
    const/4 v0, 0x0

    :goto_1
    iget v3, v1, Lkzr;->a:I

    if-ge v0, v3, :cond_1

    .line 92
    iget-object v3, v1, Lkzr;->f:[Lkzv;

    new-instance v4, Lkzv;

    invoke-direct {v4}, Lkzv;-><init>()V

    aput-object v4, v3, v0

    .line 93
    iget-object v3, v1, Lkzr;->f:[Lkzv;

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Lkzv;->a(Ljava/nio/ByteBuffer;)V

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 96
    goto :goto_0
.end method

.method public static a(Lkzr;)[B
    .locals 4

    .prologue
    .line 55
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x100

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 56
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 58
    iget v0, p0, Lkzr;->a:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 59
    iget v0, p0, Lkzr;->b:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 60
    iget-object v0, p0, Lkzr;->d:Ljava/lang/String;

    invoke-static {v2, v0}, Lkzr;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lkzr;->c:Ljava/lang/String;

    invoke-static {v2, v0}, Lkzr;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lkzr;->e:Ljava/lang/String;

    invoke-static {v2, v0}, Lkzr;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 63
    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lkzr;->a:I

    if-ge v0, v3, :cond_0

    .line 64
    iget-object v3, p0, Lkzr;->f:[Lkzv;

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Lkzv;->a(Ljava/io/DataOutputStream;)V

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 68
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 69
    return-object v0
.end method

.method public static a(Lpbo;)[B
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lkzr;

    invoke-direct {v0, p0}, Lkzr;-><init>(Lpbo;)V

    invoke-static {v0}, Lkzr;->a(Lkzr;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lkzr;->a:I

    return v0
.end method

.method public a(I)Lkzv;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lkzr;->f:[Lkzv;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lkzr;->b:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lkzr;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lkzr;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lkzr;->c:Ljava/lang/String;

    return-object v0
.end method
