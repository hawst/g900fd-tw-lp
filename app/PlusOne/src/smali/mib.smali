.class public final Lmib;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntt;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16036
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16039
    iput-object v0, p0, Lmib;->apiHeader:Llyr;

    .line 16042
    iput-object v0, p0, Lmib;->a:Lntt;

    .line 16036
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 16059
    const/4 v0, 0x0

    .line 16060
    iget-object v1, p0, Lmib;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 16061
    const/4 v0, 0x1

    iget-object v1, p0, Lmib;->apiHeader:Llyr;

    .line 16062
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16064
    :cond_0
    iget-object v1, p0, Lmib;->a:Lntt;

    if-eqz v1, :cond_1

    .line 16065
    const/4 v1, 0x2

    iget-object v2, p0, Lmib;->a:Lntt;

    .line 16066
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16068
    :cond_1
    iget-object v1, p0, Lmib;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16069
    iput v0, p0, Lmib;->ai:I

    .line 16070
    return v0
.end method

.method public a(Loxn;)Lmib;
    .locals 2

    .prologue
    .line 16078
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 16079
    sparse-switch v0, :sswitch_data_0

    .line 16083
    iget-object v1, p0, Lmib;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 16084
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmib;->ah:Ljava/util/List;

    .line 16087
    :cond_1
    iget-object v1, p0, Lmib;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16089
    :sswitch_0
    return-object p0

    .line 16094
    :sswitch_1
    iget-object v0, p0, Lmib;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 16095
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmib;->apiHeader:Llyr;

    .line 16097
    :cond_2
    iget-object v0, p0, Lmib;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16101
    :sswitch_2
    iget-object v0, p0, Lmib;->a:Lntt;

    if-nez v0, :cond_3

    .line 16102
    new-instance v0, Lntt;

    invoke-direct {v0}, Lntt;-><init>()V

    iput-object v0, p0, Lmib;->a:Lntt;

    .line 16104
    :cond_3
    iget-object v0, p0, Lmib;->a:Lntt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16079
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 16047
    iget-object v0, p0, Lmib;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 16048
    const/4 v0, 0x1

    iget-object v1, p0, Lmib;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16050
    :cond_0
    iget-object v0, p0, Lmib;->a:Lntt;

    if-eqz v0, :cond_1

    .line 16051
    const/4 v0, 0x2

    iget-object v1, p0, Lmib;->a:Lntt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16053
    :cond_1
    iget-object v0, p0, Lmib;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 16055
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16032
    invoke-virtual {p0, p1}, Lmib;->a(Loxn;)Lmib;

    move-result-object v0

    return-object v0
.end method
