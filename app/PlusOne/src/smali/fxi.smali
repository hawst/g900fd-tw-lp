.class public final Lfxi;
.super Lfwp;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static c:Z

.field private static d:I

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:F


# instance fields
.field private a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private b:Lfwn;

.field private k:Lfxf;

.field private l:Lfyc;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lfwp;-><init>(Landroid/content/Context;)V

    .line 40
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 160
    invoke-super {p0}, Lfwp;->a()V

    .line 161
    iput-object v2, p0, Lfxi;->k:Lfxf;

    .line 162
    iget-object v0, p0, Lfxi;->b:Lfwn;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v2, v2, v1}, Lfwn;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 163
    iget-object v0, p0, Lfxi;->b:Lfwn;

    invoke-virtual {v0, v2}, Lfwn;->a(Lfxf;)V

    .line 164
    iget-object v0, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d()V

    .line 165
    iget-object v0, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 166
    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 52
    invoke-super {p0, p1, p2, p3}, Lfwp;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    sget-boolean v0, Lfxi;->c:Z

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    const v1, 0x7f0d02d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxi;->g:I

    .line 59
    const v1, 0x7f0d02d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxi;->h:I

    .line 61
    const v1, 0x7f0d02d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxi;->i:I

    .line 63
    const v1, 0x7f0d02cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxi;->e:I

    .line 65
    const v1, 0x7f0d02cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxi;->d:I

    .line 67
    const v1, 0x7f0d02d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lfxi;->j:F

    .line 69
    const v1, 0x7f0d02ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxi;->f:I

    .line 71
    sput-boolean v4, Lfxi;->c:Z

    .line 74
    :cond_0
    sget v0, Lfxi;->e:I

    sget v1, Lfxi;->d:I

    sget v2, Lfxi;->h:I

    sget v3, Lfxi;->i:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lfxi;->a(IIII)V

    .line 77
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 78
    iget-object v0, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 79
    iget-object v0, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lfxi;->addView(Landroid/view/View;)V

    .line 82
    new-instance v0, Lfwn;

    invoke-direct {v0, p1, p2, p3}, Lfwn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxi;->b:Lfwn;

    .line 83
    iget-object v0, p0, Lfxi;->b:Lfwn;

    invoke-virtual {p0, v0}, Lfxi;->addView(Landroid/view/View;)V

    .line 85
    invoke-virtual {p0, p0}, Lfxi;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-void
.end method

.method public a(Lfyc;Lfxf;Z)V
    .locals 6

    .prologue
    .line 136
    if-nez p1, :cond_0

    .line 156
    :goto_0
    return-void

    .line 140
    :cond_0
    iput-object p1, p0, Lfxi;->l:Lfyc;

    .line 142
    iget-object v0, p0, Lfxi;->l:Lfyc;

    iget-object v0, v0, Lfyc;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    iget-object v0, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v1, p0, Lfxi;->l:Lfyc;

    iget-object v1, v1, Lfyc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 146
    :cond_1
    iget-object v0, p0, Lfxi;->b:Lfwn;

    iget-object v1, p0, Lfxi;->l:Lfyc;

    iget-object v1, v1, Lfyc;->a:Ljava/lang/String;

    .line 147
    invoke-virtual {p0}, Lfxi;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lfxi;->l:Lfyc;

    iget-wide v4, v3, Lfyc;->d:J

    invoke-static {v2, v4, v5}, Llhu;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lfxi;->l:Lfyc;

    iget-object v3, v3, Lfyc;->e:Ljava/lang/String;

    .line 146
    invoke-virtual {v0, v1, v2, v3, p3}, Lfwn;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 150
    iget-object v0, p0, Lfxi;->l:Lfyc;

    iget-object v0, v0, Lfyc;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 151
    iget-object v0, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v1, p0, Lfxi;->l:Lfyc;

    iget-object v1, v1, Lfyc;->b:Ljava/lang/String;

    iget-object v2, p0, Lfxi;->l:Lfyc;

    iget-object v2, v2, Lfyc;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_2
    iput-object p2, p0, Lfxi;->k:Lfxf;

    .line 155
    iget-object v0, p0, Lfxi;->b:Lfwn;

    iget-object v1, p0, Lfxi;->k:Lfxf;

    invoke-virtual {v0, v1}, Lfwn;->a(Lfxf;)V

    goto :goto_0
.end method

.method protected measureChildren(II)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 90
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 91
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 99
    iget-object v4, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    sget v5, Lfxi;->f:I

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    sget v6, Lfxi;->f:I

    .line 103
    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 102
    invoke-virtual {v4, v5, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 104
    iget-object v4, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-static {v4, v2, v2}, Lfxi;->a(Landroid/view/View;II)V

    .line 107
    sget v4, Lfxi;->j:F

    iget-object v5, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 108
    invoke-virtual {v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    .line 109
    iget-object v5, p0, Lfxi;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getMeasuredWidth()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    sget v6, Lfxi;->g:I

    add-int/2addr v5, v6

    .line 110
    sub-int v6, v0, v5

    .line 111
    sub-int v7, v3, v4

    .line 113
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 115
    :goto_0
    iget-object v8, p0, Lfxi;->b:Lfwn;

    .line 116
    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    if-eqz v0, :cond_1

    move v3, v2

    .line 117
    :goto_1
    invoke-static {v7, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 115
    invoke-virtual {v8, v6, v3}, Lfwn;->measure(II)V

    .line 120
    iget-object v3, p0, Lfxi;->b:Lfwn;

    invoke-static {v3, v5, v4}, Lfxi;->a(Landroid/view/View;II)V

    .line 121
    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0, v1}, Lfxi;->setClickable(Z)V

    .line 122
    return-void

    :cond_0
    move v0, v2

    .line 113
    goto :goto_0

    .line 116
    :cond_1
    const/high16 v3, -0x80000000

    goto :goto_1

    :cond_2
    move v1, v2

    .line 121
    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lfxi;->k:Lfxf;

    if-eqz v0, :cond_0

    .line 174
    instance-of v0, p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lfxi;->k:Lfxf;

    check-cast p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lfxf;->b(Ljava/lang/String;)V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lfxi;->k:Lfxf;

    iget-object v1, p0, Lfxi;->l:Lfyc;

    invoke-interface {v0, v1}, Lfxf;->a(Lfyc;)V

    goto :goto_0
.end method
