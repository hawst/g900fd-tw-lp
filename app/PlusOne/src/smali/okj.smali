.class public final Lokj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lokj;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 837
    const/4 v0, 0x0

    new-array v0, v0, [Lokj;

    sput-object v0, Lokj;->a:[Lokj;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 838
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 889
    const/4 v0, 0x0

    .line 890
    iget-object v1, p0, Lokj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 891
    const/4 v0, 0x1

    iget-object v1, p0, Lokj;->b:Ljava/lang/Integer;

    .line 892
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 894
    :cond_0
    iget-object v1, p0, Lokj;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 895
    const/4 v1, 0x2

    iget-object v2, p0, Lokj;->c:Ljava/lang/String;

    .line 896
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 898
    :cond_1
    iget-object v1, p0, Lokj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 899
    const/4 v1, 0x3

    iget-object v2, p0, Lokj;->d:Ljava/lang/Integer;

    .line 900
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 902
    :cond_2
    iget-object v1, p0, Lokj;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 903
    const/4 v1, 0x4

    iget-object v2, p0, Lokj;->e:Ljava/lang/String;

    .line 904
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 906
    :cond_3
    iget-object v1, p0, Lokj;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 907
    const/4 v1, 0x5

    iget-object v2, p0, Lokj;->f:Ljava/lang/Integer;

    .line 908
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 910
    :cond_4
    iget-object v1, p0, Lokj;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 911
    const/4 v1, 0x6

    iget-object v2, p0, Lokj;->g:Ljava/lang/Boolean;

    .line 912
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 914
    :cond_5
    iget-object v1, p0, Lokj;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 915
    const/4 v1, 0x7

    iget-object v2, p0, Lokj;->h:Ljava/lang/Boolean;

    .line 916
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 918
    :cond_6
    iget-object v1, p0, Lokj;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 919
    const/16 v1, 0x8

    iget-object v2, p0, Lokj;->i:Ljava/lang/Boolean;

    .line 920
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 922
    :cond_7
    iget-object v1, p0, Lokj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 923
    iput v0, p0, Lokj;->ai:I

    .line 924
    return v0
.end method

.method public a(Loxn;)Lokj;
    .locals 2

    .prologue
    .line 932
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 933
    sparse-switch v0, :sswitch_data_0

    .line 937
    iget-object v1, p0, Lokj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 938
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lokj;->ah:Ljava/util/List;

    .line 941
    :cond_1
    iget-object v1, p0, Lokj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 943
    :sswitch_0
    return-object p0

    .line 948
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokj;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 952
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokj;->c:Ljava/lang/String;

    goto :goto_0

    .line 956
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokj;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 960
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokj;->e:Ljava/lang/String;

    goto :goto_0

    .line 964
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokj;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 968
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokj;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 972
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokj;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 976
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokj;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 933
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 859
    iget-object v0, p0, Lokj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 860
    const/4 v0, 0x1

    iget-object v1, p0, Lokj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 862
    :cond_0
    iget-object v0, p0, Lokj;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 863
    const/4 v0, 0x2

    iget-object v1, p0, Lokj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 865
    :cond_1
    iget-object v0, p0, Lokj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 866
    const/4 v0, 0x3

    iget-object v1, p0, Lokj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 868
    :cond_2
    iget-object v0, p0, Lokj;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 869
    const/4 v0, 0x4

    iget-object v1, p0, Lokj;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 871
    :cond_3
    iget-object v0, p0, Lokj;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 872
    const/4 v0, 0x5

    iget-object v1, p0, Lokj;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 874
    :cond_4
    iget-object v0, p0, Lokj;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 875
    const/4 v0, 0x6

    iget-object v1, p0, Lokj;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 877
    :cond_5
    iget-object v0, p0, Lokj;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 878
    const/4 v0, 0x7

    iget-object v1, p0, Lokj;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 880
    :cond_6
    iget-object v0, p0, Lokj;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 881
    const/16 v0, 0x8

    iget-object v1, p0, Lokj;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 883
    :cond_7
    iget-object v0, p0, Lokj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 885
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 834
    invoke-virtual {p0, p1}, Lokj;->a(Loxn;)Lokj;

    move-result-object v0

    return-object v0
.end method
