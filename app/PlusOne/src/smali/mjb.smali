.class public final Lmjb;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnwi;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9394
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9397
    iput-object v0, p0, Lmjb;->apiHeader:Llyr;

    .line 9400
    iput-object v0, p0, Lmjb;->a:Lnwi;

    .line 9394
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9417
    const/4 v0, 0x0

    .line 9418
    iget-object v1, p0, Lmjb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 9419
    const/4 v0, 0x1

    iget-object v1, p0, Lmjb;->apiHeader:Llyr;

    .line 9420
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9422
    :cond_0
    iget-object v1, p0, Lmjb;->a:Lnwi;

    if-eqz v1, :cond_1

    .line 9423
    const/4 v1, 0x2

    iget-object v2, p0, Lmjb;->a:Lnwi;

    .line 9424
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9426
    :cond_1
    iget-object v1, p0, Lmjb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9427
    iput v0, p0, Lmjb;->ai:I

    .line 9428
    return v0
.end method

.method public a(Loxn;)Lmjb;
    .locals 2

    .prologue
    .line 9436
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9437
    sparse-switch v0, :sswitch_data_0

    .line 9441
    iget-object v1, p0, Lmjb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9442
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjb;->ah:Ljava/util/List;

    .line 9445
    :cond_1
    iget-object v1, p0, Lmjb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9447
    :sswitch_0
    return-object p0

    .line 9452
    :sswitch_1
    iget-object v0, p0, Lmjb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 9453
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjb;->apiHeader:Llyr;

    .line 9455
    :cond_2
    iget-object v0, p0, Lmjb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9459
    :sswitch_2
    iget-object v0, p0, Lmjb;->a:Lnwi;

    if-nez v0, :cond_3

    .line 9460
    new-instance v0, Lnwi;

    invoke-direct {v0}, Lnwi;-><init>()V

    iput-object v0, p0, Lmjb;->a:Lnwi;

    .line 9462
    :cond_3
    iget-object v0, p0, Lmjb;->a:Lnwi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9437
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9405
    iget-object v0, p0, Lmjb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 9406
    const/4 v0, 0x1

    iget-object v1, p0, Lmjb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9408
    :cond_0
    iget-object v0, p0, Lmjb;->a:Lnwi;

    if-eqz v0, :cond_1

    .line 9409
    const/4 v0, 0x2

    iget-object v1, p0, Lmjb;->a:Lnwi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9411
    :cond_1
    iget-object v0, p0, Lmjb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9413
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9390
    invoke-virtual {p0, p1}, Lmjb;->a(Loxn;)Lmjb;

    move-result-object v0

    return-object v0
.end method
