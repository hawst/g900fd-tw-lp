.class public final Ljkn;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446
    return-void
.end method

.method private a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 346
    const-class v0, Ljiy;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljiy;

    .line 347
    invoke-interface {v0}, Ljiy;->d()I

    move-result v0

    return v0
.end method

.method private varargs a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 389
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 390
    const-string v1, "com.google.android.libraries.social.notifications.SYSTEM_NOTIFICATION_DISMISSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    const-string v1, "notification_event_type"

    sget-object v2, Ljir;->c:Ljir;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 393
    const-string v1, "view_id"

    const-string v2, "AST"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    const-string v1, "notification_key_list"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    const-string v1, "effective_gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 397
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 294
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 296
    const-string v0, "//"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    const-string v1, "https:"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object p2, v0

    .line 300
    :cond_0
    :try_start_0
    const-class v0, Lhso;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    .line 301
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-interface {v0, p2, v1, v2}, Lhso;->a(Ljava/lang/String;II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 313
    :goto_1
    return-object v0

    .line 297
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :catch_0
    move-exception v0

    .line 306
    const-string v1, "SystemNotManager"

    const-string v2, "Avatar Download Failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 313
    :cond_2
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 307
    :catch_1
    move-exception v0

    .line 308
    const-string v1, "SystemNotManager"

    const-string v2, "Avatar Download Canceled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 309
    :catch_2
    move-exception v0

    .line 310
    const-string v1, "SystemNotManager"

    const-string v2, "Avatar Download OutOfMemoryError"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljkx;)Lbs;
    .locals 19

    .prologue
    .line 104
    invoke-virtual/range {p4 .. p4}, Ljkx;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_0

    .line 105
    const/4 v4, 0x0

    .line 210
    :goto_0
    return-object v4

    .line 108
    :cond_0
    invoke-virtual/range {p4 .. p4}, Ljkx;->c()Llvo;

    move-result-object v8

    .line 109
    invoke-virtual/range {p4 .. p4}, Ljkx;->d()Llvg;

    move-result-object v9

    .line 110
    invoke-virtual/range {p4 .. p4}, Ljkx;->g()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 111
    invoke-virtual/range {p4 .. p4}, Ljkx;->f()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    move v7, v4

    .line 115
    :goto_1
    const/4 v5, 0x0

    .line 116
    const/4 v4, 0x0

    .line 118
    if-nez v8, :cond_2

    .line 119
    const/4 v4, 0x0

    goto :goto_0

    .line 111
    :cond_1
    const/4 v4, 0x0

    move v7, v4

    goto :goto_1

    .line 123
    :cond_2
    iget-object v6, v8, Llvo;->b:[Llvm;

    array-length v6, v6

    if-lez v6, :cond_5

    .line 124
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 125
    iget-object v13, v8, Llvo;->b:[Llvm;

    array-length v14, v13

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v14, :cond_4

    aget-object v15, v13, v6

    .line 126
    iget-object v15, v15, Llvm;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 127
    if-eqz v15, :cond_3

    .line 128
    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 131
    :cond_4
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 132
    invoke-static {v12}, Lhss;->a(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 137
    :cond_5
    if-nez v5, :cond_d

    iget-object v6, v8, Llvo;->a:Llvi;

    if-eqz v6, :cond_d

    .line 138
    iget-object v5, v8, Llvo;->a:Llvi;

    iget-object v5, v5, Llvi;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object v6, v5

    .line 142
    :goto_3
    if-eqz v9, :cond_7

    .line 144
    iget-object v5, v9, Llvg;->b:[Llvb;

    array-length v5, v5

    const/4 v12, 0x1

    if-ne v5, v12, :cond_c

    .line 145
    const/4 v5, 0x0

    .line 149
    iget-object v12, v9, Llvg;->a:Llvp;

    if-eqz v12, :cond_6

    .line 150
    iget-object v12, v9, Llvg;->a:Llvp;

    iget-object v12, v12, Llvp;->a:[Llvl;

    .line 151
    array-length v13, v12

    if-lez v13, :cond_6

    const/4 v13, 0x0

    aget-object v13, v12, v13

    iget-object v13, v13, Llvl;->b:Llvi;

    if-eqz v13, :cond_6

    .line 152
    const/4 v13, 0x0

    aget-object v12, v12, v13

    iget-object v12, v12, Llvl;->b:Llvi;

    iget-object v12, v12, Llvi;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12}, Ljkn;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 153
    if-eqz v12, :cond_6

    .line 154
    new-instance v4, Lbq;

    invoke-direct {v4}, Lbq;-><init>()V

    invoke-virtual {v4, v12}, Lbq;->a(Landroid/graphics/Bitmap;)Lbq;

    move-result-object v5

    .line 155
    const/4 v4, 0x1

    move/from16 v18, v4

    move-object v4, v5

    move/from16 v5, v18

    .line 161
    :cond_6
    if-nez v5, :cond_7

    .line 162
    iget-object v5, v9, Llvg;->b:[Llvb;

    const/4 v9, 0x0

    aget-object v5, v5, v9

    iget-object v5, v5, Llvb;->b:Llvo;

    .line 163
    if-eqz v5, :cond_7

    .line 164
    new-instance v4, Lbr;

    invoke-direct {v4}, Lbr;-><init>()V

    iget-object v9, v5, Llvo;->c:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    iget-object v5, v5, Llvo;->d:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x2

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v13, v14

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, "\n\n"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 165
    invoke-virtual {v4, v5}, Lbr;->b(Ljava/lang/CharSequence;)Lbr;

    move-result-object v4

    .line 166
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lbr;->a(Ljava/lang/CharSequence;)Lbr;

    move-result-object v4

    .line 186
    :cond_7
    new-instance v5, Lbs;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lbs;-><init>(Landroid/content/Context;)V

    .line 187
    iget-object v9, v8, Llvo;->c:Ljava/lang/String;

    invoke-virtual {v5, v9}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    .line 188
    iget-object v9, v8, Llvo;->d:Ljava/lang/String;

    invoke-virtual {v5, v9}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 189
    const v9, 0x7f0a03a8

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lbs;->d(Ljava/lang/CharSequence;)Lbs;

    .line 190
    invoke-direct/range {p0 .. p1}, Ljkn;->a(Landroid/content/Context;)I

    move-result v9

    invoke-virtual {v5, v9}, Lbs;->a(I)Lbs;

    .line 191
    invoke-virtual {v5, v7}, Lbs;->c(I)Lbs;

    .line 192
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 193
    invoke-virtual/range {p4 .. p4}, Ljkx;->a()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v7, v9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v7}, Ljkn;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v7

    .line 192
    invoke-virtual {v5, v7}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    .line 194
    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-lez v7, :cond_8

    .line 195
    invoke-virtual {v5, v10, v11}, Lbs;->a(J)Lbs;

    .line 197
    :cond_8
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 198
    invoke-virtual/range {p4 .. p4}, Ljkx;->a()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v7}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v7

    .line 197
    invoke-virtual {v5, v7}, Lbs;->b(Landroid/app/PendingIntent;)Lbs;

    .line 199
    if-eqz v6, :cond_9

    .line 200
    invoke-virtual {v5, v6}, Lbs;->a(Landroid/graphics/Bitmap;)Lbs;

    .line 202
    :cond_9
    if-eqz v4, :cond_a

    .line 203
    invoke-virtual {v5, v4}, Lbs;->a(Lce;)Lbs;

    .line 206
    :cond_a
    invoke-static {}, Ljkn;->a()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 207
    iget-object v4, v8, Llvo;->b:[Llvm;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v5, v4}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Lbs;Ljava/util/Collection;)V

    .line 208
    invoke-virtual/range {p4 .. p4}, Ljkx;->j()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Ljkn;->a(Lbs;Ljava/lang/String;)V

    :cond_b
    move-object v4, v5

    .line 210
    goto/16 :goto_0

    .line 169
    :cond_c
    iget-object v5, v9, Llvg;->b:[Llvb;

    array-length v5, v5

    const/4 v12, 0x1

    if-le v5, v12, :cond_7

    .line 172
    new-instance v4, Lbu;

    invoke-direct {v4}, Lbu;-><init>()V

    .line 173
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lbu;->b(Ljava/lang/CharSequence;)Lbu;

    .line 174
    iget-object v5, v8, Llvo;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lbu;->c(Ljava/lang/CharSequence;)Lbu;

    .line 175
    const-string v5, " "

    invoke-virtual {v4, v5}, Lbu;->c(Ljava/lang/CharSequence;)Lbu;

    .line 176
    iget-object v9, v9, Llvg;->b:[Llvb;

    array-length v12, v9

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v12, :cond_7

    aget-object v13, v9, v5

    .line 177
    iget-object v13, v13, Llvb;->b:Llvo;

    .line 178
    const v14, 0x7f0a03a6

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    iget-object v0, v13, Llvo;->c:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x1

    iget-object v13, v13, Llvo;->d:Ljava/lang/String;

    aput-object v13, v15, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Lbu;->c(Ljava/lang/CharSequence;)Lbu;

    .line 176
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_d
    move-object v6, v5

    goto/16 :goto_3
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Lbs;Ljava/util/Collection;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lbs;",
            "Ljava/util/Collection",
            "<",
            "Llvm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 357
    const-class v0, Linw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linw;

    .line 358
    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llvm;

    .line 359
    iget-object v1, v1, Llvm;->c:Ljava/lang/String;

    invoke-interface {v0, p2, v1}, Linw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 360
    if-eqz v1, :cond_0

    .line 361
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lbs;->b(Ljava/lang/String;)Lbs;

    goto :goto_0

    .line 364
    :cond_1
    return-void
.end method

.method private a(Lbs;Ljava/lang/String;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 372
    if-eqz p2, :cond_0

    .line 373
    invoke-virtual {p1, p2}, Lbs;->a(Ljava/lang/String;)Lbs;

    .line 375
    :cond_0
    return-void
.end method

.method private static a()Z
    .locals 2

    .prologue
    .line 381
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljiw;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 437
    sget-object v1, Ljko;->a:[I

    invoke-virtual {p1}, Ljiw;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 448
    :goto_0
    :pswitch_0
    return v0

    .line 441
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 437
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private varargs b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 406
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 407
    const-string v1, "com.google.android.libraries.social.notifications.NOTIFICATION_SELECTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    const-string v1, "notification_event_type"

    sget-object v2, Ljir;->b:Ljir;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 410
    const-string v1, "notification_key_list"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 411
    const-string v1, "effective_gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 412
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 413
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 320
    const/4 v6, 0x0

    .line 321
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 324
    const-class v0, Lizs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    .line 325
    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p1, p2, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 327
    const v3, 0x7f0d014f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 328
    const v4, 0x7f0d0150

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v4, v2

    .line 329
    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 339
    :goto_0
    return-object v0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    const-string v1, "SystemNotManager"

    const-string v2, "Bitmap Download Failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v6

    .line 337
    goto :goto_0

    .line 333
    :catch_1
    move-exception v0

    .line 334
    const-string v1, "SystemNotManager"

    const-string v2, "Bitmap Download Canceled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v6

    .line 337
    goto :goto_0

    .line 335
    :catch_2
    move-exception v0

    .line 336
    const-string v1, "SystemNotManager"

    const-string v2, "Bitmap Download OutOfMemoryError"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v0, v6

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljkx;)Lbs;
    .locals 19

    .prologue
    .line 218
    invoke-virtual/range {p4 .. p4}, Ljkx;->getCount()I

    move-result v4

    new-array v12, v4, [Ljava/lang/String;

    .line 219
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 220
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 221
    const/4 v8, 0x0

    .line 222
    const/4 v7, 0x0

    .line 223
    const/4 v6, 0x0

    .line 224
    new-instance v15, Lbu;

    invoke-direct {v15}, Lbu;-><init>()V

    .line 225
    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Lbu;->b(Ljava/lang/CharSequence;)Lbu;

    .line 226
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move v11, v7

    move-object/from16 v18, v6

    move-wide v6, v4

    move-object/from16 v4, v18

    move-object v5, v8

    .line 228
    :goto_0
    invoke-virtual/range {p4 .. p4}, Ljkx;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 229
    invoke-virtual/range {p4 .. p4}, Ljkx;->getPosition()I

    move-result v8

    invoke-virtual/range {p4 .. p4}, Ljkx;->a()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v12, v8

    .line 230
    invoke-virtual/range {p4 .. p4}, Ljkx;->c()Llvo;

    move-result-object v8

    .line 231
    if-eqz v8, :cond_1

    .line 233
    if-nez v4, :cond_0

    iget-object v9, v8, Llvo;->a:Llvi;

    if-eqz v9, :cond_0

    .line 234
    iget-object v4, v8, Llvo;->a:Llvi;

    iget-object v4, v4, Llvi;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 237
    :cond_0
    const v9, 0x7f0a03a6

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/16 v16, 0x0

    iget-object v0, v8, Llvo;->c:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v10, v16

    const/16 v16, 0x1

    iget-object v0, v8, Llvo;->d:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v10, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v15, v9}, Lbu;->c(Ljava/lang/CharSequence;)Lbu;

    .line 240
    iget-object v9, v8, Llvo;->b:[Llvm;

    array-length v10, v9

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v10, :cond_1

    aget-object v16, v9, v8

    .line 241
    move-object/from16 v0, v16

    iget-object v0, v0, Llvm;->c:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_1
    move-object v10, v4

    .line 245
    invoke-virtual/range {p4 .. p4}, Ljkx;->g()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 246
    const-wide/16 v16, 0x0

    cmp-long v4, v8, v16

    if-eqz v4, :cond_a

    cmp-long v4, v8, v6

    if-gez v4, :cond_a

    .line 250
    :goto_2
    invoke-virtual/range {p4 .. p4}, Ljkx;->f()I

    move-result v4

    const/4 v6, 0x3

    if-ne v4, v6, :cond_9

    .line 251
    const/4 v7, 0x1

    .line 254
    :goto_3
    invoke-virtual/range {p4 .. p4}, Ljkx;->j()Ljava/lang/String;

    move-result-object v6

    .line 255
    if-eqz v6, :cond_8

    .line 256
    invoke-virtual {v14, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v14, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    move v11, v4

    .line 257
    :goto_4
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v14, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    if-eqz v5, :cond_2

    invoke-virtual {v14, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v11, v4, :cond_8

    :cond_2
    move-object v4, v6

    :goto_5
    move v11, v7

    move-object v5, v4

    move-wide v6, v8

    move-object v4, v10

    .line 264
    goto/16 :goto_0

    .line 256
    :cond_3
    const/4 v4, 0x1

    move v11, v4

    goto :goto_4

    .line 267
    :cond_4
    new-instance v9, Lbs;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Lbs;-><init>(Landroid/content/Context;)V

    .line 268
    const v8, 0x7f0a03a7

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 269
    invoke-virtual/range {p4 .. p4}, Ljkx;->getCount()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v10, v14

    .line 268
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    .line 270
    const v8, 0x7f0a03a8

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p4 .. p4}, Ljkx;->getCount()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v10, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Lbs;->d(Ljava/lang/CharSequence;)Lbs;

    .line 273
    invoke-direct/range {p0 .. p1}, Ljkn;->a(Landroid/content/Context;)I

    move-result v8

    invoke-virtual {v9, v8}, Lbs;->a(I)Lbs;

    .line 274
    if-eqz v11, :cond_7

    const/4 v8, 0x1

    :goto_6
    invoke-virtual {v9, v8}, Lbs;->c(I)Lbs;

    .line 275
    invoke-virtual {v9, v15}, Lbs;->a(Lce;)Lbs;

    .line 276
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v12}, Ljkn;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v9, v8}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    .line 277
    invoke-virtual {v9, v6, v7}, Lbs;->a(J)Lbs;

    .line 278
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v12}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v9, v6}, Lbs;->b(Landroid/app/PendingIntent;)Lbs;

    .line 279
    if-eqz v4, :cond_5

    .line 280
    invoke-virtual {v9, v4}, Lbs;->a(Landroid/graphics/Bitmap;)Lbs;

    .line 283
    :cond_5
    invoke-static {}, Ljkn;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 284
    invoke-virtual {v13}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v9, v4}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Lbs;Ljava/util/Collection;)V

    .line 285
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v5}, Ljkn;->a(Lbs;Ljava/lang/String;)V

    .line 287
    :cond_6
    return-object v9

    .line 274
    :cond_7
    const/4 v8, 0x0

    goto :goto_6

    :cond_8
    move-object v4, v5

    goto/16 :goto_5

    :cond_9
    move v7, v11

    goto/16 :goto_3

    :cond_a
    move-wide v8, v6

    goto/16 :goto_2
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 60
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 62
    const-string v1, "read_state IN (1,4) AND priority IN (3,4) AND push_enabled <> 0"

    invoke-static {p1, p2, p3, v1}, Ljla;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljkx;

    move-result-object v3

    .line 65
    const-class v1, Ljjq;

    .line 66
    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljjq;

    .line 67
    invoke-virtual {v1, p2, p3}, Ljjq;->h(Ljava/lang/String;Ljava/lang/String;)Ljiw;

    move-result-object v4

    .line 69
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    const-class v1, Lhei;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    new-array v2, v8, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "logged_in"

    aput-object v6, v2, v5

    invoke-interface {v1, v2}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v6, "account_name"

    invoke-interface {v2, v6}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "effective_gaia_id"

    invoke-interface {v2, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-class v2, Ljjq;

    invoke-static {p1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljjq;

    invoke-virtual {v2, v6, v7}, Ljjq;->h(Ljava/lang/String;Ljava/lang/String;)Ljiw;

    move-result-object v2

    invoke-direct {p0, v2}, Ljkn;->a(Ljiw;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, p1, v6, v7}, Ljkn;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {v3}, Ljkx;->getCount()I

    move-result v1

    .line 72
    const-string v2, "SystemNotManager"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v5, 0x2c

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Found "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " push enabled notifications"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    :cond_2
    if-lez v1, :cond_5

    invoke-direct {p0, v4}, Ljkn;->a(Ljiw;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 76
    if-ne v1, v8, :cond_4

    .line 77
    invoke-direct {p0, p1, p2, p3, v3}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljkx;)Lbs;

    move-result-object v1

    .line 79
    :goto_1
    if-eqz v1, :cond_3

    .line 80
    invoke-static {p2, p3}, Ljkp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 81
    invoke-virtual {v1}, Lbs;->c()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v8, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 86
    :cond_3
    :goto_2
    return-void

    .line 78
    :cond_4
    invoke-direct {p0, p1, p2, p3, v3}, Ljkn;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljkx;)Lbs;

    move-result-object v1

    goto :goto_1

    .line 84
    :cond_5
    invoke-virtual {p0, p1, p2, p3}, Ljkn;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 93
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 95
    invoke-static {p2, p3}, Ljkp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 97
    return-void
.end method
