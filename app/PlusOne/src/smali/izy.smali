.class public final Lizy;
.super Lkds;
.source "PG"


# instance fields
.field public a:Lizu;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:Landroid/graphics/RectF;

.field public g:Lizo;

.field private i:Lizy;

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0}, Lkds;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lizy;->h:I

    return v0
.end method

.method public a(ILizu;IIIILandroid/graphics/RectF;Lizo;)V
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0, p1}, Lizy;->a(I)V

    .line 225
    iput-object p2, p0, Lizy;->a:Lizu;

    .line 226
    iput p3, p0, Lizy;->b:I

    .line 227
    iput p6, p0, Lizy;->c:I

    .line 228
    iput p4, p0, Lizy;->d:I

    .line 229
    iput p5, p0, Lizy;->e:I

    .line 230
    iput-object p7, p0, Lizy;->f:Landroid/graphics/RectF;

    .line 231
    iput-object p8, p0, Lizy;->g:Lizo;

    .line 232
    const/4 v0, 0x0

    iput v0, p0, Lizy;->j:I

    .line 233
    return-void
.end method

.method public a(Lizy;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lizy;->i:Lizy;

    .line 398
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 240
    iget v0, p0, Lizy;->d:I

    iget v1, p0, Lizy;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 241
    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryBPixels:I
    invoke-static {}, Lcom/google/android/libraries/social/media/MediaResource;->access$000()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 242
    :cond_0
    const/4 v0, 0x6

    .line 246
    :goto_0
    return v0

    .line 243
    :cond_1
    # getter for: Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryCPixels:I
    invoke-static {}, Lcom/google/android/libraries/social/media/MediaResource;->access$100()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 244
    const/4 v0, 0x7

    goto :goto_0

    .line 246
    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public c()Lizy;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lizy;->i:Lizy;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 263
    if-ne p1, p0, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v0

    .line 267
    :cond_1
    instance-of v2, p1, Lizy;

    if-nez v2, :cond_2

    move v0, v1

    .line 268
    goto :goto_0

    .line 271
    :cond_2
    check-cast p1, Lizy;

    .line 272
    iget v2, p0, Lizy;->h:I

    iget v3, p1, Lizy;->h:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lizy;->c:I

    iget v3, p1, Lizy;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lizy;->a:Lizu;

    iget-object v3, p1, Lizy;->a:Lizu;

    .line 274
    invoke-virtual {v2, v3}, Lizu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 275
    iget v2, p0, Lizy;->b:I

    iget v3, p1, Lizy;->b:I

    if-eq v2, v3, :cond_4

    move v2, v1

    :goto_1
    if-eqz v2, :cond_3

    iget-object v2, p0, Lizy;->g:Lizo;

    iget-object v3, p1, Lizy;->g:Lizo;

    .line 276
    if-nez v2, :cond_7

    if-nez v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_3

    iget-object v2, p0, Lizy;->f:Landroid/graphics/RectF;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lizy;->f:Landroid/graphics/RectF;

    iget-object v3, p1, Lizy;->f:Landroid/graphics/RectF;

    .line 277
    invoke-virtual {v2, v3}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 275
    :cond_4
    iget v2, p0, Lizy;->b:I

    sparse-switch v2, :sswitch_data_0

    :cond_5
    move v2, v0

    goto :goto_1

    :sswitch_0
    invoke-virtual {p0}, Lizy;->b()I

    move-result v2

    invoke-virtual {p1}, Lizy;->b()I

    move-result v3

    if-eq v2, v3, :cond_5

    move v2, v1

    goto :goto_1

    :sswitch_1
    iget v2, p0, Lizy;->d:I

    iget v3, p1, Lizy;->d:I

    if-ne v2, v3, :cond_6

    iget v2, p0, Lizy;->e:I

    iget v3, p1, Lizy;->e:I

    if-eq v2, v3, :cond_5

    :cond_6
    move v2, v1

    goto :goto_1

    .line 276
    :cond_7
    if-nez v2, :cond_8

    if-nez v3, :cond_9

    :cond_8
    if-eqz v2, :cond_a

    if-nez v3, :cond_a

    :cond_9
    move v2, v1

    goto :goto_2

    :cond_a
    invoke-interface {v2, v3}, Lizo;->a(Lizo;)Z

    move-result v2

    goto :goto_2

    .line 275
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 252
    iget v0, p0, Lizy;->j:I

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lizy;->a:Lizu;

    iget v1, p0, Lizy;->b:I

    iget-object v2, p0, Lizy;->f:Landroid/graphics/RectF;

    iget v3, p0, Lizy;->h:I

    .line 256
    add-int/lit16 v3, v3, 0x20f

    .line 255
    invoke-static {v2, v3}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v2

    .line 254
    mul-int/lit8 v2, v2, 0x1f

    add-int/2addr v1, v2

    .line 253
    invoke-static {v0, v1}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v0

    iput v0, p0, Lizy;->j:I

    .line 258
    :cond_0
    iget v0, p0, Lizy;->j:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 308
    const/4 v1, 0x0

    .line 309
    const-string v0, ""

    .line 310
    iget v2, p0, Lizy;->b:I

    packed-switch v2, :pswitch_data_0

    .line 333
    :cond_0
    :goto_0
    const-string v2, ""

    .line 334
    iget-object v3, p0, Lizy;->f:Landroid/graphics/RectF;

    if-eqz v3, :cond_1

    .line 335
    iget-object v2, p0, Lizy;->f:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lizy;->f:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lizy;->f:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lizy;->f:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x4a

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, ", crop("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 338
    :cond_1
    iget-object v3, p0, Lizy;->g:Lizo;

    if-eqz v3, :cond_14

    const/4 v3, 0x1

    .line 339
    :goto_1
    const-string v4, ""

    .line 340
    iget v5, p0, Lizy;->h:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_2

    .line 341
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " no-disk-cache"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 343
    :cond_2
    iget v5, p0, Lizy;->h:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_3

    .line 344
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " download-only"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 346
    :cond_3
    iget v5, p0, Lizy;->h:I

    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_4

    .line 347
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " accept-animation"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 349
    :cond_4
    iget v5, p0, Lizy;->h:I

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_5

    .line 350
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " disable-decoding"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 352
    :cond_5
    iget v5, p0, Lizy;->h:I

    and-int/lit8 v5, v5, 0x10

    if-eqz v5, :cond_6

    .line 353
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " disable-recycling"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 355
    :cond_6
    iget v5, p0, Lizy;->h:I

    and-int/lit8 v5, v5, 0x20

    if-eqz v5, :cond_7

    .line 356
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " disable-webp"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 358
    :cond_7
    iget v5, p0, Lizy;->h:I

    and-int/lit8 v5, v5, 0x40

    if-eqz v5, :cond_8

    .line 359
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " accept-bitmap-container"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 361
    :cond_8
    iget v5, p0, Lizy;->h:I

    and-int/lit16 v5, v5, 0x80

    if-eqz v5, :cond_9

    .line 362
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " disable-loading"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 364
    :cond_9
    iget v5, p0, Lizy;->h:I

    and-int/lit16 v5, v5, 0x100

    if-eqz v5, :cond_a

    .line 365
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " disable-smart-crop"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 367
    :cond_a
    iget v5, p0, Lizy;->h:I

    and-int/lit16 v5, v5, 0x200

    if-eqz v5, :cond_b

    .line 368
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " disable-upscale"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 370
    :cond_b
    iget v5, p0, Lizy;->h:I

    and-int/lit16 v5, v5, 0x400

    if-eqz v5, :cond_c

    .line 371
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " long-term-cache"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 373
    :cond_c
    iget v5, p0, Lizy;->h:I

    and-int/lit16 v5, v5, 0x800

    if-eqz v5, :cond_d

    .line 374
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " keep-partial-download"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 376
    :cond_d
    iget v5, p0, Lizy;->h:I

    and-int/lit16 v5, v5, 0x1000

    if-eqz v5, :cond_e

    .line 377
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " disable-automatic-high-res-download"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 379
    :cond_e
    iget v5, p0, Lizy;->h:I

    and-int/lit16 v5, v5, 0x2000

    if-eqz v5, :cond_f

    .line 380
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " allow-large-file-download"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 382
    :cond_f
    iget v5, p0, Lizy;->h:I

    and-int/lit16 v5, v5, 0x4000

    if-eqz v5, :cond_10

    .line 383
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " for-media-sync"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 385
    :cond_10
    iget v5, p0, Lizy;->h:I

    const v6, 0x8000

    and-int/2addr v5, v6

    if-eqz v5, :cond_11

    .line 386
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " prefer-high-res-download"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 388
    :cond_11
    iget v5, p0, Lizy;->h:I

    const/high16 v6, 0x10000

    and-int/2addr v5, v6

    if-eqz v5, :cond_12

    .line 389
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " convert-webp-to-jpeg"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 392
    :cond_12
    iget-object v5, p0, Lizy;->a:Lizu;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x16

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "{"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") hasEdits: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 312
    :pswitch_0
    iget v1, p0, Lizy;->d:I

    iget v2, p0, Lizy;->e:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x17

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "x"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 313
    iget v2, p0, Lizy;->c:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 314
    const-string v2, "-"

    iget v0, p0, Lizy;->c:I

    packed-switch v0, :pswitch_data_1

    const-string v0, ""

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1
    const-string v0, "H"

    goto :goto_2

    :pswitch_2
    const-string v0, "S"

    goto :goto_2

    :pswitch_3
    const-string v0, "L"

    goto :goto_2

    :pswitch_4
    const-string v0, "VL"

    goto :goto_2

    :cond_13
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 318
    :pswitch_5
    const-string v1, "thumbnail"

    goto/16 :goto_0

    .line 321
    :pswitch_6
    const-string v1, "large"

    goto/16 :goto_0

    .line 324
    :pswitch_7
    const-string v1, "full"

    goto/16 :goto_0

    .line 327
    :pswitch_8
    const-string v1, "original"

    goto/16 :goto_0

    .line 330
    :pswitch_9
    iget v1, p0, Lizy;->d:I

    iget v2, p0, Lizy;->e:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "auto("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "x"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 338
    :cond_14
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 314
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
