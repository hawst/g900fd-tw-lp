.class public Lcvq;
.super Llol;
.source "PG"

# interfaces
.implements Llgs;


# static fields
.field private static final N:Ldgq;


# instance fields
.field private O:Ldgi;

.field private P:Ldgr;

.field private Q:Ldeo;

.field private R:Ljava/lang/Integer;

.field private final S:Lcvt;

.field private final T:Lcvs;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ldgq;

    const v1, 0x7f1006c2

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcvq;->N:Ldgq;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0}, Llol;-><init>()V

    .line 56
    new-instance v0, Lcvt;

    invoke-direct {v0, p0}, Lcvt;-><init>(Lcvq;)V

    iput-object v0, p0, Lcvq;->S:Lcvt;

    .line 57
    new-instance v0, Lcvs;

    invoke-direct {v0, p0}, Lcvs;-><init>(Lcvq;)V

    iput-object v0, p0, Lcvq;->T:Lcvs;

    .line 60
    new-instance v0, Ldep;

    iget-object v1, p0, Lcvq;->av:Llqm;

    new-instance v2, Lcvr;

    invoke-direct {v2, p0}, Lcvr;-><init>(Lcvq;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 242
    return-void
.end method

.method static synthetic a(Lcvq;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcvq;->R:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    :try_start_0
    const-string v0, "download"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v8}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 129
    if-eqz p2, :cond_0

    move v0, v1

    .line 130
    :goto_0
    iget-object v2, p0, Lcvq;->Q:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2, v0}, Lddl;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_2

    .line 132
    invoke-virtual {p0}, Lcvq;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "account_id"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 134
    const v2, 0x7f0a05f2

    .line 135
    invoke-virtual {p0, v2}, Lcvq;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 134
    invoke-static {p1, v1, v0, p2, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;ZLjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcvq;->R:Ljava/lang/Integer;

    .line 136
    iget-object v0, p0, Lcvq;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a05ee

    .line 138
    :goto_1
    invoke-virtual {p0}, Lcvq;->p()Lae;

    move-result-object v1

    invoke-virtual {p0, v0}, Lcvq;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldhh;->a(Lae;Ljava/lang/String;)V

    .line 143
    :goto_2
    return-void

    .line 129
    :cond_0
    const/16 v0, 0x800

    goto :goto_0

    .line 136
    :cond_1
    const v0, 0x7f0a05ed

    goto :goto_1

    .line 140
    :cond_2
    invoke-virtual {p0}, Lcvq;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a05f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 141
    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method static synthetic a(Lcvq;)V
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcvq;->P:Ldgr;

    sget-object v1, Lcvq;->N:Ldgq;

    iget-object v2, p0, Lcvq;->Q:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->y()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    return-void
.end method

.method static synthetic a(Lcvq;Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcvq;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method static synthetic a(Lcvq;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 39
    iget-object v0, p0, Lcvq;->O:Ldgi;

    invoke-interface {v0}, Ldgi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const v0, 0x7f0a05ef

    invoke-virtual {p0, v0}, Lcvq;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a07fa

    invoke-virtual {p0, v1}, Lcvq;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a07fd

    invoke-virtual {p0, v2}, Lcvq;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v1, v2}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Lcvq;->p()Lae;

    move-result-object v1

    const-string v2, "download_failed"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0a05f0

    invoke-virtual {p0, v0}, Lcvq;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0596

    invoke-virtual {p0, v1}, Lcvq;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v3, v1}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcvq;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcvq;->R:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 181
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lcvq;->P:Ldgr;

    sget-object v1, Lcvq;->N:Ldgq;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 82
    if-nez p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcvq;->R:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 169
    const-string v0, "download_failed"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcvq;->n()Lz;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcvq;->a(Landroid/content/Context;Z)V

    .line 172
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 101
    invoke-super {p0}, Llol;->aO_()V

    .line 102
    iget-object v0, p0, Lcvq;->P:Ldgr;

    sget-object v1, Lcvq;->N:Ldgq;

    iget-object v2, p0, Lcvq;->T:Lcvs;

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Ldgv;)V

    .line 103
    iget-object v0, p0, Lcvq;->S:Lcvt;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 104
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 71
    iget-object v0, p0, Lcvq;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Lcvq;->O:Ldgi;

    .line 72
    iget-object v0, p0, Lcvq;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcvq;->P:Ldgr;

    .line 73
    iget-object v0, p0, Lcvq;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcvq;->Q:Ldeo;

    .line 74
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 94
    iget-object v0, p0, Lcvq;->R:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "pending_request_id"

    iget-object v1, p0, Lcvq;->R:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    :cond_0
    return-void
.end method

.method public z()V
    .locals 3

    .prologue
    .line 108
    invoke-super {p0}, Llol;->z()V

    .line 109
    iget-object v0, p0, Lcvq;->P:Ldgr;

    sget-object v1, Lcvq;->N:Ldgq;

    iget-object v2, p0, Lcvq;->T:Lcvs;

    invoke-virtual {v0, v1, v2}, Ldgr;->b(Ldgu;Ldgv;)V

    .line 110
    iget-object v0, p0, Lcvq;->S:Lcvt;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 111
    return-void
.end method
