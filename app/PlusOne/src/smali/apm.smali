.class public final Lapm;
.super Lc;
.source "PG"

# interfaces
.implements Llnk;


# static fields
.field private static a:Lapm;


# instance fields
.field private b:Lamm;

.field private c:Llnh;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lc;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lapm;
    .locals 3

    .prologue
    .line 36
    sget-object v0, Lapm;->a:Lapm;

    if-nez v0, :cond_0

    .line 37
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 38
    instance-of v1, v0, Lapm;

    if-eqz v1, :cond_1

    .line 39
    check-cast v0, Lapm;

    sput-object v0, Lapm;->a:Lapm;

    .line 46
    :cond_0
    :goto_0
    sget-object v0, Lapm;->a:Lapm;

    return-object v0

    .line 41
    :cond_1
    new-instance v1, Lapm;

    invoke-direct {v1}, Lapm;-><init>()V

    .line 42
    sput-object v1, Lapm;->a:Lapm;

    new-instance v2, Lamo;

    invoke-direct {v2, v0}, Lamo;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, Lapm;->b:Lamm;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    .line 97
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    .line 99
    if-eqz p1, :cond_0

    .line 100
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyDeath()Landroid/os/StrictMode$ThreadPolicy$Builder;

    .line 102
    :cond_0
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 103
    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    .line 105
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    .line 103
    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 108
    return-void
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 131
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0}, Ljfb;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lamm;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lapm;->b:Lamm;

    return-object v0
.end method

.method public h_()Llnh;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lapm;->c:Llnh;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lapm;->a(Z)V

    .line 62
    invoke-virtual {p0}, Lapm;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 64
    new-instance v1, Llnh;

    invoke-direct {v1, v0}, Llnh;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lapm;->c:Llnh;

    .line 65
    iget-object v1, p0, Lapm;->c:Llnh;

    new-instance v2, Llnr;

    invoke-direct {v2, v0}, Llnr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Llnh;->a(Llnq;)Llnh;

    .line 68
    const-class v1, Lhmp;

    invoke-static {v0, v1}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lapm;->c:Llnh;

    const-class v2, Lhlw;

    new-instance v3, Lapn;

    invoke-direct {v3, v0}, Lapn;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2, v3}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 83
    invoke-super {p0}, Lc;->onCreate()V

    .line 86
    new-instance v0, Lamo;

    invoke-direct {v0, p0}, Lamo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lapm;->b:Lamm;

    .line 87
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lapm;->a(Z)V

    .line 88
    return-void
.end method

.method public onTerminate()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 112
    iput-object v0, p0, Lapm;->b:Lamm;

    .line 113
    iput-object v0, p0, Lapm;->c:Llnh;

    .line 114
    invoke-super {p0}, Lc;->onTerminate()V

    .line 115
    return-void
.end method
