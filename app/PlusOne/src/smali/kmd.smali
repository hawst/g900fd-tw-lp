.class public final Lkmd;
.super Lkcw;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lmhi;

.field private final d:Ljava/util/Date;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljac;",
            ">;"
        }
    .end annotation
.end field

.field private i:J

.field private j:J

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Landroid/graphics/Bitmap;

.field private q:I

.field private r:Z

.field private s:I

.field private t:J

.field private u:J

.field private v:Lkey;

.field private w:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 149
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "media_url"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "bytes_uploaded"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bytes_total"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "upload_state"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "upload_finish_time"

    aput-object v2, v0, v1

    sput-object v0, Lkmd;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILmhi;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lmhi;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 221
    invoke-direct {p0, p2}, Lkcw;-><init>(I)V

    .line 222
    iput-object p1, p0, Lkmd;->b:Landroid/content/Context;

    .line 223
    iput-object p3, p0, Lkmd;->c:Lmhi;

    .line 224
    iput-object p4, p0, Lkmd;->d:Ljava/util/Date;

    .line 225
    iput-object p5, p0, Lkmd;->e:Ljava/lang/String;

    .line 226
    iput-object p6, p0, Lkmd;->f:Ljava/util/List;

    .line 227
    if-eqz p6, :cond_1

    .line 228
    invoke-interface {p6}, Ljava/util/List;->size()I

    move-result v0

    .line 229
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lkmd;->g:Ljava/util/Set;

    .line 230
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lkmd;->h:Ljava/util/Map;

    .line 231
    invoke-direct {p0}, Lkmd;->d()V

    .line 236
    :goto_0
    new-instance v1, Lkcx;

    .line 237
    iget-object v0, p0, Lkmd;->b:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-virtual {p0}, Lkmd;->a()I

    move-result v2

    invoke-interface {v0, v2}, Lhei;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lkmd;->a()I

    move-result v2

    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "display_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-direct {v1}, Lkcx;-><init>()V

    .line 236
    invoke-virtual {p0, v1}, Lkmd;->a(Lkcx;)V

    .line 238
    return-void

    .line 233
    :cond_1
    iput-object v0, p0, Lkmd;->g:Ljava/util/Set;

    .line 234
    iput-object v0, p0, Lkmd;->h:Ljava/util/Map;

    goto :goto_0
.end method

.method static synthetic a(Lkmd;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lkmd;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lkmd;Lkey;)Lkey;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lkmd;->v:Lkey;

    return-object p1
.end method

.method static synthetic a(Landroid/content/Context;Ljava/io/DataInputStream;)Lkmd;
    .locals 1

    .prologue
    .line 146
    invoke-static {p0, p1}, Lkmd;->b(Landroid/content/Context;Ljava/io/DataInputStream;)Lkmd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkmd;)V
    .locals 12

    .prologue
    .line 146
    iget-object v0, p0, Lkmd;->f:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lkmd;->s:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lkmd;->b:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-virtual {p0}, Lkmd;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    iput v0, p0, Lkmd;->s:I

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lkmd;->a()I

    move-result v3

    iget-object v4, p0, Lkmd;->f:Ljava/util/List;

    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v0, "media_url IN ( "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_4

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {v0}, Lizu;->j()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v1, 0x1

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "\', "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    move v0, v1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    const/4 v3, 0x0

    :goto_2
    if-nez v3, :cond_6

    const/4 v0, 0x2

    iput v0, p0, Lkmd;->s:I

    goto :goto_0

    :cond_5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v0, " ) AND upload_account_id = "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {v5}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lkmd;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lkmd;->b:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->e(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lkmd;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    const/4 v4, 0x0

    if-nez v7, :cond_7

    :try_start_0
    const-string v0, "ShareQueue"

    const-string v1, "No media was found for query. Marking item upload status as done."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    iput v0, p0, Lkmd;->s:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_7
    const-wide/16 v2, 0x0

    const-wide/16 v0, 0x0

    move v6, v4

    move-wide v4, v2

    move-wide v2, v0

    :cond_8
    :goto_3
    :sswitch_0
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-lez v8, :cond_a

    :goto_4
    add-long/2addr v2, v0

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    add-long/2addr v4, v0

    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown upload state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    if-eqz v7, :cond_9

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    :cond_a
    const-wide/32 v0, 0x7d000

    goto :goto_4

    :sswitch_1
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkmd;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x4

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iget-wide v10, p0, Lkmd;->t:J

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    iput-wide v8, p0, Lkmd;->t:J

    iget-object v1, p0, Lkmd;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljac;

    sget-object v1, Lkmc;->a:[I

    invoke-virtual {v0}, Ljac;->ordinal()I

    move-result v8

    aget v1, v1, v8

    packed-switch v1, :pswitch_data_0

    const-string v1, "ShareQueue"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x14

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Unknown media type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :pswitch_0
    iget v0, p0, Lkmd;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkmd;->m:I

    goto/16 :goto_3

    :pswitch_1
    iget v0, p0, Lkmd;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkmd;->o:I

    goto/16 :goto_3

    :sswitch_2
    add-int/lit8 v0, v6, 0x1

    const-string v1, "ShareQueue"

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v8, 0x39

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Upload failed for media. mediaUploadFailCount="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v0

    goto/16 :goto_3

    :sswitch_3
    const-string v0, "ShareQueue"

    const-string v1, "Media upload state is STATE_DONT_UPLOAD. This should never happen."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_b
    iput-wide v2, p0, Lkmd;->i:J

    iput-wide v4, p0, Lkmd;->j:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v7, :cond_c

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_c
    invoke-direct {p0}, Lkmd;->g()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lkmd;->b:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v1, Lkmx;->f:Lief;

    invoke-virtual {p0}, Lkmd;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    const/4 v0, 0x2

    :goto_5
    iput v0, p0, Lkmd;->s:I

    goto/16 :goto_0

    :cond_e
    iget v0, p0, Lkmd;->n:I

    if-lez v0, :cond_f

    iget-wide v0, p0, Lkmd;->t:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lkmd;->t:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xdbba0

    sub-long v0, v2, v0

    iput-wide v0, p0, Lkmd;->u:J

    :goto_6
    iget-wide v0, p0, Lkmd;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_d

    const/4 v0, 0x1

    goto :goto_5

    :cond_f
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkmd;->u:J

    goto :goto_6

    :cond_10
    if-lez v6, :cond_11

    const/4 v0, 0x3

    iput v0, p0, Lkmd;->s:I

    goto/16 :goto_0

    :cond_11
    iget-object v0, p0, Lkmd;->w:Ljava/lang/String;

    if-nez v0, :cond_12

    const/4 v0, 0x4

    iput v0, p0, Lkmd;->s:I

    goto/16 :goto_0

    :cond_12
    const/4 v0, 0x0

    iput v0, p0, Lkmd;->s:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_2
        0x190 -> :sswitch_1
        0x1f4 -> :sswitch_3
        0x258 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lkmd;Ljava/io/DataOutputStream;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 146
    invoke-virtual {p0}, Lkmd;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p0, Lkmd;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-object v0, p0, Lkmd;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p0, Lkmd;->c:Lmhi;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->write([B)V

    iget-object v0, p0, Lkmd;->w:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkmd;->w:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p0, Lkmd;->f:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkmd;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    :goto_1
    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    :goto_2
    if-ge v2, v1, :cond_3

    iget-object v0, p0, Lkmd;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {v0}, Lizu;->j()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v3

    iget v3, v3, Ljac;->e:I

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    return-void
.end method

.method static synthetic a(Lkmd;Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    .line 146
    invoke-direct {p0}, Lkmd;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lkmd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x64

    :goto_0
    invoke-virtual {p0}, Lkmd;->b()Lkcx;

    move-result-object v1

    invoke-direct {p0}, Lkmd;->e()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v0, p1, v2, p2}, Lkcx;->a(ILjava/lang/String;Landroid/graphics/Bitmap;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lkmd;->j:J

    long-to-float v0, v0

    iget-wide v2, p0, Lkmd;->i:J

    long-to-float v1, v2

    invoke-static {v0, v1}, Lkly;->a(FF)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lkmd;Z)Z
    .locals 0

    .prologue
    .line 146
    iput-boolean p1, p0, Lkmd;->r:Z

    return p1
.end method

.method static synthetic b(Lkmd;)I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lkmd;->s:I

    return v0
.end method

.method private static b(Landroid/content/Context;Ljava/io/DataInputStream;)Lkmd;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 547
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 548
    new-instance v4, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 549
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v5

    .line 550
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 551
    new-array v3, v1, [B

    .line 552
    invoke-virtual {p1, v3}, Ljava/io/DataInputStream;->readFully([B)V

    .line 553
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v7

    .line 556
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    .line 557
    if-lez v8, :cond_0

    .line 558
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 559
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v8, :cond_1

    .line 560
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    invoke-static {v9}, Ljac;->a(I)Ljac;

    move-result-object v9

    .line 561
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v10

    .line 562
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {p0, v10, v9}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v9

    .line 563
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 559
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move-object v6, v0

    .line 571
    :cond_1
    :try_start_0
    new-instance v1, Lmhi;

    invoke-direct {v1}, Lmhi;-><init>()V

    invoke-static {v1, v3}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v3

    check-cast v3, Lmhi;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 578
    new-instance v0, Lkmd;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lkmd;-><init>(Landroid/content/Context;ILmhi;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;)V

    .line 580
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 581
    iput-object v7, v0, Lkmd;->w:Ljava/lang/String;

    .line 583
    :cond_2
    :goto_1
    return-object v0

    .line 572
    :catch_0
    move-exception v1

    .line 573
    const-string v2, "ShareQueue"

    const-string v3, "Error merging PostActivityRequest from post bytes. Removing item file and skipping item."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method static synthetic c(Lkmd;)I
    .locals 2

    .prologue
    .line 146
    iget v0, p0, Lkmd;->q:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lkmd;->q:I

    return v0
.end method

.method private d()V
    .locals 6

    .prologue
    .line 260
    const/4 v0, 0x0

    iget-object v1, p0, Lkmd;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 261
    iget-object v0, p0, Lkmd;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 262
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 263
    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v3

    .line 266
    iget-object v4, p0, Lkmd;->h:Ljava/util/Map;

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lkmc;->a:[I

    invoke-virtual {v3}, Ljac;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 277
    const-string v0, "ShareQueue"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unknown media type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 271
    :pswitch_0
    iget v0, p0, Lkmd;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkmd;->l:I

    goto :goto_1

    .line 274
    :pswitch_1
    iget v0, p0, Lkmd;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkmd;->n:I

    goto :goto_1

    .line 280
    :cond_1
    return-void

    .line 267
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic d(Lkmd;)V
    .locals 3

    .prologue
    .line 146
    new-instance v0, Lkme;

    iget-object v1, p0, Lkmd;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lkmd;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lkme;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lkme;->l()V

    const-string v1, "ShareQueue"

    invoke-virtual {v0, v1}, Lkme;->e(Ljava/lang/String;)V

    invoke-virtual {v0}, Lkme;->D()Loxu;

    move-result-object v0

    check-cast v0, Llzh;

    iget-object v0, v0, Llzh;->a:Lnff;

    iget-object v0, v0, Lnff;->a:Lnyb;

    iget-object v0, v0, Lnyb;->d:Ljava/lang/String;

    iput-object v0, p0, Lkmd;->w:Ljava/lang/String;

    return-void
.end method

.method private e()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 324
    iget-object v1, p0, Lkmd;->p:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 325
    iget-object v0, p0, Lkmd;->p:Landroid/graphics/Bitmap;

    .line 354
    :goto_0
    return-object v0

    .line 330
    :cond_0
    iget-object v1, p0, Lkmd;->f:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 331
    iget-object v1, p0, Lkmd;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 332
    iget-object v0, p0, Lkmd;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 333
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lkmd;->g:Ljava/util/Set;

    .line 334
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    move-object v1, v0

    .line 340
    :goto_2
    if-nez v1, :cond_2

    move-object v0, v6

    .line 341
    goto :goto_0

    .line 331
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 347
    :cond_2
    :try_start_0
    iget-object v0, p0, Lkmd;->b:Landroid/content/Context;

    const-class v2, Lizs;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    .line 348
    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lkmd;->p:Landroid/graphics/Bitmap;

    .line 350
    iget-object v0, p0, Lkmd;->p:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 352
    :catch_0
    move-exception v0

    move-object v0, v6

    goto :goto_0

    .line 354
    :catch_1
    move-exception v0

    move-object v0, v6

    goto :goto_0

    :cond_3
    move-object v1, v6

    goto :goto_2
.end method

.method static synthetic e(Lkmd;)Ljava/util/List;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkmd;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lkmd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkmd;->w:Ljava/lang/String;

    return-object v0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 510
    iget v0, p0, Lkmd;->l:I

    if-gtz v0, :cond_0

    iget v0, p0, Lkmd;->n:I

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lkmd;)I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lkmd;->q:I

    return v0
.end method

.method private g()Z
    .locals 3

    .prologue
    .line 515
    iget-object v0, p0, Lkmd;->g:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkmd;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget v1, p0, Lkmd;->l:I

    iget v2, p0, Lkmd;->n:I

    add-int/2addr v1, v2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic h(Lkmd;)I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lkmd;->l:I

    return v0
.end method

.method static synthetic i(Lkmd;)I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lkmd;->m:I

    return v0
.end method

.method static synthetic j(Lkmd;)I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lkmd;->n:I

    return v0
.end method

.method static synthetic k(Lkmd;)I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lkmd;->o:I

    return v0
.end method

.method static synthetic l(Lkmd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkmd;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lkmd;)Ljava/util/Date;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkmd;->d:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic n(Lkmd;)Lmhi;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkmd;->c:Lmhi;

    return-object v0
.end method

.method static synthetic o(Lkmd;)Lkey;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkmd;->v:Lkey;

    return-object v0
.end method

.method static synthetic p(Lkmd;)Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lkmd;->r:Z

    return v0
.end method

.method static synthetic q(Lkmd;)J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lkmd;->u:J

    return-wide v0
.end method

.method static synthetic r(Lkmd;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Lkmd;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lkmd;)J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lkmd;->i:J

    return-wide v0
.end method

.method static synthetic t(Lkmd;)J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lkmd;->j:J

    return-wide v0
.end method

.method static synthetic u(Lkmd;)Z
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Lkmd;->g()Z

    move-result v0

    return v0
.end method

.method static synthetic v(Lkmd;)Z
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Lkmd;->f()Z

    move-result v0

    return v0
.end method
