.class public final Lgbl;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Llcz;
.implements Lljh;


# static fields
.field private static m:Llct;


# instance fields
.field private a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private b:Landroid/text/StaticLayout;

.field private c:Landroid/text/StaticLayout;

.field private d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

.field private e:I

.field private f:Llan;

.field private g:Llci;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgbl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgbl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 74
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    iput-boolean v1, p0, Lgbl;->l:Z

    .line 75
    sget-object v0, Lgbl;->m:Llct;

    if-nez v0, :cond_0

    .line 76
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lgbl;->m:Llct;

    .line 79
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbl;->setWillNotDraw(Z)V

    .line 80
    invoke-virtual {p0, v1}, Lgbl;->setFocusable(Z)V

    .line 81
    invoke-virtual {p0, v1}, Lgbl;->setClickable(Z)V

    .line 82
    invoke-virtual {p0, p0}, Lgbl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 85
    iget-object v0, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 86
    iget-object v0, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 87
    iget-object v0, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    new-instance v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 90
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 92
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 93
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 117
    sget-object v0, Lgbl;->m:Llct;

    if-nez v0, :cond_0

    .line 118
    invoke-static {p0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lgbl;->m:Llct;

    .line 121
    :cond_0
    invoke-static {p0}, Lhss;->c(Landroid/content/Context;)I

    move-result v0

    .line 122
    const/16 v1, 0x10

    invoke-static {p0, v1}, Llib;->a(Landroid/content/Context;I)I

    move-result v1

    const/16 v2, 0xa

    .line 124
    invoke-static {p0, v2}, Llib;->a(Landroid/content/Context;I)I

    move-result v2

    add-int/2addr v1, v2

    .line 127
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget-object v1, Lgbl;->m:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    iget v0, p0, Lgbl;->h:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lgbl;->h:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    :cond_0
    const v0, 0x7f0a0821

    .line 260
    :goto_0
    invoke-virtual {p0}, Lgbl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 257
    :cond_1
    const v0, 0x7f0a0800

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 265
    invoke-virtual {p0}, Lgbl;->clearAnimation()V

    .line 266
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    .line 268
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lgbl;->setAlpha(F)V

    .line 271
    :cond_0
    iget-object v0, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 273
    iput-object v1, p0, Lgbl;->b:Landroid/text/StaticLayout;

    .line 274
    iput-object v1, p0, Lgbl;->c:Landroid/text/StaticLayout;

    .line 275
    iput v2, p0, Lgbl;->e:I

    .line 276
    iput-object v1, p0, Lgbl;->f:Llan;

    .line 277
    iput v2, p0, Lgbl;->h:I

    .line 278
    iput-object v1, p0, Lgbl;->g:Llci;

    .line 279
    iput-object v1, p0, Lgbl;->i:Ljava/lang/String;

    .line 280
    iput-boolean v2, p0, Lgbl;->k:Z

    .line 281
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 356
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/util/List;)V

    .line 357
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbl;->l:Z

    .line 359
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 360
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-direct {p0}, Lgbl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    .line 364
    :goto_0
    invoke-virtual {p0}, Lgbl;->drawableStateChanged()V

    .line 365
    return-void

    .line 362
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbl;->l:Z

    goto :goto_0
.end method

.method public a(Llch;Llan;ILlci;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 98
    invoke-virtual {p1}, Llch;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbl;->i:Ljava/lang/String;

    .line 99
    iput-object p2, p0, Lgbl;->f:Llan;

    .line 100
    iput p3, p0, Lgbl;->h:I

    .line 101
    iput-object p4, p0, Lgbl;->g:Llci;

    .line 102
    iput-object p5, p0, Lgbl;->j:Ljava/lang/String;

    .line 103
    iput-boolean p6, p0, Lgbl;->k:Z

    .line 105
    invoke-virtual {p0}, Lgbl;->removeAllViews()V

    .line 107
    iget-object v0, p0, Lgbl;->f:Llan;

    invoke-virtual {v0}, Llan;->b()Llag;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Llag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Llag;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lgbl;->addView(Landroid/view/View;)V

    .line 111
    invoke-direct {p0}, Lgbl;->g()Ljava/lang/String;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {p0, v0}, Lgbl;->addView(Landroid/view/View;)V

    .line 114
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lgbl;->f:Llan;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgbl;->f:Llan;

    invoke-virtual {v0}, Llan;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lgbl;->f:Llan;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgbl;->f:Llan;

    invoke-virtual {v0}, Llan;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x1

    return v0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 322
    invoke-virtual {p0}, Lgbl;->invalidate()V

    .line 323
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 324
    return-void
.end method

.method public e()V
    .locals 5

    .prologue
    .line 344
    iget-object v0, p0, Lgbl;->g:Llci;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lgbl;->g:Llci;

    iget-object v1, p0, Lgbl;->f:Llan;

    invoke-virtual {v1}, Llan;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgbl;->f:Llan;

    .line 346
    invoke-virtual {v2}, Llan;->c()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lgbl;->h:I

    iget-object v4, p0, Lgbl;->i:Ljava/lang/String;

    .line 345
    invoke-interface {v0, v1, v2, v3, v4}, Llci;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 348
    :cond_0
    return-void
.end method

.method public f()Llja;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lgbl;->f:Llan;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 5
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 180
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 181
    iget-object v1, p0, Lgbl;->b:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    .line 182
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lgbl;->b:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 184
    :cond_0
    iget-object v1, p0, Lgbl;->c:Landroid/text/StaticLayout;

    if-eqz v1, :cond_1

    .line 185
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lgbl;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 187
    :cond_1
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 298
    iget-object v0, p0, Lgbl;->g:Llci;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbl;->f:Llan;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbl;->f:Llan;

    invoke-virtual {v0}, Llan;->b()Llag;

    move-result-object v0

    if-nez v0, :cond_1

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    iget-object v0, p0, Lgbl;->f:Llan;

    invoke-virtual {v0}, Llan;->b()Llag;

    move-result-object v1

    .line 303
    iget-object v0, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eq p1, v0, :cond_2

    if-ne p1, p0, :cond_3

    .line 304
    :cond_2
    invoke-virtual {p0}, Lgbl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lhkr;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    iget-object v2, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v2}, Lhkr;->a(Landroid/view/View;)V

    .line 305
    iget-object v0, p0, Lgbl;->g:Llci;

    invoke-virtual {v1}, Llag;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgbl;->f:Llan;

    .line 306
    invoke-virtual {v2}, Llan;->c()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lgbl;->h:I

    .line 305
    invoke-interface {v0, v1, v2, v3}, Llci;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 307
    :cond_3
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    if-ne p1, v0, :cond_0

    .line 308
    iget-object v0, p0, Lgbl;->g:Llci;

    iget-object v1, p0, Lgbl;->f:Llan;

    invoke-virtual {v1}, Llan;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgbl;->f:Llan;

    .line 309
    invoke-virtual {v2}, Llan;->b()Llag;

    move-result-object v2

    invoke-virtual {v2}, Llag;->b()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lgbl;->h:I

    .line 310
    invoke-direct {p0, v3}, Lgbl;->a(I)Z

    move-result v3

    iget-object v4, p0, Lgbl;->f:Llan;

    .line 311
    invoke-virtual {v4}, Llan;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lgbl;->i:Ljava/lang/String;

    iget v6, p0, Lgbl;->h:I

    iget-boolean v7, p0, Lgbl;->l:Z

    .line 308
    invoke-interface/range {v0 .. v7}, Llci;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 214
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 216
    invoke-virtual {p0}, Lgbl;->getWidth()I

    move-result v6

    .line 217
    invoke-virtual {p0}, Lgbl;->getHeight()I

    move-result v7

    .line 219
    sget-object v0, Lgbl;->m:Llct;

    iget v0, v0, Llct;->az:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 221
    iget-object v0, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v0

    .line 222
    sget-object v1, Lgbl;->m:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 223
    iget v0, p0, Lgbl;->e:I

    .line 224
    iget-object v2, p0, Lgbl;->b:Landroid/text/StaticLayout;

    if-eqz v2, :cond_0

    .line 225
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 226
    iget-object v2, p0, Lgbl;->b:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 227
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 228
    iget-object v2, p0, Lgbl;->b:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 231
    :cond_0
    iget-object v2, p0, Lgbl;->f:Llan;

    if-eqz v2, :cond_1

    .line 232
    iget-object v2, p0, Lgbl;->f:Llan;

    invoke-virtual {v2}, Llan;->b()Llag;

    move-result-object v2

    .line 233
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Llag;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 234
    sget-object v2, Lgbl;->m:Llct;

    iget-object v2, v2, Llct;->z:Landroid/graphics/Bitmap;

    int-to-float v3, v1

    int-to-float v4, v0

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 235
    sget-object v2, Lgbl;->m:Llct;

    iget-object v2, v2, Llct;->z:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lgbl;->m:Llct;

    iget v3, v3, Llct;->k:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 239
    :cond_1
    iget-object v2, p0, Lgbl;->c:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    .line 240
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 241
    iget-object v2, p0, Lgbl;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 242
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 245
    :cond_2
    iget-boolean v0, p0, Lgbl;->k:Z

    if-eqz v0, :cond_3

    .line 246
    int-to-float v0, v7

    sget-object v1, Lgbl;->m:Llct;

    iget-object v1, v1, Llct;->w:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 247
    const/4 v1, 0x0

    int-to-float v2, v0

    int-to-float v3, v6

    int-to-float v4, v0

    sget-object v0, Lgbl;->m:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 250
    :cond_3
    invoke-virtual {p0}, Lgbl;->isPressed()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lgbl;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 251
    :cond_4
    sget-object v0, Lgbl;->m:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 252
    sget-object v0, Lgbl;->m:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 254
    :cond_5
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 192
    invoke-virtual {p0}, Lgbl;->getMeasuredHeight()I

    move-result v0

    .line 193
    invoke-virtual {p0}, Lgbl;->getMeasuredWidth()I

    move-result v1

    .line 195
    iget-object v2, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v2

    .line 196
    sub-int v3, v0, v2

    div-int/lit8 v3, v3, 0x2

    .line 197
    iget-object v4, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    sget-object v5, Lgbl;->m:Llct;

    iget v5, v5, Llct;->m:I

    sget-object v6, Lgbl;->m:Llct;

    iget v6, v6, Llct;->m:I

    add-int/2addr v6, v2

    add-int v7, v3, v2

    invoke-virtual {v4, v5, v3, v6, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 200
    sub-int v2, v1, v2

    sget-object v3, Lgbl;->m:Llct;

    iget v3, v3, Llct;->m:I

    mul-int/lit8 v3, v3, 0x4

    sub-int/2addr v2, v3

    .line 201
    sget-object v3, Lgbl;->m:Llct;

    iget v3, v3, Llct;->bd:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 202
    iget-object v3, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/high16 v4, -0x80000000

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 203
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 202
    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->measure(II)V

    .line 204
    iget-object v2, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredHeight()I

    move-result v2

    .line 205
    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 206
    iget-object v3, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    iget-object v4, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 207
    invoke-virtual {v4}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v1, v4

    sget-object v5, Lgbl;->m:Llct;

    iget v5, v5, Llct;->m:I

    sub-int/2addr v4, v5

    sget-object v5, Lgbl;->m:Llct;

    iget v5, v5, Llct;->m:I

    sub-int/2addr v1, v5

    add-int/2addr v2, v0

    .line 206
    invoke-virtual {v3, v4, v0, v1, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->layout(IIII)V

    .line 210
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 7

    .prologue
    .line 328
    iget-object v0, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    if-ne p1, v0, :cond_0

    .line 329
    iget-object v0, p0, Lgbl;->g:Llci;

    iget-object v1, p0, Lgbl;->f:Llan;

    invoke-virtual {v1}, Llan;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgbl;->f:Llan;

    .line 330
    invoke-virtual {v2}, Llan;->b()Llag;

    move-result-object v2

    invoke-virtual {v2}, Llag;->b()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lgbl;->h:I

    .line 331
    invoke-direct {p0, v3}, Lgbl;->a(I)Z

    move-result v3

    iget-object v4, p0, Lgbl;->f:Llan;

    .line 332
    invoke-virtual {v4}, Llan;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lgbl;->i:Ljava/lang/String;

    iget v6, p0, Lgbl;->h:I

    .line 329
    invoke-interface/range {v0 .. v6}, Llci;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    .line 334
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 134
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 135
    iget-object v1, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v1

    .line 136
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 137
    iget-object v4, p0, Lgbl;->a:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v4, v2, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 139
    sub-int v1, v3, v1

    sget-object v2, Lgbl;->m:Llct;

    iget v2, v2, Llct;->m:I

    mul-int/lit8 v2, v2, 0x4

    sub-int/2addr v1, v2

    .line 140
    sget-object v2, Lgbl;->m:Llct;

    iget v2, v2, Llct;->bd:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 141
    iget-object v4, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/high16 v5, -0x80000000

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 142
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 141
    invoke-virtual {v4, v2, v5}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->measure(II)V

    .line 144
    iget-object v2, p0, Lgbl;->d:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    .line 145
    invoke-virtual {p0}, Lgbl;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 146
    iget-object v2, p0, Lgbl;->f:Llan;

    invoke-virtual {v2}, Llan;->b()Llag;

    move-result-object v2

    .line 149
    invoke-virtual {v2}, Llag;->b()Ljava/lang/String;

    move-result-object v2

    .line 150
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 151
    const/16 v0, 0xf

    .line 152
    invoke-static {v4, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    .line 151
    invoke-static {v0, v2, v1, v7}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lgbl;->b:Landroid/text/StaticLayout;

    .line 154
    iget-object v0, p0, Lgbl;->b:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 157
    :cond_0
    iget-object v2, p0, Lgbl;->f:Llan;

    invoke-virtual {v2}, Llan;->b()Llag;

    move-result-object v2

    invoke-virtual {v2}, Llag;->d()Z

    move-result v5

    .line 158
    if-eqz v5, :cond_3

    iget-object v2, p0, Lgbl;->j:Ljava/lang/String;

    .line 159
    :goto_0
    if-eqz v5, :cond_1

    .line 160
    sget-object v5, Lgbl;->m:Llct;

    iget-object v5, v5, Llct;->z:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sget-object v6, Lgbl;->m:Llct;

    iget v6, v6, Llct;->k:I

    add-int/2addr v5, v6

    sub-int/2addr v1, v5

    .line 163
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 164
    const/16 v5, 0xa

    .line 165
    invoke-static {v4, v5}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    .line 164
    invoke-static {v5, v2, v1, v7}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Lgbl;->c:Landroid/text/StaticLayout;

    .line 167
    iget-object v1, p0, Lgbl;->c:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_2
    invoke-static {v4}, Lgbl;->a(Landroid/content/Context;)I

    move-result v1

    .line 171
    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgbl;->e:I

    .line 172
    invoke-virtual {p0, v3, v1}, Lgbl;->setMeasuredDimension(II)V

    .line 174
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 175
    return-void

    .line 158
    :cond_3
    iget-object v2, p0, Lgbl;->f:Llan;

    invoke-virtual {v2}, Llan;->d()Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0
.end method
