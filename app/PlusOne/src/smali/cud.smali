.class public final Lcud;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcne;


# instance fields
.field private final a:Lctz;

.field private final b:Z

.field private final c:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lctz;Z)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcud;->c:Landroid/app/Activity;

    .line 61
    iput-object p2, p0, Lcud;->a:Lctz;

    .line 62
    iput-boolean p3, p0, Lcud;->b:Z

    .line 63
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcud;->a:Lctz;

    invoke-virtual {v0}, Lctz;->c()V

    .line 154
    return-void
.end method

.method public a(Lxn;)V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method public a(Lxn;Landroid/view/Menu;)Z
    .locals 8

    .prologue
    const v7, 0x7f0204fa

    const/4 v6, 0x0

    .line 67
    const v0, 0x7f1003a7

    const v1, 0x7f0a0780

    invoke-interface {p2, v6, v0, v6, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 71
    const v1, 0x7f1000b9

    const v2, 0x7f0a0aaf

    invoke-interface {p2, v6, v1, v6, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 75
    const v2, 0x7f1004c6

    const v3, 0x7f0a063a

    invoke-interface {p2, v6, v2, v6, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    .line 80
    const v3, 0x7f1006c4

    const v4, 0x7f0a0638

    invoke-interface {p2, v6, v3, v6, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    .line 86
    const v4, 0x7f100514

    const v5, 0x7f0a0637

    invoke-interface {p2, v6, v4, v6, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v4

    .line 92
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 93
    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 94
    const v2, 0x7f020558

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 95
    const v2, 0x7f020549

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 96
    const v0, 0x7f020531

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lxn;Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 130
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 131
    const v2, 0x7f1003a7

    if-ne v0, v2, :cond_0

    .line 132
    iget-object v0, p0, Lcud;->c:Landroid/app/Activity;

    const-class v2, Lcoa;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoa;

    invoke-virtual {v0}, Lcoa;->a()V

    move v0, v1

    .line 148
    :goto_0
    return v0

    .line 134
    :cond_0
    const v2, 0x7f1000b9

    if-ne v0, v2, :cond_1

    .line 135
    iget-object v0, p0, Lcud;->c:Landroid/app/Activity;

    const-class v2, Lcng;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcng;

    invoke-virtual {v0}, Lcng;->a()V

    move v0, v1

    .line 136
    goto :goto_0

    .line 137
    :cond_1
    const v2, 0x7f1004c6

    if-ne v0, v2, :cond_2

    .line 138
    iget-object v0, p0, Lcud;->c:Landroid/app/Activity;

    const-class v2, Lcnm;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnm;

    invoke-virtual {v0}, Lcnm;->a()V

    move v0, v1

    .line 139
    goto :goto_0

    .line 140
    :cond_2
    const v2, 0x7f1006c4

    if-ne v0, v2, :cond_3

    .line 141
    iget-object v0, p0, Lcud;->c:Landroid/app/Activity;

    const-class v2, Lcnx;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnx;

    invoke-virtual {v0}, Lcnx;->a()V

    move v0, v1

    .line 142
    goto :goto_0

    .line 143
    :cond_3
    const v2, 0x7f100514

    if-ne v0, v2, :cond_4

    .line 144
    iget-object v0, p0, Lcud;->c:Landroid/app/Activity;

    const-class v2, Lcnj;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnj;

    invoke-virtual {v0}, Lcnj;->a()V

    move v0, v1

    .line 145
    goto :goto_0

    .line 148
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lxn;Landroid/view/Menu;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lcud;->a:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v4

    .line 104
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljcn;->k()I

    move-result v0

    move v3, v0

    .line 106
    :goto_0
    if-lez v3, :cond_2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 107
    :goto_1
    invoke-virtual {p1, v0}, Lxn;->b(Ljava/lang/CharSequence;)V

    .line 109
    const v0, 0x7f1003a7

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-lez v3, :cond_3

    .line 110
    invoke-virtual {v4}, Ljcn;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 109
    :goto_2
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 112
    const v0, 0x7f1000b9

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-lez v3, :cond_4

    .line 113
    invoke-virtual {v4}, Ljcn;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Ljcn;->h()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 112
    :goto_3
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 115
    const v0, 0x7f1004c6

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-lez v3, :cond_5

    .line 116
    invoke-virtual {v4}, Ljcn;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 115
    :goto_4
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 118
    const v0, 0x7f1006c4

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-boolean v0, p0, Lcud;->b:Z

    if-eqz v0, :cond_6

    if-lez v3, :cond_6

    .line 119
    invoke-virtual {v4}, Ljcn;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 118
    :goto_5
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 121
    const v0, 0x7f100514

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v5, p0, Lcud;->b:Z

    if-eqz v5, :cond_0

    if-lez v3, :cond_0

    .line 122
    invoke-virtual {v4}, Ljcn;->i()Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    .line 121
    :cond_0
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 125
    return v1

    :cond_1
    move v3, v2

    .line 104
    goto :goto_0

    .line 106
    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    move v0, v2

    .line 110
    goto :goto_2

    :cond_4
    move v0, v2

    .line 113
    goto :goto_3

    :cond_5
    move v0, v2

    .line 116
    goto :goto_4

    :cond_6
    move v0, v2

    .line 119
    goto :goto_5
.end method
