.class abstract Ljwf;
.super Ljwd;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljwd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final b:Ljava/lang/CharSequence;

.field private c:Z

.field private d:I


# direct methods
.method protected constructor <init>(Ljvz;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 451
    invoke-direct {p0}, Ljwd;-><init>()V

    .line 449
    const/4 v0, 0x0

    iput v0, p0, Ljwf;->d:I

    .line 452
    iget-boolean v0, p1, Ljvz;->a:Z

    iput-boolean v0, p0, Ljwf;->c:Z

    .line 453
    iput-object p2, p0, Ljwf;->b:Ljava/lang/CharSequence;

    .line 454
    return-void
.end method


# virtual methods
.method abstract a(I)I
.end method

.method protected synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p0}, Ljwf;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method abstract b(I)I
.end method

.method protected c()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 458
    :cond_0
    iget v0, p0, Ljwf;->d:I

    if-eq v0, v3, :cond_3

    .line 459
    iget v1, p0, Ljwf;->d:I

    .line 462
    iget v0, p0, Ljwf;->d:I

    invoke-virtual {p0, v0}, Ljwf;->a(I)I

    move-result v0

    .line 463
    if-ne v0, v3, :cond_2

    .line 464
    iget-object v0, p0, Ljwf;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 465
    iput v3, p0, Ljwf;->d:I

    .line 471
    :goto_0
    iget-boolean v2, p0, Ljwf;->c:Z

    if-eqz v2, :cond_1

    if-eq v1, v0, :cond_0

    .line 472
    :cond_1
    iget-object v2, p0, Ljwf;->b:Ljava/lang/CharSequence;

    invoke-interface {v2, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 477
    :goto_1
    return-object v0

    .line 468
    :cond_2
    invoke-virtual {p0, v0}, Ljwf;->b(I)I

    move-result v2

    iput v2, p0, Ljwf;->d:I

    goto :goto_0

    .line 477
    :cond_3
    const/4 v0, 0x3

    iput v0, p0, Ljwd;->a:I

    const/4 v0, 0x0

    goto :goto_1
.end method
