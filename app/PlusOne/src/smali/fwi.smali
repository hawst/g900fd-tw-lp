.class public final Lfwi;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/views/BarGraphListView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/BarGraphListView;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->e(Lcom/google/android/apps/plus/views/BarGraphListView;)[Lfwj;

    move-result-object v0

    if-nez v0, :cond_0

    .line 454
    const/4 v0, 0x0

    .line 456
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->e(Lcom/google/android/apps/plus/views/BarGraphListView;)[Lfwj;

    move-result-object v0

    array-length v0, v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->e(Lcom/google/android/apps/plus/views/BarGraphListView;)[Lfwj;

    move-result-object v0

    if-nez v0, :cond_0

    .line 463
    const/4 v0, 0x0

    .line 465
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->e(Lcom/google/android/apps/plus/views/BarGraphListView;)[Lfwj;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 471
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->e(Lcom/google/android/apps/plus/views/BarGraphListView;)[Lfwj;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->e(Lcom/google/android/apps/plus/views/BarGraphListView;)[Lfwj;

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 477
    :cond_0
    const/4 p2, 0x0

    .line 491
    :goto_0
    return-object p2

    .line 481
    :cond_1
    instance-of v0, p2, Lfwg;

    if-eqz v0, :cond_2

    .line 482
    check-cast p2, Lfwg;

    .line 487
    :goto_1
    iget-object v0, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->e(Lcom/google/android/apps/plus/views/BarGraphListView;)[Lfwj;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {p2, v0}, Lfwg;->a(Lfwj;)V

    .line 488
    invoke-virtual {p2}, Lfwg;->requestLayout()V

    .line 489
    invoke-virtual {p2}, Lfwg;->invalidate()V

    goto :goto_0

    .line 484
    :cond_2
    new-instance p2, Lfwg;

    iget-object v0, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    iget-object v1, p0, Lfwi;->a:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/BarGraphListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v0, v1}, Lfwg;-><init>(Lcom/google/android/apps/plus/views/BarGraphListView;Landroid/content/Context;)V

    goto :goto_1
.end method
