.class public final Lkvl;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmia;",
        "Lmib;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 36
    const-string v3, "readsquaremembers"

    new-instance v4, Lmia;

    invoke-direct {v4}, Lmia;-><init>()V

    new-instance v5, Lmib;

    invoke-direct {v5}, Lmib;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 39
    iput-object p3, p0, Lkvl;->a:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lkvl;->b:Ljava/lang/String;

    .line 41
    iput-object p5, p0, Lkvl;->c:Ljava/lang/String;

    .line 42
    const/16 v0, 0x64

    invoke-static {p6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lkvl;->p:I

    .line 43
    return-void
.end method


# virtual methods
.method protected a(Lmia;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 47
    new-instance v0, Lnrz;

    invoke-direct {v0}, Lnrz;-><init>()V

    .line 48
    const/4 v1, 0x3

    iput v1, v0, Lnrz;->b:I

    .line 49
    iget-object v1, p0, Lkvl;->c:Ljava/lang/String;

    iput-object v1, v0, Lnrz;->d:Ljava/lang/String;

    .line 50
    iget-object v1, p0, Lkvl;->b:Ljava/lang/String;

    iput-object v1, v0, Lnrz;->e:Ljava/lang/String;

    .line 51
    iget v1, p0, Lkvl;->p:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lnrz;->c:Ljava/lang/Integer;

    .line 52
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnrz;->f:Ljava/lang/Boolean;

    .line 54
    new-instance v1, Lnth;

    invoke-direct {v1}, Lnth;-><init>()V

    iput-object v1, p1, Lmia;->a:Lnth;

    .line 55
    iget-object v1, p1, Lmia;->a:Lnth;

    iget-object v2, p0, Lkvl;->a:Ljava/lang/String;

    iput-object v2, v1, Lnth;->a:Ljava/lang/String;

    .line 56
    iget-object v1, p1, Lmia;->a:Lnth;

    new-array v2, v3, [Lnrz;

    iput-object v2, v1, Lnth;->b:[Lnrz;

    .line 57
    iget-object v1, p1, Lmia;->a:Lnth;

    iget-object v1, v1, Lnth;->b:[Lnrz;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 58
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmia;

    invoke-virtual {p0, p1}, Lkvl;->a(Lmia;)V

    return-void
.end method

.method public i()Lnry;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lkvl;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmib;

    .line 62
    if-eqz v0, :cond_0

    iget-object v1, v0, Lmib;->a:Lntt;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lmib;->a:Lntt;

    iget-object v1, v1, Lntt;->a:[Lnry;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 64
    iget-object v0, v0, Lmib;->a:Lntt;

    iget-object v0, v0, Lntt;->a:[Lnry;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
