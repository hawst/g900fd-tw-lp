.class public final Lbp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/CharSequence;

.field private final c:Landroid/app/PendingIntent;

.field private final d:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 1852
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lbp;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V

    .line 1853
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1865
    iput p1, p0, Lbp;->a:I

    .line 1866
    invoke-static {p2}, Lbs;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lbp;->b:Ljava/lang/CharSequence;

    .line 1867
    iput-object p3, p0, Lbp;->c:Landroid/app/PendingIntent;

    .line 1868
    iput-object p4, p0, Lbp;->d:Landroid/os/Bundle;

    .line 1869
    return-void
.end method


# virtual methods
.method public a()Lbn;
    .locals 6

    .prologue
    .line 1924
    new-instance v0, Lbn;

    iget v1, p0, Lbp;->a:I

    iget-object v2, p0, Lbp;->b:Ljava/lang/CharSequence;

    iget-object v3, p0, Lbp;->c:Landroid/app/PendingIntent;

    iget-object v4, p0, Lbp;->d:Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lbn;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lcq;)V

    return-object v0
.end method
