.class public final Lfeo;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lhfv;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Lhfv;",
            ">.dp;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Z

.field private h:Lhfv;

.field private i:[Landroid/database/Cursor;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v2

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "avatar"

    aput-object v1, v0, v4

    const-string v1, "in_same_visibility_group"

    aput-object v1, v0, v5

    sput-object v0, Lfeo;->b:[Ljava/lang/String;

    .line 37
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "circle_id"

    aput-object v1, v0, v2

    const-string v1, "type"

    aput-object v1, v0, v3

    const-string v1, "circle_name"

    aput-object v1, v0, v4

    const-string v1, "contact_count"

    aput-object v1, v0, v5

    sput-object v0, Lfeo;->c:[Ljava/lang/String;

    .line 43
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v2

    sput-object v0, Lfeo;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZI)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lfeo;->e:Ldp;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lfeo;->i:[Landroid/database/Cursor;

    .line 58
    iput p2, p0, Lfeo;->f:I

    .line 59
    iput-boolean p3, p0, Lfeo;->g:Z

    .line 60
    iput p4, p0, Lfeo;->j:I

    .line 61
    return-void
.end method


# virtual methods
.method protected D_()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 65
    invoke-super {p0}, Lhxz;->D_()Z

    .line 66
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    .line 68
    invoke-static {v1, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lfeo;->e:Ldp;

    .line 67
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 69
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    .line 70
    invoke-static {v1, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lfeo;->e:Ldp;

    .line 69
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 71
    sget-object v1, Lktp;->a:Landroid/net/Uri;

    .line 72
    invoke-static {v1, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lfeo;->e:Ldp;

    .line 71
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method protected i()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 144
    invoke-super {p0}, Lhxz;->i()V

    .line 146
    iput-object v2, p0, Lfeo;->h:Lhfv;

    .line 148
    iget-object v0, p0, Lfeo;->i:[Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 149
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lfeo;->i:[Landroid/database/Cursor;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 150
    iget-object v1, p0, Lfeo;->i:[Landroid/database/Cursor;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, p0, Lfeo;->i:[Landroid/database/Cursor;

    aget-object v1, v1, v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 149
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_1
    iput-object v2, p0, Lfeo;->i:[Landroid/database/Cursor;

    .line 156
    :cond_2
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lfeo;->l()Lhfv;

    move-result-object v0

    return-object v0
.end method

.method protected k()Z
    .locals 2

    .prologue
    .line 79
    invoke-super {p0}, Lhxz;->k()Z

    .line 80
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfeo;->e:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public l()Lhfv;
    .locals 15

    .prologue
    .line 87
    iget v0, p0, Lfeo;->j:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 88
    iget-boolean v0, p0, Lfeo;->g:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    .line 93
    :goto_0
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lfeo;->f:I

    sget-object v3, Lfeo;->c:[Ljava/lang/String;

    invoke-static {v1, v2, v0, v3}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 97
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lktq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iget v1, p0, Lfeo;->f:I

    sget-object v2, Lfek;->a:[Ljava/lang/String;

    const-string v3, "square_name COLLATE NOCASE"

    iget-boolean v4, p0, Lfeo;->g:Z

    .line 98
    invoke-interface {v0, v1, v2, v3, v4}, Lktq;->a(I[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v11

    .line 102
    const-string v6, "interaction_sort_key DESC"

    .line 103
    const-string v4, "in_my_circles=1 AND interaction_sort_key>0"

    .line 105
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfeo;->f:I

    const/4 v2, 0x0

    sget-object v3, Lfeo;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 110
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfeo;->f:I

    invoke-static {v0, v1}, Ldsm;->e(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v13

    .line 113
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfeo;->f:I

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Ldsm;->a(Landroid/content/Context;II)Landroid/database/Cursor;

    move-result-object v14

    .line 117
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfeo;->f:I

    invoke-static {v0, v1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 119
    const-string v1, "account_status"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "audience_history"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 124
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfeo;->f:I

    sget-object v2, Lfeo;->d:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Ldsm;->a(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)Landroid/database/Cursor;

    move-result-object v6

    .line 127
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 128
    iget v1, p0, Lfeo;->f:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 130
    new-instance v0, Lfek;

    const-string v2, "domain_name"

    .line 134
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v2, "is_child"

    .line 135
    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v9

    .line 136
    invoke-virtual {p0}, Lfeo;->n()Landroid/content/Context;

    move-object v1, v10

    move-object v2, v11

    move-object v3, v12

    move-object v4, v14

    move-object v7, v13

    invoke-direct/range {v0 .. v9}, Lfek;-><init>(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;Ljava/util/Map;Ljava/lang/String;Z)V

    iput-object v0, p0, Lfeo;->h:Lhfv;

    .line 137
    const/4 v0, 0x6

    new-array v0, v0, [Landroid/database/Cursor;

    const/4 v1, 0x0

    aput-object v10, v0, v1

    const/4 v1, 0x1

    aput-object v11, v0, v1

    const/4 v1, 0x2

    aput-object v12, v0, v1

    const/4 v1, 0x3

    aput-object v14, v0, v1

    const/4 v1, 0x4

    aput-object v5, v0, v1

    const/4 v1, 0x5

    aput-object v6, v0, v1

    iput-object v0, p0, Lfeo;->i:[Landroid/database/Cursor;

    .line 139
    iget-object v0, p0, Lfeo;->h:Lhfv;

    return-object v0

    .line 88
    :cond_0
    iget v0, p0, Lfeo;->j:I

    goto/16 :goto_0
.end method
