.class public Levy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lizu;",
            "Lcom/google/android/apps/plus/views/PhotoTileView;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/os/Handler;

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Levy;->b:Ljava/util/HashMap;

    .line 44
    iput-object p1, p0, Levy;->a:Landroid/content/Context;

    .line 45
    return-void
.end method

.method static synthetic a(Levy;Landroid/content/Context;Lizu;)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 35
    invoke-virtual {p2}, Lizu;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lizu;->g()Ljac;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Levy;->a:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "file"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "media"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v0

    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    const-string v5, "media"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    iget-object v4, p0, Levy;->a:Landroid/content/Context;

    invoke-static {v4}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljem;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x8

    :goto_2
    if-eqz v0, :cond_4

    const/4 v0, 0x4

    :goto_3
    or-int/2addr v0, v2

    if-eqz v3, :cond_0

    const/4 v1, 0x2

    :cond_0
    or-int/2addr v0, v1

    return v0

    :cond_1
    move v3, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method static synthetic a(Levy;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Levy;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic b(Levy;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Levy;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Levy;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Levy;->c:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Levy;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Levy;->d:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Levy;->d:Landroid/os/Handler;

    .line 93
    iget-object v0, p0, Levy;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/plus/views/PhotoTileView;Lizu;)V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Levy;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DEBUG-STATUS-FETCHER"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Levz;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Levz;-><init>(Levy;Landroid/os/Looper;)V

    iput-object v1, p0, Levy;->d:Landroid/os/Handler;

    new-instance v0, Lewa;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lewa;-><init>(Levy;Landroid/os/Looper;)V

    iput-object v0, p0, Levy;->c:Landroid/os/Handler;

    .line 62
    :cond_0
    iget-object v0, p0, Levy;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 63
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 65
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 66
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 71
    :cond_2
    iget-object v0, p0, Levy;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 74
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 75
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 79
    iget-object v1, p0, Levy;->d:Landroid/os/Handler;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 80
    iget-object v1, p0, Levy;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 81
    return-void
.end method
