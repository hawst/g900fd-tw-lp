.class public final Lcza;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lkfd;

.field private final b:I

.field private final c:I

.field private final d:J

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/Long;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 29
    const-string v0, "ReportAbuseTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 30
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lcza;->a:Lkfd;

    .line 32
    iput p2, p0, Lcza;->b:I

    .line 33
    iput p3, p0, Lcza;->c:I

    .line 34
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcza;->d:J

    .line 35
    iput-object p5, p0, Lcza;->e:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 7

    .prologue
    .line 40
    new-instance v0, Ldmt;

    .line 41
    invoke-virtual {p0}, Lcza;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcza;->b:I

    iget-object v3, p0, Lcza;->e:Ljava/lang/String;

    iget-wide v4, p0, Lcza;->d:J

    iget v6, p0, Lcza;->c:I

    invoke-direct/range {v0 .. v6}, Ldmt;-><init>(Landroid/content/Context;ILjava/lang/String;JI)V

    .line 42
    iget-object v1, p0, Lcza;->a:Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 44
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 45
    invoke-virtual {v0}, Ldmt;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcza;->f()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a055e

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0}, Lcza;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a055c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
