.class public final Lfdy;
.super Lfdj;
.source "PG"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;


# instance fields
.field private c:[Lnmv;

.field private d:Landroid/database/Cursor;

.field private e:Landroid/database/Cursor;

.field private f:Landroid/database/Cursor;

.field private l:Lgby;

.field private m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "avatar"

    aput-object v1, v0, v3

    sput-object v0, Lfdy;->a:[Ljava/lang/String;

    .line 52
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "photo_url"

    aput-object v1, v0, v3

    sput-object v0, Lfdy;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Llci;)V
    .locals 9

    .prologue
    .line 73
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lfdj;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V

    .line 76
    invoke-static {}, Lfdy;->ar()Lhym;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdy;->a(Landroid/database/Cursor;)V

    .line 77
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;
    .locals 6

    .prologue
    .line 85
    const v0, 0x7f04022a

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 87
    new-instance v1, Lldx;

    const/4 v2, -0x2

    invoke-direct {v1, v2}, Lldx;-><init>(I)V

    .line 88
    iget-object v2, p0, Lfdy;->j:Llcr;

    iget v2, v2, Llcr;->a:I

    iput v2, v1, Lldx;->a:I

    .line 89
    iget-object v2, p0, Lfdy;->j:Llcr;

    iget v2, v2, Llcr;->f:I

    neg-int v2, v2

    iget-object v3, p0, Lfdy;->j:Llcr;

    iget v3, v3, Llcr;->d:I

    neg-int v3, v3

    iget-object v4, p0, Lfdy;->j:Llcr;

    iget v4, v4, Llcr;->f:I

    neg-int v4, v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lldx;->setMargins(IIII)V

    .line 91
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 93
    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const/16 v11, 0x2a

    const/4 v10, 0x3

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 98
    check-cast p1, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    iput-object p1, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    .line 99
    iget-object v4, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    iget v0, p0, Lfdy;->n:I

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a(I)V

    .line 102
    iget-object v0, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    iget-object v4, p0, Lfdy;->l:Lgby;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a(Lgby;)V

    .line 105
    iget-object v0, p0, Lfdy;->c:[Lnmv;

    if-eqz v0, :cond_2

    .line 106
    iget-object v5, p0, Lfdy;->c:[Lnmv;

    array-length v6, v5

    move v4, v1

    move-object v0, v3

    :goto_1
    if-ge v4, v6, :cond_3

    aget-object v7, v5, v4

    .line 108
    iget v8, v7, Lnmv;->b:I

    if-ne v8, v2, :cond_0

    .line 111
    iget-object v0, v7, Lnmv;->c:Lnni;

    iget-object v0, v0, Lnni;->a:Lpeo;

    new-instance v7, Lfdz;

    invoke-direct {v7}, Lfdz;-><init>()V

    .line 141
    invoke-static {}, Lhwr;->d()Lhxa;

    move-result-object v8

    .line 142
    invoke-static {}, Lhwr;->c()Lhxa;

    move-result-object v9

    .line 111
    invoke-static {v0, v7, v8, v9}, Lhwr;->a(Lpeo;Lhxa;Lhxa;Lhxa;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 106
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 99
    goto :goto_0

    :cond_2
    move-object v0, v3

    .line 146
    :cond_3
    iget-object v4, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a(Landroid/text/SpannableStringBuilder;)V

    .line 148
    iget-object v0, p0, Lfdy;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfdy;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 149
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 151
    iget-object v0, p0, Lfdy;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    move v0, v2

    .line 152
    :goto_2
    if-gt v0, v5, :cond_4

    .line 153
    iget-object v6, p0, Lfdy;->d:Landroid/database/Cursor;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 154
    iget-object v6, p0, Lfdy;->d:Landroid/database/Cursor;

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 156
    :cond_4
    iget-object v0, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a(Ljava/util/ArrayList;)V

    .line 157
    const-string v0, "UnifiedSearchAdapter"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 158
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "bindStreamHeaderView(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " people"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_5
    :goto_3
    iget-object v0, p0, Lfdy;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lfdy;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_9

    .line 169
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 171
    iget-object v0, p0, Lfdy;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    move v0, v1

    .line 172
    :goto_4
    if-ge v0, v5, :cond_7

    .line 173
    iget-object v6, p0, Lfdy;->e:Landroid/database/Cursor;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 174
    iget-object v6, p0, Lfdy;->e:Landroid/database/Cursor;

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 162
    :cond_6
    iget-object v0, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->a(Ljava/util/ArrayList;)V

    goto :goto_3

    .line 176
    :cond_7
    iget-object v0, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b(Ljava/util/ArrayList;)V

    .line 177
    const-string v0, "UnifiedSearchAdapter"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 178
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "bindStreamHeaderView(): "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " communities"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    :cond_8
    :goto_5
    iget-object v0, p0, Lfdy;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lfdy;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_c

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 191
    iget-object v2, p0, Lfdy;->f:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 192
    :goto_6
    if-ge v1, v2, :cond_a

    .line 193
    iget-object v3, p0, Lfdy;->f:Landroid/database/Cursor;

    invoke-interface {v3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 195
    invoke-virtual {p0}, Lfdy;->as()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lfdy;->f:Landroid/database/Cursor;

    invoke-static {v3, v4}, Lfbs;->a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;

    move-result-object v3

    .line 196
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 182
    :cond_9
    iget-object v0, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->b(Ljava/util/ArrayList;)V

    goto :goto_5

    .line 198
    :cond_a
    iget-object v1, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c(Ljava/util/ArrayList;)V

    .line 199
    const-string v1, "UnifiedSearchAdapter"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 200
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "bindStreamHeaderView(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " photos"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_b
    :goto_7
    return-void

    .line 204
    :cond_c
    iget-object v0, p0, Lfdy;->m:Lcom/google/android/apps/plus/views/UnifiedSearchHeader;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/UnifiedSearchHeader;->c(Ljava/util/ArrayList;)V

    goto :goto_7
.end method

.method public a(Lgby;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lfdy;->l:Lgby;

    .line 241
    return-void
.end method

.method public a([Lnmv;)V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lfdy;->c:[Lnmv;

    if-eq v0, p1, :cond_0

    .line 213
    iput-object p1, p0, Lfdy;->c:[Lnmv;

    .line 214
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lfdy;->a(ZI)V

    .line 216
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Lfdy;->n:I

    .line 81
    return-void
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lfdy;->d:Landroid/database/Cursor;

    if-eq v0, p1, :cond_0

    .line 220
    iput-object p1, p0, Lfdy;->d:Landroid/database/Cursor;

    .line 221
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lfdy;->a(ZI)V

    .line 223
    :cond_0
    return-void
.end method

.method public c(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lfdy;->e:Landroid/database/Cursor;

    if-eq v0, p1, :cond_0

    .line 227
    iput-object p1, p0, Lfdy;->e:Landroid/database/Cursor;

    .line 228
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lfdy;->a(ZI)V

    .line 230
    :cond_0
    return-void
.end method

.method public d(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lfdy;->f:Landroid/database/Cursor;

    if-eq v0, p1, :cond_0

    .line 234
    iput-object p1, p0, Lfdy;->f:Landroid/database/Cursor;

    .line 235
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lfdy;->a(ZI)V

    .line 237
    :cond_0
    return-void
.end method
