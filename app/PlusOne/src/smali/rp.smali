.class public final Lrp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lep;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/content/Intent;

.field private h:C

.field private i:C

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:I

.field private l:Lrl;

.field private m:Lsg;

.field private n:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private o:I

.field private p:I

.field private q:Landroid/view/View;

.field private r:Lhj;

.field private s:Lij;

.field private t:Z


# direct methods
.method constructor <init>(Lrl;IIIILjava/lang/CharSequence;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput v1, p0, Lrp;->k:I

    .line 77
    const/16 v0, 0x10

    iput v0, p0, Lrp;->o:I

    .line 85
    iput v1, p0, Lrp;->p:I

    .line 90
    iput-boolean v1, p0, Lrp;->t:Z

    .line 134
    iput-object p1, p0, Lrp;->l:Lrl;

    .line 135
    iput p3, p0, Lrp;->a:I

    .line 136
    iput p2, p0, Lrp;->b:I

    .line 137
    iput p4, p0, Lrp;->c:I

    .line 138
    iput p5, p0, Lrp;->d:I

    .line 139
    iput-object p6, p0, Lrp;->e:Ljava/lang/CharSequence;

    .line 140
    iput p7, p0, Lrp;->p:I

    .line 141
    return-void
.end method

.method static synthetic a(Lrp;)Lrl;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lrp;->l:Lrl;

    return-object v0
.end method


# virtual methods
.method public a(I)Lep;
    .locals 3

    .prologue
    .line 619
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->e()Landroid/content/Context;

    move-result-object v0

    .line 620
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 621
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrp;->a(Landroid/view/View;)Lep;

    .line 622
    return-object p0
.end method

.method public a(Landroid/view/View;)Lep;
    .locals 2

    .prologue
    .line 608
    iput-object p1, p0, Lrp;->q:Landroid/view/View;

    .line 609
    const/4 v0, 0x0

    iput-object v0, p0, Lrp;->r:Lhj;

    .line 610
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lrp;->a:I

    if-lez v0, :cond_0

    .line 611
    iget v0, p0, Lrp;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 613
    :cond_0
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->j()V

    .line 614
    return-object p0
.end method

.method public a(Lhj;)Lep;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 656
    iget-object v0, p0, Lrp;->r:Lhj;

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lrp;->r:Lhj;

    invoke-virtual {v0, v1}, Lhj;->a(Lhl;)V

    .line 659
    :cond_0
    iput-object v1, p0, Lrp;->q:Landroid/view/View;

    .line 660
    iput-object p1, p0, Lrp;->r:Lhj;

    .line 661
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 662
    iget-object v0, p0, Lrp;->r:Lhj;

    if-eqz v0, :cond_1

    .line 663
    iget-object v0, p0, Lrp;->r:Lhj;

    new-instance v1, Lrq;

    invoke-direct {v1, p0}, Lrq;-><init>(Lrp;)V

    invoke-virtual {v0, v1}, Lhj;->a(Lhl;)V

    .line 670
    :cond_1
    return-object p0
.end method

.method public a(Lij;)Lep;
    .locals 0

    .prologue
    .line 714
    iput-object p1, p0, Lrp;->s:Lij;

    .line 715
    return-object p0
.end method

.method public a()Lhj;
    .locals 1

    .prologue
    .line 651
    iget-object v0, p0, Lrp;->r:Lhj;

    return-object v0
.end method

.method public a(Lsd;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 363
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lsd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lrp;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lrp;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method a(Lsg;)V
    .locals 1

    .prologue
    .line 345
    iput-object p1, p0, Lrp;->m:Lsg;

    .line 347
    invoke-virtual {p0}, Lrp;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lsg;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 348
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 468
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v1, v0, -0x5

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    or-int/2addr v0, v1

    iput v0, p0, Lrp;->o:I

    .line 469
    return-void

    .line 468
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Lep;
    .locals 0

    .prologue
    .line 675
    invoke-virtual {p0, p1}, Lrp;->setShowAsAction(I)V

    .line 676
    return-object p0
.end method

.method b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 494
    iget v2, p0, Lrp;->o:I

    .line 495
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v3, v0, -0x3

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Lrp;->o:I

    .line 496
    iget v0, p0, Lrp;->o:I

    if-eq v2, v0, :cond_0

    .line 497
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 499
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 495
    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 149
    iget-object v1, p0, Lrp;->n:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lrp;->n:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v1, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v0

    .line 153
    :cond_1
    iget-object v1, p0, Lrp;->l:Lrl;

    iget-object v2, p0, Lrp;->l:Lrl;

    invoke-virtual {v2}, Lrl;->r()Lrl;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Lrl;->a(Lrl;Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    iget-object v1, p0, Lrp;->g:Landroid/content/Intent;

    if-eqz v1, :cond_2

    .line 164
    :try_start_0
    iget-object v1, p0, Lrp;->l:Lrl;

    invoke-virtual {v1}, Lrl;->e()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lrp;->g:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 166
    :catch_0
    move-exception v1

    .line 167
    const-string v2, "MenuItemImpl"

    const-string v3, "Can\'t find activity to handle intent; ignoring"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 171
    :cond_2
    iget-object v1, p0, Lrp;->r:Lhj;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lrp;->r:Lhj;

    invoke-virtual {v1}, Lhj;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 175
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lrp;->d:I

    return v0
.end method

.method c(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 518
    iget v2, p0, Lrp;->o:I

    .line 519
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v3, v0, -0x9

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Lrp;->o:I

    .line 520
    iget v0, p0, Lrp;->o:I

    if-eq v2, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 519
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public collapseActionView()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 695
    iget v1, p0, Lrp;->p:I

    and-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_1

    .line 708
    :cond_0
    :goto_0
    return v0

    .line 698
    :cond_1
    iget-object v1, p0, Lrp;->q:Landroid/view/View;

    if-nez v1, :cond_2

    .line 700
    const/4 v0, 0x1

    goto :goto_0

    .line 703
    :cond_2
    iget-object v1, p0, Lrp;->s:Lij;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lrp;->s:Lij;

    invoke-interface {v1, p0}, Lij;->b(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 705
    :cond_3
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0, p0}, Lrl;->b(Lrp;)Z

    move-result v0

    goto :goto_0
.end method

.method public d()C
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-char v0, p0, Lrp;->i:C

    :goto_0
    return v0

    :cond_0
    iget-char v0, p0, Lrp;->h:C

    goto :goto_0
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 577
    if-eqz p1, :cond_0

    .line 578
    iget v0, p0, Lrp;->o:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lrp;->o:I

    .line 582
    :goto_0
    return-void

    .line 580
    :cond_0
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lrp;->o:I

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 296
    invoke-virtual {p0}, Lrp;->d()C

    move-result v0

    .line 297
    if-nez v0, :cond_0

    .line 298
    const-string v0, ""

    .line 321
    :goto_0
    return-object v0

    .line 301
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 302
    sparse-switch v0, :sswitch_data_0

    .line 317
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 321
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 305
    :sswitch_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 309
    :sswitch_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 313
    :sswitch_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 302
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0xa -> :sswitch_0
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 729
    iput-boolean p1, p0, Lrp;->t:Z

    .line 730
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 731
    return-void
.end method

.method public expandActionView()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 681
    invoke-virtual {p0}, Lrp;->m()Z

    move-result v1

    if-nez v1, :cond_1

    .line 690
    :cond_0
    :goto_0
    return v0

    .line 685
    :cond_1
    iget-object v1, p0, Lrp;->s:Lij;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lrp;->s:Lij;

    invoke-interface {v1, p0}, Lij;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 687
    :cond_2
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0, p0}, Lrl;->a(Lrp;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lrp;->d()C

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    .prologue
    .line 645
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.getActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lrp;->q:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lrp;->q:Landroid/view/View;

    .line 633
    :goto_0
    return-object v0

    .line 629
    :cond_0
    iget-object v0, p0, Lrp;->r:Lhj;

    if-eqz v0, :cond_1

    .line 630
    iget-object v0, p0, Lrp;->r:Lhj;

    invoke-virtual {v0, p0}, Lhj;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lrp;->q:Landroid/view/View;

    .line 631
    iget-object v0, p0, Lrp;->q:Landroid/view/View;

    goto :goto_0

    .line 633
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 238
    iget-char v0, p0, Lrp;->i:C

    return v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lrp;->b:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lrp;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lrp;->j:Landroid/graphics/drawable/Drawable;

    .line 428
    :goto_0
    return-object v0

    .line 421
    :cond_0
    iget v0, p0, Lrp;->k:I

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->e()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lrp;->k:I

    invoke-static {v0, v1}, Lur;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 423
    const/4 v1, 0x0

    iput v1, p0, Lrp;->k:I

    .line 424
    iput-object v0, p0, Lrp;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 428
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lrp;->g:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 204
    iget v0, p0, Lrp;->a:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 550
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    .prologue
    .line 256
    iget-char v0, p0, Lrp;->h:C

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lrp;->c:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lrp;->m:Lsg;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 353
    iget-object v0, p0, Lrp;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 388
    iget-object v0, p0, Lrp;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lrp;->f:Ljava/lang/CharSequence;

    .line 390
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_0

    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 394
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 397
    :cond_0
    return-object v0

    .line 388
    :cond_1
    iget-object v0, p0, Lrp;->e:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->s()Z

    move-result v0

    return v0
.end method

.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lrp;->m:Lsg;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 565
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 735
    iget-boolean v0, p0, Lrp;->t:Z

    return v0
.end method

.method public isCheckable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 453
    iget v1, p0, Lrp;->o:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChecked()Z
    .locals 2

    .prologue
    .line 477
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 503
    iget-object v2, p0, Lrp;->r:Lhj;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lrp;->r:Lhj;

    invoke-virtual {v2}, Lhj;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 504
    iget v2, p0, Lrp;->o:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_1

    iget-object v2, p0, Lrp;->r:Lhj;

    invoke-virtual {v2}, Lhj;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 506
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 504
    goto :goto_0

    .line 506
    :cond_2
    iget v2, p0, Lrp;->o:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public j()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 569
    iget v1, p0, Lrp;->p:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 573
    iget v0, p0, Lrp;->p:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 585
    iget v0, p0, Lrp;->p:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 719
    iget v1, p0, Lrp;->p:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    .line 720
    iget-object v1, p0, Lrp;->q:Landroid/view/View;

    if-nez v1, :cond_0

    iget-object v1, p0, Lrp;->r:Lhj;

    if-eqz v1, :cond_0

    .line 721
    iget-object v1, p0, Lrp;->r:Lhj;

    invoke-virtual {v1, p0}, Lhj;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lrp;->q:Landroid/view/View;

    .line 723
    :cond_0
    iget-object v1, p0, Lrp;->q:Landroid/view/View;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 725
    :cond_1
    return v0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 639
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic setActionView(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lrp;->a(I)Lep;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lrp;->a(Landroid/view/View;)Lep;

    move-result-object v0

    return-object v0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 243
    iget-char v0, p0, Lrp;->i:C

    if-ne v0, p1, :cond_0

    .line 251
    :goto_0
    return-object p0

    .line 247
    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Lrp;->i:C

    .line 249
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    goto :goto_0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 458
    iget v2, p0, Lrp;->o:I

    .line 459
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v3, v0, -0x2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Lrp;->o:I

    .line 460
    iget v0, p0, Lrp;->o:I

    if-eq v2, v0, :cond_0

    .line 461
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 464
    :cond_0
    return-object p0

    :cond_1
    move v0, v1

    .line 459
    goto :goto_0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 482
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0, p0}, Lrl;->a(Landroid/view/MenuItem;)V

    .line 490
    :goto_0
    return-object p0

    .line 487
    :cond_0
    invoke-virtual {p0, p1}, Lrp;->b(Z)V

    goto :goto_0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 185
    if-eqz p1, :cond_0

    .line 186
    iget v0, p0, Lrp;->o:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lrp;->o:I

    .line 191
    :goto_0
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 193
    return-object p0

    .line 188
    :cond_0
    iget v0, p0, Lrp;->o:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lrp;->o:I

    goto :goto_0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 442
    const/4 v0, 0x0

    iput-object v0, p0, Lrp;->j:Landroid/graphics/drawable/Drawable;

    .line 443
    iput p1, p0, Lrp;->k:I

    .line 446
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 448
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 433
    iput v1, p0, Lrp;->k:I

    .line 434
    iput-object p1, p0, Lrp;->j:Landroid/graphics/drawable/Drawable;

    .line 435
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 437
    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lrp;->g:Landroid/content/Intent;

    .line 224
    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 261
    iget-char v0, p0, Lrp;->h:C

    if-ne v0, p1, :cond_0

    .line 269
    :goto_0
    return-object p0

    .line 265
    :cond_0
    iput-char p1, p0, Lrp;->h:C

    .line 267
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    goto :goto_0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 740
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setOnActionExpandListener()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lrp;->n:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 536
    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 274
    iput-char p1, p0, Lrp;->h:C

    .line 275
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Lrp;->i:C

    .line 277
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 279
    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 2

    .prologue
    .line 590
    and-int/lit8 v0, p1, 0x3

    packed-switch v0, :pswitch_data_0

    .line 599
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 602
    :pswitch_0
    iput p1, p0, Lrp;->p:I

    .line 603
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->j()V

    .line 604
    return-void

    .line 590
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lrp;->b(I)Lep;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrp;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 370
    iput-object p1, p0, Lrp;->e:Ljava/lang/CharSequence;

    .line 372
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 374
    iget-object v0, p0, Lrp;->m:Lsg;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lrp;->m:Lsg;

    invoke-virtual {v0, p1}, Lsg;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 378
    :cond_0
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 403
    iput-object p1, p0, Lrp;->f:Ljava/lang/CharSequence;

    .line 406
    if-nez p1, :cond_0

    .line 407
    iget-object v0, p0, Lrp;->e:Ljava/lang/CharSequence;

    .line 410
    :cond_0
    iget-object v0, p0, Lrp;->l:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 412
    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 528
    invoke-virtual {p0, p1}, Lrp;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrp;->l:Lrl;

    invoke-virtual {v0}, Lrl;->i()V

    .line 530
    :cond_0
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lrp;->e:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
