.class public final Lnji;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnja;

.field private b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 425
    invoke-direct {p0}, Loxq;-><init>()V

    .line 428
    const/4 v0, 0x0

    iput-object v0, p0, Lnji;->a:Lnja;

    .line 425
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 445
    const/4 v0, 0x0

    .line 446
    iget-object v1, p0, Lnji;->a:Lnja;

    if-eqz v1, :cond_0

    .line 447
    const/4 v0, 0x1

    iget-object v1, p0, Lnji;->a:Lnja;

    .line 448
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 450
    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Lnji;->b:Ljava/lang/Float;

    .line 451
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 452
    iget-object v1, p0, Lnji;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    iput v0, p0, Lnji;->ai:I

    .line 454
    return v0
.end method

.method public a(Loxn;)Lnji;
    .locals 2

    .prologue
    .line 462
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 463
    sparse-switch v0, :sswitch_data_0

    .line 467
    iget-object v1, p0, Lnji;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 468
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnji;->ah:Ljava/util/List;

    .line 471
    :cond_1
    iget-object v1, p0, Lnji;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 473
    :sswitch_0
    return-object p0

    .line 478
    :sswitch_1
    iget-object v0, p0, Lnji;->a:Lnja;

    if-nez v0, :cond_2

    .line 479
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnji;->a:Lnja;

    .line 481
    :cond_2
    iget-object v0, p0, Lnji;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 485
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lnji;->b:Ljava/lang/Float;

    goto :goto_0

    .line 463
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lnji;->a:Lnja;

    if-eqz v0, :cond_0

    .line 436
    const/4 v0, 0x1

    iget-object v1, p0, Lnji;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 438
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lnji;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 439
    iget-object v0, p0, Lnji;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 441
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 421
    invoke-virtual {p0, p1}, Lnji;->a(Loxn;)Lnji;

    move-result-object v0

    return-object v0
.end method
