.class public final Lpur;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lolm;

.field private b:I

.field private c:Lomr;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30
    iput-object v1, p0, Lpur;->a:Lolm;

    .line 33
    const/high16 v0, -0x80000000

    iput v0, p0, Lpur;->b:I

    .line 36
    iput-object v1, p0, Lpur;->c:Lomr;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 61
    const/4 v0, 0x0

    .line 62
    iget-object v1, p0, Lpur;->a:Lolm;

    if-eqz v1, :cond_0

    .line 63
    const/4 v0, 0x1

    iget-object v1, p0, Lpur;->a:Lolm;

    .line 64
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 66
    :cond_0
    iget v1, p0, Lpur;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 67
    const/4 v1, 0x2

    iget v2, p0, Lpur;->b:I

    .line 68
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_1
    iget-object v1, p0, Lpur;->c:Lomr;

    if-eqz v1, :cond_2

    .line 71
    const/4 v1, 0x3

    iget-object v2, p0, Lpur;->c:Lomr;

    .line 72
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_2
    iget-object v1, p0, Lpur;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 75
    const/4 v1, 0x4

    iget-object v2, p0, Lpur;->d:Ljava/lang/String;

    .line 76
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_3
    iget-object v1, p0, Lpur;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    iput v0, p0, Lpur;->ai:I

    .line 80
    return v0
.end method

.method public a(Loxn;)Lpur;
    .locals 2

    .prologue
    .line 88
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 89
    sparse-switch v0, :sswitch_data_0

    .line 93
    iget-object v1, p0, Lpur;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpur;->ah:Ljava/util/List;

    .line 97
    :cond_1
    iget-object v1, p0, Lpur;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    :sswitch_0
    return-object p0

    .line 104
    :sswitch_1
    iget-object v0, p0, Lpur;->a:Lolm;

    if-nez v0, :cond_2

    .line 105
    new-instance v0, Lolm;

    invoke-direct {v0}, Lolm;-><init>()V

    iput-object v0, p0, Lpur;->a:Lolm;

    .line 107
    :cond_2
    iget-object v0, p0, Lpur;->a:Lolm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 111
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 112
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-ne v0, v1, :cond_4

    .line 122
    :cond_3
    iput v0, p0, Lpur;->b:I

    goto :goto_0

    .line 124
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lpur;->b:I

    goto :goto_0

    .line 129
    :sswitch_3
    iget-object v0, p0, Lpur;->c:Lomr;

    if-nez v0, :cond_5

    .line 130
    new-instance v0, Lomr;

    invoke-direct {v0}, Lomr;-><init>()V

    iput-object v0, p0, Lpur;->c:Lomr;

    .line 132
    :cond_5
    iget-object v0, p0, Lpur;->c:Lomr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 136
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpur;->d:Ljava/lang/String;

    goto :goto_0

    .line 89
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lpur;->a:Lolm;

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lpur;->a:Lolm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 46
    :cond_0
    iget v0, p0, Lpur;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 47
    const/4 v0, 0x2

    iget v1, p0, Lpur;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 49
    :cond_1
    iget-object v0, p0, Lpur;->c:Lomr;

    if-eqz v0, :cond_2

    .line 50
    const/4 v0, 0x3

    iget-object v1, p0, Lpur;->c:Lomr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 52
    :cond_2
    iget-object v0, p0, Lpur;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 53
    const/4 v0, 0x4

    iget-object v1, p0, Lpur;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 55
    :cond_3
    iget-object v0, p0, Lpur;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 57
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpur;->a(Loxn;)Lpur;

    move-result-object v0

    return-object v0
.end method
