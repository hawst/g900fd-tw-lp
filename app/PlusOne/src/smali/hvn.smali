.class final Lhvn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lhvm;


# direct methods
.method constructor <init>(Lhvm;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lhvn;->a:Lhvm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/16 v1, 0x190

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 104
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4}, Lhvm;->a(Lhvm;)I

    move-result v4

    if-gtz v4, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-virtual {v4}, Lhvm;->invalidate()V

    .line 109
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4}, Lhvm;->b(Lhvm;)I

    .line 110
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4}, Lhvm;->c(Lhvm;)I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_2

    .line 111
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4, v0}, Lhvm;->a(Lhvm;I)I

    .line 118
    :cond_2
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4}, Lhvm;->c(Lhvm;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    move v1, v2

    .line 159
    :goto_1
    iget-object v3, p0, Lhvn;->a:Lhvm;

    invoke-virtual {v3}, Lhvm;->getAlpha()F

    move-result v3

    .line 160
    cmpl-float v3, v3, v2

    if-eqz v3, :cond_3

    .line 161
    iget-object v3, p0, Lhvn;->a:Lhvm;

    invoke-virtual {v3, v2}, Lhvm;->setAlpha(F)V

    .line 164
    :cond_3
    cmpl-float v2, v2, v1

    if-eqz v2, :cond_4

    .line 165
    iget-object v2, p0, Lhvn;->a:Lhvm;

    invoke-virtual {v2}, Lhvm;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 166
    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    .line 167
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 168
    invoke-static {}, Lhvm;->e()Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 169
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 172
    :cond_4
    iget-object v1, p0, Lhvn;->a:Lhvm;

    invoke-static {v1}, Lhvm;->g(Lhvm;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 173
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 120
    :pswitch_0
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4}, Lhvm;->d(Lhvm;)I

    .line 121
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4}, Lhvm;->e(Lhvm;)I

    move-result v4

    iget-object v5, p0, Lhvn;->a:Lhvm;

    invoke-static {v5}, Lhvm;->a(Lhvm;)I

    move-result v5

    if-lt v4, v5, :cond_5

    .line 122
    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4, v0}, Lhvm;->b(Lhvm;I)I

    .line 124
    :cond_5
    iget-object v0, p0, Lhvn;->a:Lhvm;

    invoke-virtual {v0}, Lhvm;->b()V

    .line 126
    iget-object v0, p0, Lhvn;->a:Lhvm;

    invoke-static {v0}, Lhvm;->f(Lhvm;)Lhvo;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 127
    iget-object v0, p0, Lhvn;->a:Lhvm;

    invoke-static {v0}, Lhvm;->f(Lhvm;)Lhvo;

    move-result-object v0

    iget-object v4, p0, Lhvn;->a:Lhvm;

    invoke-static {v4}, Lhvm;->e(Lhvm;)I

    move-result v4

    iget-object v5, p0, Lhvn;->a:Lhvm;

    invoke-static {v5}, Lhvm;->a(Lhvm;)I

    move-result v5

    invoke-interface {v0, v4, v5}, Lhvo;->a(II)V

    :cond_6
    move v0, v1

    move v1, v2

    move v2, v3

    .line 133
    goto :goto_1

    .line 137
    :pswitch_1
    iget-object v1, p0, Lhvn;->a:Lhvm;

    invoke-virtual {v1}, Lhvm;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 138
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 139
    :goto_2
    mul-int/lit8 v0, v0, 0x46

    .line 143
    const/16 v1, 0xbb8

    const/16 v3, 0x1f40

    .line 144
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 143
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v2

    .line 145
    goto/16 :goto_1

    .line 138
    :cond_7
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto :goto_2

    :pswitch_2
    move v0, v1

    move v1, v3

    .line 151
    goto/16 :goto_1

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
