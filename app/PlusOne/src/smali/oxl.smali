.class public final Loxl;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1159
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1180
    const/4 v0, 0x0

    .line 1181
    iget-object v1, p0, Loxl;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1182
    const/4 v0, 0x1

    iget-object v1, p0, Loxl;->a:Ljava/lang/Integer;

    .line 1183
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1185
    :cond_0
    iget-object v1, p0, Loxl;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1186
    const/4 v1, 0x2

    iget-object v2, p0, Loxl;->b:Ljava/lang/Integer;

    .line 1187
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1189
    :cond_1
    iget-object v1, p0, Loxl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1190
    iput v0, p0, Loxl;->ai:I

    .line 1191
    return v0
.end method

.method public a(Loxn;)Loxl;
    .locals 2

    .prologue
    .line 1199
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1200
    sparse-switch v0, :sswitch_data_0

    .line 1204
    iget-object v1, p0, Loxl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1205
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loxl;->ah:Ljava/util/List;

    .line 1208
    :cond_1
    iget-object v1, p0, Loxl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1210
    :sswitch_0
    return-object p0

    .line 1215
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxl;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 1219
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxl;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1200
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1168
    iget-object v0, p0, Loxl;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1169
    const/4 v0, 0x1

    iget-object v1, p0, Loxl;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1171
    :cond_0
    iget-object v0, p0, Loxl;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1172
    const/4 v0, 0x2

    iget-object v1, p0, Loxl;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1174
    :cond_1
    iget-object v0, p0, Loxl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1176
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1155
    invoke-virtual {p0, p1}, Loxl;->a(Loxn;)Loxl;

    move-result-object v0

    return-object v0
.end method
