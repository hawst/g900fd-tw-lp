.class public final Lnem;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnem;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2611
    const/4 v0, 0x0

    new-array v0, v0, [Lnem;

    sput-object v0, Lnem;->a:[Lnem;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2612
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2622
    const/high16 v0, -0x80000000

    iput v0, p0, Lnem;->b:I

    .line 2612
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2651
    const/4 v0, 0x0

    .line 2652
    iget v1, p0, Lnem;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 2653
    const/4 v0, 0x1

    iget v1, p0, Lnem;->b:I

    .line 2654
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2656
    :cond_0
    iget-object v1, p0, Lnem;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2657
    const/4 v1, 0x2

    iget-object v2, p0, Lnem;->c:Ljava/lang/String;

    .line 2658
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2660
    :cond_1
    iget-object v1, p0, Lnem;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2661
    const/4 v1, 0x3

    iget-object v2, p0, Lnem;->d:Ljava/lang/Integer;

    .line 2662
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2664
    :cond_2
    iget-object v1, p0, Lnem;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2665
    const/4 v1, 0x4

    iget-object v2, p0, Lnem;->e:Ljava/lang/Integer;

    .line 2666
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2668
    :cond_3
    iget-object v1, p0, Lnem;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2669
    iput v0, p0, Lnem;->ai:I

    .line 2670
    return v0
.end method

.method public a(Loxn;)Lnem;
    .locals 2

    .prologue
    .line 2678
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2679
    sparse-switch v0, :sswitch_data_0

    .line 2683
    iget-object v1, p0, Lnem;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2684
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnem;->ah:Ljava/util/List;

    .line 2687
    :cond_1
    iget-object v1, p0, Lnem;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2689
    :sswitch_0
    return-object p0

    .line 2694
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2695
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 2699
    :cond_2
    iput v0, p0, Lnem;->b:I

    goto :goto_0

    .line 2701
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnem;->b:I

    goto :goto_0

    .line 2706
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnem;->c:Ljava/lang/String;

    goto :goto_0

    .line 2710
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnem;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2714
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnem;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 2679
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2633
    iget v0, p0, Lnem;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2634
    const/4 v0, 0x1

    iget v1, p0, Lnem;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2636
    :cond_0
    iget-object v0, p0, Lnem;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2637
    const/4 v0, 0x2

    iget-object v1, p0, Lnem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2639
    :cond_1
    iget-object v0, p0, Lnem;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2640
    const/4 v0, 0x3

    iget-object v1, p0, Lnem;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2642
    :cond_2
    iget-object v0, p0, Lnem;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2643
    const/4 v0, 0x4

    iget-object v1, p0, Lnem;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2645
    :cond_3
    iget-object v0, p0, Lnem;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2647
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2608
    invoke-virtual {p0, p1}, Lnem;->a(Loxn;)Lnem;

    move-result-object v0

    return-object v0
.end method
