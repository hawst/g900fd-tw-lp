.class public final Ljbp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private a:Landroid/hardware/Camera;

.field private b:Landroid/app/Activity;

.field private c:Landroid/view/TextureView;

.field private d:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/TextureView;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Ljbp;->d:Ljava/util/Timer;

    .line 36
    iput-object p1, p0, Ljbp;->b:Landroid/app/Activity;

    .line 37
    if-eqz p2, :cond_0

    .line 38
    iput-object p2, p0, Ljbp;->c:Landroid/view/TextureView;

    .line 39
    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 40
    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 43
    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 44
    invoke-direct {p0}, Ljbp;->c()V

    .line 47
    :cond_0
    return-void
.end method

.method private static a(Landroid/hardware/Camera$Size;D)D
    .locals 5

    .prologue
    .line 114
    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    int-to-double v0, v0

    iget v2, p0, Landroid/hardware/Camera$Size;->height:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 115
    sub-double v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Landroid/hardware/Camera$Size;II)D
    .locals 2

    .prologue
    .line 119
    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    int-to-double v0, v0

    return-wide v0
.end method

.method static synthetic a(Ljbp;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    new-instance v1, Ljbr;

    invoke-direct {v1, p0}, Ljbr;-><init>(Ljbp;)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private b()V
    .locals 21

    .prologue
    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Ljbp;->a:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v12

    .line 106
    invoke-virtual {v12}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v3

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Ljbp;->c:Landroid/view/TextureView;

    .line 108
    invoke-virtual {v2}, Landroid/view/TextureView;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v2, v0, Ljbp;->c:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getHeight()I

    move-result v14

    .line 107
    int-to-double v4, v13

    int-to-double v6, v14

    div-double v16, v4, v6

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    const/4 v3, 0x0

    .line 109
    :cond_1
    iget v2, v3, Landroid/hardware/Camera$Size;->width:I

    iget v3, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v12, v2, v3}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Ljbp;->a:Landroid/hardware/Camera;

    invoke-virtual {v2, v12}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 111
    return-void

    .line 107
    :cond_2
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1}, Ljbp;->a(Landroid/hardware/Camera$Size;D)D

    move-result-wide v6

    invoke-static {v2, v13, v14}, Ljbp;->a(Landroid/hardware/Camera$Size;II)D

    move-result-wide v4

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move-object v3, v2

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1}, Ljbp;->a(Landroid/hardware/Camera$Size;D)D

    move-result-wide v8

    invoke-static {v2, v13, v14}, Ljbp;->a(Landroid/hardware/Camera$Size;II)D

    move-result-wide v10

    cmpg-double v18, v8, v6

    if-gez v18, :cond_3

    move-wide v6, v8

    move-object v3, v2

    goto :goto_0

    :cond_3
    cmpl-double v18, v8, v6

    if-nez v18, :cond_4

    cmpl-double v18, v4, v10

    if-lez v18, :cond_4

    move-wide v4, v8

    move-object v6, v2

    move-wide v2, v10

    :goto_1
    move-wide/from16 v19, v2

    move-object v3, v6

    move-wide v6, v4

    move-wide/from16 v4, v19

    goto :goto_0

    :cond_4
    move-wide/from16 v19, v4

    move-wide v4, v6

    move-object v6, v3

    move-wide/from16 v2, v19

    goto :goto_1
.end method

.method static synthetic b(Ljbp;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljbp;->c()V

    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 181
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    new-instance v3, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v3}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_2

    invoke-static {v0, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v4, v3, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v4, :cond_1

    .line 184
    :goto_1
    iget-object v2, p0, Ljbp;->c:Landroid/view/TextureView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Ljbp;->c:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v2

    if-eqz v2, :cond_0

    if-ltz v0, :cond_0

    iget-object v2, p0, Ljbp;->a:Landroid/hardware/Camera;

    if-eqz v2, :cond_4

    .line 199
    :cond_0
    :goto_2
    return-void

    .line 181
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    if-lez v2, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, -0x1

    goto :goto_1

    .line 189
    :cond_4
    :try_start_0
    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v2

    iput-object v2, p0, Ljbp;->a:Landroid/hardware/Camera;

    .line 190
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {v0, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget-object v0, p0, Ljbp;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_3
    :pswitch_0
    iget v0, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_5

    iget v0, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    :goto_4
    iget-object v1, p0, Ljbp;->a:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 191
    invoke-direct {p0}, Ljbp;->b()V

    .line 192
    iget-object v0, p0, Ljbp;->a:Landroid/hardware/Camera;

    iget-object v1, p0, Ljbp;->c:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 193
    iget-object v0, p0, Ljbp;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 194
    iget-object v0, p0, Ljbp;->d:Ljava/util/Timer;

    new-instance v1, Ljbq;

    invoke-direct {v1, p0}, Ljbq;-><init>(Ljbp;)V

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 195
    :catch_0
    move-exception v0

    .line 196
    invoke-virtual {p0}, Ljbp;->a()V

    .line 197
    const-string v1, "CameraPreviewListener"

    const-string v2, "Failed to connect preview texture."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 190
    :pswitch_1
    const/16 v1, 0x5a

    goto :goto_3

    :pswitch_2
    const/16 v1, 0xb4

    goto :goto_3

    :pswitch_3
    const/16 v1, 0x10e

    goto :goto_3

    :cond_5
    :try_start_1
    iget v0, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    iget-object v0, p0, Ljbp;->a:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Ljbp;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 56
    iget-object v0, p0, Ljbp;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 58
    :cond_0
    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    invoke-virtual {v0, v2}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 60
    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 62
    :cond_1
    iget-object v0, p0, Ljbp;->d:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 63
    iput-object v2, p0, Ljbp;->a:Landroid/hardware/Camera;

    .line 64
    iput-object v2, p0, Ljbp;->c:Landroid/view/TextureView;

    .line 65
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Ljbp;->c:Landroid/view/TextureView;

    new-instance v1, Ljbs;

    invoke-direct {v1, p0}, Ljbs;-><init>(Ljbp;)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->post(Ljava/lang/Runnable;)Z

    .line 211
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 218
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 217
    return-void
.end method
