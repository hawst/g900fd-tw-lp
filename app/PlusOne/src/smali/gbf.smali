.class public final Lgbf;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Lkzt;

.field private c:Z

.field private d:J

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Lgbg;

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/libraries/social/avatars/ui/AvatarView;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/ImageView;

.field private k:Lizu;

.field private l:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private m:I

.field private n:I

.field private o:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 98
    sget-object v0, Lgbf;->a:Llct;

    if-nez v0, :cond_0

    .line 99
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lgbf;->a:Llct;

    .line 101
    :cond_0
    new-instance v0, Lgbg;

    invoke-direct {v0, p1}, Lgbg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbf;->g:Lgbg;

    .line 102
    iget-boolean v0, p0, Lgbf;->c:Z

    if-nez v0, :cond_1

    const/16 v3, 0xc

    :goto_0
    sget-object v0, Lgbf;->a:Llct;

    iget v4, v0, Llct;->ba:I

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, Lkcr;->a(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSingleLine(Z)V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    sget-object v1, Lgbf;->a:Llct;

    iget v1, v1, Llct;->n:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setCompoundDrawablePadding(I)V

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lgbf;->i:Landroid/widget/Button;

    .line 103
    iget-object v0, p0, Lgbf;->i:Landroid/widget/Button;

    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a08fb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 104
    new-instance v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 105
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 107
    invoke-virtual {p0}, Lgbf;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a096a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    .line 109
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbf;->j:Landroid/widget/ImageView;

    .line 110
    iget-object v0, p0, Lgbf;->j:Landroid/widget/ImageView;

    sget-object v1, Lgbf;->a:Llct;

    iget-object v1, v1, Llct;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 111
    iget-object v0, p0, Lgbf;->j:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 112
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    .line 113
    return-void

    .line 102
    :cond_1
    const/4 v3, 0x6

    goto :goto_0
.end method

.method private e()I
    .locals 2

    .prologue
    .line 244
    iget v0, p0, Lgbf;->e:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhss;->a(Landroid/content/Context;I)I

    move-result v0

    sget-object v1, Lgbf;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()I
    .locals 2

    .prologue
    .line 401
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 402
    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    return v0
.end method

.method private g()Z
    .locals 3

    .prologue
    .line 463
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 464
    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 465
    iget-object v1, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v1}, Lkzt;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ldxd;->d:Lief;

    .line 466
    invoke-direct {p0}, Lgbf;->f()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 250
    invoke-virtual {p0}, Lgbf;->d()V

    .line 251
    iput v1, p0, Lgbf;->e:I

    .line 252
    iget-object v0, p0, Lgbf;->g:Lgbg;

    invoke-virtual {v0}, Lgbg;->a()V

    .line 253
    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 254
    iput v1, p0, Lgbf;->m:I

    .line 255
    iput v1, p0, Lgbf;->n:I

    .line 256
    iput-object v2, p0, Lgbf;->k:Lizu;

    .line 257
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgbf;->d:J

    .line 258
    invoke-virtual {p0}, Lgbf;->removeAllViews()V

    .line 259
    iput-object v2, p0, Lgbf;->f:Ljava/lang/String;

    .line 260
    return-void
.end method

.method public a(Lkzt;Ljava/lang/String;IJ)V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 150
    invoke-virtual {p0}, Lgbf;->removeAllViews()V

    .line 151
    iput v3, p0, Lgbf;->e:I

    .line 152
    iput-object p1, p0, Lgbf;->b:Lkzt;

    .line 154
    iget-object v0, p0, Lgbf;->b:Lkzt;

    if-eqz v0, :cond_3

    .line 155
    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lgbf;->c:Z

    .line 156
    iput-wide p4, p0, Lgbf;->d:J

    .line 157
    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->f()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lgbf;->c:Z

    if-eqz v1, :cond_5

    .line 159
    :cond_0
    iget-boolean v0, p0, Lgbf;->c:Z

    if-eqz v0, :cond_e

    .line 160
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 161
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 162
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v0}, Lgbf;->addView(Landroid/view/View;)V

    .line 163
    iget-object v0, p0, Lgbf;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lgbf;->addView(Landroid/view/View;)V

    .line 166
    invoke-direct {p0}, Lgbf;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->j()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 167
    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 168
    iget-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    .line 169
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f0a096c

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 168
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    :goto_1
    iget-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lgbf;->addView(Landroid/view/View;)V

    .line 183
    :cond_1
    :goto_2
    iget-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    sget-object v1, Lgbf;->a:Llct;

    iget v1, v1, Llct;->O:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 184
    iget-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    sget-object v1, Lgbf;->a:Llct;

    iget v1, v1, Llct;->P:I

    sget-object v4, Lgbf;->a:Llct;

    iget v4, v4, Llct;->S:I

    sget-object v5, Lgbf;->a:Llct;

    iget v5, v5, Llct;->P:I

    sget-object v6, Lgbf;->a:Llct;

    iget v6, v6, Llct;->S:I

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 189
    int-to-double v0, p3

    const-wide v4, 0x3ffc51eb851eb852L    # 1.77

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lgbf;->m:I

    .line 190
    iput p3, p0, Lgbf;->n:I

    .line 191
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v0, p0, Lgbf;->c:Z

    if-nez v0, :cond_8

    const-string v0, ""

    :goto_3
    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v1, v0, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lgbf;->k:Lizu;

    .line 192
    invoke-direct {p0}, Lgbf;->g()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 193
    const-string v0, "https://plus.google.com/hangouts/onair/watch?hl=%locale%&d=r&hid="

    invoke-static {v0}, Litk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lgbf;->b:Lkzt;

    .line 194
    invoke-virtual {v0}, Lkzt;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lgbf;->f:Ljava/lang/String;

    .line 202
    :cond_2
    :goto_5
    iget-boolean v0, p0, Lgbf;->c:Z

    if-nez v0, :cond_f

    .line 203
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lgbf;->setBackgroundColor(I)V

    .line 208
    :goto_6
    iget-object v0, p0, Lgbf;->g:Lgbg;

    iget-object v1, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0, v1, p2}, Lgbg;->a(Lkzt;Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lgbf;->g:Lgbg;

    invoke-virtual {p0, v0}, Lgbf;->addView(Landroid/view/View;)V

    .line 211
    :cond_3
    return-void

    :cond_4
    move v0, v3

    .line 155
    goto/16 :goto_0

    .line 157
    :cond_5
    iget-object v1, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iput v3, p0, Lgbf;->e:I

    sget-object v1, Lgbf;->a:Llct;

    iget v1, v1, Llct;->N:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v8}, Lhss;->a(Landroid/content/Context;I)I

    move-result v1

    sget-object v4, Lgbf;->a:Llct;

    iget v4, v4, Llct;->J:I

    add-int/2addr v1, v4

    div-int v1, p3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lgbf;->e:I

    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->b()Ljava/util/ArrayList;

    move-result-object v5

    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->c()Ljava/util/ArrayList;

    move-result-object v6

    move v4, v3

    :goto_7
    iget v0, p0, Lgbf;->e:I

    if-ge v4, v0, :cond_0

    new-instance v7, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v7, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b()V

    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v7}, Lgbf;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_7

    .line 172
    :cond_6
    iget-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    .line 173
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f0a096d

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 177
    :cond_7
    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    .line 179
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f0a096b

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lgbf;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 191
    :cond_8
    iget-wide v4, p0, Lgbf;->d:J

    const-wide/16 v6, 0x2710

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x493e0

    div-long/2addr v4, v6

    long-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    const v4, 0x45c11f

    if-le v0, v4, :cond_a

    const-string v4, "https://i1.ytimg.com/vi/"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "%s/wide_360p_v%s.jpg"

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v7}, Lkzt;->d()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_9
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_a
    const-string v4, "https://i1.ytimg.com/vi/"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "%s/sddefault_v%s.jpg"

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v7}, Lkzt;->d()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 194
    :cond_c
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 196
    :cond_d
    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llnf;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbf;->f:Ljava/lang/String;

    goto/16 :goto_5

    .line 198
    :cond_e
    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199
    iget-object v0, p0, Lgbf;->i:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lgbf;->addView(Landroid/view/View;)V

    goto/16 :goto_5

    .line 205
    :cond_f
    invoke-virtual {p0, v3}, Lgbf;->setBackgroundColor(I)V

    goto/16 :goto_6
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lgbf;->b:Lkzt;

    if-nez v0, :cond_0

    .line 118
    const-string v0, ""

    .line 126
    :goto_0
    return-object v0

    .line 120
    :cond_0
    iget-object v0, p0, Lgbf;->g:Lgbg;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lgbf;->g:Lgbg;

    invoke-virtual {v0}, Lgbg;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 123
    :cond_1
    iget-boolean v0, p0, Lgbf;->c:Z

    if-eqz v0, :cond_2

    .line 124
    invoke-virtual {p0}, Lgbf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a096a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {p0}, Lgbf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0968

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 418
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 419
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 420
    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b()V

    .line 419
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 422
    :cond_0
    iget-object v0, p0, Lgbf;->k:Lizu;

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v1, p0, Lgbf;->k:Lizu;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 426
    :cond_1
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 429
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 430
    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 429
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 432
    :cond_0
    iget-object v0, p0, Lgbf;->k:Lizu;

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 435
    :cond_1
    return-void
.end method

.method public synthetic getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lgbf;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 413
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 414
    invoke-virtual {p0}, Lgbf;->c()V

    .line 415
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 364
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 365
    invoke-direct {p0}, Lgbf;->f()I

    move-result v0

    .line 366
    instance-of v2, p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v2, :cond_1

    .line 367
    check-cast p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e()Ljava/lang/String;

    move-result-object v2

    .line 368
    invoke-static {v1, v0, v2, v5}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 370
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v2, p0, Lgbf;->i:Landroid/widget/Button;

    if-ne p1, v2, :cond_4

    .line 372
    iget-object v2, p0, Lgbf;->b:Lkzt;

    .line 373
    invoke-virtual {v2}, Lkzt;->a()Ljava/lang/String;

    move-result-object v2

    .line 372
    const-string v3, "vnd.google.android.hangouts/vnd.google.android.hangout_privileged"

    const/4 v4, 0x1

    invoke-static {v1, v3, v0, v4}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_2

    const-string v4, "hangout_uri"

    const-string v5, "https://plus.google.com/hangouts/_/"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "hangout_start_source"

    const/16 v2, 0x1d

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 374
    :cond_2
    invoke-static {v1, v3}, Leyq;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0

    .line 372
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 375
    :cond_4
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-ne p1, v0, :cond_0

    .line 376
    iget-object v0, p0, Lgbf;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 377
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 378
    invoke-direct {p0}, Lgbf;->g()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 379
    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 383
    :cond_5
    const/high16 v3, 0x80000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 385
    const-string v3, "com.google.android.youtube"

    invoke-static {v1, v3}, Lfug;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 386
    const-string v3, "com.google.android.youtube"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 387
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    if-nez v3, :cond_6

    .line 388
    invoke-virtual {v2, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    :cond_6
    :try_start_0
    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 395
    :catch_0
    move-exception v2

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 407
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 408
    invoke-virtual {p0}, Lgbf;->d()V

    .line 409
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 269
    .line 271
    iget-object v0, p0, Lgbf;->b:Lkzt;

    if-eqz v0, :cond_3

    .line 272
    iget v0, p0, Lgbf;->e:I

    if-lez v0, :cond_5

    .line 273
    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v4

    .line 274
    sget-object v0, Lgbf;->a:Llct;

    iget v0, v0, Llct;->C:I

    add-int/2addr v0, p2

    .line 275
    sget-object v1, Lgbf;->a:Llct;

    iget v5, v1, Llct;->m:I

    move v1, v2

    move v3, v0

    .line 276
    :goto_0
    iget v0, p0, Lgbf;->e:I

    if-ge v1, v0, :cond_0

    .line 277
    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 278
    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v3

    add-int v7, v5, v4

    invoke-virtual {v0, v3, v5, v6, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 280
    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getWidth()I

    move-result v0

    sget-object v6, Lgbf;->a:Llct;

    iget v6, v6, Llct;->J:I

    add-int/2addr v0, v6

    add-int/2addr v3, v0

    .line 276
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 282
    :cond_0
    sget-object v0, Lgbf;->a:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, v4

    .line 285
    :goto_1
    iget-boolean v1, p0, Lgbf;->c:Z

    if-eqz v1, :cond_1

    .line 286
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    sget-object v1, Lgbf;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    .line 290
    :cond_1
    iget-object v1, p0, Lgbf;->g:Lgbg;

    invoke-virtual {v1, p2, v0, p4, p5}, Lgbg;->layout(IIII)V

    .line 292
    iget-boolean v0, p0, Lgbf;->c:Z

    if-eqz v0, :cond_4

    .line 293
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v0

    .line 294
    iget-object v1, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v1

    .line 295
    iget-object v3, p0, Lgbf;->j:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    .line 296
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v4, v3, 0x2

    sub-int/2addr v0, v4

    .line 297
    div-int/lit8 v1, v1, 0x2

    div-int/lit8 v4, v3, 0x2

    sub-int/2addr v1, v4

    .line 298
    iget-object v4, p0, Lgbf;->j:Landroid/widget/ImageView;

    add-int v5, p2, v0

    add-int/2addr v0, v3

    iget-object v3, p0, Lgbf;->j:Landroid/widget/ImageView;

    .line 299
    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    .line 298
    invoke-virtual {v4, v5, v1, v0, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 300
    iget-object v0, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v1, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p2

    iget-object v3, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 301
    invoke-virtual {v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v3

    .line 300
    invoke-virtual {v0, p2, v2, v1, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 302
    iget-object v0, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->g()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lgbf;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 303
    :cond_2
    iget-object v0, p0, Lgbf;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    sub-int v0, p4, v0

    sget-object v1, Lgbf;->a:Llct;

    iget v1, v1, Llct;->C:I

    sub-int/2addr v0, v1

    .line 305
    sget-object v1, Lgbf;->a:Llct;

    iget v1, v1, Llct;->C:I

    .line 306
    iget-object v2, p0, Lgbf;->o:Landroid/widget/TextView;

    iget-object v3, p0, Lgbf;->o:Landroid/widget/TextView;

    .line 307
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lgbf;->o:Landroid/widget/TextView;

    .line 308
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 306
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 317
    :cond_3
    :goto_2
    return-void

    .line 311
    :cond_4
    invoke-direct {p0}, Lgbf;->e()I

    move-result v0

    iget-object v1, p0, Lgbf;->g:Lgbg;

    invoke-virtual {v1}, Lgbg;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    iget-object v1, p0, Lgbf;->i:Landroid/widget/Button;

    sget-object v2, Lgbf;->a:Llct;

    iget v2, v2, Llct;->C:I

    add-int/2addr v2, p2

    sget-object v3, Lgbf;->a:Llct;

    iget v3, v3, Llct;->C:I

    add-int/2addr v3, p2

    iget-object v4, p0, Lgbf;->i:Landroid/widget/Button;

    .line 313
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lgbf;->i:Landroid/widget/Button;

    .line 314
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 312
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/widget/Button;->layout(IIII)V

    goto :goto_2

    :cond_5
    move v0, v2

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v0, 0x0

    const/high16 v5, -0x80000000

    .line 321
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 323
    iget-object v1, p0, Lgbf;->b:Lkzt;

    if-eqz v1, :cond_3

    .line 324
    iget-boolean v1, p0, Lgbf;->c:Z

    if-eqz v1, :cond_2

    .line 325
    iget-object v1, p0, Lgbf;->j:Landroid/widget/ImageView;

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 326
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 325
    invoke-virtual {v1, v3, v4}, Landroid/widget/ImageView;->measure(II)V

    .line 327
    iget-object v1, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget v3, p0, Lgbf;->n:I

    .line 328
    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v4, p0, Lgbf;->m:I

    .line 329
    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 327
    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 330
    iget-object v1, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v1}, Lkzt;->g()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lgbf;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 331
    :cond_0
    iget-object v1, p0, Lgbf;->o:Landroid/widget/TextView;

    iget v3, p0, Lgbf;->n:I

    .line 332
    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v4, p0, Lgbf;->n:I

    .line 333
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 331
    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 340
    :cond_1
    :goto_0
    iget-object v1, p0, Lgbf;->g:Lgbg;

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 341
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 340
    invoke-virtual {v1, v3, v4}, Lgbg;->measure(II)V

    move v1, v0

    .line 342
    :goto_1
    iget v0, p0, Lgbf;->e:I

    if-ge v1, v0, :cond_3

    .line 343
    iget-object v0, p0, Lgbf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 344
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 345
    invoke-virtual {p0}, Lgbf;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lhss;->c(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 344
    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 342
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 336
    :cond_2
    iget-object v1, p0, Lgbf;->i:Landroid/widget/Button;

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 337
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 336
    invoke-virtual {v1, v3, v4}, Landroid/widget/Button;->measure(II)V

    goto :goto_0

    .line 350
    :cond_3
    sget-object v0, Lgbf;->a:Llct;

    iget v0, v0, Llct;->m:I

    invoke-direct {p0}, Lgbf;->e()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lgbf;->g:Lgbg;

    .line 351
    invoke-virtual {v1}, Lgbg;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 352
    iget-boolean v1, p0, Lgbf;->c:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_4

    .line 353
    iget-object v1, p0, Lgbf;->l:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_4
    iget-object v1, p0, Lgbf;->b:Lkzt;

    invoke-virtual {v1}, Lkzt;->h()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lgbf;->i:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_5

    .line 356
    iget-object v1, p0, Lgbf;->i:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_5
    invoke-virtual {p0, v2, v0}, Lgbf;->setMeasuredDimension(II)V

    .line 360
    return-void
.end method
