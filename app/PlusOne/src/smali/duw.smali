.class final Lduw;
.super Ldvc;
.source "PG"


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;

.field private synthetic c:Ldur;


# direct methods
.method public constructor <init>(Ldur;IIILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1592
    iput-object p1, p0, Lduw;->c:Ldur;

    .line 1593
    invoke-direct {p0, p2, p3, p4}, Ldvc;-><init>(III)V

    .line 1594
    iput-object p5, p0, Lduw;->b:Ljava/lang/String;

    .line 1595
    iput-boolean p6, p0, Lduw;->a:Z

    .line 1596
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1600
    const-string v0, "(SELECT image_url as highlights_image_url, view_order as highlights_view_order FROM all_tiles WHERE view_id = ? AND type = ? AND media_attr & 512 == 0 LIMIT ? OFFSET ?) INNER JOIN media_cache ON (highlights_image_url = media_cache.image_url)"

    return-object v0
.end method

.method protected a(I)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1624
    iget-boolean v0, p0, Lduw;->a:Z

    if-eqz v0, :cond_0

    .line 1625
    new-array v0, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1631
    :goto_0
    return-object v0

    .line 1627
    :cond_0
    iget-object v0, p0, Lduw;->c:Ldur;

    invoke-static {v0}, Ldur;->b(Ldur;)Lhei;

    move-result-object v0

    iget-object v1, p0, Lduw;->c:Ldur;

    invoke-static {v1}, Ldur;->a(Ldur;)Ldve;

    move-result-object v1

    iget v1, v1, Ldve;->a:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1628
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 1629
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    new-array v2, v4, [Ljava/lang/String;

    aput-object v1, v2, v3

    .line 1630
    invoke-static {v3, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const/4 v2, 0x4

    .line 1631
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1605
    iget-boolean v0, p0, Lduw;->a:Z

    if-eqz v0, :cond_0

    .line 1606
    const-string v0, "http_status = 200 AND representation_type & ? != 0"

    .line 1608
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "http_status = 200 AND representation_type & ? != 0 AND highlights_image_url NOT IN (SELECT image_url FROM all_tiles WHERE view_id = ? AND type = ? AND media_attr & 512 == 0)"

    goto :goto_0
.end method

.method protected c()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1619
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lduw;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1640
    const-string v0, "highlights_view_order ASC"

    return-object v0
.end method
