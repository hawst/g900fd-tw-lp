.class public final Lkul;
.super Lehh;
.source "PG"

# interfaces
.implements Lktb;
.implements Lktc;
.implements Lkvs;
.implements Lkyk;
.implements Llgs;


# instance fields
.field public N:Ljava/lang/String;

.field public O:Ljava/lang/String;

.field private aA:Ljava/lang/String;

.field private aB:Z

.field private aC:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkyf;",
            ">;"
        }
    .end annotation
.end field

.field private aD:I

.field private aE:Lkuo;

.field private aF:Z

.field private aG:Z

.field private aH:Z

.field private aI:Ljava/lang/Boolean;

.field private aJ:Ljava/lang/Boolean;

.field private aK:Ljava/lang/Boolean;

.field private aL:Ljava/lang/Boolean;

.field private aM:Ljava/lang/String;

.field private aN:J

.field private aO:Lhym;

.field private aP:Lkvq;

.field private aQ:Liax;

.field private final aw:Lkvy;

.field private final ax:Lkss;

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 106
    invoke-direct {p0}, Lehh;-><init>()V

    .line 135
    new-instance v0, Lkvy;

    iget-object v1, p0, Lkul;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkvy;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lkul;->aw:Lkvy;

    .line 136
    new-instance v0, Lkss;

    iget-object v1, p0, Lkul;->av:Llqm;

    invoke-direct {v0, p0, v1, p0, p0}, Lkss;-><init>(Lu;Llqr;Lktc;Lktb;)V

    iput-object v0, p0, Lkul;->ax:Lkss;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lkul;->aD:I

    .line 153
    iput-boolean v2, p0, Lkul;->aG:Z

    .line 154
    iput-boolean v2, p0, Lkul;->aH:Z

    .line 171
    new-instance v0, Lhnw;

    new-instance v1, Lkum;

    invoke-direct {v1, p0}, Lkum;-><init>(Lkul;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 174
    return-void
.end method

.method private a(Lhmv;)V
    .locals 4

    .prologue
    .line 1077
    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1078
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lkul;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 1080
    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1078
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1082
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 790
    iget-object v0, p0, Lkul;->O:Ljava/lang/String;

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 817
    :goto_0
    return-void

    .line 798
    :cond_0
    iput-object p1, p0, Lkul;->O:Ljava/lang/String;

    .line 799
    iput-object p2, p0, Lkul;->aA:Ljava/lang/String;

    .line 803
    iput-boolean v3, p0, Lkul;->ai:Z

    .line 805
    iput-object v4, p0, Lkul;->Y:Ljava/lang/String;

    .line 807
    invoke-virtual {p0}, Lkul;->af_()V

    .line 808
    invoke-virtual {p0}, Lkul;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "stream_id"

    iget-object v2, p0, Lkul;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    invoke-virtual {p0}, Lkul;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 811
    iput-boolean v3, p0, Lkul;->al:Z

    .line 813
    invoke-direct {p0}, Lkul;->ah()V

    .line 816
    invoke-super {p0}, Lehh;->al()V

    goto :goto_0
.end method

.method private ah()V
    .locals 1

    .prologue
    .line 820
    iget-object v0, p0, Lkul;->ap:Levp;

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lkul;->aL:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 822
    iget-object v0, p0, Lkul;->ap:Levp;

    invoke-virtual {v0}, Levp;->a()V

    .line 827
    :cond_0
    :goto_0
    return-void

    .line 824
    :cond_1
    iget-object v0, p0, Lkul;->ap:Levp;

    invoke-virtual {v0}, Levp;->d()V

    goto :goto_0
.end method

.method private l(Z)V
    .locals 5

    .prologue
    .line 617
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    new-instance v1, Lkyr;

    iget-object v2, p0, Lkul;->at:Llnl;

    iget-object v3, p0, Lkul;->R:Lhee;

    .line 618
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Lkul;->N:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, p1}, Lkyr;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 617
    invoke-virtual {v0, v1}, Lhoc;->c(Lhny;)V

    .line 619
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->n()Ldyq;

    move-result-object v0

    const-string v1, "extra_square_id"

    iget-object v2, p0, Lkul;->N:Ljava/lang/String;

    invoke-static {v1, v2}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const/4 v2, 0x4

    iget v3, v0, Ldyq;->c:I

    iget-boolean v4, v0, Ldyq;->e:Z

    iget v0, v0, Ldyq;->c:I

    invoke-static {v2, v3, v4, v0, p1}, Lhmt;->a(IIZIZ)Lmxb;

    move-result-object v0

    const-string v2, "extra_notification_volume_change"

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lkul;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->ag:Lhmv;

    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 620
    return-void
.end method

.method private m(Z)Lkzz;
    .locals 7

    .prologue
    .line 1016
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->l()Ljava/lang/String;

    move-result-object v3

    .line 1017
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->k()Ljava/lang/String;

    move-result-object v4

    .line 1018
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->m()Z

    move-result v6

    .line 1020
    new-instance v0, Lkzz;

    iget-object v1, p0, Lkul;->N:Ljava/lang/String;

    iget-object v2, p0, Lkul;->az:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v6}, Lkzz;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 1061
    sget-object v0, Lhmw;->G:Lhmw;

    return-object v0
.end method

.method protected V()Z
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method public X()V
    .locals 5

    .prologue
    .line 662
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    new-instance v1, Lkti;

    iget-object v2, p0, Lkul;->at:Llnl;

    iget-object v3, p0, Lkul;->R:Lhee;

    .line 666
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Lkul;->N:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lkti;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 665
    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 667
    invoke-virtual {p0}, Lkul;->ay()V

    .line 668
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 274
    invoke-super {p0, p1, p2, p3}, Lehh;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 276
    iget-object v0, p0, Lkul;->W:Lfdj;

    check-cast v0, Lkuo;

    iput-object v0, p0, Lkul;->aE:Lkuo;

    .line 277
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0, p0}, Lkuo;->a(Lkyk;)V

    .line 278
    iget-object v0, p0, Lkul;->aE:Lkuo;

    iget-object v2, p0, Lkul;->aI:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Lkuo;->a(Ljava/lang/Boolean;)V

    .line 279
    iget-object v0, p0, Lkul;->aE:Lkuo;

    iget-object v2, p0, Lkul;->aJ:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Lkuo;->b(Ljava/lang/Boolean;)V

    .line 281
    iget-object v0, p0, Lkul;->ad:Ljgf;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lkul;->ad:Ljgf;

    const-string v2, "squares"

    iget-object v3, p0, Lkul;->N:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    int-to-long v4, v3

    invoke-interface {v0, v2, v4, v5}, Ljgf;->a(Ljava/lang/String;J)Z

    .line 287
    :cond_0
    iget-object v0, p0, Lkul;->ap:Levp;

    invoke-virtual {v0}, Levp;->d()V

    .line 289
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348
    packed-switch p1, :pswitch_data_0

    .line 355
    invoke-super {p0, p1, p2}, Lehh;->a(ILandroid/os/Bundle;)Ldo;

    move-result-object v0

    :goto_0
    return-object v0

    .line 350
    :pswitch_0
    new-instance v0, Lktn;

    iget-object v1, p0, Lkul;->at:Llnl;

    iget-object v2, p0, Lkul;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lkul;->N:Ljava/lang/String;

    sget-object v4, Lktv;->c:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lktn;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;
    .locals 9

    .prologue
    .line 266
    new-instance v0, Lkuo;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lkuo;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 323
    invoke-super {p0, p1, p2, p3}, Lehh;->a(IILandroid/content/Intent;)V

    .line 324
    packed-switch p1, :pswitch_data_0

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 327
    :pswitch_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 328
    iget-object v0, p0, Lkul;->at:Llnl;

    invoke-static {v0}, Ldyj;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 324
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1118
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1122
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1009
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1010
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1011
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1012
    invoke-virtual {p0, v0}, Lkul;->a(Landroid/content/Intent;)V

    .line 1013
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 191
    invoke-super {p0, p1}, Lehh;->a(Landroid/os/Bundle;)V

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkul;->aC:Ljava/util/List;

    .line 195
    if-eqz p1, :cond_4

    .line 196
    const-string v0, "square_expanded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    const-string v0, "square_expanded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lkul;->aI:Ljava/lang/Boolean;

    .line 199
    :cond_0
    const-string v0, "square_category_expanded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    const-string v0, "square_category_expanded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lkul;->aJ:Ljava/lang/Boolean;

    .line 203
    :cond_1
    const-string v0, "square_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    const-string v0, "square_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkul;->az:Ljava/lang/String;

    .line 206
    :cond_2
    const-string v0, "square_stream_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 207
    const-string v0, "square_stream_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkul;->aA:Ljava/lang/String;

    .line 210
    :cond_3
    const-string v0, "square_is_restricted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkul;->aB:Z

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkul;->aF:Z

    .line 217
    :goto_0
    invoke-virtual {p0}, Lkul;->w()Lbb;

    move-result-object v0

    const/16 v1, 0x64

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 219
    invoke-virtual {p0}, Lkul;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 220
    const-string v1, "com.google.android.libraries.social.notifications.notif_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkul;->aM:Ljava/lang/String;

    .line 221
    const-string v1, "com.google.android.libraries.social.notifications.updated_version"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lkul;->aN:J

    .line 223
    new-instance v0, Lkvq;

    iget-object v1, p0, Lkul;->at:Llnl;

    iget-object v2, p0, Lkul;->R:Lhee;

    .line 224
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, p0, v2}, Lkvq;-><init>(Landroid/content/Context;Lu;I)V

    iput-object v0, p0, Lkul;->aP:Lkvq;

    .line 225
    iget-object v0, p0, Lkul;->aP:Lkvq;

    invoke-virtual {v0, p0}, Lkvq;->a(Lkvs;)V

    .line 226
    return-void

    .line 214
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkul;->aF:Z

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1086
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lhoc;

    .line 1087
    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1088
    const-string v0, "decline_invitation"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1089
    new-instance v0, Lkvn;

    iget-object v2, p0, Lkul;->at:Llnl;

    iget-object v3, p0, Lkul;->N:Ljava/lang/String;

    invoke-direct {v0, v2, v1, v3}, Lkvn;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    .line 1092
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lkul;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->cr:Lhmv;

    .line 1094
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1092
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1106
    :cond_0
    :goto_0
    return-void

    .line 1096
    :cond_1
    const-string v0, "report_invite_abuse"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1097
    iget-object v0, p0, Lkul;->at:Llnl;

    const-class v2, Lieh;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 1099
    sget-object v2, Ldxd;->i:Lief;

    invoke-interface {v0, v2, v1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1100
    new-instance v0, Ldpn;

    iget-object v1, p0, Lkul;->at:Llnl;

    iget-object v2, p0, Lkul;->R:Lhee;

    .line 1101
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lkul;->aM:Ljava/lang/String;

    iget-wide v4, p0, Lkul;->aN:J

    invoke-direct/range {v0 .. v5}, Ldpn;-><init>(Landroid/content/Context;ILjava/lang/String;J)V

    .line 1100
    invoke-virtual {v6, v0}, Lhoc;->c(Lhny;)V

    goto :goto_0

    .line 1103
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 362
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 417
    invoke-super {p0, p1, p2}, Lehh;->a(Ldo;Landroid/database/Cursor;)V

    .line 421
    :goto_0
    return-void

    .line 364
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkul;->aG:Z

    .line 369
    const/4 v6, 0x0

    .line 371
    if-eqz p2, :cond_16

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 373
    const/16 v0, 0x17

    .line 374
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 375
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/32 v4, 0x124f80

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 377
    const/4 v6, 0x1

    .line 380
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    .line 381
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0, p2}, Lkuo;->b(Landroid/database/Cursor;)V

    .line 383
    const/16 v0, 0xc

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lkul;->aK:Ljava/lang/Boolean;

    .line 384
    const/16 v0, 0x8

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lkul;->aL:Ljava/lang/Boolean;

    .line 385
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkul;->az:Ljava/lang/String;

    .line 386
    const/16 v0, 0x1e

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lkul;->aB:Z

    .line 388
    const/16 v0, 0x12

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lktg;->a([B)[Lktg;

    move-result-object v8

    if-eqz v8, :cond_9

    array-length v1, v8

    :goto_4
    if-nez v8, :cond_1

    iget-object v0, p0, Lkul;->aK:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_1
    const/4 v0, 0x1

    if-ne v1, v0, :cond_a

    const/4 v1, 0x0

    const/4 v0, 0x0

    aget-object v0, v8, v0

    invoke-virtual {v0}, Lktg;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkul;->ay:Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v0, v8, v0

    invoke-virtual {v0}, Lktg;->b()Ljava/lang/String;

    move-result-object v0

    move v2, v1

    move-object v1, v0

    :goto_5
    const/4 v0, 0x0

    iget-object v3, p0, Lkul;->aC:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-boolean v0, p0, Lkul;->aH:Z

    if-nez v0, :cond_2

    if-ne v2, v4, :cond_2

    iget-object v0, p0, Lkul;->ay:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lkul;->ay:Ljava/lang/String;

    iget-object v3, p0, Lkul;->O:Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    :cond_2
    const/4 v0, 0x1

    move v3, v0

    :goto_6
    const-string v0, "HostedSquareStreamFrag"

    const/4 v5, 0x3

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lkul;->aH:Z

    iget-object v5, p0, Lkul;->ay:Ljava/lang/String;

    iget-object v7, p0, Lkul;->O:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x5c

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "populatePrimarySpinner firstLoad="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " numStreams="

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " old="

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " streamId="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " old="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkul;->aH:Z

    if-nez v3, :cond_4

    const/4 v0, 0x0

    move v4, v0

    :goto_7
    if-ge v4, v2, :cond_4

    aget-object v0, v8, v4

    invoke-virtual {v0}, Lktg;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lkul;->aC:Ljava/util/List;

    add-int/lit8 v7, v4, 0x1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkyf;

    invoke-virtual {v0}, Lkyf;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v3, 0x1

    :cond_4
    if-eqz v3, :cond_10

    const/4 v0, -0x1

    iget-object v3, p0, Lkul;->aC:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lkul;->ay:Ljava/lang/String;

    const/4 v3, 0x1

    if-le v2, v3, :cond_5

    iget-object v0, p0, Lkul;->aC:Ljava/util/List;

    new-instance v3, Lkyf;

    const v5, 0x7f0a0227

    invoke-virtual {p0, v5}, Lkul;->e_(I)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-direct {v3, v5, v7}, Lkyf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    :cond_5
    const/4 v3, 0x0

    move v7, v3

    move v5, v0

    :goto_8
    if-ge v7, v2, :cond_d

    aget-object v0, v8, v7

    invoke-virtual {v0}, Lktg;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lktg;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v9, p0, Lkul;->aC:Ljava/util/List;

    new-instance v10, Lkyf;

    invoke-direct {v10, v0, v3}, Lkyf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v9, p0, Lkul;->O:Ljava/lang/String;

    invoke-static {v9, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_17

    add-int/lit8 v1, v7, 0x1

    move-object v12, v3

    move v3, v1

    move-object v1, v12

    :goto_9
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v5, v3

    move-object v4, v1

    move-object v1, v0

    goto :goto_8

    .line 383
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 384
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 386
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 388
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_a
    const/4 v0, 0x0

    iput-object v0, p0, Lkul;->ay:Ljava/lang/String;

    const/4 v0, 0x0

    move v2, v1

    move-object v1, v0

    goto/16 :goto_5

    :cond_b
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_6

    :cond_c
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_7

    :cond_d
    iput v5, p0, Lkul;->aD:I

    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0, v5}, Lkuo;->b(I)V

    iget-object v0, p0, Lkul;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_e

    iget-object v0, p0, Lkul;->aE:Lkuo;

    iget-object v2, p0, Lkul;->aC:Ljava/util/List;

    invoke-virtual {v0, v2}, Lkuo;->a(Ljava/util/List;)V

    iget-object v0, p0, Lkul;->aE:Lkuo;

    iget v2, p0, Lkul;->aD:I

    invoke-virtual {v0, v2}, Lkuo;->b(I)V

    :cond_e
    iget-object v0, p0, Lkul;->ay:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v4, 0x0

    :cond_f
    invoke-direct {p0, v4, v1}, Lkul;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_10
    invoke-direct {p0}, Lkul;->ah()V

    .line 391
    iget-object v0, p0, Lkul;->Q:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 393
    iget-object v0, p0, Lkul;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 394
    const-string v0, "HostedSquareStreamFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 395
    const-string v0, "- setSquareData name="

    iget-object v1, p0, Lkul;->az:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_13

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move v0, v6

    .line 405
    :goto_a
    if-eqz v0, :cond_11

    .line 406
    invoke-virtual {p0}, Lkul;->X()V

    .line 409
    :cond_11
    const/4 v0, 0x0

    iget-object v1, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v1}, Lkuo;->p()Z

    move-result v1

    if-eqz v1, :cond_12

    new-instance v0, Lhym;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lhym;-><init>([Ljava/lang/String;I)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    invoke-virtual {p0}, Lkul;->aq()Z

    move-result v1

    if-eqz v1, :cond_12

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    :cond_12
    iput-object v0, p0, Lkul;->aO:Lhym;

    .line 411
    invoke-virtual {p0}, Lkul;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 412
    invoke-virtual {p0}, Lkul;->ay()V

    goto/16 :goto_0

    .line 395
    :cond_13
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move v0, v6

    goto :goto_a

    .line 397
    :cond_14
    invoke-virtual {p0}, Lkul;->az()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 398
    iget-object v0, p0, Lkul;->U:Licq;

    const v1, 0x7f0a057b

    invoke-virtual {v0, v1}, Licq;->a(I)Licq;

    .line 399
    iget-object v0, p0, Lkul;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    :cond_15
    move v0, v6

    .line 401
    goto :goto_a

    .line 402
    :cond_16
    const/4 v0, 0x1

    goto :goto_a

    :cond_17
    move-object v0, v1

    move v3, v5

    move-object v1, v4

    goto/16 :goto_9

    .line 362
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 106
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lkul;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 5

    .prologue
    const v4, 0x7f1006e0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 441
    const v0, 0x7f10067b

    .line 442
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 443
    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 444
    invoke-virtual {p0}, Lkul;->aj()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 445
    invoke-virtual {v0}, Lhjv;->b()V

    .line 448
    :cond_0
    iget-object v0, p0, Lkul;->az:Ljava/lang/String;

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 453
    iget-object v0, p0, Lkul;->aL:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkul;->aE:Lkuo;

    if-nez v0, :cond_7

    :cond_1
    move v0, v1

    .line 454
    :goto_0
    if-nez v0, :cond_5

    .line 457
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 458
    const v0, 0x7f10068b

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 459
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 460
    const v0, 0x7f1006d9

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 465
    :cond_2
    :goto_1
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->h()Z

    move-result v0

    if-nez v0, :cond_3

    .line 466
    iget-object v0, p0, Lkul;->at:Llnl;

    const-class v3, Lhee;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v3, "is_google_plus"

    .line 467
    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 468
    invoke-interface {p1, v4}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 471
    :cond_3
    iget-object v0, p0, Lkul;->aL:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 472
    const v0, 0x7f1006dd

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 473
    const v0, 0x7f1006de

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 475
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 476
    const v0, 0x7f1006dc

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 478
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->f()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 508
    :cond_4
    :goto_3
    invoke-virtual {p0}, Lkul;->V()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 509
    const v0, 0x7f10047f

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 514
    :cond_5
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 515
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lktz;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktz;

    .line 516
    if-eqz v0, :cond_6

    .line 517
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lkua;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkua;

    .line 518
    const v1, 0x7f100693

    const v3, 0x7f10005d

    .line 520
    invoke-interface {v0}, Lkua;->a()I

    move-result v0

    .line 518
    invoke-interface {p1, v1, v3, v2, v0}, Lhjk;->a(IIII)Landroid/view/MenuItem;

    .line 523
    :cond_6
    return-void

    :cond_7
    move v0, v2

    .line 453
    goto/16 :goto_0

    .line 462
    :cond_8
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 463
    const v0, 0x7f10068c

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 478
    goto :goto_2

    .line 480
    :cond_a
    const v0, 0x7f1006db

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 482
    iget-object v3, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v3}, Lkuo;->f()Z

    move-result v3

    if-nez v3, :cond_b

    :goto_4
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_3

    :cond_b
    move v1, v2

    goto :goto_4

    .line 485
    :cond_c
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->g()I

    move-result v0

    .line 486
    packed-switch v0, :pswitch_data_0

    .line 500
    :goto_5
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 501
    const v0, 0x7f1006da

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 504
    :cond_d
    iget-object v0, p0, Lkul;->aM:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 505
    invoke-interface {p1, v4}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto/16 :goto_3

    .line 488
    :pswitch_0
    const v0, 0x7f1006d4

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_5

    .line 491
    :pswitch_1
    const v0, 0x7f1006d6

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_5

    .line 494
    :pswitch_2
    const v0, 0x7f1006d7

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_5

    .line 497
    :pswitch_3
    const v0, 0x7f1006d5

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_5

    .line 486
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v6, -0x1

    .line 916
    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 917
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Lkul;->at:Llnl;

    invoke-direct {v1, v3, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 919
    invoke-static {p2}, Lkvu;->b(I)Lhmv;

    move-result-object v3

    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 917
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 921
    invoke-static {p2}, Lkvu;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 922
    invoke-virtual {p0}, Lkul;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 923
    const-string v1, "suggestion_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 924
    const-string v1, "suggestion_ui"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 925
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eq v5, v6, :cond_0

    .line 926
    iget-object v0, p0, Lkul;->aQ:Liax;

    iget-object v1, p0, Lkul;->at:Llnl;

    const-string v6, "g:"

    iget-object v3, p0, Lkul;->N:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    const/16 v6, 0xe6

    invoke-interface/range {v0 .. v6}, Liax;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 930
    :cond_0
    return-void

    .line 926
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ILhoz;)V
    .locals 1

    .prologue
    .line 934
    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 936
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkul;->aH:Z

    .line 938
    invoke-super {p0}, Lehh;->al()V

    .line 940
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 885
    invoke-super {p0, p1, p2, p3}, Lehh;->a(Ljava/lang/String;Lhoz;Lhos;)V

    .line 886
    invoke-virtual {p0}, Lkul;->ay()V

    .line 888
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 889
    const-string v0, "GetSquareTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 891
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkul;->ak:Z

    .line 892
    invoke-virtual {p0}, Lkul;->aD()V

    .line 894
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 895
    iget-object v0, p0, Lkul;->U:Licq;

    const v1, 0x7f0a058f

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 896
    iget-object v0, p0, Lkul;->U:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 898
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lhos;->a(Z)V

    .line 907
    :cond_1
    :goto_0
    return-void

    .line 901
    :cond_2
    const-string v0, "NotificationsReportAbuseTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 902
    invoke-virtual {p0}, Lkul;->aI()Z

    move-result v0

    if-nez v0, :cond_1

    .line 903
    invoke-virtual {p0}, Lkul;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 432
    invoke-super {p0, p1}, Lehh;->a(Loo;)V

    .line 434
    invoke-virtual {p0}, Lkul;->aI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 437
    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 17

    .prologue
    .line 836
    invoke-virtual/range {p0 .. p1}, Lkul;->i(Z)V

    .line 838
    const-string v2, "HostedSquareStreamFrag"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 839
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "fetchContent - newer="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 841
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lkul;->aq()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 871
    :cond_1
    :goto_0
    return-void

    .line 845
    :cond_2
    if-nez p1, :cond_3

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkul;->aq:Z

    if-nez v2, :cond_1

    .line 849
    :cond_3
    if-eqz p1, :cond_6

    .line 850
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lkul;->Y:Ljava/lang/String;

    .line 855
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lkul;->az()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 856
    move-object/from16 v0, p0

    iget-object v2, v0, Lkul;->U:Licq;

    const v3, 0x7f0a057b

    invoke-virtual {v2, v3}, Licq;->a(I)Licq;

    .line 857
    move-object/from16 v0, p0

    iget-object v2, v0, Lkul;->U:Licq;

    sget-object v3, Lict;->a:Lict;

    invoke-virtual {v2, v3}, Licq;->a(Lict;)V

    .line 860
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lkul;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lkul;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lkul;->N:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lkul;->O:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lkul;->Y:Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lkul;->W:Lfdj;

    if-eqz v10, :cond_7

    move-object/from16 v0, p0

    iget-object v10, v0, Lkul;->W:Lfdj;

    .line 863
    invoke-virtual {v10}, Lfdj;->aj()[Ljava/lang/String;

    move-result-object v10

    :goto_1
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lkul;->Z:J

    const/16 v16, 0x0

    .line 860
    invoke-static/range {v2 .. v16}, Ldov;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Ljava/lang/String;[Ljava/lang/String;ZZJ[Ljava/lang/String;)Ldov;

    move-result-object v3

    .line 867
    if-eqz p1, :cond_8

    const-string v2, "fetch_newer"

    :goto_2
    invoke-virtual {v3, v2}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 868
    move-object/from16 v0, p0

    iget-object v2, v0, Lkul;->au:Llnh;

    const-class v4, Lhoc;

    invoke-virtual {v2, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhoc;

    invoke-virtual {v2, v3}, Lhoc;->b(Lhny;)V

    .line 870
    invoke-virtual/range {p0 .. p0}, Lkul;->ay()V

    goto :goto_0

    .line 851
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lkul;->Y:Ljava/lang/String;

    if-nez v2, :cond_4

    goto :goto_0

    .line 863
    :cond_7
    const/4 v10, 0x0

    goto :goto_1

    .line 867
    :cond_8
    const-string v2, "fetch_older"

    goto :goto_2
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v6, 0x4

    .line 527
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 528
    const v2, 0x7f10068b

    if-ne v1, v2, :cond_0

    .line 529
    invoke-virtual {p0}, Lkul;->aa()V

    .line 530
    iget-object v1, p0, Lkul;->at:Llnl;

    new-instance v2, Lhml;

    invoke-direct {v2}, Lhml;-><init>()V

    new-instance v3, Lhmk;

    sget-object v4, Lomv;->Q:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    .line 531
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    iget-object v3, p0, Lkul;->at:Llnl;

    .line 532
    invoke-virtual {v2, v3}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v2

    .line 530
    invoke-static {v1, v6, v2}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 607
    :goto_0
    return v0

    .line 534
    :cond_0
    const v2, 0x7f10068c

    if-ne v1, v2, :cond_1

    .line 535
    invoke-virtual {p0}, Lkul;->ad()V

    .line 536
    iget-object v1, p0, Lkul;->at:Llnl;

    new-instance v2, Lhml;

    invoke-direct {v2}, Lhml;-><init>()V

    new-instance v3, Lhmk;

    sget-object v4, Lomv;->ab:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    .line 537
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    iget-object v3, p0, Lkul;->at:Llnl;

    .line 538
    invoke-virtual {v2, v3}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v2

    .line 536
    invoke-static {v1, v6, v2}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto :goto_0

    .line 540
    :cond_1
    const v2, 0x7f1006d8

    if-ne v1, v2, :cond_3

    .line 541
    invoke-virtual {p0}, Lkul;->aa()V

    .line 542
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->Q:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 543
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 544
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 542
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 607
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lehh;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 545
    :cond_3
    const v2, 0x7f1006d9

    if-ne v1, v2, :cond_4

    .line 546
    invoke-virtual {p0}, Lkul;->ad()V

    .line 547
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->ab:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 548
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 549
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 547
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto :goto_1

    .line 550
    :cond_4
    const v2, 0x7f1006d4

    if-ne v1, v2, :cond_5

    .line 551
    invoke-virtual {p0, v0}, Lkul;->d(I)V

    .line 552
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->S:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 553
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 554
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 552
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto :goto_1

    .line 555
    :cond_5
    const v2, 0x7f1006d6

    if-ne v1, v2, :cond_6

    .line 556
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lkul;->d(I)V

    .line 557
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->b:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 558
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 559
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 557
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto :goto_1

    .line 560
    :cond_6
    const v2, 0x7f1006d7

    if-ne v1, v2, :cond_7

    .line 561
    invoke-virtual {p0, v6}, Lkul;->d(I)V

    .line 562
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->i:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 563
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 564
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 562
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1

    .line 565
    :cond_7
    const v2, 0x7f1006d5

    if-ne v1, v2, :cond_8

    .line 566
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lkul;->d(I)V

    .line 567
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->f:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 568
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 569
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 567
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1

    .line 570
    :cond_8
    const v2, 0x7f1006da

    if-ne v1, v2, :cond_9

    .line 571
    invoke-virtual {p0}, Lkul;->af()V

    .line 572
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->G:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 573
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 574
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 572
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1

    .line 575
    :cond_9
    const v2, 0x7f1006db

    if-ne v1, v2, :cond_a

    .line 576
    invoke-direct {p0, v0}, Lkul;->l(Z)V

    .line 577
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->ac:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 578
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 579
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 577
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1

    .line 580
    :cond_a
    const v0, 0x7f1006dc

    if-ne v1, v0, :cond_b

    .line 581
    invoke-direct {p0, v4}, Lkul;->l(Z)V

    .line 582
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->af:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 583
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 584
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 582
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1

    .line 585
    :cond_b
    const v0, 0x7f1006dd

    if-ne v1, v0, :cond_c

    .line 586
    invoke-virtual {p0}, Lkul;->ae()V

    .line 587
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->A:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 588
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 589
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 587
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1

    .line 590
    :cond_c
    const v0, 0x7f1006de

    if-ne v1, v0, :cond_d

    .line 591
    iget-object v0, p0, Lkul;->aw:Lkvy;

    iget-object v1, p0, Lkul;->N:Ljava/lang/String;

    iget-object v2, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v2}, Lkuo;->q()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lkvy;->a(Ljava/lang/String;I)V

    .line 592
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->T:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 593
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 594
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 592
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1

    .line 595
    :cond_d
    const v0, 0x7f1006e0

    if-ne v1, v0, :cond_f

    .line 596
    iget-object v0, p0, Lkul;->aM:Ljava/lang/String;

    if-eqz v0, :cond_e

    const-string v0, "extra_notification_id"

    iget-object v1, p0, Lkul;->aM:Ljava/lang/String;

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lkul;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->ab:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    const v0, 0x7f0a0550

    invoke-virtual {p0, v0}, Lkul;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0552

    invoke-virtual {p0, v1}, Lkul;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0596

    invoke-virtual {p0, v2}, Lkul;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Lkul;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0, p0, v4}, Llgr;->a(Lu;I)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "com.google.android.libraries.social.notifications.notif_id"

    iget-object v3, p0, Lkul;->aM:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.google.android.libraries.social.notifications.updated_version"

    iget-wide v4, p0, Lkul;->aN:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {p0}, Lkul;->p()Lae;

    move-result-object v1

    const-string v2, "report_invite_abuse"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 597
    :goto_2
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->aa:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 598
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 599
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 597
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1

    .line 596
    :cond_e
    iget-object v0, p0, Lkul;->ax:Lkss;

    invoke-virtual {v0}, Lkss;->a()V

    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lkul;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ab:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_2

    .line 600
    :cond_f
    const v0, 0x7f10005d

    if-ne v1, v0, :cond_2

    .line 601
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lktz;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktz;

    iget-object v1, p0, Lkul;->R:Lhee;

    .line 602
    invoke-interface {v1}, Lhee;->d()I

    invoke-virtual {p0}, Lkul;->d()Ljava/lang/String;

    invoke-interface {v0}, Lktz;->a()Landroid/content/Intent;

    move-result-object v0

    .line 601
    invoke-virtual {p0, v0}, Lkul;->a(Landroid/content/Intent;)V

    .line 603
    iget-object v0, p0, Lkul;->at:Llnl;

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->N:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 604
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkul;->at:Llnl;

    .line 605
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v1

    .line 603
    invoke-static {v0, v6, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_1
.end method

.method protected aG()I
    .locals 6

    .prologue
    .line 1126
    iget-object v0, p0, Lkul;->at:Llnl;

    iget-object v1, p0, Lkul;->R:Lhee;

    .line 1127
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x0

    iget-object v4, p0, Lkul;->N:Ljava/lang/String;

    iget-object v5, p0, Lkul;->O:Ljava/lang/String;

    .line 1126
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 312
    invoke-super {p0}, Lehh;->aO_()V

    .line 314
    iget-boolean v0, p0, Lkul;->aF:Z

    if-eqz v0, :cond_0

    .line 315
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkul;->aF:Z

    .line 317
    invoke-virtual {p0}, Lkul;->al()V

    .line 319
    :cond_0
    return-void
.end method

.method public aa()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 944
    sget-object v0, Lhmv;->cx:Lhmv;

    invoke-direct {p0, v0}, Lkul;->a(Lhmv;)V

    .line 945
    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 946
    iget-object v1, p0, Lkul;->at:Llnl;

    invoke-static {v1, v0}, Leyq;->k(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 947
    const-string v1, "square_embed"

    invoke-direct {p0, v3}, Lkul;->m(Z)Lkzz;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 948
    const-string v1, "disable_location"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 951
    const-string v1, "circle_usage_type"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 952
    const-string v1, "category_display_mode"

    const/16 v2, 0xce7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 954
    const-string v1, "clear_acl"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 956
    const-string v1, "filter_null_gaia_ids"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 957
    invoke-virtual {p0, v0}, Lkul;->a(Landroid/content/Intent;)V

    .line 958
    return-void
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 1160
    new-instance v0, Lkqw;

    sget-object v1, Lomv;->D:Lhmn;

    iget-object v2, p0, Lkul;->N:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method public ad()V
    .locals 3

    .prologue
    .line 962
    sget-object v0, Lhmv;->cy:Lhmv;

    invoke-direct {p0, v0}, Lkul;->a(Lhmv;)V

    .line 963
    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 964
    iget-object v1, p0, Lkul;->at:Llnl;

    invoke-static {v1, v0}, Leyq;->k(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 965
    const-string v1, "square_embed"

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lkul;->m(Z)Lkzz;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 966
    const-string v1, "disable_location"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 967
    invoke-virtual {p0, v0}, Lkul;->a(Landroid/content/Intent;)V

    .line 968
    return-void
.end method

.method protected ad_()Z
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    return v0
.end method

.method public ae()V
    .locals 5

    .prologue
    .line 972
    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 973
    iget-object v1, p0, Lkul;->at:Llnl;

    iget-object v2, p0, Lkul;->N:Ljava/lang/String;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/squares/legacyimpl/SquareSettingsActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "square_id"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 975
    const/4 v0, 0x4

    invoke-virtual {p0, v3, v0}, Lkul;->a(Landroid/content/Intent;I)V

    .line 976
    return-void
.end method

.method public af()V
    .locals 4

    .prologue
    .line 980
    const/4 v0, 0x0

    const v1, 0x7f0a045b

    .line 982
    invoke-virtual {p0, v1}, Lkul;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a045c

    .line 983
    invoke-virtual {p0, v2}, Lkul;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Lkul;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 980
    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 984
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    .line 985
    invoke-virtual {p0}, Lkul;->p()Lae;

    move-result-object v1

    const-string v2, "decline_invitation"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 986
    return-void
.end method

.method protected af_()V
    .locals 3

    .prologue
    .line 831
    iget-object v0, p0, Lkul;->N:Ljava/lang/String;

    iget-object v1, p0, Lkul;->O:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkul;->ag:Ljava/lang/String;

    .line 832
    return-void
.end method

.method public ag()V
    .locals 6

    .prologue
    .line 990
    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 991
    iget-object v0, p0, Lkul;->at:Llnl;

    const-class v2, Lkvh;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvh;

    iget-object v2, p0, Lkul;->N:Ljava/lang/String;

    iget-object v3, p0, Lkul;->aE:Lkuo;

    .line 992
    invoke-virtual {v3}, Lkuo;->j()I

    move-result v3

    iget-object v4, p0, Lkul;->aE:Lkuo;

    .line 993
    invoke-virtual {v4}, Lkuo;->r()I

    move-result v4

    const/4 v5, 0x0

    .line 991
    invoke-interface/range {v0 .. v5}, Lkvh;->a(ILjava/lang/String;IILjava/lang/Integer;)Landroid/content/Intent;

    move-result-object v0

    .line 994
    invoke-virtual {p0, v0}, Lkul;->a(Landroid/content/Intent;)V

    .line 995
    return-void
.end method

.method public aj()Z
    .locals 2

    .prologue
    .line 647
    invoke-super {p0}, Lehh;->aj()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lkul;->aG:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lhoc;

    .line 649
    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0}, Lhoc;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ak()Lhym;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lkul;->aO:Lhym;

    return-object v0
.end method

.method public al()V
    .locals 0

    .prologue
    .line 654
    invoke-super {p0}, Lehh;->al()V

    .line 655
    invoke-virtual {p0}, Lkul;->X()V

    .line 656
    return-void
.end method

.method protected aq()Z
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lkul;->aK:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkul;->aK:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ar()Z
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x1

    return v0
.end method

.method protected ax()Lhny;
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 875
    iget-object v1, p0, Lkul;->at:Llnl;

    iget-object v0, p0, Lkul;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x4

    iget-object v5, p0, Lkul;->N:Ljava/lang/String;

    iget-object v6, p0, Lkul;->O:Ljava/lang/String;

    iget-object v0, p0, Lkul;->W:Lfdj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkul;->W:Lfdj;

    .line 877
    invoke-virtual {v0}, Lfdj;->aj()[Ljava/lang/String;

    move-result-object v7

    .line 878
    :goto_0
    invoke-virtual {p0}, Lkul;->aL()Z

    move-result v9

    iget-wide v10, p0, Lkul;->Z:J

    .line 879
    invoke-virtual {p0}, Lkul;->aw()[Ljava/lang/String;

    move-result-object v12

    move-object v8, v4

    .line 875
    invoke-static/range {v1 .. v12}, Ldov;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZJ[Ljava/lang/String;)Ldov;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v7, v4

    .line 877
    goto :goto_0
.end method

.method protected az()Z
    .locals 1

    .prologue
    .line 1056
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lehh;->az()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1066
    invoke-super {p0, p1}, Lehh;->b(Landroid/os/Bundle;)V

    .line 1067
    const-string v0, "extra_square_id"

    iget-object v1, p0, Lkul;->N:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1110
    return-void
.end method

.method public b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 769
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lkul;->aJ:Ljava/lang/Boolean;

    .line 770
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkuo;->b(Ljava/lang/Boolean;)V

    .line 773
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0, v2, v2}, Lkuo;->a(ZI)V

    .line 775
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1170
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->j()I

    move-result v0

    return v0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 779
    iget v0, p0, Lkul;->aD:I

    if-ne v0, p1, :cond_0

    .line 787
    :goto_0
    return-void

    .line 782
    :cond_0
    iput p1, p0, Lkul;->aD:I

    .line 783
    iget-object v0, p0, Lkul;->aE:Lkuo;

    iget v1, p0, Lkul;->aD:I

    invoke-virtual {v0, v1}, Lkuo;->b(I)V

    .line 784
    iget-object v0, p0, Lkul;->aC:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkyf;

    .line 785
    invoke-virtual {v0}, Lkyf;->b()Ljava/lang/String;

    move-result-object v1

    .line 786
    invoke-virtual {v0}, Lkyf;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lkul;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 183
    invoke-super {p0, p1}, Lehh;->c(Landroid/os/Bundle;)V

    .line 184
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lhec;

    iget-object v2, p0, Lkul;->ax:Lkss;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 185
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Lkvx;

    iget-object v2, p0, Lkul;->aw:Lkvy;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 186
    iget-object v0, p0, Lkul;->au:Llnh;

    const-class v1, Liax;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Lkul;->aQ:Liax;

    .line 187
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1114
    return-void
.end method

.method public c(Z)V
    .locals 3

    .prologue
    .line 999
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lkul;->aI:Ljava/lang/Boolean;

    .line 1000
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkuo;->a(Ljava/lang/Boolean;)V

    .line 1003
    iget-object v0, p0, Lkul;->aE:Lkuo;

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lkuo;->a(ZI)V

    .line 1005
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1165
    iget-object v0, p0, Lkul;->N:Ljava/lang/String;

    return-object v0
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 911
    iget-object v0, p0, Lkul;->aP:Lkvq;

    iget-object v1, p0, Lkul;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lkvq;->a(Ljava/lang/String;I)V

    .line 912
    return-void
.end method

.method protected d(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 1026
    const/4 v0, 0x0

    .line 1027
    iget-object v1, p0, Lkul;->az:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1031
    iget-object v0, p0, Lkul;->O:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lkul;->O:Ljava/lang/String;

    .line 1032
    :goto_0
    new-instance v6, Lhgw;

    new-instance v0, Lkxr;

    iget-object v1, p0, Lkul;->N:Ljava/lang/String;

    iget-object v2, p0, Lkul;->az:Ljava/lang/String;

    iget-object v4, p0, Lkul;->O:Ljava/lang/String;

    if-nez v4, :cond_2

    const-string v4, ""

    :goto_1
    iget-boolean v5, p0, Lkul;->aB:Z

    invoke-direct/range {v0 .. v5}, Lkxr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v6, v0}, Lhgw;-><init>(Lkxr;)V

    move-object v0, v6

    .line 1036
    :cond_0
    const-string v1, "extra_acl"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1037
    invoke-super {p0, p1}, Lehh;->d(Landroid/content/Intent;)V

    .line 1038
    return-void

    .line 1031
    :cond_1
    iget-object v3, p0, Lkul;->ay:Ljava/lang/String;

    goto :goto_0

    .line 1032
    :cond_2
    iget-object v4, p0, Lkul;->aA:Ljava/lang/String;

    goto :goto_1
.end method

.method protected e(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1042
    iget-object v0, p0, Lkul;->aE:Lkuo;

    invoke-virtual {v0}, Lkuo;->j()I

    move-result v0

    .line 1043
    const-string v1, "square_membership"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1044
    const-string v0, "square_joinability"

    iget-object v1, p0, Lkul;->aE:Lkuo;

    .line 1045
    invoke-virtual {v1}, Lkuo;->r()I

    move-result v1

    .line 1044
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1048
    const-string v0, "refresh"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1049
    invoke-super {p0, p1}, Lehh;->e(Landroid/content/Intent;)V

    .line 1050
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 294
    invoke-super {p0, p1}, Lehh;->e(Landroid/os/Bundle;)V

    .line 295
    iget-object v0, p0, Lkul;->az:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "square_name"

    iget-object v1, p0, Lkul;->az:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_0
    iget-object v0, p0, Lkul;->aA:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 299
    const-string v0, "square_stream_name"

    iget-object v1, p0, Lkul;->aA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_1
    iget-object v0, p0, Lkul;->aI:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 302
    const-string v0, "square_expanded"

    iget-object v1, p0, Lkul;->aI:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 304
    :cond_2
    iget-object v0, p0, Lkul;->aJ:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 305
    const-string v0, "square_category_expanded"

    iget-object v1, p0, Lkul;->aJ:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 307
    :cond_3
    const-string v0, "square_is_restricted"

    iget-boolean v1, p0, Lkul;->aB:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 308
    return-void
.end method
