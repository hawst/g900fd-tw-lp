.class public Lkqt;
.super Lhmk;
.source "PG"


# instance fields
.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(Lhmn;II)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lhmk;-><init>(Lhmn;)V

    .line 24
    iput p2, p0, Lkqt;->b:I

    .line 25
    iput p3, p0, Lkqt;->c:I

    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-super {p0, p1}, Lhmk;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    check-cast p1, Lkqt;

    .line 32
    iget v1, p0, Lkqt;->b:I

    iget v2, p1, Lkqt;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lkqt;->c:I

    iget v2, p1, Lkqt;->c:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 35
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lkqt;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lkqt;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-super {p0}, Lhmk;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
