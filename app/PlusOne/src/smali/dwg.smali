.class public final Ldwg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lepm;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lbb;

.field private c:I

.field private d:Lept;

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lept;Lbb;III)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p4, p0, Ldwg;->c:I

    .line 55
    iput-object p2, p0, Ldwg;->d:Lept;

    .line 56
    iput-object p1, p0, Ldwg;->a:Landroid/content/Context;

    .line 57
    iput-object p3, p0, Ldwg;->b:Lbb;

    .line 58
    iput p5, p0, Ldwg;->e:I

    .line 59
    iput p6, p0, Ldwg;->f:I

    .line 60
    return-void
.end method

.method public static a(Landroid/content/Context;ILept;Legi;)[Lepm;
    .locals 3

    .prologue
    .line 121
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 122
    new-instance v0, Ldxr;

    invoke-direct {v0, p0, p1, p2, p3}, Ldxr;-><init>(Landroid/content/Context;ILept;Lu;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v0, Ldyd;

    invoke-direct {v0, p0, p1, p2}, Ldyd;-><init>(Landroid/content/Context;ILept;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    new-instance v0, Lebb;

    invoke-direct {v0, p0, p1, p2}, Lebb;-><init>(Landroid/content/Context;ILept;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v0, Lemt;

    invoke-direct {v0, p0, p1, p2}, Lemt;-><init>(Landroid/content/Context;ILept;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    new-instance v0, Ldxo;

    invoke-direct {v0, p0, p1, p2, p3}, Ldxo;-><init>(Landroid/content/Context;ILept;Legi;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    const-class v0, Lfct;

    invoke-static {p0, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfct;

    .line 129
    invoke-virtual {v0, p0, p1, p2, p3}, Lfct;->a(Landroid/content/Context;ILept;Legi;)Lepm;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lepm;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepm;

    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lepm;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Ldwg;->d:Lept;

    invoke-interface {v0}, Lept;->ad()[Lepm;

    move-result-object v0

    .line 76
    new-instance v1, Ldwh;

    invoke-direct {v1}, Ldwh;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 84
    new-instance v1, Ldwf;

    iget-object v2, p0, Ldwg;->a:Landroid/content/Context;

    iget v3, p0, Ldwg;->e:I

    iget v4, p0, Ldwg;->f:I

    invoke-direct {v1, v2, v0, v3, v4}, Ldwf;-><init>(Landroid/content/Context;[Lepm;II)V

    return-object v1
.end method

.method public a()V
    .locals 3

    .prologue
    .line 69
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 70
    iget-object v1, p0, Ldwg;->b:Lbb;

    iget v2, p0, Ldwg;->c:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 71
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lepm;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 109
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p2, Ljava/util/ArrayList;

    invoke-virtual {p0, p2}, Ldwg;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lepm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    const/4 v1, 0x0

    .line 92
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepm;

    .line 93
    invoke-interface {v0}, Lepm;->j()Landroid/view/View;

    move-result-object v3

    .line 94
    if-eqz v3, :cond_1

    .line 103
    :goto_1
    iget-object v1, p0, Ldwg;->d:Lept;

    invoke-interface {v1, v0}, Lept;->a(Lepm;)V

    .line 104
    return-void

    .line 98
    :cond_1
    const-string v3, "PromoManager"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "skipped promo without View: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
