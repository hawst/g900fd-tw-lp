.class public final Lkdr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/resources/images/ImageResource;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/resources/images/ImageResource;)V
    .locals 0

    .prologue
    .line 791
    iput-object p1, p0, Lkdr;->a:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 795
    iget-object v0, p0, Lkdr;->a:Lcom/google/android/libraries/social/resources/images/ImageResource;

    iget-object v1, p0, Lkdr;->a:Lcom/google/android/libraries/social/resources/images/ImageResource;

    # getter for: Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;
    invoke-static {v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->access$000(Lcom/google/android/libraries/social/resources/images/ImageResource;)Lkdc;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Out of memory while decoding image: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 796
    iget-object v0, p0, Lkdr;->a:Lcom/google/android/libraries/social/resources/images/ImageResource;

    iget-object v0, v0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->q()V

    .line 797
    return-void
.end method
