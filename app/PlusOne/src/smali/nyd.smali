.class public final Lnyd;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5977
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6023
    const/4 v0, 0x0

    .line 6024
    iget-object v1, p0, Lnyd;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 6025
    const/4 v0, 0x1

    iget-object v1, p0, Lnyd;->a:Ljava/lang/Boolean;

    .line 6026
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 6028
    :cond_0
    iget-object v1, p0, Lnyd;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 6029
    const/4 v1, 0x2

    iget-object v2, p0, Lnyd;->b:Ljava/lang/Boolean;

    .line 6030
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6032
    :cond_1
    iget-object v1, p0, Lnyd;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 6033
    const/4 v1, 0x3

    iget-object v2, p0, Lnyd;->c:Ljava/lang/Boolean;

    .line 6034
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6036
    :cond_2
    iget-object v1, p0, Lnyd;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 6037
    const/4 v1, 0x4

    iget-object v2, p0, Lnyd;->d:Ljava/lang/Boolean;

    .line 6038
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6040
    :cond_3
    iget-object v1, p0, Lnyd;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 6041
    const/4 v1, 0x5

    iget-object v2, p0, Lnyd;->e:Ljava/lang/Boolean;

    .line 6042
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6044
    :cond_4
    iget-object v1, p0, Lnyd;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 6045
    const/4 v1, 0x6

    iget-object v2, p0, Lnyd;->f:Ljava/lang/Boolean;

    .line 6046
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6048
    :cond_5
    iget-object v1, p0, Lnyd;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 6049
    const/4 v1, 0x7

    iget-object v2, p0, Lnyd;->g:Ljava/lang/Boolean;

    .line 6050
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6052
    :cond_6
    iget-object v1, p0, Lnyd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6053
    iput v0, p0, Lnyd;->ai:I

    .line 6054
    return v0
.end method

.method public a(Loxn;)Lnyd;
    .locals 2

    .prologue
    .line 6062
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6063
    sparse-switch v0, :sswitch_data_0

    .line 6067
    iget-object v1, p0, Lnyd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6068
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyd;->ah:Ljava/util/List;

    .line 6071
    :cond_1
    iget-object v1, p0, Lnyd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6073
    :sswitch_0
    return-object p0

    .line 6078
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyd;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 6082
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyd;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 6086
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyd;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 6090
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyd;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 6094
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyd;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 6098
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyd;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 6102
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyd;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 6063
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5996
    iget-object v0, p0, Lnyd;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 5997
    const/4 v0, 0x1

    iget-object v1, p0, Lnyd;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5999
    :cond_0
    iget-object v0, p0, Lnyd;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 6000
    const/4 v0, 0x2

    iget-object v1, p0, Lnyd;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6002
    :cond_1
    iget-object v0, p0, Lnyd;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 6003
    const/4 v0, 0x3

    iget-object v1, p0, Lnyd;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6005
    :cond_2
    iget-object v0, p0, Lnyd;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 6006
    const/4 v0, 0x4

    iget-object v1, p0, Lnyd;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6008
    :cond_3
    iget-object v0, p0, Lnyd;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 6009
    const/4 v0, 0x5

    iget-object v1, p0, Lnyd;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6011
    :cond_4
    iget-object v0, p0, Lnyd;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 6012
    const/4 v0, 0x6

    iget-object v1, p0, Lnyd;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6014
    :cond_5
    iget-object v0, p0, Lnyd;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 6015
    const/4 v0, 0x7

    iget-object v1, p0, Lnyd;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6017
    :cond_6
    iget-object v0, p0, Lnyd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6019
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5973
    invoke-virtual {p0, p1}, Lnyd;->a(Loxn;)Lnyd;

    move-result-object v0

    return-object v0
.end method
