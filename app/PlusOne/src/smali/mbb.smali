.class public final Lmbb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmxv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29968
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29971
    iput-object v0, p0, Lmbb;->apiHeader:Llyr;

    .line 29974
    iput-object v0, p0, Lmbb;->a:Lmxv;

    .line 29968
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29991
    const/4 v0, 0x0

    .line 29992
    iget-object v1, p0, Lmbb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 29993
    const/4 v0, 0x1

    iget-object v1, p0, Lmbb;->apiHeader:Llyr;

    .line 29994
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29996
    :cond_0
    iget-object v1, p0, Lmbb;->a:Lmxv;

    if-eqz v1, :cond_1

    .line 29997
    const/4 v1, 0x2

    iget-object v2, p0, Lmbb;->a:Lmxv;

    .line 29998
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30000
    :cond_1
    iget-object v1, p0, Lmbb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30001
    iput v0, p0, Lmbb;->ai:I

    .line 30002
    return v0
.end method

.method public a(Loxn;)Lmbb;
    .locals 2

    .prologue
    .line 30010
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30011
    sparse-switch v0, :sswitch_data_0

    .line 30015
    iget-object v1, p0, Lmbb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30016
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbb;->ah:Ljava/util/List;

    .line 30019
    :cond_1
    iget-object v1, p0, Lmbb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30021
    :sswitch_0
    return-object p0

    .line 30026
    :sswitch_1
    iget-object v0, p0, Lmbb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 30027
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbb;->apiHeader:Llyr;

    .line 30029
    :cond_2
    iget-object v0, p0, Lmbb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30033
    :sswitch_2
    iget-object v0, p0, Lmbb;->a:Lmxv;

    if-nez v0, :cond_3

    .line 30034
    new-instance v0, Lmxv;

    invoke-direct {v0}, Lmxv;-><init>()V

    iput-object v0, p0, Lmbb;->a:Lmxv;

    .line 30036
    :cond_3
    iget-object v0, p0, Lmbb;->a:Lmxv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30011
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29979
    iget-object v0, p0, Lmbb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 29980
    const/4 v0, 0x1

    iget-object v1, p0, Lmbb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29982
    :cond_0
    iget-object v0, p0, Lmbb;->a:Lmxv;

    if-eqz v0, :cond_1

    .line 29983
    const/4 v0, 0x2

    iget-object v1, p0, Lmbb;->a:Lmxv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29985
    :cond_1
    iget-object v0, p0, Lmbb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29987
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29964
    invoke-virtual {p0, p1}, Lmbb;->a(Loxn;)Lmbb;

    move-result-object v0

    return-object v0
.end method
