.class public final Llsl;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/lang/Double;)D
    .locals 2

    .prologue
    .line 67
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Float;F)F
    .locals 0

    .prologue
    .line 87
    if-nez p0, :cond_0

    :goto_0
    return p1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    move-result p1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Integer;)I
    .locals 1

    .prologue
    .line 43
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Long;)J
    .locals 2

    .prologue
    .line 51
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 59
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Boolean;)Z
    .locals 1

    .prologue
    .line 15
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Integer;Ljava/lang/Integer;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 91
    if-eqz p0, :cond_2

    .line 92
    if-eqz p1, :cond_1

    invoke-static {p0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    invoke-static {p1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 92
    goto :goto_0

    .line 94
    :cond_2
    if-eqz p1, :cond_0

    move v0, v1

    goto :goto_0
.end method
