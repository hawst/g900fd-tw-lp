.class public Ldqm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljuf;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ldqm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:J

.field private final b:Lizu;

.field private final c:Ljava/lang/String;

.field private final d:Lnzi;

.field private final e:J

.field private final f:Ljcj;

.field private final g:Ljcm;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    new-instance v0, Ldqn;

    invoke-direct {v0}, Ldqn;-><init>()V

    sput-object v0, Ldqm;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLizu;Ljava/lang/String;Lnzi;J)V
    .locals 4

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-wide p1, p0, Ldqm;->a:J

    .line 34
    iput-object p3, p0, Ldqm;->b:Lizu;

    .line 35
    iput-object p4, p0, Ldqm;->c:Ljava/lang/String;

    .line 36
    iput-object p5, p0, Ldqm;->d:Lnzi;

    .line 37
    iput-wide p6, p0, Ldqm;->e:J

    .line 39
    new-instance v0, Ldqp;

    iget-wide v2, p0, Ldqm;->a:J

    invoke-direct {v0, v2, v3}, Ldqp;-><init>(J)V

    iput-object v0, p0, Ldqm;->g:Ljcm;

    .line 40
    new-instance v0, Ldqo;

    invoke-direct {v0}, Ldqo;-><init>()V

    iput-object v0, p0, Ldqm;->f:Ljcj;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Ldqm;->a:J

    .line 124
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    iput-object v0, p0, Ldqm;->b:Lizu;

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldqm;->c:Ljava/lang/String;

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Ldqm;->e:J

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 129
    const/4 v1, 0x0

    .line 130
    if-eqz v0, :cond_0

    .line 132
    :try_start_0
    new-instance v2, Lnzi;

    invoke-direct {v2}, Lnzi;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzi;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    iput-object v0, p0, Ldqm;->d:Lnzi;

    .line 139
    new-instance v0, Ldqp;

    iget-wide v2, p0, Ldqm;->a:J

    invoke-direct {v0, v2, v3}, Ldqp;-><init>(J)V

    iput-object v0, p0, Ldqm;->g:Ljcm;

    .line 140
    new-instance v0, Ldqo;

    invoke-direct {v0}, Ldqo;-><init>()V

    iput-object v0, p0, Ldqm;->f:Ljcj;

    .line 141
    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 134
    const-string v2, "AllPhotosSelectable"

    const-string v3, "Failed to deserialize EditInfo."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Ldqm;->a:J

    return-wide v0
.end method

.method public a(Lnzi;)Ljuf;
    .locals 8

    .prologue
    .line 118
    new-instance v0, Ldqm;

    iget-wide v1, p0, Ldqm;->a:J

    iget-object v3, p0, Ldqm;->b:Lizu;

    iget-object v4, p0, Ldqm;->c:Ljava/lang/String;

    iget-wide v6, p0, Ldqm;->e:J

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Ldqm;-><init>(JLizu;Ljava/lang/String;Lnzi;J)V

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 76
    iput p1, p0, Ldqm;->h:I

    .line 77
    return-void
.end method

.method public b()Ljcj;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ldqm;->f:Ljcj;

    return-object v0
.end method

.method public c()Ljcm;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldqm;->g:Ljcm;

    return-object v0
.end method

.method public d()J
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 64
    iget-object v0, p0, Ldqm;->b:Lizu;

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x20

    :goto_0
    const-wide/16 v4, 0x2

    or-long/2addr v0, v4

    const-wide/16 v4, 0x4

    or-long/2addr v0, v4

    const-wide/16 v4, 0x40

    or-long/2addr v0, v4

    const-wide/16 v4, 0x80

    or-long/2addr v4, v0

    .line 66
    iget-wide v0, p0, Ldqm;->e:J

    const-wide/16 v6, 0x100

    and-long/2addr v0, v6

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    const-wide/16 v2, 0x200

    :cond_0
    or-long v0, v4, v2

    return-wide v0

    :cond_1
    move-wide v0, v2

    .line 64
    goto :goto_0

    .line 66
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Ldqm;->h:I

    return v0
.end method

.method public f()Lizu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Ldqm;->b:Lizu;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Ldqm;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lnzi;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Ldqm;->d:Lnzi;

    return-object v0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Ldqm;->e:J

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Ldqm;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 146
    iget-object v0, p0, Ldqm;->b:Lizu;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 147
    iget-object v0, p0, Ldqm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    iget-wide v0, p0, Ldqm;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 150
    iget-object v0, p0, Ldqm;->d:Lnzi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqm;->d:Lnzi;

    .line 151
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 152
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 153
    return-void

    .line 151
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
