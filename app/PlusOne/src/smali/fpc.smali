.class public final Lfpc;
.super Lhye;
.source "PG"


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:I

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2115
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "activity_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "plus_one_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "total_comment_count"

    aput-object v2, v0, v1

    sput-object v0, Lfpc;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2130
    sget-object v0, Llbi;->a:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2131
    iput p2, p0, Lfpc;->c:I

    .line 2132
    iput-object p3, p0, Lfpc;->d:Ljava/lang/String;

    .line 2133
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2137
    invoke-virtual {p0}, Lfpc;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfpc;->c:I

    invoke-static {v0, v1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 2138
    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2139
    const-string v1, "activities"

    sget-object v2, Lfpc;->b:[Ljava/lang/String;

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lfpc;->d:Ljava/lang/String;

    aput-object v7, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2142
    return-object v0
.end method
