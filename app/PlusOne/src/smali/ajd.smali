.class public Lajd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahx;


# static fields
.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbmz;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Lajl;

.field private final e:Lajv;

.field private final f:Lbks;

.field private final g:Lbit;

.field private final h:Ljfb;

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-class v0, Lajd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sget-object v1, Lbmz;->d:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lbmz;->g:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lbmz;->o:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lbmz;->a:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lbmz;->b:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lbmz;->k:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lbmz;->u:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lbmz;->v:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lbmz;->w:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lajd;->c:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lbks;Lajl;Lajv;Lbit;Ljfb;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lajd;->f:Lbks;

    .line 71
    const-string v0, "cache"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajl;

    iput-object v0, p0, Lajd;->d:Lajl;

    .line 72
    const-string v0, "analyzer"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajv;

    iput-object v0, p0, Lajd;->e:Lajv;

    .line 73
    const-string v0, "videoIdentifierGenerator"

    .line 74
    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbit;

    iput-object v0, p0, Lajd;->g:Lbit;

    .line 75
    const-string v0, "movieMakerProvider"

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Lajd;->h:Ljfb;

    .line 76
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 77
    return-void
.end method

.method static synthetic a(Lajd;)Lajl;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lajd;->d:Lajl;

    return-object v0
.end method

.method static synthetic a(Lajd;Lbkr;)V
    .locals 6

    .prologue
    .line 28
    invoke-interface {p1}, Lbkr;->d()Lbku;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lbmz;->values()[Lbmz;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    sget-object v5, Lajd;->c:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v1, v4}, Lbku;->c(Lbmz;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Lbkr;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 156
    if-nez p0, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v0

    .line 160
    :cond_1
    invoke-interface {p0}, Lbkr;->c()Lbkn;

    move-result-object v1

    .line 161
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lbkn;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lbkr;[B)Z
    .locals 1

    .prologue
    .line 169
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lbkr;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 170
    :cond_0
    const/4 v0, 0x0

    .line 177
    :goto_0
    return v0

    .line 172
    :cond_1
    invoke-interface {p0}, Lbkr;->b()[B

    move-result-object v0

    .line 177
    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method private static final c(Landroid/net/Uri;Laic;Lahy;)V
    .locals 1

    .prologue
    .line 240
    new-instance v0, Lbkm;

    invoke-direct {v0}, Lbkm;-><init>()V

    invoke-virtual {v0}, Lbkm;->c()Lbkr;

    move-result-object v0

    invoke-interface {p2, p0, p1, v0}, Lahy;->b(Landroid/net/Uri;Laic;Lbkr;)V

    .line 241
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 222
    iget-object v0, p0, Lajd;->e:Lajv;

    invoke-interface {v0}, Lajv;->a()V

    .line 223
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lahy;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 97
    iget-object v2, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 98
    iget-object v2, p0, Lajd;->g:Lbit;

    iget-object v3, p0, Lajd;->h:Ljfb;

    .line 99
    invoke-virtual {v3, p1}, Ljfb;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbit;->a(Landroid/net/Uri;)[B

    move-result-object v3

    .line 100
    iget-object v2, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    invoke-static {p1, p2, p3}, Lajd;->c(Landroid/net/Uri;Laic;Lahy;)V

    .line 130
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v2, p0, Lajd;->f:Lbks;

    iget-object v4, p0, Lajd;->d:Lajl;

    invoke-virtual {v2, p1, v4}, Lbks;->a(Landroid/net/Uri;Lajl;)Lbkr;

    move-result-object v4

    .line 105
    invoke-static {v4, v3}, Lajd;->a(Lbkr;[B)Z

    move-result v2

    if-nez v2, :cond_2

    .line 107
    iget-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    invoke-static {p1, p2, p3}, Lajd;->c(Landroid/net/Uri;Laic;Lahy;)V

    goto :goto_0

    .line 111
    :cond_1
    iget-object v0, p0, Lajd;->e:Lajv;

    new-instance v2, Laje;

    invoke-direct {v2, p0, p3, v3}, Laje;-><init>(Lajd;Lahy;[B)V

    invoke-interface {v0, p1, p2, v2}, Lajv;->a(Landroid/net/Uri;Laic;Lahy;)V

    .line 129
    :goto_1
    iget-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 112
    :cond_2
    invoke-static {v4}, Lajd;->a(Lbkr;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 114
    invoke-interface {p3, p1, p2, v4}, Lahy;->a(Landroid/net/Uri;Laic;Lbkr;)V

    goto :goto_1

    .line 115
    :cond_3
    if-eqz v4, :cond_6

    invoke-interface {v4}, Lbkr;->d()Lbku;

    move-result-object v2

    if-eqz v2, :cond_6

    sget-object v5, Lbmz;->q:Lbmz;

    invoke-virtual {v2, v5}, Lbku;->a(Lbmz;)Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v5, Lbmz;->r:Lbmz;

    invoke-virtual {v2, v5}, Lbku;->a(Lbmz;)Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v5, Lbmz;->b:Lbmz;

    invoke-virtual {v2, v5}, Lbku;->a(Lbmz;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v2, v0

    :goto_2
    if-ne v2, v0, :cond_6

    :goto_3
    if-eqz v0, :cond_8

    .line 116
    iget-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 117
    invoke-static {p1, p2, p3}, Lajd;->c(Landroid/net/Uri;Laic;Lahy;)V

    goto :goto_0

    :cond_5
    move v2, v1

    .line 115
    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_3

    .line 120
    :cond_7
    iget-object v0, p0, Lajd;->e:Lajv;

    new-instance v2, Laje;

    invoke-direct {v2, p0, p3, v3}, Laje;-><init>(Lajd;Lahy;[B)V

    invoke-interface {v0, p1, p2, v4, v2}, Lajv;->a(Landroid/net/Uri;Laic;Lbkr;Lahy;)V

    goto :goto_1

    .line 123
    :cond_8
    iget-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 124
    invoke-static {p1, p2, p3}, Lajd;->c(Landroid/net/Uri;Laic;Lahy;)V

    goto :goto_0

    .line 127
    :cond_9
    iget-object v0, p0, Lajd;->e:Lajv;

    new-instance v2, Laje;

    invoke-direct {v2, p0, p3, v3}, Laje;-><init>(Lajd;Lahy;[B)V

    invoke-interface {v0, p1, p2, v2}, Lajv;->a(Landroid/net/Uri;Laic;Lahy;)V

    goto :goto_1
.end method

.method public b(Landroid/net/Uri;Laic;Lahy;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 134
    iget-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 135
    iget-object v0, p0, Lajd;->f:Lbks;

    iget-object v1, p0, Lajd;->d:Lajl;

    invoke-virtual {v0, p1, v1}, Lbks;->a(Landroid/net/Uri;Lajl;)Lbkr;

    move-result-object v0

    .line 136
    invoke-static {v0}, Lajd;->a(Lbkr;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    invoke-interface {p3, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Lbkr;)V

    .line 146
    :goto_0
    iget-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 147
    :goto_1
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lajd;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    invoke-static {p1, p2, p3}, Lajd;->c(Landroid/net/Uri;Laic;Lahy;)V

    goto :goto_1

    .line 144
    :cond_1
    iget-object v0, p0, Lajd;->e:Lajv;

    new-instance v1, Laje;

    invoke-direct {v1, p0, p3}, Laje;-><init>(Lajd;Lahy;)V

    invoke-interface {v0, p1, p2, v1}, Lajv;->b(Landroid/net/Uri;Laic;Lahy;)V

    goto :goto_0
.end method
