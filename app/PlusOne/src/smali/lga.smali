.class public final Llga;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmkg;",
        "Lmkh;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Lkzl;

.field private final p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 42
    const-string v3, "topicstream"

    new-instance v4, Lmkg;

    invoke-direct {v4}, Lmkg;-><init>()V

    new-instance v5, Lmkh;

    invoke-direct {v5}, Lmkh;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 47
    iget-object v0, p0, Llga;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Llga;->c:Lkzl;

    .line 48
    iput p3, p0, Llga;->a:I

    .line 49
    iput-object p4, p0, Llga;->b:Ljava/lang/String;

    .line 50
    iput-object p5, p0, Llga;->p:Ljava/lang/String;

    .line 51
    return-void
.end method


# virtual methods
.method protected a(Lmkg;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 55
    new-instance v1, Lnup;

    invoke-direct {v1}, Lnup;-><init>()V

    .line 57
    new-instance v0, Lnuj;

    invoke-direct {v0}, Lnuj;-><init>()V

    .line 58
    new-instance v2, Loxz;

    invoke-direct {v2}, Loxz;-><init>()V

    iput-object v2, v0, Lnuj;->d:Loxz;

    .line 59
    iget-object v2, v0, Lnuj;->d:Loxz;

    iget-object v3, p0, Llga;->c:Lkzl;

    iget-object v4, p0, Llga;->f:Landroid/content/Context;

    iget v5, p0, Llga;->a:I

    invoke-interface {v3, v4, v5}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v3

    iput-object v3, v2, Loxz;->a:[I

    .line 60
    iget-object v2, v0, Lnuj;->d:Loxz;

    iput v6, v2, Loxz;->b:I

    .line 61
    new-instance v2, Lofl;

    invoke-direct {v2}, Lofl;-><init>()V

    iput-object v2, v0, Lnuj;->c:Lofl;

    .line 62
    iget-object v2, v0, Lnuj;->c:Lofl;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v2, Lofl;->e:Ljava/lang/Boolean;

    .line 63
    iget-object v2, v0, Lnuj;->c:Lofl;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v3, v2, Lofl;->b:Ljava/lang/Boolean;

    .line 64
    iget-object v2, v0, Lnuj;->c:Lofl;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v2, Lofl;->c:Ljava/lang/Boolean;

    .line 65
    iget-object v2, v0, Lnuj;->c:Lofl;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v2, Lofl;->d:Ljava/lang/Boolean;

    .line 66
    iget-object v2, v0, Lnuj;->c:Lofl;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v2, Lofl;->a:Ljava/lang/Boolean;

    .line 67
    iput v6, v0, Lnuj;->e:I

    .line 68
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lnuj;->b:Ljava/lang/Integer;

    .line 69
    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lnuj;->a:Ljava/lang/Integer;

    .line 71
    new-instance v2, Lnuh;

    invoke-direct {v2}, Lnuh;-><init>()V

    .line 72
    new-instance v3, Lnua;

    invoke-direct {v3}, Lnua;-><init>()V

    iput-object v3, v2, Lnuh;->a:Lnua;

    .line 73
    iget-object v3, v2, Lnuh;->a:Lnua;

    iget-object v4, p0, Llga;->f:Landroid/content/Context;

    invoke-static {v4}, Llap;->a(Landroid/content/Context;)[I

    move-result-object v4

    iput-object v4, v3, Lnua;->a:[I

    .line 75
    new-instance v3, Lnub;

    invoke-direct {v3}, Lnub;-><init>()V

    iput-object v3, v2, Lnuh;->b:Lnub;

    .line 76
    iget-object v3, v2, Lnuh;->b:Lnub;

    iput-object v0, v3, Lnub;->a:Lnuj;

    .line 78
    new-instance v0, Lnuk;

    invoke-direct {v0}, Lnuk;-><init>()V

    iput-object v0, v2, Lnuh;->c:Lnuk;

    .line 79
    iget-object v0, v2, Lnuh;->c:Lnuk;

    const/4 v3, 0x1

    iput v3, v0, Lnuk;->a:I

    .line 80
    iget-object v0, v2, Lnuh;->c:Lnuk;

    new-instance v3, Lnuc;

    invoke-direct {v3}, Lnuc;-><init>()V

    iput-object v3, v0, Lnuk;->b:Lnuc;

    .line 81
    iget-object v0, v2, Lnuh;->c:Lnuk;

    iget-object v0, v0, Lnuk;->b:Lnuc;

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lnuc;->a:Ljava/lang/Integer;

    .line 82
    iget-object v0, p0, Llga;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, v2, Lnuh;->c:Lnuk;

    iget-object v0, v0, Lnuk;->b:Lnuc;

    iget-object v3, p0, Llga;->p:Ljava/lang/String;

    iput-object v3, v0, Lnuc;->b:Ljava/lang/String;

    .line 85
    :cond_0
    iput-object v2, v1, Lnup;->a:Lnuh;

    .line 87
    new-instance v2, Logp;

    invoke-direct {v2}, Logp;-><init>()V

    .line 88
    iget-object v0, p0, Llga;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    iput-object v0, v2, Logp;->c:Ljava/lang/String;

    .line 90
    new-instance v0, Lnuo;

    invoke-direct {v0}, Lnuo;-><init>()V

    .line 91
    iput-object v2, v0, Lnuo;->a:Logp;

    .line 92
    iput-object v0, v1, Lnup;->b:Lnuo;

    .line 94
    iput-object v1, p1, Lmkg;->a:Lnup;

    .line 95
    return-void

    .line 88
    :cond_2
    const-string v3, "#"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "#"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lmkg;

    invoke-virtual {p0, p1}, Llga;->a(Lmkg;)V

    return-void
.end method
