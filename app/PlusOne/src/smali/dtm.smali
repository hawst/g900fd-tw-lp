.class final Ldtm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Z

.field private final b:Ldvz;

.field private final c:Ldtg;

.field private final d:Z

.field private final e:I

.field private final f:I

.field private final g:Landroid/content/Context;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILdvz;Ljava/lang/String;ILdtg;Z)V
    .locals 2

    .prologue
    .line 1616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1612
    const/4 v0, 0x0

    iput v0, p0, Ldtm;->j:I

    .line 1617
    iput-object p1, p0, Ldtm;->g:Landroid/content/Context;

    .line 1618
    iput p2, p0, Ldtm;->f:I

    .line 1619
    iput-object p4, p0, Ldtm;->h:Ljava/lang/String;

    .line 1620
    iput-object p3, p0, Ldtm;->b:Ldvz;

    .line 1621
    iput p5, p0, Ldtm;->e:I

    .line 1622
    iput-object p6, p0, Ldtm;->c:Ldtg;

    .line 1623
    iput-boolean p7, p0, Ldtm;->d:Z

    .line 1628
    iget-object v0, p0, Ldtm;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Ldtm;->a:Z

    .line 1630
    iget-object v0, p0, Ldtm;->b:Ldvz;

    if-eqz v0, :cond_0

    .line 1631
    iget-object v0, p0, Ldtm;->b:Ldvz;

    iget v1, p0, Ldtm;->e:I

    invoke-virtual {v0, v1}, Ldvz;->i(I)V

    .line 1633
    :cond_0
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 1686
    iget-object v0, p0, Ldtm;->b:Ldvz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldtm;->b:Ldvz;

    invoke-virtual {v0}, Ldvz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 4

    .prologue
    .line 1690
    iget-object v0, p0, Ldtm;->g:Landroid/content/Context;

    iget v1, p0, Ldtm;->f:I

    invoke-static {v0, v1}, Ljvd;->b(Landroid/content/Context;I)J

    move-result-wide v0

    iget v2, p0, Ldtm;->e:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 1694
    iget-object v0, p0, Ldtm;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldtm;->h:Ljava/lang/String;

    iget-object v1, p0, Ldtm;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 1698
    iget v0, p0, Ldtm;->j:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1636
    iget-boolean v1, p0, Ldtm;->a:Z

    if-eqz v1, :cond_0

    iget v1, p0, Ldtm;->j:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ldth;
    .locals 3

    .prologue
    .line 1640
    iget-object v0, p0, Ldtm;->c:Ldtg;

    iget-object v1, p0, Ldtm;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ldtg;->a(Ljava/lang/String;)Ldth;

    move-result-object v0

    .line 1643
    iget-object v1, p0, Ldtm;->b:Ldvz;

    if-eqz v1, :cond_0

    .line 1644
    iget-object v1, p0, Ldtm;->b:Ldvz;

    invoke-virtual {v0}, Ldth;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ldvz;->h(I)V

    .line 1645
    iget-object v1, p0, Ldtm;->b:Ldvz;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ldvz;->j(I)V

    .line 1647
    :cond_0
    iget-object v1, p0, Ldtm;->h:Ljava/lang/String;

    iput-object v1, p0, Ldtm;->i:Ljava/lang/String;

    .line 1648
    invoke-virtual {v0}, Ldth;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldtm;->h:Ljava/lang/String;

    .line 1649
    iget v1, p0, Ldtm;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ldtm;->j:I

    .line 1651
    return-object v0
.end method

.method public c()Z
    .locals 11

    .prologue
    .line 1656
    const-string v0, "EsTileSync"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1657
    const-string v0, "has_next, hasSyncedAtLeastOnePage="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1658
    invoke-direct {p0}, Ldtm;->i()Z

    move-result v1

    .line 1659
    invoke-direct {p0}, Ldtm;->h()Z

    move-result v2

    iget-object v3, p0, Ldtm;->g:Landroid/content/Context;

    iget v4, p0, Ldtm;->f:I

    .line 1660
    invoke-static {v3, v4}, Ljvd;->b(Landroid/content/Context;I)J

    move-result-wide v4

    iget v3, p0, Ldtm;->e:I

    iget-boolean v6, p0, Ldtm;->d:Z

    .line 1664
    invoke-direct {p0}, Ldtm;->g()Z

    move-result v7

    .line 1665
    invoke-direct {p0}, Ldtm;->f()Z

    move-result v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit16 v10, v10, 0xa7

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", validResumeToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", total fetched so far="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", max can fetch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isInitialSync="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isUnderMetadataLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isCancelled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1657
    :cond_0
    invoke-direct {p0}, Ldtm;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1668
    invoke-direct {p0}, Ldtm;->f()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Ldtm;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Ldtm;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ldtn;
    .locals 6

    .prologue
    .line 1672
    invoke-direct {p0}, Ldtm;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1673
    sget-object v0, Ldtn;->a:Ldtn;

    .line 1677
    :goto_0
    return-object v0

    .line 1674
    :cond_0
    invoke-direct {p0}, Ldtm;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1675
    sget-object v0, Ldtn;->b:Ldtn;

    goto :goto_0

    .line 1676
    :cond_1
    invoke-direct {p0}, Ldtm;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1677
    sget-object v0, Ldtn;->c:Ldtn;

    goto :goto_0

    .line 1679
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown stop reason, valid resume token: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1680
    invoke-direct {p0}, Ldtm;->h()Z

    move-result v2

    .line 1681
    invoke-direct {p0}, Ldtm;->g()Z

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x22

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", under metadata limit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 1702
    iget v0, p0, Ldtm;->j:I

    return v0
.end method
