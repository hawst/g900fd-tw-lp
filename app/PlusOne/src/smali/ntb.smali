.class public final Lntb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1705
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1712
    const/high16 v0, -0x80000000

    iput v0, p0, Lntb;->c:I

    .line 1705
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1732
    const/4 v0, 0x0

    .line 1733
    iget-object v1, p0, Lntb;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1734
    const/4 v0, 0x1

    iget-object v1, p0, Lntb;->a:Ljava/lang/Boolean;

    .line 1735
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1737
    :cond_0
    iget-object v1, p0, Lntb;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1738
    const/4 v1, 0x2

    iget-object v2, p0, Lntb;->b:Ljava/lang/Boolean;

    .line 1739
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1741
    :cond_1
    iget v1, p0, Lntb;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 1742
    const/4 v1, 0x3

    iget v2, p0, Lntb;->c:I

    .line 1743
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1745
    :cond_2
    iget-object v1, p0, Lntb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1746
    iput v0, p0, Lntb;->ai:I

    .line 1747
    return v0
.end method

.method public a(Loxn;)Lntb;
    .locals 2

    .prologue
    .line 1755
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1756
    sparse-switch v0, :sswitch_data_0

    .line 1760
    iget-object v1, p0, Lntb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1761
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lntb;->ah:Ljava/util/List;

    .line 1764
    :cond_1
    iget-object v1, p0, Lntb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1766
    :sswitch_0
    return-object p0

    .line 1771
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lntb;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 1775
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lntb;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1779
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1780
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1782
    :cond_2
    iput v0, p0, Lntb;->c:I

    goto :goto_0

    .line 1784
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lntb;->c:I

    goto :goto_0

    .line 1756
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1717
    iget-object v0, p0, Lntb;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1718
    const/4 v0, 0x1

    iget-object v1, p0, Lntb;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1720
    :cond_0
    iget-object v0, p0, Lntb;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1721
    const/4 v0, 0x2

    iget-object v1, p0, Lntb;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1723
    :cond_1
    iget v0, p0, Lntb;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 1724
    const/4 v0, 0x3

    iget v1, p0, Lntb;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1726
    :cond_2
    iget-object v0, p0, Lntb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1728
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1701
    invoke-virtual {p0, p1}, Lntb;->a(Loxn;)Lntb;

    move-result-object v0

    return-object v0
.end method
