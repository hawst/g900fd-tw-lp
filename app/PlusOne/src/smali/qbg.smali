.class public final Lqbg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lqbg;


# instance fields
.field public b:Lqbf;

.field public c:Lqbf;

.field public d:Lqbf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 428
    const/4 v0, 0x0

    new-array v0, v0, [Lqbg;

    sput-object v0, Lqbg;->a:[Lqbg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 429
    invoke-direct {p0}, Loxq;-><init>()V

    .line 432
    iput-object v0, p0, Lqbg;->b:Lqbf;

    .line 435
    iput-object v0, p0, Lqbg;->c:Lqbf;

    .line 438
    iput-object v0, p0, Lqbg;->d:Lqbf;

    .line 429
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 458
    const/4 v0, 0x0

    .line 459
    iget-object v1, p0, Lqbg;->b:Lqbf;

    if-eqz v1, :cond_0

    .line 460
    const/4 v0, 0x1

    iget-object v1, p0, Lqbg;->b:Lqbf;

    .line 461
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 463
    :cond_0
    iget-object v1, p0, Lqbg;->c:Lqbf;

    if-eqz v1, :cond_1

    .line 464
    const/4 v1, 0x2

    iget-object v2, p0, Lqbg;->c:Lqbf;

    .line 465
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_1
    iget-object v1, p0, Lqbg;->d:Lqbf;

    if-eqz v1, :cond_2

    .line 468
    const/4 v1, 0x3

    iget-object v2, p0, Lqbg;->d:Lqbf;

    .line 469
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    :cond_2
    iget-object v1, p0, Lqbg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 472
    iput v0, p0, Lqbg;->ai:I

    .line 473
    return v0
.end method

.method public a(Loxn;)Lqbg;
    .locals 2

    .prologue
    .line 481
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 482
    sparse-switch v0, :sswitch_data_0

    .line 486
    iget-object v1, p0, Lqbg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 487
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbg;->ah:Ljava/util/List;

    .line 490
    :cond_1
    iget-object v1, p0, Lqbg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    :sswitch_0
    return-object p0

    .line 497
    :sswitch_1
    iget-object v0, p0, Lqbg;->b:Lqbf;

    if-nez v0, :cond_2

    .line 498
    new-instance v0, Lqbf;

    invoke-direct {v0}, Lqbf;-><init>()V

    iput-object v0, p0, Lqbg;->b:Lqbf;

    .line 500
    :cond_2
    iget-object v0, p0, Lqbg;->b:Lqbf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 504
    :sswitch_2
    iget-object v0, p0, Lqbg;->c:Lqbf;

    if-nez v0, :cond_3

    .line 505
    new-instance v0, Lqbf;

    invoke-direct {v0}, Lqbf;-><init>()V

    iput-object v0, p0, Lqbg;->c:Lqbf;

    .line 507
    :cond_3
    iget-object v0, p0, Lqbg;->c:Lqbf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 511
    :sswitch_3
    iget-object v0, p0, Lqbg;->d:Lqbf;

    if-nez v0, :cond_4

    .line 512
    new-instance v0, Lqbf;

    invoke-direct {v0}, Lqbf;-><init>()V

    iput-object v0, p0, Lqbg;->d:Lqbf;

    .line 514
    :cond_4
    iget-object v0, p0, Lqbg;->d:Lqbf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 482
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lqbg;->b:Lqbf;

    if-eqz v0, :cond_0

    .line 444
    const/4 v0, 0x1

    iget-object v1, p0, Lqbg;->b:Lqbf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 446
    :cond_0
    iget-object v0, p0, Lqbg;->c:Lqbf;

    if-eqz v0, :cond_1

    .line 447
    const/4 v0, 0x2

    iget-object v1, p0, Lqbg;->c:Lqbf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 449
    :cond_1
    iget-object v0, p0, Lqbg;->d:Lqbf;

    if-eqz v0, :cond_2

    .line 450
    const/4 v0, 0x3

    iget-object v1, p0, Lqbg;->d:Lqbf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 452
    :cond_2
    iget-object v0, p0, Lqbg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 454
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 425
    invoke-virtual {p0, p1}, Lqbg;->a(Loxn;)Lqbg;

    move-result-object v0

    return-object v0
.end method
