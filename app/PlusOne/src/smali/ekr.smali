.class public final Lekr;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private Q:Lhee;

.field private R:Leks;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lloj;-><init>()V

    .line 90
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 195
    const v0, 0x7f0400f6

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 196
    invoke-virtual {p0}, Lekr;->k()Landroid/os/Bundle;

    move-result-object v3

    .line 199
    const v0, 0x102000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 201
    const-string v1, "audience"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lhgw;

    .line 202
    new-instance v4, Leks;

    invoke-virtual {p0}, Lekr;->n()Lz;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Leks;-><init>(Landroid/content/Context;Lhgw;)V

    iput-object v4, p0, Lekr;->R:Leks;

    .line 203
    iget-object v1, p0, Lekr;->R:Leks;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 204
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 207
    const v0, 0x7f1001db

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    const v0, 0x7f1001da

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 212
    const-string v0, "people_list_title"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 213
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    const v0, 0x7f100346

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 215
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    :cond_0
    return-object v2
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 184
    invoke-super {p0, p1}, Lloj;->a(Landroid/os/Bundle;)V

    .line 189
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lekr;->a(II)V

    .line 190
    return-void
.end method

.method public aO_()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 224
    invoke-super {p0}, Lloj;->aO_()V

    .line 226
    invoke-virtual {p0}, Lekr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "restrict_to_domain"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {p0}, Lekr;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100173

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lekr;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0989

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lekr;->Q:Lhee;

    invoke-interface {v4}, Lhee;->g()Lhej;

    move-result-object v4

    const-string v5, "domain_name"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    :cond_0
    return-void
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 46
    iget-object v0, p0, Lekr;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lekr;->Q:Lhee;

    .line 47
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 241
    invoke-virtual {p0}, Lekr;->a()V

    .line 242
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 246
    iget-object v0, p0, Lekr;->R:Leks;

    invoke-virtual {v0, p3}, Leks;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekt;

    .line 247
    iget v2, v0, Lekt;->a:I

    packed-switch v2, :pswitch_data_0

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 249
    :pswitch_0
    iget-object v0, v0, Lekt;->b:Ljqs;

    .line 252
    invoke-virtual {v0}, Ljqs;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 253
    invoke-virtual {v0}, Ljqs;->a()Ljava/lang/String;

    move-result-object v0

    .line 255
    :goto_1
    if-eqz v0, :cond_2

    .line 256
    const-string v2, "g:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 258
    :goto_2
    if-eqz v0, :cond_0

    .line 259
    iget-object v2, p0, Lekr;->Q:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 260
    invoke-virtual {p0}, Lekr;->n()Lz;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v2, v0, v1, v4}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 262
    invoke-virtual {p0}, Lekr;->a()V

    .line 263
    invoke-virtual {p0, v0}, Lekr;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 256
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1

    .line 247
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
