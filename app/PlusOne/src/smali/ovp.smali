.class public final Lovp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lovp;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Lovq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    new-array v0, v0, [Lovp;

    sput-object v0, Lovp;->a:[Lovp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Loxq;-><init>()V

    .line 93
    sget-object v0, Lovq;->a:[Lovq;

    iput-object v0, p0, Lovp;->c:[Lovq;

    .line 88
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 114
    .line 115
    iget-object v0, p0, Lovp;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 116
    const/4 v0, 0x1

    iget-object v2, p0, Lovp;->b:Ljava/lang/String;

    .line 117
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 119
    :goto_0
    iget-object v2, p0, Lovp;->c:[Lovq;

    if-eqz v2, :cond_1

    .line 120
    iget-object v2, p0, Lovp;->c:[Lovq;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 121
    if-eqz v4, :cond_0

    .line 122
    const/4 v5, 0x2

    .line 123
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 120
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 127
    :cond_1
    iget-object v1, p0, Lovp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    iput v0, p0, Lovp;->ai:I

    .line 129
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lovp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 137
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 138
    sparse-switch v0, :sswitch_data_0

    .line 142
    iget-object v2, p0, Lovp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 143
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lovp;->ah:Ljava/util/List;

    .line 146
    :cond_1
    iget-object v2, p0, Lovp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    :sswitch_0
    return-object p0

    .line 153
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lovp;->b:Ljava/lang/String;

    goto :goto_0

    .line 157
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 158
    iget-object v0, p0, Lovp;->c:[Lovq;

    if-nez v0, :cond_3

    move v0, v1

    .line 159
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lovq;

    .line 160
    iget-object v3, p0, Lovp;->c:[Lovq;

    if-eqz v3, :cond_2

    .line 161
    iget-object v3, p0, Lovp;->c:[Lovq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 163
    :cond_2
    iput-object v2, p0, Lovp;->c:[Lovq;

    .line 164
    :goto_2
    iget-object v2, p0, Lovp;->c:[Lovq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 165
    iget-object v2, p0, Lovp;->c:[Lovq;

    new-instance v3, Lovq;

    invoke-direct {v3}, Lovq;-><init>()V

    aput-object v3, v2, v0

    .line 166
    iget-object v2, p0, Lovp;->c:[Lovq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 167
    invoke-virtual {p1}, Loxn;->a()I

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 158
    :cond_3
    iget-object v0, p0, Lovp;->c:[Lovq;

    array-length v0, v0

    goto :goto_1

    .line 170
    :cond_4
    iget-object v2, p0, Lovp;->c:[Lovq;

    new-instance v3, Lovq;

    invoke-direct {v3}, Lovq;-><init>()V

    aput-object v3, v2, v0

    .line 171
    iget-object v2, p0, Lovp;->c:[Lovq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 138
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 98
    iget-object v0, p0, Lovp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x1

    iget-object v1, p0, Lovp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lovp;->c:[Lovq;

    if-eqz v0, :cond_2

    .line 102
    iget-object v1, p0, Lovp;->c:[Lovq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 103
    if-eqz v3, :cond_1

    .line 104
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 102
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_2
    iget-object v0, p0, Lovp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 110
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lovp;->a(Loxn;)Lovp;

    move-result-object v0

    return-object v0
.end method
