.class public final Loxm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loxm;


# instance fields
.field private b:[B

.field private c:Ljava/lang/Integer;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Loxm;

    sput-object v0, Loxm;->a:[Loxm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25
    const/high16 v0, -0x80000000

    iput v0, p0, Loxm;->d:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    iget-object v1, p0, Loxm;->b:[B

    if-eqz v1, :cond_0

    .line 47
    const/4 v0, 0x1

    iget-object v1, p0, Loxm;->b:[B

    .line 48
    invoke-static {v0, v1}, Loxo;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 50
    :cond_0
    iget-object v1, p0, Loxm;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 51
    const/4 v1, 0x2

    iget-object v2, p0, Loxm;->c:Ljava/lang/Integer;

    .line 52
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_1
    iget v1, p0, Loxm;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 55
    const/4 v1, 0x3

    iget v2, p0, Loxm;->d:I

    .line 56
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget-object v1, p0, Loxm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    iput v0, p0, Loxm;->ai:I

    .line 60
    return v0
.end method

.method public a(Loxn;)Loxm;
    .locals 2

    .prologue
    .line 68
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 69
    sparse-switch v0, :sswitch_data_0

    .line 73
    iget-object v1, p0, Loxm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 74
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loxm;->ah:Ljava/util/List;

    .line 77
    :cond_1
    iget-object v1, p0, Loxm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    :sswitch_0
    return-object p0

    .line 84
    :sswitch_1
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Loxm;->b:[B

    goto :goto_0

    .line 88
    :sswitch_2
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxm;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 92
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 93
    if-eqz v0, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 98
    :cond_2
    iput v0, p0, Loxm;->d:I

    goto :goto_0

    .line 100
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Loxm;->d:I

    goto :goto_0

    .line 69
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Loxm;->b:[B

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Loxm;->b:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 33
    :cond_0
    iget-object v0, p0, Loxm;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 34
    const/4 v0, 0x2

    iget-object v1, p0, Loxm;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 36
    :cond_1
    iget v0, p0, Loxm;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 37
    const/4 v0, 0x3

    iget v1, p0, Loxm;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 39
    :cond_2
    iget-object v0, p0, Loxm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 41
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loxm;->a(Loxn;)Loxm;

    move-result-object v0

    return-object v0
.end method
