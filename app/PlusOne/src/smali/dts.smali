.class public final enum Ldts;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldts;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldts;

.field public static final enum b:Ldts;

.field public static final enum c:Ldts;

.field public static final enum d:Ldts;

.field public static final enum e:Ldts;

.field public static final enum f:Ldts;

.field private static final synthetic h:[Ldts;


# instance fields
.field public final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 75
    new-instance v0, Ldts;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v3, v4}, Ldts;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Ldts;->a:Ldts;

    .line 78
    new-instance v0, Ldts;

    const-string v1, "USER_PULL_DOWN"

    invoke-direct {v0, v1, v4, v3}, Ldts;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Ldts;->b:Ldts;

    .line 81
    new-instance v0, Ldts;

    const-string v1, "USER_FORCE_REFRESH"

    invoke-direct {v0, v1, v5, v3}, Ldts;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Ldts;->c:Ldts;

    .line 84
    new-instance v0, Ldts;

    const-string v1, "USER_LOAD_MORE"

    invoke-direct {v0, v1, v6, v3}, Ldts;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Ldts;->d:Ldts;

    .line 87
    new-instance v0, Ldts;

    const-string v1, "PERIODIC"

    invoke-direct {v0, v1, v7, v4}, Ldts;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Ldts;->e:Ldts;

    .line 90
    new-instance v0, Ldts;

    const-string v1, "TICKLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, Ldts;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Ldts;->f:Ldts;

    .line 73
    const/4 v0, 0x6

    new-array v0, v0, [Ldts;

    sget-object v1, Ldts;->a:Ldts;

    aput-object v1, v0, v3

    sget-object v1, Ldts;->b:Ldts;

    aput-object v1, v0, v4

    sget-object v1, Ldts;->c:Ldts;

    aput-object v1, v0, v5

    sget-object v1, Ldts;->d:Ldts;

    aput-object v1, v0, v6

    sget-object v1, Ldts;->e:Ldts;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldts;->f:Ldts;

    aput-object v2, v0, v1

    sput-object v0, Ldts;->h:[Ldts;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput-boolean p3, p0, Ldts;->g:Z

    .line 96
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldts;
    .locals 1

    .prologue
    .line 73
    const-class v0, Ldts;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldts;

    return-object v0
.end method

.method public static values()[Ldts;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Ldts;->h:[Ldts;

    invoke-virtual {v0}, [Ldts;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldts;

    return-object v0
.end method
