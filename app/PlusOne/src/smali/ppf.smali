.class public final Lppf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lppf;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Lpih;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lppf;

    sput-object v0, Lppf;->a:[Lppf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30
    const/high16 v0, -0x80000000

    iput v0, p0, Lppf;->f:I

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lppf;->g:Lpih;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Lppf;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 74
    const/4 v0, 0x1

    iget-object v1, p0, Lppf;->b:Ljava/lang/String;

    .line 75
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 77
    :cond_0
    iget-object v1, p0, Lppf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 78
    const/4 v1, 0x2

    iget-object v2, p0, Lppf;->c:Ljava/lang/String;

    .line 79
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_1
    iget v1, p0, Lppf;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 82
    const/4 v1, 0x3

    iget v2, p0, Lppf;->f:I

    .line 83
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_2
    iget-object v1, p0, Lppf;->g:Lpih;

    if-eqz v1, :cond_3

    .line 86
    const/4 v1, 0x4

    iget-object v2, p0, Lppf;->g:Lpih;

    .line 87
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_3
    iget-object v1, p0, Lppf;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 90
    const/4 v1, 0x5

    iget-object v2, p0, Lppf;->h:Ljava/lang/String;

    .line 91
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_4
    iget-object v1, p0, Lppf;->i:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 94
    const/4 v1, 0x6

    iget-object v2, p0, Lppf;->i:Ljava/lang/String;

    .line 95
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_5
    iget-object v1, p0, Lppf;->d:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 98
    const/4 v1, 0x7

    iget-object v2, p0, Lppf;->d:Ljava/lang/String;

    .line 99
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_6
    iget-object v1, p0, Lppf;->e:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 102
    const/16 v1, 0x8

    iget-object v2, p0, Lppf;->e:Ljava/lang/String;

    .line 103
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_7
    iget-object v1, p0, Lppf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    iput v0, p0, Lppf;->ai:I

    .line 107
    return v0
.end method

.method public a(Loxn;)Lppf;
    .locals 2

    .prologue
    .line 115
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 116
    sparse-switch v0, :sswitch_data_0

    .line 120
    iget-object v1, p0, Lppf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 121
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lppf;->ah:Ljava/util/List;

    .line 124
    :cond_1
    iget-object v1, p0, Lppf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    :sswitch_0
    return-object p0

    .line 131
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppf;->b:Ljava/lang/String;

    goto :goto_0

    .line 135
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppf;->c:Ljava/lang/String;

    goto :goto_0

    .line 139
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 140
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 146
    :cond_2
    iput v0, p0, Lppf;->f:I

    goto :goto_0

    .line 148
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lppf;->f:I

    goto :goto_0

    .line 153
    :sswitch_4
    iget-object v0, p0, Lppf;->g:Lpih;

    if-nez v0, :cond_4

    .line 154
    new-instance v0, Lpih;

    invoke-direct {v0}, Lpih;-><init>()V

    iput-object v0, p0, Lppf;->g:Lpih;

    .line 156
    :cond_4
    iget-object v0, p0, Lppf;->g:Lpih;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 160
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppf;->h:Ljava/lang/String;

    goto :goto_0

    .line 164
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppf;->i:Ljava/lang/String;

    goto :goto_0

    .line 168
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppf;->d:Ljava/lang/String;

    goto :goto_0

    .line 172
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppf;->e:Ljava/lang/String;

    goto :goto_0

    .line 116
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lppf;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x1

    iget-object v1, p0, Lppf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 45
    :cond_0
    iget-object v0, p0, Lppf;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 46
    const/4 v0, 0x2

    iget-object v1, p0, Lppf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 48
    :cond_1
    iget v0, p0, Lppf;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 49
    const/4 v0, 0x3

    iget v1, p0, Lppf;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 51
    :cond_2
    iget-object v0, p0, Lppf;->g:Lpih;

    if-eqz v0, :cond_3

    .line 52
    const/4 v0, 0x4

    iget-object v1, p0, Lppf;->g:Lpih;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 54
    :cond_3
    iget-object v0, p0, Lppf;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 55
    const/4 v0, 0x5

    iget-object v1, p0, Lppf;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 57
    :cond_4
    iget-object v0, p0, Lppf;->i:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 58
    const/4 v0, 0x6

    iget-object v1, p0, Lppf;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 60
    :cond_5
    iget-object v0, p0, Lppf;->d:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 61
    const/4 v0, 0x7

    iget-object v1, p0, Lppf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 63
    :cond_6
    iget-object v0, p0, Lppf;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 64
    const/16 v0, 0x8

    iget-object v1, p0, Lppf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 66
    :cond_7
    iget-object v0, p0, Lppf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 68
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lppf;->a(Loxn;)Lppf;

    move-result-object v0

    return-object v0
.end method
