.class public final Lchp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:Lcom/google/android/libraries/photoeditor/core/FilterChain;


# instance fields
.field private a:I

.field private b:I

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/graphics/Bitmap;

.field private g:Landroid/graphics/Bitmap;

.field private h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

.field private i:Z

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-direct {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;-><init>()V

    sput-object v0, Lchp;->c:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/16 v0, 0x800

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput v0, p0, Lchp;->a:I

    .line 32
    iput v0, p0, Lchp;->b:I

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lchp;->i:Z

    .line 397
    return-void
.end method

.method static synthetic a(Lchp;Lcom/google/android/libraries/photoeditor/core/FilterChain;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lchp;->a(Lcom/google/android/libraries/photoeditor/core/FilterChain;)V

    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/libraries/photoeditor/core/FilterChain;)V
    .locals 4

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lchp;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The source image was not yet set up"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 130
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lchp;->n()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lchp;->e:Landroid/graphics/Bitmap;

    .line 133
    if-nez p1, :cond_1

    .line 134
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-direct {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;-><init>()V

    iput-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 143
    :goto_0
    invoke-virtual {p0}, Lchp;->d()Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    monitor-exit p0

    return-void

    .line 136
    :cond_1
    :try_start_2
    iput-object p1, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 137
    sget-object v0, Lhan;->a:Lhao;

    iget-object v1, p0, Lchp;->d:Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-interface {v0, v1, p1, v2, v3}, Lhao;->renderFilterChain(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lchp;->f:Landroid/graphics/Bitmap;

    .line 140
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lchp;->a(Z)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lchp;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lchp;->f:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lchp;->g:Landroid/graphics/Bitmap;

    if-eq v0, v1, :cond_0

    .line 105
    iget-object v0, p0, Lchp;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 108
    :cond_0
    iput-object v2, p0, Lchp;->f:Landroid/graphics/Bitmap;

    .line 113
    :cond_1
    iput-object v2, p0, Lchp;->g:Landroid/graphics/Bitmap;

    .line 114
    return-void
.end method


# virtual methods
.method public a(Z)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 245
    if-eqz p1, :cond_0

    iget-object v0, p0, Lchp;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lchp;->e:Landroid/graphics/Bitmap;

    .line 275
    :goto_0
    return-object v0

    .line 251
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lchp;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-direct {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;-><init>()V

    .line 253
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 254
    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectWidth()F

    move-result v2

    iget-object v3, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectHeight()F

    move-result v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 255
    iget-object v2, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v2

    iget-object v3, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offsetTo(FF)V

    .line 256
    iget-object v2, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 258
    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getRotationAngle()F

    move-result v2

    iget-object v3, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 259
    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getAspectRatio()I

    move-result v3

    iget-object v4, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 260
    invoke-virtual {v4}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v4

    iget-object v5, p0, Lchp;->d:Landroid/graphics/Bitmap;

    .line 261
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget-object v6, p0, Lchp;->d:Landroid/graphics/Bitmap;

    .line 262
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 256
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Landroid/graphics/RectF;FIIII)V

    .line 265
    sget-object v1, Lhan;->a:Lhao;

    iget-object v2, p0, Lchp;->d:Landroid/graphics/Bitmap;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v0, v3, v4}, Lhao;->renderFilterChain(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 267
    iget v1, p0, Lchp;->a:I

    iget v2, p0, Lchp;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lchp;->e:Landroid/graphics/Bitmap;

    .line 269
    iget-object v0, p0, Lchp;->e:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 271
    :cond_1
    iget-object v0, p0, Lchp;->d:Landroid/graphics/Bitmap;

    iget v1, p0, Lchp;->a:I

    iget v2, p0, Lchp;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lchp;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lchp;->j:Z

    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lchp;->a(Lcom/google/android/libraries/photoeditor/core/FilterChain;)V

    .line 150
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(II)V
    .locals 2

    .prologue
    const/16 v1, 0x800

    .line 196
    iget v0, p0, Lchp;->a:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lchp;->b:I

    if-ne v0, p2, :cond_0

    .line 204
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lchp;->a:I

    .line 201
    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lchp;->b:I

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lchp;->g:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public declared-synchronized a(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;Lchs;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 59
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v2, p0, Lchp;->d:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v2}, Landroid/graphics/Bitmap;->sameAs(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v1

    .line 60
    :goto_0
    if-eqz v2, :cond_2

    iget-object v3, p0, Lchp;->d:Landroid/graphics/Bitmap;

    if-nez v3, :cond_2

    .line 61
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Invalid source bitmap reference (cannot be null during the initial call)"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v2, v0

    goto :goto_0

    .line 65
    :cond_2
    if-eqz p1, :cond_3

    :try_start_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v3, v4, :cond_3

    .line 66
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 67
    if-nez p1, :cond_3

    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to convert the image pixel format"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lchp;->e:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    iget-object v3, p0, Lchp;->d:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lchp;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v3, 0x0

    iput-object v3, p0, Lchp;->d:Landroid/graphics/Bitmap;

    :cond_4
    invoke-direct {p0}, Lchp;->n()V

    const/4 v3, 0x0

    iput-object v3, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 74
    if-nez v2, :cond_5

    .line 75
    iput-object p1, p0, Lchp;->d:Landroid/graphics/Bitmap;

    .line 78
    :cond_5
    const/4 v2, 0x0

    iput-boolean v2, p0, Lchp;->j:Z

    .line 79
    if-eqz p2, :cond_6

    .line 80
    invoke-virtual {p2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->size()I

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f()Z

    move-result v2

    if-nez v2, :cond_7

    :cond_6
    move v0, v1

    :cond_7
    iput-boolean v0, p0, Lchp;->i:Z

    .line 82
    if-nez p3, :cond_8

    .line 83
    invoke-direct {p0, p2}, Lchp;->a(Lcom/google/android/libraries/photoeditor/core/FilterChain;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :goto_1
    monitor-exit p0

    return-void

    .line 85
    :cond_8
    :try_start_2
    new-instance v0, Lchq;

    invoke-direct {v0, p0, p3}, Lchq;-><init>(Lchp;Lchs;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lchq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 352
    invoke-virtual {p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    .line 353
    if-ne v0, v2, :cond_0

    .line 384
    :goto_0
    return-void

    .line 357
    :cond_0
    iput-boolean v2, p0, Lchp;->j:Z

    .line 359
    const/16 v1, 0x14

    if-eq v0, v1, :cond_1

    invoke-static {v0}, Lchp;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 360
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lchp;->e:Landroid/graphics/Bitmap;

    .line 361
    invoke-virtual {p0, v2}, Lchp;->a(Z)Landroid/graphics/Bitmap;

    .line 364
    :cond_2
    invoke-direct {p0}, Lchp;->n()V

    .line 366
    iput-object p2, p0, Lchp;->f:Landroid/graphics/Bitmap;

    .line 370
    iget-object v1, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    iget-object v2, p0, Lchp;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->c(I)V

    .line 371
    iget-object v1, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    iget-object v2, p0, Lchp;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d(I)V

    .line 373
    sget-object v1, Lhan;->a:Lhao;

    iget-object v2, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-interface {v1, v2, p1}, Lhao;->a(Lcom/google/android/libraries/photoeditor/core/FilterChain;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 378
    invoke-static {v0}, Lchp;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 379
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 380
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 383
    :cond_3
    invoke-virtual {p0}, Lchp;->d()Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lchp;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lchp;->j:Z

    .line 155
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d()V

    .line 156
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-direct {p0, v0}, Lchp;->a(Lcom/google/android/libraries/photoeditor/core/FilterChain;)V

    .line 157
    return-void

    .line 153
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 164
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v0

    if-nez v0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget-object v0, p0, Lchp;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 169
    sget-object v0, Lhan;->a:Lhao;

    iget-object v1, p0, Lchp;->f:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v2

    invoke-interface {v0, v1, v2, v3}, Lhao;->rotateImage(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lchp;->f:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 170
    :cond_2
    iget-object v0, p0, Lchp;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Lhan;->a:Lhao;

    iget-object v1, p0, Lchp;->d:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v2

    invoke-interface {v0, v1, v2, v3}, Lhao;->rotateImage(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lchp;->d:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public d()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lchp;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lchp;->g:Landroid/graphics/Bitmap;

    .line 218
    :goto_0
    return-object v0

    .line 216
    :cond_0
    invoke-virtual {p0}, Lchp;->g()Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lchp;->a:I

    iget v2, p0, Lchp;->b:I

    .line 215
    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lchp;->g:Landroid/graphics/Bitmap;

    .line 218
    iget-object v0, p0, Lchp;->g:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lchp;->d:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0}, Lchp;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lchp;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lchp;->f:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lchp;->d:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public h()Lcom/google/android/libraries/photoeditor/core/FilterChain;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    return-object v0
.end method

.method public i()Lcom/google/android/libraries/photoeditor/core/FilterChain;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lchp;->c:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 313
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    if-nez v0, :cond_0

    .line 322
    const/4 v0, 0x0

    .line 332
    :goto_0
    return v0

    .line 325
    :cond_0
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->c()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    const/16 v1, 0x12

    if-eq v0, v1, :cond_1

    .line 328
    const/4 v0, 0x1

    goto :goto_0

    .line 332
    :cond_1
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f()Z

    move-result v0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 341
    iget-boolean v0, p0, Lchp;->j:Z

    return v0
.end method

.method public m()Z
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 345
    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getRotationAngle()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 346
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 347
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectWidth()F

    move-result v0

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectHeight()F

    move-result v0

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lchp;->h:Lcom/google/android/libraries/photoeditor/core/FilterChain;

    .line 348
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
