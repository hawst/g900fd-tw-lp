.class public final Lkjv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/MultiAutoCompleteTextView$Tokenizer;


# static fields
.field private static a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lkjv;->b:Ljava/lang/String;

    .line 28
    sget-object v0, Lkjv;->a:Ljava/util/HashSet;

    if-nez v0, :cond_1

    .line 29
    new-instance v0, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v1, 0x2c

    invoke-direct {v0, v1}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 30
    invoke-virtual {v0, p2}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 32
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, Lkjv;->a:Ljava/util/HashSet;

    .line 34
    invoke-virtual {v0}, Landroid/text/TextUtils$SimpleStringSplitter;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 35
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\n"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 37
    sget-object v2, Lkjv;->a:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 41
    :cond_1
    return-void
.end method

.method private a(Ljava/lang/CharSequence;II)I
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 125
    add-int/lit8 v0, p2, 0x1

    if-ge v0, p3, :cond_1

    add-int/lit8 v0, p2, 0x1

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-direct {p0, v0}, Lkjv;->a(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    :cond_0
    :goto_0
    return p2

    .line 128
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 129
    const/4 v1, 0x0

    .line 130
    :goto_1
    if-ge p2, p3, :cond_5

    .line 131
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 133
    const/16 v2, 0x200b

    if-eq v0, v2, :cond_0

    .line 137
    if-eq v0, v4, :cond_0

    .line 142
    invoke-direct {p0, v0}, Lkjv;->a(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 143
    add-int/lit8 v2, v1, 0x1

    .line 144
    const/4 v0, 0x4

    if-ge v2, v0, :cond_0

    .line 149
    add-int/lit8 v1, p2, 0x1

    .line 150
    :goto_2
    if-ge v1, v3, :cond_2

    .line 151
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 152
    if-eq v0, v4, :cond_0

    .line 156
    invoke-direct {p0, v0}, Lkjv;->a(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 161
    :cond_2
    if-eq v1, v3, :cond_0

    .line 166
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    move p2, v1

    move v1, v2

    .line 169
    :cond_3
    if-le p2, p3, :cond_4

    .line 170
    invoke-static {v0}, Lkjw;->a(C)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p2, :cond_0

    add-int/lit8 v0, p2, -0x1

    .line 171
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-direct {p0, v0}, Lkjv;->a(C)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    :cond_4
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_5
    move p2, p3

    .line 176
    goto :goto_0
.end method

.method private a(C)Z
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x200b

    if-eq p1, v0, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public findTokenEnd(Ljava/lang/CharSequence;I)I
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0, p1, p2, p2}, Lkjv;->a(Ljava/lang/CharSequence;II)I

    move-result v0

    return v0
.end method

.method public findTokenStart(Ljava/lang/CharSequence;I)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 49
    add-int/lit8 v1, p2, -0x1

    :goto_0
    if-ltz v1, :cond_0

    .line 52
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 53
    const/16 v4, 0xa

    if-ne v0, v4, :cond_1

    .line 82
    :cond_0
    :goto_1
    return p2

    .line 57
    :cond_1
    invoke-static {v0}, Lkjw;->a(C)Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz v1, :cond_2

    add-int/lit8 v0, v1, -0x1

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-direct {p0, v0}, Lkjv;->a(C)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 60
    :cond_2
    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Landroid/text/Spannable;

    const-class v4, Lljd;

    invoke-interface {v0, v1, v1, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lljd;

    if-eqz v0, :cond_3

    array-length v0, v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    if-nez v0, :cond_0

    .line 65
    invoke-direct {p0, p1, v1, p2}, Lkjv;->a(Ljava/lang/CharSequence;II)I

    move-result v0

    .line 68
    :goto_3
    if-ge v0, p2, :cond_6

    .line 69
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-direct {p0, v4}, Lkjv;->a(C)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move-object v0, p1

    .line 60
    check-cast v0, Landroid/text/Spannable;

    const-class v4, Landroid/text/style/URLSpan;

    invoke-interface {v0, v1, v1, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    if-eqz v0, :cond_5

    array-length v4, v0

    if-eqz v4, :cond_5

    array-length v5, v0

    move v4, v3

    :goto_4
    if-ge v4, v5, :cond_5

    aget-object v6, v0, v4

    invoke-static {v6}, Lljd;->a(Landroid/text/style/URLSpan;)Z

    move-result v6

    if-eqz v6, :cond_4

    move v0, v2

    goto :goto_2

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_5
    move v0, v3

    goto :goto_2

    .line 77
    :cond_6
    if-ne v0, p2, :cond_7

    move p2, v1

    .line 78
    goto :goto_1

    .line 49
    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 181
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 182
    if-eqz v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-direct {p0, v0}, Lkjv;->a(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v4, p1

    .line 190
    :goto_0
    return-object v4

    .line 185
    :cond_1
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_2

    .line 186
    new-instance v4, Landroid/text/SpannableString;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lkjv;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, p1

    .line 187
    check-cast v0, Landroid/text/Spanned;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const-class v3, Ljava/lang/Object;

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    goto :goto_0

    .line 190
    :cond_2
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkjv;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
