.class public final Lnjz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnjz;


# instance fields
.field public b:Lnja;

.field public c:Lnie;

.field public d:Ljava/lang/String;

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7151
    const/4 v0, 0x0

    new-array v0, v0, [Lnjz;

    sput-object v0, Lnjz;->a:[Lnjz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7152
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7168
    iput-object v0, p0, Lnjz;->b:Lnja;

    .line 7171
    iput-object v0, p0, Lnjz;->c:Lnie;

    .line 7176
    const/high16 v0, -0x80000000

    iput v0, p0, Lnjz;->e:I

    .line 7152
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7199
    const/4 v0, 0x0

    .line 7200
    iget-object v1, p0, Lnjz;->b:Lnja;

    if-eqz v1, :cond_0

    .line 7201
    const/4 v0, 0x1

    iget-object v1, p0, Lnjz;->b:Lnja;

    .line 7202
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7204
    :cond_0
    iget-object v1, p0, Lnjz;->c:Lnie;

    if-eqz v1, :cond_1

    .line 7205
    const/4 v1, 0x2

    iget-object v2, p0, Lnjz;->c:Lnie;

    .line 7206
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7208
    :cond_1
    iget-object v1, p0, Lnjz;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 7209
    const/4 v1, 0x3

    iget-object v2, p0, Lnjz;->d:Ljava/lang/String;

    .line 7210
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7212
    :cond_2
    iget v1, p0, Lnjz;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 7213
    const/4 v1, 0x4

    iget v2, p0, Lnjz;->e:I

    .line 7214
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7216
    :cond_3
    iget-object v1, p0, Lnjz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7217
    iput v0, p0, Lnjz;->ai:I

    .line 7218
    return v0
.end method

.method public a(Loxn;)Lnjz;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 7226
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7227
    sparse-switch v0, :sswitch_data_0

    .line 7231
    iget-object v1, p0, Lnjz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7232
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjz;->ah:Ljava/util/List;

    .line 7235
    :cond_1
    iget-object v1, p0, Lnjz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7237
    :sswitch_0
    return-object p0

    .line 7242
    :sswitch_1
    iget-object v0, p0, Lnjz;->b:Lnja;

    if-nez v0, :cond_2

    .line 7243
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnjz;->b:Lnja;

    .line 7245
    :cond_2
    iget-object v0, p0, Lnjz;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7249
    :sswitch_2
    iget-object v0, p0, Lnjz;->c:Lnie;

    if-nez v0, :cond_3

    .line 7250
    new-instance v0, Lnie;

    invoke-direct {v0}, Lnie;-><init>()V

    iput-object v0, p0, Lnjz;->c:Lnie;

    .line 7252
    :cond_3
    iget-object v0, p0, Lnjz;->c:Lnie;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7256
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjz;->d:Ljava/lang/String;

    goto :goto_0

    .line 7260
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 7261
    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-ne v0, v1, :cond_5

    .line 7271
    :cond_4
    iput v0, p0, Lnjz;->e:I

    goto :goto_0

    .line 7273
    :cond_5
    iput v2, p0, Lnjz;->e:I

    goto :goto_0

    .line 7227
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7181
    iget-object v0, p0, Lnjz;->b:Lnja;

    if-eqz v0, :cond_0

    .line 7182
    const/4 v0, 0x1

    iget-object v1, p0, Lnjz;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7184
    :cond_0
    iget-object v0, p0, Lnjz;->c:Lnie;

    if-eqz v0, :cond_1

    .line 7185
    const/4 v0, 0x2

    iget-object v1, p0, Lnjz;->c:Lnie;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7187
    :cond_1
    iget-object v0, p0, Lnjz;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 7188
    const/4 v0, 0x3

    iget-object v1, p0, Lnjz;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7190
    :cond_2
    iget v0, p0, Lnjz;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 7191
    const/4 v0, 0x4

    iget v1, p0, Lnjz;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7193
    :cond_3
    iget-object v0, p0, Lnjz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7195
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7148
    invoke-virtual {p0, p1}, Lnjz;->a(Loxn;)Lnjz;

    move-result-object v0

    return-object v0
.end method
