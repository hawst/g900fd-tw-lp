.class public Lloa;
.super Llrl;
.source "PG"

# interfaces
.implements Llnk;


# instance fields
.field private e:Llqw;

.field public final x:Llnh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Llrl;-><init>()V

    .line 19
    new-instance v0, Llnh;

    invoke-direct {v0}, Llnh;-><init>()V

    iput-object v0, p0, Lloa;->x:Llnh;

    return-void
.end method

.method static synthetic a(Lloa;)Llqc;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lloa;->y:Llqc;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lloa;->x:Llnh;

    new-instance v1, Lloe;

    iget-object v2, p0, Lloa;->y:Llqc;

    invoke-direct {v1, p0, v2}, Lloe;-><init>(Landroid/app/Activity;Llqr;)V

    invoke-virtual {v0, v1}, Llnh;->a(Llnq;)Llnh;

    .line 59
    return-void
.end method

.method public h_()Llnh;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lloa;->x:Llnh;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 29
    invoke-virtual {p0}, Lloa;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lloa;->x:Llnh;

    invoke-virtual {v1, p0}, Llnh;->a(Landroid/content/Context;)V

    .line 31
    iget-object v1, p0, Lloa;->x:Llnh;

    invoke-virtual {v1, v0}, Llnh;->a(Llnh;)V

    .line 33
    invoke-virtual {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 34
    iget-object v0, p0, Lloa;->x:Llnh;

    const-class v1, Lloi;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lloi;

    iget-object v2, p0, Lloa;->y:Llqc;

    iget-object v3, p0, Lloa;->x:Llnh;

    invoke-interface {v0, p0, v2, v3}, Lloi;->a(Landroid/app/Activity;Llqr;Llnh;)V

    goto :goto_0

    .line 36
    :cond_0
    iget-object v0, p0, Lloa;->x:Llnh;

    invoke-virtual {v0}, Llnh;->a()V

    .line 37
    iget-object v0, p0, Lloa;->y:Llqc;

    new-instance v1, Llob;

    invoke-direct {v1, p0, p1}, Llob;-><init>(Lloa;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Llqc;->a(Llqw;)Llqw;

    move-result-object v0

    iput-object v0, p0, Lloa;->e:Llqw;

    .line 48
    invoke-super {p0, p1}, Llrl;->onCreate(Landroid/os/Bundle;)V

    .line 49
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lloa;->y:Llqc;

    iget-object v1, p0, Lloa;->e:Llqw;

    invoke-virtual {v0, v1}, Llqc;->b(Llqw;)V

    .line 54
    invoke-super {p0}, Llrl;->onDestroy()V

    .line 55
    return-void
.end method
