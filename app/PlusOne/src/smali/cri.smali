.class public final Lcri;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lctm;


# instance fields
.field final a:Lcra;

.field private final b:I

.field private final c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcra;I)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcri;->c:Landroid/view/LayoutInflater;

    .line 36
    iput-object p2, p0, Lcri;->a:Lcra;

    .line 37
    iput p3, p0, Lcri;->b:I

    .line 38
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcri;->b:I

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 42
    if-nez p1, :cond_0

    .line 43
    iget-object v0, p0, Lcri;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f040056

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 46
    check-cast v0, Lcom/google/android/apps/photos/views/CollectionListHeaderView;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcrk;

    invoke-static {v2, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcrk;

    iget-object v3, p0, Lcri;->a:Lcra;

    invoke-virtual {v0, p0, v3, v2}, Lcom/google/android/apps/photos/views/CollectionListHeaderView;->a(Lctm;Lcra;Ldhp;)V

    new-instance v0, Lcrj;

    invoke-direct {v0, p0, v2}, Lcrj;-><init>(Lcri;Lcrk;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-object v1

    :cond_0
    move-object v1, p1

    goto :goto_0
.end method

.method public a(Lctn;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcri;->a:Lcra;

    invoke-interface {p1, v0}, Lctn;->a(Lcra;)V

    .line 74
    return-void
.end method
