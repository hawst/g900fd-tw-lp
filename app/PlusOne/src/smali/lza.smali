.class public final Llza;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnar;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26647
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26650
    iput-object v0, p0, Llza;->apiHeader:Llyq;

    .line 26653
    iput-object v0, p0, Llza;->a:Lnar;

    .line 26647
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26670
    const/4 v0, 0x0

    .line 26671
    iget-object v1, p0, Llza;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 26672
    const/4 v0, 0x1

    iget-object v1, p0, Llza;->apiHeader:Llyq;

    .line 26673
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26675
    :cond_0
    iget-object v1, p0, Llza;->a:Lnar;

    if-eqz v1, :cond_1

    .line 26676
    const/4 v1, 0x2

    iget-object v2, p0, Llza;->a:Lnar;

    .line 26677
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26679
    :cond_1
    iget-object v1, p0, Llza;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26680
    iput v0, p0, Llza;->ai:I

    .line 26681
    return v0
.end method

.method public a(Loxn;)Llza;
    .locals 2

    .prologue
    .line 26689
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26690
    sparse-switch v0, :sswitch_data_0

    .line 26694
    iget-object v1, p0, Llza;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26695
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llza;->ah:Ljava/util/List;

    .line 26698
    :cond_1
    iget-object v1, p0, Llza;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26700
    :sswitch_0
    return-object p0

    .line 26705
    :sswitch_1
    iget-object v0, p0, Llza;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 26706
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llza;->apiHeader:Llyq;

    .line 26708
    :cond_2
    iget-object v0, p0, Llza;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26712
    :sswitch_2
    iget-object v0, p0, Llza;->a:Lnar;

    if-nez v0, :cond_3

    .line 26713
    new-instance v0, Lnar;

    invoke-direct {v0}, Lnar;-><init>()V

    iput-object v0, p0, Llza;->a:Lnar;

    .line 26715
    :cond_3
    iget-object v0, p0, Llza;->a:Lnar;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26690
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26658
    iget-object v0, p0, Llza;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 26659
    const/4 v0, 0x1

    iget-object v1, p0, Llza;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26661
    :cond_0
    iget-object v0, p0, Llza;->a:Lnar;

    if-eqz v0, :cond_1

    .line 26662
    const/4 v0, 0x2

    iget-object v1, p0, Llza;->a:Lnar;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26664
    :cond_1
    iget-object v0, p0, Llza;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26666
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26643
    invoke-virtual {p0, p1}, Llza;->a(Loxn;)Llza;

    move-result-object v0

    return-object v0
.end method
