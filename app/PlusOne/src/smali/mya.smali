.class public final Lmya;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Llux;

.field public b:Lmyb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1645
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1648
    sget-object v0, Llux;->a:[Llux;

    iput-object v0, p0, Lmya;->a:[Llux;

    .line 1651
    const/4 v0, 0x0

    iput-object v0, p0, Lmya;->b:Lmyb;

    .line 1645
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1672
    .line 1673
    iget-object v1, p0, Lmya;->a:[Llux;

    if-eqz v1, :cond_1

    .line 1674
    iget-object v2, p0, Lmya;->a:[Llux;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1675
    if-eqz v4, :cond_0

    .line 1676
    const/4 v5, 0x1

    .line 1677
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1674
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1681
    :cond_1
    iget-object v1, p0, Lmya;->b:Lmyb;

    if-eqz v1, :cond_2

    .line 1682
    const/4 v1, 0x2

    iget-object v2, p0, Lmya;->b:Lmyb;

    .line 1683
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1685
    :cond_2
    iget-object v1, p0, Lmya;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1686
    iput v0, p0, Lmya;->ai:I

    .line 1687
    return v0
.end method

.method public a(Loxn;)Lmya;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1695
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1696
    sparse-switch v0, :sswitch_data_0

    .line 1700
    iget-object v2, p0, Lmya;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1701
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmya;->ah:Ljava/util/List;

    .line 1704
    :cond_1
    iget-object v2, p0, Lmya;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1706
    :sswitch_0
    return-object p0

    .line 1711
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1712
    iget-object v0, p0, Lmya;->a:[Llux;

    if-nez v0, :cond_3

    move v0, v1

    .line 1713
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llux;

    .line 1714
    iget-object v3, p0, Lmya;->a:[Llux;

    if-eqz v3, :cond_2

    .line 1715
    iget-object v3, p0, Lmya;->a:[Llux;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1717
    :cond_2
    iput-object v2, p0, Lmya;->a:[Llux;

    .line 1718
    :goto_2
    iget-object v2, p0, Lmya;->a:[Llux;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1719
    iget-object v2, p0, Lmya;->a:[Llux;

    new-instance v3, Llux;

    invoke-direct {v3}, Llux;-><init>()V

    aput-object v3, v2, v0

    .line 1720
    iget-object v2, p0, Lmya;->a:[Llux;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1721
    invoke-virtual {p1}, Loxn;->a()I

    .line 1718
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1712
    :cond_3
    iget-object v0, p0, Lmya;->a:[Llux;

    array-length v0, v0

    goto :goto_1

    .line 1724
    :cond_4
    iget-object v2, p0, Lmya;->a:[Llux;

    new-instance v3, Llux;

    invoke-direct {v3}, Llux;-><init>()V

    aput-object v3, v2, v0

    .line 1725
    iget-object v2, p0, Lmya;->a:[Llux;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1729
    :sswitch_2
    iget-object v0, p0, Lmya;->b:Lmyb;

    if-nez v0, :cond_5

    .line 1730
    new-instance v0, Lmyb;

    invoke-direct {v0}, Lmyb;-><init>()V

    iput-object v0, p0, Lmya;->b:Lmyb;

    .line 1732
    :cond_5
    iget-object v0, p0, Lmya;->b:Lmyb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1696
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1656
    iget-object v0, p0, Lmya;->a:[Llux;

    if-eqz v0, :cond_1

    .line 1657
    iget-object v1, p0, Lmya;->a:[Llux;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1658
    if-eqz v3, :cond_0

    .line 1659
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1657
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1663
    :cond_1
    iget-object v0, p0, Lmya;->b:Lmyb;

    if-eqz v0, :cond_2

    .line 1664
    const/4 v0, 0x2

    iget-object v1, p0, Lmya;->b:Lmyb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1666
    :cond_2
    iget-object v0, p0, Lmya;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1668
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1641
    invoke-virtual {p0, p1}, Lmya;->a(Loxn;)Lmya;

    move-result-object v0

    return-object v0
.end method
