.class public final Lmex;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnoh;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29482
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29485
    iput-object v0, p0, Lmex;->apiHeader:Llyr;

    .line 29488
    iput-object v0, p0, Lmex;->a:Lnoh;

    .line 29482
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29505
    const/4 v0, 0x0

    .line 29506
    iget-object v1, p0, Lmex;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 29507
    const/4 v0, 0x1

    iget-object v1, p0, Lmex;->apiHeader:Llyr;

    .line 29508
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29510
    :cond_0
    iget-object v1, p0, Lmex;->a:Lnoh;

    if-eqz v1, :cond_1

    .line 29511
    const/4 v1, 0x2

    iget-object v2, p0, Lmex;->a:Lnoh;

    .line 29512
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29514
    :cond_1
    iget-object v1, p0, Lmex;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29515
    iput v0, p0, Lmex;->ai:I

    .line 29516
    return v0
.end method

.method public a(Loxn;)Lmex;
    .locals 2

    .prologue
    .line 29524
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29525
    sparse-switch v0, :sswitch_data_0

    .line 29529
    iget-object v1, p0, Lmex;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29530
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmex;->ah:Ljava/util/List;

    .line 29533
    :cond_1
    iget-object v1, p0, Lmex;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29535
    :sswitch_0
    return-object p0

    .line 29540
    :sswitch_1
    iget-object v0, p0, Lmex;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 29541
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmex;->apiHeader:Llyr;

    .line 29543
    :cond_2
    iget-object v0, p0, Lmex;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29547
    :sswitch_2
    iget-object v0, p0, Lmex;->a:Lnoh;

    if-nez v0, :cond_3

    .line 29548
    new-instance v0, Lnoh;

    invoke-direct {v0}, Lnoh;-><init>()V

    iput-object v0, p0, Lmex;->a:Lnoh;

    .line 29550
    :cond_3
    iget-object v0, p0, Lmex;->a:Lnoh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29525
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29493
    iget-object v0, p0, Lmex;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 29494
    const/4 v0, 0x1

    iget-object v1, p0, Lmex;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29496
    :cond_0
    iget-object v0, p0, Lmex;->a:Lnoh;

    if-eqz v0, :cond_1

    .line 29497
    const/4 v0, 0x2

    iget-object v1, p0, Lmex;->a:Lnoh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29499
    :cond_1
    iget-object v0, p0, Lmex;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29501
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29478
    invoke-virtual {p0, p1}, Lmex;->a(Loxn;)Lmex;

    move-result-object v0

    return-object v0
.end method
