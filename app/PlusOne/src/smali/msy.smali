.class public final Lmsy;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmsy;


# instance fields
.field public b:I

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 997
    const/4 v0, 0x0

    new-array v0, v0, [Lmsy;

    sput-object v0, Lmsy;->a:[Lmsy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 998
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1007
    const/high16 v0, -0x80000000

    iput v0, p0, Lmsy;->b:I

    .line 998
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1030
    const/4 v0, 0x0

    .line 1031
    iget-object v1, p0, Lmsy;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1032
    const/4 v0, 0x1

    iget-object v1, p0, Lmsy;->c:Ljava/lang/Integer;

    .line 1033
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1035
    :cond_0
    iget-object v1, p0, Lmsy;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1036
    const/4 v1, 0x2

    iget-object v2, p0, Lmsy;->d:Ljava/lang/Integer;

    .line 1037
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1039
    :cond_1
    iget-object v1, p0, Lmsy;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1040
    const/4 v1, 0x3

    iget-object v2, p0, Lmsy;->e:Ljava/lang/Boolean;

    .line 1041
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1043
    :cond_2
    iget v1, p0, Lmsy;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 1044
    const/4 v1, 0x4

    iget v2, p0, Lmsy;->b:I

    .line 1045
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1047
    :cond_3
    iget-object v1, p0, Lmsy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1048
    iput v0, p0, Lmsy;->ai:I

    .line 1049
    return v0
.end method

.method public a(Loxn;)Lmsy;
    .locals 2

    .prologue
    .line 1057
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1058
    sparse-switch v0, :sswitch_data_0

    .line 1062
    iget-object v1, p0, Lmsy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1063
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmsy;->ah:Ljava/util/List;

    .line 1066
    :cond_1
    iget-object v1, p0, Lmsy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1068
    :sswitch_0
    return-object p0

    .line 1073
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsy;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1077
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsy;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 1081
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmsy;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 1085
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1086
    if-eqz v0, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x64

    if-eq v0, v1, :cond_2

    const/16 v1, 0x66

    if-eq v0, v1, :cond_2

    const/16 v1, 0x67

    if-eq v0, v1, :cond_2

    const/16 v1, 0x68

    if-eq v0, v1, :cond_2

    const/16 v1, 0x69

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x78

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x77

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x86

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x87

    if-eq v0, v1, :cond_2

    const/16 v1, 0x79

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x80

    if-eq v0, v1, :cond_2

    const/16 v1, 0x81

    if-eq v0, v1, :cond_2

    const/16 v1, 0x82

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83

    if-eq v0, v1, :cond_2

    const/16 v1, 0x89

    if-eq v0, v1, :cond_2

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x70

    if-eq v0, v1, :cond_2

    const/16 v1, 0x71

    if-eq v0, v1, :cond_2

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_2

    const/16 v1, 0x75

    if-eq v0, v1, :cond_2

    const/16 v1, 0x76

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x85

    if-eq v0, v1, :cond_2

    const/16 v1, 0x88

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x92

    if-eq v0, v1, :cond_2

    const/16 v1, 0x93

    if-eq v0, v1, :cond_2

    const/16 v1, 0x95

    if-eq v0, v1, :cond_2

    const/16 v1, 0x97

    if-eq v0, v1, :cond_2

    const/16 v1, 0x98

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x90

    if-eq v0, v1, :cond_2

    const/16 v1, 0x91

    if-eq v0, v1, :cond_2

    const/16 v1, 0x99

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x94

    if-eq v0, v1, :cond_2

    const/16 v1, 0x96

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa4

    if-ne v0, v1, :cond_3

    .line 1156
    :cond_2
    iput v0, p0, Lmsy;->b:I

    goto/16 :goto_0

    .line 1158
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmsy;->b:I

    goto/16 :goto_0

    .line 1058
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1012
    iget-object v0, p0, Lmsy;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1013
    const/4 v0, 0x1

    iget-object v1, p0, Lmsy;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1015
    :cond_0
    iget-object v0, p0, Lmsy;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1016
    const/4 v0, 0x2

    iget-object v1, p0, Lmsy;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1018
    :cond_1
    iget-object v0, p0, Lmsy;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1019
    const/4 v0, 0x3

    iget-object v1, p0, Lmsy;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1021
    :cond_2
    iget v0, p0, Lmsy;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 1022
    const/4 v0, 0x4

    iget v1, p0, Lmsy;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1024
    :cond_3
    iget-object v0, p0, Lmsy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1026
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 994
    invoke-virtual {p0, p1}, Lmsy;->a(Loxn;)Lmsy;

    move-result-object v0

    return-object v0
.end method
