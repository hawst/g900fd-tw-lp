.class public final Lmfz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmyy;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2752
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2755
    iput-object v0, p0, Lmfz;->apiHeader:Llyr;

    .line 2758
    iput-object v0, p0, Lmfz;->a:Lmyy;

    .line 2752
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2775
    const/4 v0, 0x0

    .line 2776
    iget-object v1, p0, Lmfz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 2777
    const/4 v0, 0x1

    iget-object v1, p0, Lmfz;->apiHeader:Llyr;

    .line 2778
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2780
    :cond_0
    iget-object v1, p0, Lmfz;->a:Lmyy;

    if-eqz v1, :cond_1

    .line 2781
    const/4 v1, 0x2

    iget-object v2, p0, Lmfz;->a:Lmyy;

    .line 2782
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2784
    :cond_1
    iget-object v1, p0, Lmfz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2785
    iput v0, p0, Lmfz;->ai:I

    .line 2786
    return v0
.end method

.method public a(Loxn;)Lmfz;
    .locals 2

    .prologue
    .line 2794
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2795
    sparse-switch v0, :sswitch_data_0

    .line 2799
    iget-object v1, p0, Lmfz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2800
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfz;->ah:Ljava/util/List;

    .line 2803
    :cond_1
    iget-object v1, p0, Lmfz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2805
    :sswitch_0
    return-object p0

    .line 2810
    :sswitch_1
    iget-object v0, p0, Lmfz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 2811
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfz;->apiHeader:Llyr;

    .line 2813
    :cond_2
    iget-object v0, p0, Lmfz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2817
    :sswitch_2
    iget-object v0, p0, Lmfz;->a:Lmyy;

    if-nez v0, :cond_3

    .line 2818
    new-instance v0, Lmyy;

    invoke-direct {v0}, Lmyy;-><init>()V

    iput-object v0, p0, Lmfz;->a:Lmyy;

    .line 2820
    :cond_3
    iget-object v0, p0, Lmfz;->a:Lmyy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2795
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2763
    iget-object v0, p0, Lmfz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 2764
    const/4 v0, 0x1

    iget-object v1, p0, Lmfz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2766
    :cond_0
    iget-object v0, p0, Lmfz;->a:Lmyy;

    if-eqz v0, :cond_1

    .line 2767
    const/4 v0, 0x2

    iget-object v1, p0, Lmfz;->a:Lmyy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2769
    :cond_1
    iget-object v0, p0, Lmfz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2771
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2748
    invoke-virtual {p0, p1}, Lmfz;->a(Loxn;)Lmfz;

    move-result-object v0

    return-object v0
.end method
