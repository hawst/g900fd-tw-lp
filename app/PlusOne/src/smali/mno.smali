.class public final Lmno;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmoc;

.field public b:Lmne;

.field public c:Lmne;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3925
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3928
    iput-object v0, p0, Lmno;->a:Lmoc;

    .line 3931
    iput-object v0, p0, Lmno;->b:Lmne;

    .line 3934
    iput-object v0, p0, Lmno;->c:Lmne;

    .line 3925
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3954
    const/4 v0, 0x0

    .line 3955
    iget-object v1, p0, Lmno;->a:Lmoc;

    if-eqz v1, :cond_0

    .line 3956
    const/4 v0, 0x1

    iget-object v1, p0, Lmno;->a:Lmoc;

    .line 3957
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3959
    :cond_0
    iget-object v1, p0, Lmno;->b:Lmne;

    if-eqz v1, :cond_1

    .line 3960
    const/4 v1, 0x2

    iget-object v2, p0, Lmno;->b:Lmne;

    .line 3961
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3963
    :cond_1
    iget-object v1, p0, Lmno;->c:Lmne;

    if-eqz v1, :cond_2

    .line 3964
    const/4 v1, 0x3

    iget-object v2, p0, Lmno;->c:Lmne;

    .line 3965
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3967
    :cond_2
    iget-object v1, p0, Lmno;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3968
    iput v0, p0, Lmno;->ai:I

    .line 3969
    return v0
.end method

.method public a(Loxn;)Lmno;
    .locals 2

    .prologue
    .line 3977
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3978
    sparse-switch v0, :sswitch_data_0

    .line 3982
    iget-object v1, p0, Lmno;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3983
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmno;->ah:Ljava/util/List;

    .line 3986
    :cond_1
    iget-object v1, p0, Lmno;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3988
    :sswitch_0
    return-object p0

    .line 3993
    :sswitch_1
    iget-object v0, p0, Lmno;->a:Lmoc;

    if-nez v0, :cond_2

    .line 3994
    new-instance v0, Lmoc;

    invoke-direct {v0}, Lmoc;-><init>()V

    iput-object v0, p0, Lmno;->a:Lmoc;

    .line 3996
    :cond_2
    iget-object v0, p0, Lmno;->a:Lmoc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4000
    :sswitch_2
    iget-object v0, p0, Lmno;->b:Lmne;

    if-nez v0, :cond_3

    .line 4001
    new-instance v0, Lmne;

    invoke-direct {v0}, Lmne;-><init>()V

    iput-object v0, p0, Lmno;->b:Lmne;

    .line 4003
    :cond_3
    iget-object v0, p0, Lmno;->b:Lmne;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4007
    :sswitch_3
    iget-object v0, p0, Lmno;->c:Lmne;

    if-nez v0, :cond_4

    .line 4008
    new-instance v0, Lmne;

    invoke-direct {v0}, Lmne;-><init>()V

    iput-object v0, p0, Lmno;->c:Lmne;

    .line 4010
    :cond_4
    iget-object v0, p0, Lmno;->c:Lmne;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3978
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3939
    iget-object v0, p0, Lmno;->a:Lmoc;

    if-eqz v0, :cond_0

    .line 3940
    const/4 v0, 0x1

    iget-object v1, p0, Lmno;->a:Lmoc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3942
    :cond_0
    iget-object v0, p0, Lmno;->b:Lmne;

    if-eqz v0, :cond_1

    .line 3943
    const/4 v0, 0x2

    iget-object v1, p0, Lmno;->b:Lmne;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3945
    :cond_1
    iget-object v0, p0, Lmno;->c:Lmne;

    if-eqz v0, :cond_2

    .line 3946
    const/4 v0, 0x3

    iget-object v1, p0, Lmno;->c:Lmne;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3948
    :cond_2
    iget-object v0, p0, Lmno;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3950
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3921
    invoke-virtual {p0, p1}, Lmno;->a(Loxn;)Lmno;

    move-result-object v0

    return-object v0
.end method
