.class public final Lpci;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:[Ljava/lang/String;

.field private f:[Lpcn;

.field private g:[Loyr;

.field private h:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpci;->e:[Ljava/lang/String;

    .line 23
    sget-object v0, Lpcn;->a:[Lpcn;

    iput-object v0, p0, Lpci;->f:[Lpcn;

    .line 26
    sget-object v0, Loyr;->a:[Loyr;

    iput-object v0, p0, Lpci;->g:[Loyr;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 73
    .line 74
    iget-object v0, p0, Lpci;->a:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 75
    const/4 v0, 0x1

    iget-object v2, p0, Lpci;->a:Ljava/lang/Long;

    .line 76
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 78
    :goto_0
    iget-object v2, p0, Lpci;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 79
    const/4 v2, 0x2

    iget-object v3, p0, Lpci;->b:Ljava/lang/String;

    .line 80
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_0
    iget-object v2, p0, Lpci;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 83
    const/4 v2, 0x3

    iget-object v3, p0, Lpci;->c:Ljava/lang/String;

    .line 84
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    :cond_1
    iget-object v2, p0, Lpci;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 87
    const/4 v2, 0x4

    iget-object v3, p0, Lpci;->d:Ljava/lang/String;

    .line 88
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 90
    :cond_2
    iget-object v2, p0, Lpci;->e:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lpci;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 92
    iget-object v4, p0, Lpci;->e:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 94
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 92
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 96
    :cond_3
    add-int/2addr v0, v3

    .line 97
    iget-object v2, p0, Lpci;->e:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 99
    :cond_4
    iget-object v2, p0, Lpci;->f:[Lpcn;

    if-eqz v2, :cond_6

    .line 100
    iget-object v3, p0, Lpci;->f:[Lpcn;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 101
    if-eqz v5, :cond_5

    .line 102
    const/4 v6, 0x6

    .line 103
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 100
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 107
    :cond_6
    iget-object v2, p0, Lpci;->g:[Loyr;

    if-eqz v2, :cond_8

    .line 108
    iget-object v2, p0, Lpci;->g:[Loyr;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 109
    if-eqz v4, :cond_7

    .line 110
    const/4 v5, 0x7

    .line 111
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 108
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 115
    :cond_8
    iget-object v1, p0, Lpci;->h:[B

    if-eqz v1, :cond_9

    .line 116
    const/16 v1, 0x8

    iget-object v2, p0, Lpci;->h:[B

    .line 117
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_9
    iget-object v1, p0, Lpci;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    iput v0, p0, Lpci;->ai:I

    .line 121
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpci;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 129
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 130
    sparse-switch v0, :sswitch_data_0

    .line 134
    iget-object v2, p0, Lpci;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 135
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpci;->ah:Ljava/util/List;

    .line 138
    :cond_1
    iget-object v2, p0, Lpci;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    :sswitch_0
    return-object p0

    .line 145
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpci;->a:Ljava/lang/Long;

    goto :goto_0

    .line 149
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpci;->b:Ljava/lang/String;

    goto :goto_0

    .line 153
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpci;->c:Ljava/lang/String;

    goto :goto_0

    .line 157
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpci;->d:Ljava/lang/String;

    goto :goto_0

    .line 161
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 162
    iget-object v0, p0, Lpci;->e:[Ljava/lang/String;

    array-length v0, v0

    .line 163
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 164
    iget-object v3, p0, Lpci;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    iput-object v2, p0, Lpci;->e:[Ljava/lang/String;

    .line 166
    :goto_1
    iget-object v2, p0, Lpci;->e:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 167
    iget-object v2, p0, Lpci;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 168
    invoke-virtual {p1}, Loxn;->a()I

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 171
    :cond_2
    iget-object v2, p0, Lpci;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 175
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 176
    iget-object v0, p0, Lpci;->f:[Lpcn;

    if-nez v0, :cond_4

    move v0, v1

    .line 177
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lpcn;

    .line 178
    iget-object v3, p0, Lpci;->f:[Lpcn;

    if-eqz v3, :cond_3

    .line 179
    iget-object v3, p0, Lpci;->f:[Lpcn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 181
    :cond_3
    iput-object v2, p0, Lpci;->f:[Lpcn;

    .line 182
    :goto_3
    iget-object v2, p0, Lpci;->f:[Lpcn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 183
    iget-object v2, p0, Lpci;->f:[Lpcn;

    new-instance v3, Lpcn;

    invoke-direct {v3}, Lpcn;-><init>()V

    aput-object v3, v2, v0

    .line 184
    iget-object v2, p0, Lpci;->f:[Lpcn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 185
    invoke-virtual {p1}, Loxn;->a()I

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 176
    :cond_4
    iget-object v0, p0, Lpci;->f:[Lpcn;

    array-length v0, v0

    goto :goto_2

    .line 188
    :cond_5
    iget-object v2, p0, Lpci;->f:[Lpcn;

    new-instance v3, Lpcn;

    invoke-direct {v3}, Lpcn;-><init>()V

    aput-object v3, v2, v0

    .line 189
    iget-object v2, p0, Lpci;->f:[Lpcn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 193
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 194
    iget-object v0, p0, Lpci;->g:[Loyr;

    if-nez v0, :cond_7

    move v0, v1

    .line 195
    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Loyr;

    .line 196
    iget-object v3, p0, Lpci;->g:[Loyr;

    if-eqz v3, :cond_6

    .line 197
    iget-object v3, p0, Lpci;->g:[Loyr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 199
    :cond_6
    iput-object v2, p0, Lpci;->g:[Loyr;

    .line 200
    :goto_5
    iget-object v2, p0, Lpci;->g:[Loyr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 201
    iget-object v2, p0, Lpci;->g:[Loyr;

    new-instance v3, Loyr;

    invoke-direct {v3}, Loyr;-><init>()V

    aput-object v3, v2, v0

    .line 202
    iget-object v2, p0, Lpci;->g:[Loyr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 203
    invoke-virtual {p1}, Loxn;->a()I

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 194
    :cond_7
    iget-object v0, p0, Lpci;->g:[Loyr;

    array-length v0, v0

    goto :goto_4

    .line 206
    :cond_8
    iget-object v2, p0, Lpci;->g:[Loyr;

    new-instance v3, Loyr;

    invoke-direct {v3}, Loyr;-><init>()V

    aput-object v3, v2, v0

    .line 207
    iget-object v2, p0, Lpci;->g:[Loyr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 211
    :sswitch_8
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpci;->h:[B

    goto/16 :goto_0

    .line 130
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-object v1, p0, Lpci;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 34
    const/4 v1, 0x1

    iget-object v2, p0, Lpci;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(IJ)V

    .line 36
    :cond_0
    iget-object v1, p0, Lpci;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 37
    const/4 v1, 0x2

    iget-object v2, p0, Lpci;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 39
    :cond_1
    iget-object v1, p0, Lpci;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 40
    const/4 v1, 0x3

    iget-object v2, p0, Lpci;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 42
    :cond_2
    iget-object v1, p0, Lpci;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 43
    const/4 v1, 0x4

    iget-object v2, p0, Lpci;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 45
    :cond_3
    iget-object v1, p0, Lpci;->e:[Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 46
    iget-object v2, p0, Lpci;->e:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 47
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    :cond_4
    iget-object v1, p0, Lpci;->f:[Lpcn;

    if-eqz v1, :cond_6

    .line 51
    iget-object v2, p0, Lpci;->f:[Lpcn;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 52
    if-eqz v4, :cond_5

    .line 53
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 51
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 57
    :cond_6
    iget-object v1, p0, Lpci;->g:[Loyr;

    if-eqz v1, :cond_8

    .line 58
    iget-object v1, p0, Lpci;->g:[Loyr;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 59
    if-eqz v3, :cond_7

    .line 60
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 58
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 64
    :cond_8
    iget-object v0, p0, Lpci;->h:[B

    if-eqz v0, :cond_9

    .line 65
    const/16 v0, 0x8

    iget-object v1, p0, Lpci;->h:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 67
    :cond_9
    iget-object v0, p0, Lpci;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 69
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lpci;->a(Loxn;)Lpci;

    move-result-object v0

    return-object v0
.end method
