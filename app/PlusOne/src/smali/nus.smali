.class public final Lnus;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lpyk;

.field private b:I

.field private c:Lpes;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22
    sget-object v0, Lpyk;->a:[Lpyk;

    iput-object v0, p0, Lnus;->a:[Lpyk;

    .line 25
    const/high16 v0, -0x80000000

    iput v0, p0, Lnus;->b:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lnus;->c:Lpes;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 52
    .line 53
    iget-object v1, p0, Lnus;->a:[Lpyk;

    if-eqz v1, :cond_1

    .line 54
    iget-object v2, p0, Lnus;->a:[Lpyk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 55
    if-eqz v4, :cond_0

    .line 56
    const/4 v5, 0x1

    .line 57
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 54
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    :cond_1
    iget v1, p0, Lnus;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 62
    const/4 v1, 0x2

    iget v2, p0, Lnus;->b:I

    .line 63
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_2
    iget-object v1, p0, Lnus;->c:Lpes;

    if-eqz v1, :cond_3

    .line 66
    const/4 v1, 0x3

    iget-object v2, p0, Lnus;->c:Lpes;

    .line 67
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_3
    iget-object v1, p0, Lnus;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    iput v0, p0, Lnus;->ai:I

    .line 71
    return v0
.end method

.method public a(Loxn;)Lnus;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 80
    sparse-switch v0, :sswitch_data_0

    .line 84
    iget-object v2, p0, Lnus;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 85
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnus;->ah:Ljava/util/List;

    .line 88
    :cond_1
    iget-object v2, p0, Lnus;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    :sswitch_0
    return-object p0

    .line 95
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 96
    iget-object v0, p0, Lnus;->a:[Lpyk;

    if-nez v0, :cond_3

    move v0, v1

    .line 97
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpyk;

    .line 98
    iget-object v3, p0, Lnus;->a:[Lpyk;

    if-eqz v3, :cond_2

    .line 99
    iget-object v3, p0, Lnus;->a:[Lpyk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    :cond_2
    iput-object v2, p0, Lnus;->a:[Lpyk;

    .line 102
    :goto_2
    iget-object v2, p0, Lnus;->a:[Lpyk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 103
    iget-object v2, p0, Lnus;->a:[Lpyk;

    new-instance v3, Lpyk;

    invoke-direct {v3}, Lpyk;-><init>()V

    aput-object v3, v2, v0

    .line 104
    iget-object v2, p0, Lnus;->a:[Lpyk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 105
    invoke-virtual {p1}, Loxn;->a()I

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 96
    :cond_3
    iget-object v0, p0, Lnus;->a:[Lpyk;

    array-length v0, v0

    goto :goto_1

    .line 108
    :cond_4
    iget-object v2, p0, Lnus;->a:[Lpyk;

    new-instance v3, Lpyk;

    invoke-direct {v3}, Lpyk;-><init>()V

    aput-object v3, v2, v0

    .line 109
    iget-object v2, p0, Lnus;->a:[Lpyk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 113
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 114
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-ne v0, v2, :cond_6

    .line 120
    :cond_5
    iput v0, p0, Lnus;->b:I

    goto :goto_0

    .line 122
    :cond_6
    iput v1, p0, Lnus;->b:I

    goto :goto_0

    .line 127
    :sswitch_3
    iget-object v0, p0, Lnus;->c:Lpes;

    if-nez v0, :cond_7

    .line 128
    new-instance v0, Lpes;

    invoke-direct {v0}, Lpes;-><init>()V

    iput-object v0, p0, Lnus;->c:Lpes;

    .line 130
    :cond_7
    iget-object v0, p0, Lnus;->c:Lpes;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 80
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lnus;->a:[Lpyk;

    if-eqz v0, :cond_1

    .line 34
    iget-object v1, p0, Lnus;->a:[Lpyk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 35
    if-eqz v3, :cond_0

    .line 36
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 34
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_1
    iget v0, p0, Lnus;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 41
    const/4 v0, 0x2

    iget v1, p0, Lnus;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 43
    :cond_2
    iget-object v0, p0, Lnus;->c:Lpes;

    if-eqz v0, :cond_3

    .line 44
    const/4 v0, 0x3

    iget-object v1, p0, Lnus;->c:Lpes;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 46
    :cond_3
    iget-object v0, p0, Lnus;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 48
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnus;->a(Loxn;)Lnus;

    move-result-object v0

    return-object v0
.end method
