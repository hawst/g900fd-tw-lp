.class public final Lmyd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyk;

.field public b:Lmyc;

.field public c:Lmyb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1304
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1307
    iput-object v0, p0, Lmyd;->a:Lmyk;

    .line 1310
    iput-object v0, p0, Lmyd;->b:Lmyc;

    .line 1313
    iput-object v0, p0, Lmyd;->c:Lmyb;

    .line 1304
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1333
    const/4 v0, 0x0

    .line 1334
    iget-object v1, p0, Lmyd;->a:Lmyk;

    if-eqz v1, :cond_0

    .line 1335
    const/4 v0, 0x1

    iget-object v1, p0, Lmyd;->a:Lmyk;

    .line 1336
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1338
    :cond_0
    iget-object v1, p0, Lmyd;->b:Lmyc;

    if-eqz v1, :cond_1

    .line 1339
    const/4 v1, 0x2

    iget-object v2, p0, Lmyd;->b:Lmyc;

    .line 1340
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1342
    :cond_1
    iget-object v1, p0, Lmyd;->c:Lmyb;

    if-eqz v1, :cond_2

    .line 1343
    const/4 v1, 0x3

    iget-object v2, p0, Lmyd;->c:Lmyb;

    .line 1344
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1346
    :cond_2
    iget-object v1, p0, Lmyd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1347
    iput v0, p0, Lmyd;->ai:I

    .line 1348
    return v0
.end method

.method public a(Loxn;)Lmyd;
    .locals 2

    .prologue
    .line 1356
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1357
    sparse-switch v0, :sswitch_data_0

    .line 1361
    iget-object v1, p0, Lmyd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1362
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyd;->ah:Ljava/util/List;

    .line 1365
    :cond_1
    iget-object v1, p0, Lmyd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1367
    :sswitch_0
    return-object p0

    .line 1372
    :sswitch_1
    iget-object v0, p0, Lmyd;->a:Lmyk;

    if-nez v0, :cond_2

    .line 1373
    new-instance v0, Lmyk;

    invoke-direct {v0}, Lmyk;-><init>()V

    iput-object v0, p0, Lmyd;->a:Lmyk;

    .line 1375
    :cond_2
    iget-object v0, p0, Lmyd;->a:Lmyk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1379
    :sswitch_2
    iget-object v0, p0, Lmyd;->b:Lmyc;

    if-nez v0, :cond_3

    .line 1380
    new-instance v0, Lmyc;

    invoke-direct {v0}, Lmyc;-><init>()V

    iput-object v0, p0, Lmyd;->b:Lmyc;

    .line 1382
    :cond_3
    iget-object v0, p0, Lmyd;->b:Lmyc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1386
    :sswitch_3
    iget-object v0, p0, Lmyd;->c:Lmyb;

    if-nez v0, :cond_4

    .line 1387
    new-instance v0, Lmyb;

    invoke-direct {v0}, Lmyb;-><init>()V

    iput-object v0, p0, Lmyd;->c:Lmyb;

    .line 1389
    :cond_4
    iget-object v0, p0, Lmyd;->c:Lmyb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1357
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1318
    iget-object v0, p0, Lmyd;->a:Lmyk;

    if-eqz v0, :cond_0

    .line 1319
    const/4 v0, 0x1

    iget-object v1, p0, Lmyd;->a:Lmyk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1321
    :cond_0
    iget-object v0, p0, Lmyd;->b:Lmyc;

    if-eqz v0, :cond_1

    .line 1322
    const/4 v0, 0x2

    iget-object v1, p0, Lmyd;->b:Lmyc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1324
    :cond_1
    iget-object v0, p0, Lmyd;->c:Lmyb;

    if-eqz v0, :cond_2

    .line 1325
    const/4 v0, 0x3

    iget-object v1, p0, Lmyd;->c:Lmyb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1327
    :cond_2
    iget-object v0, p0, Lmyd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1329
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1300
    invoke-virtual {p0, p1}, Lmyd;->a(Loxn;)Lmyd;

    move-result-object v0

    return-object v0
.end method
