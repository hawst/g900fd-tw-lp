.class public final Lnrv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lnrv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:[Lnru;

.field private c:[Lnsr;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2614
    const v0, 0x2ac95ff

    new-instance v1, Lnrw;

    invoke-direct {v1}, Lnrw;-><init>()V

    .line 2619
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lnrv;->a:Loxr;

    .line 2618
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2615
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2622
    sget-object v0, Lnsr;->a:[Lnsr;

    iput-object v0, p0, Lnrv;->c:[Lnsr;

    .line 2625
    sget-object v0, Lnru;->a:[Lnru;

    iput-object v0, p0, Lnrv;->b:[Lnru;

    .line 2615
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2650
    .line 2651
    iget-object v0, p0, Lnrv;->c:[Lnsr;

    if-eqz v0, :cond_1

    .line 2652
    iget-object v3, p0, Lnrv;->c:[Lnsr;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2653
    if-eqz v5, :cond_0

    .line 2654
    const/4 v6, 0x1

    .line 2655
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2652
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2659
    :cond_2
    iget-object v2, p0, Lnrv;->b:[Lnru;

    if-eqz v2, :cond_4

    .line 2660
    iget-object v2, p0, Lnrv;->b:[Lnru;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 2661
    if-eqz v4, :cond_3

    .line 2662
    const/4 v5, 0x2

    .line 2663
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2660
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2667
    :cond_4
    iget-object v1, p0, Lnrv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2668
    iput v0, p0, Lnrv;->ai:I

    .line 2669
    return v0
.end method

.method public a(Loxn;)Lnrv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2677
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2678
    sparse-switch v0, :sswitch_data_0

    .line 2682
    iget-object v2, p0, Lnrv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2683
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnrv;->ah:Ljava/util/List;

    .line 2686
    :cond_1
    iget-object v2, p0, Lnrv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2688
    :sswitch_0
    return-object p0

    .line 2693
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2694
    iget-object v0, p0, Lnrv;->c:[Lnsr;

    if-nez v0, :cond_3

    move v0, v1

    .line 2695
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsr;

    .line 2696
    iget-object v3, p0, Lnrv;->c:[Lnsr;

    if-eqz v3, :cond_2

    .line 2697
    iget-object v3, p0, Lnrv;->c:[Lnsr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2699
    :cond_2
    iput-object v2, p0, Lnrv;->c:[Lnsr;

    .line 2700
    :goto_2
    iget-object v2, p0, Lnrv;->c:[Lnsr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2701
    iget-object v2, p0, Lnrv;->c:[Lnsr;

    new-instance v3, Lnsr;

    invoke-direct {v3}, Lnsr;-><init>()V

    aput-object v3, v2, v0

    .line 2702
    iget-object v2, p0, Lnrv;->c:[Lnsr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2703
    invoke-virtual {p1}, Loxn;->a()I

    .line 2700
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2694
    :cond_3
    iget-object v0, p0, Lnrv;->c:[Lnsr;

    array-length v0, v0

    goto :goto_1

    .line 2706
    :cond_4
    iget-object v2, p0, Lnrv;->c:[Lnsr;

    new-instance v3, Lnsr;

    invoke-direct {v3}, Lnsr;-><init>()V

    aput-object v3, v2, v0

    .line 2707
    iget-object v2, p0, Lnrv;->c:[Lnsr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2711
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2712
    iget-object v0, p0, Lnrv;->b:[Lnru;

    if-nez v0, :cond_6

    move v0, v1

    .line 2713
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnru;

    .line 2714
    iget-object v3, p0, Lnrv;->b:[Lnru;

    if-eqz v3, :cond_5

    .line 2715
    iget-object v3, p0, Lnrv;->b:[Lnru;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2717
    :cond_5
    iput-object v2, p0, Lnrv;->b:[Lnru;

    .line 2718
    :goto_4
    iget-object v2, p0, Lnrv;->b:[Lnru;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 2719
    iget-object v2, p0, Lnrv;->b:[Lnru;

    new-instance v3, Lnru;

    invoke-direct {v3}, Lnru;-><init>()V

    aput-object v3, v2, v0

    .line 2720
    iget-object v2, p0, Lnrv;->b:[Lnru;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2721
    invoke-virtual {p1}, Loxn;->a()I

    .line 2718
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2712
    :cond_6
    iget-object v0, p0, Lnrv;->b:[Lnru;

    array-length v0, v0

    goto :goto_3

    .line 2724
    :cond_7
    iget-object v2, p0, Lnrv;->b:[Lnru;

    new-instance v3, Lnru;

    invoke-direct {v3}, Lnru;-><init>()V

    aput-object v3, v2, v0

    .line 2725
    iget-object v2, p0, Lnrv;->b:[Lnru;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2678
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2630
    iget-object v1, p0, Lnrv;->c:[Lnsr;

    if-eqz v1, :cond_1

    .line 2631
    iget-object v2, p0, Lnrv;->c:[Lnsr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2632
    if-eqz v4, :cond_0

    .line 2633
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2631
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2637
    :cond_1
    iget-object v1, p0, Lnrv;->b:[Lnru;

    if-eqz v1, :cond_3

    .line 2638
    iget-object v1, p0, Lnrv;->b:[Lnru;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 2639
    if-eqz v3, :cond_2

    .line 2640
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2638
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2644
    :cond_3
    iget-object v0, p0, Lnrv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2646
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2611
    invoke-virtual {p0, p1}, Lnrv;->a(Loxn;)Lnrv;

    move-result-object v0

    return-object v0
.end method
