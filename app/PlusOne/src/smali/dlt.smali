.class public final Ldlt;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmfs;",
        "Lmft;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llui;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ldsl;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Ldsl;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Llui;",
            ">;",
            "Ldsl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    const-string v3, "notificationsack"

    new-instance v4, Lmfs;

    invoke-direct {v4}, Lmfs;-><init>()V

    new-instance v5, Lmft;

    invoke-direct {v5}, Lmft;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 31
    iput-object p3, p0, Ldlt;->a:Ljava/util/ArrayList;

    .line 32
    iput-object p4, p0, Ldlt;->b:Ldsl;

    .line 33
    return-void
.end method


# virtual methods
.method protected a(Lmfs;)V
    .locals 5

    .prologue
    .line 37
    new-instance v1, Lmya;

    invoke-direct {v1}, Lmya;-><init>()V

    .line 41
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 42
    iget-object v0, p0, Ldlt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llui;

    .line 43
    iget-object v4, v0, Llui;->k:Llux;

    if-eqz v4, :cond_0

    .line 44
    iget-object v0, v0, Llui;->k:Llux;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Llux;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llux;

    iput-object v0, v1, Lmya;->a:[Llux;

    .line 51
    new-instance v2, Lmyb;

    invoke-direct {v2}, Lmyb;-><init>()V

    .line 52
    iget-object v0, p0, Ldlt;->b:Ldsl;

    sget-object v3, Ldlu;->a:[I

    invoke-virtual {v0}, Ldsl;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x1

    :goto_1
    iput v0, v2, Lmyb;->a:I

    .line 53
    iput-object v2, v1, Lmya;->b:Lmyb;

    .line 55
    iput-object v1, p1, Lmfs;->a:Lmya;

    .line 56
    return-void

    .line 52
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lmfs;

    invoke-virtual {p0, p1}, Ldlt;->a(Lmfs;)V

    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 19
    return-void
.end method
