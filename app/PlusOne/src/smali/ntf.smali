.class public final Lntf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1013
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1034
    const/4 v0, 0x0

    .line 1035
    iget-object v1, p0, Lntf;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1036
    const/4 v0, 0x1

    iget-object v1, p0, Lntf;->a:Ljava/lang/String;

    .line 1037
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1039
    :cond_0
    iget-object v1, p0, Lntf;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1040
    const/4 v1, 0x2

    iget-object v2, p0, Lntf;->b:Ljava/lang/Boolean;

    .line 1041
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1043
    :cond_1
    iget-object v1, p0, Lntf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1044
    iput v0, p0, Lntf;->ai:I

    .line 1045
    return v0
.end method

.method public a(Loxn;)Lntf;
    .locals 2

    .prologue
    .line 1053
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1054
    sparse-switch v0, :sswitch_data_0

    .line 1058
    iget-object v1, p0, Lntf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1059
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lntf;->ah:Ljava/util/List;

    .line 1062
    :cond_1
    iget-object v1, p0, Lntf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1064
    :sswitch_0
    return-object p0

    .line 1069
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lntf;->a:Ljava/lang/String;

    goto :goto_0

    .line 1073
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lntf;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1054
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1022
    iget-object v0, p0, Lntf;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1023
    const/4 v0, 0x1

    iget-object v1, p0, Lntf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1025
    :cond_0
    iget-object v0, p0, Lntf;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1026
    const/4 v0, 0x2

    iget-object v1, p0, Lntf;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1028
    :cond_1
    iget-object v0, p0, Lntf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1030
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1009
    invoke-virtual {p0, p1}, Lntf;->a(Loxn;)Lntf;

    move-result-object v0

    return-object v0
.end method
