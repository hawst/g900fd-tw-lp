.class public Lify;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lifx;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[F

.field private static final z:Ligd;


# instance fields
.field private c:[F

.field private d:[F

.field private e:Ligk;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:[F

.field private k:I

.field private l:I

.field private m:I

.field private n:[Liga;

.field private o:[Liga;

.field private p:[Liga;

.field private q:[Liga;

.field private final r:Ligk;

.field private s:I

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lige;",
            ">;"
        }
    .end annotation
.end field

.field private final u:[F

.field private final v:Landroid/graphics/RectF;

.field private final w:Landroid/graphics/RectF;

.field private final x:[F

.field private final y:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 40
    const-class v0, Lify;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lify;->a:Ljava/lang/String;

    .line 54
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lify;->b:[F

    .line 67
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v2, v0, v1

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v3, v0, v1

    const/4 v1, 0x4

    aput v3, v0, v1

    const/4 v1, 0x5

    aput v3, v0, v1

    const/4 v1, 0x6

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v3, v0, v1

    .line 270
    new-instance v0, Ligc;

    invoke-direct {v0}, Ligc;-><init>()V

    sput-object v0, Lify;->z:Ligd;

    return-void

    .line 54
    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    const/16 v0, 0x80

    new-array v0, v0, [F

    iput-object v0, p0, Lify;->c:[F

    .line 143
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lify;->d:[F

    .line 144
    new-instance v0, Ligk;

    invoke-direct {v0}, Ligk;-><init>()V

    iput-object v0, p0, Lify;->e:Ligk;

    .line 146
    iput v3, p0, Lify;->f:I

    .line 147
    iput v3, p0, Lify;->g:I

    .line 154
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lify;->j:[F

    .line 219
    new-array v0, v5, [Liga;

    new-instance v1, Lifz;

    const-string v2, "aPosition"

    invoke-direct {v1, v2}, Lifz;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Ligb;

    const-string v2, "uMatrix"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Ligb;

    const-string v2, "uColor"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    iput-object v0, p0, Lify;->n:[Liga;

    .line 224
    const/4 v0, 0x5

    new-array v0, v0, [Liga;

    new-instance v1, Lifz;

    const-string v2, "aPosition"

    invoke-direct {v1, v2}, Lifz;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Ligb;

    const-string v2, "uMatrix"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Ligb;

    const-string v2, "uTextureMatrix"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Ligb;

    const-string v2, "uTextureSampler"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Ligb;

    const-string v2, "uAlpha"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    iput-object v0, p0, Lify;->o:[Liga;

    .line 231
    const/4 v0, 0x5

    new-array v0, v0, [Liga;

    new-instance v1, Lifz;

    const-string v2, "aPosition"

    invoke-direct {v1, v2}, Lifz;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Ligb;

    const-string v2, "uMatrix"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Ligb;

    const-string v2, "uTextureMatrix"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Ligb;

    const-string v2, "uTextureSampler"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Ligb;

    const-string v2, "uAlpha"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    iput-object v0, p0, Lify;->p:[Liga;

    .line 238
    const/4 v0, 0x5

    new-array v0, v0, [Liga;

    new-instance v1, Lifz;

    const-string v2, "aPosition"

    invoke-direct {v1, v2}, Lifz;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Ligb;

    const-string v2, "uMatrix"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Lifz;

    const-string v2, "aTextureCoordinate"

    invoke-direct {v1, v2}, Lifz;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Ligb;

    const-string v2, "uTextureSampler"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Ligb;

    const-string v2, "uAlpha"

    invoke-direct {v1, v2}, Ligb;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    iput-object v0, p0, Lify;->q:[Liga;

    .line 246
    new-instance v0, Ligk;

    invoke-direct {v0}, Ligk;-><init>()V

    iput-object v0, p0, Lify;->r:Ligk;

    .line 247
    new-instance v0, Ligk;

    invoke-direct {v0}, Ligk;-><init>()V

    .line 250
    iput v3, p0, Lify;->s:I

    .line 252
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lify;->t:Ljava/util/ArrayList;

    .line 263
    const/16 v0, 0x20

    new-array v0, v0, [F

    iput-object v0, p0, Lify;->u:[F

    .line 264
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lify;->v:Landroid/graphics/RectF;

    .line 266
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lify;->w:Landroid/graphics/RectF;

    .line 267
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lify;->x:[F

    .line 268
    new-array v0, v7, [I

    iput-object v0, p0, Lify;->y:[I

    .line 273
    iget-object v0, p0, Lify;->x:[F

    invoke-static {v0, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 274
    iget-object v0, p0, Lify;->c:[F

    iget v1, p0, Lify;->g:I

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 275
    iget-object v0, p0, Lify;->d:[F

    iget v1, p0, Lify;->f:I

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 276
    iget-object v0, p0, Lify;->t:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    sget-object v0, Lify;->b:[F

    const/16 v1, 0x50

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v0, v3, v2}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 279
    invoke-virtual {p0, v1}, Lify;->a(Ljava/nio/FloatBuffer;)I

    move-result v0

    iput v0, p0, Lify;->m:I

    .line 281
    const v0, 0x8b31

    const-string v1, "uniform mat4 uMatrix;\nattribute vec2 aPosition;\nvoid main() {\n  vec4 pos = vec4(aPosition, 0.0, 1.0);\n  gl_Position = uMatrix * pos;\n}\n"

    invoke-static {v0, v1}, Lify;->a(ILjava/lang/String;)I

    move-result v0

    .line 282
    const v1, 0x8b31

    const-string v2, "uniform mat4 uMatrix;\nuniform mat4 uTextureMatrix;\nattribute vec2 aPosition;\nvarying vec2 vTextureCoord;\nvoid main() {\n  vec4 pos = vec4(aPosition, 0.0, 1.0);\n  gl_Position = uMatrix * pos;\n  vTextureCoord = (uTextureMatrix * pos).xy;\n}\n"

    invoke-static {v1, v2}, Lify;->a(ILjava/lang/String;)I

    move-result v1

    .line 283
    const v2, 0x8b31

    const-string v3, "uniform mat4 uMatrix;\nattribute vec2 aPosition;\nattribute vec2 aTextureCoordinate;\nvarying vec2 vTextureCoord;\nvoid main() {\n  vec4 pos = vec4(aPosition, 0.0, 1.0);\n  gl_Position = uMatrix * pos;\n  vTextureCoord = aTextureCoordinate;\n}\n"

    invoke-static {v2, v3}, Lify;->a(ILjava/lang/String;)I

    move-result v2

    .line 284
    const v3, 0x8b30

    const-string v4, "precision mediump float;\nuniform vec4 uColor;\nvoid main() {\n  gl_FragColor = uColor;\n}\n"

    invoke-static {v3, v4}, Lify;->a(ILjava/lang/String;)I

    move-result v3

    .line 285
    const v4, 0x8b30

    const-string v5, "precision mediump float;\nvarying vec2 vTextureCoord;\nuniform float uAlpha;\nuniform sampler2D uTextureSampler;\nvoid main() {\n  gl_FragColor = texture2D(uTextureSampler, vTextureCoord);\n  gl_FragColor *= uAlpha;\n}\n"

    invoke-static {v4, v5}, Lify;->a(ILjava/lang/String;)I

    move-result v4

    .line 286
    const v5, 0x8b30

    const-string v6, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform float uAlpha;\nuniform samplerExternalOES uTextureSampler;\nvoid main() {\n  gl_FragColor = texture2D(uTextureSampler, vTextureCoord);\n  gl_FragColor *= uAlpha;\n}\n"

    invoke-static {v5, v6}, Lify;->a(ILjava/lang/String;)I

    move-result v5

    .line 289
    iget-object v6, p0, Lify;->n:[Liga;

    invoke-direct {p0, v0, v3, v6}, Lify;->a(II[Liga;)I

    .line 290
    iget-object v0, p0, Lify;->o:[Liga;

    invoke-direct {p0, v1, v4, v0}, Lify;->a(II[Liga;)I

    move-result v0

    iput v0, p0, Lify;->k:I

    .line 292
    iget-object v0, p0, Lify;->p:[Liga;

    invoke-direct {p0, v1, v5, v0}, Lify;->a(II[Liga;)I

    move-result v0

    iput v0, p0, Lify;->l:I

    .line 294
    iget-object v0, p0, Lify;->q:[Liga;

    invoke-direct {p0, v2, v4, v0}, Lify;->a(II[Liga;)I

    .line 295
    const/16 v0, 0x303

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 296
    invoke-static {}, Lify;->e()V

    .line 297
    return-void
.end method

.method private a(II[Liga;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 309
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v0

    .line 310
    invoke-static {}, Lify;->e()V

    .line 311
    if-nez v0, :cond_0

    .line 312
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot create GL program: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_0
    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 315
    invoke-static {}, Lify;->e()V

    .line 316
    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 317
    invoke-static {}, Lify;->e()V

    .line 318
    invoke-static {v0}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 319
    invoke-static {}, Lify;->e()V

    .line 320
    iget-object v2, p0, Lify;->y:[I

    .line 321
    const v3, 0x8b82

    invoke-static {v0, v3, v2, v1}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 322
    aget v2, v2, v1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 323
    sget-object v2, Lify;->a:Ljava/lang/String;

    const-string v3, "Could not link program: "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    sget-object v2, Lify;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    move v0, v1

    .line 328
    :cond_1
    :goto_0
    array-length v2, p3

    if-ge v1, v2, :cond_2

    .line 329
    aget-object v2, p3, v1

    invoke-virtual {v2, v0}, Liga;->a(I)V

    .line 328
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 331
    :cond_2
    return v0
.end method

.method private static a(ILjava/lang/String;)I
    .locals 1

    .prologue
    .line 337
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v0

    .line 340
    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 341
    invoke-static {}, Lify;->e()V

    .line 342
    invoke-static {v0}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 343
    invoke-static {}, Lify;->e()V

    .line 345
    return v0
.end method

.method private static a(Landroid/graphics/RectF;Landroid/graphics/RectF;Lifv;)V
    .locals 6

    .prologue
    .line 647
    invoke-virtual {p2}, Lifv;->c()I

    move-result v0

    .line 648
    invoke-virtual {p2}, Lifv;->d()I

    move-result v1

    .line 649
    invoke-virtual {p2}, Lifv;->e()I

    move-result v2

    .line 650
    invoke-virtual {p2}, Lifv;->f()I

    move-result v3

    .line 652
    iget v4, p0, Landroid/graphics/RectF;->left:F

    int-to-float v5, v2

    div-float/2addr v4, v5

    iput v4, p0, Landroid/graphics/RectF;->left:F

    .line 653
    iget v4, p0, Landroid/graphics/RectF;->right:F

    int-to-float v5, v2

    div-float/2addr v4, v5

    iput v4, p0, Landroid/graphics/RectF;->right:F

    .line 654
    iget v4, p0, Landroid/graphics/RectF;->top:F

    int-to-float v5, v3

    div-float/2addr v4, v5

    iput v4, p0, Landroid/graphics/RectF;->top:F

    .line 655
    iget v4, p0, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, v3

    div-float/2addr v4, v5

    iput v4, p0, Landroid/graphics/RectF;->bottom:F

    .line 658
    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 659
    iget v2, p0, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v0

    if-lez v2, :cond_0

    .line 660
    iget v2, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v4

    iget v5, p0, Landroid/graphics/RectF;->left:F

    sub-float v5, v0, v5

    mul-float/2addr v4, v5

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v4, v5

    add-float/2addr v2, v4

    iput v2, p1, Landroid/graphics/RectF;->right:F

    .line 661
    iput v0, p0, Landroid/graphics/RectF;->right:F

    .line 663
    :cond_0
    int-to-float v0, v1

    int-to-float v1, v3

    div-float/2addr v0, v1

    .line 664
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_1

    .line 665
    iget v1, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget v3, p0, Landroid/graphics/RectF;->top:F

    sub-float v3, v0, v3

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p1, Landroid/graphics/RectF;->bottom:F

    .line 666
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    .line 668
    :cond_1
    return-void
.end method

.method private b(Lifv;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 12

    .prologue
    const/4 v1, 0x2

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 632
    iget-object v0, p0, Lify;->x:[F

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    aput v2, v0, v3

    iget-object v0, p0, Lify;->x:[F

    const/4 v2, 0x5

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v4

    aput v4, v0, v2

    iget-object v0, p0, Lify;->x:[F

    const/16 v2, 0xc

    iget v4, p2, Landroid/graphics/RectF;->left:F

    aput v4, v0, v2

    iget-object v0, p0, Lify;->x:[F

    const/16 v2, 0xd

    iget v4, p2, Landroid/graphics/RectF;->top:F

    aput v4, v0, v2

    .line 633
    iget-object v6, p0, Lify;->x:[F

    invoke-virtual {p1}, Lifv;->h()I

    move-result v0

    const/16 v2, 0xde1

    if-ne v0, v2, :cond_3

    iget-object v2, p0, Lify;->o:[Liga;

    iget v0, p0, Lify;->k:I

    move-object v9, v2

    :goto_0
    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    invoke-static {}, Lify;->e()V

    invoke-virtual {p1}, Lifv;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lify;->d()F

    move-result v0

    const v2, 0x3f733333    # 0.95f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    :cond_0
    move v0, v10

    :goto_1
    if-eqz v0, :cond_5

    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    invoke-static {}, Lify;->e()V

    :goto_2
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    invoke-static {}, Lify;->e()V

    invoke-virtual {p1, p0}, Lifv;->b(Lifx;)Z

    invoke-virtual {p1}, Lifv;->h()I

    move-result v0

    invoke-virtual {p1}, Lifv;->b()I

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    invoke-static {}, Lify;->e()V

    const/4 v0, 0x3

    aget-object v0, v9, v0

    iget v0, v0, Liga;->a:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    invoke-static {}, Lify;->e()V

    const/4 v0, 0x4

    aget-object v0, v9, v0

    iget v0, v0, Liga;->a:I

    invoke-virtual {p0}, Lify;->d()F

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    invoke-static {}, Lify;->e()V

    const v0, 0x8892

    iget v2, p0, Lify;->m:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    invoke-static {}, Lify;->e()V

    aget-object v0, v9, v3

    iget v0, v0, Liga;->a:I

    const/16 v2, 0x1406

    const/16 v4, 0x8

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    invoke-static {}, Lify;->e()V

    const v0, 0x8892

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    invoke-static {}, Lify;->e()V

    aget-object v0, v9, v1

    iget v0, v0, Liga;->a:I

    invoke-static {v0, v10, v3, v6, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    invoke-static {}, Lify;->e()V

    invoke-virtual {p1}, Lifv;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lify;->a(I)V

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-virtual {p0, v8, v0}, Lify;->a(FF)V

    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p0, v11, v0, v11}, Lify;->a(FFF)V

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p0, v8, v0}, Lify;->a(FF)V

    :cond_1
    iget v6, p3, Landroid/graphics/RectF;->left:F

    iget v7, p3, Landroid/graphics/RectF;->top:F

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lify;->u:[F

    iget-object v4, p0, Lify;->c:[F

    iget v5, p0, Lify;->g:I

    invoke-static/range {v2 .. v8}, Landroid/opengl/Matrix;->translateM([FI[FIFFF)V

    iget-object v2, p0, Lify;->u:[F

    invoke-static {v2, v3, v0, v1, v11}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    iget-object v0, p0, Lify;->u:[F

    const/16 v1, 0x10

    iget-object v2, p0, Lify;->j:[F

    iget-object v4, p0, Lify;->u:[F

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    aget-object v0, v9, v10

    iget v0, v0, Liga;->a:I

    iget-object v1, p0, Lify;->u:[F

    const/16 v2, 0x10

    invoke-static {v0, v10, v3, v1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    invoke-static {}, Lify;->e()V

    aget-object v0, v9, v3

    iget v0, v0, Liga;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    invoke-static {}, Lify;->e()V

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-static {v1, v3, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    invoke-static {}, Lify;->e()V

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    invoke-static {}, Lify;->e()V

    invoke-virtual {p1}, Lifv;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lify;->b()V

    :cond_2
    iget v0, p0, Lify;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lify;->s:I

    .line 634
    return-void

    .line 633
    :cond_3
    iget-object v2, p0, Lify;->p:[Liga;

    iget v0, p0, Lify;->l:I

    move-object v9, v2

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto/16 :goto_1

    :cond_5
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    invoke-static {}, Lify;->e()V

    goto/16 :goto_2
.end method

.method public static e()V
    .locals 5

    .prologue
    .line 996
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    .line 997
    if-eqz v0, :cond_0

    .line 998
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    .line 999
    sget-object v2, Lify;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x15

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "GL error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1001
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/nio/FloatBuffer;)I
    .locals 5

    .prologue
    const v4, 0x8892

    const/4 v3, 0x0

    .line 948
    sget-object v0, Lify;->z:Ligd;

    const/4 v1, 0x1

    iget-object v2, p0, Lify;->y:[I

    invoke-interface {v0, v1, v2, v3}, Ligd;->a(I[II)V

    invoke-static {}, Lify;->e()V

    iget-object v0, p0, Lify;->y:[I

    aget v0, v0, v3

    invoke-static {v4, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    invoke-static {}, Lify;->e()V

    invoke-virtual {p1}, Ljava/nio/Buffer;->capacity()I

    move-result v1

    shl-int/lit8 v1, v1, 0x2

    const v2, 0x88e4

    invoke-static {v4, v1, p1, v2}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    invoke-static {}, Lify;->e()V

    return v0
.end method

.method public a()Ligd;
    .locals 1

    .prologue
    .line 1039
    sget-object v0, Lify;->z:Ligd;

    return-object v0
.end method

.method public a(FF)V
    .locals 6

    .prologue
    .line 405
    iget v0, p0, Lify;->g:I

    .line 406
    iget-object v1, p0, Lify;->c:[F

    .line 407
    add-int/lit8 v2, v0, 0xc

    aget v3, v1, v2

    aget v4, v1, v0

    mul-float/2addr v4, p1

    add-int/lit8 v5, v0, 0x4

    aget v5, v1, v5

    mul-float/2addr v5, p2

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 408
    add-int/lit8 v2, v0, 0xd

    aget v3, v1, v2

    add-int/lit8 v4, v0, 0x1

    aget v4, v1, v4

    mul-float/2addr v4, p1

    add-int/lit8 v5, v0, 0x5

    aget v5, v1, v5

    mul-float/2addr v5, p2

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 409
    add-int/lit8 v2, v0, 0xe

    aget v3, v1, v2

    add-int/lit8 v4, v0, 0x2

    aget v4, v1, v4

    mul-float/2addr v4, p1

    add-int/lit8 v5, v0, 0x6

    aget v5, v1, v5

    mul-float/2addr v5, p2

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 410
    add-int/lit8 v2, v0, 0xf

    aget v3, v1, v2

    add-int/lit8 v4, v0, 0x3

    aget v4, v1, v4

    mul-float/2addr v4, p1

    add-int/lit8 v0, v0, 0x7

    aget v0, v1, v0

    mul-float/2addr v0, p2

    add-float/2addr v0, v4

    add-float/2addr v0, v3

    aput v0, v1, v2

    .line 411
    return-void
.end method

.method public a(FFF)V
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lify;->c:[F

    iget v1, p0, Lify;->g:I

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 416
    return-void
.end method

.method public a(FFFF)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x10

    .line 420
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 429
    :goto_0
    return-void

    .line 423
    :cond_0
    iget-object v0, p0, Lify;->u:[F

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 424
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    .line 425
    iget-object v4, p0, Lify;->c:[F

    .line 426
    iget v5, p0, Lify;->g:I

    move-object v2, v0

    move v3, v8

    move-object v6, v0

    move v7, v1

    .line 427
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 428
    invoke-static {v0, v8, v4, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 447
    and-int/lit8 v2, p1, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    .line 448
    :goto_0
    if-eqz v2, :cond_1

    .line 449
    invoke-virtual {p0}, Lify;->d()F

    move-result v2

    .line 450
    iget v3, p0, Lify;->f:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lify;->f:I

    .line 451
    iget-object v3, p0, Lify;->d:[F

    array-length v3, v3

    iget v4, p0, Lify;->f:I

    if-gt v3, v4, :cond_0

    .line 452
    iget-object v3, p0, Lify;->d:[F

    iget-object v4, p0, Lify;->d:[F

    array-length v4, v4

    shl-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v3

    iput-object v3, p0, Lify;->d:[F

    .line 454
    :cond_0
    iget-object v3, p0, Lify;->d:[F

    iget v4, p0, Lify;->f:I

    aput v2, v3, v4

    .line 456
    :cond_1
    and-int/lit8 v2, p1, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 457
    :goto_1
    if-eqz v0, :cond_3

    .line 458
    iget v0, p0, Lify;->g:I

    .line 459
    iget v1, p0, Lify;->g:I

    add-int/lit8 v1, v1, 0x10

    iput v1, p0, Lify;->g:I

    .line 460
    iget-object v1, p0, Lify;->c:[F

    array-length v1, v1

    iget v2, p0, Lify;->g:I

    if-gt v1, v2, :cond_2

    .line 461
    iget-object v1, p0, Lify;->c:[F

    iget-object v2, p0, Lify;->c:[F

    array-length v2, v2

    shl-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v1

    iput-object v1, p0, Lify;->c:[F

    .line 463
    :cond_2
    iget-object v1, p0, Lify;->c:[F

    iget-object v2, p0, Lify;->c:[F

    iget v3, p0, Lify;->g:I

    const/16 v4, 0x10

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 465
    :cond_3
    iget-object v0, p0, Lify;->e:Ligk;

    invoke-virtual {v0, p1}, Ligk;->a(I)V

    .line 466
    return-void

    :cond_4
    move v2, v1

    .line 447
    goto :goto_0

    :cond_5
    move v0, v1

    .line 456
    goto :goto_1
.end method

.method public a(II)V
    .locals 8

    .prologue
    const/high16 v6, -0x40800000    # -1.0f

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 350
    iput p1, p0, Lify;->h:I

    .line 351
    iput p2, p0, Lify;->i:I

    .line 352
    iget v0, p0, Lify;->h:I

    iget v3, p0, Lify;->i:I

    invoke-static {v1, v1, v0, v3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 353
    invoke-static {}, Lify;->e()V

    .line 354
    iget-object v0, p0, Lify;->c:[F

    iget v3, p0, Lify;->g:I

    invoke-static {v0, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 355
    iget-object v0, p0, Lify;->j:[F

    int-to-float v3, p1

    int-to-float v5, p2

    move v4, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 356
    iget-object v0, p0, Lify;->t:Ljava/util/ArrayList;

    iget-object v1, p0, Lify;->t:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lige;

    if-nez v0, :cond_0

    .line 357
    iget-object v0, p0, Lify;->c:[F

    iget v1, p0, Lify;->g:I

    int-to-float v3, p2

    invoke-static {v0, v1, v2, v3, v2}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 360
    iget-object v0, p0, Lify;->c:[F

    iget v1, p0, Lify;->g:I

    invoke-static {v0, v1, v7, v6, v7}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 362
    :cond_0
    return-void
.end method

.method public a(Lifv;II)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 921
    invoke-virtual {p1}, Lifv;->h()I

    move-result v0

    .line 922
    invoke-virtual {p1}, Lifv;->b()I

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 923
    invoke-static {}, Lify;->e()V

    .line 924
    invoke-virtual {p1}, Lifv;->e()I

    move-result v3

    .line 925
    invoke-virtual {p1}, Lifv;->f()I

    move-result v4

    .line 926
    const/4 v8, 0x0

    move v2, p2

    move v5, v1

    move v6, p2

    move v7, p3

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 927
    return-void
.end method

.method public a(Lifv;IIII)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 586
    if-lez p4, :cond_0

    if-gtz p5, :cond_1

    .line 593
    :cond_0
    :goto_0
    return-void

    .line 589
    :cond_1
    iget-object v4, p0, Lify;->v:Landroid/graphics/RectF;

    invoke-virtual {p1}, Lifv;->c()I

    move-result v1

    invoke-virtual {p1}, Lifv;->d()I

    move-result v0

    invoke-virtual {p1}, Lifv;->g()Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, -0x1

    move v3, v2

    :goto_1
    int-to-float v3, v3

    int-to-float v2, v2

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-virtual {v4, v3, v2, v1, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 590
    iget-object v0, p0, Lify;->w:Landroid/graphics/RectF;

    int-to-float v1, p2

    int-to-float v2, p3

    add-int v3, p2, p4

    int-to-float v3, v3

    add-int v4, p3, p5

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 591
    iget-object v0, p0, Lify;->v:Landroid/graphics/RectF;

    iget-object v1, p0, Lify;->w:Landroid/graphics/RectF;

    invoke-static {v0, v1, p1}, Lify;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;Lifv;)V

    .line 592
    iget-object v0, p0, Lify;->v:Landroid/graphics/RectF;

    iget-object v1, p0, Lify;->w:Landroid/graphics/RectF;

    invoke-direct {p0, p1, v0, v1}, Lify;->b(Lifv;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public a(Lifv;IILandroid/graphics/Bitmap;II)V
    .locals 7

    .prologue
    .line 940
    invoke-virtual {p1}, Lifv;->h()I

    move-result v0

    .line 941
    invoke-virtual {p1}, Lifv;->b()I

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 942
    invoke-static {}, Lify;->e()V

    .line 943
    const/4 v1, 0x0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;II)V

    .line 944
    return-void
.end method

.method public a(Lifv;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 931
    invoke-virtual {p1}, Lifv;->h()I

    move-result v0

    .line 932
    invoke-virtual {p1}, Lifv;->b()I

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 933
    invoke-static {}, Lify;->e()V

    .line 934
    invoke-static {v0, v2, p2, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 935
    return-void
.end method

.method public a(Lifv;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 611
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 614
    :cond_1
    iget-object v0, p0, Lify;->v:Landroid/graphics/RectF;

    invoke-virtual {v0, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 615
    iget-object v0, p0, Lify;->w:Landroid/graphics/RectF;

    invoke-virtual {v0, p3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 617
    iget-object v0, p0, Lify;->v:Landroid/graphics/RectF;

    iget-object v1, p0, Lify;->w:Landroid/graphics/RectF;

    invoke-static {v0, v1, p1}, Lify;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;Lifv;)V

    .line 618
    iget-object v0, p0, Lify;->v:Landroid/graphics/RectF;

    iget-object v1, p0, Lify;->w:Landroid/graphics/RectF;

    invoke-direct {p0, p1, v0, v1}, Lify;->b(Lifv;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public a(Lifv;)Z
    .locals 4

    .prologue
    .line 792
    invoke-virtual {p1}, Lifv;->i()Z

    move-result v0

    .line 793
    if-eqz v0, :cond_0

    .line 794
    iget-object v1, p0, Lify;->r:Ligk;

    monitor-enter v1

    .line 795
    :try_start_0
    iget-object v2, p0, Lify;->r:Ligk;

    invoke-virtual {p1}, Lifv;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ligk;->a(I)V

    .line 796
    monitor-exit v1

    .line 798
    :cond_0
    return v0

    .line 796
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 470
    iget-object v2, p0, Lify;->e:Ligk;

    invoke-virtual {v2}, Ligk;->a()I

    move-result v3

    .line 471
    and-int/lit8 v2, v3, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    .line 472
    :goto_0
    if-eqz v2, :cond_0

    .line 473
    iget v2, p0, Lify;->f:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lify;->f:I

    .line 475
    :cond_0
    and-int/lit8 v2, v3, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 476
    :goto_1
    if-eqz v0, :cond_1

    .line 477
    iget v0, p0, Lify;->g:I

    add-int/lit8 v0, v0, -0x10

    iput v0, p0, Lify;->g:I

    .line 479
    :cond_1
    return-void

    :cond_2
    move v2, v1

    .line 471
    goto :goto_0

    :cond_3
    move v0, v1

    .line 475
    goto :goto_1
.end method

.method public b(Lifv;)V
    .locals 4

    .prologue
    const v3, 0x812f

    const v2, 0x46180400    # 9729.0f

    .line 910
    invoke-virtual {p1}, Lifv;->h()I

    move-result v0

    .line 911
    invoke-virtual {p1}, Lifv;->b()I

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 912
    invoke-static {}, Lify;->e()V

    .line 913
    const/16 v1, 0x2802

    invoke-static {v0, v1, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 914
    const/16 v1, 0x2803

    invoke-static {v0, v1, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 915
    const/16 v1, 0x2801

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 916
    const/16 v1, 0x2800

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 917
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 366
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v1, v1, v1, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 367
    invoke-static {}, Lify;->e()V

    .line 368
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 369
    invoke-static {}, Lify;->e()V

    .line 370
    return-void
.end method

.method public d()F
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lify;->d:[F

    iget v1, p0, Lify;->f:I

    aget v0, v0, v1

    return v0
.end method
