.class public final Lpxp;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lpxo;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22
    iput v1, p0, Lpxp;->b:I

    .line 25
    sget-object v0, Lpxo;->a:[Lpxo;

    iput-object v0, p0, Lpxp;->a:[Lpxo;

    .line 28
    iput v1, p0, Lpxp;->c:I

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x80000000

    .line 52
    .line 53
    iget v0, p0, Lpxp;->b:I

    if-eq v0, v6, :cond_3

    .line 54
    const/4 v0, 0x1

    iget v2, p0, Lpxp;->b:I

    .line 55
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 57
    :goto_0
    iget-object v2, p0, Lpxp;->a:[Lpxo;

    if-eqz v2, :cond_1

    .line 58
    iget-object v2, p0, Lpxp;->a:[Lpxo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 59
    if-eqz v4, :cond_0

    .line 60
    const/4 v5, 0x2

    .line 61
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 58
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 65
    :cond_1
    iget v1, p0, Lpxp;->c:I

    if-eq v1, v6, :cond_2

    .line 66
    const/4 v1, 0x3

    iget v2, p0, Lpxp;->c:I

    .line 67
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_2
    iget-object v1, p0, Lpxp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    iput v0, p0, Lpxp;->ai:I

    .line 71
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpxp;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 80
    sparse-switch v0, :sswitch_data_0

    .line 84
    iget-object v2, p0, Lpxp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 85
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpxp;->ah:Ljava/util/List;

    .line 88
    :cond_1
    iget-object v2, p0, Lpxp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    :sswitch_0
    return-object p0

    .line 95
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 96
    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    .line 98
    :cond_2
    iput v0, p0, Lpxp;->b:I

    goto :goto_0

    .line 100
    :cond_3
    iput v4, p0, Lpxp;->b:I

    goto :goto_0

    .line 105
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 106
    iget-object v0, p0, Lpxp;->a:[Lpxo;

    if-nez v0, :cond_5

    move v0, v1

    .line 107
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpxo;

    .line 108
    iget-object v3, p0, Lpxp;->a:[Lpxo;

    if-eqz v3, :cond_4

    .line 109
    iget-object v3, p0, Lpxp;->a:[Lpxo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 111
    :cond_4
    iput-object v2, p0, Lpxp;->a:[Lpxo;

    .line 112
    :goto_2
    iget-object v2, p0, Lpxp;->a:[Lpxo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 113
    iget-object v2, p0, Lpxp;->a:[Lpxo;

    new-instance v3, Lpxo;

    invoke-direct {v3}, Lpxo;-><init>()V

    aput-object v3, v2, v0

    .line 114
    iget-object v2, p0, Lpxp;->a:[Lpxo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 115
    invoke-virtual {p1}, Loxn;->a()I

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 106
    :cond_5
    iget-object v0, p0, Lpxp;->a:[Lpxo;

    array-length v0, v0

    goto :goto_1

    .line 118
    :cond_6
    iget-object v2, p0, Lpxp;->a:[Lpxo;

    new-instance v3, Lpxo;

    invoke-direct {v3}, Lpxo;-><init>()V

    aput-object v3, v2, v0

    .line 119
    iget-object v2, p0, Lpxp;->a:[Lpxo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 123
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 124
    if-eqz v0, :cond_7

    if-eq v0, v4, :cond_7

    if-eq v0, v5, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    if-eq v0, v2, :cond_7

    const/4 v2, 0x5

    if-eq v0, v2, :cond_7

    const/4 v2, 0x6

    if-eq v0, v2, :cond_7

    const/4 v2, 0x7

    if-ne v0, v2, :cond_8

    .line 132
    :cond_7
    iput v0, p0, Lpxp;->c:I

    goto/16 :goto_0

    .line 134
    :cond_8
    iput v1, p0, Lpxp;->c:I

    goto/16 :goto_0

    .line 80
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 33
    iget v0, p0, Lpxp;->b:I

    if-eq v0, v5, :cond_0

    .line 34
    const/4 v0, 0x1

    iget v1, p0, Lpxp;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 36
    :cond_0
    iget-object v0, p0, Lpxp;->a:[Lpxo;

    if-eqz v0, :cond_2

    .line 37
    iget-object v1, p0, Lpxp;->a:[Lpxo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 38
    if-eqz v3, :cond_1

    .line 39
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_2
    iget v0, p0, Lpxp;->c:I

    if-eq v0, v5, :cond_3

    .line 44
    const/4 v0, 0x3

    iget v1, p0, Lpxp;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 46
    :cond_3
    iget-object v0, p0, Lpxp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 48
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpxp;->a(Loxn;)Lpxp;

    move-result-object v0

    return-object v0
.end method
