.class public final Lpqn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpqn;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Lpqo;

.field public d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [Lpqn;

    sput-object v0, Lpqn;->a:[Lpqn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Loxq;-><init>()V

    .line 91
    sget-object v0, Lpqo;->a:[Lpqo;

    iput-object v0, p0, Lpqn;->c:[Lpqo;

    .line 26
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 117
    .line 118
    iget-object v0, p0, Lpqn;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 119
    const/4 v0, 0x1

    iget-object v2, p0, Lpqn;->b:Ljava/lang/String;

    .line 120
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 122
    :goto_0
    iget-object v2, p0, Lpqn;->c:[Lpqo;

    if-eqz v2, :cond_1

    .line 123
    iget-object v2, p0, Lpqn;->c:[Lpqo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 124
    if-eqz v4, :cond_0

    .line 125
    const/4 v5, 0x2

    .line 126
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 123
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 130
    :cond_1
    iget-object v1, p0, Lpqn;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 131
    const/4 v1, 0x3

    iget-object v2, p0, Lpqn;->d:Ljava/lang/Boolean;

    .line 132
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 134
    :cond_2
    iget-object v1, p0, Lpqn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    iput v0, p0, Lpqn;->ai:I

    .line 136
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpqn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 144
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 145
    sparse-switch v0, :sswitch_data_0

    .line 149
    iget-object v2, p0, Lpqn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 150
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpqn;->ah:Ljava/util/List;

    .line 153
    :cond_1
    iget-object v2, p0, Lpqn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    :sswitch_0
    return-object p0

    .line 160
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqn;->b:Ljava/lang/String;

    goto :goto_0

    .line 164
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 165
    iget-object v0, p0, Lpqn;->c:[Lpqo;

    if-nez v0, :cond_3

    move v0, v1

    .line 166
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpqo;

    .line 167
    iget-object v3, p0, Lpqn;->c:[Lpqo;

    if-eqz v3, :cond_2

    .line 168
    iget-object v3, p0, Lpqn;->c:[Lpqo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 170
    :cond_2
    iput-object v2, p0, Lpqn;->c:[Lpqo;

    .line 171
    :goto_2
    iget-object v2, p0, Lpqn;->c:[Lpqo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 172
    iget-object v2, p0, Lpqn;->c:[Lpqo;

    new-instance v3, Lpqo;

    invoke-direct {v3}, Lpqo;-><init>()V

    aput-object v3, v2, v0

    .line 173
    iget-object v2, p0, Lpqn;->c:[Lpqo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 174
    invoke-virtual {p1}, Loxn;->a()I

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 165
    :cond_3
    iget-object v0, p0, Lpqn;->c:[Lpqo;

    array-length v0, v0

    goto :goto_1

    .line 177
    :cond_4
    iget-object v2, p0, Lpqn;->c:[Lpqo;

    new-instance v3, Lpqo;

    invoke-direct {v3}, Lpqo;-><init>()V

    aput-object v3, v2, v0

    .line 178
    iget-object v2, p0, Lpqn;->c:[Lpqo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 182
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpqn;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 145
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 98
    iget-object v0, p0, Lpqn;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x1

    iget-object v1, p0, Lpqn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lpqn;->c:[Lpqo;

    if-eqz v0, :cond_2

    .line 102
    iget-object v1, p0, Lpqn;->c:[Lpqo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 103
    if-eqz v3, :cond_1

    .line 104
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 102
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_2
    iget-object v0, p0, Lpqn;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 109
    const/4 v0, 0x3

    iget-object v1, p0, Lpqn;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 111
    :cond_3
    iget-object v0, p0, Lpqn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 113
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lpqn;->a(Loxn;)Lpqn;

    move-result-object v0

    return-object v0
.end method
