.class public Lenq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Ljava/lang/String;

.field private synthetic c:Lenl;


# direct methods
.method public constructor <init>(Lenl;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1045
    iput-object p1, p0, Lenq;->c:Lenl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046
    iput-object p2, p0, Lenq;->a:Landroid/view/View;

    .line 1047
    iput-object p3, p0, Lenq;->b:Ljava/lang/String;

    .line 1048
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1065
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1052
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1056
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lenq;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1057
    iget-object v0, p0, Lenq;->c:Lenl;

    iget-object v1, p0, Lenq;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lenl;->c(Landroid/view/View;)V

    .line 1061
    :goto_0
    return-void

    .line 1059
    :cond_0
    iget-object v0, p0, Lenq;->c:Lenl;

    iget-object v1, p0, Lenq;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lenl;->d(Landroid/view/View;)V

    goto :goto_0
.end method
