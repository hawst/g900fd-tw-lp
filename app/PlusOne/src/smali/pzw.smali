.class public final Lpzw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lqap;

.field public b:Lpzx;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 126
    iput-object v0, p0, Lpzw;->a:Lqap;

    .line 131
    iput-object v0, p0, Lpzw;->b:Lpzx;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 159
    const/4 v0, 0x0

    .line 160
    iget-object v1, p0, Lpzw;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 161
    const/4 v0, 0x1

    iget-object v1, p0, Lpzw;->c:Ljava/lang/Long;

    .line 162
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 164
    :cond_0
    iget-object v1, p0, Lpzw;->a:Lqap;

    if-eqz v1, :cond_1

    .line 165
    const/4 v1, 0x2

    iget-object v2, p0, Lpzw;->a:Lqap;

    .line 166
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_1
    iget-object v1, p0, Lpzw;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 169
    const/4 v1, 0x3

    iget-object v2, p0, Lpzw;->d:Ljava/lang/Long;

    .line 170
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_2
    iget-object v1, p0, Lpzw;->b:Lpzx;

    if-eqz v1, :cond_3

    .line 173
    const/4 v1, 0x4

    iget-object v2, p0, Lpzw;->b:Lpzx;

    .line 174
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_3
    iget-object v1, p0, Lpzw;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 177
    const/4 v1, 0x5

    iget-object v2, p0, Lpzw;->e:Ljava/lang/String;

    .line 178
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_4
    iget-object v1, p0, Lpzw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    iput v0, p0, Lpzw;->ai:I

    .line 182
    return v0
.end method

.method public a(Loxn;)Lpzw;
    .locals 2

    .prologue
    .line 190
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 191
    sparse-switch v0, :sswitch_data_0

    .line 195
    iget-object v1, p0, Lpzw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpzw;->ah:Ljava/util/List;

    .line 199
    :cond_1
    iget-object v1, p0, Lpzw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    :sswitch_0
    return-object p0

    .line 206
    :sswitch_1
    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpzw;->c:Ljava/lang/Long;

    goto :goto_0

    .line 210
    :sswitch_2
    iget-object v0, p0, Lpzw;->a:Lqap;

    if-nez v0, :cond_2

    .line 211
    new-instance v0, Lqap;

    invoke-direct {v0}, Lqap;-><init>()V

    iput-object v0, p0, Lpzw;->a:Lqap;

    .line 213
    :cond_2
    iget-object v0, p0, Lpzw;->a:Lqap;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 217
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpzw;->d:Ljava/lang/Long;

    goto :goto_0

    .line 221
    :sswitch_4
    iget-object v0, p0, Lpzw;->b:Lpzx;

    if-nez v0, :cond_3

    .line 222
    new-instance v0, Lpzx;

    invoke-direct {v0}, Lpzx;-><init>()V

    iput-object v0, p0, Lpzw;->b:Lpzx;

    .line 224
    :cond_3
    iget-object v0, p0, Lpzw;->b:Lpzx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 228
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpzw;->e:Ljava/lang/String;

    goto :goto_0

    .line 191
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lpzw;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x1

    iget-object v1, p0, Lpzw;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->c(IJ)V

    .line 141
    :cond_0
    iget-object v0, p0, Lpzw;->a:Lqap;

    if-eqz v0, :cond_1

    .line 142
    const/4 v0, 0x2

    iget-object v1, p0, Lpzw;->a:Lqap;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 144
    :cond_1
    iget-object v0, p0, Lpzw;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 145
    const/4 v0, 0x3

    iget-object v1, p0, Lpzw;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 147
    :cond_2
    iget-object v0, p0, Lpzw;->b:Lpzx;

    if-eqz v0, :cond_3

    .line 148
    const/4 v0, 0x4

    iget-object v1, p0, Lpzw;->b:Lpzx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 150
    :cond_3
    iget-object v0, p0, Lpzw;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 151
    const/4 v0, 0x5

    iget-object v1, p0, Lpzw;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 153
    :cond_4
    iget-object v0, p0, Lpzw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 155
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpzw;->a(Loxn;)Lpzw;

    move-result-object v0

    return-object v0
.end method
