.class public final Lldi;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Llci;

.field private c:Landroid/text/StaticLayout;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lldi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lldi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    sget-object v0, Lldi;->a:Llct;

    if-nez v0, :cond_0

    .line 48
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lldi;->a:Llct;

    .line 51
    :cond_0
    invoke-virtual {p0, v1}, Lldi;->setFocusable(Z)V

    .line 52
    invoke-virtual {p0, v1}, Lldi;->setClickable(Z)V

    .line 53
    invoke-virtual {p0, p0}, Lldi;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method

.method private final b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lldi;->e:I

    packed-switch v0, :pswitch_data_0

    .line 156
    :pswitch_0
    sget-object v0, Lldi;->a:Llct;

    iget-object v0, v0, Llct;->ai:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    .line 147
    :pswitch_1
    sget-object v0, Lldi;->a:Llct;

    iget-object v0, v0, Llct;->ah:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 151
    :pswitch_2
    sget-object v0, Lldi;->a:Llct;

    iget-object v0, v0, Llct;->aj:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 145
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 124
    iput-object v0, p0, Lldi;->b:Llci;

    .line 125
    iput-object v0, p0, Lldi;->c:Landroid/text/StaticLayout;

    .line 126
    iput-object v0, p0, Lldi;->d:Ljava/lang/String;

    .line 127
    iput v1, p0, Lldi;->e:I

    .line 128
    iput-boolean v1, p0, Lldi;->f:Z

    .line 129
    return-void
.end method

.method public a(Llci;Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lldi;->b:Llci;

    .line 59
    iput-object p2, p0, Lldi;->d:Ljava/lang/String;

    .line 60
    iput p3, p0, Lldi;->e:I

    .line 61
    iput-boolean p4, p0, Lldi;->f:Z

    .line 62
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 140
    invoke-virtual {p0}, Lldi;->invalidate()V

    .line 141
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 142
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lldi;->d:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lldi;->b:Llci;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lldi;->b:Llci;

    iget-object v1, p0, Lldi;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Llci;->q(Ljava/lang/String;)V

    .line 136
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 91
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 93
    invoke-virtual {p0}, Lldi;->getWidth()I

    move-result v6

    .line 94
    invoke-virtual {p0}, Lldi;->getHeight()I

    move-result v7

    .line 95
    sget-object v0, Lldi;->a:Llct;

    iget v0, v0, Llct;->m:I

    .line 97
    iget-object v1, p0, Lldi;->c:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    .line 98
    iget-object v1, p0, Lldi;->c:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sub-int v1, v7, v1

    div-int/lit8 v1, v1, 0x2

    .line 99
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 100
    iget-object v2, p0, Lldi;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 101
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 104
    :cond_0
    invoke-direct {p0}, Lldi;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v1, v6, v1

    sget-object v2, Lldi;->a:Llct;

    iget v2, v2, Llct;->m:I

    sub-int/2addr v1, v2

    .line 106
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v2, v7, v2

    div-int/lit8 v2, v2, 0x2

    .line 107
    int-to-float v1, v1

    int-to-float v2, v2

    sget-object v3, Lldi;->a:Llct;

    iget-object v3, v3, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 109
    iget-boolean v0, p0, Lldi;->f:Z

    if-eqz v0, :cond_1

    .line 110
    sget-object v0, Lldi;->a:Llct;

    iget v0, v0, Llct;->m:I

    sub-int v0, v6, v0

    .line 111
    sget-object v1, Lldi;->a:Llct;

    iget-object v1, v1, Llct;->w:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    float-to-int v4, v1

    .line 112
    sget-object v1, Lldi;->a:Llct;

    iget v1, v1, Llct;->m:I

    int-to-float v1, v1

    sub-int v2, v7, v4

    int-to-float v2, v2

    int-to-float v3, v0

    sub-int v0, v7, v4

    int-to-float v4, v0

    sget-object v0, Lldi;->a:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 116
    :cond_1
    invoke-virtual {p0}, Lldi;->isPressed()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lldi;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 117
    :cond_2
    sget-object v0, Lldi;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 118
    sget-object v0, Lldi;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 120
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    .line 66
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 68
    invoke-direct {p0}, Lldi;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 71
    invoke-virtual {p0}, Lldi;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v2, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    iget-object v3, p0, Lldi;->d:Ljava/lang/String;

    sget-object v4, Lldi;->a:Llct;

    iget v4, v4, Llct;->m:I

    mul-int/lit8 v4, v4, 0x3

    sub-int v4, v0, v4

    .line 72
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    const/4 v5, 0x1

    .line 70
    invoke-static {v2, v3, v4, v5}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lldi;->c:Landroid/text/StaticLayout;

    .line 75
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 77
    iget-object v2, p0, Lldi;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    sget-object v2, Lldi;->a:Llct;

    iget v2, v2, Llct;->m:I

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    .line 79
    invoke-virtual {p0, v0, v1}, Lldi;->setMeasuredDimension(II)V

    .line 80
    return-void
.end method
