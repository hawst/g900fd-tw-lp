.class public final Lmwf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field private c:[Lmvz;

.field private d:[Lmus;

.field private e:[Ljava/lang/Long;

.field private f:[Lmwz;

.field private g:[Lmud;

.field private h:[Lmue;

.field private i:[Lmvs;

.field private j:Ljava/lang/String;

.field private k:Lmud;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/Integer;

.field private n:Lmuv;

.field private o:I

.field private p:Lmwc;

.field private q:Lmts;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7089
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7092
    sget-object v0, Lmvz;->a:[Lmvz;

    iput-object v0, p0, Lmwf;->c:[Lmvz;

    .line 7095
    sget-object v0, Lmus;->a:[Lmus;

    iput-object v0, p0, Lmwf;->d:[Lmus;

    .line 7098
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lmwf;->e:[Ljava/lang/Long;

    .line 7101
    sget-object v0, Lmwz;->a:[Lmwz;

    iput-object v0, p0, Lmwf;->f:[Lmwz;

    .line 7104
    sget-object v0, Lmud;->a:[Lmud;

    iput-object v0, p0, Lmwf;->g:[Lmud;

    .line 7107
    sget-object v0, Lmue;->a:[Lmue;

    iput-object v0, p0, Lmwf;->h:[Lmue;

    .line 7110
    sget-object v0, Lmvs;->a:[Lmvs;

    iput-object v0, p0, Lmwf;->i:[Lmvs;

    .line 7119
    iput-object v1, p0, Lmwf;->k:Lmud;

    .line 7126
    iput-object v1, p0, Lmwf;->n:Lmuv;

    .line 7129
    const/high16 v0, -0x80000000

    iput v0, p0, Lmwf;->o:I

    .line 7132
    iput-object v1, p0, Lmwf;->p:Lmwc;

    .line 7135
    iput-object v1, p0, Lmwf;->q:Lmts;

    .line 7089
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 7223
    .line 7224
    iget-object v0, p0, Lmwf;->c:[Lmvz;

    if-eqz v0, :cond_1

    .line 7225
    iget-object v3, p0, Lmwf;->c:[Lmvz;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 7226
    if-eqz v5, :cond_0

    .line 7227
    const/4 v6, 0x1

    .line 7228
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7225
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 7232
    :cond_2
    iget-object v2, p0, Lmwf;->d:[Lmus;

    if-eqz v2, :cond_4

    .line 7233
    iget-object v3, p0, Lmwf;->d:[Lmus;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 7234
    if-eqz v5, :cond_3

    .line 7235
    const/4 v6, 0x2

    .line 7236
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7233
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7240
    :cond_4
    iget-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 7241
    iget-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x8

    .line 7242
    add-int/2addr v0, v2

    .line 7243
    iget-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 7245
    :cond_5
    iget-object v2, p0, Lmwf;->f:[Lmwz;

    if-eqz v2, :cond_7

    .line 7246
    iget-object v3, p0, Lmwf;->f:[Lmwz;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 7247
    if-eqz v5, :cond_6

    .line 7248
    const/4 v6, 0x4

    .line 7249
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7246
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 7253
    :cond_7
    iget-object v2, p0, Lmwf;->g:[Lmud;

    if-eqz v2, :cond_9

    .line 7254
    iget-object v3, p0, Lmwf;->g:[Lmud;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    .line 7255
    if-eqz v5, :cond_8

    .line 7256
    const/4 v6, 0x5

    .line 7257
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7254
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 7261
    :cond_9
    iget-object v2, p0, Lmwf;->h:[Lmue;

    if-eqz v2, :cond_b

    .line 7262
    iget-object v3, p0, Lmwf;->h:[Lmue;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_b

    aget-object v5, v3, v2

    .line 7263
    if-eqz v5, :cond_a

    .line 7264
    const/4 v6, 0x6

    .line 7265
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7262
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 7269
    :cond_b
    iget-object v2, p0, Lmwf;->i:[Lmvs;

    if-eqz v2, :cond_d

    .line 7270
    iget-object v2, p0, Lmwf;->i:[Lmvs;

    array-length v3, v2

    :goto_5
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 7271
    if-eqz v4, :cond_c

    .line 7272
    const/4 v5, 0x7

    .line 7273
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7270
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 7277
    :cond_d
    iget-object v1, p0, Lmwf;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 7278
    const/16 v1, 0x8

    iget-object v2, p0, Lmwf;->a:Ljava/lang/Integer;

    .line 7279
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7281
    :cond_e
    iget-object v1, p0, Lmwf;->j:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 7282
    const/16 v1, 0x9

    iget-object v2, p0, Lmwf;->j:Ljava/lang/String;

    .line 7283
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7285
    :cond_f
    iget-object v1, p0, Lmwf;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 7286
    const/16 v1, 0xa

    iget-object v2, p0, Lmwf;->b:Ljava/lang/Integer;

    .line 7287
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7289
    :cond_10
    iget-object v1, p0, Lmwf;->k:Lmud;

    if-eqz v1, :cond_11

    .line 7290
    const/16 v1, 0xb

    iget-object v2, p0, Lmwf;->k:Lmud;

    .line 7291
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7293
    :cond_11
    iget-object v1, p0, Lmwf;->l:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 7294
    const/16 v1, 0xc

    iget-object v2, p0, Lmwf;->l:Ljava/lang/String;

    .line 7295
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7297
    :cond_12
    iget-object v1, p0, Lmwf;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    .line 7298
    const/16 v1, 0xd

    iget-object v2, p0, Lmwf;->m:Ljava/lang/Integer;

    .line 7299
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7301
    :cond_13
    iget-object v1, p0, Lmwf;->n:Lmuv;

    if-eqz v1, :cond_14

    .line 7302
    const/16 v1, 0xe

    iget-object v2, p0, Lmwf;->n:Lmuv;

    .line 7303
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7305
    :cond_14
    iget-object v1, p0, Lmwf;->p:Lmwc;

    if-eqz v1, :cond_15

    .line 7306
    const/16 v1, 0xf

    iget-object v2, p0, Lmwf;->p:Lmwc;

    .line 7307
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7309
    :cond_15
    iget-object v1, p0, Lmwf;->q:Lmts;

    if-eqz v1, :cond_16

    .line 7310
    const/16 v1, 0x10

    iget-object v2, p0, Lmwf;->q:Lmts;

    .line 7311
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7313
    :cond_16
    iget v1, p0, Lmwf;->o:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_17

    .line 7314
    const/16 v1, 0x21

    iget v2, p0, Lmwf;->o:I

    .line 7315
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7317
    :cond_17
    iget-object v1, p0, Lmwf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7318
    iput v0, p0, Lmwf;->ai:I

    .line 7319
    return v0
.end method

.method public a(Loxn;)Lmwf;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 7327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7328
    sparse-switch v0, :sswitch_data_0

    .line 7332
    iget-object v2, p0, Lmwf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 7333
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmwf;->ah:Ljava/util/List;

    .line 7336
    :cond_1
    iget-object v2, p0, Lmwf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7338
    :sswitch_0
    return-object p0

    .line 7343
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7344
    iget-object v0, p0, Lmwf;->c:[Lmvz;

    if-nez v0, :cond_3

    move v0, v1

    .line 7345
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmvz;

    .line 7346
    iget-object v3, p0, Lmwf;->c:[Lmvz;

    if-eqz v3, :cond_2

    .line 7347
    iget-object v3, p0, Lmwf;->c:[Lmvz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7349
    :cond_2
    iput-object v2, p0, Lmwf;->c:[Lmvz;

    .line 7350
    :goto_2
    iget-object v2, p0, Lmwf;->c:[Lmvz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 7351
    iget-object v2, p0, Lmwf;->c:[Lmvz;

    new-instance v3, Lmvz;

    invoke-direct {v3}, Lmvz;-><init>()V

    aput-object v3, v2, v0

    .line 7352
    iget-object v2, p0, Lmwf;->c:[Lmvz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7353
    invoke-virtual {p1}, Loxn;->a()I

    .line 7350
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7344
    :cond_3
    iget-object v0, p0, Lmwf;->c:[Lmvz;

    array-length v0, v0

    goto :goto_1

    .line 7356
    :cond_4
    iget-object v2, p0, Lmwf;->c:[Lmvz;

    new-instance v3, Lmvz;

    invoke-direct {v3}, Lmvz;-><init>()V

    aput-object v3, v2, v0

    .line 7357
    iget-object v2, p0, Lmwf;->c:[Lmvz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7361
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7362
    iget-object v0, p0, Lmwf;->d:[Lmus;

    if-nez v0, :cond_6

    move v0, v1

    .line 7363
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmus;

    .line 7364
    iget-object v3, p0, Lmwf;->d:[Lmus;

    if-eqz v3, :cond_5

    .line 7365
    iget-object v3, p0, Lmwf;->d:[Lmus;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7367
    :cond_5
    iput-object v2, p0, Lmwf;->d:[Lmus;

    .line 7368
    :goto_4
    iget-object v2, p0, Lmwf;->d:[Lmus;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 7369
    iget-object v2, p0, Lmwf;->d:[Lmus;

    new-instance v3, Lmus;

    invoke-direct {v3}, Lmus;-><init>()V

    aput-object v3, v2, v0

    .line 7370
    iget-object v2, p0, Lmwf;->d:[Lmus;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7371
    invoke-virtual {p1}, Loxn;->a()I

    .line 7368
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 7362
    :cond_6
    iget-object v0, p0, Lmwf;->d:[Lmus;

    array-length v0, v0

    goto :goto_3

    .line 7374
    :cond_7
    iget-object v2, p0, Lmwf;->d:[Lmus;

    new-instance v3, Lmus;

    invoke-direct {v3}, Lmus;-><init>()V

    aput-object v3, v2, v0

    .line 7375
    iget-object v2, p0, Lmwf;->d:[Lmus;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7379
    :sswitch_3
    const/16 v0, 0x19

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7380
    iget-object v0, p0, Lmwf;->e:[Ljava/lang/Long;

    array-length v0, v0

    .line 7381
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    .line 7382
    iget-object v3, p0, Lmwf;->e:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7383
    iput-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    .line 7384
    :goto_5
    iget-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 7385
    iget-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 7386
    invoke-virtual {p1}, Loxn;->a()I

    .line 7384
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 7389
    :cond_8
    iget-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 7393
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7394
    iget-object v0, p0, Lmwf;->f:[Lmwz;

    if-nez v0, :cond_a

    move v0, v1

    .line 7395
    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lmwz;

    .line 7396
    iget-object v3, p0, Lmwf;->f:[Lmwz;

    if-eqz v3, :cond_9

    .line 7397
    iget-object v3, p0, Lmwf;->f:[Lmwz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7399
    :cond_9
    iput-object v2, p0, Lmwf;->f:[Lmwz;

    .line 7400
    :goto_7
    iget-object v2, p0, Lmwf;->f:[Lmwz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 7401
    iget-object v2, p0, Lmwf;->f:[Lmwz;

    new-instance v3, Lmwz;

    invoke-direct {v3}, Lmwz;-><init>()V

    aput-object v3, v2, v0

    .line 7402
    iget-object v2, p0, Lmwf;->f:[Lmwz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7403
    invoke-virtual {p1}, Loxn;->a()I

    .line 7400
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 7394
    :cond_a
    iget-object v0, p0, Lmwf;->f:[Lmwz;

    array-length v0, v0

    goto :goto_6

    .line 7406
    :cond_b
    iget-object v2, p0, Lmwf;->f:[Lmwz;

    new-instance v3, Lmwz;

    invoke-direct {v3}, Lmwz;-><init>()V

    aput-object v3, v2, v0

    .line 7407
    iget-object v2, p0, Lmwf;->f:[Lmwz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7411
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7412
    iget-object v0, p0, Lmwf;->g:[Lmud;

    if-nez v0, :cond_d

    move v0, v1

    .line 7413
    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Lmud;

    .line 7414
    iget-object v3, p0, Lmwf;->g:[Lmud;

    if-eqz v3, :cond_c

    .line 7415
    iget-object v3, p0, Lmwf;->g:[Lmud;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7417
    :cond_c
    iput-object v2, p0, Lmwf;->g:[Lmud;

    .line 7418
    :goto_9
    iget-object v2, p0, Lmwf;->g:[Lmud;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    .line 7419
    iget-object v2, p0, Lmwf;->g:[Lmud;

    new-instance v3, Lmud;

    invoke-direct {v3}, Lmud;-><init>()V

    aput-object v3, v2, v0

    .line 7420
    iget-object v2, p0, Lmwf;->g:[Lmud;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7421
    invoke-virtual {p1}, Loxn;->a()I

    .line 7418
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 7412
    :cond_d
    iget-object v0, p0, Lmwf;->g:[Lmud;

    array-length v0, v0

    goto :goto_8

    .line 7424
    :cond_e
    iget-object v2, p0, Lmwf;->g:[Lmud;

    new-instance v3, Lmud;

    invoke-direct {v3}, Lmud;-><init>()V

    aput-object v3, v2, v0

    .line 7425
    iget-object v2, p0, Lmwf;->g:[Lmud;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7429
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7430
    iget-object v0, p0, Lmwf;->h:[Lmue;

    if-nez v0, :cond_10

    move v0, v1

    .line 7431
    :goto_a
    add-int/2addr v2, v0

    new-array v2, v2, [Lmue;

    .line 7432
    iget-object v3, p0, Lmwf;->h:[Lmue;

    if-eqz v3, :cond_f

    .line 7433
    iget-object v3, p0, Lmwf;->h:[Lmue;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7435
    :cond_f
    iput-object v2, p0, Lmwf;->h:[Lmue;

    .line 7436
    :goto_b
    iget-object v2, p0, Lmwf;->h:[Lmue;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    .line 7437
    iget-object v2, p0, Lmwf;->h:[Lmue;

    new-instance v3, Lmue;

    invoke-direct {v3}, Lmue;-><init>()V

    aput-object v3, v2, v0

    .line 7438
    iget-object v2, p0, Lmwf;->h:[Lmue;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7439
    invoke-virtual {p1}, Loxn;->a()I

    .line 7436
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 7430
    :cond_10
    iget-object v0, p0, Lmwf;->h:[Lmue;

    array-length v0, v0

    goto :goto_a

    .line 7442
    :cond_11
    iget-object v2, p0, Lmwf;->h:[Lmue;

    new-instance v3, Lmue;

    invoke-direct {v3}, Lmue;-><init>()V

    aput-object v3, v2, v0

    .line 7443
    iget-object v2, p0, Lmwf;->h:[Lmue;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7447
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7448
    iget-object v0, p0, Lmwf;->i:[Lmvs;

    if-nez v0, :cond_13

    move v0, v1

    .line 7449
    :goto_c
    add-int/2addr v2, v0

    new-array v2, v2, [Lmvs;

    .line 7450
    iget-object v3, p0, Lmwf;->i:[Lmvs;

    if-eqz v3, :cond_12

    .line 7451
    iget-object v3, p0, Lmwf;->i:[Lmvs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7453
    :cond_12
    iput-object v2, p0, Lmwf;->i:[Lmvs;

    .line 7454
    :goto_d
    iget-object v2, p0, Lmwf;->i:[Lmvs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_14

    .line 7455
    iget-object v2, p0, Lmwf;->i:[Lmvs;

    new-instance v3, Lmvs;

    invoke-direct {v3}, Lmvs;-><init>()V

    aput-object v3, v2, v0

    .line 7456
    iget-object v2, p0, Lmwf;->i:[Lmvs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7457
    invoke-virtual {p1}, Loxn;->a()I

    .line 7454
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 7448
    :cond_13
    iget-object v0, p0, Lmwf;->i:[Lmvs;

    array-length v0, v0

    goto :goto_c

    .line 7460
    :cond_14
    iget-object v2, p0, Lmwf;->i:[Lmvs;

    new-instance v3, Lmvs;

    invoke-direct {v3}, Lmvs;-><init>()V

    aput-object v3, v2, v0

    .line 7461
    iget-object v2, p0, Lmwf;->i:[Lmvs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7465
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwf;->a:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 7469
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwf;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 7473
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwf;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 7477
    :sswitch_b
    iget-object v0, p0, Lmwf;->k:Lmud;

    if-nez v0, :cond_15

    .line 7478
    new-instance v0, Lmud;

    invoke-direct {v0}, Lmud;-><init>()V

    iput-object v0, p0, Lmwf;->k:Lmud;

    .line 7480
    :cond_15
    iget-object v0, p0, Lmwf;->k:Lmud;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7484
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwf;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 7488
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwf;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 7492
    :sswitch_e
    iget-object v0, p0, Lmwf;->n:Lmuv;

    if-nez v0, :cond_16

    .line 7493
    new-instance v0, Lmuv;

    invoke-direct {v0}, Lmuv;-><init>()V

    iput-object v0, p0, Lmwf;->n:Lmuv;

    .line 7495
    :cond_16
    iget-object v0, p0, Lmwf;->n:Lmuv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7499
    :sswitch_f
    iget-object v0, p0, Lmwf;->p:Lmwc;

    if-nez v0, :cond_17

    .line 7500
    new-instance v0, Lmwc;

    invoke-direct {v0}, Lmwc;-><init>()V

    iput-object v0, p0, Lmwf;->p:Lmwc;

    .line 7502
    :cond_17
    iget-object v0, p0, Lmwf;->p:Lmwc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7506
    :sswitch_10
    iget-object v0, p0, Lmwf;->q:Lmts;

    if-nez v0, :cond_18

    .line 7507
    new-instance v0, Lmts;

    invoke-direct {v0}, Lmts;-><init>()V

    iput-object v0, p0, Lmwf;->q:Lmts;

    .line 7509
    :cond_18
    iget-object v0, p0, Lmwf;->q:Lmts;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7513
    :sswitch_11
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 7514
    if-eqz v0, :cond_19

    const/4 v2, 0x1

    if-eq v0, v2, :cond_19

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1a

    .line 7517
    :cond_19
    iput v0, p0, Lmwf;->o:I

    goto/16 :goto_0

    .line 7519
    :cond_1a
    iput v1, p0, Lmwf;->o:I

    goto/16 :goto_0

    .line 7328
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x108 -> :sswitch_11
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 7140
    iget-object v1, p0, Lmwf;->c:[Lmvz;

    if-eqz v1, :cond_1

    .line 7141
    iget-object v2, p0, Lmwf;->c:[Lmvz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 7142
    if-eqz v4, :cond_0

    .line 7143
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7141
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7147
    :cond_1
    iget-object v1, p0, Lmwf;->d:[Lmus;

    if-eqz v1, :cond_3

    .line 7148
    iget-object v2, p0, Lmwf;->d:[Lmus;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 7149
    if-eqz v4, :cond_2

    .line 7150
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7148
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7154
    :cond_3
    iget-object v1, p0, Lmwf;->e:[Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 7155
    iget-object v2, p0, Lmwf;->e:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 7156
    const/4 v5, 0x3

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->c(IJ)V

    .line 7155
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 7159
    :cond_4
    iget-object v1, p0, Lmwf;->f:[Lmwz;

    if-eqz v1, :cond_6

    .line 7160
    iget-object v2, p0, Lmwf;->f:[Lmwz;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 7161
    if-eqz v4, :cond_5

    .line 7162
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7160
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 7166
    :cond_6
    iget-object v1, p0, Lmwf;->g:[Lmud;

    if-eqz v1, :cond_8

    .line 7167
    iget-object v2, p0, Lmwf;->g:[Lmud;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 7168
    if-eqz v4, :cond_7

    .line 7169
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7167
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 7173
    :cond_8
    iget-object v1, p0, Lmwf;->h:[Lmue;

    if-eqz v1, :cond_a

    .line 7174
    iget-object v2, p0, Lmwf;->h:[Lmue;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 7175
    if-eqz v4, :cond_9

    .line 7176
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7174
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 7180
    :cond_a
    iget-object v1, p0, Lmwf;->i:[Lmvs;

    if-eqz v1, :cond_c

    .line 7181
    iget-object v1, p0, Lmwf;->i:[Lmvs;

    array-length v2, v1

    :goto_6
    if-ge v0, v2, :cond_c

    aget-object v3, v1, v0

    .line 7182
    if-eqz v3, :cond_b

    .line 7183
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 7181
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 7187
    :cond_c
    iget-object v0, p0, Lmwf;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 7188
    const/16 v0, 0x8

    iget-object v1, p0, Lmwf;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7190
    :cond_d
    iget-object v0, p0, Lmwf;->j:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 7191
    const/16 v0, 0x9

    iget-object v1, p0, Lmwf;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7193
    :cond_e
    iget-object v0, p0, Lmwf;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 7194
    const/16 v0, 0xa

    iget-object v1, p0, Lmwf;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7196
    :cond_f
    iget-object v0, p0, Lmwf;->k:Lmud;

    if-eqz v0, :cond_10

    .line 7197
    const/16 v0, 0xb

    iget-object v1, p0, Lmwf;->k:Lmud;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7199
    :cond_10
    iget-object v0, p0, Lmwf;->l:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 7200
    const/16 v0, 0xc

    iget-object v1, p0, Lmwf;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7202
    :cond_11
    iget-object v0, p0, Lmwf;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 7203
    const/16 v0, 0xd

    iget-object v1, p0, Lmwf;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7205
    :cond_12
    iget-object v0, p0, Lmwf;->n:Lmuv;

    if-eqz v0, :cond_13

    .line 7206
    const/16 v0, 0xe

    iget-object v1, p0, Lmwf;->n:Lmuv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7208
    :cond_13
    iget-object v0, p0, Lmwf;->p:Lmwc;

    if-eqz v0, :cond_14

    .line 7209
    const/16 v0, 0xf

    iget-object v1, p0, Lmwf;->p:Lmwc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7211
    :cond_14
    iget-object v0, p0, Lmwf;->q:Lmts;

    if-eqz v0, :cond_15

    .line 7212
    const/16 v0, 0x10

    iget-object v1, p0, Lmwf;->q:Lmts;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7214
    :cond_15
    iget v0, p0, Lmwf;->o:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_16

    .line 7215
    const/16 v0, 0x21

    iget v1, p0, Lmwf;->o:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7217
    :cond_16
    iget-object v0, p0, Lmwf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7219
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7085
    invoke-virtual {p0, p1}, Lmwf;->a(Loxn;)Lmwf;

    move-result-object v0

    return-object v0
.end method
