.class public final Ldkq;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmdk;",
        "Lmdl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:I

.field private q:Lncj;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)V
    .locals 6

    .prologue
    .line 37
    if-eqz p6, :cond_0

    const-string v3, "getuseritemsdeltabackground"

    :goto_0
    new-instance v4, Lmdk;

    invoke-direct {v4}, Lmdk;-><init>()V

    new-instance v5, Lmdl;

    invoke-direct {v5}, Lmdl;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 39
    iput-object p4, p0, Ldkq;->a:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Ldkq;->b:Ljava/lang/String;

    .line 41
    iput p5, p0, Ldkq;->p:I

    .line 42
    return-void

    .line 37
    :cond_0
    const-string v3, "getuseritemsdelta"

    goto :goto_0
.end method


# virtual methods
.method protected a(Lmdk;)V
    .locals 3

    .prologue
    .line 46
    new-instance v0, Lnci;

    invoke-direct {v0}, Lnci;-><init>()V

    iput-object v0, p1, Lmdk;->a:Lnci;

    .line 47
    iget-object v0, p1, Lmdk;->a:Lnci;

    .line 48
    iget v1, p0, Ldkq;->p:I

    iput v1, v0, Lnci;->a:I

    .line 49
    new-instance v1, Lndj;

    invoke-direct {v1}, Lndj;-><init>()V

    iput-object v1, v0, Lnci;->b:Lndj;

    .line 50
    iget-object v1, v0, Lnci;->b:Lndj;

    iget-object v2, p0, Ldkq;->b:Ljava/lang/String;

    iput-object v2, v1, Lndj;->a:Ljava/lang/String;

    .line 51
    iget-object v1, p0, Ldkq;->a:Ljava/lang/String;

    iput-object v1, v0, Lnci;->c:Ljava/lang/String;

    .line 52
    return-void
.end method

.method protected a(Lmdl;)V
    .locals 7

    .prologue
    .line 57
    iget-object v0, p1, Lmdl;->a:Lncj;

    iput-object v0, p0, Ldkq;->q:Lncj;

    .line 58
    const-string v0, "GetUserItemsDelta"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "response:  resumeToken: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldkq;->q:Lncj;

    iget-object v1, v1, Lncj;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldkq;->q:Lncj;

    iget-object v2, v2, Lncj;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldkq;->q:Lncj;

    iget-object v3, v3, Lncj;->c:[Lnzx;

    array-length v3, v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x23

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " syncToken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " num tiles; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 64
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmdk;

    invoke-virtual {p0, p1}, Ldkq;->a(Lmdk;)V

    return-void
.end method

.method public b()Lnyz;
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->d:[Lnyz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->d:[Lnyz;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->d:[Lnyz;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmdl;

    invoke-virtual {p0, p1}, Ldkq;->a(Lmdl;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()[Lnzx;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->c:[Lnzx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->c:[Lnzx;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lnzx;

    goto :goto_0
.end method

.method public f()[Lotf;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->e:[Lotf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkq;->q:Lncj;

    iget-object v0, v0, Lncj;->e:[Lotf;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lotf;

    goto :goto_0
.end method
