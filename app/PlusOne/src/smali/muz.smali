.class public final Lmuz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:[Ljava/lang/Integer;

.field private c:Lmtv;

.field private d:[Lmtv;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13254
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13259
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lmuz;->b:[Ljava/lang/Integer;

    .line 13262
    const/4 v0, 0x0

    iput-object v0, p0, Lmuz;->c:Lmtv;

    .line 13265
    sget-object v0, Lmtv;->a:[Lmtv;

    iput-object v0, p0, Lmuz;->d:[Lmtv;

    .line 13254
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 13309
    .line 13310
    iget-object v0, p0, Lmuz;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 13311
    const/4 v0, 0x1

    iget-object v2, p0, Lmuz;->a:Ljava/lang/Integer;

    .line 13312
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13314
    :goto_0
    iget-object v2, p0, Lmuz;->b:[Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmuz;->b:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 13316
    iget-object v4, p0, Lmuz;->b:[Ljava/lang/Integer;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 13318
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 13316
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 13320
    :cond_0
    add-int/2addr v0, v3

    .line 13321
    iget-object v2, p0, Lmuz;->b:[Ljava/lang/Integer;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 13323
    :cond_1
    iget-object v2, p0, Lmuz;->c:Lmtv;

    if-eqz v2, :cond_2

    .line 13324
    const/4 v2, 0x3

    iget-object v3, p0, Lmuz;->c:Lmtv;

    .line 13325
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 13327
    :cond_2
    iget-object v2, p0, Lmuz;->d:[Lmtv;

    if-eqz v2, :cond_4

    .line 13328
    iget-object v2, p0, Lmuz;->d:[Lmtv;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 13329
    if-eqz v4, :cond_3

    .line 13330
    const/4 v5, 0x4

    .line 13331
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 13328
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 13335
    :cond_4
    iget-object v1, p0, Lmuz;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 13336
    const/4 v1, 0x5

    iget-object v2, p0, Lmuz;->e:Ljava/lang/String;

    .line 13337
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13339
    :cond_5
    iget-object v1, p0, Lmuz;->f:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 13340
    const/4 v1, 0x6

    iget-object v2, p0, Lmuz;->f:Ljava/lang/String;

    .line 13341
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13343
    :cond_6
    iget-object v1, p0, Lmuz;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 13344
    const/4 v1, 0x7

    iget-object v2, p0, Lmuz;->g:Ljava/lang/String;

    .line 13345
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13347
    :cond_7
    iget-object v1, p0, Lmuz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13348
    iput v0, p0, Lmuz;->ai:I

    .line 13349
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmuz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13357
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 13358
    sparse-switch v0, :sswitch_data_0

    .line 13362
    iget-object v2, p0, Lmuz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 13363
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmuz;->ah:Ljava/util/List;

    .line 13366
    :cond_1
    iget-object v2, p0, Lmuz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13368
    :sswitch_0
    return-object p0

    .line 13373
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuz;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 13377
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 13378
    iget-object v0, p0, Lmuz;->b:[Ljava/lang/Integer;

    array-length v0, v0

    .line 13379
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    .line 13380
    iget-object v3, p0, Lmuz;->b:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 13381
    iput-object v2, p0, Lmuz;->b:[Ljava/lang/Integer;

    .line 13382
    :goto_1
    iget-object v2, p0, Lmuz;->b:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 13383
    iget-object v2, p0, Lmuz;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 13384
    invoke-virtual {p1}, Loxn;->a()I

    .line 13382
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 13387
    :cond_2
    iget-object v2, p0, Lmuz;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 13391
    :sswitch_3
    iget-object v0, p0, Lmuz;->c:Lmtv;

    if-nez v0, :cond_3

    .line 13392
    new-instance v0, Lmtv;

    invoke-direct {v0}, Lmtv;-><init>()V

    iput-object v0, p0, Lmuz;->c:Lmtv;

    .line 13394
    :cond_3
    iget-object v0, p0, Lmuz;->c:Lmtv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 13398
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 13399
    iget-object v0, p0, Lmuz;->d:[Lmtv;

    if-nez v0, :cond_5

    move v0, v1

    .line 13400
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lmtv;

    .line 13401
    iget-object v3, p0, Lmuz;->d:[Lmtv;

    if-eqz v3, :cond_4

    .line 13402
    iget-object v3, p0, Lmuz;->d:[Lmtv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 13404
    :cond_4
    iput-object v2, p0, Lmuz;->d:[Lmtv;

    .line 13405
    :goto_3
    iget-object v2, p0, Lmuz;->d:[Lmtv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 13406
    iget-object v2, p0, Lmuz;->d:[Lmtv;

    new-instance v3, Lmtv;

    invoke-direct {v3}, Lmtv;-><init>()V

    aput-object v3, v2, v0

    .line 13407
    iget-object v2, p0, Lmuz;->d:[Lmtv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 13408
    invoke-virtual {p1}, Loxn;->a()I

    .line 13405
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 13399
    :cond_5
    iget-object v0, p0, Lmuz;->d:[Lmtv;

    array-length v0, v0

    goto :goto_2

    .line 13411
    :cond_6
    iget-object v2, p0, Lmuz;->d:[Lmtv;

    new-instance v3, Lmtv;

    invoke-direct {v3}, Lmtv;-><init>()V

    aput-object v3, v2, v0

    .line 13412
    iget-object v2, p0, Lmuz;->d:[Lmtv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 13416
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmuz;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 13420
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmuz;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 13424
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmuz;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 13358
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 13276
    iget-object v1, p0, Lmuz;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 13277
    const/4 v1, 0x1

    iget-object v2, p0, Lmuz;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 13279
    :cond_0
    iget-object v1, p0, Lmuz;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 13280
    iget-object v2, p0, Lmuz;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 13281
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 13280
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13284
    :cond_1
    iget-object v1, p0, Lmuz;->c:Lmtv;

    if-eqz v1, :cond_2

    .line 13285
    const/4 v1, 0x3

    iget-object v2, p0, Lmuz;->c:Lmtv;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 13287
    :cond_2
    iget-object v1, p0, Lmuz;->d:[Lmtv;

    if-eqz v1, :cond_4

    .line 13288
    iget-object v1, p0, Lmuz;->d:[Lmtv;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 13289
    if-eqz v3, :cond_3

    .line 13290
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 13288
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 13294
    :cond_4
    iget-object v0, p0, Lmuz;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 13295
    const/4 v0, 0x5

    iget-object v1, p0, Lmuz;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 13297
    :cond_5
    iget-object v0, p0, Lmuz;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 13298
    const/4 v0, 0x6

    iget-object v1, p0, Lmuz;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 13300
    :cond_6
    iget-object v0, p0, Lmuz;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 13301
    const/4 v0, 0x7

    iget-object v1, p0, Lmuz;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 13303
    :cond_7
    iget-object v0, p0, Lmuz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 13305
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 13250
    invoke-virtual {p0, p1}, Lmuz;->a(Loxn;)Lmuz;

    move-result-object v0

    return-object v0
.end method
