.class final Lcol;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lene;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lene",
        "<",
        "Ljava/lang/Long;",
        "Lcom/google/android/libraries/social/media/MediaResource;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcof;


# direct methods
.method constructor <init>(Lcof;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Lcol;->a:Lcof;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 474
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 475
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 477
    iget-object v1, p0, Lcol;->a:Lcof;

    invoke-static {v1}, Lcof;->a(Lcof;)Landroid/content/Context;

    move-result-object v1

    const-class v4, Lcpi;

    invoke-static {v1, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcpi;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v0, Lcph;->a:Lcph;

    invoke-virtual {v1, v4, v5, v0}, Lcpi;->a(JLcph;)Lcpg;

    move-result-object v1

    .line 479
    if-eqz v1, :cond_0

    .line 480
    iget-object v0, p0, Lcol;->a:Lcof;

    invoke-static {v0}, Lcof;->b(Lcof;)Lcom;

    move-result-object v0

    iget v0, v0, Lcom;->b:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 485
    iget-object v0, p0, Lcol;->a:Lcof;

    invoke-static {v0}, Lcof;->c(Lcof;)Lizs;

    move-result-object v0

    iget-object v1, v1, Lcpg;->f:Lizu;

    iget-object v4, p0, Lcol;->a:Lcof;

    .line 486
    invoke-static {v4}, Lcof;->b(Lcof;)Lcom;

    move-result-object v4

    iget v4, v4, Lcom;->c:I

    iget-object v5, p0, Lcol;->a:Lcof;

    invoke-static {v5}, Lcof;->b(Lcof;)Lcom;

    move-result-object v5

    iget v5, v5, Lcom;->d:I

    iget-object v6, p0, Lcol;->a:Lcof;

    .line 487
    invoke-static {v6}, Lcof;->b(Lcof;)Lcom;

    move-result-object v6

    iget v6, v6, Lcom;->a:I

    .line 485
    invoke-virtual {v0, v1, v4, v5, v6}, Lizs;->a(Lizu;III)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    .line 494
    :goto_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 480
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 489
    :cond_2
    iget-object v0, p0, Lcol;->a:Lcof;

    invoke-static {v0}, Lcof;->c(Lcof;)Lizs;

    move-result-object v0

    iget-object v1, v1, Lcpg;->f:Lizu;

    iget-object v4, p0, Lcol;->a:Lcof;

    .line 490
    invoke-static {v4}, Lcof;->b(Lcof;)Lcom;

    move-result-object v4

    iget v4, v4, Lcom;->b:I

    iget-object v5, p0, Lcol;->a:Lcof;

    .line 491
    invoke-static {v5}, Lcof;->b(Lcof;)Lcom;

    move-result-object v5

    iget v5, v5, Lcom;->a:I

    .line 489
    invoke-virtual {v0, v1, v4, v5}, Lizs;->c(Lizu;II)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    goto :goto_2

    .line 496
    :cond_3
    return-object v2
.end method

.method public b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 501
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/MediaResource;

    .line 502
    iget-object v2, p0, Lcol;->a:Lcof;

    invoke-static {v2}, Lcof;->c(Lcof;)Lizs;

    move-result-object v2

    invoke-virtual {v2, v0}, Lizs;->a(Lcom/google/android/libraries/social/media/MediaResource;)V

    goto :goto_0

    .line 504
    :cond_0
    return-void
.end method
