.class public Lasy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livo;


# instance fields
.field private final a:Livo;

.field private final b:Latd;

.field private final c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Livo;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Livo;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Latc;

.field private final f:Latc;

.field private final g:Late;

.field private final h:Late;

.field private final i:Late;

.field private final j:Livd;

.field private final k:Livo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lasy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Livo;Latd;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lasz;

    invoke-direct {v0, p0}, Lasz;-><init>(Lasy;)V

    iput-object v0, p0, Lasy;->k:Livo;

    .line 93
    const-string v0, "historicalAnalysisPerformanceLogs"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livo;

    iput-object v0, p0, Lasy;->a:Livo;

    .line 95
    const-string v0, "settings"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latd;

    iput-object v0, p0, Lasy;->b:Latd;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lasy;->c:Ljava/util/Collection;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lasy;->d:Ljava/util/Collection;

    .line 100
    new-instance v0, Latc;

    const-string v1, "No analysis is active"

    invoke-direct {v0, v1}, Latc;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lasy;->e:Latc;

    .line 101
    new-instance v0, Latc;

    const-string v1, "No playback loop is active"

    invoke-direct {v0, v1}, Latc;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lasy;->f:Latc;

    .line 103
    new-instance v0, Late;

    const-string v1, "No active storyboard"

    invoke-direct {v0, v1}, Late;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lasy;->g:Late;

    .line 104
    new-instance v0, Late;

    const-string v1, "No startup intent"

    invoke-direct {v0, v1}, Late;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lasy;->h:Late;

    .line 105
    new-instance v0, Late;

    const-string v1, "No player screen controller"

    invoke-direct {v0, v1}, Late;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lasy;->i:Late;

    .line 107
    new-instance v0, Livd;

    invoke-direct {v0}, Livd;-><init>()V

    iput-object v0, p0, Lasy;->j:Livd;

    .line 108
    return-void
.end method

.method static synthetic a(Lasy;)Latd;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lasy;->b:Latd;

    return-object v0
.end method

.method static synthetic b(Lasy;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lasy;->d:Ljava/util/Collection;

    return-object v0
.end method

.method static synthetic c(Lasy;)Latc;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lasy;->f:Latc;

    return-object v0
.end method

.method static synthetic d(Lasy;)Late;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lasy;->g:Late;

    return-object v0
.end method

.method static synthetic e(Lasy;)Late;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lasy;->i:Late;

    return-object v0
.end method

.method static synthetic f(Lasy;)Livd;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lasy;->j:Livd;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lasy;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 182
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 158
    if-nez p1, :cond_0

    .line 171
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lasy;->h:Late;

    new-instance v1, Lata;

    invoke-direct {v1, p1}, Lata;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Late;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Lboi;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lasy;->g:Late;

    invoke-virtual {v0, p1}, Late;->a(Ljava/lang/Object;)V

    .line 154
    return-void
.end method

.method public a(Livf;)V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lasy;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 199
    return-void
.end method

.method public a(Livm;)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lasy;->b:Latd;

    invoke-interface {v0}, Latd;->ad()Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lasy;->a:Livo;

    invoke-interface {v0, p1}, Livo;->a(Livm;)V

    .line 211
    iget-object v0, p0, Lasy;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livo;

    .line 212
    invoke-interface {v0, p1}, Livo;->a(Livm;)V

    goto :goto_1

    .line 214
    :cond_1
    iget-object v0, p0, Lasy;->e:Latc;

    invoke-virtual {v0, p1}, Latc;->a(Livm;)V

    .line 215
    iget-object v0, p0, Lasy;->h:Late;

    invoke-virtual {v0, p1}, Late;->a(Livm;)V

    .line 216
    iget-object v0, p0, Lasy;->k:Livo;

    invoke-interface {v0, p1}, Livo;->a(Livm;)V

    goto :goto_0
.end method

.method public a(Livo;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lasy;->e:Latc;

    invoke-virtual {v0, p1}, Latc;->a(Livo;)V

    .line 146
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lasy;->i:Late;

    invoke-virtual {v0, p1}, Late;->a(Ljava/lang/Object;)V

    .line 175
    return-void
.end method

.method public b()Livo;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lasy;->k:Livo;

    return-object v0
.end method

.method public b(Livo;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lasy;->f:Latc;

    invoke-virtual {v0, p1}, Latc;->a(Livo;)V

    .line 150
    return-void
.end method
