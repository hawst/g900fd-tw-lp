.class public final Lnxh;
.super Loxq;
.source "PG"


# instance fields
.field public a:[I

.field public b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnxh;->a:[I

    .line 16
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnxh;->b:[Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 37
    .line 38
    iget-object v0, p0, Lnxh;->a:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lnxh;->a:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 40
    iget-object v3, p0, Lnxh;->a:[I

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    .line 42
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_0
    iget-object v0, p0, Lnxh;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 47
    :goto_1
    iget-object v2, p0, Lnxh;->b:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnxh;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 49
    iget-object v3, p0, Lnxh;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 51
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 49
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 53
    :cond_1
    add-int/2addr v0, v2

    .line 54
    iget-object v1, p0, Lnxh;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 56
    :cond_2
    iget-object v1, p0, Lnxh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    iput v0, p0, Lnxh;->ai:I

    .line 58
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lnxh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 67
    sparse-switch v0, :sswitch_data_0

    .line 71
    iget-object v1, p0, Lnxh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnxh;->ah:Ljava/util/List;

    .line 75
    :cond_1
    iget-object v1, p0, Lnxh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    :sswitch_0
    return-object p0

    .line 82
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 83
    iget-object v0, p0, Lnxh;->a:[I

    array-length v0, v0

    .line 84
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 85
    iget-object v2, p0, Lnxh;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    iput-object v1, p0, Lnxh;->a:[I

    .line 87
    :goto_1
    iget-object v1, p0, Lnxh;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 88
    iget-object v1, p0, Lnxh;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 89
    invoke-virtual {p1}, Loxn;->a()I

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 92
    :cond_2
    iget-object v1, p0, Lnxh;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 96
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 97
    iget-object v0, p0, Lnxh;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 98
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 99
    iget-object v2, p0, Lnxh;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    iput-object v1, p0, Lnxh;->b:[Ljava/lang/String;

    .line 101
    :goto_2
    iget-object v1, p0, Lnxh;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 102
    iget-object v1, p0, Lnxh;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 103
    invoke-virtual {p1}, Loxn;->a()I

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 106
    :cond_3
    iget-object v1, p0, Lnxh;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 21
    iget-object v1, p0, Lnxh;->a:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnxh;->a:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 22
    iget-object v2, p0, Lnxh;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 23
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 22
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 26
    :cond_0
    iget-object v1, p0, Lnxh;->b:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 27
    iget-object v1, p0, Lnxh;->b:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 28
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 31
    :cond_1
    iget-object v0, p0, Lnxh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnxh;->a(Loxn;)Lnxh;

    move-result-object v0

    return-object v0
.end method
