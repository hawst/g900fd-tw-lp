.class public final Lpoi;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpoi;


# instance fields
.field private b:I

.field private c:Lpof;

.field private d:[Lpof;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x0

    new-array v0, v0, [Lpoi;

    sput-object v0, Lpoi;->a:[Lpoi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 542
    invoke-direct {p0}, Loxq;-><init>()V

    .line 556
    const/high16 v0, -0x80000000

    iput v0, p0, Lpoi;->b:I

    .line 559
    const/4 v0, 0x0

    iput-object v0, p0, Lpoi;->c:Lpof;

    .line 562
    sget-object v0, Lpof;->a:[Lpof;

    iput-object v0, p0, Lpoi;->d:[Lpof;

    .line 542
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 586
    .line 587
    iget v0, p0, Lpoi;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_3

    .line 588
    const/4 v0, 0x1

    iget v2, p0, Lpoi;->b:I

    .line 589
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 591
    :goto_0
    iget-object v2, p0, Lpoi;->c:Lpof;

    if-eqz v2, :cond_0

    .line 592
    const/4 v2, 0x2

    iget-object v3, p0, Lpoi;->c:Lpof;

    .line 593
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 595
    :cond_0
    iget-object v2, p0, Lpoi;->d:[Lpof;

    if-eqz v2, :cond_2

    .line 596
    iget-object v2, p0, Lpoi;->d:[Lpof;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 597
    if-eqz v4, :cond_1

    .line 598
    const/4 v5, 0x3

    .line 599
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 596
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 603
    :cond_2
    iget-object v1, p0, Lpoi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 604
    iput v0, p0, Lpoi;->ai:I

    .line 605
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpoi;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 613
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 614
    sparse-switch v0, :sswitch_data_0

    .line 618
    iget-object v2, p0, Lpoi;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 619
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpoi;->ah:Ljava/util/List;

    .line 622
    :cond_1
    iget-object v2, p0, Lpoi;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 624
    :sswitch_0
    return-object p0

    .line 629
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 630
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 638
    :cond_2
    iput v0, p0, Lpoi;->b:I

    goto :goto_0

    .line 640
    :cond_3
    iput v4, p0, Lpoi;->b:I

    goto :goto_0

    .line 645
    :sswitch_2
    iget-object v0, p0, Lpoi;->c:Lpof;

    if-nez v0, :cond_4

    .line 646
    new-instance v0, Lpof;

    invoke-direct {v0}, Lpof;-><init>()V

    iput-object v0, p0, Lpoi;->c:Lpof;

    .line 648
    :cond_4
    iget-object v0, p0, Lpoi;->c:Lpof;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 652
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 653
    iget-object v0, p0, Lpoi;->d:[Lpof;

    if-nez v0, :cond_6

    move v0, v1

    .line 654
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpof;

    .line 655
    iget-object v3, p0, Lpoi;->d:[Lpof;

    if-eqz v3, :cond_5

    .line 656
    iget-object v3, p0, Lpoi;->d:[Lpof;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 658
    :cond_5
    iput-object v2, p0, Lpoi;->d:[Lpof;

    .line 659
    :goto_2
    iget-object v2, p0, Lpoi;->d:[Lpof;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 660
    iget-object v2, p0, Lpoi;->d:[Lpof;

    new-instance v3, Lpof;

    invoke-direct {v3}, Lpof;-><init>()V

    aput-object v3, v2, v0

    .line 661
    iget-object v2, p0, Lpoi;->d:[Lpof;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 662
    invoke-virtual {p1}, Loxn;->a()I

    .line 659
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 653
    :cond_6
    iget-object v0, p0, Lpoi;->d:[Lpof;

    array-length v0, v0

    goto :goto_1

    .line 665
    :cond_7
    iget-object v2, p0, Lpoi;->d:[Lpof;

    new-instance v3, Lpof;

    invoke-direct {v3}, Lpof;-><init>()V

    aput-object v3, v2, v0

    .line 666
    iget-object v2, p0, Lpoi;->d:[Lpof;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 614
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 567
    iget v0, p0, Lpoi;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 568
    const/4 v0, 0x1

    iget v1, p0, Lpoi;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 570
    :cond_0
    iget-object v0, p0, Lpoi;->c:Lpof;

    if-eqz v0, :cond_1

    .line 571
    const/4 v0, 0x2

    iget-object v1, p0, Lpoi;->c:Lpof;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 573
    :cond_1
    iget-object v0, p0, Lpoi;->d:[Lpof;

    if-eqz v0, :cond_3

    .line 574
    iget-object v1, p0, Lpoi;->d:[Lpof;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 575
    if-eqz v3, :cond_2

    .line 576
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 574
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 580
    :cond_3
    iget-object v0, p0, Lpoi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 582
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 538
    invoke-virtual {p0, p1}, Lpoi;->a(Loxn;)Lpoi;

    move-result-object v0

    return-object v0
.end method
