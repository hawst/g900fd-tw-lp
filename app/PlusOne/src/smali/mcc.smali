.class public final Lmcc;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnhp;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29725
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29728
    iput-object v0, p0, Lmcc;->apiHeader:Llyq;

    .line 29731
    iput-object v0, p0, Lmcc;->a:Lnhp;

    .line 29725
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29748
    const/4 v0, 0x0

    .line 29749
    iget-object v1, p0, Lmcc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 29750
    const/4 v0, 0x1

    iget-object v1, p0, Lmcc;->apiHeader:Llyq;

    .line 29751
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29753
    :cond_0
    iget-object v1, p0, Lmcc;->a:Lnhp;

    if-eqz v1, :cond_1

    .line 29754
    const/4 v1, 0x2

    iget-object v2, p0, Lmcc;->a:Lnhp;

    .line 29755
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29757
    :cond_1
    iget-object v1, p0, Lmcc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29758
    iput v0, p0, Lmcc;->ai:I

    .line 29759
    return v0
.end method

.method public a(Loxn;)Lmcc;
    .locals 2

    .prologue
    .line 29767
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29768
    sparse-switch v0, :sswitch_data_0

    .line 29772
    iget-object v1, p0, Lmcc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29773
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcc;->ah:Ljava/util/List;

    .line 29776
    :cond_1
    iget-object v1, p0, Lmcc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29778
    :sswitch_0
    return-object p0

    .line 29783
    :sswitch_1
    iget-object v0, p0, Lmcc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 29784
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmcc;->apiHeader:Llyq;

    .line 29786
    :cond_2
    iget-object v0, p0, Lmcc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29790
    :sswitch_2
    iget-object v0, p0, Lmcc;->a:Lnhp;

    if-nez v0, :cond_3

    .line 29791
    new-instance v0, Lnhp;

    invoke-direct {v0}, Lnhp;-><init>()V

    iput-object v0, p0, Lmcc;->a:Lnhp;

    .line 29793
    :cond_3
    iget-object v0, p0, Lmcc;->a:Lnhp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29768
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29736
    iget-object v0, p0, Lmcc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 29737
    const/4 v0, 0x1

    iget-object v1, p0, Lmcc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29739
    :cond_0
    iget-object v0, p0, Lmcc;->a:Lnhp;

    if-eqz v0, :cond_1

    .line 29740
    const/4 v0, 0x2

    iget-object v1, p0, Lmcc;->a:Lnhp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29742
    :cond_1
    iget-object v0, p0, Lmcc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29744
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29721
    invoke-virtual {p0, p1}, Lmcc;->a(Loxn;)Lmcc;

    move-result-object v0

    return-object v0
.end method
