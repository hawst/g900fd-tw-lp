.class public final Lmvg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmvg;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Boolean;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9574
    const/4 v0, 0x0

    new-array v0, v0, [Lmvg;

    sput-object v0, Lmvg;->a:[Lmvg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9575
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9584
    const/high16 v0, -0x80000000

    iput v0, p0, Lmvg;->e:I

    .line 9575
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9607
    const/4 v0, 0x0

    .line 9608
    iget-object v1, p0, Lmvg;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 9609
    const/4 v0, 0x1

    iget-object v1, p0, Lmvg;->b:Ljava/lang/Integer;

    .line 9610
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9612
    :cond_0
    iget-object v1, p0, Lmvg;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 9613
    const/4 v1, 0x2

    iget-object v2, p0, Lmvg;->c:Ljava/lang/Integer;

    .line 9614
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9616
    :cond_1
    iget-object v1, p0, Lmvg;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 9617
    const/4 v1, 0x3

    iget-object v2, p0, Lmvg;->d:Ljava/lang/Boolean;

    .line 9618
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9620
    :cond_2
    iget v1, p0, Lmvg;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 9621
    const/4 v1, 0x4

    iget v2, p0, Lmvg;->e:I

    .line 9622
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9624
    :cond_3
    iget-object v1, p0, Lmvg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9625
    iput v0, p0, Lmvg;->ai:I

    .line 9626
    return v0
.end method

.method public a(Loxn;)Lmvg;
    .locals 2

    .prologue
    .line 9634
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9635
    sparse-switch v0, :sswitch_data_0

    .line 9639
    iget-object v1, p0, Lmvg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9640
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvg;->ah:Ljava/util/List;

    .line 9643
    :cond_1
    iget-object v1, p0, Lmvg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9645
    :sswitch_0
    return-object p0

    .line 9650
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvg;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 9654
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvg;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 9658
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmvg;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 9662
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9663
    if-eqz v0, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x64

    if-eq v0, v1, :cond_2

    const/16 v1, 0x66

    if-eq v0, v1, :cond_2

    const/16 v1, 0x67

    if-eq v0, v1, :cond_2

    const/16 v1, 0x68

    if-eq v0, v1, :cond_2

    const/16 v1, 0x69

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x78

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x77

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x86

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x87

    if-eq v0, v1, :cond_2

    const/16 v1, 0x79

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x80

    if-eq v0, v1, :cond_2

    const/16 v1, 0x81

    if-eq v0, v1, :cond_2

    const/16 v1, 0x82

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83

    if-eq v0, v1, :cond_2

    const/16 v1, 0x89

    if-eq v0, v1, :cond_2

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x70

    if-eq v0, v1, :cond_2

    const/16 v1, 0x71

    if-eq v0, v1, :cond_2

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_2

    const/16 v1, 0x75

    if-eq v0, v1, :cond_2

    const/16 v1, 0x76

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x85

    if-eq v0, v1, :cond_2

    const/16 v1, 0x88

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x92

    if-eq v0, v1, :cond_2

    const/16 v1, 0x93

    if-eq v0, v1, :cond_2

    const/16 v1, 0x95

    if-eq v0, v1, :cond_2

    const/16 v1, 0x97

    if-eq v0, v1, :cond_2

    const/16 v1, 0x98

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x90

    if-eq v0, v1, :cond_2

    const/16 v1, 0x91

    if-eq v0, v1, :cond_2

    const/16 v1, 0x99

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x94

    if-eq v0, v1, :cond_2

    const/16 v1, 0x96

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa4

    if-ne v0, v1, :cond_3

    .line 9733
    :cond_2
    iput v0, p0, Lmvg;->e:I

    goto/16 :goto_0

    .line 9735
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmvg;->e:I

    goto/16 :goto_0

    .line 9635
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9589
    iget-object v0, p0, Lmvg;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 9590
    const/4 v0, 0x1

    iget-object v1, p0, Lmvg;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9592
    :cond_0
    iget-object v0, p0, Lmvg;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 9593
    const/4 v0, 0x2

    iget-object v1, p0, Lmvg;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9595
    :cond_1
    iget-object v0, p0, Lmvg;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 9596
    const/4 v0, 0x3

    iget-object v1, p0, Lmvg;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 9598
    :cond_2
    iget v0, p0, Lmvg;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 9599
    const/4 v0, 0x4

    iget v1, p0, Lmvg;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9601
    :cond_3
    iget-object v0, p0, Lmvg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9603
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9571
    invoke-virtual {p0, p1}, Lmvg;->a(Loxn;)Lmvg;

    move-result-object v0

    return-object v0
.end method
