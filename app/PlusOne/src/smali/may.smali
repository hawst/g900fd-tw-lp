.class public final Lmay;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyf;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2185
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2188
    iput-object v0, p0, Lmay;->apiHeader:Llyq;

    .line 2191
    iput-object v0, p0, Lmay;->a:Lmyf;

    .line 2185
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2208
    const/4 v0, 0x0

    .line 2209
    iget-object v1, p0, Lmay;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 2210
    const/4 v0, 0x1

    iget-object v1, p0, Lmay;->apiHeader:Llyq;

    .line 2211
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2213
    :cond_0
    iget-object v1, p0, Lmay;->a:Lmyf;

    if-eqz v1, :cond_1

    .line 2214
    const/4 v1, 0x2

    iget-object v2, p0, Lmay;->a:Lmyf;

    .line 2215
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2217
    :cond_1
    iget-object v1, p0, Lmay;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2218
    iput v0, p0, Lmay;->ai:I

    .line 2219
    return v0
.end method

.method public a(Loxn;)Lmay;
    .locals 2

    .prologue
    .line 2227
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2228
    sparse-switch v0, :sswitch_data_0

    .line 2232
    iget-object v1, p0, Lmay;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2233
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmay;->ah:Ljava/util/List;

    .line 2236
    :cond_1
    iget-object v1, p0, Lmay;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2238
    :sswitch_0
    return-object p0

    .line 2243
    :sswitch_1
    iget-object v0, p0, Lmay;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 2244
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmay;->apiHeader:Llyq;

    .line 2246
    :cond_2
    iget-object v0, p0, Lmay;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2250
    :sswitch_2
    iget-object v0, p0, Lmay;->a:Lmyf;

    if-nez v0, :cond_3

    .line 2251
    new-instance v0, Lmyf;

    invoke-direct {v0}, Lmyf;-><init>()V

    iput-object v0, p0, Lmay;->a:Lmyf;

    .line 2253
    :cond_3
    iget-object v0, p0, Lmay;->a:Lmyf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2228
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2196
    iget-object v0, p0, Lmay;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 2197
    const/4 v0, 0x1

    iget-object v1, p0, Lmay;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2199
    :cond_0
    iget-object v0, p0, Lmay;->a:Lmyf;

    if-eqz v0, :cond_1

    .line 2200
    const/4 v0, 0x2

    iget-object v1, p0, Lmay;->a:Lmyf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2202
    :cond_1
    iget-object v0, p0, Lmay;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2204
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2181
    invoke-virtual {p0, p1}, Lmay;->a(Loxn;)Lmay;

    move-result-object v0

    return-object v0
.end method
