.class public final Lnqs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnqt;

.field public b:[Llww;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Loxq;-><init>()V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lnqs;->a:Lnqt;

    .line 166
    sget-object v0, Llww;->a:[Llww;

    iput-object v0, p0, Lnqs;->b:[Llww;

    .line 160
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 187
    .line 188
    iget-object v0, p0, Lnqs;->a:Lnqt;

    if-eqz v0, :cond_2

    .line 189
    const/4 v0, 0x1

    iget-object v2, p0, Lnqs;->a:Lnqt;

    .line 190
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 192
    :goto_0
    iget-object v2, p0, Lnqs;->b:[Llww;

    if-eqz v2, :cond_1

    .line 193
    iget-object v2, p0, Lnqs;->b:[Llww;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 194
    if-eqz v4, :cond_0

    .line 195
    const/4 v5, 0x2

    .line 196
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 193
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 200
    :cond_1
    iget-object v1, p0, Lnqs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    iput v0, p0, Lnqs;->ai:I

    .line 202
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnqs;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 210
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 211
    sparse-switch v0, :sswitch_data_0

    .line 215
    iget-object v2, p0, Lnqs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 216
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnqs;->ah:Ljava/util/List;

    .line 219
    :cond_1
    iget-object v2, p0, Lnqs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    :sswitch_0
    return-object p0

    .line 226
    :sswitch_1
    iget-object v0, p0, Lnqs;->a:Lnqt;

    if-nez v0, :cond_2

    .line 227
    new-instance v0, Lnqt;

    invoke-direct {v0}, Lnqt;-><init>()V

    iput-object v0, p0, Lnqs;->a:Lnqt;

    .line 229
    :cond_2
    iget-object v0, p0, Lnqs;->a:Lnqt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 233
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 234
    iget-object v0, p0, Lnqs;->b:[Llww;

    if-nez v0, :cond_4

    move v0, v1

    .line 235
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llww;

    .line 236
    iget-object v3, p0, Lnqs;->b:[Llww;

    if-eqz v3, :cond_3

    .line 237
    iget-object v3, p0, Lnqs;->b:[Llww;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    :cond_3
    iput-object v2, p0, Lnqs;->b:[Llww;

    .line 240
    :goto_2
    iget-object v2, p0, Lnqs;->b:[Llww;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 241
    iget-object v2, p0, Lnqs;->b:[Llww;

    new-instance v3, Llww;

    invoke-direct {v3}, Llww;-><init>()V

    aput-object v3, v2, v0

    .line 242
    iget-object v2, p0, Lnqs;->b:[Llww;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 243
    invoke-virtual {p1}, Loxn;->a()I

    .line 240
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 234
    :cond_4
    iget-object v0, p0, Lnqs;->b:[Llww;

    array-length v0, v0

    goto :goto_1

    .line 246
    :cond_5
    iget-object v2, p0, Lnqs;->b:[Llww;

    new-instance v3, Llww;

    invoke-direct {v3}, Llww;-><init>()V

    aput-object v3, v2, v0

    .line 247
    iget-object v2, p0, Lnqs;->b:[Llww;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 211
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 171
    iget-object v0, p0, Lnqs;->a:Lnqt;

    if-eqz v0, :cond_0

    .line 172
    const/4 v0, 0x1

    iget-object v1, p0, Lnqs;->a:Lnqt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 174
    :cond_0
    iget-object v0, p0, Lnqs;->b:[Llww;

    if-eqz v0, :cond_2

    .line 175
    iget-object v1, p0, Lnqs;->b:[Llww;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 176
    if-eqz v3, :cond_1

    .line 177
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 175
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_2
    iget-object v0, p0, Lnqs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 183
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lnqs;->a(Loxn;)Lnqs;

    move-result-object v0

    return-object v0
.end method
