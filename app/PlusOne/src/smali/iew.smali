.class public final Liew;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lloy;

.field private static final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lloy;

    const-string v1, "debug.app.status"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Liew;->a:Lloy;

    .line 69
    const-string v0, "https://support.google.com/mobile/?p=plus_survey_android"

    .line 70
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Liew;->b:Landroid/net/Uri;

    .line 69
    return-void
.end method

.method static a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    .line 93
    if-ge v0, p1, :cond_0

    .line 103
    :goto_0
    return-object p0

    .line 97
    :cond_0
    int-to-double v0, v0

    int-to-double v2, p1

    div-double/2addr v0, v2

    .line 98
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 100
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/2addr v1, v0

    .line 101
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int v0, v2, v0

    .line 103
    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Llrw;->a(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lifb;->a:Lifb;

    invoke-static {p0, v0}, Liew;->a(Landroid/app/Activity;Lifb;)V

    .line 178
    return-void
.end method

.method public static a(Landroid/app/Activity;Lifb;)V
    .locals 3

    .prologue
    .line 187
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 188
    sget-object v0, Liew;->a:Lloy;

    .line 191
    const-class v0, Lijl;

    invoke-static {v2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lijl;

    .line 196
    const-class v1, Likp;

    .line 197
    invoke-static {v2, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Likp;

    invoke-interface {v1}, Likp;->a()Liko;

    move-result-object v1

    .line 196
    invoke-interface {v0, v1}, Lijl;->a(Lijj;)Lijl;

    move-result-object v0

    .line 197
    invoke-interface {v0}, Lijl;->a()Lijk;

    move-result-object v0

    .line 198
    invoke-interface {v0}, Lijk;->b()V

    .line 200
    new-instance v1, Liex;

    invoke-direct {v1, p0, p1, v2, v0}, Liex;-><init>(Landroid/app/Activity;Lifb;Landroid/content/Context;Lijk;)V

    invoke-interface {v0, v1}, Lijk;->a(Lijm;)V

    .line 265
    new-instance v1, Liey;

    invoke-direct {v1, p0}, Liey;-><init>(Landroid/app/Activity;)V

    invoke-interface {v0, v1}, Lijk;->a(Lijn;)V

    .line 273
    return-void
.end method

.method static synthetic b(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 44
    sget-object v0, Liew;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v2, "version"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "unknown"

    goto :goto_0
.end method
