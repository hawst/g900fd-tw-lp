.class public final Lhqv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 168
    const/high16 v0, -0x80000000

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lhqv;->a:Ljava/lang/String;

    .line 210
    const-string v0, "content://com.google.android.apps.plus.iu.IuStatus"

    .line 211
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 213
    const-string v1, "uploads"

    .line 214
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lhqv;->b:Landroid/net/Uri;

    .line 213
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 217
    const-string v0, "media"

    invoke-static {p0, v0}, Lhqv;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;J)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 249
    const-string v0, "uploads"

    invoke-static {p0, v0}, Lhqv;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 237
    const-string v1, "content://"

    invoke-static {p0}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    packed-switch p0, :pswitch_data_0

    .line 292
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 260
    :pswitch_0
    const-string v0, "SYNC_STATE_IDLE"

    goto :goto_0

    .line 262
    :pswitch_1
    const-string v0, "SYNC_STATE_SYNCING"

    goto :goto_0

    .line 264
    :pswitch_2
    const-string v0, "SYNC_STATE_REJECT_ON_WIFI"

    goto :goto_0

    .line 266
    :pswitch_3
    const-string v0, "SYNC_STATE_REJECT_ON_ROAMING"

    goto :goto_0

    .line 268
    :pswitch_4
    const-string v0, "SYNC_STATE_REJECT_ON_POWER"

    goto :goto_0

    .line 270
    :pswitch_5
    const-string v0, "SYNC_STATE_REJECT_ON_USER_AUTH"

    goto :goto_0

    .line 272
    :pswitch_6
    const-string v0, "SYNC_STATE_REJECT_ON_AUTO_SYNC"

    goto :goto_0

    .line 274
    :pswitch_7
    const-string v0, "SYNC_STATE_REJECT_ON_DISABLED_DOWNSYNC"

    goto :goto_0

    .line 276
    :pswitch_8
    const-string v0, "SYNC_STATE_REJECT_ON_BACKGROUND_DATA"

    goto :goto_0

    .line 278
    :pswitch_9
    const-string v0, "SYNC_STATE_STOP_ON_QUOTA_REACHED"

    goto :goto_0

    .line 280
    :pswitch_a
    const-string v0, "SYNC_STATE_STOP_ON_USER_AUTH"

    goto :goto_0

    .line 282
    :pswitch_b
    const-string v0, "SYNC_STATE_WAIT_ON_SDCARD"

    goto :goto_0

    .line 284
    :pswitch_c
    const-string v0, "SYNC_STATE_STOP_ON_SDCARD"

    goto :goto_0

    .line 286
    :pswitch_d
    const-string v0, "SYNC_STATE_YIELD"

    goto :goto_0

    .line 288
    :pswitch_e
    const-string v0, "SYNC_STATE_STOP_ON_NETWORK"

    goto :goto_0

    .line 290
    :pswitch_f
    const-string v0, "SYNC_STATE_STOP_ON_IOE"

    goto :goto_0

    .line 258
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 301
    const-string v0, "upload_all"

    invoke-static {p0, v0}, Lhqv;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 302
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 303
    const-string v2, "account_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 304
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 305
    return-void
.end method

.method public static b(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 221
    const-string v0, "settings"

    invoke-static {p0, v0}, Lhqv;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 225
    const-string v0, "iu"

    invoke-static {p0, v0}, Lhqv;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 229
    const-string v0, "upload_all"

    invoke-static {p0, v0}, Lhqv;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 233
    const-string v0, "uploads"

    invoke-static {p0, v0}, Lhqv;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    const-class v0, Lhph;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhph;

    invoke-interface {v0}, Lhph;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static g(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 253
    const-class v0, Lhpo;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpo;

    .line 254
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lhpo;->b(J)V

    .line 255
    return-void
.end method
