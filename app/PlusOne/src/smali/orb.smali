.class public final Lorb;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 640
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 671
    const/4 v0, 0x0

    .line 672
    iget-object v1, p0, Lorb;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 673
    const/4 v0, 0x1

    iget-object v1, p0, Lorb;->a:Ljava/lang/Long;

    .line 674
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 676
    :cond_0
    iget-object v1, p0, Lorb;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 677
    const/4 v1, 0x2

    iget-object v2, p0, Lorb;->b:Ljava/lang/Long;

    .line 678
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 680
    :cond_1
    iget-object v1, p0, Lorb;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 681
    const/4 v1, 0x3

    iget-object v2, p0, Lorb;->c:Ljava/lang/String;

    .line 682
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 684
    :cond_2
    iget-object v1, p0, Lorb;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 685
    const/4 v1, 0x4

    iget-object v2, p0, Lorb;->d:Ljava/lang/Integer;

    .line 686
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 688
    :cond_3
    iget-object v1, p0, Lorb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 689
    iput v0, p0, Lorb;->ai:I

    .line 690
    return v0
.end method

.method public a(Loxn;)Lorb;
    .locals 2

    .prologue
    .line 698
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 699
    sparse-switch v0, :sswitch_data_0

    .line 703
    iget-object v1, p0, Lorb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 704
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorb;->ah:Ljava/util/List;

    .line 707
    :cond_1
    iget-object v1, p0, Lorb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 709
    :sswitch_0
    return-object p0

    .line 714
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lorb;->a:Ljava/lang/Long;

    goto :goto_0

    .line 718
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lorb;->b:Ljava/lang/Long;

    goto :goto_0

    .line 722
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorb;->c:Ljava/lang/String;

    goto :goto_0

    .line 726
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lorb;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 699
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 653
    iget-object v0, p0, Lorb;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 654
    const/4 v0, 0x1

    iget-object v1, p0, Lorb;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 656
    :cond_0
    iget-object v0, p0, Lorb;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 657
    const/4 v0, 0x2

    iget-object v1, p0, Lorb;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 659
    :cond_1
    iget-object v0, p0, Lorb;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 660
    const/4 v0, 0x3

    iget-object v1, p0, Lorb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 662
    :cond_2
    iget-object v0, p0, Lorb;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 663
    const/4 v0, 0x4

    iget-object v1, p0, Lorb;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 665
    :cond_3
    iget-object v0, p0, Lorb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 667
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 636
    invoke-virtual {p0, p1}, Lorb;->a(Loxn;)Lorb;

    move-result-object v0

    return-object v0
.end method
