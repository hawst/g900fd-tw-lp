.class public final Lkcq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->getWidth()I

    move-result v4

    .line 39
    const v0, 0x7fffffff

    .line 40
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iget-object v5, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-static {v5}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->a(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)J

    move-result-wide v6

    sub-long/2addr v2, v6

    long-to-float v2, v2

    .line 41
    invoke-static {}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->a()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v3, v2

    move v2, v0

    move v0, v1

    .line 43
    :goto_0
    iget-object v5, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-static {v5}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->b(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 44
    iget-object v5, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-static {v5}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)[Landroid/graphics/Rect;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-virtual {v5, v3, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 45
    iget-object v5, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-static {v5}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)[Landroid/graphics/Rect;

    move-result-object v5

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    iget-object v0, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-static {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->b(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v0, v1

    :goto_1
    if-ltz v3, :cond_2

    .line 50
    iget-object v5, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-static {v5}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)[Landroid/graphics/Rect;

    move-result-object v5

    aget-object v5, v5, v3

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-le v5, v4, :cond_1

    .line 51
    add-int/lit8 v0, v0, 0x1

    invoke-static {}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->b()I

    move-result v5

    iget-object v6, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-static {v6}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)[Landroid/graphics/Rect;

    move-result-object v6

    aget-object v6, v6, v3

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/2addr v5, v6

    mul-int/2addr v5, v0

    sub-int v5, v2, v5

    .line 52
    iget-object v6, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-static {v6}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)[Landroid/graphics/Rect;

    move-result-object v6

    aget-object v6, v6, v3

    invoke-virtual {v6, v5, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 49
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 56
    :cond_2
    iget-object v0, p0, Lkcq;->a:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->invalidate()V

    .line 57
    return-void
.end method
