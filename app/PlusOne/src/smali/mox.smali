.class public final Lmox;
.super Loxq;
.source "PG"


# instance fields
.field public a:Locf;

.field private b:Lltu;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 178
    invoke-direct {p0}, Loxq;-><init>()V

    .line 181
    iput-object v0, p0, Lmox;->b:Lltu;

    .line 186
    iput-object v0, p0, Lmox;->a:Locf;

    .line 178
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 206
    const/4 v0, 0x0

    .line 207
    iget-object v1, p0, Lmox;->b:Lltu;

    if-eqz v1, :cond_0

    .line 208
    const/4 v0, 0x5

    iget-object v1, p0, Lmox;->b:Lltu;

    .line 209
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 211
    :cond_0
    iget-object v1, p0, Lmox;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 212
    const/4 v1, 0x6

    iget-object v2, p0, Lmox;->c:Ljava/lang/String;

    .line 213
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_1
    iget-object v1, p0, Lmox;->a:Locf;

    if-eqz v1, :cond_2

    .line 216
    const/4 v1, 0x7

    iget-object v2, p0, Lmox;->a:Locf;

    .line 217
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_2
    iget-object v1, p0, Lmox;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    iput v0, p0, Lmox;->ai:I

    .line 221
    return v0
.end method

.method public a(Loxn;)Lmox;
    .locals 2

    .prologue
    .line 229
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 230
    sparse-switch v0, :sswitch_data_0

    .line 234
    iget-object v1, p0, Lmox;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 235
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmox;->ah:Ljava/util/List;

    .line 238
    :cond_1
    iget-object v1, p0, Lmox;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    :sswitch_0
    return-object p0

    .line 245
    :sswitch_1
    iget-object v0, p0, Lmox;->b:Lltu;

    if-nez v0, :cond_2

    .line 246
    new-instance v0, Lltu;

    invoke-direct {v0}, Lltu;-><init>()V

    iput-object v0, p0, Lmox;->b:Lltu;

    .line 248
    :cond_2
    iget-object v0, p0, Lmox;->b:Lltu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 252
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmox;->c:Ljava/lang/String;

    goto :goto_0

    .line 256
    :sswitch_3
    iget-object v0, p0, Lmox;->a:Locf;

    if-nez v0, :cond_3

    .line 257
    new-instance v0, Locf;

    invoke-direct {v0}, Locf;-><init>()V

    iput-object v0, p0, Lmox;->a:Locf;

    .line 259
    :cond_3
    iget-object v0, p0, Lmox;->a:Locf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 230
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2a -> :sswitch_1
        0x32 -> :sswitch_2
        0x3a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lmox;->b:Lltu;

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x5

    iget-object v1, p0, Lmox;->b:Lltu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 194
    :cond_0
    iget-object v0, p0, Lmox;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 195
    const/4 v0, 0x6

    iget-object v1, p0, Lmox;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 197
    :cond_1
    iget-object v0, p0, Lmox;->a:Locf;

    if-eqz v0, :cond_2

    .line 198
    const/4 v0, 0x7

    iget-object v1, p0, Lmox;->a:Locf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 200
    :cond_2
    iget-object v0, p0, Lmox;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 202
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lmox;->a(Loxn;)Lmox;

    move-result-object v0

    return-object v0
.end method
