.class public final Lesv;
.super Lhye;
.source "PG"


# static fields
.field private static b:[Ljava/lang/String;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    sput-object v0, Lesv;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I[Lnhm;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 45
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lesv;->b:[Ljava/lang/String;

    .line 46
    invoke-static {p3}, Lesv;->a([Lnhm;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v6, v5

    .line 45
    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iput p2, p0, Lesv;->c:I

    .line 49
    return-void
.end method

.method private D()Ljava/util/HashSet;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p0}, Lesv;->n()Landroid/content/Context;

    move-result-object v0

    .line 101
    iget v2, p0, Lesv;->c:I

    .line 102
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 103
    const/4 v1, 0x0

    .line 105
    const/4 v4, 0x7

    const/4 v5, 0x1

    :try_start_0
    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "circle_id"

    aput-object v7, v5, v6

    invoke-static {v0, v2, v4, v5}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 108
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 111
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 114
    :cond_1
    if-eqz v1, :cond_2

    .line 115
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 119
    :cond_2
    const-string v0, "FriendLocations"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "initSelectedCircles: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_3
    return-object v3

    .line 114
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 115
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private static a([Lnhm;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 57
    :cond_0
    const-string v0, "0"

    .line 68
    :goto_0
    return-object v0

    .line 60
    :cond_1
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 61
    const-string v0, "gaia_id IN ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    array-length v2, p0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, p0, v0

    .line 63
    iget-object v3, v3, Lnhm;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 64
    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 66
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 67
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 73
    invoke-virtual {p0}, Lesv;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lesv;->c:I

    invoke-static {v0, v1}, Ldsm;->b(Landroid/content/Context;I)Z

    .line 74
    invoke-super {p0}, Lhye;->C()Landroid/database/Cursor;

    move-result-object v1

    .line 76
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 77
    const-string v3, "circle_ids"

    iget v0, p0, Lesv;->c:I

    invoke-virtual {p0}, Lesv;->n()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v0}, Ldhv;->q(Landroid/content/Context;I)Ljava/util/HashSet;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lesv;->D()Ljava/util/HashSet;

    move-result-object v0

    :cond_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 78
    invoke-static {v1, v2}, Lhyc;->a(Landroid/database/Cursor;Landroid/os/Bundle;)Lhyc;

    move-result-object v0

    return-object v0
.end method
