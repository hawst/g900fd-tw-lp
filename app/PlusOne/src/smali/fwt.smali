.class public final Lfwt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdd;
.implements Llip;


# static fields
.field private static b:Landroid/graphics/Paint;


# instance fields
.field private final c:Landroid/view/View;

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/graphics/RectF;

.field private f:Lhsn;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private i:Lkda;

.field private j:Z

.field private final k:Ljava/lang/String;

.field private final l:I

.field private final m:I


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 76
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lfwt;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhsn;I)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhsn;I)V
    .locals 9

    .prologue
    .line 91
    const/4 v5, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v8}, Lfwt;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhsn;II)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhsn;II)V
    .locals 3

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lfwt;->c:Landroid/view/View;

    .line 111
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 112
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lfwt;->d:Landroid/graphics/Rect;

    .line 113
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lfwt;->e:Landroid/graphics/RectF;

    .line 114
    iput-object p6, p0, Lfwt;->f:Lhsn;

    .line 115
    iput-object p2, p0, Lfwt;->g:Ljava/lang/String;

    .line 116
    iput-object p4, p0, Lfwt;->h:Ljava/lang/String;

    .line 118
    iput-object p3, p0, Lfwt;->k:Ljava/lang/String;

    .line 119
    iput p7, p0, Lfwt;->l:I

    .line 120
    iput p8, p0, Lfwt;->m:I

    .line 122
    sget-object v1, Lfwt;->b:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    .line 123
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 124
    sput-object v1, Lfwt;->b:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 125
    sget-object v1, Lfwt;->b:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 126
    sget-object v1, Lfwt;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0139

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    sget-object v0, Lfwt;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 130
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Llip;Llip;)I
    .locals 1

    .prologue
    .line 302
    sget-object v0, Llip;->a_:Lliq;

    invoke-virtual {v0, p1, p2}, Lliq;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method

.method public a()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lfwt;->d:Landroid/graphics/Rect;

    return-object v0
.end method

.method public a(IIII)V
    .locals 5

    .prologue
    .line 177
    iget-object v0, p0, Lfwt;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 178
    iget-object v0, p0, Lfwt;->e:Landroid/graphics/RectF;

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 179
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 215
    iget v0, p0, Lfwt;->m:I

    packed-switch v0, :pswitch_data_0

    .line 229
    :goto_0
    return-void

    .line 217
    :pswitch_0
    iget-object v0, p0, Lfwt;->d:Landroid/graphics/Rect;

    sget-object v1, Lfwt;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 220
    :pswitch_1
    iget-object v0, p0, Lfwt;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lfwt;->d:Landroid/graphics/Rect;

    .line 221
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lfwt;->d:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget-object v3, Lfwt;->b:Landroid/graphics/Paint;

    .line 220
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 224
    :pswitch_2
    iget-object v0, p0, Lfwt;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 225
    invoke-static {v0}, Lhss;->i(Landroid/content/Context;)F

    move-result v0

    .line 226
    iget-object v1, p0, Lfwt;->e:Landroid/graphics/RectF;

    sget-object v2, Lfwt;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v0, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lhsn;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lfwt;->f:Lhsn;

    .line 198
    return-void
.end method

.method public a(Lkda;)V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lfwt;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 155
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 297
    iput-boolean p1, p0, Lfwt;->j:Z

    .line 298
    return-void
.end method

.method public a(III)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 238
    const/4 v0, 0x3

    if-ne p3, v0, :cond_0

    .line 239
    iput-boolean v2, p0, Lfwt;->j:Z

    move v0, v1

    .line 274
    :goto_0
    return v0

    .line 243
    :cond_0
    iget-object v0, p0, Lfwt;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_2

    .line 244
    if-ne p3, v1, :cond_1

    .line 245
    iput-boolean v2, p0, Lfwt;->j:Z

    :cond_1
    move v0, v2

    .line 247
    goto :goto_0

    .line 250
    :cond_2
    packed-switch p3, :pswitch_data_0

    :goto_1
    move v0, v1

    .line 274
    goto :goto_0

    .line 252
    :pswitch_0
    iput-boolean v1, p0, Lfwt;->j:Z

    goto :goto_1

    .line 257
    :pswitch_1
    iget-boolean v0, p0, Lfwt;->j:Z

    if-eqz v0, :cond_3

    .line 258
    iget-object v0, p0, Lfwt;->f:Lhsn;

    if-eqz v0, :cond_4

    .line 259
    iget-object v0, p0, Lfwt;->f:Lhsn;

    iget-object v3, p0, Lfwt;->g:Ljava/lang/String;

    invoke-interface {v0, v3}, Lhsn;->a(Ljava/lang/String;)V

    .line 268
    :cond_3
    :goto_2
    iput-boolean v2, p0, Lfwt;->j:Z

    goto :goto_1

    .line 261
    :cond_4
    iget-object v0, p0, Lfwt;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lhsn;

    invoke-static {v0, v3}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsn;

    .line 263
    if-eqz v0, :cond_3

    .line 264
    iget-object v3, p0, Lfwt;->g:Ljava/lang/String;

    invoke-interface {v0, v3}, Lhsn;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 250
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()V
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Lfwt;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhso;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    .line 135
    iget-object v1, p0, Lfwt;->k:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 136
    iget-object v1, p0, Lfwt;->k:Ljava/lang/String;

    iget v2, p0, Lfwt;->l:I

    iget v3, p0, Lfwt;->m:I

    invoke-interface {v0, v1, v2, v3, p0}, Lhso;->a(Ljava/lang/String;IILkdd;)Lkda;

    move-result-object v0

    iput-object v0, p0, Lfwt;->i:Lkda;

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v1, p0, Lfwt;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lfwt;->g:Ljava/lang/String;

    iget v1, p0, Lfwt;->l:I

    iget v2, p0, Lfwt;->m:I

    invoke-interface {v0, v1, v2, p0}, Lhso;->a(IILkdd;)Lkda;

    move-result-object v0

    iput-object v0, p0, Lfwt;->i:Lkda;

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lfwt;->i:Lkda;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lfwt;->i:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lfwt;->i:Lkda;

    .line 150
    :cond_0
    return-void
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 28
    check-cast p1, Llip;

    check-cast p2, Llip;

    invoke-virtual {p0, p1, p2}, Lfwt;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method

.method public d()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lfwt;->i:Lkda;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfwt;->i:Lkda;

    invoke-virtual {v0}, Lkda;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 162
    iget-object v0, p0, Lfwt;->i:Lkda;

    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 165
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lfwt;->j:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const-string v1, " gaia id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lfwt;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string v1, " name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lfwt;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    iget-object v1, p0, Lfwt;->c:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 312
    const-string v1, " view: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lfwt;->c:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 313
    const-string v1, " context: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lfwt;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 315
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
