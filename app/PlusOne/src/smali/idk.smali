.class public final Lidk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;


# instance fields
.field private c:Lgk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgk",
            "<",
            "Lidl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lidm;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    invoke-static {p1}, Lidk;->a(Landroid/content/Context;)V

    .line 150
    return-void
.end method

.method public static a(Ljava/util/TimeZone;Ljava/util/Calendar;)J
    .locals 7

    .prologue
    .line 227
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 228
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 229
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 230
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 231
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 232
    const/16 v5, 0xc

    invoke-virtual {p1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 233
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 234
    mul-int/lit8 v0, v0, 0x3c

    add-int/2addr v0, v6

    const v6, 0xea60

    mul-int/2addr v6, v0

    move-object v0, p0

    .line 236
    invoke-virtual/range {v0 .. v6}, Ljava/util/TimeZone;->getOffset(IIIIII)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method private static a([Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Lgk;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Calendar;",
            ")",
            "Lgk",
            "<",
            "Lidl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    new-instance v9, Lgk;

    invoke-direct {v9}, Lgk;-><init>()V

    .line 187
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v4

    .line 188
    invoke-static {v4, p2}, Lidk;->a(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v5

    .line 190
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    move v8, v0

    :goto_0
    if-ltz v8, :cond_1

    .line 191
    aget-object v0, p0, v8

    invoke-static {v0}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 192
    invoke-static {v1, p2}, Lidk;->a(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v2

    .line 193
    invoke-virtual {v9, v2, v3}, Lgk;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidl;

    .line 195
    if-nez v0, :cond_0

    .line 196
    new-instance v0, Lidl;

    invoke-direct {v0}, Lidl;-><init>()V

    .line 197
    invoke-virtual {v9, v2, v3, v0}, Lgk;->b(JLjava/lang/Object;)V

    :cond_0
    move-object v7, p1

    .line 200
    invoke-virtual/range {v0 .. v7}, Lidl;->a(Ljava/util/TimeZone;JLjava/util/TimeZone;JLjava/lang/String;)V

    .line 190
    add-int/lit8 v0, v8, -0x1

    move v8, v0

    goto :goto_0

    .line 204
    :cond_1
    invoke-virtual {v9}, Lgk;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 205
    invoke-virtual {v9, v1}, Lgk;->b(I)J

    move-result-wide v2

    .line 206
    invoke-virtual {v9, v2, v3}, Lgk;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidl;

    .line 207
    invoke-virtual {v0}, Lidl;->a()V

    .line 204
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 210
    :cond_2
    return-object v9
.end method

.method public static a(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;
    .locals 6

    .prologue
    .line 359
    invoke-static {p0}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    if-eqz v0, :cond_1

    invoke-static {v1, p1}, Lidk;->a(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v2

    invoke-static {v0, p1}, Lidk;->a(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lidk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p2, :cond_0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/util/TimeZone;
    .locals 2

    .prologue
    .line 330
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 336
    :cond_0
    :goto_0
    return-object v0

    .line 333
    :cond_1
    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 334
    if-eqz v0, :cond_2

    .line 335
    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lidk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 336
    :cond_2
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 153
    sget-object v0, Lidk;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 154
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a044e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidk;->a:Ljava/lang/String;

    .line 155
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a044f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidk;->b:Ljava/lang/String;

    .line 158
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 244
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 245
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 248
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Ljava/lang/Long;)Lidm;
    .locals 14

    .prologue
    .line 252
    iget-object v0, p0, Lidk;->c:Lgk;

    invoke-virtual {v0}, Lgk;->b()I

    move-result v5

    .line 253
    const/4 v0, 0x0

    .line 255
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p2, :cond_1

    .line 256
    invoke-virtual {p0}, Lidk;->b()Lidm;

    move-result-object v0

    .line 312
    :cond_0
    :goto_0
    return-object v0

    .line 259
    :cond_1
    invoke-static {p1}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 260
    const/4 v1, 0x0

    .line 262
    if-eqz v2, :cond_9

    .line 263
    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lidk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 264
    invoke-virtual {v2}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 266
    iget-object v3, p0, Lidk;->e:Ljava/util/Calendar;

    invoke-static {v2, v3}, Lidk;->a(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    move-object v4, v1

    .line 273
    :goto_1
    if-lez v5, :cond_0

    .line 274
    const/4 v1, 0x0

    .line 277
    if-eqz p2, :cond_8

    .line 278
    iget-object v1, p0, Lidk;->c:Lgk;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgk;->b(J)I

    move-result v1

    .line 280
    if-gez v1, :cond_8

    .line 281
    const/4 v1, 0x0

    move v2, v1

    move-object v1, v0

    .line 286
    :goto_2
    if-ge v2, v5, :cond_6

    .line 287
    iget-object v0, p0, Lidk;->c:Lgk;

    invoke-virtual {v0, v2}, Lgk;->b(I)J

    move-result-wide v6

    .line 288
    iget-object v0, p0, Lidk;->c:Lgk;

    .line 289
    invoke-virtual {v0, v6, v7}, Lgk;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidl;

    invoke-virtual {v0}, Lidl;->b()Ljava/util/ArrayList;

    move-result-object v8

    .line 290
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 292
    const/4 v0, 0x0

    move v3, v0

    :goto_3
    if-ge v3, v9, :cond_5

    .line 293
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidm;

    .line 296
    invoke-virtual {v0}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    .line 299
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    cmp-long v11, v6, v12

    if-nez v11, :cond_2

    if-nez v10, :cond_0

    .line 300
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 302
    :cond_2
    if-nez v10, :cond_3

    if-nez v3, :cond_7

    .line 292
    :cond_3
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_3

    .line 267
    :cond_4
    if-nez p2, :cond_9

    .line 269
    invoke-virtual {p0}, Lidk;->b()Lidm;

    move-result-object v0

    goto/16 :goto_0

    .line 307
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 308
    goto :goto_2

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_4

    :cond_8
    move v2, v1

    move-object v1, v0

    goto :goto_2

    :cond_9
    move-object v4, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/util/TimeZone;)J
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lidk;->e:Ljava/util/Calendar;

    invoke-static {p1, v0}, Lidk;->a(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lidm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lidk;->d:Ljava/util/List;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;)Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 345
    invoke-direct {p0, p1, p2}, Lidk;->c(Ljava/lang/String;Ljava/lang/Long;)Lidm;

    move-result-object v0

    .line 346
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lidm;->a()Ljava/util/TimeZone;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/Calendar;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 161
    iput-object p1, p0, Lidk;->e:Ljava/util/Calendar;

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lidk;->d:Ljava/util/List;

    .line 164
    invoke-static {}, Ljava/util/TimeZone;->getAvailableIDs()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lidk;->b:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lidk;->a([Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Lgk;

    move-result-object v0

    iput-object v0, p0, Lidk;->c:Lgk;

    .line 166
    iget-object v0, p0, Lidk;->c:Lgk;

    invoke-virtual {v0}, Lgk;->b()I

    move-result v6

    move v5, v2

    move v1, v2

    .line 169
    :goto_0
    if-ge v5, v6, :cond_1

    .line 170
    iget-object v0, p0, Lidk;->c:Lgk;

    invoke-virtual {v0, v5}, Lgk;->b(I)J

    move-result-wide v8

    .line 171
    iget-object v0, p0, Lidk;->c:Lgk;

    invoke-virtual {v0, v8, v9}, Lgk;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidl;

    .line 172
    invoke-virtual {v0}, Lidl;->b()Ljava/util/ArrayList;

    move-result-object v7

    .line 173
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move v3, v1

    move v1, v2

    .line 175
    :goto_1
    if-ge v1, v8, :cond_0

    .line 176
    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidm;

    .line 177
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0, v3}, Lidm;->a(I)V

    .line 178
    iget-object v3, p0, Lidk;->d:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_1

    .line 169
    :cond_0
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v1, v3

    goto :goto_0

    .line 181
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/Long;)I
    .locals 1

    .prologue
    .line 354
    invoke-direct {p0, p1, p2}, Lidk;->c(Ljava/lang/String;Ljava/lang/Long;)Lidm;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lidm;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public b()Lidm;
    .locals 4

    .prologue
    .line 316
    iget-object v0, p0, Lidk;->e:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lidk;->e:Ljava/util/Calendar;

    invoke-static {v0, v2}, Lidk;->a(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lidk;->c(Ljava/lang/String;Ljava/lang/Long;)Lidm;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lidk;->a(Ljava/lang/String;Ljava/lang/Long;)Ljava/util/TimeZone;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0}, Lidk;->b()Lidm;

    move-result-object v0

    .line 322
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lidm;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lidk;->b(Ljava/lang/String;Ljava/lang/Long;)I

    move-result v0

    return v0
.end method
