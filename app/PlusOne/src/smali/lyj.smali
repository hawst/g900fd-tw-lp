.class public final Llyj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 321
    const/4 v0, 0x0

    .line 322
    iget-object v1, p0, Llyj;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 323
    const/4 v0, 0x1

    iget-object v1, p0, Llyj;->a:Ljava/lang/String;

    .line 324
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 326
    :cond_0
    iget-object v1, p0, Llyj;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 327
    const/4 v1, 0x2

    iget-object v2, p0, Llyj;->b:Ljava/lang/String;

    .line 328
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_1
    iget-object v1, p0, Llyj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    iput v0, p0, Llyj;->ai:I

    .line 332
    return v0
.end method

.method public a(Loxn;)Llyj;
    .locals 2

    .prologue
    .line 340
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 341
    sparse-switch v0, :sswitch_data_0

    .line 345
    iget-object v1, p0, Llyj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 346
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyj;->ah:Ljava/util/List;

    .line 349
    :cond_1
    iget-object v1, p0, Llyj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    :sswitch_0
    return-object p0

    .line 356
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyj;->a:Ljava/lang/String;

    goto :goto_0

    .line 360
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyj;->b:Ljava/lang/String;

    goto :goto_0

    .line 341
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Llyj;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 310
    const/4 v0, 0x1

    iget-object v1, p0, Llyj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 312
    :cond_0
    iget-object v0, p0, Llyj;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 313
    const/4 v0, 0x2

    iget-object v1, p0, Llyj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 315
    :cond_1
    iget-object v0, p0, Llyj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 317
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0, p1}, Llyj;->a(Loxn;)Llyj;

    move-result-object v0

    return-object v0
.end method
