.class public final Lfjx;
.super Llol;
.source "PG"

# interfaces
.implements Lfja;
.implements Lhmq;
.implements Ljma;
.implements Lkgr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lfja;",
        "Lhmq;",
        "Ljma",
        "<",
        "Lhpu;",
        ">;",
        "Lkgr;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;

.field private static final O:[Ljava/lang/String;

.field private static final P:Landroid/net/Uri;

.field private static Q:Z

.field private static R:Landroid/content/IntentFilter;


# instance fields
.field private S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private T:Lcom/google/android/libraries/social/settings/PreferenceCategory;

.field private U:Lfjh;

.field private V:Lkhl;

.field private W:Lkha;

.field private X:Lfjh;

.field private Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private aa:Lkhl;

.field private ab:Lkhl;

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Landroid/os/Handler;

.field private ah:Lkhg;

.field private ai:Lfur;

.field private aj:Z

.field private ak:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private al:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lfkp;",
            ">;"
        }
    .end annotation
.end field

.field private final am:Lhov;

.field private final an:Landroid/content/BroadcastReceiver;

.field private ao:Lkgp;

.field private ap:Lkhr;

.field private aq:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 71
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "auto_upload_enabled"

    aput-object v1, v0, v3

    const-string v1, "auto_upload_account_id"

    aput-object v1, v0, v4

    const-string v1, "sync_on_wifi_only"

    aput-object v1, v0, v5

    const-string v1, "sync_on_roaming"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "sync_on_battery"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "video_upload_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "upload_full_resolution"

    aput-object v2, v0, v1

    sput-object v0, Lfjx;->N:[Ljava/lang/String;

    .line 81
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "quota_limit"

    aput-object v1, v0, v3

    const-string v1, "quota_used"

    aput-object v1, v0, v4

    const-string v1, "quota_unlimited"

    aput-object v1, v0, v5

    sput-object v0, Lfjx;->O:[Ljava/lang/String;

    .line 104
    const-string v0, "https://www.google.com/settings/storage/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfjx;->P:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 215
    invoke-direct {p0}, Llol;-><init>()V

    .line 133
    new-instance v0, Lhov;

    iget-object v1, p0, Lfjx;->av:Llqm;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Lfjx;->am:Lhov;

    .line 135
    new-instance v0, Lfjy;

    invoke-direct {v0, p0}, Lfjy;-><init>(Lfjx;)V

    iput-object v0, p0, Lfjx;->an:Landroid/content/BroadcastReceiver;

    .line 184
    new-instance v0, Lkgp;

    iget-object v1, p0, Lfjx;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkgp;-><init>(Lkgr;Llqr;)V

    iput-object v0, p0, Lfjx;->ao:Lkgp;

    .line 188
    new-instance v0, Lfka;

    iget-object v1, p0, Lfjx;->ao:Lkgp;

    iget-object v2, p0, Lfjx;->av:Llqm;

    invoke-direct {v0, p0, p0, v1, v2}, Lfka;-><init>(Lfjx;Lkgr;Lkgp;Llqr;)V

    iput-object v0, p0, Lfjx;->aq:Lbc;

    .line 215
    return-void
.end method

.method static synthetic A(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic B(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic C(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic V()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lfjx;->P:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic W()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lfjx;->N:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic X()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lfjx;->O:[Ljava/lang/String;

    return-object v0
.end method

.method private Y()V
    .locals 3

    .prologue
    .line 279
    iget-boolean v0, p0, Lfjx;->af:Z

    if-nez v0, :cond_0

    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfjx;->af:Z

    .line 281
    iget-object v0, p0, Lfjx;->at:Llnl;

    iget-object v1, p0, Lfjx;->an:Landroid/content/BroadcastReceiver;

    sget-object v2, Lfjx;->R:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Llnl;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 283
    :cond_0
    return-void
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 287
    iget-boolean v0, p0, Lfjx;->af:Z

    if-eqz v0, :cond_0

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfjx;->af:Z

    .line 289
    iget-object v0, p0, Lfjx;->at:Llnl;

    iget-object v1, p0, Lfjx;->an:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Llnl;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 291
    :cond_0
    return-void
.end method

.method static synthetic a(Lfjx;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0a06ec

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a06e5

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a06e6

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a06e7

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0a06e8

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0a06e9

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0a06ea

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0a06eb

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic a(Lfjx;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lfjx;->ak:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic a(Lfjx;Ljava/lang/Integer;Lhmv;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lfjx;->a(Ljava/lang/Integer;Lhmv;)V

    return-void
.end method

.method static synthetic a(Lfjx;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 67
    iput-boolean v2, p0, Lfjx;->ac:Z

    sget-object v0, Lhmv;->m:Lhmv;

    invoke-direct {p0, p1, v0}, Lfjx;->a(Ljava/util/List;Lhmv;)V

    iget-object v0, p0, Lfjx;->aa:Lkhl;

    const v1, 0x7f0a06e0

    invoke-virtual {v0, v1}, Lkhl;->g(I)V

    iget-object v0, p0, Lfjx;->aa:Lkhl;

    const v1, 0x7f0a06e2

    invoke-virtual {v0, v1}, Lkhl;->i(I)V

    new-instance v0, Lfke;

    invoke-direct {v0, p0, p1}, Lfke;-><init>(Lfjx;Ljava/util/ArrayList;)V

    invoke-direct {p0}, Lfjx;->Y()V

    new-array v1, v2, [Ljava/lang/Void;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic a(Lfjx;Ljava/util/List;Lhmv;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lfjx;->a(Ljava/util/List;Lhmv;)V

    return-void
.end method

.method private a(Ljava/lang/Integer;Lhmv;)V
    .locals 4

    .prologue
    .line 702
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 703
    iget-object v0, p0, Lfjx;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lfjx;->at:Llnl;

    .line 704
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, p2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 703
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 707
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;Lhmv;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lhmv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 696
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 697
    invoke-direct {p0, v0, p2}, Lfjx;->a(Ljava/lang/Integer;Lhmv;)V

    goto :goto_0

    .line 699
    :cond_0
    return-void
.end method

.method private a(Lkhp;)V
    .locals 5

    .prologue
    .line 547
    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v1, Lhpu;

    .line 548
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 549
    invoke-virtual {v0}, Lhpu;->a()Ljava/util/List;

    move-result-object v2

    .line 551
    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 553
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 554
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 555
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 556
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 559
    :cond_0
    new-instance v0, Lfiz;

    iget-object v1, p0, Lfjx;->at:Llnl;

    iget-object v3, p0, Lfjx;->al:Landroid/util/SparseArray;

    invoke-direct {v0, v1, v2, v3}, Lfiz;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/util/SparseArray;)V

    .line 561
    invoke-virtual {v0, p1}, Lfiz;->a(Lkhp;)V

    .line 562
    invoke-virtual {v0, p0}, Lfiz;->a(Lfja;)V

    .line 563
    iget-object v1, p0, Lfjx;->T:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 564
    return-void
.end method

.method private a(ZZ)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 735
    iget-object v0, p0, Lfjx;->W:Lkha;

    invoke-virtual {v0}, Lkha;->k()Ljava/lang/String;

    move-result-object v0

    const-string v3, "WIFI_ONLY"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    .line 736
    iget-object v4, p0, Lfjx;->W:Lkha;

    if-eqz p2, :cond_1

    sget-boolean v0, Lfjx;->Q:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lkha;->b(Z)V

    .line 737
    iget-object v4, p0, Lfjx;->X:Lfjh;

    if-eqz p2, :cond_2

    sget-boolean v0, Lfjx;->Q:Z

    if-nez v0, :cond_2

    if-nez v3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lfjh;->b(Z)V

    .line 738
    iget-object v0, p0, Lfjx;->U:Lfjh;

    invoke-virtual {v0, p2}, Lfjh;->b(Z)V

    .line 739
    iget-object v0, p0, Lfjx;->V:Lkhl;

    invoke-virtual {v0, p2}, Lkhl;->b(Z)V

    .line 740
    iget-object v0, p0, Lfjx;->ab:Lkhl;

    invoke-virtual {v0, v1}, Lkhl;->b(Z)V

    .line 741
    iget-object v0, p0, Lfjx;->Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz p2, :cond_3

    sget-boolean v3, Lfjx;->Q:Z

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->b(Z)V

    .line 742
    iget-object v0, p0, Lfjx;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->b(Z)V

    .line 743
    iget-object v0, p0, Lfjx;->aa:Lkhl;

    invoke-virtual {v0, p2}, Lkhl;->b(Z)V

    .line 745
    iget-object v0, p0, Lfjx;->ah:Lkhg;

    invoke-virtual {v0}, Lkhg;->a()Landroid/widget/Switch;

    move-result-object v0

    .line 747
    if-eqz v0, :cond_0

    .line 748
    invoke-virtual {v0, p2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 749
    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 752
    :cond_0
    iget-object v0, p0, Lfjx;->T:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    .line 753
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->a(I)Lkhl;

    move-result-object v0

    check-cast v0, Lfiz;

    .line 754
    invoke-virtual {v0, p2}, Lfiz;->b(Z)V

    .line 755
    return-void

    :cond_1
    move v0, v2

    .line 736
    goto :goto_0

    :cond_2
    move v0, v2

    .line 737
    goto :goto_1

    :cond_3
    move v1, v2

    .line 741
    goto :goto_2
.end method

.method static synthetic a(Lfjx;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lfjx;->ac:Z

    return v0
.end method

.method static synthetic a(Lfjx;Z)Z
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lfjx;->ac:Z

    return p1
.end method

.method static synthetic b(Lfjx;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lfjx;->Z()V

    return-void
.end method

.method static synthetic b(Lfjx;Z)Z
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lfjx;->ad:Z

    return p1
.end method

.method static synthetic c(Lfjx;)Lkhl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->aa:Lkhl;

    return-object v0
.end method

.method static synthetic d(Lfjx;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->ag:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic f(Lfjx;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 67
    iget-object v0, p0, Lfjx;->ak:Ljava/util/Map;

    const-string v1, "account_quotas"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    iput-object v0, p0, Lfjx;->al:Landroid/util/SparseArray;

    iget-object v0, p0, Lfjx;->T:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->e()V

    new-instance v0, Lfkg;

    invoke-direct {v0, p0}, Lfkg;-><init>(Lfjx;)V

    invoke-direct {p0, v0}, Lfjx;->a(Lkhp;)V

    iget-object v0, p0, Lfjx;->ak:Ljava/util/Map;

    const-string v1, "sync_on_roaming"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lfjx;->Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_1

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    iget-object v0, p0, Lfjx;->ak:Ljava/util/Map;

    const-string v1, "sync_on_battery"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lfjx;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v2, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    sget-boolean v0, Lfjx;->Q:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_3
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_5

    const-string v1, "WIFI_ONLY"

    const v4, 0x7f0a06ca

    :goto_4
    iget-object v5, p0, Lfjx;->W:Lkha;

    invoke-virtual {v5, v4}, Lkha;->i(I)V

    iget-object v4, p0, Lfjx;->W:Lkha;

    invoke-virtual {v4, v1}, Lkha;->b(Ljava/lang/String;)V

    if-eqz v0, :cond_6

    move v0, v2

    :goto_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_7

    const-string v0, "WIFI_ONLY"

    const v1, 0x7f0a06d0

    :goto_6
    iget-object v4, p0, Lfjx;->X:Lfjh;

    invoke-virtual {v4, v1}, Lfjh;->i(I)V

    iget-object v1, p0, Lfjx;->X:Lfjh;

    invoke-virtual {v1, v0}, Lfjh;->d_(Ljava/lang/String;)V

    iget-object v0, p0, Lfjx;->ak:Ljava/util/Map;

    const-string v1, "upload_full_resolution"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_8

    const-string v1, "FULL"

    const v0, 0x7f0a06d9

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    :goto_7
    iget-object v4, p0, Lfjx;->U:Lfjh;

    invoke-virtual {v4, v0}, Lfjh;->d(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfjx;->U:Lfjh;

    invoke-virtual {v0, v1}, Lfjh;->d_(Ljava/lang/String;)V

    invoke-static {}, Lhqd;->a()Z

    move-result v7

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    invoke-virtual {v0}, Lhpu;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v4, v2

    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lfjx;->at:Llnl;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v1, v9}, Lhqd;->e(Landroid/content/Context;I)Z

    move-result v9

    and-int v1, v4, v9

    if-nez v9, :cond_0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    move v4, v1

    goto :goto_8

    :cond_1
    move v0, v3

    goto/16 :goto_0

    :cond_2
    move v0, v3

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lfjx;->ak:Ljava/util/Map;

    const-string v1, "sync_on_wifi_only"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto/16 :goto_2

    :cond_4
    move v0, v3

    goto/16 :goto_3

    :cond_5
    const-string v1, "WIFI_OR_MOBILE"

    const v4, 0x7f0a06cb

    goto/16 :goto_4

    :cond_6
    iget-object v0, p0, Lfjx;->ak:Ljava/util/Map;

    const-string v1, "video_upload_wifi_only"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto/16 :goto_5

    :cond_7
    const-string v0, "WIFI_OR_MOBILE"

    const v1, 0x7f0a06d1

    goto/16 :goto_6

    :cond_8
    const-string v1, "STANDARD"

    const v0, 0x7f0a06da

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :cond_9
    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    invoke-virtual {v0}, Lhpu;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0, v3, v3}, Lfjx;->a(ZZ)V

    :cond_a
    :goto_9
    return-void

    :cond_b
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v2

    :goto_a
    invoke-direct {p0, v2, v0}, Lfjx;->a(ZZ)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lfjx;->ae:Z

    if-nez v0, :cond_a

    if-eqz v7, :cond_c

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    :cond_c
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    move-object v1, v5

    :goto_b
    new-instance v5, Lfkf;

    iget-object v0, p0, Lfjx;->at:Llnl;

    invoke-direct {v5, v0, v1}, Lfkf;-><init>(Landroid/content/Context;Ljava/util/List;)V

    const v0, 0x7f0a0440

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v8, Lhei;

    invoke-static {v0, v8}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v7, :cond_f

    if-nez v4, :cond_f

    const v1, 0x7f0a06c1

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v6, v4, v3

    aput-object v0, v4, v2

    invoke-virtual {p0, v1, v4}, Lfjx;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_c
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lfjx;->at:Llnl;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a06c0

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a06c4

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a07f8

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iput-boolean v2, p0, Lfjx;->ae:Z

    goto/16 :goto_9

    :cond_d
    move v0, v3

    goto/16 :goto_a

    :cond_e
    move-object v1, v6

    goto :goto_b

    :cond_f
    if-nez v7, :cond_10

    const v0, 0x7f0a06c2

    invoke-virtual {p0, v0}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    :cond_10
    const v1, 0x7f0a06c3

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v6, v4, v3

    aput-object v0, v4, v2

    invoke-virtual {p0, v1, v4}, Lfjx;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_c
.end method

.method static synthetic g(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic i(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic j(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic k(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic l(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic m(Lfjx;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lfjx;->Y()V

    return-void
.end method

.method static synthetic n(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic o(Lfjx;)Lcom/google/android/libraries/social/settings/PreferenceCategory;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->T:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    return-object v0
.end method

.method static synthetic p(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic q(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic r(Lfjx;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lfjx;->aj:Z

    return v0
.end method

.method static synthetic s(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic t(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic u(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic v(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic w(Lfjx;)Lhov;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->am:Lhov;

    return-object v0
.end method

.method static synthetic x(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic y(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method

.method static synthetic z(Lfjx;)Llnl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfjx;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 273
    invoke-super {p0}, Llol;->A()V

    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfjx;->aj:Z

    .line 275
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 534
    sget-object v0, Lhmw;->o:Lhmw;

    return-object v0
.end method

.method public U()V
    .locals 2

    .prologue
    .line 543
    invoke-virtual {p0}, Lfjx;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbb;->b(I)Ldo;

    move-result-object v0

    invoke-virtual {v0}, Ldo;->t()V

    .line 544
    return-void
.end method

.method public a()V
    .locals 4

    .prologue
    .line 330
    invoke-virtual {p0}, Lfjx;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lfjx;->aq:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 331
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 220
    new-instance v0, Lkhg;

    check-cast p1, Los;

    invoke-direct {v0, p1}, Lkhg;-><init>(Los;)V

    iput-object v0, p0, Lfjx;->ah:Lkhg;

    .line 221
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 227
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lfjx;->at:Llnl;

    invoke-virtual {v1}, Llnl;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lfjx;->ag:Landroid/os/Handler;

    .line 229
    if-eqz p1, :cond_0

    .line 230
    const-string v0, "dialog_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfjx;->ae:Z

    .line 231
    const-string v0, "loaded_quota_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfjx;->ad:Z

    .line 234
    :cond_0
    sget-object v0, Lfjx;->R:Landroid/content/IntentFilter;

    if-nez v0, :cond_1

    .line 235
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 236
    sput-object v0, Lfjx;->R:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.libraries.social.autobackup.upload_all_progress"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v1, Ljgn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    .line 239
    invoke-interface {v0}, Ljgn;->e()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lfjx;->Q:Z

    .line 241
    :cond_1
    return-void

    .line 239
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 524
    if-nez p1, :cond_0

    .line 525
    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 526
    invoke-virtual {v0}, Lhpu;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 527
    invoke-virtual {p0}, Lfjx;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbb;->b(I)Ldo;

    move-result-object v0

    invoke-virtual {v0}, Ldo;->t()V

    .line 530
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 258
    invoke-super {p0}, Llol;->aO_()V

    .line 259
    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 260
    invoke-virtual {v0}, Lhpu;->b()Ljlx;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Ljlx;->a(Ljma;Z)V

    .line 261
    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 67
    invoke-virtual {p0}, Lfjx;->U()V

    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 539
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 245
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 246
    iget-object v0, p0, Lfjx;->au:Llnh;

    const-class v1, Lfur;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfur;

    iput-object v0, p0, Lfjx;->ai:Lfur;

    .line 247
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 335
    invoke-virtual {p0}, Lfjx;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lfjx;->aq:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 336
    return-void
.end method

.method public e()V
    .locals 7

    .prologue
    const v4, 0x7f0a06d2

    const v6, 0x7f0a06cf

    const/4 v5, 0x0

    .line 340
    new-instance v0, Lkhr;

    iget-object v1, p0, Lfjx;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfjx;->ap:Lkhr;

    .line 341
    new-instance v0, Lfkg;

    invoke-direct {v0, p0}, Lfkg;-><init>(Lfjx;)V

    .line 343
    iget-object v1, p0, Lfjx;->ap:Lkhr;

    const v2, 0x7f0a06b1

    .line 344
    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a06b2

    .line 345
    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 343
    invoke-virtual {v1, v2, v3}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lfjx;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 346
    iget-object v1, p0, Lfjx;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 347
    iget-object v1, p0, Lfjx;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const v2, 0x7f0a05b9

    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 348
    iget-object v1, p0, Lfjx;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v1, v5}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Z)V

    .line 349
    iget-object v1, p0, Lfjx;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 351
    iget-object v1, p0, Lfjx;->ai:Lfur;

    invoke-virtual {v1}, Lfur;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 352
    iget-object v1, p0, Lfjx;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const v2, 0x7f0a06b3

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->i(I)V

    .line 355
    :cond_0
    iget-object v1, p0, Lfjx;->ah:Lkhg;

    iget-object v2, p0, Lfjx;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lkhg;->a(Lcom/google/android/libraries/social/settings/CheckBoxPreference;Z)V

    .line 357
    iget-object v1, p0, Lfjx;->ap:Lkhr;

    const v2, 0x7f0a0889

    .line 358
    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 357
    invoke-virtual {v1, v2}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v1

    iput-object v1, p0, Lfjx;->T:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    .line 360
    invoke-direct {p0, v0}, Lfjx;->a(Lkhp;)V

    .line 362
    iget-object v1, p0, Lfjx;->ap:Lkhr;

    const v2, 0x7f0a0887

    .line 363
    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 362
    invoke-virtual {v1, v2}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v1

    .line 365
    new-instance v2, Lfjh;

    iget-object v3, p0, Lfjx;->at:Llnl;

    invoke-direct {v2, v3}, Lfjh;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lfjx;->U:Lfjh;

    .line 366
    iget-object v2, p0, Lfjx;->U:Lfjh;

    invoke-virtual {p0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->e(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v2, p0, Lfjx;->U:Lfjh;

    invoke-virtual {p0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->a(Ljava/lang/CharSequence;)V

    .line 368
    iget-object v2, p0, Lfjx;->U:Lfjh;

    const v3, 0x7f0a06d3

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->d(Ljava/lang/CharSequence;)V

    .line 369
    iget-object v2, p0, Lfjx;->U:Lfjh;

    const v3, 0x7f0a05c7

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->a(Ljava/lang/Object;)V

    .line 370
    iget-object v2, p0, Lfjx;->U:Lfjh;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0012

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->a([Ljava/lang/CharSequence;)V

    .line 372
    iget-object v2, p0, Lfjx;->U:Lfjh;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0011

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->c([Ljava/lang/CharSequence;)V

    .line 374
    iget-object v2, p0, Lfjx;->U:Lfjh;

    const v3, 0x7f0a05c0

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->d(Ljava/lang/String;)V

    .line 375
    iget-object v2, p0, Lfjx;->U:Lfjh;

    invoke-virtual {v2, v5}, Lfjh;->d(Z)V

    .line 376
    iget-object v2, p0, Lfjx;->U:Lfjh;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0013

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->b([Ljava/lang/CharSequence;)V

    .line 378
    iget-object v2, p0, Lfjx;->U:Lfjh;

    invoke-virtual {v2, v0}, Lfjh;->a(Lkhp;)V

    .line 380
    iget-object v2, p0, Lfjx;->U:Lfjh;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 382
    iget-object v2, p0, Lfjx;->ap:Lkhr;

    const v3, 0x7f0a06db

    .line 383
    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a06dc

    .line 384
    invoke-virtual {p0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 382
    invoke-virtual {v2, v3, v4}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v2

    iput-object v2, p0, Lfjx;->V:Lkhl;

    .line 385
    iget-object v2, p0, Lfjx;->V:Lkhl;

    const v3, 0x7f0a05c2

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkhl;->d(Ljava/lang/String;)V

    .line 386
    iget-object v2, p0, Lfjx;->V:Lkhl;

    invoke-virtual {v2, v5}, Lkhl;->d(Z)V

    .line 387
    iget-object v2, p0, Lfjx;->V:Lkhl;

    new-instance v3, Lfkb;

    invoke-direct {v3, p0}, Lfkb;-><init>(Lfjx;)V

    invoke-virtual {v2, v3}, Lkhl;->a(Lkhq;)V

    .line 400
    iget-object v2, p0, Lfjx;->V:Lkhl;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 402
    iget-object v1, p0, Lfjx;->ap:Lkhr;

    const v2, 0x7f0a0888

    .line 403
    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 402
    invoke-virtual {v1, v2}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v1

    .line 405
    iget-object v2, p0, Lfjx;->ap:Lkhr;

    const v3, 0x7f0a06c9

    .line 406
    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a06ca

    .line 407
    invoke-virtual {p0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 405
    invoke-virtual {v2, v3, v4}, Lkhr;->g(Ljava/lang/String;Ljava/lang/String;)Lkha;

    move-result-object v2

    iput-object v2, p0, Lfjx;->W:Lkha;

    .line 408
    iget-object v2, p0, Lfjx;->W:Lkha;

    const v3, 0x7f0a05c5

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkha;->a(Ljava/lang/Object;)V

    .line 409
    iget-object v2, p0, Lfjx;->W:Lkha;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f000d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkha;->a([Ljava/lang/CharSequence;)V

    .line 411
    iget-object v2, p0, Lfjx;->W:Lkha;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f000c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkha;->b([Ljava/lang/CharSequence;)V

    .line 413
    iget-object v2, p0, Lfjx;->W:Lkha;

    const v3, 0x7f0a05bb

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkha;->d(Ljava/lang/String;)V

    .line 414
    iget-object v2, p0, Lfjx;->W:Lkha;

    invoke-virtual {v2, v5}, Lkha;->d(Z)V

    .line 415
    iget-object v2, p0, Lfjx;->W:Lkha;

    invoke-virtual {v2, v0}, Lkha;->a(Lkhp;)V

    .line 417
    iget-object v2, p0, Lfjx;->W:Lkha;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 419
    new-instance v2, Lfjh;

    iget-object v3, p0, Lfjx;->at:Llnl;

    invoke-direct {v2, v3}, Lfjh;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lfjx;->X:Lfjh;

    .line 420
    iget-object v2, p0, Lfjx;->X:Lfjh;

    invoke-virtual {p0, v6}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->e(Ljava/lang/CharSequence;)V

    .line 421
    iget-object v2, p0, Lfjx;->X:Lfjh;

    invoke-virtual {p0, v6}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->a(Ljava/lang/CharSequence;)V

    .line 422
    iget-object v2, p0, Lfjx;->X:Lfjh;

    const v3, 0x7f0a06d0

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->d(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v2, p0, Lfjx;->X:Lfjh;

    const v3, 0x7f0a05c6

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->a(Ljava/lang/Object;)V

    .line 424
    iget-object v2, p0, Lfjx;->X:Lfjh;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->a([Ljava/lang/CharSequence;)V

    .line 426
    iget-object v2, p0, Lfjx;->X:Lfjh;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->c([Ljava/lang/CharSequence;)V

    .line 428
    iget-object v2, p0, Lfjx;->X:Lfjh;

    const v3, 0x7f0a05bf

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->d(Ljava/lang/String;)V

    .line 429
    iget-object v2, p0, Lfjx;->X:Lfjh;

    invoke-virtual {v2, v5}, Lfjh;->d(Z)V

    .line 430
    iget-object v2, p0, Lfjx;->X:Lfjh;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0010

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfjh;->b([Ljava/lang/CharSequence;)V

    .line 432
    iget-object v2, p0, Lfjx;->X:Lfjh;

    invoke-virtual {v2, v0}, Lfjh;->a(Lkhp;)V

    .line 434
    iget-object v2, p0, Lfjx;->X:Lfjh;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 436
    iget-object v2, p0, Lfjx;->ap:Lkhr;

    const v3, 0x7f0a06c5

    .line 437
    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a06c6

    .line 438
    invoke-virtual {p0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 436
    invoke-virtual {v2, v3, v4}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v2

    iput-object v2, p0, Lfjx;->Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 439
    iget-object v2, p0, Lfjx;->Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0015

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 441
    iget-object v2, p0, Lfjx;->Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const v3, 0x7f0a05bd

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 442
    iget-object v2, p0, Lfjx;->Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Z)V

    .line 443
    iget-object v2, p0, Lfjx;->Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 445
    iget-object v2, p0, Lfjx;->Y:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 447
    iget-object v2, p0, Lfjx;->ap:Lkhr;

    const v3, 0x7f0a06c7

    .line 448
    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a06c8

    .line 449
    invoke-virtual {p0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 447
    invoke-virtual {v2, v3, v4}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v2

    iput-object v2, p0, Lfjx;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 450
    iget-object v2, p0, Lfjx;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p0}, Lfjx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 452
    iget-object v2, p0, Lfjx;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const v3, 0x7f0a05be

    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 453
    iget-object v2, p0, Lfjx;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Z)V

    .line 454
    iget-object v2, p0, Lfjx;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 456
    iget-object v0, p0, Lfjx;->Z:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 458
    iget-object v0, p0, Lfjx;->ap:Lkhr;

    const v2, 0x7f0a06df

    .line 459
    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a06e1

    .line 460
    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 458
    invoke-virtual {v0, v2, v3}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    iput-object v0, p0, Lfjx;->aa:Lkhl;

    .line 461
    iget-object v0, p0, Lfjx;->aa:Lkhl;

    const v2, 0x7f0a05bc

    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkhl;->d(Ljava/lang/String;)V

    .line 462
    iget-object v0, p0, Lfjx;->aa:Lkhl;

    invoke-virtual {v0, v5}, Lkhl;->d(Z)V

    .line 463
    iget-object v0, p0, Lfjx;->aa:Lkhl;

    new-instance v2, Lfkc;

    invoke-direct {v2, p0}, Lfkc;-><init>(Lfjx;)V

    invoke-virtual {v0, v2}, Lkhl;->a(Lkhq;)V

    .line 495
    iget-object v0, p0, Lfjx;->aa:Lkhl;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 497
    iget-object v0, p0, Lfjx;->ap:Lkhr;

    const v1, 0x7f0a0563

    .line 498
    invoke-virtual {p0, v1}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 497
    invoke-virtual {v0, v1}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 500
    iget-object v1, p0, Lfjx;->ap:Lkhr;

    const v2, 0x7f0a06dd

    .line 501
    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a06de

    .line 502
    invoke-virtual {p0, v3}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 500
    invoke-virtual {v1, v2, v3}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v1

    iput-object v1, p0, Lfjx;->ab:Lkhl;

    .line 503
    iget-object v1, p0, Lfjx;->ab:Lkhl;

    const v2, 0x7f0a05c3

    invoke-virtual {p0, v2}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkhl;->d(Ljava/lang/String;)V

    .line 504
    iget-object v1, p0, Lfjx;->ab:Lkhl;

    invoke-virtual {v1, v5}, Lkhl;->d(Z)V

    .line 506
    iget-object v1, p0, Lfjx;->ab:Lkhl;

    new-instance v2, Lfkd;

    invoke-direct {v2, p0}, Lfkd;-><init>(Lfjx;)V

    invoke-virtual {v1, v2}, Lkhl;->a(Lkhq;)V

    .line 519
    iget-object v1, p0, Lfjx;->ab:Lkhl;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 520
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 251
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 252
    const-string v0, "dialog_shown"

    iget-boolean v1, p0, Lfjx;->ae:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 253
    const-string v0, "loaded_quota_settings"

    iget-boolean v1, p0, Lfjx;->ad:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 254
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 265
    invoke-super {p0}, Llol;->z()V

    .line 266
    iget-object v0, p0, Lfjx;->at:Llnl;

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 267
    invoke-virtual {v0}, Lhpu;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0, p0}, Ljlx;->a(Ljma;)V

    .line 268
    invoke-direct {p0}, Lfjx;->Z()V

    .line 269
    return-void
.end method
