.class public final Lmch;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfj;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25432
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25435
    iput-object v0, p0, Lmch;->apiHeader:Llyr;

    .line 25438
    iput-object v0, p0, Lmch;->a:Lnfj;

    .line 25432
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 25455
    const/4 v0, 0x0

    .line 25456
    iget-object v1, p0, Lmch;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 25457
    const/4 v0, 0x1

    iget-object v1, p0, Lmch;->apiHeader:Llyr;

    .line 25458
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25460
    :cond_0
    iget-object v1, p0, Lmch;->a:Lnfj;

    if-eqz v1, :cond_1

    .line 25461
    const/4 v1, 0x2

    iget-object v2, p0, Lmch;->a:Lnfj;

    .line 25462
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25464
    :cond_1
    iget-object v1, p0, Lmch;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25465
    iput v0, p0, Lmch;->ai:I

    .line 25466
    return v0
.end method

.method public a(Loxn;)Lmch;
    .locals 2

    .prologue
    .line 25474
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 25475
    sparse-switch v0, :sswitch_data_0

    .line 25479
    iget-object v1, p0, Lmch;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 25480
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmch;->ah:Ljava/util/List;

    .line 25483
    :cond_1
    iget-object v1, p0, Lmch;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25485
    :sswitch_0
    return-object p0

    .line 25490
    :sswitch_1
    iget-object v0, p0, Lmch;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 25491
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmch;->apiHeader:Llyr;

    .line 25493
    :cond_2
    iget-object v0, p0, Lmch;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25497
    :sswitch_2
    iget-object v0, p0, Lmch;->a:Lnfj;

    if-nez v0, :cond_3

    .line 25498
    new-instance v0, Lnfj;

    invoke-direct {v0}, Lnfj;-><init>()V

    iput-object v0, p0, Lmch;->a:Lnfj;

    .line 25500
    :cond_3
    iget-object v0, p0, Lmch;->a:Lnfj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25475
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25443
    iget-object v0, p0, Lmch;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 25444
    const/4 v0, 0x1

    iget-object v1, p0, Lmch;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25446
    :cond_0
    iget-object v0, p0, Lmch;->a:Lnfj;

    if-eqz v0, :cond_1

    .line 25447
    const/4 v0, 0x2

    iget-object v1, p0, Lmch;->a:Lnfj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25449
    :cond_1
    iget-object v0, p0, Lmch;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 25451
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 25428
    invoke-virtual {p0, p1}, Lmch;->a(Loxn;)Lmch;

    move-result-object v0

    return-object v0
.end method
