.class public final Lmfb;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnwf;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9070
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9073
    iput-object v0, p0, Lmfb;->apiHeader:Llyr;

    .line 9076
    iput-object v0, p0, Lmfb;->a:Lnwf;

    .line 9070
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9093
    const/4 v0, 0x0

    .line 9094
    iget-object v1, p0, Lmfb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 9095
    const/4 v0, 0x1

    iget-object v1, p0, Lmfb;->apiHeader:Llyr;

    .line 9096
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9098
    :cond_0
    iget-object v1, p0, Lmfb;->a:Lnwf;

    if-eqz v1, :cond_1

    .line 9099
    const/4 v1, 0x2

    iget-object v2, p0, Lmfb;->a:Lnwf;

    .line 9100
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9102
    :cond_1
    iget-object v1, p0, Lmfb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9103
    iput v0, p0, Lmfb;->ai:I

    .line 9104
    return v0
.end method

.method public a(Loxn;)Lmfb;
    .locals 2

    .prologue
    .line 9112
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9113
    sparse-switch v0, :sswitch_data_0

    .line 9117
    iget-object v1, p0, Lmfb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9118
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfb;->ah:Ljava/util/List;

    .line 9121
    :cond_1
    iget-object v1, p0, Lmfb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9123
    :sswitch_0
    return-object p0

    .line 9128
    :sswitch_1
    iget-object v0, p0, Lmfb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 9129
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfb;->apiHeader:Llyr;

    .line 9131
    :cond_2
    iget-object v0, p0, Lmfb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9135
    :sswitch_2
    iget-object v0, p0, Lmfb;->a:Lnwf;

    if-nez v0, :cond_3

    .line 9136
    new-instance v0, Lnwf;

    invoke-direct {v0}, Lnwf;-><init>()V

    iput-object v0, p0, Lmfb;->a:Lnwf;

    .line 9138
    :cond_3
    iget-object v0, p0, Lmfb;->a:Lnwf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9113
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9081
    iget-object v0, p0, Lmfb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 9082
    const/4 v0, 0x1

    iget-object v1, p0, Lmfb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9084
    :cond_0
    iget-object v0, p0, Lmfb;->a:Lnwf;

    if-eqz v0, :cond_1

    .line 9085
    const/4 v0, 0x2

    iget-object v1, p0, Lmfb;->a:Lnwf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9087
    :cond_1
    iget-object v0, p0, Lmfb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9089
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9066
    invoke-virtual {p0, p1}, Lmfb;->a(Loxn;)Lmfb;

    move-result-object v0

    return-object v0
.end method
