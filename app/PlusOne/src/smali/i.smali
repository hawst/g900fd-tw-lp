.class public Li;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    return-void
.end method

.method public static a(Landroid/view/View;IIII)Li;
    .locals 3

    .prologue
    .line 76
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 77
    new-instance v0, Lj;

    new-instance v1, Lk;

    invoke-static {p0, p1, p2, p3, p4}, Landroid/app/ActivityOptions;->makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;

    move-result-object v2

    invoke-direct {v1, v2}, Lk;-><init>(Landroid/app/ActivityOptions;)V

    invoke-direct {v0, v1}, Lj;-><init>(Lk;)V

    .line 81
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Li;

    invoke-direct {v0}, Li;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return-object v0
.end method
