.class public final Lmmx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6890
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6921
    const/4 v0, 0x0

    .line 6922
    iget-object v1, p0, Lmmx;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 6923
    const/4 v0, 0x1

    iget-object v1, p0, Lmmx;->a:Ljava/lang/Float;

    .line 6924
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 6926
    :cond_0
    iget-object v1, p0, Lmmx;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 6927
    const/4 v1, 0x2

    iget-object v2, p0, Lmmx;->b:Ljava/lang/Float;

    .line 6928
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6930
    :cond_1
    iget-object v1, p0, Lmmx;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 6931
    const/4 v1, 0x3

    iget-object v2, p0, Lmmx;->c:Ljava/lang/Float;

    .line 6932
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6934
    :cond_2
    iget-object v1, p0, Lmmx;->d:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 6935
    const/4 v1, 0x4

    iget-object v2, p0, Lmmx;->d:Ljava/lang/Float;

    .line 6936
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6938
    :cond_3
    iget-object v1, p0, Lmmx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6939
    iput v0, p0, Lmmx;->ai:I

    .line 6940
    return v0
.end method

.method public a(Loxn;)Lmmx;
    .locals 2

    .prologue
    .line 6948
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6949
    sparse-switch v0, :sswitch_data_0

    .line 6953
    iget-object v1, p0, Lmmx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6954
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmx;->ah:Ljava/util/List;

    .line 6957
    :cond_1
    iget-object v1, p0, Lmmx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6959
    :sswitch_0
    return-object p0

    .line 6964
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmx;->a:Ljava/lang/Float;

    goto :goto_0

    .line 6968
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmx;->b:Ljava/lang/Float;

    goto :goto_0

    .line 6972
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmx;->c:Ljava/lang/Float;

    goto :goto_0

    .line 6976
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmx;->d:Ljava/lang/Float;

    goto :goto_0

    .line 6949
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6903
    iget-object v0, p0, Lmmx;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 6904
    const/4 v0, 0x1

    iget-object v1, p0, Lmmx;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6906
    :cond_0
    iget-object v0, p0, Lmmx;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 6907
    const/4 v0, 0x2

    iget-object v1, p0, Lmmx;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6909
    :cond_1
    iget-object v0, p0, Lmmx;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 6910
    const/4 v0, 0x3

    iget-object v1, p0, Lmmx;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6912
    :cond_2
    iget-object v0, p0, Lmmx;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 6913
    const/4 v0, 0x4

    iget-object v1, p0, Lmmx;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6915
    :cond_3
    iget-object v0, p0, Lmmx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6917
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6886
    invoke-virtual {p0, p1}, Lmmx;->a(Loxn;)Lmmx;

    move-result-object v0

    return-object v0
.end method
