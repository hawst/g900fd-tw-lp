.class public final Logj;
.super Loxq;
.source "PG"


# instance fields
.field public a:[I

.field public b:[I

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6691
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6694
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Logj;->a:[I

    .line 6697
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Logj;->b:[I

    .line 6691
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 6728
    .line 6729
    iget-object v0, p0, Logj;->a:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Logj;->a:[I

    array-length v0, v0

    if-lez v0, :cond_5

    .line 6731
    iget-object v3, p0, Logj;->a:[I

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    .line 6733
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6731
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6736
    :cond_0
    iget-object v0, p0, Logj;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 6738
    :goto_1
    iget-object v2, p0, Logj;->b:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Logj;->b:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 6740
    iget-object v3, p0, Logj;->b:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    .line 6742
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6740
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6744
    :cond_1
    add-int/2addr v0, v2

    .line 6745
    iget-object v1, p0, Logj;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6747
    :cond_2
    iget-object v1, p0, Logj;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 6748
    const/4 v1, 0x3

    iget-object v2, p0, Logj;->c:Ljava/lang/Boolean;

    .line 6749
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6751
    :cond_3
    iget-object v1, p0, Logj;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 6752
    const/4 v1, 0x4

    iget-object v2, p0, Logj;->d:Ljava/lang/Boolean;

    .line 6753
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6755
    :cond_4
    iget-object v1, p0, Logj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6756
    iput v0, p0, Logj;->ai:I

    .line 6757
    return v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Logj;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6765
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6766
    sparse-switch v0, :sswitch_data_0

    .line 6770
    iget-object v1, p0, Logj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6771
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Logj;->ah:Ljava/util/List;

    .line 6774
    :cond_1
    iget-object v1, p0, Logj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6776
    :sswitch_0
    return-object p0

    .line 6781
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 6782
    iget-object v0, p0, Logj;->a:[I

    array-length v0, v0

    .line 6783
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 6784
    iget-object v2, p0, Logj;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6785
    iput-object v1, p0, Logj;->a:[I

    .line 6786
    :goto_1
    iget-object v1, p0, Logj;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 6787
    iget-object v1, p0, Logj;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 6788
    invoke-virtual {p1}, Loxn;->a()I

    .line 6786
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6791
    :cond_2
    iget-object v1, p0, Logj;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 6795
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 6796
    iget-object v0, p0, Logj;->b:[I

    array-length v0, v0

    .line 6797
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 6798
    iget-object v2, p0, Logj;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6799
    iput-object v1, p0, Logj;->b:[I

    .line 6800
    :goto_2
    iget-object v1, p0, Logj;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 6801
    iget-object v1, p0, Logj;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 6802
    invoke-virtual {p1}, Loxn;->a()I

    .line 6800
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6805
    :cond_3
    iget-object v1, p0, Logj;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 6809
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Logj;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 6813
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Logj;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 6766
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6706
    iget-object v1, p0, Logj;->a:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Logj;->a:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 6707
    iget-object v2, p0, Logj;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 6708
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 6707
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6711
    :cond_0
    iget-object v1, p0, Logj;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Logj;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 6712
    iget-object v1, p0, Logj;->b:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 6713
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 6712
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6716
    :cond_1
    iget-object v0, p0, Logj;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 6717
    const/4 v0, 0x3

    iget-object v1, p0, Logj;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6719
    :cond_2
    iget-object v0, p0, Logj;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 6720
    const/4 v0, 0x4

    iget-object v1, p0, Logj;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6722
    :cond_3
    iget-object v0, p0, Logj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6724
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6687
    invoke-virtual {p0, p1}, Logj;->a(Loxn;)Logj;

    move-result-object v0

    return-object v0
.end method
