.class public final Ledo;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Lhjj;
.implements Lhmq;
.implements Lhob;
.implements Lkch;
.implements Lljv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhjj;",
        "Lhmq;",
        "Lhob;",
        "Lkch;",
        "Lljv;"
    }
.end annotation


# instance fields
.field private final N:Lhje;

.field private O:Lhee;

.field private P:Llhd;

.field private Q:Lhoc;

.field private R:Lkci;

.field private S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private T:Lexo;

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Levx;

.field private final Y:Licq;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 78
    invoke-direct {p0}, Llol;-><init>()V

    .line 93
    new-instance v0, Lhje;

    iget-object v1, p0, Ledo;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Ledo;->N:Lhje;

    .line 96
    new-instance v0, Llhd;

    invoke-direct {v0}, Llhd;-><init>()V

    iput-object v0, p0, Ledo;->P:Llhd;

    .line 97
    new-instance v0, Lhoc;

    iget-object v1, p0, Ledo;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iget-object v1, p0, Ledo;->au:Llnh;

    .line 98
    invoke-virtual {v0, v1}, Lhoc;->a(Llnh;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Ledo;->Q:Lhoc;

    .line 99
    new-instance v0, Lkci;

    iget-object v1, p0, Ledo;->av:Llqm;

    const v2, 0x7f10005b

    invoke-direct {v0, p0, v1, v2, p0}, Lkci;-><init>(Lu;Llqr;ILkch;)V

    iput-object v0, p0, Ledo;->R:Lkci;

    .line 334
    new-instance v0, Licq;

    iget-object v1, p0, Ledo;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a06f7

    .line 335
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    const v1, 0x7f0a057b

    .line 336
    invoke-virtual {v0, v1}, Licq;->a(I)Licq;

    move-result-object v0

    iput-object v0, p0, Ledo;->Y:Licq;

    .line 334
    return-void
.end method

.method private U()V
    .locals 1

    .prologue
    .line 471
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Ledo;->c(I)V

    .line 472
    invoke-direct {p0}, Ledo;->V()V

    .line 473
    return-void
.end method

.method private V()V
    .locals 4

    .prologue
    .line 510
    invoke-virtual {p0}, Ledo;->n()Lz;

    move-result-object v1

    .line 511
    iget-object v0, p0, Ledo;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 512
    if-eqz v1, :cond_1

    const-string v2, "fetch_newer"

    invoke-virtual {v0, v2}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 513
    iget-boolean v2, p0, Ledo;->W:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Ledo;->W()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 514
    iget-object v2, p0, Ledo;->Y:Licq;

    sget-object v3, Lict;->a:Lict;

    invoke-virtual {v2, v3}, Licq;->a(Lict;)V

    .line 517
    :cond_0
    new-instance v2, Ldos;

    iget-object v3, p0, Ledo;->O:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v2, v1, v3}, Ldos;-><init>(Landroid/content/Context;I)V

    .line 518
    const-string v1, "fetch_newer"

    invoke-virtual {v2, v1}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 519
    invoke-virtual {v0, v2}, Lhoc;->b(Lhny;)V

    .line 520
    invoke-direct {p0}, Ledo;->e()V

    .line 522
    :cond_1
    return-void
.end method

.method private W()Z
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Ledo;->T:Lexo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledo;->T:Lexo;

    invoke-virtual {v0}, Lexo;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ledo;)Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ledo;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 466
    invoke-virtual {p0}, Ledo;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10030a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 467
    invoke-virtual {p0}, Ledo;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10030b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 468
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Ledo;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Ledo;->R:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 109
    :cond_0
    iget-object v0, p0, Ledo;->N:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 110
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 573
    sget-object v0, Lhmw;->A:Lhmw;

    return-object v0
.end method

.method public K_()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ledo;->R:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 115
    invoke-direct {p0}, Ledo;->U()V

    .line 116
    return-void
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ledo;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l()Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    .line 407
    const v0, 0x7f0400d8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 408
    new-instance v0, Levx;

    const v1, 0x7f10030c

    .line 409
    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Levx;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Ledo;->X:Levx;

    .line 410
    iget-object v0, p0, Ledo;->X:Levx;

    invoke-virtual {v0, p0}, Levx;->a(Landroid/view/View$OnClickListener;)V

    .line 411
    const v0, 0x7f100304

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Ledo;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 413
    iget-object v0, p0, Ledo;->P:Llhd;

    iget-object v1, p0, Ledo;->X:Levx;

    invoke-virtual {v0, v1}, Llhd;->a(Llhc;)V

    .line 414
    iget-object v0, p0, Ledo;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v1, Llkd;

    iget-object v2, p0, Ledo;->P:Llhd;

    invoke-direct {v1, v2}, Llkd;-><init>(Llhc;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkc;)V

    .line 415
    new-instance v0, Lexo;

    iget-object v1, p0, Ledo;->at:Llnl;

    iget-object v2, p0, Ledo;->O:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    const/4 v2, 0x0

    iget-object v5, p0, Ledo;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 416
    invoke-virtual {p0}, Ledo;->n()Lz;

    move-result-object v3

    instance-of v6, v3, Lcom/google/android/apps/plus/phone/HomeActivity;

    move-object v3, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lexo;-><init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/View$OnClickListener;Lljv;Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Z)V

    iput-object v0, p0, Ledo;->T:Lexo;

    .line 417
    iget-object v0, p0, Ledo;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v1, p0, Ledo;->T:Lexo;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 419
    const v0, 0x7f10030a

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 420
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 421
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    invoke-virtual {p0}, Ledo;->n()Lz;

    move-result-object v0

    .line 424
    instance-of v1, v0, Ljgf;

    if-eqz v1, :cond_0

    .line 425
    check-cast v0, Ljgf;

    .line 426
    const-string v1, "events"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljgf;->a(Ljava/lang/String;J)Z

    .line 429
    :cond_0
    iget-object v0, p0, Ledo;->Y:Licq;

    new-instance v1, Ledq;

    invoke-direct {v1, p0}, Ledq;-><init>(Ledo;)V

    invoke-virtual {v0, v1}, Licq;->a(Lico;)Licq;

    .line 439
    return-object v7
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 462
    new-instance v0, Ledr;

    iget-object v1, p0, Ledo;->at:Llnl;

    iget-object v2, p0, Ledo;->O:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ledr;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 477
    iget-object v0, p0, Ledo;->T:Lexo;

    invoke-virtual {v0, p1}, Lexo;->a(Landroid/database/Cursor;)V

    .line 478
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ledo;->W:Z

    .line 481
    iget-boolean v0, p0, Ledo;->W:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Ledo;->U:Z

    if-nez v0, :cond_3

    move v0, v1

    .line 482
    :goto_1
    if-eqz v0, :cond_4

    .line 484
    :goto_2
    invoke-direct {p0, v2}, Ledo;->c(I)V

    .line 486
    iget-boolean v2, p0, Ledo;->W:Z

    if-eqz v2, :cond_5

    .line 487
    iget-object v0, p0, Ledo;->Y:Licq;

    sget-object v2, Lict;->b:Lict;

    invoke-virtual {v0, v2}, Licq;->a(Lict;)V

    .line 501
    :cond_0
    :goto_3
    iget-boolean v0, p0, Ledo;->U:Z

    if-nez v0, :cond_1

    .line 502
    iput-boolean v1, p0, Ledo;->V:Z

    .line 506
    :cond_1
    iget-object v0, p0, Ledo;->N:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 507
    return-void

    :cond_2
    move v0, v2

    .line 478
    goto :goto_0

    :cond_3
    move v0, v2

    .line 481
    goto :goto_1

    .line 482
    :cond_4
    const/16 v2, 0x8

    goto :goto_2

    .line 489
    :cond_5
    iget-boolean v2, p0, Ledo;->U:Z

    if-eqz v2, :cond_6

    .line 490
    invoke-direct {p0}, Ledo;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Ledo;->Y:Licq;

    sget-object v2, Lict;->a:Lict;

    invoke-virtual {v0, v2}, Licq;->a(Lict;)V

    goto :goto_3

    .line 493
    :cond_6
    if-eqz v0, :cond_7

    .line 495
    iget-object v0, p0, Ledo;->Y:Licq;

    sget-object v2, Lict;->b:Lict;

    invoke-virtual {v0, v2}, Licq;->a(Lict;)V

    goto :goto_3

    .line 496
    :cond_7
    invoke-direct {p0}, Ledo;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Ledo;->Y:Licq;

    sget-object v2, Lict;->c:Lict;

    invoke-virtual {v0, v2}, Licq;->a(Lict;)V

    goto :goto_3
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 368
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 370
    iget-object v0, p0, Ledo;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 372
    if-eqz p1, :cond_0

    .line 373
    const-string v0, "events_refresh"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Ledo;->U:Z

    .line 374
    const-string v0, "events_initialload"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Ledo;->V:Z

    .line 375
    const-string v0, "events_datapresent"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Ledo;->W:Z

    .line 381
    :goto_0
    invoke-virtual {p0}, Ledo;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 382
    return-void

    .line 377
    :cond_0
    invoke-virtual {p0}, Ledo;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 378
    const-string v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Ledo;->U:Z

    goto :goto_0
.end method

.method public a(Landroid/text/style/URLSpan;)V
    .locals 0

    .prologue
    .line 565
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 538
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 78
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Ledo;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 386
    const v0, 0x7f10067b

    .line 387
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 388
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 390
    const v0, 0x7f0a062d

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 391
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_mobile_events"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 392
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 341
    invoke-virtual {p3, v1}, Lhos;->a(Z)V

    .line 342
    const-string v0, "fetch_newer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iput-boolean v1, p0, Ledo;->U:Z

    .line 345
    invoke-direct {p0}, Ledo;->e()V

    .line 347
    new-instance v0, Ledp;

    invoke-direct {v0, p0}, Ledp;-><init>(Ledo;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 356
    :cond_0
    return-void
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Ledo;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 144
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 396
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 397
    const v1, 0x7f10067b

    if-ne v0, v1, :cond_0

    .line 398
    invoke-direct {p0}, Ledo;->U()V

    .line 399
    const/4 v0, 0x1

    .line 401
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 444
    invoke-super {p0}, Llol;->aO_()V

    .line 446
    iget-object v0, p0, Ledo;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    invoke-direct {p0}, Ledo;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Ledo;->Y:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 451
    :cond_0
    iget-boolean v0, p0, Ledo;->U:Z

    if-eqz v0, :cond_1

    .line 452
    invoke-direct {p0}, Ledo;->V()V

    .line 455
    :cond_1
    iget-object v0, p0, Ledo;->X:Levx;

    if-eqz v0, :cond_2

    .line 456
    iget-object v0, p0, Ledo;->X:Levx;

    invoke-virtual {v0}, Levx;->a()V

    .line 458
    :cond_2
    return-void
.end method

.method public aQ_()Z
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Ledo;->Q:Lhoc;

    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 135
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 526
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 527
    iget-object v0, p0, Ledo;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 528
    iget-object v0, p0, Ledo;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ledo;->O:Lhee;

    .line 529
    iget-object v0, p0, Ledo;->au:Llnh;

    const-class v1, Llhe;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhe;

    .line 530
    if-eqz v0, :cond_0

    .line 531
    iget-object v1, p0, Ledo;->P:Llhd;

    invoke-virtual {v0}, Llhe;->a()Llhc;

    move-result-object v0

    invoke-virtual {v1, v0}, Llhd;->a(Llhc;)V

    .line 533
    :cond_0
    iget-object v0, p0, Ledo;->au:Llnh;

    const-class v1, Lhsn;

    new-instance v2, Lfdo;

    iget-object v3, p0, Ledo;->at:Llnl;

    invoke-direct {v2, v3}, Lfdo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 534
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 360
    const-string v0, "events_refresh"

    iget-boolean v1, p0, Ledo;->U:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 361
    const-string v0, "events_initialload"

    iget-boolean v1, p0, Ledo;->V:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 362
    const-string v0, "events_datapresent"

    iget-boolean v1, p0, Ledo;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 363
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 364
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 542
    iget-object v0, p0, Ledo;->O:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 544
    instance-of v0, p1, Lfxm;

    if-eqz v0, :cond_1

    .line 545
    check-cast p1, Lfxm;

    .line 546
    invoke-virtual {p1}, Lfxm;->f()Lidh;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_0

    .line 549
    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v2

    .line 550
    invoke-virtual {v0}, Lidh;->e()Ljava/lang/String;

    move-result-object v0

    .line 551
    invoke-virtual {p0}, Ledo;->n()Lz;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v1, v2, v0, v4}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Ledo;->a(Landroid/content/Intent;)V

    .line 561
    :cond_0
    :goto_0
    return-void

    .line 554
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v2, 0x7f10030a

    if-eq v0, v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v2, 0x7f10030c

    if-ne v0, v2, :cond_0

    .line 555
    :cond_2
    iget-object v0, p0, Ledo;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Ledo;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->ay:Lhmv;

    .line 556
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 555
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 558
    invoke-virtual {p0}, Ledo;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 557
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/NewEventActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account_id"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 559
    invoke-virtual {p0, v2}, Ledo;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
