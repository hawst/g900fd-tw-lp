.class public final Lmek;
.super Loxq;
.source "PG"


# instance fields
.field private a:Loji;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10933
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10936
    iput-object v0, p0, Lmek;->apiHeader:Llyq;

    .line 10939
    iput-object v0, p0, Lmek;->a:Loji;

    .line 10933
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10956
    const/4 v0, 0x0

    .line 10957
    iget-object v1, p0, Lmek;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 10958
    const/4 v0, 0x1

    iget-object v1, p0, Lmek;->apiHeader:Llyq;

    .line 10959
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10961
    :cond_0
    iget-object v1, p0, Lmek;->a:Loji;

    if-eqz v1, :cond_1

    .line 10962
    const/4 v1, 0x2

    iget-object v2, p0, Lmek;->a:Loji;

    .line 10963
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10965
    :cond_1
    iget-object v1, p0, Lmek;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10966
    iput v0, p0, Lmek;->ai:I

    .line 10967
    return v0
.end method

.method public a(Loxn;)Lmek;
    .locals 2

    .prologue
    .line 10975
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10976
    sparse-switch v0, :sswitch_data_0

    .line 10980
    iget-object v1, p0, Lmek;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10981
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmek;->ah:Ljava/util/List;

    .line 10984
    :cond_1
    iget-object v1, p0, Lmek;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10986
    :sswitch_0
    return-object p0

    .line 10991
    :sswitch_1
    iget-object v0, p0, Lmek;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 10992
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmek;->apiHeader:Llyq;

    .line 10994
    :cond_2
    iget-object v0, p0, Lmek;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10998
    :sswitch_2
    iget-object v0, p0, Lmek;->a:Loji;

    if-nez v0, :cond_3

    .line 10999
    new-instance v0, Loji;

    invoke-direct {v0}, Loji;-><init>()V

    iput-object v0, p0, Lmek;->a:Loji;

    .line 11001
    :cond_3
    iget-object v0, p0, Lmek;->a:Loji;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10976
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10944
    iget-object v0, p0, Lmek;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 10945
    const/4 v0, 0x1

    iget-object v1, p0, Lmek;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10947
    :cond_0
    iget-object v0, p0, Lmek;->a:Loji;

    if-eqz v0, :cond_1

    .line 10948
    const/4 v0, 0x2

    iget-object v1, p0, Lmek;->a:Loji;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10950
    :cond_1
    iget-object v0, p0, Lmek;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10952
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10929
    invoke-virtual {p0, p1}, Lmek;->a(Loxn;)Lmek;

    move-result-object v0

    return-object v0
.end method
