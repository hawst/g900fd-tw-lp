.class public final Lela;
.super Lhye;
.source "PG"


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:[Ljava/lang/String;

.field private final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljqs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "in_my_circles"

    aput-object v2, v0, v1

    sput-object v0, Lela;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/HashMap;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljqs;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 51
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lela;->a(Landroid/net/Uri;)V

    .line 52
    iput p2, p0, Lela;->b:I

    .line 53
    iput-object p3, p0, Lela;->c:[Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lela;->d:Ljava/util/HashMap;

    .line 55
    if-eqz p5, :cond_0

    const-string v0, "gaia_id IS NOT NULL"

    :goto_0
    invoke-virtual {p0, v0}, Lela;->a(Ljava/lang/String;)V

    .line 56
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lhym;Ljava/util/HashMap;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhym;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljqs;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    const-string v0, "_id"

    invoke-virtual {p1, v0}, Lhym;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 126
    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Lhym;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 127
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lhym;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 128
    const-string v0, "gaia_id"

    invoke-virtual {p1, v0}, Lhym;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 130
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 131
    const/4 v0, 0x0

    .line 133
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v2, v0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 134
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 135
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqs;

    .line 136
    iget-object v3, p0, Lela;->c:[Ljava/lang/String;

    array-length v3, v3

    new-array v10, v3, [Ljava/lang/Object;

    .line 137
    add-int/lit8 v3, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v4

    .line 138
    aput-object v1, v10, v5

    .line 139
    invoke-virtual {v0}, Ljqs;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v6

    .line 140
    invoke-virtual {v0}, Ljqs;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    invoke-virtual {v0}, Ljqs;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v7

    .line 143
    :cond_0
    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 144
    goto :goto_0

    .line 146
    :cond_1
    new-instance v0, Lelb;

    invoke-direct {v0, v6}, Lelb;-><init>(I)V

    invoke-static {v8, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 162
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 163
    invoke-virtual {p1, v0}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_1

    .line 165
    :cond_2
    return-void
.end method

.method private a(Ljava/util/HashMap;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljqs;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    const-string v0, "person_id IN("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v7

    .line 90
    :goto_0
    iget-object v3, p0, Lela;->d:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 91
    if-lez v0, :cond_0

    .line 92
    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    const/16 v3, 0x3f

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_1
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 97
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 98
    iget-object v0, p0, Lela;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    .line 100
    iget v1, p0, Lela;->b:I

    .line 101
    invoke-virtual {p0}, Lela;->n()Landroid/content/Context;

    move-result-object v0

    sget-object v3, Lela;->e:[Ljava/lang/String;

    move-object v6, v2

    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 103
    if-nez v1, :cond_2

    .line 117
    :goto_1
    return v7

    .line 108
    :cond_2
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 114
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v7, v8

    .line 117
    goto :goto_1
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 63
    new-instance v0, Lhym;

    iget-object v1, p0, Lela;->c:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    .line 65
    iget-object v1, p0, Lela;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-object v0

    .line 69
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lela;->d:Ljava/util/HashMap;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 71
    invoke-direct {p0, v1}, Lela;->a(Ljava/util/HashMap;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 72
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :cond_2
    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 79
    invoke-direct {p0, v0, v1}, Lela;->a(Lhym;Ljava/util/HashMap;)V

    goto :goto_0
.end method
