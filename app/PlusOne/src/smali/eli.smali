.class final Leli;
.super Lekx;
.source "PG"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/view/LayoutInflater;

.field private synthetic h:Lelc;


# direct methods
.method constructor <init>(Lelc;)V
    .locals 2

    .prologue
    .line 102
    iput-object p1, p0, Leli;->h:Lelc;

    invoke-direct {p0}, Lekx;-><init>()V

    .line 119
    iget-object v0, p0, Leli;->h:Lelc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lelc;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Leli;->g:Landroid/view/LayoutInflater;

    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Leli;->d:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 180
    if-nez p2, :cond_1

    .line 182
    if-nez p1, :cond_0

    const-string v0, "BULLHORN"

    .line 188
    :goto_0
    return-object v0

    .line 182
    :cond_0
    const-string v0, "YMK"

    goto :goto_0

    .line 186
    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Leli;->a:Ljava/util/ArrayList;

    add-int/lit8 v1, p2, -0x1

    .line 187
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnrp;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Leli;->b:Ljava/util/ArrayList;

    add-int/lit8 v1, p2, -0x1

    .line 188
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnrp;

    goto :goto_0
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Leli;->d:I

    if-ge p1, v0, :cond_0

    :goto_0
    return p1

    :cond_0
    iget v0, p0, Leli;->d:I

    sub-int/2addr p1, v0

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 129
    iget-object v0, p0, Leli;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Leli;->c:I

    .line 130
    iget v3, p0, Leli;->c:I

    iget v0, p0, Leli;->c:I

    if-lez v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v3

    iput v0, p0, Leli;->d:I

    .line 132
    iget-object v0, p0, Leli;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput v0, p0, Leli;->e:I

    .line 133
    iget v0, p0, Leli;->c:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iget v3, p0, Leli;->e:I

    if-nez v3, :cond_4

    :goto_4
    add-int/2addr v0, v1

    iput v0, p0, Leli;->f:I

    .line 134
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Leli;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 130
    goto :goto_1

    .line 132
    :cond_2
    iget-object v0, p0, Leli;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 133
    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/view/View;I)Landroid/view/View;
    .locals 5

    .prologue
    .line 293
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    .line 294
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->b()V

    .line 296
    iget-object v0, p0, Leli;->h:Lelc;

    invoke-static {v0}, Lelc;->a(Lelc;)Llnl;

    move-result-object v0

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 298
    const v0, 0x7f10048a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 300
    const v1, 0x7f10048b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 301
    if-nez p2, :cond_0

    .line 302
    const v2, 0x7f0a09ba

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 303
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v1, v2

    .line 310
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    return-object p1

    .line 305
    :cond_0
    const v2, 0x7f0a09bf

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 306
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 307
    const v4, 0x7f0a09c0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, v2

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Leli;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Leli;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Lemi;->a(Ljava/util/ArrayList;)Z

    .line 319
    :cond_0
    iget-object v0, p0, Leli;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Leli;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Lemi;->a(Ljava/util/ArrayList;)Z

    .line 322
    :cond_1
    invoke-direct {p0}, Leli;->b()V

    .line 323
    invoke-virtual {p0}, Leli;->notifyDataSetChanged()V

    .line 324
    iget-object v0, p0, Leli;->h:Lelc;

    iget-object v1, p0, Leli;->h:Lelc;

    invoke-virtual {v1}, Lelc;->x()Landroid/view/View;

    invoke-virtual {v0}, Lelc;->aa()V

    .line 325
    return-void
.end method

.method public a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    iput-object p1, p0, Leli;->a:Ljava/util/ArrayList;

    .line 124
    iput-object p2, p0, Leli;->b:Ljava/util/ArrayList;

    .line 125
    invoke-direct {p0}, Leli;->b()V

    .line 126
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Leli;->c:I

    iget v1, p0, Leli;->e:I

    add-int/2addr v0, v1

    iget v1, p0, Leli;->f:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 175
    invoke-direct {p0, p1}, Leli;->a(I)I

    move-result v0

    .line 176
    invoke-direct {p0, p1}, Leli;->b(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Leli;->a(II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 193
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 208
    invoke-direct {p0, p1}, Leli;->a(I)I

    move-result v0

    .line 209
    invoke-direct {p0, p1}, Leli;->b(I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 216
    invoke-direct {p0, p1}, Leli;->a(I)I

    move-result v6

    .line 217
    invoke-direct {p0, p1}, Leli;->b(I)I

    move-result v7

    .line 219
    if-nez v7, :cond_1

    .line 221
    if-nez p2, :cond_0

    .line 222
    iget-object v0, p0, Leli;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f040162

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 224
    :cond_0
    invoke-virtual {p0, p2, v6}, Leli;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    .line 277
    :goto_0
    return-object p2

    .line 229
    :cond_1
    if-nez p2, :cond_3

    .line 230
    iget-object v0, p0, Leli;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f040164

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    move-object p2, v0

    .line 236
    :goto_1
    invoke-virtual {p0, p1}, Leli;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 238
    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 239
    iget-object v2, p0, Leli;->h:Lelc;

    iget-object v8, p0, Leli;->h:Lelc;

    iget-object v8, v8, Lelc;->U:Lhxh;

    invoke-virtual {v0, v2, v8, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 241
    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->b(Z)V

    .line 243
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->b()V

    .line 245
    invoke-direct {p0, v6, v7}, Leli;->a(II)Ljava/lang/Object;

    move-result-object v2

    iget-object v8, p0, Leli;->h:Lelc;

    iget-object v8, v8, Lelc;->W:Ljava/lang/String;

    iget-object v9, p0, Leli;->h:Lelc;

    iget-object v9, v9, Lelc;->Y:Lfuw;

    invoke-virtual {v0, v2, v8, v9}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/Object;Ljava/lang/String;Lfuw;)V

    .line 246
    iget-object v2, p0, Leli;->h:Lelc;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    check-cast v1, Lnrp;

    .line 254
    iget v2, v1, Lnrp;->g:I

    if-nez v2, :cond_4

    const/16 v2, 0xc

    .line 259
    :goto_2
    invoke-virtual {v0, v2, v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(ILjava/lang/Object;)V

    .line 261
    packed-switch v2, :pswitch_data_0

    move-object v2, v5

    .line 262
    :goto_3
    if-eqz v2, :cond_2

    .line 263
    new-instance v5, Lhmk;

    invoke-direct {v5, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {p2, v5}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 265
    :cond_2
    new-instance v2, Lkqq;

    sget-object v5, Lonl;->j:Lhmn;

    iget-object v8, v1, Lnrp;->e:Ljava/lang/String;

    invoke-direct {v2, v5, v8}, Lkqq;-><init>(Lhmn;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 267
    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    .line 269
    if-nez v6, :cond_6

    iget v0, p0, Leli;->c:I

    :goto_4
    if-ne v7, v0, :cond_7

    move v0, v3

    .line 270
    :goto_5
    iget-object v2, v1, Lnrp;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    iget-object v1, v1, Lnrp;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 271
    :goto_6
    if-eqz v0, :cond_9

    .line 272
    invoke-virtual {p2, v3}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->b(Z)V

    goto :goto_0

    .line 233
    :cond_3
    check-cast p2, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    goto :goto_1

    .line 254
    :cond_4
    if-nez v7, :cond_5

    const/16 v2, 0xd

    goto :goto_2

    :cond_5
    const/16 v2, 0xe

    goto :goto_2

    .line 261
    :pswitch_0
    sget-object v2, Lonl;->h:Lhmn;

    goto :goto_3

    :pswitch_1
    sget-object v2, Lonl;->i:Lhmn;

    goto :goto_3

    .line 269
    :cond_6
    iget v0, p0, Leli;->e:I

    goto :goto_4

    :cond_7
    move v0, v4

    goto :goto_5

    :cond_8
    move v3, v4

    .line 270
    goto :goto_6

    .line 274
    :cond_9
    invoke-virtual {p2, v3}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a(Z)V

    goto/16 :goto_0

    .line 261
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x3

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Leli;->c:I

    if-nez v0, :cond_0

    iget v0, p0, Leli;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method
