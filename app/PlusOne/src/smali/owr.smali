.class public final Lowr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lowr;


# instance fields
.field private b:Lows;

.field private c:[I

.field private d:I

.field private e:Loww;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lowr;

    sput-object v0, Lowr;->a:[Lowr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lowr;->b:Lows;

    .line 30
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lowr;->c:[I

    .line 33
    const/high16 v0, -0x80000000

    iput v0, p0, Lowr;->d:I

    .line 36
    iput-object v1, p0, Lowr;->e:Loww;

    .line 19
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 61
    .line 62
    iget-object v0, p0, Lowr;->b:Lows;

    if-eqz v0, :cond_4

    .line 63
    const/4 v0, 0x1

    iget-object v2, p0, Lowr;->b:Lows;

    .line 64
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 66
    :goto_0
    iget-object v2, p0, Lowr;->c:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lowr;->c:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 68
    iget-object v3, p0, Lowr;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 70
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 68
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 72
    :cond_0
    add-int/2addr v0, v2

    .line 73
    iget-object v1, p0, Lowr;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 75
    :cond_1
    iget v1, p0, Lowr;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 76
    const/4 v1, 0x3

    iget v2, p0, Lowr;->d:I

    .line 77
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_2
    iget-object v1, p0, Lowr;->e:Loww;

    if-eqz v1, :cond_3

    .line 80
    const/4 v1, 0x4

    iget-object v2, p0, Lowr;->e:Loww;

    .line 81
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_3
    iget-object v1, p0, Lowr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    iput v0, p0, Lowr;->ai:I

    .line 85
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lowr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 93
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 94
    sparse-switch v0, :sswitch_data_0

    .line 98
    iget-object v1, p0, Lowr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lowr;->ah:Ljava/util/List;

    .line 102
    :cond_1
    iget-object v1, p0, Lowr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    :sswitch_0
    return-object p0

    .line 109
    :sswitch_1
    iget-object v0, p0, Lowr;->b:Lows;

    if-nez v0, :cond_2

    .line 110
    new-instance v0, Lows;

    invoke-direct {v0}, Lows;-><init>()V

    iput-object v0, p0, Lowr;->b:Lows;

    .line 112
    :cond_2
    iget-object v0, p0, Lowr;->b:Lows;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 116
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 117
    iget-object v0, p0, Lowr;->c:[I

    array-length v0, v0

    .line 118
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 119
    iget-object v2, p0, Lowr;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    iput-object v1, p0, Lowr;->c:[I

    .line 121
    :goto_1
    iget-object v1, p0, Lowr;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 122
    iget-object v1, p0, Lowr;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 123
    invoke-virtual {p1}, Loxn;->a()I

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 126
    :cond_3
    iget-object v1, p0, Lowr;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 130
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 131
    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 133
    :cond_4
    iput v0, p0, Lowr;->d:I

    goto :goto_0

    .line 135
    :cond_5
    iput v3, p0, Lowr;->d:I

    goto :goto_0

    .line 140
    :sswitch_4
    iget-object v0, p0, Lowr;->e:Loww;

    if-nez v0, :cond_6

    .line 141
    new-instance v0, Loww;

    invoke-direct {v0}, Loww;-><init>()V

    iput-object v0, p0, Lowr;->e:Loww;

    .line 143
    :cond_6
    iget-object v0, p0, Lowr;->e:Loww;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 94
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 41
    iget-object v0, p0, Lowr;->b:Lows;

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x1

    iget-object v1, p0, Lowr;->b:Lows;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lowr;->c:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lowr;->c:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 45
    iget-object v1, p0, Lowr;->c:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 46
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_1
    iget v0, p0, Lowr;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 50
    const/4 v0, 0x3

    iget v1, p0, Lowr;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 52
    :cond_2
    iget-object v0, p0, Lowr;->e:Loww;

    if-eqz v0, :cond_3

    .line 53
    const/4 v0, 0x4

    iget-object v1, p0, Lowr;->e:Loww;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 55
    :cond_3
    iget-object v0, p0, Lowr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 57
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lowr;->a(Loxn;)Lowr;

    move-result-object v0

    return-object v0
.end method
