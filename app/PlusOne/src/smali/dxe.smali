.class public final Ldxe;
.super Lfhh;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-direct {p0}, Lfhh;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILfib;)V
    .locals 4

    .prologue
    .line 67
    const-string v0, "OobProfilePhotoFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x35

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onUploadProfilePhotoComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 72
    :cond_0
    iget-object v0, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->a(ILfib;)V

    .line 74
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v0

    if-nez v0, :cond_1

    .line 75
    iget-object v0, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->a(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Llnh;

    move-result-object v0

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 76
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-static {v0}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    iget-object v1, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    iget-object v2, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->n()Lz;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, p2, v0, v3}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->a(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v0, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->b(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V

    goto :goto_0
.end method

.method public l(ILfib;)V
    .locals 2

    .prologue
    .line 89
    const-string v0, "OobProfilePhotoFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x37

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onGetProfileAndContactComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 93
    :cond_0
    iget-object v0, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->N:Ljava/lang/Boolean;

    .line 94
    iget-object v0, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->a(ILfib;)V

    .line 95
    iget-object v0, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->c(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V

    .line 96
    iget-object v0, p0, Ldxe;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->b(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V

    .line 97
    return-void
.end method

.method public m(ILfib;)V
    .locals 2

    .prologue
    .line 102
    const-string v0, "OobProfilePhotoFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x34

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onInsertCameraPhotoComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    :cond_0
    return-void
.end method
