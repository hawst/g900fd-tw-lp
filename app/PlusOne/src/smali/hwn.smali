.class final Lhwn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhwm;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private synthetic c:Lhwi;


# direct methods
.method constructor <init>(Lhwi;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Lhwn;->c:Lhwi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Lhwm;
    .locals 1

    .prologue
    .line 722
    iget v0, p0, Lhwn;->b:I

    if-ge p1, v0, :cond_0

    .line 723
    iget-object v0, p0, Lhwn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhwm;

    return-object v0

    .line 725
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 703
    iget-object v0, p0, Lhwn;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lhwn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    .line 705
    const/4 v0, 0x0

    iput-object v0, p0, Lhwn;->a:Ljava/util/ArrayList;

    .line 714
    :cond_0
    iput v2, p0, Lhwn;->b:I

    .line 715
    return-void

    :cond_1
    move v1, v2

    .line 707
    :goto_0
    iget v0, p0, Lhwn;->b:I

    if-ge v1, v0, :cond_0

    .line 708
    iget-object v0, p0, Lhwn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhwm;

    invoke-virtual {v0}, Lhwm;->a()V

    .line 707
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lhuq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 669
    iget-object v1, p0, Lhwn;->a:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 670
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lhwn;->a:Ljava/util/ArrayList;

    .line 672
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 673
    iget-object v1, p0, Lhwn;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 678
    iget-object v1, p0, Lhwn;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    move v2, v0

    .line 680
    :goto_0
    if-ge v3, v4, :cond_2

    .line 681
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuq;

    .line 683
    if-eqz v0, :cond_3

    .line 686
    if-ge v2, v5, :cond_1

    .line 687
    iget-object v1, p0, Lhwn;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhwm;

    invoke-virtual {v1, v0}, Lhwm;->a(Lhuq;)V

    .line 691
    :goto_1
    add-int/lit8 v0, v2, 0x1

    .line 680
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 689
    :cond_1
    iget-object v1, p0, Lhwn;->a:Ljava/util/ArrayList;

    new-instance v6, Lhwm;

    iget-object v7, p0, Lhwn;->c:Lhwi;

    invoke-direct {v6, v7, v0}, Lhwm;-><init>(Lhwi;Lhuq;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 694
    :cond_2
    iput v2, p0, Lhwn;->b:I

    .line 695
    return-void

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public b()I
    .locals 1

    .prologue
    .line 718
    iget v0, p0, Lhwn;->b:I

    return v0
.end method
