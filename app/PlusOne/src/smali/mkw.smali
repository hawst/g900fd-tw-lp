.class public final Lmkw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmou;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30697
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30700
    iput-object v0, p0, Lmkw;->apiHeader:Llyq;

    .line 30703
    iput-object v0, p0, Lmkw;->a:Lmou;

    .line 30697
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 30720
    const/4 v0, 0x0

    .line 30721
    iget-object v1, p0, Lmkw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 30722
    const/4 v0, 0x1

    iget-object v1, p0, Lmkw;->apiHeader:Llyq;

    .line 30723
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 30725
    :cond_0
    iget-object v1, p0, Lmkw;->a:Lmou;

    if-eqz v1, :cond_1

    .line 30726
    const/4 v1, 0x2

    iget-object v2, p0, Lmkw;->a:Lmou;

    .line 30727
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30729
    :cond_1
    iget-object v1, p0, Lmkw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30730
    iput v0, p0, Lmkw;->ai:I

    .line 30731
    return v0
.end method

.method public a(Loxn;)Lmkw;
    .locals 2

    .prologue
    .line 30739
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30740
    sparse-switch v0, :sswitch_data_0

    .line 30744
    iget-object v1, p0, Lmkw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30745
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkw;->ah:Ljava/util/List;

    .line 30748
    :cond_1
    iget-object v1, p0, Lmkw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30750
    :sswitch_0
    return-object p0

    .line 30755
    :sswitch_1
    iget-object v0, p0, Lmkw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 30756
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmkw;->apiHeader:Llyq;

    .line 30758
    :cond_2
    iget-object v0, p0, Lmkw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30762
    :sswitch_2
    iget-object v0, p0, Lmkw;->a:Lmou;

    if-nez v0, :cond_3

    .line 30763
    new-instance v0, Lmou;

    invoke-direct {v0}, Lmou;-><init>()V

    iput-object v0, p0, Lmkw;->a:Lmou;

    .line 30765
    :cond_3
    iget-object v0, p0, Lmkw;->a:Lmou;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30740
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30708
    iget-object v0, p0, Lmkw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 30709
    const/4 v0, 0x1

    iget-object v1, p0, Lmkw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30711
    :cond_0
    iget-object v0, p0, Lmkw;->a:Lmou;

    if-eqz v0, :cond_1

    .line 30712
    const/4 v0, 0x2

    iget-object v1, p0, Lmkw;->a:Lmou;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30714
    :cond_1
    iget-object v0, p0, Lmkw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30716
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 30693
    invoke-virtual {p0, p1}, Lmkw;->a(Loxn;)Lmkw;

    move-result-object v0

    return-object v0
.end method
