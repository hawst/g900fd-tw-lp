.class public final Lhni;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static d:Lhni;


# instance fields
.field a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Landroid/util/Property",
            "<**>;>;"
        }
    .end annotation
.end field

.field c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lhni;->a:Landroid/util/SparseArray;

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhni;->b:Ljava/util/Map;

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhni;->c:Ljava/util/Map;

    .line 95
    iget-object v0, p0, Lhni;->b:Ljava/util/Map;

    const-string v1, "position"

    new-array v2, v6, [Landroid/util/Property;

    sget-object v3, Lhno;->a:Landroid/util/Property;

    aput-object v3, v2, v4

    sget-object v3, Lhno;->b:Landroid/util/Property;

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v0, p0, Lhni;->c:Ljava/util/Map;

    const-string v1, "position"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "x"

    aput-object v3, v2, v4

    const-string v3, "y"

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lhni;->b:Ljava/util/Map;

    const-string v1, "scale"

    new-array v2, v6, [Landroid/util/Property;

    sget-object v3, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    aput-object v3, v2, v4

    sget-object v3, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v0, p0, Lhni;->c:Ljava/util/Map;

    const-string v1, "scale"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "sx"

    aput-object v3, v2, v4

    const-string v3, "sy"

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v0, p0, Lhni;->b:Ljava/util/Map;

    const-string v1, "opacity"

    new-array v2, v5, [Landroid/util/Property;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v0, p0, Lhni;->c:Ljava/util/Map;

    const-string v1, "opacity"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    return-void
.end method

.method static a(Lhnj;Landroid/util/Property;D)F
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhnj;",
            "Landroid/util/Property",
            "<**>;D)F"
        }
    .end annotation

    .prologue
    .line 394
    invoke-virtual {p0, p1}, Lhnj;->a(Landroid/util/Property;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    new-instance v0, Lhnk;

    const-string v1, "Cannot animate position if stage size was not defined"

    invoke-direct {v0, v1}, Lhnk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 398
    sget-object v1, Lhno;->a:Landroid/util/Property;

    if-ne p1, v1, :cond_2

    .line 399
    iget v0, p0, Lhnj;->a:F

    .line 403
    :cond_1
    :goto_0
    double-to-float v1, p2

    mul-float/2addr v0, v1

    return v0

    .line 400
    :cond_2
    sget-object v1, Lhno;->b:Landroid/util/Property;

    if-ne p1, v1, :cond_1

    .line 401
    iget v0, p0, Lhnj;->b:F

    goto :goto_0
.end method

.method public static a()Lhni;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lhni;->d:Lhni;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lhni;

    invoke-direct {v0}, Lhni;-><init>()V

    sput-object v0, Lhni;->d:Lhni;

    .line 45
    :cond_0
    sget-object v0, Lhni;->d:Lhni;

    return-object v0
.end method

.method private a(Lhnj;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhnj;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 305
    iget-object v0, p1, Lhnj;->c:Lorg/json/JSONObject;

    const-string v1, "animations"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 306
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 307
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 308
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 309
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-direct {p0, p1, v4, v2}, Lhni;->a(Lhnj;Lorg/json/JSONObject;Ljava/util/Collection;)V

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 311
    :cond_0
    return-object v2
.end method

.method private a(Lhnj;Lorg/json/JSONObject;Ljava/util/Collection;)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhnj;",
            "Lorg/json/JSONObject;",
            "Ljava/util/Collection",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 316
    move-object/from16 v0, p1

    iget-object v2, v0, Lhnj;->d:Ljava/util/Map;

    const-string v3, "id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .line 317
    if-nez v13, :cond_1

    .line 325
    :cond_0
    return-void

    .line 320
    :cond_1
    const-string v2, "animations"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 321
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v15

    .line 322
    const/4 v2, 0x0

    move v12, v2

    :goto_0
    if-ge v12, v15, :cond_0

    .line 323
    invoke-virtual {v14, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v2, "duration"

    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v6

    double-to-long v0, v2

    move-wide/from16 v16, v0

    const-string v2, "delay"

    const-wide/16 v6, 0x0

    invoke-virtual {v5, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v6

    double-to-long v0, v2

    move-wide/from16 v18, v0

    const-string v2, "property"

    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lhni;->b:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/util/Property;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhni;->c:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    if-nez v3, :cond_5

    const/4 v4, 0x1

    :goto_1
    const-string v6, "keyframes"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lorg/json/JSONArray;->length()I

    move-result v21

    if-lez v21, :cond_b

    const/4 v5, 0x0

    :goto_2
    array-length v6, v2

    if-ge v5, v6, :cond_b

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    move v10, v6

    move-object v11, v7

    move-object v6, v8

    :goto_3
    move/from16 v0, v21

    if-ge v10, v0, :cond_9

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v22

    if-eqz v4, :cond_6

    const/4 v7, 0x0

    :goto_4
    if-eqz v7, :cond_2

    aget-object v8, v3, v5

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    :cond_2
    if-nez v6, :cond_3

    move/from16 v0, v21

    new-array v6, v0, [Landroid/animation/Keyframe;

    :cond_3
    const/4 v8, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v8

    double-to-float v0, v8

    move/from16 v23, v0

    aget-object v24, v2, v5

    if-eqz v4, :cond_7

    const/4 v7, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v8

    :goto_5
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1, v8, v9}, Lhni;->a(Lhnj;Landroid/util/Property;D)F

    move-result v7

    move/from16 v0, v23

    invoke-static {v0, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    aput-object v7, v6, v10

    if-eqz v11, :cond_4

    add-int/lit8 v7, v10, -0x1

    aget-object v7, v6, v7

    aget-object v8, v6, v10

    invoke-interface {v11, v7, v8}, Lhnn;->a(Landroid/animation/Keyframe;Landroid/animation/Keyframe;)V

    aget-object v7, v6, v10

    invoke-virtual {v7, v11}, Landroid/animation/Keyframe;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_4
    invoke-virtual/range {v22 .. v22}, Lorg/json/JSONArray;->length()I

    move-result v7

    const/4 v8, 0x3

    if-lt v7, v8, :cond_8

    const/4 v7, 0x2

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v7, "name"

    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v9, "cubic-bezier"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    new-instance v7, Lhnm;

    const-string v9, "x1"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v9, v0

    const-string v11, "y1"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v11, v0

    const-string v22, "x2"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    const-string v23, "y2"

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v8, v0

    move/from16 v0, v22

    invoke-direct {v7, v9, v11, v0, v8}, Lhnm;-><init>(FFFF)V

    :goto_6
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    move-object v11, v7

    goto/16 :goto_3

    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v7, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    goto/16 :goto_4

    :cond_7
    aget-object v8, v3, v5

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    goto/16 :goto_5

    :cond_8
    const/4 v7, 0x0

    goto :goto_6

    :cond_9
    if-eqz v6, :cond_a

    const/4 v7, 0x1

    new-array v7, v7, [Landroid/animation/PropertyValuesHolder;

    const/4 v8, 0x0

    aget-object v9, v2, v5

    invoke-static {v9, v6}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v7, v8

    invoke-static {v13, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    move-wide/from16 v0, v16

    invoke-virtual {v6, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    move-result-object v7

    move-wide/from16 v0, v18

    invoke-virtual {v7, v0, v1}, Landroid/animation/Animator;->setStartDelay(J)V

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 322
    :cond_b
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;ILjava/util/Map;)Landroid/animation/Animator;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/animation/Animator;"
        }
    .end annotation

    .prologue
    const/high16 v4, -0x40800000    # -1.0f

    .line 122
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lhni;->a(Landroid/content/Context;ILjava/util/Map;FF)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;ILjava/util/Map;FF)Landroid/animation/Animator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;FF)",
            "Landroid/animation/Animator;"
        }
    .end annotation

    .prologue
    .line 158
    :try_start_0
    new-instance v0, Lhnj;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lhnj;-><init>(Lhni;Landroid/content/Context;ILjava/util/Map;FF)V

    .line 160
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 161
    invoke-direct {p0, v0}, Lhni;->a(Lhnj;)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 162
    new-instance v2, Lhnl;

    invoke-direct {v2, p0, v0}, Lhnl;-><init>(Lhni;Lhnj;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 163
    return-object v1

    .line 164
    :catch_0
    move-exception v0

    .line 165
    new-instance v1, Lhnk;

    invoke-direct {v1, v0}, Lhnk;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 166
    :catch_1
    move-exception v0

    .line 167
    new-instance v1, Lhnk;

    invoke-direct {v1, v0}, Lhnk;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 168
    :catch_2
    move-exception v0

    .line 169
    new-instance v1, Lhnk;

    invoke-direct {v1, v0}, Lhnk;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 235
    const/4 v1, 0x0

    .line 237
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 238
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 239
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 241
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 242
    if-eqz v3, :cond_1

    .line 243
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 249
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 250
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    throw v0

    .line 247
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 249
    if-eqz v1, :cond_2

    .line 250
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_2
    return-object v0
.end method
