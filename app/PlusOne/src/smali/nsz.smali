.class public final Lnsz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:[Ljava/lang/String;

.field public d:[Lnsa;

.field private e:I

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 162
    invoke-direct {p0}, Loxq;-><init>()V

    .line 193
    iput v1, p0, Lnsz;->b:I

    .line 196
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnsz;->c:[Ljava/lang/String;

    .line 199
    iput v1, p0, Lnsz;->e:I

    .line 202
    sget-object v0, Lnsa;->a:[Lnsa;

    iput-object v0, p0, Lnsz;->d:[Lnsa;

    .line 162
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 239
    .line 240
    iget-object v0, p0, Lnsz;->a:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 241
    const/4 v0, 0x1

    iget-object v2, p0, Lnsz;->a:Ljava/lang/String;

    .line 242
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 244
    :goto_0
    iget v2, p0, Lnsz;->b:I

    if-eq v2, v7, :cond_0

    .line 245
    const/4 v2, 0x2

    iget v3, p0, Lnsz;->b:I

    .line 246
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 248
    :cond_0
    iget-object v2, p0, Lnsz;->c:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnsz;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 250
    iget-object v4, p0, Lnsz;->c:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 252
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 250
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 254
    :cond_1
    add-int/2addr v0, v3

    .line 255
    iget-object v2, p0, Lnsz;->c:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 257
    :cond_2
    iget v2, p0, Lnsz;->e:I

    if-eq v2, v7, :cond_3

    .line 258
    const/4 v2, 0x4

    iget v3, p0, Lnsz;->e:I

    .line 259
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 261
    :cond_3
    iget-object v2, p0, Lnsz;->d:[Lnsa;

    if-eqz v2, :cond_5

    .line 262
    iget-object v2, p0, Lnsz;->d:[Lnsa;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 263
    if-eqz v4, :cond_4

    .line 264
    const/4 v5, 0x6

    .line 265
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 262
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 269
    :cond_5
    iget-object v1, p0, Lnsz;->f:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 270
    const/4 v1, 0x7

    iget-object v2, p0, Lnsz;->f:Ljava/lang/String;

    .line 271
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_6
    iget-object v1, p0, Lnsz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Lnsz;->ai:I

    .line 275
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnsz;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 283
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 284
    sparse-switch v0, :sswitch_data_0

    .line 288
    iget-object v2, p0, Lnsz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 289
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnsz;->ah:Ljava/util/List;

    .line 292
    :cond_1
    iget-object v2, p0, Lnsz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    :sswitch_0
    return-object p0

    .line 299
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsz;->a:Ljava/lang/String;

    goto :goto_0

    .line 303
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 304
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd

    if-eq v0, v2, :cond_2

    const/16 v2, 0xe

    if-eq v0, v2, :cond_2

    const/16 v2, 0xf

    if-eq v0, v2, :cond_2

    const/16 v2, 0x10

    if-eq v0, v2, :cond_2

    const/16 v2, 0x11

    if-eq v0, v2, :cond_2

    const/16 v2, 0x12

    if-eq v0, v2, :cond_2

    const/16 v2, 0x13

    if-eq v0, v2, :cond_2

    const/16 v2, 0x14

    if-eq v0, v2, :cond_2

    const/16 v2, 0x15

    if-eq v0, v2, :cond_2

    const/16 v2, 0x16

    if-eq v0, v2, :cond_2

    const/16 v2, 0x17

    if-ne v0, v2, :cond_3

    .line 327
    :cond_2
    iput v0, p0, Lnsz;->b:I

    goto :goto_0

    .line 329
    :cond_3
    iput v1, p0, Lnsz;->b:I

    goto :goto_0

    .line 334
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 335
    iget-object v0, p0, Lnsz;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 336
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 337
    iget-object v3, p0, Lnsz;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 338
    iput-object v2, p0, Lnsz;->c:[Ljava/lang/String;

    .line 339
    :goto_1
    iget-object v2, p0, Lnsz;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 340
    iget-object v2, p0, Lnsz;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 341
    invoke-virtual {p1}, Loxn;->a()I

    .line 339
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 344
    :cond_4
    iget-object v2, p0, Lnsz;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 348
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 349
    if-eqz v0, :cond_5

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_5

    if-eq v0, v6, :cond_5

    if-ne v0, v7, :cond_6

    .line 354
    :cond_5
    iput v0, p0, Lnsz;->e:I

    goto/16 :goto_0

    .line 356
    :cond_6
    iput v1, p0, Lnsz;->e:I

    goto/16 :goto_0

    .line 361
    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 362
    iget-object v0, p0, Lnsz;->d:[Lnsa;

    if-nez v0, :cond_8

    move v0, v1

    .line 363
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsa;

    .line 364
    iget-object v3, p0, Lnsz;->d:[Lnsa;

    if-eqz v3, :cond_7

    .line 365
    iget-object v3, p0, Lnsz;->d:[Lnsa;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 367
    :cond_7
    iput-object v2, p0, Lnsz;->d:[Lnsa;

    .line 368
    :goto_3
    iget-object v2, p0, Lnsz;->d:[Lnsa;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 369
    iget-object v2, p0, Lnsz;->d:[Lnsa;

    new-instance v3, Lnsa;

    invoke-direct {v3}, Lnsa;-><init>()V

    aput-object v3, v2, v0

    .line 370
    iget-object v2, p0, Lnsz;->d:[Lnsa;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 371
    invoke-virtual {p1}, Loxn;->a()I

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 362
    :cond_8
    iget-object v0, p0, Lnsz;->d:[Lnsa;

    array-length v0, v0

    goto :goto_2

    .line 374
    :cond_9
    iget-object v2, p0, Lnsz;->d:[Lnsa;

    new-instance v3, Lnsa;

    invoke-direct {v3}, Lnsa;-><init>()V

    aput-object v3, v2, v0

    .line 375
    iget-object v2, p0, Lnsz;->d:[Lnsa;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 379
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsz;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 284
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v6, -0x80000000

    .line 209
    iget-object v1, p0, Lnsz;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 210
    const/4 v1, 0x1

    iget-object v2, p0, Lnsz;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 212
    :cond_0
    iget v1, p0, Lnsz;->b:I

    if-eq v1, v6, :cond_1

    .line 213
    const/4 v1, 0x2

    iget v2, p0, Lnsz;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 215
    :cond_1
    iget-object v1, p0, Lnsz;->c:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 216
    iget-object v2, p0, Lnsz;->c:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 217
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 216
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 220
    :cond_2
    iget v1, p0, Lnsz;->e:I

    if-eq v1, v6, :cond_3

    .line 221
    const/4 v1, 0x4

    iget v2, p0, Lnsz;->e:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 223
    :cond_3
    iget-object v1, p0, Lnsz;->d:[Lnsa;

    if-eqz v1, :cond_5

    .line 224
    iget-object v1, p0, Lnsz;->d:[Lnsa;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 225
    if-eqz v3, :cond_4

    .line 226
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 224
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 230
    :cond_5
    iget-object v0, p0, Lnsz;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 231
    const/4 v0, 0x7

    iget-object v1, p0, Lnsz;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 233
    :cond_6
    iget-object v0, p0, Lnsz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 235
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0, p1}, Lnsz;->a(Loxn;)Lnsz;

    move-result-object v0

    return-object v0
.end method
