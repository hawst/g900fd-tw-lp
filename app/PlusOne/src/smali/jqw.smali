.class public final Ljqw;
.super Llgr;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Llgr;-><init>()V

    return-void
.end method

.method public static a(Lu;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Lu;->p()Lae;

    move-result-object v1

    .line 34
    invoke-virtual {v1, p2}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 35
    if-nez v0, :cond_1

    .line 36
    invoke-virtual {p0}, Lu;->n()Lz;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    new-instance v0, Ljqw;

    invoke-direct {v0}, Ljqw;-><init>()V

    .line 39
    if-nez p5, :cond_0

    .line 40
    new-instance p5, Landroid/os/Bundle;

    invoke-direct {p5}, Landroid/os/Bundle;-><init>()V

    .line 42
    :cond_0
    const-string v2, "personName"

    invoke-virtual {p5, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v2, "defaultCircleName"

    invoke-virtual {p5, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v2, "account_id"

    invoke-virtual {p5, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 45
    invoke-virtual {v0, p5}, Ljqw;->f(Landroid/os/Bundle;)V

    .line 46
    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljqw;->a(Lu;I)V

    .line 47
    invoke-virtual {v0, v1, p2}, Ljqw;->a(Lae;Ljava/lang/String;)V

    .line 50
    :cond_1
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 54
    invoke-virtual {p0}, Ljqw;->n()Lz;

    move-result-object v0

    .line 55
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 57
    invoke-virtual {p0}, Ljqw;->k()Landroid/os/Bundle;

    move-result-object v2

    .line 58
    const-string v3, "personName"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 59
    const-string v3, "defaultCircleName"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    invoke-virtual {p0}, Ljqw;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0520

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 63
    const v3, 0x7f0a0597

    invoke-virtual {v1, v3, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 64
    const v3, 0x7f0a0583

    invoke-virtual {v1, v3, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 65
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 67
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04007c

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 68
    const v0, 0x7f100139

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 69
    invoke-virtual {p0}, Ljqw;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0521

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 74
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 79
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 80
    invoke-virtual {p0}, Ljqw;->n()Lz;

    move-result-object v1

    .line 81
    invoke-virtual {p0}, Ljqw;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 82
    const-class v0, Ljpb;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpb;

    .line 83
    invoke-interface {v0, v1, v2}, Ljpb;->b(Landroid/content/Context;I)V

    .line 84
    new-instance v0, Ljrk;

    invoke-direct {v0, v1, v2}, Ljrk;-><init>(Landroid/content/Context;I)V

    .line 85
    invoke-virtual {v0}, Ljrk;->e()V

    .line 86
    const-class v2, Lhoc;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    invoke-static {v1, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 88
    :cond_0
    invoke-super {p0, p1, p2}, Llgr;->onClick(Landroid/content/DialogInterface;I)V

    .line 89
    return-void
.end method
