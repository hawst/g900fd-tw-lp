.class public final Ldpj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Ldpj;->a:Landroid/content/Context;

    .line 62
    return-void
.end method


# virtual methods
.method public a()Ldpi;
    .locals 15

    .prologue
    .line 125
    new-instance v0, Ldpi;

    iget-object v1, p0, Ldpj;->a:Landroid/content/Context;

    const-string v2, "ModifyCircleMembershipsTask"

    iget v3, p0, Ldpj;->b:I

    iget-object v4, p0, Ldpj;->c:Ljava/lang/String;

    iget-object v5, p0, Ldpj;->d:Ljava/lang/String;

    iget v6, p0, Ldpj;->e:I

    iget-object v7, p0, Ldpj;->f:Ljava/util/ArrayList;

    iget-object v8, p0, Ldpj;->g:Ljava/util/ArrayList;

    iget-boolean v9, p0, Ldpj;->h:Z

    iget-boolean v10, p0, Ldpj;->i:Z

    iget-boolean v11, p0, Ldpj;->j:Z

    iget-object v12, p0, Ldpj;->k:Ljava/lang/String;

    iget-object v13, p0, Ldpj;->l:Ljava/lang/String;

    iget-object v14, p0, Ldpj;->m:Ljava/lang/String;

    invoke-direct/range {v0 .. v14}, Ldpi;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(I)Ldpj;
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Ldpj;->b:I

    .line 66
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ldpj;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Ldpj;->c:Ljava/lang/String;

    .line 71
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Ldpj;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldpj;"
        }
    .end annotation

    .prologue
    .line 85
    iput-object p1, p0, Ldpj;->f:Ljava/util/ArrayList;

    .line 86
    return-object p0
.end method

.method public a(Z)Ldpj;
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Ldpj;->h:Z

    .line 96
    return-object p0
.end method

.method public b(I)Ldpj;
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Ldpj;->e:I

    .line 81
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ldpj;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Ldpj;->d:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public b(Ljava/util/ArrayList;)Ldpj;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldpj;"
        }
    .end annotation

    .prologue
    .line 90
    iput-object p1, p0, Ldpj;->g:Ljava/util/ArrayList;

    .line 91
    return-object p0
.end method

.method public b(Z)Ldpj;
    .locals 0

    .prologue
    .line 100
    iput-boolean p1, p0, Ldpj;->i:Z

    .line 101
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ldpj;
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Ldpj;->k:Ljava/lang/String;

    .line 111
    return-object p0
.end method

.method public c(Z)Ldpj;
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Ldpj;->j:Z

    .line 106
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ldpj;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Ldpj;->l:Ljava/lang/String;

    .line 116
    return-object p0
.end method

.method public e(Ljava/lang/String;)Ldpj;
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Ldpj;->m:Ljava/lang/String;

    .line 121
    return-object p0
.end method
