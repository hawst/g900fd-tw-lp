.class public Lfbs;
.super Lhya;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final e:I

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lhym;

.field private l:Lhym;

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Landroid/view/View$OnClickListener;

.field private q:Landroid/view/View$OnLongClickListener;

.field private final r:Lctz;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-direct {p0, p1, v1}, Lhya;-><init>(Landroid/content/Context;B)V

    .line 67
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lfbs;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    move v0, v1

    .line 92
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 93
    invoke-virtual {p0, v1, v1}, Lfbs;->b(ZZ)V

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_0
    const-class v0, Lctz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lfbs;->r:Lctz;

    .line 96
    iput p2, p0, Lfbs;->e:I

    .line 97
    new-instance v0, Ljvl;

    invoke-direct {v0, p1}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v0, v0, Ljvl;->a:I

    iput v0, p0, Lfbs;->m:I

    .line 98
    const v0, 0x7f0a08ad

    iput v0, p0, Lfbs;->a:I

    .line 99
    const v0, 0x7f0a08ae

    iput v0, p0, Lfbs;->b:I

    .line 100
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lfbs;->c:Landroid/view/LayoutInflater;

    .line 101
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;
    .locals 8

    .prologue
    .line 405
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 406
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 407
    const/4 v3, 0x7

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 408
    invoke-static {v0, v1}, Ljvj;->a(J)Ljac;

    move-result-object v4

    .line 411
    const-wide/32 v6, 0x40000

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 412
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 413
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p0, v1, v4, v0}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    .line 418
    :goto_0
    return-object v0

    .line 415
    :cond_0
    invoke-static {p0, v2, v3, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 18

    .prologue
    .line 336
    move-object/from16 v10, p1

    check-cast v10, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 338
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lfbs;->a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;

    move-result-object v5

    .line 339
    invoke-virtual {v10, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    .line 342
    const/16 v2, 0xa

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 344
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lfbs;->o:Z

    if-nez v3, :cond_2

    if-lez v2, :cond_2

    .line 345
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Ljava/lang/Integer;)V

    .line 351
    :goto_1
    const/16 v2, 0xb

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    .line 353
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lfbs;->o:Z

    if-eqz v3, :cond_4

    .line 354
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljava/lang/Integer;)V

    .line 359
    :goto_3
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 360
    const/4 v2, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 363
    if-eqz v2, :cond_5

    invoke-static {v2}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 364
    invoke-static {}, Ljvj;->b()Ljava/lang/String;

    move-result-object v3

    .line 369
    :goto_4
    const/16 v2, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 370
    const/16 v2, 0xe

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 371
    const-wide v14, 0x220000000L

    and-long/2addr v14, v8

    const-wide/16 v16, 0x0

    cmp-long v2, v14, v16

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    move v11, v2

    .line 374
    :goto_5
    const/4 v2, 0x0

    .line 375
    move-object/from16 v0, p0

    iget-object v4, v0, Lfbs;->r:Lctz;

    invoke-virtual {v4}, Lctz;->a()Ljcn;

    move-result-object v4

    .line 377
    if-eqz v4, :cond_0

    .line 378
    new-instance v2, Ljug;

    invoke-direct {v2, v3}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v13, Ljue;

    invoke-direct {v13, v5}, Ljue;-><init>(Lizu;)V

    .line 379
    invoke-virtual {v4, v2, v13}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v2

    check-cast v2, Ljuc;

    .line 381
    :cond_0
    if-nez v2, :cond_8

    .line 382
    new-instance v2, Ljuc;

    move-object v4, v3

    invoke-direct/range {v2 .. v9}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    move-object v3, v2

    .line 384
    :goto_6
    const-wide/16 v4, 0x100

    and-long/2addr v4, v6

    const-wide/16 v8, 0x0

    cmp-long v2, v4, v8

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    .line 386
    :goto_7
    invoke-virtual {v10, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljcl;)V

    .line 387
    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/views/PhotoTileView;->n(Z)V

    .line 388
    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->d(Z)V

    .line 390
    const v2, 0x7f100079

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 391
    const v2, 0x7f10007a

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 392
    const v2, 0x7f100093

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 393
    const v2, 0x7f10007f

    const/4 v3, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 394
    move-object/from16 v0, p0

    iget-object v2, v0, Lfbs;->p:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v2, v0, Lfbs;->q:Landroid/view/View$OnLongClickListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 396
    return-void

    .line 342
    :cond_1
    const/16 v2, 0xa

    .line 343
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    goto/16 :goto_0

    .line 347
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 351
    :cond_3
    const/16 v2, 0xb

    .line 352
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_2

    .line 356
    :cond_4
    invoke-virtual {v10, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljava/lang/Integer;)V

    goto/16 :goto_3

    .line 366
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lfbs;->i:Ljava/lang/String;

    goto/16 :goto_4

    .line 371
    :cond_6
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_5

    .line 384
    :cond_7
    const/4 v2, 0x0

    goto :goto_7

    :cond_8
    move-object v3, v2

    goto/16 :goto_6
.end method

.method static synthetic a(Lfbs;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lfbs;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 169
    packed-switch p1, :pswitch_data_0

    .line 192
    :cond_0
    :goto_0
    return v0

    .line 171
    :pswitch_0
    iget-object v1, p0, Lfbs;->j:Lhym;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lfbs;->j:Lhym;

    invoke-virtual {v0}, Lhym;->getCount()I

    move-result v0

    goto :goto_0

    .line 175
    :pswitch_1
    invoke-super {p0, p1}, Lhya;->a(I)I

    move-result v0

    goto :goto_0

    .line 179
    :pswitch_2
    iget-object v1, p0, Lfbs;->l:Lhym;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lfbs;->l:Lhym;

    invoke-virtual {v0}, Lhym;->getCount()I

    move-result v0

    goto :goto_0

    .line 183
    :pswitch_3
    invoke-virtual {p0, p1}, Lfbs;->l(I)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 187
    invoke-super {p0, p1}, Lhya;->a(I)I

    move-result v0

    .line 188
    iget-object v1, p0, Lfbs;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(II)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 257
    packed-switch p1, :pswitch_data_0

    .line 278
    :cond_0
    :goto_0
    return v0

    .line 259
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 263
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 267
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 271
    :pswitch_3
    invoke-super {p0}, Lhya;->getCount()I

    move-result v1

    if-eq p2, v1, :cond_0

    .line 274
    const/4 v0, 0x3

    goto :goto_0

    .line 257
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 200
    packed-switch p2, :pswitch_data_0

    .line 222
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 203
    :pswitch_0
    iget-object v0, p0, Lfbs;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f040075

    .line 204
    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 205
    const v1, 0x7f1001c9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 206
    const v1, 0x7f100237

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 207
    new-instance v1, Llka;

    const/4 v2, 0x2

    const/4 v3, -0x2

    iget v4, p0, Lfbs;->m:I

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Llka;-><init>(IIII)V

    .line 209
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 214
    :pswitch_1
    invoke-virtual {p0, p1, p5}, Lfbs;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 218
    :pswitch_2
    invoke-super {p0}, Lhya;->getCount()I

    move-result v0

    if-ne p4, v0, :cond_0

    iget-object v0, p0, Lfbs;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0400f8

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lfbs;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f04018e

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 292
    iget-object v0, p0, Lfbs;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f04018e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 134
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lfbs;->a(ILandroid/database/Cursor;)V

    .line 135
    iget-boolean v0, p0, Lfbs;->n:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfbs;->j:Lhym;

    :cond_1
    :goto_0
    iget-object v0, p0, Lfbs;->j:Lhym;

    invoke-virtual {p0, v2, v0}, Lfbs;->a(ILandroid/database/Cursor;)V

    .line 136
    return-void

    .line 135
    :cond_2
    iget-object v0, p0, Lfbs;->j:Lhym;

    if-nez v0, :cond_1

    new-instance v0, Lhym;

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lfbs;->j:Lhym;

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lfbs;->j:Lhym;

    invoke-virtual {v1, v0}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected a(Landroid/database/Cursor;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0, p2, p1}, Lfbs;->a(Landroid/view/View;Landroid/database/Cursor;)V

    .line 297
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lfbs;->p:Landroid/view/View$OnClickListener;

    .line 122
    return-void
.end method

.method public a(Landroid/view/View$OnLongClickListener;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lfbs;->q:Landroid/view/View$OnLongClickListener;

    .line 126
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 9

    .prologue
    const/4 v1, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 229
    packed-switch p2, :pswitch_data_0

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 232
    :pswitch_0
    const v0, 0x7f1001c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez p2, :cond_1

    iget v1, p0, Lfbs;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    iget v1, p0, Lfbs;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 237
    :pswitch_1
    invoke-virtual {p0, p3, p1}, Lfbs;->a(Landroid/database/Cursor;Landroid/view/View;)V

    goto :goto_0

    .line 242
    :pswitch_2
    invoke-super {p0, v1}, Lhya;->a(I)I

    move-result v0

    if-ge p4, v0, :cond_0

    .line 243
    iget-object v0, p0, Lfbs;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-super {p0, v1}, Lhya;->a(I)I

    move-result v0

    sub-int/2addr v0, p4

    const/16 v1, 0x64

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lfbs;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v7, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lfbt;

    iget-object v1, p0, Lfbs;->k:Landroid/content/Context;

    iget v2, p0, Lfbs;->e:I

    iget-object v4, p0, Lfbs;->h:Ljava/lang/String;

    iget v5, p0, Lfbs;->g:I

    iget-object v6, p0, Lfbs;->i:Ljava/lang/String;

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lfbt;-><init>(Landroid/content/Context;ILfbs;Ljava/lang/String;ILjava/lang/String;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_3

    new-array v1, v8, [Ljava/lang/String;

    iget-object v2, p0, Lfbs;->f:Ljava/lang/String;

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    :goto_1
    invoke-direct {p0, p1, p3}, Lfbs;->a(Landroid/view/View;Landroid/database/Cursor;)V

    goto :goto_0

    :cond_3
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lfbs;->f:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 229
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lfbs;->h:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 112
    iput p2, p0, Lfbs;->g:I

    .line 113
    iput-object p1, p0, Lfbs;->i:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 104
    iput-boolean p1, p0, Lfbs;->n:Z

    .line 105
    return-void
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lfbs;->m:I

    return v0
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 150
    if-nez p1, :cond_2

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 151
    :goto_0
    const-string v1, "resume_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbs;->f:Ljava/lang/String;

    .line 152
    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Lfbs;->a(ILandroid/database/Cursor;)V

    .line 153
    iget-boolean v0, p0, Lfbs;->n:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfbs;->l:Lhym;

    :cond_1
    :goto_1
    const/4 v0, 0x2

    iget-object v1, p0, Lfbs;->l:Lhym;

    invoke-virtual {p0, v0, v1}, Lfbs;->a(ILandroid/database/Cursor;)V

    .line 154
    return-void

    .line 150
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 153
    :cond_3
    iget-object v0, p0, Lfbs;->l:Lhym;

    if-nez v0, :cond_1

    new-instance v0, Lhym;

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lfbs;->l:Lhym;

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lfbs;->l:Lhym;

    invoke-virtual {v1, v0}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lfbs;->o:Z

    .line 109
    return-void
.end method

.method protected c()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lfbs;->p:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x4

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method
