.class public final Loqv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Loqu;

.field private b:Ljava/lang/Boolean;

.field private c:Loqq;

.field private d:Loqp;

.field private e:Lorm;

.field private f:Losb;

.field private g:Lops;

.field private h:Loru;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    iput-object v0, p0, Loqv;->c:Loqq;

    .line 18
    iput-object v0, p0, Loqv;->a:Loqu;

    .line 21
    iput-object v0, p0, Loqv;->d:Loqp;

    .line 24
    iput-object v0, p0, Loqv;->e:Lorm;

    .line 27
    iput-object v0, p0, Loqv;->f:Losb;

    .line 30
    iput-object v0, p0, Loqv;->g:Lops;

    .line 33
    iput-object v0, p0, Loqv;->h:Loru;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 69
    iget-object v1, p0, Loqv;->c:Loqq;

    if-eqz v1, :cond_0

    .line 70
    const/4 v0, 0x1

    iget-object v1, p0, Loqv;->c:Loqq;

    .line 71
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 73
    :cond_0
    iget-object v1, p0, Loqv;->a:Loqu;

    if-eqz v1, :cond_1

    .line 74
    const/4 v1, 0x2

    iget-object v2, p0, Loqv;->a:Loqu;

    .line 75
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_1
    iget-object v1, p0, Loqv;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 78
    const/4 v1, 0x3

    iget-object v2, p0, Loqv;->b:Ljava/lang/Boolean;

    .line 79
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 81
    :cond_2
    iget-object v1, p0, Loqv;->d:Loqp;

    if-eqz v1, :cond_3

    .line 82
    const/4 v1, 0x4

    iget-object v2, p0, Loqv;->d:Loqp;

    .line 83
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_3
    iget-object v1, p0, Loqv;->e:Lorm;

    if-eqz v1, :cond_4

    .line 86
    const/4 v1, 0x5

    iget-object v2, p0, Loqv;->e:Lorm;

    .line 87
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_4
    iget-object v1, p0, Loqv;->f:Losb;

    if-eqz v1, :cond_5

    .line 90
    const/4 v1, 0x6

    iget-object v2, p0, Loqv;->f:Losb;

    .line 91
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_5
    iget-object v1, p0, Loqv;->g:Lops;

    if-eqz v1, :cond_6

    .line 94
    const/4 v1, 0x7

    iget-object v2, p0, Loqv;->g:Lops;

    .line 95
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_6
    iget-object v1, p0, Loqv;->h:Loru;

    if-eqz v1, :cond_7

    .line 98
    const/16 v1, 0x8

    iget-object v2, p0, Loqv;->h:Loru;

    .line 99
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_7
    iget-object v1, p0, Loqv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    iput v0, p0, Loqv;->ai:I

    .line 103
    return v0
.end method

.method public a(Loxn;)Loqv;
    .locals 2

    .prologue
    .line 111
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 112
    sparse-switch v0, :sswitch_data_0

    .line 116
    iget-object v1, p0, Loqv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 117
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loqv;->ah:Ljava/util/List;

    .line 120
    :cond_1
    iget-object v1, p0, Loqv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    :sswitch_0
    return-object p0

    .line 127
    :sswitch_1
    iget-object v0, p0, Loqv;->c:Loqq;

    if-nez v0, :cond_2

    .line 128
    new-instance v0, Loqq;

    invoke-direct {v0}, Loqq;-><init>()V

    iput-object v0, p0, Loqv;->c:Loqq;

    .line 130
    :cond_2
    iget-object v0, p0, Loqv;->c:Loqq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 134
    :sswitch_2
    iget-object v0, p0, Loqv;->a:Loqu;

    if-nez v0, :cond_3

    .line 135
    new-instance v0, Loqu;

    invoke-direct {v0}, Loqu;-><init>()V

    iput-object v0, p0, Loqv;->a:Loqu;

    .line 137
    :cond_3
    iget-object v0, p0, Loqv;->a:Loqu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 141
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loqv;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 145
    :sswitch_4
    iget-object v0, p0, Loqv;->d:Loqp;

    if-nez v0, :cond_4

    .line 146
    new-instance v0, Loqp;

    invoke-direct {v0}, Loqp;-><init>()V

    iput-object v0, p0, Loqv;->d:Loqp;

    .line 148
    :cond_4
    iget-object v0, p0, Loqv;->d:Loqp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 152
    :sswitch_5
    iget-object v0, p0, Loqv;->e:Lorm;

    if-nez v0, :cond_5

    .line 153
    new-instance v0, Lorm;

    invoke-direct {v0}, Lorm;-><init>()V

    iput-object v0, p0, Loqv;->e:Lorm;

    .line 155
    :cond_5
    iget-object v0, p0, Loqv;->e:Lorm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 159
    :sswitch_6
    iget-object v0, p0, Loqv;->f:Losb;

    if-nez v0, :cond_6

    .line 160
    new-instance v0, Losb;

    invoke-direct {v0}, Losb;-><init>()V

    iput-object v0, p0, Loqv;->f:Losb;

    .line 162
    :cond_6
    iget-object v0, p0, Loqv;->f:Losb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 166
    :sswitch_7
    iget-object v0, p0, Loqv;->g:Lops;

    if-nez v0, :cond_7

    .line 167
    new-instance v0, Lops;

    invoke-direct {v0}, Lops;-><init>()V

    iput-object v0, p0, Loqv;->g:Lops;

    .line 169
    :cond_7
    iget-object v0, p0, Loqv;->g:Lops;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 173
    :sswitch_8
    iget-object v0, p0, Loqv;->h:Loru;

    if-nez v0, :cond_8

    .line 174
    new-instance v0, Loru;

    invoke-direct {v0}, Loru;-><init>()V

    iput-object v0, p0, Loqv;->h:Loru;

    .line 176
    :cond_8
    iget-object v0, p0, Loqv;->h:Loru;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 112
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Loqv;->c:Loqq;

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Loqv;->c:Loqq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 41
    :cond_0
    iget-object v0, p0, Loqv;->a:Loqu;

    if-eqz v0, :cond_1

    .line 42
    const/4 v0, 0x2

    iget-object v1, p0, Loqv;->a:Loqu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 44
    :cond_1
    iget-object v0, p0, Loqv;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 45
    const/4 v0, 0x3

    iget-object v1, p0, Loqv;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 47
    :cond_2
    iget-object v0, p0, Loqv;->d:Loqp;

    if-eqz v0, :cond_3

    .line 48
    const/4 v0, 0x4

    iget-object v1, p0, Loqv;->d:Loqp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 50
    :cond_3
    iget-object v0, p0, Loqv;->e:Lorm;

    if-eqz v0, :cond_4

    .line 51
    const/4 v0, 0x5

    iget-object v1, p0, Loqv;->e:Lorm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 53
    :cond_4
    iget-object v0, p0, Loqv;->f:Losb;

    if-eqz v0, :cond_5

    .line 54
    const/4 v0, 0x6

    iget-object v1, p0, Loqv;->f:Losb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 56
    :cond_5
    iget-object v0, p0, Loqv;->g:Lops;

    if-eqz v0, :cond_6

    .line 57
    const/4 v0, 0x7

    iget-object v1, p0, Loqv;->g:Lops;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 59
    :cond_6
    iget-object v0, p0, Loqv;->h:Loru;

    if-eqz v0, :cond_7

    .line 60
    const/16 v0, 0x8

    iget-object v1, p0, Loqv;->h:Loru;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_7
    iget-object v0, p0, Loqv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 64
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loqv;->a(Loxn;)Loqv;

    move-result-object v0

    return-object v0
.end method
