.class public final Lore;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lore;


# instance fields
.field private b:[Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 953
    const/4 v0, 0x0

    new-array v0, v0, [Lore;

    sput-object v0, Lore;->a:[Lore;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 954
    invoke-direct {p0}, Loxq;-><init>()V

    .line 957
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lore;->b:[Ljava/lang/String;

    .line 954
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 978
    .line 979
    iget-object v1, p0, Lore;->b:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lore;->b:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 981
    iget-object v2, p0, Lore;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 983
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 981
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 986
    :cond_0
    iget-object v0, p0, Lore;->b:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 988
    :cond_1
    iget-object v1, p0, Lore;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 989
    const/4 v1, 0x2

    iget-object v2, p0, Lore;->c:Ljava/lang/String;

    .line 990
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 992
    :cond_2
    iget-object v1, p0, Lore;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 993
    iput v0, p0, Lore;->ai:I

    .line 994
    return v0
.end method

.method public a(Loxn;)Lore;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1002
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1003
    sparse-switch v0, :sswitch_data_0

    .line 1007
    iget-object v1, p0, Lore;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1008
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lore;->ah:Ljava/util/List;

    .line 1011
    :cond_1
    iget-object v1, p0, Lore;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1013
    :sswitch_0
    return-object p0

    .line 1018
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1019
    iget-object v0, p0, Lore;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 1020
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1021
    iget-object v2, p0, Lore;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1022
    iput-object v1, p0, Lore;->b:[Ljava/lang/String;

    .line 1023
    :goto_1
    iget-object v1, p0, Lore;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1024
    iget-object v1, p0, Lore;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1025
    invoke-virtual {p1}, Loxn;->a()I

    .line 1023
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1028
    :cond_2
    iget-object v1, p0, Lore;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1032
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lore;->c:Ljava/lang/String;

    goto :goto_0

    .line 1003
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 964
    iget-object v0, p0, Lore;->b:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 965
    iget-object v1, p0, Lore;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 966
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 965
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 969
    :cond_0
    iget-object v0, p0, Lore;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 970
    const/4 v0, 0x2

    iget-object v1, p0, Lore;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 972
    :cond_1
    iget-object v0, p0, Lore;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 974
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 950
    invoke-virtual {p0, p1}, Lore;->a(Loxn;)Lore;

    move-result-object v0

    return-object v0
.end method
