.class public final Lnbl;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lnbk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1299
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1302
    sget-object v0, Lnbk;->a:[Lnbk;

    iput-object v0, p0, Lnbl;->a:[Lnbk;

    .line 1299
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1320
    .line 1321
    iget-object v1, p0, Lnbl;->a:[Lnbk;

    if-eqz v1, :cond_1

    .line 1322
    iget-object v2, p0, Lnbl;->a:[Lnbk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1323
    if-eqz v4, :cond_0

    .line 1324
    const/4 v5, 0x1

    .line 1325
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1322
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1329
    :cond_1
    iget-object v1, p0, Lnbl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1330
    iput v0, p0, Lnbl;->ai:I

    .line 1331
    return v0
.end method

.method public a(Loxn;)Lnbl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1339
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1340
    sparse-switch v0, :sswitch_data_0

    .line 1344
    iget-object v2, p0, Lnbl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1345
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnbl;->ah:Ljava/util/List;

    .line 1348
    :cond_1
    iget-object v2, p0, Lnbl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1350
    :sswitch_0
    return-object p0

    .line 1355
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1356
    iget-object v0, p0, Lnbl;->a:[Lnbk;

    if-nez v0, :cond_3

    move v0, v1

    .line 1357
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnbk;

    .line 1358
    iget-object v3, p0, Lnbl;->a:[Lnbk;

    if-eqz v3, :cond_2

    .line 1359
    iget-object v3, p0, Lnbl;->a:[Lnbk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1361
    :cond_2
    iput-object v2, p0, Lnbl;->a:[Lnbk;

    .line 1362
    :goto_2
    iget-object v2, p0, Lnbl;->a:[Lnbk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1363
    iget-object v2, p0, Lnbl;->a:[Lnbk;

    new-instance v3, Lnbk;

    invoke-direct {v3}, Lnbk;-><init>()V

    aput-object v3, v2, v0

    .line 1364
    iget-object v2, p0, Lnbl;->a:[Lnbk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1365
    invoke-virtual {p1}, Loxn;->a()I

    .line 1362
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1356
    :cond_3
    iget-object v0, p0, Lnbl;->a:[Lnbk;

    array-length v0, v0

    goto :goto_1

    .line 1368
    :cond_4
    iget-object v2, p0, Lnbl;->a:[Lnbk;

    new-instance v3, Lnbk;

    invoke-direct {v3}, Lnbk;-><init>()V

    aput-object v3, v2, v0

    .line 1369
    iget-object v2, p0, Lnbl;->a:[Lnbk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1340
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1307
    iget-object v0, p0, Lnbl;->a:[Lnbk;

    if-eqz v0, :cond_1

    .line 1308
    iget-object v1, p0, Lnbl;->a:[Lnbk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1309
    if-eqz v3, :cond_0

    .line 1310
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1308
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1314
    :cond_1
    iget-object v0, p0, Lnbl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1316
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1295
    invoke-virtual {p0, p1}, Lnbl;->a(Loxn;)Lnbl;

    move-result-object v0

    return-object v0
.end method
