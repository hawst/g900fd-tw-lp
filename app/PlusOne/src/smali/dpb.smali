.class public final Ldpb;
.super Lhny;
.source "PG"


# static fields
.field private static final a:Landroid/content/Intent;

.field private static i:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:I

.field private final c:Lkfd;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 36
    sput-object v0, Ldpb;->a:Landroid/content/Intent;

    const-string v1, "http://www.com"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 50
    const-string v0, "GetRedirectUrlTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 51
    iput-object p3, p0, Ldpb;->d:Ljava/lang/String;

    .line 52
    iput p2, p0, Ldpb;->b:I

    .line 53
    iput-object p4, p0, Ldpb;->f:Ljava/lang/String;

    .line 54
    iput-object p5, p0, Ldpb;->e:Ljava/lang/String;

    .line 55
    iput-object p6, p0, Ldpb;->h:Ljava/lang/String;

    .line 56
    sget-object v0, Ldpb;->i:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Ldpb;->i:Ljava/util/HashSet;

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sget-object v1, Ldpb;->a:Landroid/content/Intent;

    const/high16 v2, 0x10000

    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 63
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 64
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 65
    sget-object v4, Ldpb;->i:Ljava/util/HashSet;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 68
    :cond_0
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Ldpb;->c:Lkfd;

    .line 69
    return-void
.end method

.method public static a(Lhoz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "activity_id"

    invoke-static {p0, v0}, Ldpb;->a(Lhoz;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lhoz;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 89
    :cond_0
    const/4 v0, 0x0

    .line 92
    :goto_0
    return-object v0

    .line 91
    :cond_1
    invoke-virtual {p0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lhoz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string v0, "author_gaia_id"

    invoke-static {p0, v0}, Ldpb;->a(Lhoz;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lhoz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string v0, "creation_source_id"

    invoke-static {p0, v0}, Ldpb;->a(Lhoz;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lhoz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const-string v0, "redirect_url"

    invoke-static {p0, v0}, Ldpb;->a(Lhoz;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 102
    new-instance v1, Lhoz;

    invoke-direct {v1, v3}, Lhoz;-><init>(Z)V

    .line 103
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v5

    .line 104
    const-string v0, "activity_id"

    iget-object v4, p0, Ldpb;->d:Ljava/lang/String;

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v0, "creation_source_id"

    iget-object v4, p0, Ldpb;->f:Ljava/lang/String;

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v0, "author_gaia_id"

    iget-object v4, p0, Ldpb;->e:Ljava/lang/String;

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Ldpb;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v4, Lexl;

    invoke-direct {v4, v0}, Lexl;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v4}, Lexl;->a()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 109
    const-string v0, "redirect_url"

    iget-object v2, p0, Ldpb;->h:Ljava/lang/String;

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 126
    :goto_1
    return-object v0

    .line 108
    :sswitch_0
    new-instance v4, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0}, Ldpb;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v6, 0x10000

    invoke-virtual {v0, v4, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v4, v2

    :goto_2
    if-ge v4, v7, :cond_1

    sget-object v8, Ldpb;->i:Ljava/util/HashSet;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_0

    .line 113
    :cond_3
    new-instance v0, Ldkm;

    invoke-virtual {p0}, Ldpb;->f()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Ldpb;->b:I

    iget-object v4, p0, Ldpb;->h:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v4}, Ldkm;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    iget-object v2, p0, Ldpb;->c:Lkfd;

    invoke-interface {v2, v0}, Lkfd;->a(Lkff;)V

    invoke-virtual {v0}, Ldkm;->t()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "GetRedirectUrlTask"

    invoke-virtual {v0, v2}, Ldkm;->d(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 120
    :cond_4
    :goto_3
    if-eqz v0, :cond_6

    .line 121
    const-string v2, "redirect_url"

    invoke-virtual {v5, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 122
    goto :goto_1

    .line 113
    :cond_5
    invoke-virtual {v0}, Ldkm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Ldpb;->h:Ljava/lang/String;

    goto :goto_3

    .line 125
    :cond_6
    const-string v0, "redirect_url"

    iget-object v2, p0, Ldpb;->h:Ljava/lang/String;

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 126
    goto :goto_1

    .line 108
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f -> :sswitch_0
    .end sparse-switch
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Ldpb;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0a02

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
