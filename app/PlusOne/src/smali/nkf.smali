.class public final Lnkf;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:[Lnjh;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6045
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6053
    const/high16 v0, -0x80000000

    iput v0, p0, Lnkf;->a:I

    .line 6056
    sget-object v0, Lnjh;->a:[Lnjh;

    iput-object v0, p0, Lnkf;->b:[Lnjh;

    .line 6045
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 6087
    .line 6088
    iget v0, p0, Lnkf;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_4

    .line 6089
    const/4 v0, 0x1

    iget v2, p0, Lnkf;->a:I

    .line 6090
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6092
    :goto_0
    iget-object v2, p0, Lnkf;->b:[Lnjh;

    if-eqz v2, :cond_1

    .line 6093
    iget-object v2, p0, Lnkf;->b:[Lnjh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6094
    if-eqz v4, :cond_0

    .line 6095
    const/4 v5, 0x2

    .line 6096
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6093
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6100
    :cond_1
    iget-object v1, p0, Lnkf;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 6101
    const/4 v1, 0x3

    iget-object v2, p0, Lnkf;->c:Ljava/lang/Boolean;

    .line 6102
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6104
    :cond_2
    iget-object v1, p0, Lnkf;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 6105
    const/4 v1, 0x4

    iget-object v2, p0, Lnkf;->d:Ljava/lang/Boolean;

    .line 6106
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6108
    :cond_3
    iget-object v1, p0, Lnkf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6109
    iput v0, p0, Lnkf;->ai:I

    .line 6110
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnkf;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 6118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6119
    sparse-switch v0, :sswitch_data_0

    .line 6123
    iget-object v2, p0, Lnkf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 6124
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnkf;->ah:Ljava/util/List;

    .line 6127
    :cond_1
    iget-object v2, p0, Lnkf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6129
    :sswitch_0
    return-object p0

    .line 6134
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6135
    if-eq v0, v4, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 6137
    :cond_2
    iput v0, p0, Lnkf;->a:I

    goto :goto_0

    .line 6139
    :cond_3
    iput v4, p0, Lnkf;->a:I

    goto :goto_0

    .line 6144
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6145
    iget-object v0, p0, Lnkf;->b:[Lnjh;

    if-nez v0, :cond_5

    move v0, v1

    .line 6146
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnjh;

    .line 6147
    iget-object v3, p0, Lnkf;->b:[Lnjh;

    if-eqz v3, :cond_4

    .line 6148
    iget-object v3, p0, Lnkf;->b:[Lnjh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6150
    :cond_4
    iput-object v2, p0, Lnkf;->b:[Lnjh;

    .line 6151
    :goto_2
    iget-object v2, p0, Lnkf;->b:[Lnjh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 6152
    iget-object v2, p0, Lnkf;->b:[Lnjh;

    new-instance v3, Lnjh;

    invoke-direct {v3}, Lnjh;-><init>()V

    aput-object v3, v2, v0

    .line 6153
    iget-object v2, p0, Lnkf;->b:[Lnjh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6154
    invoke-virtual {p1}, Loxn;->a()I

    .line 6151
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6145
    :cond_5
    iget-object v0, p0, Lnkf;->b:[Lnjh;

    array-length v0, v0

    goto :goto_1

    .line 6157
    :cond_6
    iget-object v2, p0, Lnkf;->b:[Lnjh;

    new-instance v3, Lnjh;

    invoke-direct {v3}, Lnjh;-><init>()V

    aput-object v3, v2, v0

    .line 6158
    iget-object v2, p0, Lnkf;->b:[Lnjh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6162
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnkf;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 6166
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnkf;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 6119
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 6065
    iget v0, p0, Lnkf;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 6066
    const/4 v0, 0x1

    iget v1, p0, Lnkf;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6068
    :cond_0
    iget-object v0, p0, Lnkf;->b:[Lnjh;

    if-eqz v0, :cond_2

    .line 6069
    iget-object v1, p0, Lnkf;->b:[Lnjh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 6070
    if-eqz v3, :cond_1

    .line 6071
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 6069
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6075
    :cond_2
    iget-object v0, p0, Lnkf;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 6076
    const/4 v0, 0x3

    iget-object v1, p0, Lnkf;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6078
    :cond_3
    iget-object v0, p0, Lnkf;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 6079
    const/4 v0, 0x4

    iget-object v1, p0, Lnkf;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6081
    :cond_4
    iget-object v0, p0, Lnkf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6083
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6041
    invoke-virtual {p0, p1}, Lnkf;->a(Loxn;)Lnkf;

    move-result-object v0

    return-object v0
.end method
