.class public Ldwu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljcl;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ldwu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljcj;

.field private final c:Ljcm;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Ldwv;

    invoke-direct {v0}, Ldwv;-><init>()V

    sput-object v0, Ldwu;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldwu;->a:Ljava/lang/String;

    .line 29
    new-instance v0, Ldww;

    invoke-direct {v0}, Ldww;-><init>()V

    iput-object v0, p0, Ldwu;->b:Ljcj;

    .line 30
    new-instance v0, Ldwx;

    iget-object v1, p0, Ldwu;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ldwx;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldwu;->c:Ljcm;

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Ldwu;->a:Ljava/lang/String;

    .line 23
    new-instance v0, Ldww;

    invoke-direct {v0}, Ldww;-><init>()V

    iput-object v0, p0, Ldwu;->b:Ljcj;

    .line 24
    new-instance v0, Ldwx;

    iget-object v1, p0, Ldwu;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ldwx;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldwu;->c:Ljcm;

    .line 25
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Ldwu;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Ldwu;->d:I

    .line 51
    return-void
.end method

.method public b()Ljcj;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Ldwu;->b:Ljcj;

    return-object v0
.end method

.method public c()Ljcm;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ldwu;->c:Ljcm;

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 45
    const-wide/16 v0, 0x100

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Ldwu;->d:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Ldwu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    return-void
.end method
