.class public final Llzj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnvx;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6478
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6481
    iput-object v0, p0, Llzj;->apiHeader:Llyr;

    .line 6484
    iput-object v0, p0, Llzj;->a:Lnvx;

    .line 6478
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6501
    const/4 v0, 0x0

    .line 6502
    iget-object v1, p0, Llzj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 6503
    const/4 v0, 0x1

    iget-object v1, p0, Llzj;->apiHeader:Llyr;

    .line 6504
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6506
    :cond_0
    iget-object v1, p0, Llzj;->a:Lnvx;

    if-eqz v1, :cond_1

    .line 6507
    const/4 v1, 0x2

    iget-object v2, p0, Llzj;->a:Lnvx;

    .line 6508
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6510
    :cond_1
    iget-object v1, p0, Llzj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6511
    iput v0, p0, Llzj;->ai:I

    .line 6512
    return v0
.end method

.method public a(Loxn;)Llzj;
    .locals 2

    .prologue
    .line 6520
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6521
    sparse-switch v0, :sswitch_data_0

    .line 6525
    iget-object v1, p0, Llzj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6526
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzj;->ah:Ljava/util/List;

    .line 6529
    :cond_1
    iget-object v1, p0, Llzj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6531
    :sswitch_0
    return-object p0

    .line 6536
    :sswitch_1
    iget-object v0, p0, Llzj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 6537
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzj;->apiHeader:Llyr;

    .line 6539
    :cond_2
    iget-object v0, p0, Llzj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6543
    :sswitch_2
    iget-object v0, p0, Llzj;->a:Lnvx;

    if-nez v0, :cond_3

    .line 6544
    new-instance v0, Lnvx;

    invoke-direct {v0}, Lnvx;-><init>()V

    iput-object v0, p0, Llzj;->a:Lnvx;

    .line 6546
    :cond_3
    iget-object v0, p0, Llzj;->a:Lnvx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6521
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6489
    iget-object v0, p0, Llzj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 6490
    const/4 v0, 0x1

    iget-object v1, p0, Llzj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6492
    :cond_0
    iget-object v0, p0, Llzj;->a:Lnvx;

    if-eqz v0, :cond_1

    .line 6493
    const/4 v0, 0x2

    iget-object v1, p0, Llzj;->a:Lnvx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6495
    :cond_1
    iget-object v0, p0, Llzj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6497
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6474
    invoke-virtual {p0, p1}, Llzj;->a(Loxn;)Llzj;

    move-result-object v0

    return-object v0
.end method
