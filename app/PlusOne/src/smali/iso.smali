.class public final Liso;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Liso;


# instance fields
.field private c:Landroid/mtp/MtpDevice;

.field private d:J

.field private e:Lisp;

.field private volatile f:List;

.field private final g:Lisr;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 78
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x3808

    .line 79
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/16 v2, 0x3801

    .line 80
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x380b

    .line 81
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x3807

    .line 82
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    const/16 v2, 0x3804

    .line 83
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    .line 79
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 78
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Liso;->a:Ljava/util/Set;

    .line 86
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/Integer;

    const v2, 0xb984

    .line 88
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/16 v2, 0x300a

    .line 89
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0xb982

    .line 90
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x300b

    .line 91
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    .line 87
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 86
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Liso;->b:Ljava/util/Set;

    .line 99
    new-instance v0, Liso;

    .line 100
    invoke-static {}, Lisq;->a()Lisr;

    move-result-object v1

    invoke-direct {v0, v1}, Liso;-><init>(Lisr;)V

    sput-object v0, Liso;->h:Liso;

    .line 99
    return-void
.end method

.method protected constructor <init>(Lisr;)V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Liso;->g:Lisr;

    .line 108
    return-void
.end method

.method public static a()Liso;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Liso;->h:Liso;

    return-object v0
.end method


# virtual methods
.method public a(II)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, Liso;->f:List;

    .line 198
    if-nez v0, :cond_0

    .line 199
    const/4 v0, 0x0

    .line 215
    :goto_0
    return-object v0

    .line 201
    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    .line 202
    iget-object v1, v0, List;->c:[Lisg;

    iget-object v2, v0, List;->a:[I

    aget v2, v2, p1

    aget-object v1, v1, v2

    .line 203
    iget v2, v1, Lisg;->b:I

    if-ne v2, p1, :cond_1

    .line 204
    iget-object v0, v1, Lisg;->a:Lisu;

    goto :goto_0

    .line 206
    :cond_1
    iget-object v0, v0, List;->b:[Lisj;

    iget v2, v1, Lisg;->d:I

    add-int/2addr v2, p1

    add-int/lit8 v2, v2, -0x1

    iget v1, v1, Lisg;->b:I

    sub-int v1, v2, v1

    aget-object v0, v0, v1

    goto :goto_0

    .line 210
    :cond_2
    iget-object v1, v0, List;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, p1

    .line 211
    iget-object v2, v0, List;->c:[Lisg;

    iget-object v3, v0, List;->a:[I

    aget v3, v3, v1

    aget-object v2, v2, v3

    .line 212
    iget v3, v2, Lisg;->c:I

    if-ne v3, v1, :cond_3

    .line 213
    iget-object v0, v2, Lisg;->a:Lisu;

    goto :goto_0

    .line 215
    :cond_3
    iget-object v0, v0, List;->b:[Lisj;

    iget v3, v2, Lisg;->d:I

    add-int/2addr v1, v3

    iget v2, v2, Lisg;->b:I

    sub-int/2addr v1, v2

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public declared-synchronized a(Landroid/mtp/MtpDevice;)V
    .locals 1

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liso;->c:Landroid/mtp/MtpDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 140
    :goto_0
    monitor-exit p0

    return-void

    .line 138
    :cond_0
    :try_start_1
    iput-object p1, p0, Liso;->c:Landroid/mtp/MtpDevice;

    .line 139
    invoke-virtual {p0}, Liso;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Lisj;I)V
    .locals 1

    .prologue
    .line 424
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liso;->e:Lisp;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Liso;->e:Lisp;

    invoke-interface {v0, p1, p2}, Lisp;->a(Lisj;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    :cond_0
    monitor-exit p0

    return-void

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lisp;)V
    .locals 1

    .prologue
    .line 167
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Liso;->e:Lisp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    monitor-exit p0

    return-void

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 409
    monitor-enter p0

    if-nez p1, :cond_0

    .line 410
    :try_start_0
    invoke-virtual {p0}, Liso;->i()V

    .line 412
    :cond_0
    iget-object v0, p0, Liso;->e:Lisp;

    if-eqz v0, :cond_1

    .line 413
    iget-object v0, p0, Liso;->e:Lisp;

    invoke-interface {v0}, Lisp;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    :cond_1
    monitor-exit p0

    return-void

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 123
    sget-object v0, Liso;->a:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Liso;->b:Ljava/util/Set;

    .line 124
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/mtp/MtpDevice;J)Z
    .locals 2

    .prologue
    .line 395
    iget-wide v0, p0, Liso;->d:J

    cmp-long v0, v0, p2

    if-nez v0, :cond_0

    iget-object v0, p0, Liso;->c:Landroid/mtp/MtpDevice;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected declared-synchronized a(Landroid/mtp/MtpDevice;JList;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 400
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Liso;->a(Landroid/mtp/MtpDevice;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 401
    const/4 v0, 0x0

    .line 405
    :goto_0
    monitor-exit p0

    return v0

    .line 403
    :cond_0
    :try_start_1
    iput-object p4, p0, Liso;->f:List;

    .line 404
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Liso;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Landroid/mtp/MtpDevice;
    .locals 1

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liso;->c:Landroid/mtp/MtpDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(II)Lisj;
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Liso;->f:List;

    .line 228
    if-nez v0, :cond_0

    .line 229
    const/4 v0, 0x0

    .line 234
    :goto_0
    return-object v0

    .line 231
    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    .line 232
    iget-object v0, v0, List;->b:[Lisj;

    aget-object v0, v0, p1

    goto :goto_0

    .line 234
    :cond_1
    iget-object v1, v0, List;->b:[Lisj;

    iget-object v0, v0, List;->b:[Lisj;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method public declared-synchronized b(Lisp;)V
    .locals 1

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liso;->e:Lisp;

    if-ne v0, p1, :cond_0

    .line 177
    const/4 v0, 0x0

    iput-object v0, p0, Liso;->e:Lisp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    :cond_0
    monitor-exit p0

    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)[Lisg;
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Liso;->f:List;

    .line 374
    if-nez v0, :cond_0

    .line 375
    const/4 v0, 0x0

    .line 377
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    iget-object v0, v0, List;->c:[Lisg;

    goto :goto_0

    :cond_1
    iget-object v0, v0, List;->d:[Lisg;

    goto :goto_0
.end method

.method public c(II)I
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    .line 250
    iget-object v4, p0, Liso;->f:List;

    .line 251
    if-nez v4, :cond_1

    .line 252
    const/4 v0, -0x1

    .line 277
    :cond_0
    :goto_0
    return v0

    .line 254
    :cond_1
    if-ne p2, v7, :cond_2

    .line 255
    iget-object v0, v4, List;->b:[Lisj;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    sub-int p1, v0, p1

    .line 259
    :cond_2
    iget-object v0, v4, List;->c:[Lisg;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v3, v1

    .line 260
    :goto_1
    if-lt v2, v3, :cond_4

    .line 261
    add-int v0, v2, v3

    div-int/lit8 v0, v0, 0x2

    .line 262
    iget-object v5, v4, List;->c:[Lisg;

    aget-object v5, v5, v0

    iget v5, v5, Lisg;->d:I

    iget-object v6, v4, List;->c:[Lisg;

    aget-object v6, v6, v0

    iget v6, v6, Lisg;->e:I

    add-int/2addr v5, v6

    if-gt v5, p1, :cond_3

    .line 264
    add-int/lit8 v0, v0, 0x1

    move v3, v0

    goto :goto_1

    .line 265
    :cond_3
    iget-object v2, v4, List;->c:[Lisg;

    aget-object v2, v2, v0

    iget v2, v2, Lisg;->d:I

    if-le v2, p1, :cond_5

    .line 266
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 269
    :cond_5
    iget-object v1, v4, List;->c:[Lisg;

    aget-object v1, v1, v0

    iget v1, v1, Lisg;->b:I

    add-int/2addr v1, p1

    iget-object v2, v4, List;->c:[Lisg;

    aget-object v0, v2, v0

    iget v0, v0, Lisg;->d:I

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x1

    .line 274
    if-ne p2, v7, :cond_0

    .line 275
    iget-object v1, v4, List;->a:[I

    array-length v1, v1

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public declared-synchronized c()Z
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liso;->c:Landroid/mtp/MtpDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(II)I
    .locals 4

    .prologue
    .line 288
    iget-object v1, p0, Liso;->f:List;

    .line 289
    if-nez v1, :cond_0

    .line 290
    const/4 v0, -0x1

    .line 304
    :goto_0
    return v0

    .line 292
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 293
    iget-object v0, v1, List;->c:[Lisg;

    iget-object v1, v1, List;->a:[I

    aget v1, v1, p1

    aget-object v0, v0, v1

    .line 294
    iget v1, v0, Lisg;->b:I

    if-ne v1, p1, :cond_1

    .line 295
    add-int/lit8 p1, p1, 0x1

    .line 297
    :cond_1
    iget v1, v0, Lisg;->d:I

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    iget v0, v0, Lisg;->b:I

    sub-int v0, v1, v0

    goto :goto_0

    .line 299
    :cond_2
    iget-object v0, v1, List;->a:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    .line 300
    iget-object v2, v1, List;->c:[Lisg;

    iget-object v3, v1, List;->a:[I

    aget v3, v3, v0

    aget-object v2, v2, v3

    .line 301
    iget v3, v2, Lisg;->c:I

    if-ne v3, v0, :cond_3

    .line 302
    add-int/lit8 v0, v0, -0x1

    .line 304
    :cond_3
    iget-object v1, v1, List;->b:[Lisj;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget v3, v2, Lisg;->d:I

    sub-int/2addr v1, v3

    sub-int v0, v1, v0

    iget v1, v2, Lisg;->b:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public declared-synchronized d()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Liso;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Liso;->f:List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 150
    :cond_0
    const/4 v0, 0x0

    .line 152
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Liso;->g:Lisr;

    invoke-virtual {v0, p0}, Lisr;->a(Liso;)Lisq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e(II)I
    .locals 3

    .prologue
    .line 323
    iget-object v0, p0, Liso;->f:List;

    .line 324
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 325
    iget-object v0, v0, List;->c:[Lisg;

    aget-object v0, v0, p1

    iget v0, v0, Lisg;->b:I

    .line 327
    :goto_0
    return v0

    :cond_0
    iget-object v1, v0, List;->a:[I

    array-length v1, v1

    iget-object v2, v0, List;->c:[Lisg;

    iget-object v0, v0, List;->c:[Lisg;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    aget-object v0, v2, v0

    iget v0, v0, Lisg;->c:I

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public declared-synchronized e()Z
    .locals 1

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liso;->f:List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Liso;->f:List;

    .line 186
    if-eqz v0, :cond_0

    iget-object v0, v0, List;->a:[I

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(II)I
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Liso;->f:List;

    .line 341
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 342
    iget-object v0, v0, List;->a:[I

    aget v0, v0, p1

    .line 344
    :goto_0
    return v0

    :cond_0
    iget-object v1, v0, List;->c:[Lisg;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, v0, List;->a:[I

    iget-object v0, v0, List;->a:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    aget v0, v2, v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Liso;->f:List;

    .line 314
    if-eqz v0, :cond_0

    iget-object v0, v0, List;->b:[Lisj;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Liso;->c:Landroid/mtp/MtpDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Liso;->f:List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 385
    iget-wide v0, p0, Liso;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Liso;->d:J

    .line 386
    const/4 v0, 0x0

    iput-object v0, p0, Liso;->f:List;

    .line 387
    return-void
.end method

.method protected declared-synchronized j()V
    .locals 1

    .prologue
    .line 418
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liso;->e:Lisp;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Liso;->e:Lisp;

    invoke-interface {v0}, Lisp;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    :cond_0
    monitor-exit p0

    return-void

    .line 418
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected k()J
    .locals 2

    .prologue
    .line 430
    iget-wide v0, p0, Liso;->d:J

    return-wide v0
.end method
