.class public final Llna;
.super Loxu;
.source "PG"


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field public d:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 9
    invoke-direct {p0}, Loxu;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Llna;->a:I

    .line 15
    iput-wide v2, p0, Llna;->b:J

    .line 18
    iput-wide v2, p0, Llna;->c:J

    .line 21
    sget-object v0, Loxx;->f:[B

    iput-object v0, p0, Llna;->d:[B

    .line 9
    return-void
.end method

.method public static a([B)Llna;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Llna;

    invoke-direct {v0}, Llna;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Llna;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 49
    const/4 v0, 0x1

    iget v1, p0, Llna;->a:I

    .line 51
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 52
    iget-wide v2, p0, Llna;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 53
    const/4 v1, 0x2

    iget-wide v2, p0, Llna;->b:J

    .line 54
    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_0
    iget-wide v2, p0, Llna;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 57
    const/4 v1, 0x3

    iget-wide v2, p0, Llna;->c:J

    .line 58
    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_1
    iget-object v1, p0, Llna;->d:[B

    sget-object v2, Loxx;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    .line 61
    const/4 v1, 0x4

    iget-object v2, p0, Llna;->d:[B

    .line 62
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_2
    iput v0, p0, Llna;->ai:I

    .line 65
    return v0
.end method

.method public a(Loxn;)Llna;
    .locals 2

    .prologue
    .line 73
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 74
    sparse-switch v0, :sswitch_data_0

    .line 78
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    :sswitch_0
    return-object p0

    .line 84
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Llna;->a:I

    goto :goto_0

    .line 88
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    iput-wide v0, p0, Llna;->b:J

    goto :goto_0

    .line 92
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    iput-wide v0, p0, Llna;->c:J

    goto :goto_0

    .line 96
    :sswitch_4
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Llna;->d:[B

    goto :goto_0

    .line 74
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 35
    const/4 v0, 0x1

    iget v1, p0, Llna;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 36
    iget-wide v0, p0, Llna;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x2

    iget-wide v2, p0, Llna;->b:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 39
    :cond_0
    iget-wide v0, p0, Llna;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 40
    const/4 v0, 0x3

    iget-wide v2, p0, Llna;->c:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 42
    :cond_1
    iget-object v0, p0, Llna;->d:[B

    sget-object v1, Loxx;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 43
    const/4 v0, 0x4

    iget-object v1, p0, Llna;->d:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 45
    :cond_2
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llna;->a(Loxn;)Llna;

    move-result-object v0

    return-object v0
.end method
