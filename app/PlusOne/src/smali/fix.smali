.class public final Lfix;
.super Llol;
.source "PG"

# interfaces
.implements Lkhf;
.implements Llgs;


# instance fields
.field private final N:Lkhe;

.field private O:Lkhr;

.field private P:Lhee;

.field private Q:Llgr;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Llol;-><init>()V

    .line 26
    new-instance v0, Lkhe;

    iget-object v1, p0, Lfix;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    iput-object v0, p0, Lfix;->N:Lkhe;

    return-void
.end method

.method static synthetic a(Lfix;)Llgr;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lfix;->Q:Llgr;

    return-object v0
.end method

.method static synthetic a(Lfix;Llgr;)Llgr;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lfix;->Q:Llgr;

    return-object p1
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lkhr;

    iget-object v1, p0, Lfix;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfix;->O:Lkhr;

    .line 41
    iget-object v0, p0, Lfix;->O:Lkhr;

    const v1, 0x7f0a089c

    .line 42
    invoke-virtual {p0, v1}, Lfix;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a089d

    .line 43
    invoke-virtual {p0, v2}, Lfix;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 41
    invoke-virtual {v0, v1, v2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 44
    new-instance v1, Lfiy;

    invoke-direct {v1, p0}, Lfiy;-><init>(Lfix;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 57
    iget-object v1, p0, Lfix;->N:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 58
    return-void
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lfix;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 63
    iget-object v1, p0, Lfix;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "logged_out"

    const/4 v2, 0x1

    .line 64
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Lhek;->c()I

    .line 66
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "https://plus.google.com/downgrade/"

    .line 67
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 68
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Lfix;->a(Landroid/content/Intent;)V

    .line 70
    invoke-virtual {p0}, Lfix;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 71
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lfix;->Q:Llgr;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lfix;->Q:Llgr;

    invoke-virtual {v0}, Llgr;->c()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 78
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 35
    iget-object v0, p0, Lfix;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lfix;->P:Lhee;

    .line 36
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfix;->Q:Llgr;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lfix;->Q:Llgr;

    invoke-virtual {v0}, Llgr;->c()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 85
    :cond_0
    return-void
.end method
