.class public final Leco;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;
.implements Ldzn;
.implements Lexs;
.implements Lfxf;
.implements Lhjj;
.implements Lhmq;
.implements Lhob;
.implements Lkch;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ldzn;",
        "Lexs;",
        "Lfxf;",
        "Lhjj;",
        "Lhmq;",
        "Lhob;",
        "Lkch;",
        "Llgs;"
    }
.end annotation


# static fields
.field private static N:I


# instance fields
.field private final O:Lhje;

.field private P:Lhee;

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Lidh;

.field private T:Llah;

.field private U:I

.field private V:I

.field private W:Ljava/lang/String;

.field private X:Z

.field private Y:Z

.field private Z:Ljava/lang/String;

.field private aA:Z

.field private aB:I

.field private aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private aD:Z

.field private aE:J

.field private aF:[B

.field private aG:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Void;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private aH:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private aI:Lhrt;

.field private aJ:Liwk;

.field private final aK:Licq;

.field private aL:Lkci;

.field private aM:Landroid/database/ContentObserver;

.field private final aN:Lfhh;

.field private aa:Ljava/lang/String;

.field private ab:I

.field private final ac:Ledl;

.field private ad:Ljava/lang/Runnable;

.field private ae:Z

.field private af:Leaq;

.field private ag:Ljava/lang/Runnable;

.field private ah:Z

.field private ai:Lexq;

.field private aj:Ljava/lang/String;

.field private ak:Ljava/lang/String;

.field private al:I

.field private am:Ljava/lang/Integer;

.field private an:Ljava/lang/Integer;

.field private ao:Ljava/lang/Integer;

.field private ap:Ljava/lang/Integer;

.field private aq:Ljava/lang/Integer;

.field private ar:Z

.field private as:Z

.field private aw:Ljava/lang/Integer;

.field private ax:Ljava/lang/Integer;

.field private ay:Z

.field private az:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 147
    invoke-direct {p0}, Llol;-><init>()V

    .line 231
    new-instance v0, Lhje;

    iget-object v1, p0, Leco;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Leco;->O:Lhje;

    .line 251
    new-instance v0, Ledl;

    invoke-direct {v0, p0}, Ledl;-><init>(Leco;)V

    iput-object v0, p0, Leco;->ac:Ledl;

    .line 261
    new-instance v0, Leaq;

    invoke-direct {v0}, Leaq;-><init>()V

    iput-object v0, p0, Leco;->af:Leaq;

    .line 268
    const/high16 v0, -0x80000000

    iput v0, p0, Leco;->al:I

    .line 285
    const/4 v0, -0x1

    iput v0, p0, Leco;->aB:I

    .line 296
    new-instance v0, Licq;

    iget-object v1, p0, Leco;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    iput-object v0, p0, Leco;->aK:Licq;

    .line 299
    new-instance v0, Lhnw;

    new-instance v1, Ledk;

    invoke-direct {v1, p0}, Ledk;-><init>(Leco;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 302
    new-instance v0, Lkci;

    iget-object v1, p0, Leco;->av:Llqm;

    const v2, 0x7f10005b

    invoke-direct {v0, p0, v1, v2, p0}, Lkci;-><init>(Lu;Llqr;ILkch;)V

    iput-object v0, p0, Leco;->aL:Lkci;

    .line 478
    new-instance v0, Lecp;

    .line 479
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lecp;-><init>(Leco;Landroid/os/Handler;)V

    iput-object v0, p0, Leco;->aM:Landroid/database/ContentObserver;

    .line 520
    new-instance v0, Lecs;

    invoke-direct {v0, p0}, Lecs;-><init>(Leco;)V

    iput-object v0, p0, Leco;->aN:Lfhh;

    .line 2470
    return-void
.end method

.method static synthetic a(Leco;I)I
    .locals 0

    .prologue
    .line 147
    iput p1, p0, Leco;->ab:I

    return p1
.end method

.method static synthetic a(Leco;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Leco;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Leco;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Leco;->Q:Ljava/lang/String;

    return-object p1
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 587
    iget-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 608
    :cond_0
    :goto_0
    return-void

    .line 591
    :cond_1
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 593
    if-eqz v0, :cond_2

    .line 594
    invoke-virtual {v0}, Lt;->a()V

    .line 597
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    .line 598
    invoke-direct {p0}, Leco;->ad()V

    .line 600
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    .line 602
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 603
    const v1, 0x7f0a0592

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 605
    :cond_3
    const v1, 0x7f0a0862

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 606
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 147
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p0}, Lhqd;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "instant"

    move-object v6, v0

    :goto_0
    move v8, v1

    :goto_1
    array-length v0, p3

    if-ge v8, v0, :cond_4

    aget-object v1, p3, v8

    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    const-string v0, "album_id"

    invoke-virtual {v9, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "upload_account_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "media_url"

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    :try_start_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lhsf;->d:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "media_id"

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    invoke-static {p0}, Lhqv;->e(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    :cond_2
    move-object v6, v7

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    return-void

    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_2
.end method

.method private a(Landroid/content/Context;Lpao;)V
    .locals 15

    .prologue
    .line 2572
    move-object/from16 v0, p2

    iget-object v7, v0, Lpao;->f:Ljava/lang/String;

    .line 2577
    move-object/from16 v0, p2

    iget-object v1, v0, Lpao;->e:Loya;

    if-eqz v1, :cond_2

    .line 2578
    move-object/from16 v0, p2

    iget-object v1, v0, Lpao;->e:Loya;

    sget-object v2, Lozb;->a:Loxr;

    invoke-virtual {v1, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lozb;

    .line 2579
    iget-object v3, v1, Lozb;->c:Ljava/lang/Double;

    .line 2580
    iget-object v2, v1, Lozb;->d:Ljava/lang/Double;

    .line 2581
    iget-object v1, v1, Lozb;->b:Ljava/lang/String;

    .line 2589
    :goto_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    move v6, v4

    :goto_1
    if-eqz v3, :cond_4

    if-eqz v2, :cond_4

    const/4 v4, 0x1

    :goto_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_3
    const-string v8, "http://maps.google.com/maps"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    if-eqz v4, :cond_0

    const-string v9, "ll"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v13, v14

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, ","

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    if-eqz v6, :cond_6

    const-string v1, "cid"

    invoke-virtual {v8, v1, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    :goto_4
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lisy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2590
    return-void

    .line 2584
    :cond_2
    const/4 v3, 0x0

    .line 2585
    const/4 v2, 0x0

    .line 2586
    const/4 v1, 0x0

    goto :goto_0

    .line 2589
    :cond_3
    const/4 v4, 0x0

    move v6, v4

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    goto :goto_3

    :cond_6
    if-eqz v4, :cond_8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v6, 0x2c

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    if-eqz v5, :cond_7

    const/16 v2, 0x28

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_7
    const-string v1, "q"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_4

    :cond_8
    if-eqz v5, :cond_1

    const-string v2, "q"

    invoke-virtual {v8, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_4
.end method

.method static synthetic a(Leco;ILfib;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Leco;->b(ILfib;)V

    return-void
.end method

.method static synthetic a(Leco;Lfib;)V
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p1}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->at:Llnl;

    const v1, 0x7f0a0864

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method static synthetic a(Leco;Lhgw;)V
    .locals 6

    .prologue
    .line 147
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->Q:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0a0947

    invoke-direct {p0, v0}, Leco;->f(I)V

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->Q:Ljava/lang/String;

    iget-object v3, p0, Leco;->aa:Ljava/lang/String;

    iget-object v4, p0, Leco;->S:Lidh;

    if-eqz v4, :cond_2

    iget-object v4, p0, Leco;->S:Lidh;

    invoke-virtual {v4}, Lidh;->e()Ljava/lang/String;

    move-result-object v4

    :goto_1
    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhgw;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method static synthetic a(Leco;Z)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Leco;->c(Z)V

    return-void
.end method

.method static synthetic a(Leco;ZZ)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Leco;->a(ZZ)V

    return-void
.end method

.method static synthetic a(Leco;ZZZ)V
    .locals 11

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 147
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v4

    const-string v0, "dialog_sync_disabled"

    invoke-virtual {v4, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f0a06c4

    invoke-virtual {p0, v0}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0a07f8

    invoke-virtual {p0, v0}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v5

    const v0, 0x7f0a06c0

    invoke-virtual {p0, v0}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, "dialog_sync_disabled"

    const v3, 0x7f0a0440

    invoke-virtual {p0, v3}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Leco;->P:Lhee;

    invoke-interface {v6}, Lhee;->g()Lhej;

    move-result-object v6

    const-string v7, "account_name"

    invoke-interface {v6, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz p1, :cond_2

    if-eqz p3, :cond_1

    const v7, 0x7f0a0922

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v3, v8, v9

    aput-object v6, v8, v10

    invoke-virtual {p0, v7, v8}, Leco;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-static {v1, v3, v2, v5}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v1

    invoke-virtual {v1, p0, v9}, Llgr;->a(Lu;I)V

    invoke-virtual {v1, v4, v0}, Llgr;->a(Lae;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0a0924

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v3, v1, v9

    aput-object v6, v1, v10

    invoke-virtual {p0, v0, v1}, Leco;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0a0926

    invoke-virtual {p0, v0}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0a0925

    invoke-virtual {p0, v0}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, "dialog_backup_disabled"

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_4

    if-eqz p2, :cond_3

    const v3, 0x7f0a0921

    invoke-virtual {p0, v3}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_3
    const v7, 0x7f0a0920

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v3, v8, v9

    aput-object v6, v8, v10

    invoke-virtual {p0, v7, v8}, Leco;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_4
    const v0, 0x7f0a0923

    invoke-virtual {p0, v0}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "dialog_backup_disabled"

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 2215
    if-eqz p2, :cond_0

    .line 2216
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->dA:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    :try_start_0
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    iget-object v0, p0, Leco;->at:Llnl;

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IZILjava/lang/Integer;ZII)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Leco;->a(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2219
    :cond_0
    :goto_0
    iget-object v0, p0, Leco;->S:Lidh;

    invoke-static {v0}, Ldrm;->b(Lidh;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 2220
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->Q:Ljava/lang/String;

    iget-object v3, p0, Leco;->aa:Ljava/lang/String;

    const/4 v4, 0x3

    iget-object v5, p0, Leco;->aF:[B

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I[B)I

    .line 2224
    :cond_1
    invoke-direct {p0, v10}, Leco;->k(Z)V

    .line 2225
    return-void

    .line 2216
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a08ed

    invoke-static {v0, v1, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic a(Leco;[B)[B
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Leco;->aF:[B

    return-object p1
.end method

.method private ad()V
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Leco;->aL:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 312
    :cond_0
    iget-object v0, p0, Leco;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 313
    return-void
.end method

.method private ae()V
    .locals 3

    .prologue
    .line 489
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->dA:Lhmv;

    .line 490
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 489
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 491
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 493
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0}, Leyq;->f(Landroid/content/Context;I)Leyt;

    move-result-object v0

    const/4 v1, 0x2

    .line 494
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Leyt;->a(Ljava/lang/Integer;)Leyt;

    move-result-object v0

    const/16 v1, 0x1e

    .line 495
    invoke-virtual {v0, v1}, Leyt;->a(I)Leyt;

    move-result-object v0

    .line 496
    invoke-virtual {v0}, Leyt;->a()Landroid/content/Intent;

    move-result-object v0

    .line 497
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Leco;->a(Landroid/content/Intent;I)V

    .line 498
    return-void
.end method

.method private af()V
    .locals 3

    .prologue
    .line 1308
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v0

    const-string v1, "comment"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 1311
    if-nez v0, :cond_0

    .line 1312
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aN:Lhmv;

    .line 1313
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1312
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1314
    const v0, 0x7f0a0751

    iget-object v1, p0, Leco;->W:Ljava/lang/String;

    iget-object v2, p0, Leco;->P:Lhee;

    .line 1316
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 1315
    invoke-static {v0, v1, v2}, Ldzl;->a(ILjava/lang/String;I)Ldzl;

    move-result-object v0

    .line 1317
    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Ldzl;->a(Lu;I)V

    .line 1318
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v1

    const-string v2, "comment"

    invoke-virtual {v0, v1, v2}, Ldzl;->a(Lae;Ljava/lang/String;)V

    .line 1320
    :cond_0
    return-void
.end method

.method private ag()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 1326
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1327
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    const v2, 0x7f0a0941

    .line 1328
    invoke-virtual {p0, v2}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Leco;->S:Lidh;

    .line 1330
    invoke-virtual {v4}, Lidh;->i()Lpbj;

    move-result-object v4

    iget-object v4, v4, Lpbj;->b:Lpbd;

    iget-object v4, v4, Lpbd;->b:Lpbc;

    iget-object v4, v4, Lpbc;->a:Ljava/lang/Boolean;

    .line 1329
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0xc

    :goto_0
    const/4 v7, 0x1

    move v6, v5

    move v8, v5

    .line 1327
    invoke-static/range {v0 .. v8}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Lhgw;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Leco;->a(Landroid/content/Intent;I)V

    .line 1340
    return-void

    .line 1329
    :cond_0
    const/16 v4, 0xb

    goto :goto_0
.end method

.method private ah()V
    .locals 1

    .prologue
    .line 1611
    invoke-virtual {p0}, Leco;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Leco;->c(Z)V

    .line 1612
    return-void

    .line 1611
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ai()V
    .locals 2

    .prologue
    .line 1779
    iget-object v0, p0, Leco;->ag:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1780
    iget-object v0, p0, Leco;->ag:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1781
    const/4 v0, 0x0

    iput-object v0, p0, Leco;->ag:Ljava/lang/Runnable;

    .line 1783
    :cond_0
    return-void
.end method

.method private aj()V
    .locals 2

    .prologue
    .line 1810
    .line 1811
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 1812
    if-eqz v0, :cond_0

    .line 1813
    invoke-virtual {v0}, Lt;->a()V

    .line 1815
    :cond_0
    return-void
.end method

.method private ak()V
    .locals 3

    .prologue
    .line 2134
    invoke-virtual {p0}, Leco;->x()Landroid/view/View;

    move-result-object v0

    .line 2135
    if-nez v0, :cond_1

    .line 2146
    :cond_0
    :goto_0
    return-void

    .line 2139
    :cond_1
    const v1, 0x7f1000a7

    .line 2140
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfxy;

    .line 2142
    if-eqz v0, :cond_0

    .line 2143
    iget-object v1, p0, Leco;->S:Lidh;

    iget-object v2, p0, Leco;->af:Leaq;

    invoke-virtual {v0, v1, v2, p0}, Lfxy;->a(Lidh;Leaq;Lfxf;)V

    .line 2144
    invoke-virtual {v0}, Lfxy;->invalidate()V

    goto :goto_0
.end method

.method private al()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2149
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2151
    iget-object v0, p0, Leco;->af:Leaq;

    iget-boolean v3, p0, Leco;->Y:Z

    iput-boolean v3, v0, Leaq;->h:Z

    .line 2153
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2154
    iget-object v3, p0, Leco;->af:Leaq;

    iget-object v6, p0, Leco;->S:Lidh;

    invoke-virtual {v6}, Lidh;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    iput-boolean v6, v3, Leaq;->k:Z

    .line 2157
    iget-object v3, p0, Leco;->af:Leaq;

    iput-boolean v2, v3, Leaq;->e:Z

    .line 2158
    iget-object v3, p0, Leco;->af:Leaq;

    iput-boolean v2, v3, Leaq;->f:Z

    .line 2160
    iget-object v3, p0, Leco;->af:Leaq;

    iget-boolean v3, v3, Leaq;->g:Z

    if-eqz v3, :cond_0

    .line 2161
    iget-object v3, p0, Leco;->S:Lidh;

    invoke-static {v3, v0, v4, v5}, Ldrm;->a(Lidh;Ljava/lang/String;J)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2162
    iget-object v0, p0, Leco;->af:Leaq;

    iput-boolean v1, v0, Leaq;->e:Z

    .line 2179
    :cond_0
    :goto_0
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 2180
    iget-object v3, p0, Leco;->af:Leaq;

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v4

    iget-object v5, p0, Leco;->S:Lidh;

    invoke-static {v4, v5, v0}, Ldrm;->b(Landroid/content/Context;Lidh;I)Z

    move-result v0

    iput-boolean v0, v3, Leaq;->j:Z

    .line 2183
    iget-object v3, p0, Leco;->af:Leaq;

    iget-object v0, p0, Leco;->am:Ljava/lang/Integer;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Leaq;->c:Z

    .line 2184
    iget-object v0, p0, Leco;->af:Leaq;

    iget v3, p0, Leco;->al:I

    iput v3, v0, Leaq;->b:I

    .line 2186
    iget-object v0, p0, Leco;->af:Leaq;

    iget v3, p0, Leco;->V:I

    iput v3, v0, Leaq;->a:I

    .line 2188
    iget-object v0, p0, Leco;->af:Leaq;

    iget-object v3, p0, Leco;->W:Ljava/lang/String;

    if-eqz v3, :cond_7

    :goto_2
    iput-boolean v1, v0, Leaq;->l:Z

    .line 2191
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_plus_page"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2192
    iget-object v0, p0, Leco;->af:Leaq;

    iput-boolean v2, v0, Leaq;->e:Z

    .line 2194
    iget-object v0, p0, Leco;->af:Leaq;

    iput-boolean v2, v0, Leaq;->f:Z

    .line 2197
    :cond_1
    iget v0, p0, Leco;->U:I

    const/16 v1, 0x3a

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Leco;->af:Leaq;

    iget-boolean v0, v0, Leaq;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Leco;->af:Leaq;

    iget-boolean v0, v0, Leaq;->d:Z

    if-nez v0, :cond_2

    .line 2199
    new-instance v0, Lecq;

    invoke-direct {v0, p0}, Lecq;-><init>(Leco;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 2208
    :cond_2
    iput v2, p0, Leco;->U:I

    .line 2209
    return-void

    .line 2163
    :cond_3
    iget-object v3, p0, Leco;->S:Lidh;

    invoke-static {v3, v4, v5}, Ldrm;->a(Lidh;J)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2164
    iget-object v0, p0, Leco;->af:Leaq;

    iput-boolean v1, v0, Leaq;->f:Z

    goto :goto_0

    .line 2166
    :cond_4
    iget-object v3, p0, Leco;->ad:Ljava/lang/Runnable;

    if-nez v3, :cond_5

    .line 2167
    new-instance v3, Ledf;

    invoke-direct {v3, p0}, Ledf;-><init>(Leco;)V

    iput-object v3, p0, Leco;->ad:Ljava/lang/Runnable;

    .line 2170
    :cond_5
    iget-object v3, p0, Leco;->ad:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2171
    iget-object v3, p0, Leco;->S:Lidh;

    invoke-static {v3, v0, v4, v5}, Ldrm;->b(Lidh;Ljava/lang/String;J)J

    move-result-wide v4

    .line 2173
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    .line 2174
    iget-object v0, p0, Leco;->ad:Ljava/lang/Runnable;

    invoke-static {v0, v4, v5}, Llsx;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 2183
    goto :goto_1

    :cond_7
    move v1, v2

    .line 2188
    goto :goto_2
.end method

.method static synthetic b(Leco;I)I
    .locals 0

    .prologue
    .line 147
    iput p1, p0, Leco;->U:I

    return p1
.end method

.method static synthetic b(Leco;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Leco;->R:Ljava/lang/String;

    return-object p1
.end method

.method private b(ILfib;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1429
    iget-object v0, p0, Leco;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->an:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 1455
    :cond_0
    :goto_0
    return-void

    .line 1433
    :cond_1
    iput-object v2, p0, Leco;->an:Ljava/lang/Integer;

    .line 1435
    invoke-direct {p0}, Leco;->ad()V

    .line 1436
    invoke-direct {p0}, Leco;->aj()V

    .line 1438
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1439
    invoke-virtual {p2}, Lfib;->c()I

    move-result v0

    .line 1440
    const/16 v1, 0x190

    if-lt v0, v1, :cond_3

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_3

    .line 1441
    iput-boolean v4, p0, Leco;->as:Z

    .line 1450
    :cond_2
    :goto_1
    invoke-virtual {p0}, Leco;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1451
    invoke-virtual {p0}, Leco;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1453
    iget-object v0, p0, Leco;->ai:Lexq;

    const-string v1, "HGEUC"

    invoke-virtual {v0, v1}, Lexq;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 1443
    :cond_3
    iput-boolean v4, p0, Leco;->ay:Z

    .line 1444
    iget-object v0, p0, Leco;->S:Lidh;

    if-eqz v0, :cond_2

    .line 1445
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0695

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1446
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method static synthetic b(Leco;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Leco;->af()V

    return-void
.end method

.method static synthetic b(Leco;ILfib;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Leco;->d(ILfib;)V

    return-void
.end method

.method static synthetic b(Leco;Z)Z
    .locals 0

    .prologue
    .line 147
    iput-boolean p1, p0, Leco;->ar:Z

    return p1
.end method

.method static synthetic c(Leco;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Leco;->Z:Ljava/lang/String;

    return-object p1
.end method

.method private c(ILfib;)V
    .locals 3

    .prologue
    .line 1477
    iget-object v0, p0, Leco;->am:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->am:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 1500
    :cond_0
    :goto_0
    return-void

    .line 1481
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Leco;->am:Ljava/lang/Integer;

    .line 1483
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v0

    const-string v1, "send_rsvp"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 1485
    if-eqz v0, :cond_2

    .line 1486
    invoke-virtual {v0}, Lt;->a()V

    .line 1489
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1490
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1491
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1493
    :cond_3
    const/high16 v0, -0x80000000

    iput v0, p0, Leco;->al:I

    .line 1495
    iget-object v0, p0, Leco;->S:Lidh;

    if-eqz v0, :cond_0

    .line 1496
    invoke-direct {p0}, Leco;->al()V

    .line 1497
    invoke-direct {p0}, Leco;->ak()V

    goto :goto_0
.end method

.method private c(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1208
    if-nez p1, :cond_0

    .line 1235
    :goto_0
    return-void

    .line 1212
    :cond_0
    const v0, 0x7f100249

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1213
    const v1, 0x7f100304

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1214
    iget-boolean v2, p0, Leco;->as:Z

    if-eqz v2, :cond_2

    .line 1215
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1216
    const v2, 0x7f0a090c

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1217
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1218
    iget-object v0, p0, Leco;->aK:Licq;

    invoke-virtual {v0}, Licq;->e()V

    .line 1234
    :cond_1
    :goto_1
    invoke-direct {p0}, Leco;->ad()V

    goto :goto_0

    .line 1219
    :cond_2
    iget-object v2, p0, Leco;->S:Lidh;

    if-eqz v2, :cond_3

    .line 1220
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1221
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1222
    iget-object v0, p0, Leco;->aK:Licq;

    invoke-virtual {v0}, Licq;->e()V

    goto :goto_1

    .line 1223
    :cond_3
    iget-boolean v2, p0, Leco;->az:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Leco;->an:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 1224
    :cond_4
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1225
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1226
    iget-object v0, p0, Leco;->aK:Licq;

    invoke-virtual {v0}, Licq;->a()V

    goto :goto_1

    .line 1227
    :cond_5
    iget-boolean v2, p0, Leco;->ay:Z

    if-eqz v2, :cond_1

    .line 1228
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1229
    const v2, 0x7f0a090b

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1230
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1231
    iget-object v0, p0, Leco;->aK:Licq;

    invoke-virtual {v0}, Licq;->e()V

    goto :goto_1
.end method

.method static synthetic c(Leco;)V
    .locals 8

    .prologue
    .line 147
    const/4 v0, 0x0

    iput-boolean v0, p0, Leco;->ae:Z

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->Q:Ljava/lang/String;

    iget-object v3, p0, Leco;->aj:Ljava/lang/String;

    iget-object v4, p0, Leco;->ak:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Leco;->aa:Ljava/lang/String;

    iget-boolean v7, p0, Leco;->ae:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->an:Ljava/lang/Integer;

    invoke-direct {p0}, Leco;->ad()V

    return-void
.end method

.method static synthetic c(Leco;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Leco;->e(I)V

    return-void
.end method

.method static synthetic c(Leco;ILfib;)V
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Leco;->aw:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->aw:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Leco;->aj()V

    const/4 v0, 0x0

    iput-object v0, p0, Leco;->aw:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aQ:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    goto :goto_0
.end method

.method private c(Z)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 900
    const/4 v0, 0x0

    iput-boolean v0, p0, Leco;->ay:Z

    .line 901
    const/4 v0, 0x1

    iput-boolean v0, p0, Leco;->ae:Z

    .line 902
    if-eqz p1, :cond_0

    move-object v3, v4

    .line 903
    :goto_0
    if-eqz p1, :cond_1

    .line 904
    :goto_1
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->Q:Ljava/lang/String;

    iget-object v5, p0, Leco;->Z:Ljava/lang/String;

    iget-object v6, p0, Leco;->aa:Ljava/lang/String;

    iget-boolean v7, p0, Leco;->ae:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->an:Ljava/lang/Integer;

    .line 907
    invoke-direct {p0}, Leco;->ad()V

    .line 908
    return-void

    .line 902
    :cond_0
    iget-object v3, p0, Leco;->aj:Ljava/lang/String;

    goto :goto_0

    .line 903
    :cond_1
    iget-object v4, p0, Leco;->ak:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic d(Leco;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Leco;->aa:Ljava/lang/String;

    return-object p1
.end method

.method private d(ILfib;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1503
    iget-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 1519
    :cond_0
    :goto_0
    return-void

    .line 1507
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    .line 1509
    invoke-direct {p0}, Leco;->aj()V

    .line 1511
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1512
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1513
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1515
    :cond_2
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aP:Lhmv;

    .line 1516
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1515
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1517
    invoke-direct {p0, v3}, Leco;->c(Z)V

    goto :goto_0
.end method

.method static synthetic d(Leco;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v3

    const-string v0, "dialog_check_in"

    invoke-virtual {v3, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leco;->S:Lidh;

    invoke-static {v0}, Ldrm;->b(Lidh;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Leco;->S:Lidh;

    invoke-virtual {v2}, Lidh;->i()Lpbj;

    move-result-object v2

    iget-object v2, v2, Lpbj;->a:Lpbe;

    if-eqz v2, :cond_2

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, v2, Lpbe;->e:Ljava/lang/Boolean;

    invoke-virtual {v4, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Ledd;->b:Ledd;

    :goto_1
    new-instance v4, Ledh;

    invoke-direct {v4, v0, v2}, Ledh;-><init>(ZLedd;)V

    const-string v0, "dialog_check_in"

    invoke-virtual {v4, v3, v0}, Lt;->a(Lae;Ljava/lang/String;)V

    invoke-virtual {v4, p0, v1}, Lt;->a(Lu;I)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v4, p0, Leco;->S:Lidh;

    invoke-virtual {v4}, Lidh;->i()Lpbj;

    move-result-object v4

    iget-object v4, v4, Lpbj;->b:Lpbd;

    iget-object v4, v4, Lpbd;->b:Lpbc;

    iget-object v4, v4, Lpbc;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Ledd;->c:Ledd;

    goto :goto_1

    :cond_3
    sget-object v2, Ledd;->a:Ledd;

    goto :goto_1
.end method

.method static synthetic d(Leco;ILfib;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Leco;->c(ILfib;)V

    return-void
.end method

.method static synthetic e(Leco;)Leaq;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Leco;->af:Leaq;

    return-object v0
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 1458
    iget-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 1464
    :cond_0
    :goto_0
    return-void

    .line 1462
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    .line 1463
    invoke-direct {p0}, Leco;->aj()V

    goto :goto_0
.end method

.method private e(ILfib;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1594
    iget-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 1608
    :cond_0
    :goto_0
    return-void

    .line 1598
    :cond_1
    invoke-direct {p0}, Leco;->aj()V

    .line 1600
    const/4 v0, 0x0

    iput-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    .line 1602
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1603
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1604
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1606
    :cond_2
    invoke-direct {p0, v2}, Leco;->c(Z)V

    goto :goto_0
.end method

.method static synthetic e(Leco;ILfib;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Leco;->e(ILfib;)V

    return-void
.end method

.method static synthetic f(Leco;)Llnl;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Leco;->at:Llnl;

    return-object v0
.end method

.method private f(I)V
    .locals 3

    .prologue
    .line 1803
    const/4 v0, 0x0

    .line 1804
    invoke-virtual {p0, p1}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 1803
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 1806
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 1807
    return-void
.end method

.method static synthetic f(Leco;ILfib;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Leco;->a(ILfib;)V

    return-void
.end method

.method static synthetic g(Leco;)Lhee;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Leco;->P:Lhee;

    return-object v0
.end method

.method static synthetic h(Leco;)Lidh;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Leco;->S:Lidh;

    return-object v0
.end method

.method static synthetic i(Leco;)V
    .locals 4

    .prologue
    .line 147
    const v0, 0x7f0a094f

    invoke-direct {p0, v0}, Leco;->f(I)V

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->Q:Ljava/lang/String;

    iget-object v3, p0, Leco;->aa:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->g(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->aw:Ljava/lang/Integer;

    return-void
.end method

.method private i(Z)V
    .locals 3

    .prologue
    .line 1288
    if-eqz p1, :cond_0

    const v0, 0x7f0a094c

    :goto_0
    invoke-direct {p0, v0}, Leco;->f(I)V

    .line 1289
    iget-object v0, p0, Leco;->at:Llnl;

    iget-object v1, p0, Leco;->P:Lhee;

    .line 1290
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->W:Ljava/lang/String;

    .line 1289
    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/plus/service/EsService;->e(Landroid/content/Context;ILjava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    .line 1292
    return-void

    .line 1288
    :cond_0
    const v0, 0x7f0a0949

    goto :goto_0
.end method

.method static synthetic j(Leco;)Lhrt;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Leco;->aI:Lhrt;

    return-object v0
.end method

.method private j(Z)V
    .locals 4

    .prologue
    .line 1770
    iget-object v0, p0, Leco;->ag:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1776
    :goto_0
    return-void

    .line 1774
    :cond_0
    new-instance v0, Ledg;

    invoke-direct {v0, p0, p1}, Ledg;-><init>(Leco;Z)V

    iput-object v0, p0, Leco;->ag:Ljava/lang/Runnable;

    .line 1775
    iget-object v0, p0, Leco;->ag:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method static synthetic k(Leco;)Ledl;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Leco;->ac:Ledl;

    return-object v0
.end method

.method private k(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2231
    iget-object v0, p0, Leco;->af:Leaq;

    iget-boolean v0, v0, Leaq;->d:Z

    if-ne p1, v0, :cond_1

    .line 2244
    :cond_0
    :goto_0
    return-void

    .line 2235
    :cond_1
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    .line 2236
    if-eqz v0, :cond_0

    .line 2240
    iget-object v1, p0, Leco;->aH:Landroid/os/AsyncTask;

    if-eqz v1, :cond_2

    .line 2241
    iget-object v1, p0, Leco;->aH:Landroid/os/AsyncTask;

    invoke-virtual {v1, v5}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 2243
    :cond_2
    new-instance v1, Ledi;

    invoke-direct {v1, p0}, Ledi;-><init>(Leco;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Ledi;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Leco;->aH:Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 1616
    sget-object v0, Lhmw;->B:Lhmw;

    return-object v0
.end method

.method public K_()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Leco;->aL:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 318
    invoke-direct {p0}, Leco;->ah()V

    .line 319
    return-void
.end method

.method public U()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1663
    iget-object v3, p0, Leco;->S:Lidh;

    invoke-virtual {v3}, Lidh;->o()Lpao;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1664
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v6

    iget-object v3, p0, Leco;->S:Lidh;

    invoke-virtual {v3}, Lidh;->o()Lpao;

    move-result-object v3

    iget-object v4, v3, Lpao;->e:Loya;

    if-nez v4, :cond_1

    iget-object v4, v3, Lpao;->b:Ljava/lang/String;

    if-nez v4, :cond_1

    iget-object v4, v3, Lpao;->f:Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-direct {p0, v6, v3}, Leco;->a(Landroid/content/Context;Lpao;)V

    .line 1666
    :cond_0
    :goto_0
    return-void

    .line 1664
    :cond_1
    iget-object v4, v3, Lpao;->e:Loya;

    if-eqz v4, :cond_4

    iget-object v0, v3, Lpao;->e:Loya;

    sget-object v3, Lozb;->a:Loxr;

    invoke-virtual {v0, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lozb;

    iget-object v4, v0, Lozb;->c:Ljava/lang/Double;

    iget-object v3, v0, Lozb;->d:Ljava/lang/Double;

    iget-object v0, v0, Lozb;->b:Ljava/lang/String;

    move-object v5, v4

    move-object v4, v3

    move-object v3, v0

    :goto_1
    if-eqz v5, :cond_5

    if-eqz v4, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    :goto_3
    const-string v2, "http://maps.google.com/maps"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x2c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_2

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "daddr"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    :goto_4
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v6, v0}, Lisy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :cond_4
    move-object v3, v0

    move-object v4, v0

    move-object v5, v0

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    if-eqz v1, :cond_3

    const-string v0, "daddr"

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_4
.end method

.method public V()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1670
    const/4 v0, 0x0

    .line 1671
    iget-object v1, p0, Leco;->S:Lidh;

    invoke-virtual {v1}, Lidh;->g()I

    move-result v1

    .line 1672
    iget-object v2, p0, Leco;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 1673
    if-nez v1, :cond_1

    iget-object v3, p0, Leco;->S:Lidh;

    .line 1674
    invoke-virtual {v3}, Lidh;->i()Lpbj;

    move-result-object v3

    iget-object v3, v3, Lpbj;->g:Lltn;

    if-eqz v3, :cond_1

    .line 1675
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Leco;->at:Llnl;

    invoke-direct {v1, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->aC:Lhmv;

    .line 1676
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1675
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1677
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->S:Lidh;

    .line 1678
    invoke-virtual {v1}, Lidh;->i()Lpbj;

    move-result-object v1

    iget-object v1, v1, Lpbj;->g:Lltn;

    iget-object v1, v1, Lltn;->a:Ljava/lang/String;

    .line 1677
    invoke-static {v0, v2, v1}, Leyq;->g(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 1687
    :goto_0
    if-eqz v1, :cond_0

    .line 1689
    :try_start_0
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    invoke-static {v0, v1}, Leyq;->a(Landroid/content/Context;Landroid/content/Intent;)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1696
    :cond_0
    :goto_1
    return-void

    .line 1679
    :cond_1
    if-ne v1, v4, :cond_4

    .line 1680
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->S:Lidh;

    .line 1681
    invoke-virtual {v1}, Lidh;->q()Ljava/lang/String;

    move-result-object v1

    .line 1680
    const-string v3, "vnd.google.android.hangouts/vnd.google.android.hangout_on_air_whitelist"

    invoke-static {v0, v3, v2, v4}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v2, "hangout_external_key"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "hangout_external_key_type"

    const-string v2, "hoaevent"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1682
    :cond_2
    if-eqz v0, :cond_3

    .line 1683
    iput-boolean v4, p0, Leco;->ah:Z

    :cond_3
    move-object v1, v0

    goto :goto_0

    .line 1690
    :catch_0
    move-exception v0

    .line 1691
    const-string v2, "HostedEventFragment"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1692
    const-string v2, "HostedEventFragment"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot launch activity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public W()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1700
    .line 1701
    iget-object v0, p0, Leco;->S:Lidh;

    invoke-virtual {v0}, Lidh;->b()Lozp;

    move-result-object v6

    .line 1702
    iget-object v0, p0, Leco;->at:Llnl;

    const-string v1, "com.google.android.youtube"

    .line 1703
    invoke-static {v0, v1}, Lfug;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 1705
    iget-object v0, p0, Leco;->at:Llnl;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 1707
    if-eqz v6, :cond_5

    iget-object v1, v6, Lozp;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1708
    invoke-static {v6}, Ldrm;->c(Lozp;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1709
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1710
    const-string v3, "https://plus.google.com/hangouts/onair/watch?hl=%locale%&d=r&hid=hoaevent/"

    invoke-static {v3}, Litk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v6, Lozp;->g:Ljava/lang/String;

    .line 1711
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&ytl="

    .line 1712
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v6, Lozp;->m:Ljava/lang/String;

    .line 1713
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1714
    iget-object v3, v6, Lozp;->l:Lpbj;

    if-eqz v3, :cond_0

    iget-object v3, v6, Lozp;->l:Lpbj;

    iget-object v3, v3, Lpbj;->f:Lltl;

    if-eqz v3, :cond_0

    iget-object v3, v6, Lozp;->l:Lpbj;

    iget-object v3, v3, Lpbj;->f:Lltl;

    iget-object v3, v3, Lltl;->a:Ljava/lang/String;

    .line 1716
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v6, Lozp;->l:Lpbj;

    iget-object v3, v3, Lpbj;->f:Lltl;

    iget-object v3, v3, Lltl;->a:Ljava/lang/String;

    .line 1717
    invoke-static {v3}, Llnf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1720
    const-string v3, "&preroll="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v6, Lozp;->l:Lpbj;

    iget-object v4, v4, Lpbj;->f:Lltl;

    iget-object v4, v4, Lltl;->a:Ljava/lang/String;

    .line 1721
    invoke-static {v4}, Llnf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1725
    :cond_0
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v3

    .line 1727
    sget-object v1, Lhmv;->bf:Lhmv;

    .line 1728
    invoke-static {v6}, Ldrm;->a(Lozp;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1729
    invoke-static {v6}, Ldrm;->b(Lozp;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lhmv;->be:Lhmv;

    .line 1733
    :cond_1
    :goto_0
    new-instance v4, Lhmr;

    iget-object v6, p0, Leco;->at:Llnl;

    invoke-direct {v4, v6}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 1734
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1733
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    move v0, v2

    move-object v1, v3

    .line 1750
    :goto_1
    if-eqz v1, :cond_3

    .line 1751
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1752
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1753
    const/high16 v1, 0x80000

    invoke-virtual {v3, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1754
    if-eqz v0, :cond_2

    .line 1755
    const-string v0, "com.google.android.youtube"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1756
    iget-object v0, p0, Leco;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v3, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1757
    invoke-virtual {v3, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1761
    :cond_2
    :try_start_0
    invoke-virtual {p0, v3}, Leco;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1766
    :cond_3
    :goto_2
    return-void

    .line 1729
    :cond_4
    sget-object v1, Lhmv;->bd:Lhmv;

    goto :goto_0

    .line 1735
    :cond_5
    invoke-static {v6}, Ldrm;->a(Lozp;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1736
    iget-object v1, v6, Lozp;->m:Ljava/lang/String;

    invoke-static {v1}, Llnf;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1738
    invoke-static {v6}, Ldrm;->b(Lozp;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lhmv;->bb:Lhmv;

    .line 1741
    :goto_3
    new-instance v6, Lhmr;

    iget-object v7, p0, Leco;->at:Llnl;

    invoke-direct {v6, v7}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 1742
    invoke-virtual {v6, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1741
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    move v0, v3

    move-object v1, v4

    .line 1743
    goto :goto_1

    .line 1738
    :cond_6
    sget-object v1, Lhmv;->ba:Lhmv;

    goto :goto_3

    .line 1743
    :cond_7
    iget-object v1, p0, Leco;->S:Lidh;

    invoke-virtual {v1}, Lidh;->i()Lpbj;

    move-result-object v1

    iget-object v1, v1, Lpbj;->f:Lltl;

    if-eqz v1, :cond_8

    iget-object v1, p0, Leco;->S:Lidh;

    .line 1744
    invoke-virtual {v1}, Lidh;->i()Lpbj;

    move-result-object v1

    iget-object v1, v1, Lpbj;->f:Lltl;

    iget-object v1, v1, Lltl;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1745
    iget-object v1, p0, Leco;->S:Lidh;

    invoke-virtual {v1}, Lidh;->i()Lpbj;

    move-result-object v1

    iget-object v1, v1, Lpbj;->f:Lltl;

    iget-object v1, v1, Lltl;->a:Ljava/lang/String;

    .line 1746
    new-instance v4, Lhmr;

    iget-object v6, p0, Leco;->at:Llnl;

    invoke-direct {v4, v6}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v6, Lhmv;->bc:Lhmv;

    .line 1747
    invoke-virtual {v4, v6}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    .line 1746
    invoke-interface {v0, v4}, Lhms;->a(Lhmr;)V

    move v0, v3

    goto/16 :goto_1

    .line 1762
    :catch_0
    move-exception v0

    .line 1763
    const-string v1, "HostedEventFragment"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot launch activity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_8
    move v0, v3

    move-object v1, v5

    goto/16 :goto_1
.end method

.method public X()V
    .locals 5

    .prologue
    .line 1787
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aE:Lhmv;

    .line 1788
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1787
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1789
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1791
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Leco;->Q:Ljava/lang/String;

    iget-object v4, p0, Leco;->aa:Ljava/lang/String;

    iget-object v0, p0, Leco;->S:Lidh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->S:Lidh;

    .line 1792
    invoke-virtual {v0}, Lidh;->e()Ljava/lang/String;

    move-result-object v0

    .line 1790
    :goto_0
    invoke-static {v2, v1, v3, v4, v0}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Leco;->a(Landroid/content/Intent;)V

    .line 1793
    return-void

    .line 1792
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 2562
    iget-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l()Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 913
    const v0, 0x7f0400d7

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 914
    const v0, 0x7f100304

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 915
    new-instance v0, Lexq;

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Leco;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    iget-object v3, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-direct {v0, v2, v3, p0, p0}, Lexq;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Lexs;Lfxf;)V

    iput-object v0, p0, Leco;->ai:Lexq;

    .line 917
    iget-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v2, p0, Leco;->ai:Lexq;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 919
    invoke-virtual {p0}, Leco;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x2

    iget-object v3, p0, Leco;->ac:Ledl;

    invoke-virtual {v0, v2, v4, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 920
    invoke-virtual {p0}, Leco;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v5, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 921
    invoke-virtual {p0}, Leco;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 922
    invoke-virtual {p0}, Leco;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 924
    sget v0, Leco;->N:I

    if-nez v0, :cond_0

    .line 925
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0}, Llsc;->b(Landroid/util/DisplayMetrics;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 926
    const/16 v0, 0x10

    sput v0, Leco;->N:I

    .line 932
    :cond_0
    :goto_0
    invoke-direct {p0, v1}, Leco;->c(Landroid/view/View;)V

    .line 933
    return-object v1

    .line 928
    :cond_1
    const/16 v0, 0x8

    sput v0, Leco;->N:I

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 991
    const/4 v0, 0x0

    .line 992
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v2

    .line 993
    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v5

    .line 995
    packed-switch p1, :pswitch_data_0

    .line 1027
    :goto_0
    :pswitch_0
    return-object v0

    .line 997
    :pswitch_1
    new-instance v0, Lect;

    sget-object v3, Lidg;->a:Landroid/net/Uri;

    move-object v1, p0

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lect;-><init>(Leco;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;I)V

    goto :goto_0

    .line 1007
    :pswitch_2
    new-instance v0, Lecu;

    sget-object v3, Lidg;->a:Landroid/net/Uri;

    move-object v1, p0

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lecu;-><init>(Leco;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;I)V

    goto :goto_0

    .line 1017
    :pswitch_3
    new-instance v0, Lecv;

    sget-object v3, Lidg;->a:Landroid/net/Uri;

    move-object v1, p0

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lecv;-><init>(Leco;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;I)V

    goto :goto_0

    .line 995
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, -0x1

    .line 1344
    if-eq p2, v0, :cond_1

    .line 1345
    packed-switch p1, :pswitch_data_0

    .line 1395
    :cond_0
    :goto_0
    return-void

    .line 1347
    :pswitch_0
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aG:Lhmv;

    .line 1348
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1347
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0

    .line 1355
    :cond_1
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 1384
    :pswitch_1
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aF:Lhmv;

    .line 1385
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1384
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1386
    const-string v0, "extra_acl"

    .line 1387
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 1388
    new-instance v1, Lecy;

    invoke-direct {v1, p0, v0}, Lecy;-><init>(Leco;Lhgw;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1358
    :pswitch_2
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 1359
    const-string v0, "shareables"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1362
    if-eqz v2, :cond_3

    iget-object v0, p0, Leco;->af:Leaq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Leco;->af:Leaq;

    iget-boolean v0, v0, Leaq;->d:Z

    if-eqz v0, :cond_3

    .line 1365
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1366
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    .line 1367
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    .line 1369
    if-eqz v0, :cond_2

    .line 1370
    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    .line 1371
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1372
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1366
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 1378
    :cond_3
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0951

    const/4 v3, 0x1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ledn;

    iget-object v3, p0, Leco;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Leco;->Q:Ljava/lang/String;

    invoke-direct {v1, v0, v3, v4, v2}, Ledn;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_4

    new-array v0, v5, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ledn;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_4
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ledn;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1345
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    .line 1355
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2377
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2381
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, -0x80000000

    const/4 v3, 0x1

    .line 813
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 815
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 817
    iget-object v0, p0, Leco;->af:Leaq;

    iput-boolean v3, v0, Leaq;->i:Z

    .line 819
    if-eqz p1, :cond_b

    .line 820
    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leco;->Q:Ljava/lang/String;

    .line 821
    const-string v0, "typeid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Leco;->U:I

    .line 822
    const-string v0, "invitation_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leco;->Z:Ljava/lang/String;

    .line 823
    const-string v0, "incoming_rsvp_type"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leco;->ab:I

    .line 826
    const-string v0, "refresh"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Leco;->ar:Z

    .line 827
    const-string v0, "scroll_pos"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leco;->aB:I

    .line 828
    const-string v0, "first_timestamp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Leco;->aE:J

    .line 830
    const-string v0, "fetch_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    const-string v0, "fetch_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->an:Ljava/lang/Integer;

    .line 833
    :cond_0
    const-string v0, "comment_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 834
    const-string v0, "comment_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    .line 837
    :cond_1
    const-string v0, "invite_more_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 838
    const-string v0, "invite_more_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    .line 841
    :cond_2
    const-string v0, "rsvp_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 842
    const-string v0, "rsvp_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->am:Ljava/lang/Integer;

    .line 845
    :cond_3
    const-string v0, "set_comment_availability_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 846
    const-string v0, "set_comment_availability_id"

    .line 847
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    .line 850
    :cond_4
    const-string v0, "temp_rsvp_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 851
    const-string v0, "temp_rsvp_state"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leco;->al:I

    .line 855
    :cond_5
    const-string v0, "delete_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 856
    const-string v0, "delete_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->aw:Ljava/lang/Integer;

    .line 858
    :cond_6
    const-string v0, "abuse_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 859
    const-string v0, "abuse_request_id"

    .line 860
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    .line 863
    :cond_7
    const-string v0, "view_logged"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 864
    const-string v0, "view_logged"

    .line 865
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leco;->aA:Z

    .line 868
    :cond_8
    iget-boolean v0, p0, Leco;->aA:Z

    if-nez v0, :cond_9

    .line 869
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aI:Lhmv;

    .line 870
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 869
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 871
    iput-boolean v3, p0, Leco;->aA:Z

    .line 874
    :cond_9
    iget-object v0, p0, Leco;->af:Leaq;

    const-string v1, "expanded"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Leaq;->i:Z

    .line 875
    const-string v0, "fetching_newer"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Leco;->ae:Z

    .line 887
    :goto_0
    iget v0, p0, Leco;->ab:I

    if-eq v0, v4, :cond_a

    .line 888
    iget v0, p0, Leco;->ab:I

    invoke-virtual {p0, v0}, Leco;->d(I)V

    .line 889
    iput v4, p0, Leco;->ab:I

    .line 891
    :cond_a
    return-void

    .line 877
    :cond_b
    iget-object v0, p0, Leco;->Q:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 878
    iput-boolean v3, p0, Leco;->ar:Z

    goto :goto_0

    .line 882
    :cond_c
    iput-boolean v3, p0, Leco;->as:Z

    .line 883
    invoke-virtual {p0}, Leco;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Leco;->c(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2350
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    .line 2351
    const-string v1, "report_event"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2352
    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->W:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    .line 2354
    const v0, 0x7f0a055c

    invoke-direct {p0, v0}, Leco;->f(I)V

    .line 2360
    :cond_0
    :goto_0
    return-void

    .line 2355
    :cond_1
    const-string v1, "dialog_sync_disabled"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "dialog_backup_disabled"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2356
    :cond_2
    invoke-static {}, Lhqd;->b()V

    .line 2357
    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1}, Lhqd;->f(Landroid/content/Context;I)V

    .line 2358
    invoke-direct {p0, v2, v2}, Leco;->a(ZZ)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1305
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/4 v6, 0x4

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1032
    iget-object v2, p0, Leco;->ai:Lexq;

    const-string v3, "OLF"

    invoke-virtual {v2, v3}, Lexq;->i(Ljava/lang/String;)V

    .line 1034
    invoke-virtual {p1}, Ldo;->o()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1165
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1036
    :pswitch_1
    iput-boolean v1, p0, Leco;->aD:Z

    .line 1037
    iput-boolean v0, p0, Leco;->az:Z

    .line 1038
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1039
    const/16 v2, 0x8

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Leco;->V:I

    .line 1041
    const/4 v2, 0x2

    invoke-static {p2, v0, v2}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;

    move-result-object v2

    iput-object v2, p0, Leco;->S:Lidh;

    .line 1044
    iget-object v2, p0, Leco;->S:Lidh;

    invoke-static {v2}, Ldrm;->e(Lidh;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1045
    invoke-direct {p0, v0}, Leco;->j(Z)V

    .line 1050
    :goto_1
    const/4 v2, 0x3

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 1052
    if-eqz v2, :cond_1

    .line 1053
    invoke-static {v2}, Llah;->a([B)Llah;

    move-result-object v2

    iput-object v2, p0, Leco;->T:Llah;

    .line 1056
    :cond_1
    iget-object v2, p0, Leco;->S:Lidh;

    invoke-virtual {v2}, Lidh;->e()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Leco;->R:Ljava/lang/String;

    .line 1057
    iget-object v2, p0, Leco;->S:Lidh;

    invoke-virtual {v2}, Lidh;->h()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Leco;->aa:Ljava/lang/String;

    .line 1058
    const/4 v2, 0x7

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_7

    :goto_2
    iput-boolean v0, p0, Leco;->X:Z

    .line 1060
    iget-object v0, p0, Leco;->af:Leaq;

    iget-boolean v0, v0, Leaq;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Leco;->af:Leaq;

    iget-boolean v0, v0, Leaq;->f:Z

    if-eqz v0, :cond_2

    .line 1061
    invoke-virtual {p0, v1}, Leco;->a(Z)V

    .line 1064
    :cond_2
    iget-object v0, p0, Leco;->ai:Lexq;

    iget-object v2, p0, Leco;->af:Leaq;

    invoke-virtual {v0, p2, v2}, Lexq;->a(Landroid/database/Cursor;Leaq;)V

    .line 1069
    iget v0, p0, Leco;->aB:I

    if-eq v0, v5, :cond_3

    iget-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Leco;->ai:Lexq;

    .line 1070
    invoke-virtual {v0}, Lexq;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1071
    iget-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget v2, p0, Leco;->aB:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f(I)V

    .line 1072
    iput v5, p0, Leco;->aB:I

    .line 1074
    :cond_3
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leco;->aj:Ljava/lang/String;

    .line 1075
    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leco;->ak:Ljava/lang/String;

    .line 1076
    const/4 v0, 0x6

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leco;->W:Ljava/lang/String;

    .line 1077
    iget-object v0, p0, Leco;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 1079
    iget-boolean v0, p0, Leco;->ar:Z

    if-eqz v0, :cond_4

    .line 1080
    invoke-direct {p0, v1}, Leco;->c(Z)V

    .line 1083
    :cond_4
    invoke-direct {p0}, Leco;->al()V

    .line 1099
    :goto_3
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    .line 1100
    instance-of v1, v0, Lcom/google/android/apps/plus/phone/EventActivity;

    if-eqz v1, :cond_5

    .line 1101
    check-cast v0, Lcom/google/android/apps/plus/phone/EventActivity;

    .line 1102
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EventActivity;->l()I

    move-result v1

    .line 1104
    packed-switch v1, :pswitch_data_1

    .line 1130
    :cond_5
    :goto_4
    invoke-virtual {p0}, Leco;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Leco;->c(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1047
    :cond_6
    invoke-direct {p0}, Leco;->ai()V

    goto/16 :goto_1

    :cond_7
    move v0, v1

    .line 1058
    goto :goto_2

    .line 1085
    :cond_8
    iget-object v2, p0, Leco;->ai:Lexq;

    iget-object v3, p0, Leco;->af:Leaq;

    invoke-virtual {v2, v4, v3}, Lexq;->a(Landroid/database/Cursor;Leaq;)V

    .line 1088
    iget-boolean v2, p0, Leco;->as:Z

    if-eqz v2, :cond_9

    .line 1089
    iput-object v4, p0, Leco;->S:Lidh;

    .line 1090
    iput-boolean v1, p0, Leco;->az:Z

    .line 1091
    iput-object v4, p0, Leco;->an:Ljava/lang/Integer;

    .line 1092
    iput-object v4, p0, Leco;->am:Ljava/lang/Integer;

    .line 1093
    iput-boolean v0, p0, Leco;->ay:Z

    goto :goto_3

    .line 1095
    :cond_9
    invoke-direct {p0, v1}, Leco;->c(Z)V

    goto :goto_3

    .line 1106
    :pswitch_2
    new-instance v0, Lecw;

    invoke-direct {v0, p0, v1}, Lecw;-><init>(Leco;I)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_4

    .line 1123
    :pswitch_3
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EventActivity;->a(I)V

    .line 1124
    invoke-direct {p0}, Leco;->ae()V

    goto :goto_4

    .line 1134
    :pswitch_4
    iget-object v0, p0, Leco;->ai:Lexq;

    invoke-virtual {v0, p2}, Lexq;->a(Landroid/database/Cursor;)V

    .line 1136
    if-nez p2, :cond_a

    .line 1137
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leco;->aE:J

    goto/16 :goto_0

    .line 1138
    :cond_a
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1142
    iget-wide v2, p0, Leco;->aE:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 1143
    iput-wide v0, p0, Leco;->aE:J

    .line 1144
    iget-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h()V

    goto/16 :goto_0

    .line 1150
    :pswitch_5
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1153
    if-eqz p2, :cond_b

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1154
    :goto_5
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1155
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1156
    const/4 v3, 0x3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1157
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1159
    new-instance v5, Ldrx;

    .line 1160
    invoke-static {v4}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v3, v4}, Ldrx;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    invoke-virtual {v1, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 1164
    :cond_b
    iget-object v0, p0, Leco;->ai:Lexq;

    invoke-virtual {v0, v1}, Lexq;->a(Ljava/util/HashMap;)V

    goto/16 :goto_0

    .line 1034
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 1104
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 147
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Leco;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lfyc;)V
    .locals 3

    .prologue
    .line 2390
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v0

    .line 2392
    const-string v1, "update_card"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2400
    :goto_0
    return-void

    .line 2396
    :cond_0
    invoke-static {}, Leba;->U()Leba;

    move-result-object v1

    .line 2397
    invoke-virtual {v1, p1}, Leba;->a(Lfyc;)V

    .line 2398
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Leba;->a(Lu;I)V

    .line 2399
    const-string v2, "update_card"

    invoke-virtual {v1, v0, v2}, Leba;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lhjk;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 943
    const v0, 0x7f10067b

    .line 944
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 945
    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 947
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    .line 948
    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 949
    const-string v3, "is_google_plus"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    .line 950
    if-eqz v6, :cond_5

    iget-object v0, p0, Leco;->W:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Leco;->S:Lidh;

    .line 952
    invoke-static {v0, v5}, Ldrm;->a(Lidh;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v4, v1

    .line 954
    :goto_0
    if-eqz v6, :cond_6

    iget-boolean v0, p0, Leco;->X:Z

    if-eqz v0, :cond_6

    move v3, v1

    .line 957
    :goto_1
    if-eqz v6, :cond_7

    iget-object v0, p0, Leco;->S:Lidh;

    if-eqz v0, :cond_7

    iget-object v0, p0, Leco;->S:Lidh;

    .line 959
    invoke-virtual {v0}, Lidh;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 961
    :goto_2
    if-eqz v6, :cond_8

    if-nez v0, :cond_8

    iget-object v5, p0, Leco;->W:Ljava/lang/String;

    if-eqz v5, :cond_8

    iget-object v5, p0, Leco;->aq:Ljava/lang/Integer;

    if-nez v5, :cond_8

    .line 966
    :goto_3
    if-eqz v4, :cond_0

    .line 967
    const v2, 0x7f10067d

    invoke-interface {p1, v2}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 970
    :cond_0
    if-eqz v3, :cond_1

    .line 971
    const v2, 0x7f10067e

    invoke-interface {p1, v2}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 974
    :cond_1
    if-eqz v0, :cond_2

    .line 975
    const v0, 0x7f1006b9

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 976
    const v0, 0x7f1006bd

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 977
    if-eqz v3, :cond_9

    const v0, 0x7f1006ba

    :goto_4
    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 980
    :cond_2
    if-eqz v6, :cond_3

    iget-object v0, p0, Leco;->af:Leaq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Leco;->af:Leaq;

    iget-boolean v0, v0, Leaq;->j:Z

    if-eqz v0, :cond_3

    .line 981
    const v0, 0x7f1006bf

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 984
    :cond_3
    if-eqz v1, :cond_4

    .line 985
    const v0, 0x7f1006e0

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 987
    :cond_4
    return-void

    :cond_5
    move v4, v2

    .line 952
    goto :goto_0

    :cond_6
    move v3, v2

    .line 954
    goto :goto_1

    :cond_7
    move v0, v2

    .line 959
    goto :goto_2

    :cond_8
    move v1, v2

    .line 961
    goto :goto_3

    .line 977
    :cond_9
    const v0, 0x7f1006bb

    goto :goto_4
.end method

.method public a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1416
    iget-object v0, p0, Leco;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1426
    :cond_0
    :goto_0
    return-void

    .line 1420
    :cond_1
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aO:Lhmv;

    .line 1421
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1420
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1423
    const v0, 0x7f0a0929

    invoke-direct {p0, v0}, Leco;->f(I)V

    .line 1424
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->W:Ljava/lang/String;

    iget-object v3, p0, Leco;->Q:Ljava/lang/String;

    iget-object v4, p0, Leco;->aa:Ljava/lang/String;

    iget-object v6, p0, Leco;->aF:[B

    move-object v5, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 1469
    const-string v0, "EventPlusOneTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1470
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1471
    invoke-virtual {p0}, Leco;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1474
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2467
    iget-object v0, p0, Leco;->at:Llnl;

    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;J)I

    .line 2468
    return-void
.end method

.method public a(Lnzx;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 2104
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 2105
    new-instance v2, Ldew;

    .line 2106
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 2107
    const/4 v0, 0x3

    new-array v3, v8, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Leco;->R:Ljava/lang/String;

    iget-object v6, p0, Leco;->Q:Ljava/lang/String;

    const-string v7, "PLUS_EVENT"

    .line 2108
    invoke-static {v1, v5, v6, v7}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 2107
    invoke-static {v0, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2112
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 2113
    if-eqz v0, :cond_0

    iget-object v4, v0, Lnzu;->b:Lnym;

    if-eqz v4, :cond_0

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v0}, Ldew;->b(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 2115
    invoke-virtual {v0, v3}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 2116
    invoke-virtual {v0, v8}, Ldew;->f(Z)Ldew;

    move-result-object v0

    .line 2117
    invoke-virtual {v0, v8}, Ldew;->h(Z)Ldew;

    .line 2119
    invoke-virtual {v2}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Leco;->a(Landroid/content/Intent;)V

    .line 2120
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->dZ:Lhmv;

    .line 2121
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2120
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2122
    return-void

    :cond_0
    move-object v0, v1

    .line 2113
    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 938
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 939
    return-void
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 1819
    if-eqz p1, :cond_1

    .line 1820
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    .line 1822
    iget-object v1, p0, Leco;->aG:Landroid/os/AsyncTask;

    if-eqz v1, :cond_0

    .line 1823
    iget-object v1, p0, Leco;->aG:Landroid/os/AsyncTask;

    invoke-virtual {v1, v5}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 1825
    :cond_0
    new-instance v1, Lecz;

    invoke-direct {v1, p0}, Lecz;-><init>(Leco;)V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    iget-object v0, p0, Leco;->P:Lhee;

    .line 1869
    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v5

    const/4 v0, 0x2

    iget-object v3, p0, Leco;->P:Lhee;

    .line 1870
    invoke-interface {v3}, Lhee;->g()Lhej;

    move-result-object v3

    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1868
    invoke-virtual {v1, v2}, Lecz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Leco;->aG:Landroid/os/AsyncTask;

    .line 1875
    :goto_0
    iput-boolean v5, p0, Leco;->Y:Z

    .line 1876
    return-void

    .line 1872
    :cond_1
    invoke-direct {p0, v3}, Leco;->k(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1239
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1240
    const v3, 0x7f10067b

    if-ne v0, v3, :cond_0

    .line 1241
    invoke-direct {p0}, Leco;->ah()V

    move v0, v1

    .line 1284
    :goto_0
    return v0

    .line 1243
    :cond_0
    const v3, 0x7f10067d

    if-ne v0, v3, :cond_1

    .line 1244
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leco;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->aH:Lhmv;

    .line 1245
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1244
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 1246
    invoke-direct {p0}, Leco;->ae()V

    move v0, v1

    .line 1247
    goto :goto_0

    .line 1248
    :cond_1
    const v3, 0x7f10067e

    if-ne v0, v3, :cond_2

    .line 1249
    invoke-direct {p0}, Leco;->af()V

    move v0, v1

    .line 1250
    goto :goto_0

    .line 1251
    :cond_2
    const v3, 0x7f1006b9

    if-ne v0, v3, :cond_3

    .line 1252
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leco;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->aK:Lhmv;

    .line 1253
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1252
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 1254
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1256
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Leco;->Q:Ljava/lang/String;

    iget-object v4, p0, Leco;->aa:Ljava/lang/String;

    .line 1255
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/apps/plus/phone/EditEventActivity;

    invoke-direct {v5, v2, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v5, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account_id"

    invoke-virtual {v5, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "event_id"

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "auth_key"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1257
    invoke-virtual {p0, v5}, Leco;->a(Landroid/content/Intent;)V

    move v0, v1

    .line 1258
    goto :goto_0

    .line 1259
    :cond_3
    const v3, 0x7f1006bd

    if-ne v0, v3, :cond_4

    .line 1260
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leco;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->aJ:Lhmv;

    .line 1261
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 1260
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 1262
    new-instance v0, Ledb;

    invoke-direct {v0}, Ledb;-><init>()V

    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v3

    const-string v4, "delete_event_conf"

    invoke-virtual {v0, v3, v4}, Ledb;->a(Lae;Ljava/lang/String;)V

    invoke-virtual {v0, p0, v2}, Ledb;->a(Lu;I)V

    move v0, v1

    .line 1263
    goto/16 :goto_0

    .line 1264
    :cond_4
    const v3, 0x7f1006bf

    if-ne v0, v3, :cond_5

    .line 1265
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leco;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->aD:Lhmv;

    .line 1266
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1265
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 1267
    invoke-direct {p0}, Leco;->ag()V

    move v0, v1

    .line 1268
    goto/16 :goto_0

    .line 1269
    :cond_5
    const v3, 0x7f1006bb

    if-ne v0, v3, :cond_6

    .line 1271
    invoke-direct {p0, v1}, Leco;->i(Z)V

    move v0, v1

    .line 1272
    goto/16 :goto_0

    .line 1273
    :cond_6
    const v3, 0x7f1006ba

    if-ne v0, v3, :cond_7

    .line 1275
    invoke-direct {p0, v2}, Leco;->i(Z)V

    move v0, v1

    .line 1276
    goto/16 :goto_0

    .line 1277
    :cond_7
    const v3, 0x7f1006e0

    if-ne v0, v3, :cond_8

    .line 1278
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leco;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->aL:Lhmv;

    .line 1279
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 1278
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 1280
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leco;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->ab:Lhmv;

    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    const v0, 0x7f0a0550

    invoke-virtual {p0, v0}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0a0776

    invoke-virtual {p0, v3}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0596

    invoke-virtual {p0, v4}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0a0597

    invoke-virtual {p0, v5}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Llgr;->a(Lu;I)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "activity_id"

    iget-object v4, p0, Leco;->W:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v2

    const-string v3, "report_event"

    invoke-virtual {v0, v2, v3}, Llgr;->a(Lae;Ljava/lang/String;)V

    move v0, v1

    .line 1281
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 1284
    goto/16 :goto_0
.end method

.method public aO_()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 661
    invoke-super {p0}, Llol;->aO_()V

    .line 662
    iget-object v0, p0, Leco;->aN:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 664
    iget-object v0, p0, Leco;->ai:Lexq;

    const-string v1, "OR"

    invoke-virtual {v0, v1}, Lexq;->i(Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Leco;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Leco;->an:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 668
    iget-object v0, p0, Leco;->an:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 669
    iget-object v1, p0, Leco;->an:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leco;->b(ILfib;)V

    .line 670
    iput-object v2, p0, Leco;->an:Ljava/lang/Integer;

    .line 674
    :cond_0
    iget-object v0, p0, Leco;->am:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 675
    iget-object v0, p0, Leco;->am:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 676
    iget-object v0, p0, Leco;->am:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 677
    iget-object v1, p0, Leco;->am:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leco;->c(ILfib;)V

    .line 678
    iput-object v2, p0, Leco;->am:Ljava/lang/Integer;

    .line 682
    :cond_1
    iget-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 683
    iget-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 684
    iget-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 685
    iget-object v1, p0, Leco;->ao:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leco;->d(ILfib;)V

    .line 686
    iput-object v2, p0, Leco;->ao:Ljava/lang/Integer;

    .line 690
    :cond_2
    iget-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 691
    iget-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 692
    iget-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 693
    iget-object v1, p0, Leco;->ap:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leco;->e(ILfib;)V

    .line 694
    iput-object v2, p0, Leco;->ap:Ljava/lang/Integer;

    .line 698
    :cond_3
    iget-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 699
    iget-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 700
    iget-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 701
    iget-object v1, p0, Leco;->aq:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leco;->a(ILfib;)V

    .line 702
    iput-object v2, p0, Leco;->aq:Ljava/lang/Integer;

    .line 706
    :cond_4
    iget-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 707
    iget-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 708
    iget-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    .line 709
    iget-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Leco;->e(I)V

    .line 710
    iput-object v2, p0, Leco;->ax:Ljava/lang/Integer;

    .line 714
    :cond_5
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Leco;->at:Llnl;

    .line 715
    invoke-static {v1}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Leco;->aM:Landroid/database/ContentObserver;

    .line 714
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 717
    iget-object v0, p0, Leco;->S:Lidh;

    invoke-static {v0}, Ldrm;->e(Lidh;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 718
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Leco;->j(Z)V

    .line 721
    :cond_6
    iget-boolean v0, p0, Leco;->ah:Z

    if-eqz v0, :cond_7

    .line 722
    iput-boolean v3, p0, Leco;->ah:Z

    .line 723
    invoke-direct {p0, v3}, Leco;->j(Z)V

    .line 727
    :cond_7
    invoke-virtual {p0}, Leco;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1000a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfxs;

    .line 728
    if-eqz v0, :cond_8

    .line 729
    invoke-virtual {v0}, Lfxs;->e()V

    .line 730
    invoke-virtual {v0}, Lfxs;->b()V

    .line 733
    :cond_8
    return-void
.end method

.method public aQ_()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Leco;->as:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Leco;->an:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Leco;->az:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aa()V
    .locals 0

    .prologue
    .line 2404
    invoke-direct {p0}, Leco;->ag()V

    .line 2405
    return-void
.end method

.method public ab()V
    .locals 0

    .prologue
    .line 2409
    invoke-direct {p0}, Leco;->ae()V

    .line 2410
    return-void
.end method

.method public ac()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 2439
    iget-object v0, p0, Leco;->aJ:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2440
    iget-object v0, p0, Leco;->at:Llnl;

    iget-object v1, p0, Leco;->aJ:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Llnl;->startActivity(Landroid/content/Intent;)V

    .line 2463
    :cond_0
    :goto_0
    return-void

    .line 2443
    :cond_1
    iget-object v0, p0, Leco;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->W:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2444
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lhoc;

    .line 2445
    iget-object v0, p0, Leco;->T:Llah;

    if-eqz v0, :cond_2

    iget-object v0, p0, Leco;->T:Llah;

    invoke-virtual {v0}, Llah;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2446
    new-instance v0, Ldot;

    iget-object v1, p0, Leco;->at:Llnl;

    iget-object v2, p0, Leco;->P:Lhee;

    .line 2447
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Leco;->W:Ljava/lang/String;

    move v5, v4

    invoke-direct/range {v0 .. v5}, Ldot;-><init>(Landroid/content/Context;ILjava/lang/String;ZZ)V

    .line 2446
    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    goto :goto_0

    .line 2450
    :cond_2
    iget-object v0, p0, Leco;->S:Lidh;

    invoke-virtual {v0}, Lidh;->i()Lpbj;

    move-result-object v0

    .line 2451
    if-eqz v0, :cond_3

    iget-object v1, v0, Lpbj;->b:Lpbd;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lpbj;->b:Lpbd;

    iget-object v1, v1, Lpbd;->b:Lpbc;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lpbj;->b:Lpbd;

    iget-object v0, v0, Lpbd;->b:Lpbc;

    iget-object v0, v0, Lpbc;->a:Ljava/lang/Boolean;

    .line 2453
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v4, v7

    .line 2455
    :cond_3
    iget-object v0, p0, Leco;->at:Llnl;

    .line 2456
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v1

    iget-object v2, p0, Leco;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const-string v3, "plus_one_promo"

    .line 2455
    invoke-static {v0, v1, v2, v4, v3}, Lfva;->a(Landroid/content/Context;Lae;IZLjava/lang/String;)Z

    move-result v5

    .line 2458
    new-instance v0, Ldot;

    iget-object v1, p0, Leco;->at:Llnl;

    iget-object v2, p0, Leco;->P:Lhee;

    .line 2459
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Leco;->W:Ljava/lang/String;

    move v4, v7

    invoke-direct/range {v0 .. v5}, Ldot;-><init>(Landroid/content/Context;ILjava/lang/String;ZZ)V

    .line 2458
    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2364
    const-string v0, "dialog_sync_disabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2365
    invoke-direct {p0, v1, v1}, Leco;->a(ZZ)V

    .line 2369
    :goto_0
    return-void

    .line 2366
    :cond_0
    const-string v0, "dialog_backup_disabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1797
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1799
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v1

    const/4 v2, 0x0

    .line 1798
    invoke-static {v1, v0, p1, v2}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Leco;->a(Landroid/content/Intent;)V

    .line 1800
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 2385
    iget-object v0, p0, Leco;->af:Leaq;

    iput-boolean p1, v0, Leaq;->i:Z

    .line 2386
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 1172
    iget-boolean v0, p0, Leco;->aD:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Leco;->ak:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Leco;->ay:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    if-nez v0, :cond_1

    .line 1179
    :cond_0
    :goto_0
    return-void

    .line 1176
    :cond_1
    iget-object v0, p0, Leco;->ai:Lexq;

    invoke-virtual {v0}, Lexq;->getCount()I

    move-result v0

    sget v1, Leco;->N:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    .line 1177
    const/4 v0, 0x1

    iput-boolean v0, p0, Leco;->aD:Z

    iget-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v1, Lecx;

    invoke-direct {v1, p0}, Lecx;-><init>(Leco;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 802
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 803
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 804
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leco;->P:Lhee;

    .line 805
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhrt;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrt;

    iput-object v0, p0, Leco;->aI:Lhrt;

    .line 806
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 807
    new-instance v1, Liwk;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2, v0}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v0, Lixj;

    .line 808
    invoke-virtual {v1, v0}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Leco;->aJ:Liwk;

    .line 809
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2373
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2434
    iget-object v0, p0, Leco;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    const-string v0, "https://plus.google.com/s/%23"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "#"

    const/16 v0, 0x1d

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Leco;->at:Llnl;

    invoke-static {v2, v1, v0}, Leyq;->i(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Leco;->a(Landroid/content/Intent;)V

    .line 2435
    :goto_1
    return-void

    .line 2434
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Ljpf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Ljpf;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "extra_gaia_id"

    invoke-static {v2, v0}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v0, p0, Leco;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leco;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->C:Lhmv;

    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    invoke-virtual {v3, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v2

    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    :cond_2
    iget-object v0, p0, Leco;->at:Llnl;

    iget-object v2, p0, Leco;->W:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2}, Litm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Leco;->aL:Lkci;

    invoke-virtual {v0}, Lkci;->f()Z

    move-result v0

    return v0
.end method

.method public d(I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/high16 v6, -0x80000000

    .line 1621
    iget-object v0, p0, Leco;->S:Lidh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->S:Lidh;

    invoke-static {v0}, Ldrm;->a(Lidh;)I

    move-result v0

    if-eq p1, v0, :cond_3

    .line 1622
    :cond_0
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leco;->Q:Ljava/lang/String;

    iget-object v3, p0, Leco;->aa:Ljava/lang/String;

    iget-object v5, p0, Leco;->aF:[B

    move v4, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I[B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leco;->am:Ljava/lang/Integer;

    .line 1624
    iput p1, p0, Leco;->al:I

    .line 1626
    if-ne p1, v7, :cond_4

    .line 1627
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->az:Lhmv;

    .line 1628
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1627
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1637
    :cond_1
    :goto_0
    iget-object v0, p0, Leco;->S:Lidh;

    if-eqz v0, :cond_2

    .line 1638
    invoke-direct {p0}, Leco;->al()V

    .line 1639
    invoke-direct {p0}, Leco;->ak()V

    .line 1642
    :cond_2
    iget v0, p0, Leco;->ab:I

    if-eq v0, v6, :cond_3

    .line 1643
    const/4 v0, 0x0

    const v1, 0x7f0a074c

    .line 1644
    invoke-virtual {p0, v1}, Leco;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 1643
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 1645
    invoke-virtual {p0}, Leco;->p()Lae;

    move-result-object v1

    const-string v2, "send_rsvp"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 1646
    iput v6, p0, Leco;->ab:I

    .line 1650
    :cond_3
    iput-boolean v7, p0, Leco;->Y:Z

    .line 1651
    return-void

    .line 1629
    :cond_4
    const/4 v0, 0x6

    if-ne p1, v0, :cond_5

    .line 1630
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aB:Lhmv;

    .line 1631
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1630
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0

    .line 1632
    :cond_5
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 1633
    iget-object v0, p0, Leco;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leco;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aA:Lhmv;

    .line 1634
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1633
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 1656
    iget-object v0, p0, Leco;->S:Lidh;

    invoke-virtual {v0}, Lidh;->o()Lpao;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1657
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leco;->S:Lidh;

    invoke-virtual {v1}, Lidh;->o()Lpao;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Leco;->a(Landroid/content/Context;Lpao;)V

    .line 1659
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 756
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 758
    const-string v0, "id"

    iget-object v1, p0, Leco;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    const-string v0, "typeid"

    iget v1, p0, Leco;->U:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 760
    const-string v0, "invitation_token"

    iget-object v1, p0, Leco;->Z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    const-string v0, "incoming_rsvp_type"

    iget v1, p0, Leco;->ab:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 762
    const-string v0, "refresh"

    iget-boolean v1, p0, Leco;->ar:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 763
    const-string v0, "expanded"

    iget-object v1, p0, Leco;->af:Leaq;

    iget-boolean v1, v1, Leaq;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 764
    const-string v0, "view_logged"

    iget-boolean v1, p0, Leco;->aA:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 765
    const-string v0, "first_timestamp"

    iget-wide v2, p0, Leco;->aE:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 767
    iget-object v0, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    if-eqz v0, :cond_0

    .line 768
    const-string v0, "scroll_pos"

    iget-object v1, p0, Leco;->aC:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 770
    :cond_0
    iget-object v0, p0, Leco;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 771
    const-string v0, "fetch_req_id"

    iget-object v1, p0, Leco;->an:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 773
    :cond_1
    iget-object v0, p0, Leco;->am:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 774
    const-string v0, "rsvp_req_id"

    iget-object v1, p0, Leco;->am:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 776
    :cond_2
    const-string v0, "temp_rsvp_state"

    iget v1, p0, Leco;->al:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 777
    iget-object v0, p0, Leco;->ao:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 778
    const-string v0, "comment_req_id"

    iget-object v1, p0, Leco;->ao:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 781
    :cond_3
    iget-object v0, p0, Leco;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 782
    const-string v0, "invite_more_req_id"

    iget-object v1, p0, Leco;->ap:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 785
    :cond_4
    iget-object v0, p0, Leco;->aw:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 786
    const-string v0, "delete_req_id"

    iget-object v1, p0, Leco;->aw:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 789
    :cond_5
    iget-object v0, p0, Leco;->aq:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 790
    const-string v0, "abuse_request_id"

    iget-object v1, p0, Leco;->aq:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 793
    :cond_6
    iget-object v0, p0, Leco;->ax:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 794
    const-string v0, "set_comment_availability_id"

    iget-object v1, p0, Leco;->ax:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 797
    :cond_7
    iget-object v0, p0, Leco;->ai:Lexq;

    const-string v1, "ON"

    invoke-virtual {v0, v1}, Lexq;->i(Ljava/lang/String;)V

    .line 798
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 642
    .line 643
    invoke-virtual {p0}, Leco;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1000a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfxs;

    .line 645
    if-eqz v0, :cond_0

    .line 646
    invoke-virtual {v0}, Lfxs;->d()V

    .line 648
    :cond_0
    iget-object v0, p0, Leco;->aG:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 649
    iget-object v0, p0, Leco;->aG:Landroid/os/AsyncTask;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 650
    iput-object v3, p0, Leco;->aG:Landroid/os/AsyncTask;

    .line 652
    :cond_1
    iget-object v0, p0, Leco;->aH:Landroid/os/AsyncTask;

    if-eqz v0, :cond_2

    .line 653
    iget-object v0, p0, Leco;->aH:Landroid/os/AsyncTask;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 654
    iput-object v3, p0, Leco;->aH:Landroid/os/AsyncTask;

    .line 656
    :cond_2
    invoke-super {p0}, Llol;->h()V

    .line 657
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 628
    invoke-virtual {p0}, Leco;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Leco;->aM:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 629
    iget-object v0, p0, Leco;->aN:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 631
    invoke-virtual {p0}, Leco;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1000a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfxs;

    .line 633
    if-eqz v0, :cond_0

    .line 634
    invoke-virtual {v0}, Lfxs;->c()V

    .line 636
    :cond_0
    invoke-direct {p0}, Leco;->ai()V

    .line 637
    invoke-super {p0}, Llol;->z()V

    .line 638
    return-void
.end method
