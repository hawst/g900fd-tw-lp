.class public final Loks;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/String;

.field private b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 989
    invoke-direct {p0}, Loxq;-><init>()V

    .line 992
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loks;->a:[Ljava/lang/String;

    .line 995
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loks;->b:[Ljava/lang/String;

    .line 989
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1016
    .line 1017
    iget-object v0, p0, Loks;->a:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Loks;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1019
    iget-object v3, p0, Loks;->a:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 1021
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1019
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1024
    :cond_0
    iget-object v0, p0, Loks;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 1026
    :goto_1
    iget-object v2, p0, Loks;->b:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Loks;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1028
    iget-object v3, p0, Loks;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 1030
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1028
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1032
    :cond_1
    add-int/2addr v0, v2

    .line 1033
    iget-object v1, p0, Loks;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1035
    :cond_2
    iget-object v1, p0, Loks;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1036
    iput v0, p0, Loks;->ai:I

    .line 1037
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Loks;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1045
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1046
    sparse-switch v0, :sswitch_data_0

    .line 1050
    iget-object v1, p0, Loks;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1051
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loks;->ah:Ljava/util/List;

    .line 1054
    :cond_1
    iget-object v1, p0, Loks;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1056
    :sswitch_0
    return-object p0

    .line 1061
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1062
    iget-object v0, p0, Loks;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 1063
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1064
    iget-object v2, p0, Loks;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1065
    iput-object v1, p0, Loks;->a:[Ljava/lang/String;

    .line 1066
    :goto_1
    iget-object v1, p0, Loks;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1067
    iget-object v1, p0, Loks;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1068
    invoke-virtual {p1}, Loxn;->a()I

    .line 1066
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1071
    :cond_2
    iget-object v1, p0, Loks;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1075
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1076
    iget-object v0, p0, Loks;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 1077
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1078
    iget-object v2, p0, Loks;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1079
    iput-object v1, p0, Loks;->b:[Ljava/lang/String;

    .line 1080
    :goto_2
    iget-object v1, p0, Loks;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 1081
    iget-object v1, p0, Loks;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1082
    invoke-virtual {p1}, Loxn;->a()I

    .line 1080
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1085
    :cond_3
    iget-object v1, p0, Loks;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1046
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1000
    iget-object v1, p0, Loks;->a:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1001
    iget-object v2, p0, Loks;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1002
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1001
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1005
    :cond_0
    iget-object v1, p0, Loks;->b:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1006
    iget-object v1, p0, Loks;->b:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1007
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 1006
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1010
    :cond_1
    iget-object v0, p0, Loks;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1012
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 985
    invoke-virtual {p0, p1}, Loks;->a(Loxn;)Loks;

    move-result-object v0

    return-object v0
.end method
