.class public final Lkxl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lhym;

.field private final d:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkxl;-><init>(Ljava/lang/Exception;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Lhym;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lkxl;->d:Ljava/lang/Exception;

    .line 27
    iput-object p1, p0, Lkxl;->c:Lhym;

    .line 28
    iput-object p2, p0, Lkxl;->a:Ljava/lang/String;

    .line 31
    invoke-static {p2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p3, v0

    :cond_0
    iput-object p3, p0, Lkxl;->b:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lhym;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lkxl;->c:Lhym;

    .line 36
    iput-object v2, p0, Lkxl;->a:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lkxl;->b:Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lkxl;->d:Ljava/lang/Exception;

    .line 39
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lkxl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lkxl;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lhym;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lkxl;->c:Lhym;

    return-object v0
.end method

.method public d()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lkxl;->d:Ljava/lang/Exception;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lkxl;->d:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
