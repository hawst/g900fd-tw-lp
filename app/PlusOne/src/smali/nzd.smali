.class public final Lnzd;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnyo;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3611
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3614
    const/4 v0, 0x0

    iput-object v0, p0, Lnzd;->a:Lnyo;

    .line 3611
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 3651
    const/4 v0, 0x0

    .line 3652
    iget-object v1, p0, Lnzd;->a:Lnyo;

    if-eqz v1, :cond_0

    .line 3653
    const/4 v0, 0x1

    iget-object v1, p0, Lnzd;->a:Lnyo;

    .line 3654
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3656
    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Lnzd;->b:Ljava/lang/Long;

    .line 3657
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3658
    iget-object v1, p0, Lnzd;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3659
    const/4 v1, 0x3

    iget-object v2, p0, Lnzd;->c:Ljava/lang/Integer;

    .line 3660
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3662
    :cond_1
    iget-object v1, p0, Lnzd;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3663
    const/4 v1, 0x4

    iget-object v2, p0, Lnzd;->d:Ljava/lang/Integer;

    .line 3664
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3666
    :cond_2
    iget-object v1, p0, Lnzd;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3667
    const/4 v1, 0x5

    iget-object v2, p0, Lnzd;->e:Ljava/lang/String;

    .line 3668
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3670
    :cond_3
    iget-object v1, p0, Lnzd;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3671
    const/4 v1, 0x6

    iget-object v2, p0, Lnzd;->f:Ljava/lang/String;

    .line 3672
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3674
    :cond_4
    iget-object v1, p0, Lnzd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3675
    iput v0, p0, Lnzd;->ai:I

    .line 3676
    return v0
.end method

.method public a(Loxn;)Lnzd;
    .locals 2

    .prologue
    .line 3684
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3685
    sparse-switch v0, :sswitch_data_0

    .line 3689
    iget-object v1, p0, Lnzd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3690
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnzd;->ah:Ljava/util/List;

    .line 3693
    :cond_1
    iget-object v1, p0, Lnzd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3695
    :sswitch_0
    return-object p0

    .line 3700
    :sswitch_1
    iget-object v0, p0, Lnzd;->a:Lnyo;

    if-nez v0, :cond_2

    .line 3701
    new-instance v0, Lnyo;

    invoke-direct {v0}, Lnyo;-><init>()V

    iput-object v0, p0, Lnzd;->a:Lnyo;

    .line 3703
    :cond_2
    iget-object v0, p0, Lnzd;->a:Lnyo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3707
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnzd;->b:Ljava/lang/Long;

    goto :goto_0

    .line 3711
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnzd;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 3715
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnzd;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 3719
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzd;->e:Ljava/lang/String;

    goto :goto_0

    .line 3723
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzd;->f:Ljava/lang/String;

    goto :goto_0

    .line 3685
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3629
    iget-object v0, p0, Lnzd;->a:Lnyo;

    if-eqz v0, :cond_0

    .line 3630
    const/4 v0, 0x1

    iget-object v1, p0, Lnzd;->a:Lnyo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3632
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lnzd;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 3633
    iget-object v0, p0, Lnzd;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3634
    const/4 v0, 0x3

    iget-object v1, p0, Lnzd;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3636
    :cond_1
    iget-object v0, p0, Lnzd;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 3637
    const/4 v0, 0x4

    iget-object v1, p0, Lnzd;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3639
    :cond_2
    iget-object v0, p0, Lnzd;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3640
    const/4 v0, 0x5

    iget-object v1, p0, Lnzd;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3642
    :cond_3
    iget-object v0, p0, Lnzd;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3643
    const/4 v0, 0x6

    iget-object v1, p0, Lnzd;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3645
    :cond_4
    iget-object v0, p0, Lnzd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3647
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3607
    invoke-virtual {p0, p1}, Lnzd;->a(Loxn;)Lnzd;

    move-result-object v0

    return-object v0
.end method
