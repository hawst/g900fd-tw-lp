.class public final Lcqs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcsq;


# instance fields
.field final a:Lcqw;

.field final b:Lcrc;

.field private final c:Ljvl;

.field private final d:Lcqv;

.field private final e:Lctz;

.field private final f:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcrc;Ljvl;Lcqv;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const-class v0, Lctz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcqs;->e:Lctz;

    .line 77
    const-class v0, Lcqw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqw;

    iput-object v0, p0, Lcqs;->a:Lcqw;

    .line 78
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcqs;->f:Landroid/view/LayoutInflater;

    .line 79
    iput-object p2, p0, Lcqs;->b:Lcrc;

    .line 80
    iput-object p3, p0, Lcqs;->c:Ljvl;

    .line 81
    iput-object p4, p0, Lcqs;->d:Lcqv;

    .line 82
    return-void
.end method

.method private a(Lcrc;Ljcn;)I
    .locals 5

    .prologue
    .line 261
    const/4 v0, 0x0

    .line 263
    if-eqz p2, :cond_1

    iget-object v1, p1, Lcrc;->a:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 264
    iget-object v1, p1, Lcrc;->c:Lcra;

    iget-object v1, v1, Lcra;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrb;

    .line 265
    iget v3, v0, Lcrb;->i:I

    const/16 v4, 0x65

    if-eq v3, v4, :cond_2

    new-instance v3, Ljug;

    iget-object v4, p1, Lcrc;->c:Lcra;

    iget-object v4, v4, Lcra;->c:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljue;

    iget-object v0, v0, Lcrb;->c:Lizu;

    invoke-direct {v4, v0}, Ljue;-><init>(Lizu;)V

    .line 266
    invoke-virtual {p2, v3, v4}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v0

    check-cast v0, Ljuc;

    if-eqz v0, :cond_2

    .line 268
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 270
    goto :goto_0

    :cond_0
    move v0, v1

    .line 273
    :cond_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcqs;->c:Ljvl;

    iget v0, v0, Ljvl;->a:I

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public a(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14

    .prologue
    .line 86
    iget-object v2, p0, Lcqs;->c:Ljvl;

    iget v2, v2, Ljvl;->a:I

    if-ge p1, v2, :cond_c

    .line 87
    if-nez p3, :cond_16

    iget-object v2, p0, Lcqs;->f:Landroid/view/LayoutInflater;

    const v3, 0x7f04018e

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    move/from16 v0, p2

    move/from16 v1, p2

    invoke-direct {v2, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v12, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    iget-object v2, p0, Lcqs;->b:Lcrc;

    iget-object v2, v2, Lcrc;->a:Ljava/util/List;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcqs;->b:Lcrc;

    iget-object v2, v2, Lcrc;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move v3, v2

    :goto_1
    iget-object v2, p0, Lcqs;->b:Lcrc;

    iget-object v2, v2, Lcrc;->a:Ljava/util/List;

    if-eqz v2, :cond_7

    if-ge p1, v3, :cond_7

    iget-object v2, p0, Lcqs;->b:Lcrc;

    iget-object v2, v2, Lcrc;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcrb;

    invoke-virtual {v2}, Lcrb;->a()Z

    move-result v2

    if-nez v2, :cond_7

    add-int/lit8 v2, v3, -0x1

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcqs;->c:Ljvl;

    iget v2, v2, Ljvl;->a:I

    if-ne v3, v2, :cond_0

    iget-object v2, p0, Lcqs;->b:Lcrc;

    iget-object v2, v2, Lcrc;->b:Lcrd;

    iget-boolean v2, v2, Lcrd;->a:Z

    if-nez v2, :cond_7

    :cond_0
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v12, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcqs;->b:Lcrc;

    iget-object v2, v2, Lcrc;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcrb;

    move-object v11, v12

    check-cast v11, Lcom/google/android/apps/plus/views/PhotoTileView;

    const/high16 v2, 0x10000

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->i(I)V

    iget-object v2, v10, Lcrb;->c:Lizu;

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    iget-object v2, v10, Lcrb;->f:Ljava/lang/Integer;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    if-lez v2, :cond_8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Ljava/lang/Integer;)V

    :goto_3
    iget-object v2, v10, Lcrb;->e:Ljava/lang/Integer;

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljava/lang/Integer;)V

    iget-wide v2, v10, Lcrb;->h:J

    const-wide v4, 0x200000000L

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    iget-wide v2, v10, Lcrb;->h:J

    const-wide/32 v4, 0x20000000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    iget-wide v2, v10, Lcrb;->g:J

    const-wide/16 v4, 0x4000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    :cond_1
    const/4 v2, 0x1

    move v13, v2

    :goto_4
    iget-object v2, p0, Lcqs;->e:Lctz;

    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    new-instance v3, Ljug;

    iget-object v4, v10, Lcrb;->d:Lcra;

    iget-object v4, v4, Lcra;->c:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljue;

    iget-object v5, v10, Lcrb;->c:Lizu;

    invoke-direct {v4, v5}, Ljue;-><init>(Lizu;)V

    invoke-virtual {v2, v3, v4}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v2

    check-cast v2, Ljuc;

    if-nez v2, :cond_2

    new-instance v2, Ljuc;

    iget-object v3, v10, Lcrb;->d:Lcra;

    iget-object v3, v3, Lcra;->a:Ljava/lang/String;

    iget-object v4, v10, Lcrb;->d:Lcra;

    iget-object v4, v4, Lcra;->c:Ljava/lang/String;

    iget-object v5, v10, Lcrb;->c:Lizu;

    iget-wide v6, v10, Lcrb;->g:J

    iget-wide v8, v10, Lcrb;->h:J

    invoke-direct/range {v2 .. v9}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    :cond_2
    iget-wide v4, v10, Lcrb;->g:J

    const-wide/16 v6, 0x100

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    :goto_5
    iget-object v5, v10, Lcrb;->c:Lizu;

    invoke-virtual {v2}, Ljuc;->h()Lnzi;

    move-result-object v4

    if-nez v4, :cond_b

    const/4 v4, 0x0

    :goto_6
    invoke-virtual {v11, v5, v4}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;Lizo;)V

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljcl;)V

    invoke-virtual {v11, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->d(Z)V

    invoke-virtual {v11, v13}, Lcom/google/android/apps/plus/views/PhotoTileView;->n(Z)V

    iget-object v2, p0, Lcqs;->d:Lcqv;

    invoke-interface {v2}, Lcqv;->a()Landroid/view/View$OnLongClickListener;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    new-instance v2, Lcqu;

    invoke-direct {v2, p0, v10}, Lcqu;-><init>(Lcqs;Lcrb;)V

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcqs;->d:Lcqv;

    invoke-interface {v2}, Lcqv;->b()Levy;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(I)V

    iget-object v3, v10, Lcrb;->c:Lizu;

    invoke-virtual {v2, v11, v3}, Levy;->a(Lcom/google/android/apps/plus/views/PhotoTileView;Lizu;)V

    :cond_3
    iget-object v2, p0, Lcqs;->d:Lcqv;

    invoke-interface {v2}, Lcqv;->d()Z

    iget-object v2, p0, Lcqs;->d:Lcqv;

    invoke-interface {v2}, Lcqv;->c()Leqt;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcqs;->d:Lcqv;

    invoke-interface {v2}, Lcqv;->c()Leqt;

    move-result-object v2

    iget-object v3, v10, Lcrb;->c:Lizu;

    invoke-interface {v2, v3, v11}, Leqt;->a(Lizu;Lcom/google/android/apps/plus/views/PhotoTileView;)V

    :cond_4
    move-object/from16 p3, v12

    .line 96
    :cond_5
    :goto_7
    return-object p3

    .line 87
    :cond_6
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_1

    :cond_7
    const/16 v2, 0x8

    goto/16 :goto_2

    :cond_8
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Ljava/lang/Integer;)V

    goto/16 :goto_3

    :cond_9
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_4

    :cond_a
    const/4 v3, 0x0

    goto :goto_5

    :cond_b
    new-instance v4, Ljub;

    invoke-virtual {v2}, Ljuc;->h()Lnzi;

    move-result-object v6

    invoke-direct {v4, v6}, Ljub;-><init>(Lnzi;)V

    goto :goto_6

    .line 88
    :cond_c
    invoke-virtual {p0}, Lcqs;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_12

    .line 89
    if-nez p3, :cond_d

    iget-object v2, p0, Lcqs;->f:Landroid/view/LayoutInflater;

    const v3, 0x7f040059

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    move/from16 v0, p2

    move/from16 v1, p2

    invoke-direct {v2, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_d
    iget-object v2, p0, Lcqs;->b:Lcrc;

    invoke-virtual {v2}, Lcrc;->c()Z

    move-result v3

    if-eqz v3, :cond_10

    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    if-eqz v3, :cond_5

    const v2, 0x7f1001d2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PhotoTileView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->setVisibility(I)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->c(Z)V

    iget-object v3, p0, Lcqs;->b:Lcrc;

    iget-object v4, p0, Lcqs;->c:Ljvl;

    iget v4, v4, Ljvl;->a:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lcrc;->a(I)Lcrb;

    move-result-object v3

    if-eqz v3, :cond_e

    iget-object v3, v3, Lcrb;->c:Lizu;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    :cond_e
    new-instance v2, Lcqt;

    invoke-direct {v2, p0}, Lcqt;-><init>(Lcqs;)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v3, 0x0

    iget-object v2, p0, Lcqs;->e:Lctz;

    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v4

    const v2, 0x7f100197

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v4, :cond_f

    iget-object v3, p0, Lcqs;->b:Lcrc;

    invoke-direct {p0, v3, v4}, Lcqs;->a(Lcrc;Ljcn;)I

    move-result v3

    new-instance v5, Ljug;

    iget-object v6, p0, Lcqs;->b:Lcrc;

    iget-object v6, v6, Lcrc;->c:Lcra;

    iget-object v6, v6, Lcra;->c:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljug;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljcn;->a(Ljcj;)I

    move-result v4

    sub-int v3, v4, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_f
    if-lez v3, :cond_11

    const/4 v3, 0x0

    :goto_9
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_10
    const/16 v2, 0x8

    goto :goto_8

    :cond_11
    const/16 v3, 0x8

    goto :goto_9

    .line 90
    :cond_12
    invoke-virtual {p0}, Lcqs;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_15

    .line 91
    if-nez p3, :cond_13

    iget-object v2, p0, Lcqs;->f:Landroid/view/LayoutInflater;

    const v3, 0x7f0400f8

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    move/from16 v0, p2

    move/from16 v1, p2

    invoke-direct {v2, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_13
    iget-object v2, p0, Lcqs;->b:Lcrc;

    iget-object v2, v2, Lcrc;->b:Lcrd;

    iget-boolean v2, v2, Lcrd;->b:Z

    if-eqz v2, :cond_14

    const/4 v2, 0x0

    :goto_a
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    :cond_14
    const/16 v2, 0x8

    goto :goto_a

    .line 93
    :cond_15
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Unhandled collection list photo row child"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_16
    move-object/from16 v12, p3

    goto/16 :goto_0
.end method

.method public a(J)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcrb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    iget-object v0, p0, Lcqs;->b:Lcrc;

    iget-object v0, v0, Lcrc;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 298
    :goto_0
    return-object v0

    .line 284
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 286
    iget-object v0, p0, Lcqs;->b:Lcrc;

    iget-object v0, v0, Lcrc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 287
    iget-object v1, p0, Lcqs;->b:Lcrc;

    invoke-virtual {v1}, Lcrc;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcqs;->c:Ljvl;

    iget v1, v1, Ljvl;->a:I

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcqs;->b:Lcrc;

    iget-object v1, v1, Lcrc;->b:Lcrd;

    iget-boolean v1, v1, Lcrd;->a:Z

    if-eqz v1, :cond_3

    :cond_1
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    .line 291
    :goto_1
    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v1, :cond_4

    .line 292
    iget-object v0, p0, Lcqs;->b:Lcrc;

    iget-object v0, v0, Lcrc;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrb;

    .line 293
    iget-wide v4, v0, Lcrb;->h:J

    and-long/2addr v4, p1

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    .line 294
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v1, v0

    .line 287
    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 298
    goto :goto_0
.end method

.method public a(Lctn;)V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcqs;->b:Lcrc;

    invoke-interface {p1, v0}, Lctn;->a(Lcrc;)V

    .line 309
    return-void
.end method
