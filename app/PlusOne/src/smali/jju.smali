.class public final Ljju;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Ljin;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 609
    sget-object v0, Ljjv;->a:[I

    invoke-virtual {p0}, Ljin;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 615
    const-string v0, "GunsSyncer"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown fetch category: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 611
    :pswitch_0
    const-string v0, "important_fetch_paging_token"

    goto :goto_0

    .line 613
    :pswitch_1
    const-string v0, "low_fetch_paging_token"

    goto :goto_0

    .line 609
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 553
    const-string v0, "user_data"

    invoke-virtual {p1, v0, p0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 554
    if-gtz v0, :cond_0

    .line 556
    const-string v0, "user_data"

    invoke-virtual {p1, v0, v1, p0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 558
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 469
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v0

    .line 470
    invoke-virtual {v0, p1, p2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    .line 472
    if-nez v0, :cond_0

    .line 473
    const-string v0, "GunsSyncer"

    const-string v1, "Cannot find database helper for account."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :goto_0
    return-void

    .line 477
    :cond_0
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 478
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 479
    const-string v2, "sync_version"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 480
    invoke-static {v1, v0}, Ljju;->a(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljin;[B)V
    .locals 4

    .prologue
    .line 413
    invoke-static {p3}, Ljju;->b(Ljin;)Ljava/lang/String;

    move-result-object v0

    .line 414
    if-nez v0, :cond_0

    .line 415
    const-string v0, "GunsSyncer"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x30

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot store paging token for unknown category: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    :goto_0
    return-void

    .line 417
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 418
    invoke-virtual {v1, v0, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 419
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v0

    .line 420
    invoke-virtual {v0, p1, p2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    .line 422
    if-nez v0, :cond_1

    .line 423
    const-string v0, "GunsSyncer"

    const-string v1, "Cannot find database helper for account."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 427
    :cond_1
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 428
    invoke-static {v1, v0}, Ljju;->a(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Llub;Z)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    .line 309
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v0

    .line 310
    invoke-virtual {v0, p1, p2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    .line 312
    if-nez v0, :cond_0

    .line 313
    const-string v0, "GunsSyncer"

    const-string v1, "Cannot find database helper for account."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 318
    array-length v3, p3

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_5

    aget-object v4, p3, v0

    .line 319
    if-nez p4, :cond_1

    .line 320
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v4, Llub;->h:Ljava/lang/Boolean;

    .line 322
    :cond_1
    new-array v5, v10, [Ljava/lang/String;

    iget-object v6, v4, Llub;->b:Ljava/lang/String;

    aput-object v6, v5, v1

    invoke-static {p0, p1, p2, v5}, Ljla;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljkx;

    move-result-object v5

    .line 324
    invoke-virtual {v5}, Ljkx;->getCount()I

    move-result v6

    .line 325
    if-le v6, v10, :cond_2

    .line 327
    const-string v4, "GunsSyncer"

    const-string v5, "More than one row for a single key"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 329
    :cond_2
    if-ne v6, v10, :cond_4

    .line 331
    invoke-virtual {v5}, Ljkx;->moveToFirst()Z

    .line 332
    invoke-virtual {v5}, Ljkx;->b()J

    move-result-wide v6

    .line 333
    iget-object v8, v4, Llub;->f:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 334
    cmp-long v6, v8, v6

    if-lez v6, :cond_3

    .line 335
    invoke-static {v4, v2}, Ljju;->a(Llub;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 337
    :cond_3
    invoke-virtual {v5}, Ljkx;->close()V

    goto :goto_2

    .line 340
    :cond_4
    invoke-static {v4, v2}, Ljju;->a(Llub;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 341
    invoke-virtual {v5}, Ljkx;->close()V

    goto :goto_2

    .line 344
    :cond_5
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Ljkw;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    new-instance v0, Ljkn;

    invoke-direct {v0}, Ljkn;-><init>()V

    invoke-virtual {v0, p0, p1, p2}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static declared-synchronized a(Landroid/content/Context;Lkfo;)V
    .locals 6

    .prologue
    .line 47
    const-class v1, Ljju;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v0

    .line 48
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    .line 50
    if-nez v0, :cond_0

    .line 51
    const-string v0, "GunsSyncer"

    const-string v2, "Cannot find database helper for account."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :goto_0
    monitor-exit v1

    return-void

    .line 55
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljky;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 56
    const-string v2, "DELETE FROM %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "notifications"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized a(Landroid/content/Context;Lkfo;Ljin;Lpew;)V
    .locals 6

    .prologue
    .line 121
    const-class v1, Ljju;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lpew;->b:[Llub;

    const/4 v4, 0x0

    .line 121
    invoke-static {p0, v0, v2, v3, v4}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Llub;Z)V

    .line 126
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lpew;->d:[B

    .line 126
    invoke-static {p2}, Ljju;->a(Ljin;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v0, "GunsSyncer"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x30

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot store paging token for unknown category: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :goto_0
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lpew;->c:[B

    .line 130
    invoke-static {p0, v0, v2, p2, v3}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljin;[B)V

    .line 134
    iget-object v0, p3, Lpew;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p3, Lpew;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 136
    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lpew;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 135
    invoke-static {p0, v0, v2, v4, v5}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :cond_0
    monitor-exit v1

    return-void

    .line 126
    :cond_1
    :try_start_1
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v5, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "GunsSyncer"

    const-string v2, "Cannot find database helper for account."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 126
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v5, v0}, Ljju;->a(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static declared-synchronized a(Landroid/content/Context;Lkfo;Ljin;Lpfm;)V
    .locals 6

    .prologue
    .line 145
    const-class v1, Ljju;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lpfm;->b:[Llub;

    const/4 v4, 0x0

    .line 145
    invoke-static {p0, v0, v2, v3, v4}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Llub;Z)V

    .line 150
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lpfm;->c:[B

    .line 150
    invoke-static {p0, v0, v2, p2, v3}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljin;[B)V

    .line 156
    iget-object v0, p3, Lpfm;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p3, Lpfm;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lpfm;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 157
    invoke-static {p0, v0, v2, v4, v5}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :cond_0
    monitor-exit v1

    return-void

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized a(Landroid/content/Context;Lmys;Lkfo;)V
    .locals 5

    .prologue
    .line 244
    const-class v1, Ljju;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 245
    invoke-virtual {p2}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lmys;->d:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 244
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "GunsSyncer"

    const-string v2, "Cannot find database helper for account."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :goto_0
    invoke-virtual {p2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 247
    invoke-virtual {p2}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lmys;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 246
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "GunsSyncer"

    const-string v2, "Cannot find database helper for account."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :goto_1
    invoke-virtual {p2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 249
    invoke-virtual {p2}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lmys;->b:Ljava/lang/String;

    .line 248
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "GunsSyncer"

    const-string v2, "Cannot find database helper for account."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :goto_2
    monitor-exit v1

    return-void

    .line 244
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "has_unread"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-static {v2, v0}, Ljju;->a(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 246
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "unread_count"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v2, v0}, Ljju;->a(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 248
    :cond_2
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "unread_count_string"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2, v0}, Ljju;->a(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private static a(Llub;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 562
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 563
    const-string v2, "key"

    iget-object v3, p0, Llub;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const-string v2, "priority"

    iget v3, p0, Llub;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 565
    const-string v2, "read_state"

    iget v3, p0, Llub;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 566
    const-string v2, "sort_version"

    iget-object v3, p0, Llub;->g:Ljava/lang/Long;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 567
    const-string v2, "last_modified_version"

    iget-object v3, p0, Llub;->f:Ljava/lang/Long;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 568
    const-string v2, "push_enabled"

    iget-object v3, p0, Llub;->h:Ljava/lang/Boolean;

    if-nez v3, :cond_7

    .line 569
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 568
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 573
    iget-object v0, p0, Llub;->c:Llvn;

    if-eqz v0, :cond_6

    .line 574
    iget-object v0, p0, Llub;->c:Llvn;

    .line 575
    iget-object v2, v0, Llvn;->d:Llva;

    if-eqz v2, :cond_1

    .line 576
    const-string v2, "payload"

    iget-object v3, v0, Llvn;->d:Llva;

    .line 577
    invoke-static {v3}, Loxu;->a(Loxu;)[B

    move-result-object v3

    .line 576
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 579
    :cond_1
    iget-object v2, v0, Llvn;->b:Llvg;

    if-eqz v2, :cond_2

    .line 580
    const-string v2, "expanded_info"

    iget-object v3, v0, Llvn;->b:Llvg;

    .line 581
    invoke-static {v3}, Loxu;->a(Loxu;)[B

    move-result-object v3

    .line 580
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 583
    :cond_2
    iget-object v2, v0, Llvn;->a:Llvb;

    if-eqz v2, :cond_5

    .line 584
    iget-object v2, p0, Llub;->c:Llvn;

    iget-object v2, v2, Llvn;->a:Llvb;

    .line 585
    iget-object v3, v2, Llvb;->d:Llvd;

    if-eqz v3, :cond_3

    .line 586
    const-string v3, "default_destination"

    iget-object v4, v2, Llvb;->d:Llvd;

    iget-object v4, v4, Llvd;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_3
    iget-object v3, v2, Llvb;->c:Ljava/lang/Long;

    if-eqz v3, :cond_4

    .line 589
    const-string v3, "creation_time"

    iget-object v4, v2, Llvb;->c:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 591
    :cond_4
    iget-object v3, v2, Llvb;->b:Llvo;

    if-eqz v3, :cond_5

    .line 592
    const-string v3, "simple_collapsed_layout"

    iget-object v2, v2, Llvb;->b:Llvo;

    .line 593
    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    .line 592
    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 596
    :cond_5
    iget-object v2, v0, Llvn;->c:Llvc;

    if-eqz v2, :cond_6

    .line 597
    iget-object v0, v0, Llvn;->c:Llvc;

    iget-object v0, v0, Llvc;->a:Lluz;

    .line 598
    if-eqz v0, :cond_6

    .line 599
    const-string v2, "category"

    iget-object v0, v0, Lluz;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_6
    const-string v0, "notifications"

    const/4 v2, 0x0

    const/4 v3, 0x5

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 605
    return-void

    .line 568
    :cond_7
    iget-object v3, p0, Llub;->h:Ljava/lang/Boolean;

    .line 569
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static declared-synchronized a(Landroid/content/Context;Lkfo;Ljava/lang/String;)[B
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 637
    const-class v10, Ljju;

    monitor-enter v10

    :try_start_0
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v0

    .line 638
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    .line 640
    if-nez v0, :cond_0

    .line 641
    const-string v0, "GunsSyncer"

    const-string v1, "Cannot find database helper for account."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v9

    .line 656
    :goto_0
    monitor-exit v10

    return-object v0

    .line 645
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljky;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 646
    const-string v1, "user_data"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 649
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v9

    .line 650
    goto :goto_0

    .line 653
    :cond_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 654
    invoke-interface {v1, p2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 656
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0

    .line 656
    :catchall_1
    move-exception v0

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method static declared-synchronized a(Landroid/content/Context;Lkfo;Ljin;)[B
    .locals 5

    .prologue
    .line 94
    const-class v1, Ljju;

    monitor-enter v1

    :try_start_0
    invoke-static {p2}, Ljju;->a(Ljin;)Ljava/lang/String;

    move-result-object v0

    .line 95
    if-nez v0, :cond_0

    .line 96
    const-string v0, "GunsSyncer"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x33

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot retrieve paging token for unknown category: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    const/4 v0, 0x0

    .line 99
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {p0, p1, v0}, Ljju;->a(Landroid/content/Context;Lkfo;Ljava/lang/String;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[Ljiu;
    .locals 4

    .prologue
    .line 366
    array-length v0, p3

    new-array v0, v0, [Ljiu;

    .line 367
    invoke-static {p0, p1, p2, p3}, Ljla;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljkx;

    move-result-object v1

    .line 370
    :goto_0
    invoke-virtual {v1}, Ljkx;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 371
    invoke-virtual {v1}, Ljkx;->getPosition()I

    move-result v2

    new-instance v3, Ljiu;

    .line 372
    invoke-virtual {v1}, Ljkx;->a()Ljava/lang/String;

    .line 373
    invoke-virtual {v1}, Ljkx;->i()Ljava/lang/String;

    .line 374
    invoke-virtual {v1}, Ljkx;->h()Llva;

    invoke-direct {v3}, Ljiu;-><init>()V

    aput-object v3, v0, v2

    goto :goto_0

    .line 376
    :cond_0
    return-object v0
.end method

.method static declared-synchronized b(Landroid/content/Context;Lkfo;)Ljava/lang/Long;
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 64
    const-class v10, Ljju;

    monitor-enter v10

    :try_start_0
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    .line 67
    if-nez v0, :cond_0

    .line 68
    const-string v0, "GunsSyncer"

    const-string v1, "Cannot find database helper for account."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v9

    .line 84
    :goto_0
    monitor-exit v10

    return-object v0

    .line 72
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljky;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 73
    const-string v1, "user_data"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "sync_version"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 76
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v9

    .line 77
    goto :goto_0

    .line 80
    :cond_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 81
    const-string v0, "sync_version"

    .line 82
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 81
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 84
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0

    .line 84
    :catchall_1
    move-exception v0

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method private static b(Ljin;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 622
    sget-object v0, Ljjv;->a:[I

    invoke-virtual {p0}, Ljin;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 628
    const-string v0, "GunsSyncer"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown fetch category: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 624
    :pswitch_0
    const-string v0, "important_sync_token"

    goto :goto_0

    .line 626
    :pswitch_1
    const-string v0, "low_sync_token"

    goto :goto_0

    .line 622
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static declared-synchronized b(Landroid/content/Context;Lkfo;Ljin;)[B
    .locals 5

    .prologue
    .line 108
    const-class v1, Ljju;

    monitor-enter v1

    :try_start_0
    invoke-static {p2}, Ljju;->b(Ljin;)Ljava/lang/String;

    move-result-object v0

    .line 109
    if-nez v0, :cond_0

    .line 110
    const-string v0, "GunsSyncer"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x31

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot retrieve sync token for unknown category: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    const/4 v0, 0x0

    .line 113
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {p0, p1, v0}, Ljju;->a(Landroid/content/Context;Lkfo;Ljava/lang/String;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
