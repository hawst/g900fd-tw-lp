.class public final Lkib;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Lkhs;

.field private c:Landroid/content/Context;

.field private d:J

.field private e:I

.field private f:Landroid/content/SharedPreferences;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/libraries/social/settings/PreferenceScreen;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkid;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkic;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/DialogInterface;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lkie;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 2

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkib;->d:J

    .line 145
    iput-object p1, p0, Lkib;->a:Landroid/app/Activity;

    .line 146
    iput p2, p0, Lkib;->e:I

    .line 148
    iput-object p1, p0, Lkib;->c:Landroid/content/Context;

    invoke-static {p1}, Lkib;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkib;->a(Ljava/lang/String;)V

    .line 149
    return-void
.end method

.method public static b(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 368
    invoke-static {p0}, Lkib;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 369
    const/4 v1, 0x0

    .line 368
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 373
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_preferences"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 769
    monitor-enter p0

    .line 771
    :try_start_0
    iget-object v0, p0, Lkib;->k:Ljava/util/List;

    if-nez v0, :cond_1

    .line 772
    monitor-exit p0

    .line 782
    :cond_0
    return-void

    .line 775
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lkib;->k:Ljava/util/List;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 776
    iget-object v0, p0, Lkib;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 777
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 779
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 780
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/DialogInterface;

    invoke-interface {v0}, Landroid/content/DialogInterface;->dismiss()V

    .line 779
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 777
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lcom/google/android/libraries/social/settings/PreferenceScreen;
    .locals 2

    .prologue
    .line 281
    new-instance v0, Lcom/google/android/libraries/social/settings/PreferenceScreen;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 282
    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->a(Lkib;)V

    .line 283
    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)Lkhl;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lkib;->h:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    if-nez v0, :cond_0

    .line 413
    const/4 v0, 0x0

    .line 416
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkib;->h:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->a(Ljava/lang/CharSequence;)Lkhl;

    move-result-object v0

    goto :goto_0
.end method

.method a()Lkhs;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lkib;->b:Lkhs;

    return-object v0
.end method

.method a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 606
    monitor-enter p0

    .line 607
    :try_start_0
    iget-object v0, p0, Lkib;->i:Ljava/util/List;

    if-nez v0, :cond_1

    monitor-exit p0

    .line 617
    :cond_0
    return-void

    .line 608
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lkib;->i:Ljava/util/List;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 609
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 611
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 612
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 613
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkid;

    invoke-interface {v0, p1, p2}, Lkid;->a(ILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 614
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 609
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 735
    monitor-enter p0

    .line 737
    :try_start_0
    iget-object v0, p0, Lkib;->k:Ljava/util/List;

    if-nez v0, :cond_0

    .line 738
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkib;->k:Ljava/util/List;

    .line 741
    :cond_0
    iget-object v0, p0, Lkib;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 742
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 316
    iput-object p1, p0, Lkib;->g:Ljava/lang/String;

    .line 317
    const/4 v0, 0x0

    iput-object v0, p0, Lkib;->f:Landroid/content/SharedPreferences;

    .line 318
    return-void
.end method

.method a(Lkhs;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lkib;->b:Lkhs;

    .line 174
    return-void
.end method

.method a(Lkic;)V
    .locals 1

    .prologue
    .line 673
    monitor-enter p0

    .line 674
    :try_start_0
    iget-object v0, p0, Lkib;->j:Ljava/util/List;

    if-nez v0, :cond_0

    .line 675
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkib;->j:Ljava/util/List;

    .line 678
    :cond_0
    iget-object v0, p0, Lkib;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 679
    iget-object v0, p0, Lkib;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 681
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lkid;)V
    .locals 1

    .prologue
    .line 576
    monitor-enter p0

    .line 577
    :try_start_0
    iget-object v0, p0, Lkib;->i:Ljava/util/List;

    if-nez v0, :cond_0

    .line 578
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkib;->i:Ljava/util/List;

    .line 581
    :cond_0
    iget-object v0, p0, Lkib;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 582
    iget-object v0, p0, Lkib;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lkie;)V
    .locals 0

    .prologue
    .line 791
    iput-object p1, p0, Lkib;->l:Lkie;

    .line 792
    return-void
.end method

.method a(Lcom/google/android/libraries/social/settings/PreferenceScreen;)Z
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lkib;->h:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    if-eq p1, v0, :cond_0

    .line 397
    iput-object p1, p0, Lkib;->h:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    .line 398
    const/4 v0, 0x1

    .line 401
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()J
    .locals 4

    .prologue
    .line 292
    monitor-enter p0

    .line 293
    :try_start_0
    iget-wide v0, p0, Lkib;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lkib;->d:J

    monitor-exit p0

    return-wide v0

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 746
    monitor-enter p0

    .line 748
    :try_start_0
    iget-object v0, p0, Lkib;->k:Ljava/util/List;

    if-nez v0, :cond_0

    .line 749
    monitor-exit p0

    .line 753
    :goto_0
    return-void

    .line 752
    :cond_0
    iget-object v0, p0, Lkib;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 753
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b(Lkic;)V
    .locals 1

    .prologue
    .line 690
    monitor-enter p0

    .line 691
    :try_start_0
    iget-object v0, p0, Lkib;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p0, Lkib;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 694
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 351
    iget-object v0, p0, Lkib;->f:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lkib;->c:Landroid/content/Context;

    iget-object v1, p0, Lkib;->g:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lkib;->f:Landroid/content/SharedPreferences;

    .line 356
    :cond_0
    iget-object v0, p0, Lkib;->f:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method d()Lcom/google/android/libraries/social/settings/PreferenceScreen;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lkib;->h:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    return-object v0
.end method

.method e()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 509
    invoke-virtual {p0}, Lkib;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method f()Z
    .locals 1

    .prologue
    .line 528
    const/4 v0, 0x1

    return v0
.end method

.method g()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lkib;->a:Landroid/app/Activity;

    return-object v0
.end method

.method h()V
    .locals 1

    .prologue
    .line 656
    monitor-enter p0

    .line 657
    :try_start_0
    monitor-exit p0

    return-void

    .line 658
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method i()V
    .locals 4

    .prologue
    .line 702
    const/4 v0, 0x0

    .line 704
    monitor-enter p0

    .line 705
    :try_start_0
    iget-object v1, p0, Lkib;->j:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 706
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkib;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v2, v0

    .line 708
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 710
    if-eqz v2, :cond_0

    .line 711
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 712
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    .line 713
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkic;

    invoke-interface {v0}, Lkic;->i()V

    .line 712
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 708
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 718
    :cond_0
    invoke-direct {p0}, Lkib;->l()V

    .line 719
    return-void

    :cond_1
    move-object v2, v0

    goto :goto_0
.end method

.method j()I
    .locals 2

    .prologue
    .line 729
    monitor-enter p0

    .line 730
    :try_start_0
    iget v0, p0, Lkib;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lkib;->e:I

    monitor-exit p0

    return v0

    .line 731
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method k()Lkie;
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lkib;->l:Lkie;

    return-object v0
.end method
