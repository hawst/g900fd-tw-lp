.class public final Lmyn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyk;

.field public b:Lmyl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1223
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1226
    iput-object v0, p0, Lmyn;->a:Lmyk;

    .line 1229
    iput-object v0, p0, Lmyn;->b:Lmyl;

    .line 1223
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1246
    const/4 v0, 0x0

    .line 1247
    iget-object v1, p0, Lmyn;->a:Lmyk;

    if-eqz v1, :cond_0

    .line 1248
    const/4 v0, 0x1

    iget-object v1, p0, Lmyn;->a:Lmyk;

    .line 1249
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1251
    :cond_0
    iget-object v1, p0, Lmyn;->b:Lmyl;

    if-eqz v1, :cond_1

    .line 1252
    const/4 v1, 0x2

    iget-object v2, p0, Lmyn;->b:Lmyl;

    .line 1253
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1255
    :cond_1
    iget-object v1, p0, Lmyn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1256
    iput v0, p0, Lmyn;->ai:I

    .line 1257
    return v0
.end method

.method public a(Loxn;)Lmyn;
    .locals 2

    .prologue
    .line 1265
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1266
    sparse-switch v0, :sswitch_data_0

    .line 1270
    iget-object v1, p0, Lmyn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1271
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyn;->ah:Ljava/util/List;

    .line 1274
    :cond_1
    iget-object v1, p0, Lmyn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1276
    :sswitch_0
    return-object p0

    .line 1281
    :sswitch_1
    iget-object v0, p0, Lmyn;->a:Lmyk;

    if-nez v0, :cond_2

    .line 1282
    new-instance v0, Lmyk;

    invoke-direct {v0}, Lmyk;-><init>()V

    iput-object v0, p0, Lmyn;->a:Lmyk;

    .line 1284
    :cond_2
    iget-object v0, p0, Lmyn;->a:Lmyk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1288
    :sswitch_2
    iget-object v0, p0, Lmyn;->b:Lmyl;

    if-nez v0, :cond_3

    .line 1289
    new-instance v0, Lmyl;

    invoke-direct {v0}, Lmyl;-><init>()V

    iput-object v0, p0, Lmyn;->b:Lmyl;

    .line 1291
    :cond_3
    iget-object v0, p0, Lmyn;->b:Lmyl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1266
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1234
    iget-object v0, p0, Lmyn;->a:Lmyk;

    if-eqz v0, :cond_0

    .line 1235
    const/4 v0, 0x1

    iget-object v1, p0, Lmyn;->a:Lmyk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1237
    :cond_0
    iget-object v0, p0, Lmyn;->b:Lmyl;

    if-eqz v0, :cond_1

    .line 1238
    const/4 v0, 0x2

    iget-object v1, p0, Lmyn;->b:Lmyl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1240
    :cond_1
    iget-object v0, p0, Lmyn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1242
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1219
    invoke-virtual {p0, p1}, Lmyn;->a(Loxn;)Lmyn;

    move-result-object v0

    return-object v0
.end method
