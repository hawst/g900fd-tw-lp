.class public final Ldwp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Ldwp;->a:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 29
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 6

    .prologue
    const/4 v0, 0x7

    const/4 v1, 0x6

    const/4 v2, 0x5

    const/4 v3, 0x3

    const/4 v4, 0x2

    .line 32
    if-ge p1, v4, :cond_7

    .line 33
    iget-object v5, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v5}, Ldwp;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 37
    :goto_0
    if-ge v4, v3, :cond_6

    .line 38
    iget-object v4, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v4}, Ldwp;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 42
    :goto_1
    if-ge v3, v2, :cond_5

    .line 43
    iget-object v3, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v3}, Ldwp;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 47
    :goto_2
    if-ge v2, v1, :cond_4

    .line 48
    iget-object v2, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v2}, Ldwp;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 52
    :goto_3
    if-ge v1, v0, :cond_3

    .line 53
    iget-object v1, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v1}, Ldwp;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 57
    :goto_4
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 58
    iget-object v0, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Ldwp;->f(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 59
    const/16 v0, 0x8

    .line 62
    :cond_0
    const/16 v1, 0x9

    if-ge v0, v1, :cond_1

    .line 63
    iget-object v0, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Ldwp;->g(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 64
    const/16 v0, 0x9

    .line 67
    :cond_1
    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 68
    iget-object v0, p0, Ldwp;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Ldwp;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 69
    const/16 v0, 0xa

    .line 72
    :cond_2
    return v0

    :cond_3
    move v0, v1

    goto :goto_4

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    move v2, v3

    goto :goto_2

    :cond_6
    move v3, v4

    goto :goto_1

    :cond_7
    move v4, p1

    goto :goto_0
.end method

.method a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP TABLE photos"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 77
    const-string v0, "CREATE TABLE photos (_ID INTEGER PRIMARY KEY AUTOINCREMENT,fingerprint TEXT NOT NULL, local_path TEXT, account_name TEXT, );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 86
    const-string v0, "ALTER TABLE photos ADD COLUMN remote_url TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 88
    const-string v0, "ALTER TABLE photos ADD COLUMN photo_ids BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 93
    const-string v0, "DROP TABLE photos"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 94
    const-string v0, "CREATE TABLE photos (_ID INTEGER PRIMARY KEY AUTOINCREMENT,fingerprint TEXT NOT NULL, local_path TEXT, account_name TEXT, remote_url TEXT, photo_ids BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 105
    const-string v0, "DROP TABLE photos"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 106
    const-string v0, "CREATE TABLE photos (_id INTEGER PRIMARY KEY AUTOINCREMENT,fingerprint TEXT NOT NULL, local_path TEXT, account_name TEXT, remote_url TEXT, photo_ids BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method e(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 117
    const-string v0, "ALTER TABLE photos ADD COLUMN media_attr INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method f(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 121
    const-string v0, "ALTER TABLE photos ADD COLUMN cleanup_time INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 123
    return-void
.end method

.method g(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 126
    const-string v0, "ALTER TABLE photos ADD COLUMN restore_rows BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method h(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 131
    iget-object v0, p0, Ldwp;->a:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 134
    const-string v2, "ALTER TABLE photos ADD COLUMN account_id INTEGER NOT NULL DEFAULT -1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 137
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 140
    :try_start_0
    const-string v3, "SELECT DISTINCT account_name FROM photos"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 141
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 142
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 143
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 149
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 148
    :cond_2
    if-eqz v1, :cond_3

    .line 149
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 154
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 155
    invoke-interface {v0, v1}, Lhei;->a(Ljava/lang/String;)I

    move-result v3

    .line 156
    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    .line 158
    const-string v3, "DELETE FROM photos WHERE account_name = ?"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 160
    :cond_4
    const-string v4, "UPDATE photos SET account_id = ? where account_name = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 161
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    aput-object v1, v5, v7

    .line 160
    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 164
    :cond_5
    return-void
.end method
