.class public final Looj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Looj;


# instance fields
.field public b:Loog;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;

.field private e:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    new-array v0, v0, [Looj;

    sput-object v0, Looj;->a:[Looj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 236
    invoke-direct {p0}, Loxq;-><init>()V

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Looj;->b:Loog;

    .line 236
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 268
    const/4 v0, 0x0

    .line 269
    iget-object v1, p0, Looj;->b:Loog;

    if-eqz v1, :cond_0

    .line 270
    const/4 v0, 0x1

    iget-object v1, p0, Looj;->b:Loog;

    .line 271
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 273
    :cond_0
    iget-object v1, p0, Looj;->e:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 274
    const/4 v1, 0x2

    iget-object v2, p0, Looj;->e:Ljava/lang/Long;

    .line 275
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    :cond_1
    iget-object v1, p0, Looj;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 278
    const/4 v1, 0x3

    iget-object v2, p0, Looj;->d:Ljava/lang/Float;

    .line 279
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 281
    :cond_2
    iget-object v1, p0, Looj;->c:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 282
    const/4 v1, 0x4

    iget-object v2, p0, Looj;->c:Ljava/lang/Float;

    .line 283
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 285
    :cond_3
    iget-object v1, p0, Looj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 286
    iput v0, p0, Looj;->ai:I

    .line 287
    return v0
.end method

.method public a(Loxn;)Looj;
    .locals 2

    .prologue
    .line 295
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 296
    sparse-switch v0, :sswitch_data_0

    .line 300
    iget-object v1, p0, Looj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 301
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Looj;->ah:Ljava/util/List;

    .line 304
    :cond_1
    iget-object v1, p0, Looj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    :sswitch_0
    return-object p0

    .line 311
    :sswitch_1
    iget-object v0, p0, Looj;->b:Loog;

    if-nez v0, :cond_2

    .line 312
    new-instance v0, Loog;

    invoke-direct {v0}, Loog;-><init>()V

    iput-object v0, p0, Looj;->b:Loog;

    .line 314
    :cond_2
    iget-object v0, p0, Looj;->b:Loog;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 318
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Looj;->e:Ljava/lang/Long;

    goto :goto_0

    .line 322
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Looj;->d:Ljava/lang/Float;

    goto :goto_0

    .line 326
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Looj;->c:Ljava/lang/Float;

    goto :goto_0

    .line 296
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 250
    iget-object v0, p0, Looj;->b:Loog;

    if-eqz v0, :cond_0

    .line 251
    const/4 v0, 0x1

    iget-object v1, p0, Looj;->b:Loog;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 253
    :cond_0
    iget-object v0, p0, Looj;->e:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 254
    const/4 v0, 0x2

    iget-object v1, p0, Looj;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 256
    :cond_1
    iget-object v0, p0, Looj;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 257
    const/4 v0, 0x3

    iget-object v1, p0, Looj;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 259
    :cond_2
    iget-object v0, p0, Looj;->c:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 260
    const/4 v0, 0x4

    iget-object v1, p0, Looj;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 262
    :cond_3
    iget-object v0, p0, Looj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 264
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0, p1}, Looj;->a(Loxn;)Looj;

    move-result-object v0

    return-object v0
.end method
