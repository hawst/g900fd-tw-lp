.class public final Llum;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llum;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Llum;

    sput-object v0, Llum;->a:[Llum;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23
    const/high16 v0, -0x80000000

    iput v0, p0, Llum;->b:I

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    iget v1, p0, Llum;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 44
    const/4 v0, 0x1

    iget v1, p0, Llum;->b:I

    .line 45
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 47
    :cond_0
    iget-object v1, p0, Llum;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x2

    iget-object v2, p0, Llum;->c:Ljava/lang/String;

    .line 49
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_1
    iget-object v1, p0, Llum;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    iput v0, p0, Llum;->ai:I

    .line 53
    return v0
.end method

.method public a(Loxn;)Llum;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 61
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 62
    sparse-switch v0, :sswitch_data_0

    .line 66
    iget-object v1, p0, Llum;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 67
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llum;->ah:Ljava/util/List;

    .line 70
    :cond_1
    iget-object v1, p0, Llum;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    :sswitch_0
    return-object p0

    .line 77
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 78
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 86
    :cond_2
    iput v0, p0, Llum;->b:I

    goto :goto_0

    .line 88
    :cond_3
    iput v2, p0, Llum;->b:I

    goto :goto_0

    .line 93
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llum;->c:Ljava/lang/String;

    goto :goto_0

    .line 62
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30
    iget v0, p0, Llum;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 31
    const/4 v0, 0x1

    iget v1, p0, Llum;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 33
    :cond_0
    iget-object v0, p0, Llum;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 34
    const/4 v0, 0x2

    iget-object v1, p0, Llum;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 36
    :cond_1
    iget-object v0, p0, Llum;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 38
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llum;->a(Loxn;)Llum;

    move-result-object v0

    return-object v0
.end method
