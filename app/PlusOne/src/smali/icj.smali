.class public Licj;
.super Lllq;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Licj;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/16 v1, 0x14e

    aput v1, v0, v2

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 134
    new-instance v0, Lick;

    invoke-direct {v0}, Lick;-><init>()V

    sput-object v0, Licj;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const/high16 v0, -0x80000000

    iput v0, p0, Licj;->e:I

    .line 34
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const/high16 v0, -0x80000000

    iput v0, p0, Licj;->e:I

    .line 37
    iput-object p3, p0, Licj;->a:Ljava/lang/String;

    .line 38
    iput p1, p0, Licj;->e:I

    .line 39
    iput-object p2, p0, Licj;->c:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Licj;->d:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Licj;->b:Ljava/lang/String;

    .line 42
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const/high16 v0, -0x80000000

    iput v0, p0, Licj;->e:I

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Licj;->a:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Licj;->b:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Licj;->c:Ljava/lang/String;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Licj;->d:Ljava/lang/String;

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Licj;->e:I

    .line 153
    return-void
.end method

.method public constructor <init>(Loyv;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const/high16 v0, -0x80000000

    iput v0, p0, Licj;->e:I

    .line 45
    iget-object v0, p1, Loyv;->b:Ljava/lang/String;

    iput-object v0, p0, Licj;->a:Ljava/lang/String;

    .line 46
    iget v0, p1, Loyv;->f:I

    iput v0, p0, Licj;->e:I

    .line 47
    iget-object v0, p1, Loyv;->c:Ljava/lang/String;

    iput-object v0, p0, Licj;->c:Ljava/lang/String;

    .line 48
    iget-object v0, p1, Loyv;->d:Ljava/lang/String;

    iput-object v0, p0, Licj;->d:Ljava/lang/String;

    .line 49
    iget-object v0, p1, Loyv;->e:Lpdi;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p1, Loyv;->e:Lpdi;

    iget-object v0, v0, Lpdi;->a:Ljava/lang/String;

    iput-object v0, p0, Licj;->b:Ljava/lang/String;

    .line 52
    :cond_0
    return-void
.end method

.method public static a([B)Licj;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 105
    if-nez p0, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-object v0

    .line 109
    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 110
    if-eqz v1, :cond_0

    new-instance v0, Licj;

    invoke-direct {v0}, Licj;-><init>()V

    invoke-static {v1}, Licj;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Licj;->a:Ljava/lang/String;

    invoke-static {v1}, Licj;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Licj;->b:Ljava/lang/String;

    invoke-static {v1}, Licj;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Licj;->c:Ljava/lang/String;

    invoke-static {v1}, Licj;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Licj;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, v0, Licj;->e:I

    goto :goto_0
.end method

.method public static a(Licj;)[B
    .locals 3

    .prologue
    .line 87
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 88
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 90
    iget-object v2, p0, Licj;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Licj;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Licj;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Licj;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Licj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Licj;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Licj;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Licj;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Licj;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 96
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 97
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 98
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Licj;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Licj;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Licj;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Licj;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Licj;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 183
    iget v0, p0, Licj;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Licj;->e:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Licj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Licj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Licj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Licj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 166
    iget v0, p0, Licj;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 167
    return-void
.end method
