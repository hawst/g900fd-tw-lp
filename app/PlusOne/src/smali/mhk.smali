.class public final Lmhk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmxe;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19843
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19846
    iput-object v0, p0, Lmhk;->apiHeader:Llyq;

    .line 19849
    iput-object v0, p0, Lmhk;->a:Lmxe;

    .line 19843
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 19866
    const/4 v0, 0x0

    .line 19867
    iget-object v1, p0, Lmhk;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 19868
    const/4 v0, 0x1

    iget-object v1, p0, Lmhk;->apiHeader:Llyq;

    .line 19869
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19871
    :cond_0
    iget-object v1, p0, Lmhk;->a:Lmxe;

    if-eqz v1, :cond_1

    .line 19872
    const/4 v1, 0x2

    iget-object v2, p0, Lmhk;->a:Lmxe;

    .line 19873
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19875
    :cond_1
    iget-object v1, p0, Lmhk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19876
    iput v0, p0, Lmhk;->ai:I

    .line 19877
    return v0
.end method

.method public a(Loxn;)Lmhk;
    .locals 2

    .prologue
    .line 19885
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 19886
    sparse-switch v0, :sswitch_data_0

    .line 19890
    iget-object v1, p0, Lmhk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 19891
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhk;->ah:Ljava/util/List;

    .line 19894
    :cond_1
    iget-object v1, p0, Lmhk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19896
    :sswitch_0
    return-object p0

    .line 19901
    :sswitch_1
    iget-object v0, p0, Lmhk;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 19902
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhk;->apiHeader:Llyq;

    .line 19904
    :cond_2
    iget-object v0, p0, Lmhk;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19908
    :sswitch_2
    iget-object v0, p0, Lmhk;->a:Lmxe;

    if-nez v0, :cond_3

    .line 19909
    new-instance v0, Lmxe;

    invoke-direct {v0}, Lmxe;-><init>()V

    iput-object v0, p0, Lmhk;->a:Lmxe;

    .line 19911
    :cond_3
    iget-object v0, p0, Lmhk;->a:Lmxe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19886
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 19854
    iget-object v0, p0, Lmhk;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 19855
    const/4 v0, 0x1

    iget-object v1, p0, Lmhk;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 19857
    :cond_0
    iget-object v0, p0, Lmhk;->a:Lmxe;

    if-eqz v0, :cond_1

    .line 19858
    const/4 v0, 0x2

    iget-object v1, p0, Lmhk;->a:Lmxe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 19860
    :cond_1
    iget-object v0, p0, Lmhk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 19862
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 19839
    invoke-virtual {p0, p1}, Lmhk;->a(Loxn;)Lmhk;

    move-result-object v0

    return-object v0
.end method
