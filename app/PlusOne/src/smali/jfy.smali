.class public abstract Ljfy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Landroid/content/Context;

.field public b:I

.field private c:Ljava/lang/String;

.field private d:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(I)Landroid/content/Intent;
.end method

.method public abstract a(ILjgb;)V
.end method

.method public abstract a(JLjgb;)V
.end method

.method public a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 61
    iput-object p1, p0, Ljfy;->a:Landroid/content/Context;

    .line 63
    iget v0, p0, Ljfy;->b:I

    if-eq v0, p2, :cond_0

    .line 64
    iput p2, p0, Ljfy;->b:I

    .line 65
    invoke-virtual {p0}, Ljfy;->at_()V

    .line 67
    :cond_0
    return-void
.end method

.method public a(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public abstract a(Ljfz;J)V
.end method

.method public abstract a(J)Z
.end method

.method public abstract as_()V
.end method

.method public abstract at_()V
.end method

.method public au_()Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public av_()Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public abstract b(J)I
.end method

.method public b(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 229
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Ljfy;->c:Ljava/lang/String;

    .line 204
    return-void
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x1

    return v0
.end method

.method public abstract c(J)Landroid/content/Intent;
.end method

.method public c(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Ljfy;->d:Landroid/database/ContentObserver;

    .line 217
    return-void
.end method

.method public abstract c(I)Z
.end method

.method public d()I
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public abstract d(I)I
.end method

.method public abstract e(I)J
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    return v0
.end method

.method public o()V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public p()V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method protected s()V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Ljfy;->d:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Ljfy;->d:Landroid/database/ContentObserver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/ContentObserver;->dispatchChange(Z)V

    .line 210
    :cond_0
    return-void
.end method

.method public w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Ljfy;->c:Ljava/lang/String;

    return-object v0
.end method

.method public x()V
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Ljfy;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {p0}, Ljfy;->p()V

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-virtual {p0}, Ljfy;->o()V

    goto :goto_0
.end method
