.class final Ljfe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Ljfd;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Ljfd;
    .locals 5

    .prologue
    .line 106
    invoke-static {}, Ljff;->values()[Ljff;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    aget-object v2, v0, v1

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 109
    :goto_0
    const/4 v0, 0x0

    .line 110
    if-eqz v1, :cond_0

    .line 111
    new-instance v0, Lood;

    invoke-direct {v0}, Lood;-><init>()V

    .line 113
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 119
    new-instance v4, Ljfd;

    invoke-direct {v4, v2, v3, v0, v1}, Ljfd;-><init>(Ljff;ILood;Ljava/lang/String;)V

    return-object v4

    .line 108
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid storyboard proto"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(I)[Ljfd;
    .locals 1

    .prologue
    .line 128
    new-array v0, p1, [Ljfd;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Ljfe;->a(Landroid/os/Parcel;)Ljfd;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Ljfe;->a(I)[Ljfd;

    move-result-object v0

    return-object v0
.end method
