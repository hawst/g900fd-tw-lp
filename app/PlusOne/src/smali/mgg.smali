.class public final Lmgg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnax;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23893
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23896
    iput-object v0, p0, Lmgg;->apiHeader:Llyq;

    .line 23899
    iput-object v0, p0, Lmgg;->a:Lnax;

    .line 23893
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 23916
    const/4 v0, 0x0

    .line 23917
    iget-object v1, p0, Lmgg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 23918
    const/4 v0, 0x1

    iget-object v1, p0, Lmgg;->apiHeader:Llyq;

    .line 23919
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23921
    :cond_0
    iget-object v1, p0, Lmgg;->a:Lnax;

    if-eqz v1, :cond_1

    .line 23922
    const/4 v1, 0x2

    iget-object v2, p0, Lmgg;->a:Lnax;

    .line 23923
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23925
    :cond_1
    iget-object v1, p0, Lmgg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23926
    iput v0, p0, Lmgg;->ai:I

    .line 23927
    return v0
.end method

.method public a(Loxn;)Lmgg;
    .locals 2

    .prologue
    .line 23935
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 23936
    sparse-switch v0, :sswitch_data_0

    .line 23940
    iget-object v1, p0, Lmgg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 23941
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgg;->ah:Ljava/util/List;

    .line 23944
    :cond_1
    iget-object v1, p0, Lmgg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23946
    :sswitch_0
    return-object p0

    .line 23951
    :sswitch_1
    iget-object v0, p0, Lmgg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 23952
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmgg;->apiHeader:Llyq;

    .line 23954
    :cond_2
    iget-object v0, p0, Lmgg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23958
    :sswitch_2
    iget-object v0, p0, Lmgg;->a:Lnax;

    if-nez v0, :cond_3

    .line 23959
    new-instance v0, Lnax;

    invoke-direct {v0}, Lnax;-><init>()V

    iput-object v0, p0, Lmgg;->a:Lnax;

    .line 23961
    :cond_3
    iget-object v0, p0, Lmgg;->a:Lnax;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23936
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23904
    iget-object v0, p0, Lmgg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 23905
    const/4 v0, 0x1

    iget-object v1, p0, Lmgg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23907
    :cond_0
    iget-object v0, p0, Lmgg;->a:Lnax;

    if-eqz v0, :cond_1

    .line 23908
    const/4 v0, 0x2

    iget-object v1, p0, Lmgg;->a:Lnax;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23910
    :cond_1
    iget-object v0, p0, Lmgg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23912
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 23889
    invoke-virtual {p0, p1}, Lmgg;->a(Loxn;)Lmgg;

    move-result-object v0

    return-object v0
.end method
