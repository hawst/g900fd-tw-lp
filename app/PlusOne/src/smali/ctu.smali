.class public final Lctu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfyp;
.implements Ljma;
.implements Llnx;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lfyp;",
        "Ljma",
        "<",
        "Lctz;",
        ">;",
        "Llnx;",
        "Llrb;",
        "Llrc;",
        "Llrg;"
    }
.end annotation


# instance fields
.field private final a:Lu;

.field private final b:Lcty;

.field private c:Lctz;

.field private d:Lhms;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lu;Llqr;Lcty;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lctu;->a:Lu;

    .line 73
    iput-object p3, p0, Lctu;->b:Lcty;

    .line 75
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 76
    return-void
.end method

.method static synthetic a(Lctu;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lctu;->c(Ljava/util/ArrayList;)V

    return-void
.end method

.method private c(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Ljcl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lctu;->c:Lctz;

    invoke-virtual {v0, p1}, Lctz;->b(Ljava/util/List;)V

    .line 108
    invoke-direct {p0}, Lctu;->e()V

    .line 109
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 155
    iget-object v0, p0, Lctu;->c:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    .line 157
    iget-object v1, p0, Lctu;->a:Lu;

    invoke-virtual {v1}, Lu;->n()Lz;

    move-result-object v1

    invoke-static {v1}, Llhn;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lctu;->a:Lu;

    invoke-virtual {v1}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11003f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lctu;->a:Lu;

    invoke-virtual {v1}, Lu;->x()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 158
    :cond_0
    iget-object v0, p0, Lctu;->b:Lcty;

    invoke-interface {v0}, Lcty;->e()V

    .line 159
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lctu;->b:Lcty;

    invoke-interface {v0}, Lcty;->H_()V

    .line 69
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 80
    iput-object p1, p0, Lctu;->e:Landroid/content/Context;

    .line 81
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lctu;->c:Lctz;

    .line 82
    const-class v0, Lhms;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lctu;->d:Lhms;

    .line 83
    return-void
.end method

.method public a(Ljcn;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lctu;->c:Lctz;

    invoke-virtual {v0, p1}, Lctz;->a(Ljcn;)V

    .line 134
    invoke-direct {p0}, Lctu;->e()V

    .line 135
    return-void
.end method

.method public a(Ljava/util/ArrayList;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Ljcl;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcl;

    .line 96
    instance-of v2, v0, Ljuf;

    if-eqz v2, :cond_0

    check-cast v0, Ljuf;

    .line 97
    invoke-interface {v0}, Ljuf;->h()Lnzi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lctu;->a:Lu;

    invoke-virtual {v0}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a068b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11003e

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a07fa

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a07fd

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "selected_media"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v1, p0, Lctu;->a:Lu;

    invoke-virtual {v1}, Lu;->p()Lae;

    move-result-object v1

    const-string v2, "deselect_photos"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    new-instance v1, Lctx;

    invoke-direct {v1, p0}, Lctx;-><init>(Lctu;)V

    invoke-virtual {v0, v1}, Llgr;->a(Llgs;)V

    .line 99
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0

    .line 102
    :cond_1
    invoke-direct {p0, p1}, Lctu;->c(Ljava/util/ArrayList;)V

    .line 103
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljcl;)Z
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lctv;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1}, Lctv;-><init>(ILjcl;)V

    invoke-virtual {p0, v0}, Lctu;->a(Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    invoke-virtual {p0}, Lctu;->a()V

    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lctu;->c:Lctz;

    invoke-virtual {v0}, Lctz;->b()Ljlx;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Ljlx;->a(Ljma;Z)V

    .line 146
    return-void
.end method

.method public b(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Ljcl;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 120
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v0, p0, Lctu;->a:Lu;

    invoke-virtual {v0}, Lu;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account_id"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lctu;->d:Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lctu;->e:Landroid/content/Context;

    invoke-direct {v4, v5, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v5, Lhmv;->eg:Lhmv;

    invoke-virtual {v4, v5}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    invoke-interface {v3, v4}, Lhms;->a(Lhmr;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    iget-object v0, p0, Lctu;->c:Lctz;

    invoke-virtual {v0, p1}, Lctz;->a(Ljava/util/List;)V

    .line 122
    invoke-direct {p0}, Lctu;->e()V

    .line 123
    return-void
.end method

.method public b(Ljcl;)V
    .locals 2

    .prologue
    .line 113
    new-instance v0, Lctw;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1}, Lctw;-><init>(ILjcl;)V

    invoke-virtual {p0, v0}, Lctu;->b(Ljava/util/ArrayList;)V

    .line 116
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lctu;->c:Lctz;

    invoke-virtual {v0}, Lctz;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0, p0}, Ljlx;->a(Ljma;)V

    .line 151
    return-void
.end method

.method public c(Ljcl;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lctu;->c:Lctz;

    invoke-virtual {v0, p1}, Lctz;->b(Ljcl;)V

    .line 128
    invoke-direct {p0}, Lctu;->e()V

    .line 129
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lctu;->c:Lctz;

    invoke-virtual {v0}, Lctz;->c()V

    .line 140
    invoke-direct {p0}, Lctu;->e()V

    .line 141
    return-void
.end method
