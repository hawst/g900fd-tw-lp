.class final Lvu;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lvr;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic b:Lvt;


# direct methods
.method constructor <init>(Lvt;)V
    .locals 1

    .prologue
    .line 2211
    iput-object p1, p0, Lvu;->b:Lvt;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lvu;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2232
    invoke-virtual {p0, p1, p2}, Lvu;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2233
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 2237
    iget v5, p1, Landroid/os/Message;->what:I

    .line 2238
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2241
    packed-switch v5, :pswitch_data_0

    .line 2247
    :goto_0
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lvu;->b:Lvt;

    iget-object v1, v1, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    add-int/lit8 v3, v1, -0x1

    if-ltz v3, :cond_1

    .line 2248
    iget-object v1, p0, Lvu;->b:Lvt;

    iget-object v1, v1, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvp;

    .line 2249
    if-nez v1, :cond_0

    .line 2250
    iget-object v1, p0, Lvu;->b:Lvt;

    iget-object v1, v1, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v3

    goto :goto_1

    .line 2241
    :pswitch_1
    iget-object v1, p0, Lvu;->b:Lvt;

    iget-object v3, v1, Lvt;->d:Lxa;

    move-object v1, v2

    check-cast v1, Lvy;

    invoke-virtual {v3, v1}, Lxa;->a(Lvy;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lvu;->b:Lvt;

    iget-object v3, v1, Lvt;->d:Lxa;

    move-object v1, v2

    check-cast v1, Lvy;

    invoke-virtual {v3, v1}, Lxa;->b(Lvy;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lvu;->b:Lvt;

    iget-object v3, v1, Lvt;->d:Lxa;

    move-object v1, v2

    check-cast v1, Lvy;

    invoke-virtual {v3, v1}, Lxa;->c(Lvy;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lvu;->b:Lvt;

    iget-object v3, v1, Lvt;->d:Lxa;

    move-object v1, v2

    check-cast v1, Lvy;

    invoke-virtual {v3, v1}, Lxa;->d(Lvy;)V

    goto :goto_0

    .line 2252
    :cond_0
    :try_start_1
    iget-object v4, p0, Lvu;->a:Ljava/util/ArrayList;

    iget-object v1, v1, Lvp;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v1, v3

    .line 2254
    goto :goto_1

    .line 2256
    :cond_1
    iget-object v1, p0, Lvu;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 2257
    const/4 v1, 0x0

    move v4, v1

    :goto_2
    if-ge v4, v6, :cond_3

    .line 2258
    iget-object v1, p0, Lvu;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvr;

    iget-object v7, v1, Lvr;->a:Lvp;

    iget-object v8, v1, Lvr;->b:Lvq;

    const v3, 0xff00

    and-int/2addr v3, v5

    sparse-switch v3, :sswitch_data_0

    .line 2257
    :cond_2
    :goto_3
    :pswitch_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 2258
    :sswitch_0
    move-object v0, v2

    check-cast v0, Lvy;

    move-object v3, v0

    invoke-virtual {v1, v3}, Lvr;->a(Lvy;)Z

    move-result v1

    if-eqz v1, :cond_2

    packed-switch v5, :pswitch_data_1

    goto :goto_3

    :pswitch_6
    invoke-virtual {v8, v7, v3}, Lvq;->a(Lvp;Lvy;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 2261
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lvu;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    throw v1

    .line 2258
    :pswitch_7
    :try_start_2
    invoke-virtual {v8, v7, v3}, Lvq;->b(Lvp;Lvy;)V

    goto :goto_3

    :pswitch_8
    invoke-virtual {v8, v7}, Lvq;->a(Lvp;)V

    goto :goto_3

    :pswitch_9
    invoke-virtual {v8, v3}, Lvq;->a(Lvy;)V

    goto :goto_3

    :pswitch_a
    invoke-virtual {v8, v7, v3}, Lvq;->c(Lvp;Lvy;)V

    goto :goto_3

    :pswitch_b
    invoke-virtual {v8, v7, v3}, Lvq;->d(Lvp;Lvy;)V

    goto :goto_3

    :sswitch_1
    packed-switch v5, :pswitch_data_2

    goto :goto_3

    :pswitch_c
    invoke-virtual {v8, v7}, Lvq;->b(Lvp;)V

    goto :goto_3

    :pswitch_d
    invoke-virtual {v8, v7}, Lvq;->c(Lvp;)V

    goto :goto_3

    :pswitch_e
    invoke-virtual {v8, v7}, Lvq;->d(Lvp;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 2261
    :cond_3
    iget-object v1, p0, Lvu;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2262
    return-void

    .line 2241
    nop

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 2258
    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x101
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x201
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
