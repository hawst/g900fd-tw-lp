.class public final Ldgr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ldgu;

.field public static final b:Ldgu;


# instance fields
.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldgw;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ldgu;",
            "Ljava/util/Set",
            "<",
            "Ldgv;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldgu;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldgu;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldgu;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ldgp;

    invoke-direct {v0}, Ldgp;-><init>()V

    sput-object v0, Ldgr;->a:Ldgu;

    .line 19
    new-instance v0, Ldgp;

    invoke-direct {v0}, Ldgp;-><init>()V

    sput-object v0, Ldgr;->b:Ldgu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldgr;->c:Ljava/util/Set;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldgr;->d:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldgr;->e:Ljava/util/Set;

    .line 58
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldgr;->f:Ljava/util/Set;

    .line 59
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldgr;->g:Ljava/util/Set;

    .line 61
    new-instance v0, Ldgs;

    invoke-direct {v0, p0}, Ldgs;-><init>(Ldgr;)V

    iput-object v0, p0, Ldgr;->h:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Ldgr;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Ldgr;->c:Ljava/util/Set;

    return-object v0
.end method

.method private e(Ldgu;)Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Ldgr;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 191
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ldgu;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 93
    iget-object v0, p0, Ldgr;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgu;

    .line 94
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 95
    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_1
    return-object v1
.end method

.method public a(Ldgu;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ldgr;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method public a(Ldgu;Ldgv;)V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Ldgr;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 145
    if-nez v0, :cond_0

    .line 146
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 147
    iget-object v1, p0, Ldgr;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method

.method public a(Ldgu;Z)V
    .locals 1

    .prologue
    .line 107
    if-eqz p2, :cond_1

    .line 108
    iget-object v0, p0, Ldgr;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 113
    :goto_0
    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Ldgr;->h:Ljava/lang/Runnable;

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 116
    :cond_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Ldgr;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ldgw;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ldgr;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public b(Ldgu;Ldgv;)V
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0, p1}, Ldgr;->e(Ldgu;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Ldgr;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 162
    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Ldgu;Z)V
    .locals 1

    .prologue
    .line 124
    if-eqz p2, :cond_1

    .line 125
    iget-object v0, p0, Ldgr;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 130
    :goto_0
    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Ldgr;->h:Ljava/lang/Runnable;

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 133
    :cond_0
    return-void

    .line 127
    :cond_1
    iget-object v0, p0, Ldgr;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Ldgu;)Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Ldgr;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(Ldgu;)Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Ldgr;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(Ldgu;)V
    .locals 2

    .prologue
    .line 169
    invoke-direct {p0, p1}, Ldgr;->e(Ldgu;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Ldgr;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 175
    new-instance v1, Ldgt;

    invoke-direct {v1, v0, p1}, Ldgt;-><init>(Ljava/util/Set;Ldgu;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
