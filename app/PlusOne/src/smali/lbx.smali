.class public final Llbx;
.super Lllq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llan;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lnmb;Lnly;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 86
    invoke-direct {p0}, Lllq;-><init>()V

    .line 88
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 93
    if-eqz p3, :cond_1

    iget-object v0, p3, Lnly;->b:[Lnma;

    if-eqz v0, :cond_1

    move v0, v1

    .line 94
    :goto_0
    iget-object v2, p3, Lnly;->b:[Lnma;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 95
    iget-object v2, p3, Lnly;->b:[Lnma;

    aget-object v2, v2, v0

    .line 97
    iget-object v5, v2, Lnma;->b:[Lnrp;

    if-eqz v5, :cond_0

    iget-object v5, v2, Lnma;->b:[Lnrp;

    array-length v5, v5

    if-lez v5, :cond_0

    .line 98
    new-instance v5, Llan;

    iget-object v2, v2, Lnma;->b:[Lnrp;

    aget-object v2, v2, v1

    invoke-direct {v5, p1, v2}, Llan;-><init>(Landroid/content/Context;Lnrp;)V

    .line 100
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_1
    if-eqz p2, :cond_2

    .line 106
    iget-object v1, p2, Lnmb;->b:Ljava/lang/String;

    .line 107
    iget-object v2, p2, Lnmb;->c:Ljava/lang/String;

    .line 108
    iget-object v3, p2, Lnmb;->d:Ljava/lang/String;

    .line 111
    :goto_1
    const/4 v5, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Llbx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    .line 112
    return-void

    :cond_2
    move-object v2, v3

    move-object v1, v3

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lnmf;Lnmd;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Llbx;-><init>(Landroid/content/Context;Lnmf;Lnmd;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lnmf;Lnmd;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-direct {p0}, Lllq;-><init>()V

    .line 63
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 68
    if-eqz p3, :cond_0

    iget-object v0, p3, Lnmd;->b:[Lnrp;

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p3, Lnmd;->b:[Lnrp;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 70
    iget-object v1, p3, Lnmd;->b:[Lnrp;

    aget-object v1, v1, v0

    .line 71
    new-instance v2, Llan;

    invoke-direct {v2, p1, v1}, Llan;-><init>(Landroid/content/Context;Lnrp;)V

    .line 72
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    if-eqz p2, :cond_1

    .line 77
    iget-object v1, p2, Lnmf;->b:Ljava/lang/String;

    .line 78
    iget-object v2, p2, Lnmf;->c:Ljava/lang/String;

    .line 79
    iget-object v3, p2, Lnmf;->d:Ljava/lang/String;

    :goto_1
    move-object v0, p0

    move v5, p4

    .line 82
    invoke-direct/range {v0 .. v5}, Llbx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    .line 83
    return-void

    :cond_1
    move-object v2, v3

    move-object v1, v3

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Llan;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Lllq;-><init>()V

    .line 46
    invoke-direct/range {p0 .. p5}, Llbx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lnlw;)V
    .locals 6

    .prologue
    .line 49
    invoke-direct {p0}, Lllq;-><init>()V

    .line 50
    if-eqz p1, :cond_0

    .line 51
    iget-object v1, p1, Lnlw;->b:Ljava/lang/String;

    iget-object v2, p1, Lnlw;->c:Ljava/lang/String;

    iget-object v3, p1, Lnlw;->d:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Llbx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    .line 54
    :cond_0
    return-void
.end method

.method public static a([B)Llbx;
    .locals 9

    .prologue
    .line 164
    if-nez p0, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 175
    :goto_0
    return-object v0

    .line 168
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 170
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 171
    invoke-static {v6}, Llbx;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-static {v6}, Llbx;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    .line 173
    invoke-static {v6}, Llbx;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    .line 174
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v7, :cond_1

    invoke-static {v6}, Llan;->a(Ljava/nio/ByteBuffer;)Llan;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 175
    :cond_1
    new-instance v0, Llbx;

    invoke-direct/range {v0 .. v5}, Llbx;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method protected static a(Ljava/io/DataOutputStream;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataOutputStream;",
            "Ljava/util/List",
            "<",
            "Llan;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 181
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    move v2, v0

    .line 182
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v6, v3

    .line 184
    :goto_1
    if-ge v6, v2, :cond_5

    .line 185
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llan;

    .line 186
    invoke-virtual {v0}, Llan;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lllq;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v0}, Llan;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lllq;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v1, v0, Llan;->a:Landroid/text/Spanned;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v1, v0, Llan;->a:Landroid/text/Spanned;

    invoke-static {v1}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v1

    invoke-static {p0, v1}, Lllq;->a(Ljava/io/DataOutputStream;[B)V

    :goto_2
    invoke-virtual {v0}, Llan;->b()Llag;

    move-result-object v1

    invoke-static {v1, p0}, Llag;->a(Llag;Ljava/io/DataOutputStream;)V

    invoke-virtual {v0}, Llan;->a()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {p0, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v5, v3

    :goto_3
    if-ge v5, v8, :cond_3

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llag;

    invoke-static {v1, p0}, Llag;->a(Llag;Ljava/io/DataOutputStream;)V

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_3

    :cond_0
    move v2, v3

    .line 181
    goto :goto_0

    .line 186
    :cond_1
    invoke-virtual {p0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v0}, Llan;->d()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lllq;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lllq;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Llan;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    :goto_4
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 184
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_4
    move v0, v3

    .line 186
    goto :goto_4

    .line 188
    :cond_5
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Llan;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 116
    iput-object p1, p0, Llbx;->a:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Llbx;->b:Ljava/lang/String;

    .line 118
    iput-object p3, p0, Llbx;->c:Ljava/lang/String;

    .line 119
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Llbx;->e:Ljava/util/ArrayList;

    .line 120
    iput p5, p0, Llbx;->d:I

    .line 121
    return-void

    .line 119
    :cond_0
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static a(Llbx;)[B
    .locals 3

    .prologue
    .line 145
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 146
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 148
    :try_start_0
    iget v2, p0, Llbx;->d:I

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 151
    invoke-virtual {p0}, Llbx;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Llbx;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 152
    invoke-virtual {p0}, Llbx;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Llbx;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 153
    invoke-virtual {p0}, Llbx;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Llbx;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0}, Llbx;->d()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Llbx;->a(Ljava/io/DataOutputStream;Ljava/util/List;)V

    .line 155
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 157
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 160
    return-object v0

    .line 157
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    throw v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Llbx;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 209
    iget-object v0, p0, Llbx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    .line 210
    iget-object v0, p0, Llbx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llan;

    .line 211
    invoke-virtual {v0}, Llan;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 212
    invoke-virtual {v0}, Llan;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 213
    invoke-virtual {v0, v1}, Llan;->a(Z)V

    move v0, v1

    .line 217
    :goto_1
    return v0

    .line 209
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 217
    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Llbx;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Llbx;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Llan;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Llbx;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Llbx;->d:I

    return v0
.end method
