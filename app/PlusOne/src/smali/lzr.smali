.class public final Llzr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnbc;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24298
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24301
    iput-object v0, p0, Llzr;->apiHeader:Llyr;

    .line 24304
    iput-object v0, p0, Llzr;->a:Lnbc;

    .line 24298
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 24321
    const/4 v0, 0x0

    .line 24322
    iget-object v1, p0, Llzr;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 24323
    const/4 v0, 0x1

    iget-object v1, p0, Llzr;->apiHeader:Llyr;

    .line 24324
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24326
    :cond_0
    iget-object v1, p0, Llzr;->a:Lnbc;

    if-eqz v1, :cond_1

    .line 24327
    const/4 v1, 0x2

    iget-object v2, p0, Llzr;->a:Lnbc;

    .line 24328
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24330
    :cond_1
    iget-object v1, p0, Llzr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24331
    iput v0, p0, Llzr;->ai:I

    .line 24332
    return v0
.end method

.method public a(Loxn;)Llzr;
    .locals 2

    .prologue
    .line 24340
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24341
    sparse-switch v0, :sswitch_data_0

    .line 24345
    iget-object v1, p0, Llzr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24346
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzr;->ah:Ljava/util/List;

    .line 24349
    :cond_1
    iget-object v1, p0, Llzr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24351
    :sswitch_0
    return-object p0

    .line 24356
    :sswitch_1
    iget-object v0, p0, Llzr;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 24357
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzr;->apiHeader:Llyr;

    .line 24359
    :cond_2
    iget-object v0, p0, Llzr;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24363
    :sswitch_2
    iget-object v0, p0, Llzr;->a:Lnbc;

    if-nez v0, :cond_3

    .line 24364
    new-instance v0, Lnbc;

    invoke-direct {v0}, Lnbc;-><init>()V

    iput-object v0, p0, Llzr;->a:Lnbc;

    .line 24366
    :cond_3
    iget-object v0, p0, Llzr;->a:Lnbc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24341
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24309
    iget-object v0, p0, Llzr;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 24310
    const/4 v0, 0x1

    iget-object v1, p0, Llzr;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24312
    :cond_0
    iget-object v0, p0, Llzr;->a:Lnbc;

    if-eqz v0, :cond_1

    .line 24313
    const/4 v0, 0x2

    iget-object v1, p0, Llzr;->a:Lnbc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24315
    :cond_1
    iget-object v0, p0, Llzr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 24317
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 24294
    invoke-virtual {p0, p1}, Llzr;->a(Loxn;)Llzr;

    move-result-object v0

    return-object v0
.end method
