.class public final Lmor;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lmoq;

.field private e:Lltr;

.field private f:Lmos;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 428
    invoke-direct {p0}, Loxq;-><init>()V

    .line 437
    iput-object v0, p0, Lmor;->d:Lmoq;

    .line 440
    iput-object v0, p0, Lmor;->e:Lltr;

    .line 443
    iput-object v0, p0, Lmor;->f:Lmos;

    .line 428
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 477
    const/4 v0, 0x0

    .line 478
    iget-object v1, p0, Lmor;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 479
    const/4 v0, 0x1

    iget-object v1, p0, Lmor;->a:Ljava/lang/String;

    .line 480
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 482
    :cond_0
    iget-object v1, p0, Lmor;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 483
    const/4 v1, 0x2

    iget-object v2, p0, Lmor;->b:Ljava/lang/String;

    .line 484
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 486
    :cond_1
    iget-object v1, p0, Lmor;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 487
    const/4 v1, 0x3

    iget-object v2, p0, Lmor;->c:Ljava/lang/String;

    .line 488
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    :cond_2
    iget-object v1, p0, Lmor;->d:Lmoq;

    if-eqz v1, :cond_3

    .line 491
    const/4 v1, 0x4

    iget-object v2, p0, Lmor;->d:Lmoq;

    .line 492
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 494
    :cond_3
    iget-object v1, p0, Lmor;->e:Lltr;

    if-eqz v1, :cond_4

    .line 495
    const/4 v1, 0x5

    iget-object v2, p0, Lmor;->e:Lltr;

    .line 496
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 498
    :cond_4
    iget-object v1, p0, Lmor;->f:Lmos;

    if-eqz v1, :cond_5

    .line 499
    const/4 v1, 0x6

    iget-object v2, p0, Lmor;->f:Lmos;

    .line 500
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 502
    :cond_5
    iget-object v1, p0, Lmor;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 503
    const/4 v1, 0x7

    iget-object v2, p0, Lmor;->g:Ljava/lang/Boolean;

    .line 504
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 506
    :cond_6
    iget-object v1, p0, Lmor;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 507
    iput v0, p0, Lmor;->ai:I

    .line 508
    return v0
.end method

.method public a(Loxn;)Lmor;
    .locals 2

    .prologue
    .line 516
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 517
    sparse-switch v0, :sswitch_data_0

    .line 521
    iget-object v1, p0, Lmor;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 522
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmor;->ah:Ljava/util/List;

    .line 525
    :cond_1
    iget-object v1, p0, Lmor;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 527
    :sswitch_0
    return-object p0

    .line 532
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmor;->a:Ljava/lang/String;

    goto :goto_0

    .line 536
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmor;->b:Ljava/lang/String;

    goto :goto_0

    .line 540
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmor;->c:Ljava/lang/String;

    goto :goto_0

    .line 544
    :sswitch_4
    iget-object v0, p0, Lmor;->d:Lmoq;

    if-nez v0, :cond_2

    .line 545
    new-instance v0, Lmoq;

    invoke-direct {v0}, Lmoq;-><init>()V

    iput-object v0, p0, Lmor;->d:Lmoq;

    .line 547
    :cond_2
    iget-object v0, p0, Lmor;->d:Lmoq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 551
    :sswitch_5
    iget-object v0, p0, Lmor;->e:Lltr;

    if-nez v0, :cond_3

    .line 552
    new-instance v0, Lltr;

    invoke-direct {v0}, Lltr;-><init>()V

    iput-object v0, p0, Lmor;->e:Lltr;

    .line 554
    :cond_3
    iget-object v0, p0, Lmor;->e:Lltr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 558
    :sswitch_6
    iget-object v0, p0, Lmor;->f:Lmos;

    if-nez v0, :cond_4

    .line 559
    new-instance v0, Lmos;

    invoke-direct {v0}, Lmos;-><init>()V

    iput-object v0, p0, Lmor;->f:Lmos;

    .line 561
    :cond_4
    iget-object v0, p0, Lmor;->f:Lmos;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 565
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmor;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 517
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lmor;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 451
    const/4 v0, 0x1

    iget-object v1, p0, Lmor;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 453
    :cond_0
    iget-object v0, p0, Lmor;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 454
    const/4 v0, 0x2

    iget-object v1, p0, Lmor;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 456
    :cond_1
    iget-object v0, p0, Lmor;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 457
    const/4 v0, 0x3

    iget-object v1, p0, Lmor;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 459
    :cond_2
    iget-object v0, p0, Lmor;->d:Lmoq;

    if-eqz v0, :cond_3

    .line 460
    const/4 v0, 0x4

    iget-object v1, p0, Lmor;->d:Lmoq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 462
    :cond_3
    iget-object v0, p0, Lmor;->e:Lltr;

    if-eqz v0, :cond_4

    .line 463
    const/4 v0, 0x5

    iget-object v1, p0, Lmor;->e:Lltr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 465
    :cond_4
    iget-object v0, p0, Lmor;->f:Lmos;

    if-eqz v0, :cond_5

    .line 466
    const/4 v0, 0x6

    iget-object v1, p0, Lmor;->f:Lmos;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 468
    :cond_5
    iget-object v0, p0, Lmor;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 469
    const/4 v0, 0x7

    iget-object v1, p0, Lmor;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 471
    :cond_6
    iget-object v0, p0, Lmor;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 473
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 424
    invoke-virtual {p0, p1}, Lmor;->a(Loxn;)Lmor;

    move-result-object v0

    return-object v0
.end method
