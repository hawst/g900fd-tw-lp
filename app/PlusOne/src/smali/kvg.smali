.class final Lkvg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Ligy;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:Landroid/app/Activity;

.field private d:Ligv;

.field private e:Livx;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lkvg;->a:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lkvg;->b:Ljava/lang/String;

    .line 63
    return-void
.end method


# virtual methods
.method a(I)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v3, -0x1

    .line 95
    iget-object v0, p0, Lkvg;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 96
    iget-object v0, p0, Lkvg;->c:Landroid/app/Activity;

    const-class v1, Lkvh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvh;

    iget-object v2, p0, Lkvg;->a:Ljava/lang/String;

    iget-object v1, p0, Lkvg;->b:Ljava/lang/String;

    .line 100
    const-string v4, "moderator"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x2

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move v1, p1

    move v4, v3

    .line 96
    invoke-interface/range {v0 .. v5}, Lkvh;->a(ILjava/lang/String;IILjava/lang/Integer;)Landroid/content/Intent;

    move-result-object v0

    .line 101
    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 102
    if-eqz v1, :cond_0

    .line 103
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 105
    :cond_0
    return-object v0

    .line 100
    :cond_1
    const-string v4, "pending"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, 0x3

    goto :goto_0

    :cond_2
    const-string v4, "banned"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x4

    goto :goto_0

    :cond_3
    const-string v4, "invited"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 6

    .prologue
    .line 75
    iget-object v0, p0, Lkvg;->e:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Liwl;

    new-instance v3, Liwm;

    invoke-direct {v3}, Liwm;-><init>()V

    iget-object v4, p0, Lkvg;->c:Landroid/app/Activity;

    const v5, 0x7f0a04e6

    .line 77
    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v3

    .line 78
    invoke-virtual {v3}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 76
    invoke-virtual {v1, v2, v3}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 81
    return-void
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 1

    .prologue
    .line 68
    iput-object p1, p0, Lkvg;->c:Landroid/app/Activity;

    .line 69
    iput-object p3, p0, Lkvg;->d:Ligv;

    .line 70
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lkvg;->e:Livx;

    .line 71
    return-void
.end method

.method public a(ZIIII)V
    .locals 2

    .prologue
    .line 87
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 88
    iget-object v0, p0, Lkvg;->d:Ligv;

    invoke-virtual {p0, p5}, Lkvg;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Ligv;->a(Landroid/content/Intent;)V

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lkvg;->d:Ligv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_0
.end method
