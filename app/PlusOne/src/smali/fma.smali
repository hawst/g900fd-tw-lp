.class public final Lfma;
.super Lhny;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Lnyq;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILnyq;)V
    .locals 1

    .prologue
    .line 26
    const-string v0, "SetPhotosSettingsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lfma;->a:Landroid/content/Context;

    .line 28
    iput p2, p0, Lfma;->b:I

    .line 29
    iput-object p3, p0, Lfma;->c:Lnyq;

    .line 30
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 34
    .line 36
    :try_start_0
    new-instance v0, Lnyq;

    invoke-direct {v0}, Lnyq;-><init>()V

    iget-object v2, p0, Lfma;->c:Lnyq;

    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnyq;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    new-instance v2, Ldni;

    iget-object v3, p0, Lfma;->a:Landroid/content/Context;

    iget v4, p0, Lfma;->b:I

    invoke-direct {v2, v3, v4, v0}, Ldni;-><init>(Landroid/content/Context;ILnyq;)V

    .line 41
    invoke-virtual {v2}, Lkff;->l()V

    .line 42
    new-instance v0, Lhoz;

    iget v3, v2, Lkff;->i:I

    iget-object v4, v2, Lkff;->k:Ljava/lang/Exception;

    .line 43
    invoke-virtual {v2}, Lkff;->t()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lfma;->f()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a06a9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-direct {v0, v3, v4, v1}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 44
    return-object v0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    const-string v2, "SetPhotosSettingsTask"

    const-string v3, "Failed to load photos settings"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lfma;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a057b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
