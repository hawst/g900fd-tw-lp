.class public final Lmez;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnql;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28834
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28837
    iput-object v0, p0, Lmez;->apiHeader:Llyr;

    .line 28840
    iput-object v0, p0, Lmez;->a:Lnql;

    .line 28834
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 28857
    const/4 v0, 0x0

    .line 28858
    iget-object v1, p0, Lmez;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 28859
    const/4 v0, 0x1

    iget-object v1, p0, Lmez;->apiHeader:Llyr;

    .line 28860
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28862
    :cond_0
    iget-object v1, p0, Lmez;->a:Lnql;

    if-eqz v1, :cond_1

    .line 28863
    const/4 v1, 0x2

    iget-object v2, p0, Lmez;->a:Lnql;

    .line 28864
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28866
    :cond_1
    iget-object v1, p0, Lmez;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28867
    iput v0, p0, Lmez;->ai:I

    .line 28868
    return v0
.end method

.method public a(Loxn;)Lmez;
    .locals 2

    .prologue
    .line 28876
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 28877
    sparse-switch v0, :sswitch_data_0

    .line 28881
    iget-object v1, p0, Lmez;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 28882
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmez;->ah:Ljava/util/List;

    .line 28885
    :cond_1
    iget-object v1, p0, Lmez;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28887
    :sswitch_0
    return-object p0

    .line 28892
    :sswitch_1
    iget-object v0, p0, Lmez;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 28893
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmez;->apiHeader:Llyr;

    .line 28895
    :cond_2
    iget-object v0, p0, Lmez;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28899
    :sswitch_2
    iget-object v0, p0, Lmez;->a:Lnql;

    if-nez v0, :cond_3

    .line 28900
    new-instance v0, Lnql;

    invoke-direct {v0}, Lnql;-><init>()V

    iput-object v0, p0, Lmez;->a:Lnql;

    .line 28902
    :cond_3
    iget-object v0, p0, Lmez;->a:Lnql;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28877
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28845
    iget-object v0, p0, Lmez;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 28846
    const/4 v0, 0x1

    iget-object v1, p0, Lmez;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28848
    :cond_0
    iget-object v0, p0, Lmez;->a:Lnql;

    if-eqz v0, :cond_1

    .line 28849
    const/4 v0, 0x2

    iget-object v1, p0, Lmez;->a:Lnql;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28851
    :cond_1
    iget-object v0, p0, Lmez;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 28853
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 28830
    invoke-virtual {p0, p1}, Lmez;->a(Loxn;)Lmez;

    move-result-object v0

    return-object v0
.end method
