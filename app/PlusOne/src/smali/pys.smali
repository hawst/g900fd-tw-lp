.class public Lpys;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final i:[B


# instance fields
.field a:I

.field b:Z

.field c:[I

.field d:I

.field e:I

.field private final f:[B

.field private g:I

.field private h:I

.field private j:Z

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/16 v0, 0x300

    new-array v0, v0, [B

    sput-object v0, Lpys;->i:[B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v2, 0x100

    new-array v2, v2, [I

    iput-object v2, p0, Lpys;->c:[I

    .line 26
    iput-object p1, p0, Lpys;->f:[B

    .line 28
    new-instance v3, Lpyt;

    invoke-direct {v3, p1}, Lpyt;-><init>([B)V

    .line 30
    :try_start_0
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v2

    const/16 v4, 0x47

    if-ne v2, v4, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v2

    const/16 v4, 0x49

    if-ne v2, v4, :cond_2

    move v2, v1

    :goto_1
    if-eqz v2, :cond_3

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v2

    const/16 v4, 0x46

    if-ne v2, v4, :cond_3

    move v2, v1

    :goto_2
    if-nez v2, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lpys;->b:Z

    .line 31
    :cond_0
    :goto_3
    invoke-virtual {v3}, Lpyt;->a()I

    move-result v0

    iput v0, p0, Lpys;->a:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_4
    :try_start_1
    invoke-virtual {v3}, Lpyt;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 41
    :goto_5
    return-void

    :cond_1
    move v2, v0

    .line 30
    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_1

    :cond_3
    move v2, v0

    goto :goto_2

    :cond_4
    const-wide/16 v4, 0x3

    :try_start_2
    invoke-virtual {v3, v4, v5}, Ljava/io/InputStream;->skip(J)J

    invoke-direct {p0, v3}, Lpys;->a(Ljava/io/InputStream;)I

    move-result v2

    iput v2, p0, Lpys;->g:I

    invoke-direct {p0, v3}, Lpys;->a(Ljava/io/InputStream;)I

    move-result v2

    iput v2, p0, Lpys;->h:I

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v2

    and-int/lit16 v4, v2, 0x80

    if-eqz v4, :cond_5

    move v0, v1

    :cond_5
    iput-boolean v0, p0, Lpys;->j:Z

    const/4 v0, 0x2

    and-int/lit8 v2, v2, 0x7

    shl-int/2addr v0, v2

    iput v0, p0, Lpys;->k:I

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    iput v0, p0, Lpys;->e:I

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/io/InputStream;->skip(J)J

    iget-boolean v0, p0, Lpys;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lpys;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lpys;->c:[I

    iget v2, p0, Lpys;->k:I

    invoke-static {v3, v0, v2}, Lpys;->a(Ljava/io/InputStream;[II)Z

    iget-object v0, p0, Lpys;->c:[I

    iget v2, p0, Lpys;->e:I

    aget v0, v0, v2

    iput v0, p0, Lpys;->d:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 33
    :catch_0
    move-exception v0

    iput-boolean v1, p0, Lpys;->b:Z

    goto :goto_4

    .line 41
    :catch_1
    move-exception v0

    goto :goto_5
.end method

.method private a(Ljava/io/InputStream;)I
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method static a(Ljava/io/InputStream;[II)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 109
    sget-object v3, Lpys;->i:[B

    monitor-enter v3

    .line 110
    mul-int/lit8 v1, p2, 0x3

    .line 111
    :try_start_0
    sget-object v2, Lpys;->i:[B

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 112
    if-ge v2, v1, :cond_0

    .line 113
    monitor-exit v3

    .line 126
    :goto_0
    return v0

    :cond_0
    move v1, v0

    .line 117
    :goto_1
    if-ge v1, p2, :cond_1

    .line 118
    sget-object v2, Lpys;->i:[B

    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v2, v0

    and-int/lit16 v5, v0, 0xff

    .line 119
    sget-object v0, Lpys;->i:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, v0, v4

    and-int/lit16 v4, v0, 0xff

    .line 120
    sget-object v6, Lpys;->i:[B

    add-int/lit8 v0, v2, 0x1

    aget-byte v2, v6, v2

    and-int/lit16 v6, v2, 0xff

    .line 121
    add-int/lit8 v2, v1, 0x1

    const/high16 v7, -0x1000000

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v5, v7

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v4, v5

    or-int/2addr v4, v6

    aput v4, p1, v1

    move v1, v2

    .line 122
    goto :goto_1

    .line 124
    :cond_1
    monitor-exit v3

    .line 126
    const/4 v0, 0x1

    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()[B
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lpys;->f:[B

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lpys;->g:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lpys;->h:I

    return v0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lpys;->f:[B

    array-length v0, v0

    iget-object v1, p0, Lpys;->c:[I

    array-length v1, v1

    shl-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method
