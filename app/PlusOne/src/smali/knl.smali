.class final Lknl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Lkng;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lkng;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lknl;->a:Landroid/net/Uri;

    .line 79
    iput-object p2, p0, Lknl;->b:Lkng;

    .line 80
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 86
    if-ne p0, p1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    instance-of v2, p1, Lknl;

    if-nez v2, :cond_2

    move v0, v1

    .line 91
    goto :goto_0

    .line 94
    :cond_2
    check-cast p1, Lknl;

    .line 96
    iget-object v2, p0, Lknl;->a:Landroid/net/Uri;

    iget-object v3, p1, Lknl;->a:Landroid/net/Uri;

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lknl;->a:Landroid/net/Uri;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lknl;->a:Landroid/net/Uri;

    iget-object v3, p1, Lknl;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v2, p0, Lknl;->b:Lkng;

    iget-object v3, p1, Lknl;->b:Lkng;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lknl;->b:Lkng;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lknl;->b:Lkng;

    iget-object v3, p1, Lknl;->b:Lkng;

    .line 98
    invoke-virtual {v2, v3}, Lkng;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 105
    iget-object v0, p0, Lknl;->a:Landroid/net/Uri;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lknl;->b:Lkng;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 110
    return v0

    .line 105
    :cond_0
    iget-object v0, p0, Lknl;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0

    .line 108
    :cond_1
    iget-object v1, p0, Lknl;->b:Lkng;

    invoke-virtual {v1}, Lkng;->hashCode()I

    move-result v1

    goto :goto_1
.end method
