.class public final Lmkb;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lntw;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16198
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16201
    iput-object v0, p0, Lmkb;->apiHeader:Llyr;

    .line 16204
    iput-object v0, p0, Lmkb;->a:Lntw;

    .line 16198
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 16221
    const/4 v0, 0x0

    .line 16222
    iget-object v1, p0, Lmkb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 16223
    const/4 v0, 0x1

    iget-object v1, p0, Lmkb;->apiHeader:Llyr;

    .line 16224
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16226
    :cond_0
    iget-object v1, p0, Lmkb;->a:Lntw;

    if-eqz v1, :cond_1

    .line 16227
    const/4 v1, 0x2

    iget-object v2, p0, Lmkb;->a:Lntw;

    .line 16228
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16230
    :cond_1
    iget-object v1, p0, Lmkb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16231
    iput v0, p0, Lmkb;->ai:I

    .line 16232
    return v0
.end method

.method public a(Loxn;)Lmkb;
    .locals 2

    .prologue
    .line 16240
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 16241
    sparse-switch v0, :sswitch_data_0

    .line 16245
    iget-object v1, p0, Lmkb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 16246
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkb;->ah:Ljava/util/List;

    .line 16249
    :cond_1
    iget-object v1, p0, Lmkb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16251
    :sswitch_0
    return-object p0

    .line 16256
    :sswitch_1
    iget-object v0, p0, Lmkb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 16257
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkb;->apiHeader:Llyr;

    .line 16259
    :cond_2
    iget-object v0, p0, Lmkb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16263
    :sswitch_2
    iget-object v0, p0, Lmkb;->a:Lntw;

    if-nez v0, :cond_3

    .line 16264
    new-instance v0, Lntw;

    invoke-direct {v0}, Lntw;-><init>()V

    iput-object v0, p0, Lmkb;->a:Lntw;

    .line 16266
    :cond_3
    iget-object v0, p0, Lmkb;->a:Lntw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16241
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 16209
    iget-object v0, p0, Lmkb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 16210
    const/4 v0, 0x1

    iget-object v1, p0, Lmkb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16212
    :cond_0
    iget-object v0, p0, Lmkb;->a:Lntw;

    if-eqz v0, :cond_1

    .line 16213
    const/4 v0, 0x2

    iget-object v1, p0, Lmkb;->a:Lntw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16215
    :cond_1
    iget-object v0, p0, Lmkb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 16217
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16194
    invoke-virtual {p0, p1}, Lmkb;->a(Loxn;)Lmkb;

    move-result-object v0

    return-object v0
.end method
