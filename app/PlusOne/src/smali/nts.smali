.class public final Lnts;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnsr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 578
    invoke-direct {p0}, Loxq;-><init>()V

    .line 581
    sget-object v0, Lnsr;->a:[Lnsr;

    iput-object v0, p0, Lnts;->a:[Lnsr;

    .line 578
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 599
    .line 600
    iget-object v1, p0, Lnts;->a:[Lnsr;

    if-eqz v1, :cond_1

    .line 601
    iget-object v2, p0, Lnts;->a:[Lnsr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 602
    if-eqz v4, :cond_0

    .line 603
    const/4 v5, 0x1

    .line 604
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 601
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 608
    :cond_1
    iget-object v1, p0, Lnts;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 609
    iput v0, p0, Lnts;->ai:I

    .line 610
    return v0
.end method

.method public a(Loxn;)Lnts;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 618
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 619
    sparse-switch v0, :sswitch_data_0

    .line 623
    iget-object v2, p0, Lnts;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 624
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnts;->ah:Ljava/util/List;

    .line 627
    :cond_1
    iget-object v2, p0, Lnts;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 629
    :sswitch_0
    return-object p0

    .line 634
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 635
    iget-object v0, p0, Lnts;->a:[Lnsr;

    if-nez v0, :cond_3

    move v0, v1

    .line 636
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsr;

    .line 637
    iget-object v3, p0, Lnts;->a:[Lnsr;

    if-eqz v3, :cond_2

    .line 638
    iget-object v3, p0, Lnts;->a:[Lnsr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 640
    :cond_2
    iput-object v2, p0, Lnts;->a:[Lnsr;

    .line 641
    :goto_2
    iget-object v2, p0, Lnts;->a:[Lnsr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 642
    iget-object v2, p0, Lnts;->a:[Lnsr;

    new-instance v3, Lnsr;

    invoke-direct {v3}, Lnsr;-><init>()V

    aput-object v3, v2, v0

    .line 643
    iget-object v2, p0, Lnts;->a:[Lnsr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 644
    invoke-virtual {p1}, Loxn;->a()I

    .line 641
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 635
    :cond_3
    iget-object v0, p0, Lnts;->a:[Lnsr;

    array-length v0, v0

    goto :goto_1

    .line 647
    :cond_4
    iget-object v2, p0, Lnts;->a:[Lnsr;

    new-instance v3, Lnsr;

    invoke-direct {v3}, Lnsr;-><init>()V

    aput-object v3, v2, v0

    .line 648
    iget-object v2, p0, Lnts;->a:[Lnsr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 619
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 586
    iget-object v0, p0, Lnts;->a:[Lnsr;

    if-eqz v0, :cond_1

    .line 587
    iget-object v1, p0, Lnts;->a:[Lnsr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 588
    if-eqz v3, :cond_0

    .line 589
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 587
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 593
    :cond_1
    iget-object v0, p0, Lnts;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 595
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 574
    invoke-virtual {p0, p1}, Lnts;->a(Loxn;)Lnts;

    move-result-object v0

    return-object v0
.end method
