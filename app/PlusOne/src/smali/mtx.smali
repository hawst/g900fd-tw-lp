.class public final Lmtx;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:I

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8153
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8174
    const/high16 v0, -0x80000000

    iput v0, p0, Lmtx;->d:I

    .line 8153
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8207
    const/4 v0, 0x0

    .line 8208
    iget-object v1, p0, Lmtx;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 8209
    const/4 v0, 0x1

    iget-object v1, p0, Lmtx;->a:Ljava/lang/Boolean;

    .line 8210
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 8212
    :cond_0
    iget-object v1, p0, Lmtx;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 8213
    const/4 v1, 0x2

    iget-object v2, p0, Lmtx;->b:Ljava/lang/Boolean;

    .line 8214
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8216
    :cond_1
    iget-object v1, p0, Lmtx;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 8217
    const/4 v1, 0x3

    iget-object v2, p0, Lmtx;->c:Ljava/lang/Boolean;

    .line 8218
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8220
    :cond_2
    iget v1, p0, Lmtx;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 8221
    const/4 v1, 0x4

    iget v2, p0, Lmtx;->d:I

    .line 8222
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8224
    :cond_3
    iget-object v1, p0, Lmtx;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 8225
    const/4 v1, 0x5

    iget-object v2, p0, Lmtx;->e:Ljava/lang/Boolean;

    .line 8226
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8228
    :cond_4
    iget-object v1, p0, Lmtx;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 8229
    const/4 v1, 0x6

    iget-object v2, p0, Lmtx;->f:Ljava/lang/Boolean;

    .line 8230
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8232
    :cond_5
    iget-object v1, p0, Lmtx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8233
    iput v0, p0, Lmtx;->ai:I

    .line 8234
    return v0
.end method

.method public a(Loxn;)Lmtx;
    .locals 2

    .prologue
    .line 8242
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8243
    sparse-switch v0, :sswitch_data_0

    .line 8247
    iget-object v1, p0, Lmtx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8248
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtx;->ah:Ljava/util/List;

    .line 8251
    :cond_1
    iget-object v1, p0, Lmtx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8253
    :sswitch_0
    return-object p0

    .line 8258
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtx;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 8262
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtx;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 8266
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtx;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 8270
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 8271
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 8280
    :cond_2
    iput v0, p0, Lmtx;->d:I

    goto :goto_0

    .line 8282
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmtx;->d:I

    goto :goto_0

    .line 8287
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtx;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 8291
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtx;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 8243
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8183
    iget-object v0, p0, Lmtx;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 8184
    const/4 v0, 0x1

    iget-object v1, p0, Lmtx;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8186
    :cond_0
    iget-object v0, p0, Lmtx;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 8187
    const/4 v0, 0x2

    iget-object v1, p0, Lmtx;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8189
    :cond_1
    iget-object v0, p0, Lmtx;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 8190
    const/4 v0, 0x3

    iget-object v1, p0, Lmtx;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8192
    :cond_2
    iget v0, p0, Lmtx;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 8193
    const/4 v0, 0x4

    iget v1, p0, Lmtx;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8195
    :cond_3
    iget-object v0, p0, Lmtx;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 8196
    const/4 v0, 0x5

    iget-object v1, p0, Lmtx;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8198
    :cond_4
    iget-object v0, p0, Lmtx;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 8199
    const/4 v0, 0x6

    iget-object v1, p0, Lmtx;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8201
    :cond_5
    iget-object v0, p0, Lmtx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8203
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8149
    invoke-virtual {p0, p1}, Lmtx;->a(Loxn;)Lmtx;

    move-result-object v0

    return-object v0
.end method
