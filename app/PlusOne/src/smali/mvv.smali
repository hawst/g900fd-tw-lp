.class public final Lmvv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lmvw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6405
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6474
    const/4 v0, 0x0

    iput-object v0, p0, Lmvv;->d:Lmvw;

    .line 6405
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6497
    const/4 v0, 0x0

    .line 6498
    iget-object v1, p0, Lmvv;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 6499
    const/4 v0, 0x1

    iget-object v1, p0, Lmvv;->a:Ljava/lang/Integer;

    .line 6500
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6502
    :cond_0
    iget-object v1, p0, Lmvv;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6503
    const/4 v1, 0x2

    iget-object v2, p0, Lmvv;->b:Ljava/lang/String;

    .line 6504
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6506
    :cond_1
    iget-object v1, p0, Lmvv;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6507
    const/4 v1, 0x3

    iget-object v2, p0, Lmvv;->c:Ljava/lang/String;

    .line 6508
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6510
    :cond_2
    iget-object v1, p0, Lmvv;->d:Lmvw;

    if-eqz v1, :cond_3

    .line 6511
    const/4 v1, 0x4

    iget-object v2, p0, Lmvv;->d:Lmvw;

    .line 6512
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6514
    :cond_3
    iget-object v1, p0, Lmvv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6515
    iput v0, p0, Lmvv;->ai:I

    .line 6516
    return v0
.end method

.method public a(Loxn;)Lmvv;
    .locals 2

    .prologue
    .line 6524
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6525
    sparse-switch v0, :sswitch_data_0

    .line 6529
    iget-object v1, p0, Lmvv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6530
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvv;->ah:Ljava/util/List;

    .line 6533
    :cond_1
    iget-object v1, p0, Lmvv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6535
    :sswitch_0
    return-object p0

    .line 6540
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvv;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 6544
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvv;->b:Ljava/lang/String;

    goto :goto_0

    .line 6548
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvv;->c:Ljava/lang/String;

    goto :goto_0

    .line 6552
    :sswitch_4
    iget-object v0, p0, Lmvv;->d:Lmvw;

    if-nez v0, :cond_2

    .line 6553
    new-instance v0, Lmvw;

    invoke-direct {v0}, Lmvw;-><init>()V

    iput-object v0, p0, Lmvv;->d:Lmvw;

    .line 6555
    :cond_2
    iget-object v0, p0, Lmvv;->d:Lmvw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6525
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6479
    iget-object v0, p0, Lmvv;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 6480
    const/4 v0, 0x1

    iget-object v1, p0, Lmvv;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6482
    :cond_0
    iget-object v0, p0, Lmvv;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 6483
    const/4 v0, 0x2

    iget-object v1, p0, Lmvv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6485
    :cond_1
    iget-object v0, p0, Lmvv;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6486
    const/4 v0, 0x3

    iget-object v1, p0, Lmvv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6488
    :cond_2
    iget-object v0, p0, Lmvv;->d:Lmvw;

    if-eqz v0, :cond_3

    .line 6489
    const/4 v0, 0x4

    iget-object v1, p0, Lmvv;->d:Lmvw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6491
    :cond_3
    iget-object v0, p0, Lmvv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6493
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6401
    invoke-virtual {p0, p1}, Lmvv;->a(Loxn;)Lmvv;

    move-result-object v0

    return-object v0
.end method
