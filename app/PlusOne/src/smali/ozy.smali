.class public final Lozy;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lozy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:[Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/Boolean;

.field public b:Ljava/lang/String;

.field public c:Lpdi;

.field public d:Loya;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Loya;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:[Loya;

.field private n:Loya;

.field private o:[Loya;

.field private p:Ljava/lang/String;

.field private q:Loya;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Loya;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x281bd7d

    new-instance v1, Lozz;

    invoke-direct {v1}, Lozz;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lozy;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lozy;->c:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozy;->m:[Loya;

    .line 35
    iput-object v1, p0, Lozy;->n:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozy;->o:[Loya;

    .line 43
    iput-object v1, p0, Lozy;->d:Loya;

    .line 46
    iput-object v1, p0, Lozy;->q:Loya;

    .line 59
    iput-object v1, p0, Lozy;->g:Loya;

    .line 64
    iput-object v1, p0, Lozy;->v:Loya;

    .line 75
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lozy;->A:[Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 184
    .line 185
    iget-object v0, p0, Lozy;->h:Ljava/lang/String;

    if-eqz v0, :cond_1e

    .line 186
    const/4 v0, 0x1

    iget-object v2, p0, Lozy;->h:Ljava/lang/String;

    .line 187
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 189
    :goto_0
    iget-object v2, p0, Lozy;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 190
    const/4 v2, 0x2

    iget-object v3, p0, Lozy;->i:Ljava/lang/String;

    .line 191
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 193
    :cond_0
    iget-object v2, p0, Lozy;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 194
    const/4 v2, 0x3

    iget-object v3, p0, Lozy;->b:Ljava/lang/String;

    .line 195
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 197
    :cond_1
    iget-object v2, p0, Lozy;->j:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 198
    const/4 v2, 0x4

    iget-object v3, p0, Lozy;->j:Ljava/lang/String;

    .line 199
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 201
    :cond_2
    iget-object v2, p0, Lozy;->k:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 202
    const/4 v2, 0x5

    iget-object v3, p0, Lozy;->k:Ljava/lang/String;

    .line 203
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 205
    :cond_3
    iget-object v2, p0, Lozy;->c:Lpdi;

    if-eqz v2, :cond_4

    .line 206
    const/4 v2, 0x6

    iget-object v3, p0, Lozy;->c:Lpdi;

    .line 207
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 209
    :cond_4
    iget-object v2, p0, Lozy;->l:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 210
    const/4 v2, 0x7

    iget-object v3, p0, Lozy;->l:Ljava/lang/String;

    .line 211
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 213
    :cond_5
    iget-object v2, p0, Lozy;->m:[Loya;

    if-eqz v2, :cond_7

    .line 214
    iget-object v3, p0, Lozy;->m:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 215
    if-eqz v5, :cond_6

    .line 216
    const/16 v6, 0x8

    .line 217
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 214
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 221
    :cond_7
    iget-object v2, p0, Lozy;->n:Loya;

    if-eqz v2, :cond_8

    .line 222
    const/16 v2, 0x9

    iget-object v3, p0, Lozy;->n:Loya;

    .line 223
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 225
    :cond_8
    iget-object v2, p0, Lozy;->o:[Loya;

    if-eqz v2, :cond_a

    .line 226
    iget-object v3, p0, Lozy;->o:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    .line 227
    if-eqz v5, :cond_9

    .line 228
    const/16 v6, 0xb

    .line 229
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 226
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 233
    :cond_a
    iget-object v2, p0, Lozy;->p:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 234
    const/16 v2, 0xc

    iget-object v3, p0, Lozy;->p:Ljava/lang/String;

    .line 235
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 237
    :cond_b
    iget-object v2, p0, Lozy;->d:Loya;

    if-eqz v2, :cond_c

    .line 238
    const/16 v2, 0x18

    iget-object v3, p0, Lozy;->d:Loya;

    .line 239
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 241
    :cond_c
    iget-object v2, p0, Lozy;->q:Loya;

    if-eqz v2, :cond_d

    .line 242
    const/16 v2, 0x19

    iget-object v3, p0, Lozy;->q:Loya;

    .line 243
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 245
    :cond_d
    iget-object v2, p0, Lozy;->e:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 246
    const/16 v2, 0x1a

    iget-object v3, p0, Lozy;->e:Ljava/lang/String;

    .line 247
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 249
    :cond_e
    iget-object v2, p0, Lozy;->f:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 250
    const/16 v2, 0x1b

    iget-object v3, p0, Lozy;->f:Ljava/lang/String;

    .line 251
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 253
    :cond_f
    iget-object v2, p0, Lozy;->r:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 254
    const/16 v2, 0x1c

    iget-object v3, p0, Lozy;->r:Ljava/lang/String;

    .line 255
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 257
    :cond_10
    iget-object v2, p0, Lozy;->s:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 258
    const/16 v2, 0x1d

    iget-object v3, p0, Lozy;->s:Ljava/lang/String;

    .line 259
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 261
    :cond_11
    iget-object v2, p0, Lozy;->t:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 262
    const/16 v2, 0x4b

    iget-object v3, p0, Lozy;->t:Ljava/lang/String;

    .line 263
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 265
    :cond_12
    iget-object v2, p0, Lozy;->g:Loya;

    if-eqz v2, :cond_13

    .line 266
    const/16 v2, 0x52

    iget-object v3, p0, Lozy;->g:Loya;

    .line 267
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 269
    :cond_13
    iget-object v2, p0, Lozy;->u:Ljava/lang/String;

    if-eqz v2, :cond_14

    .line 270
    const/16 v2, 0x6a

    iget-object v3, p0, Lozy;->u:Ljava/lang/String;

    .line 271
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 273
    :cond_14
    iget-object v2, p0, Lozy;->v:Loya;

    if-eqz v2, :cond_15

    .line 274
    const/16 v2, 0xb9

    iget-object v3, p0, Lozy;->v:Loya;

    .line 275
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 277
    :cond_15
    iget-object v2, p0, Lozy;->w:Ljava/lang/String;

    if-eqz v2, :cond_16

    .line 278
    const/16 v2, 0xe3

    iget-object v3, p0, Lozy;->w:Ljava/lang/String;

    .line 279
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 281
    :cond_16
    iget-object v2, p0, Lozy;->x:Ljava/lang/String;

    if-eqz v2, :cond_17

    .line 282
    const/16 v2, 0xe4

    iget-object v3, p0, Lozy;->x:Ljava/lang/String;

    .line 283
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 285
    :cond_17
    iget-object v2, p0, Lozy;->y:Ljava/lang/String;

    if-eqz v2, :cond_18

    .line 286
    const/16 v2, 0xe5

    iget-object v3, p0, Lozy;->y:Ljava/lang/String;

    .line 287
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 289
    :cond_18
    iget-object v2, p0, Lozy;->z:Ljava/lang/String;

    if-eqz v2, :cond_19

    .line 290
    const/16 v2, 0xe6

    iget-object v3, p0, Lozy;->z:Ljava/lang/String;

    .line 291
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 293
    :cond_19
    iget-object v2, p0, Lozy;->A:[Ljava/lang/String;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lozy;->A:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1b

    .line 295
    iget-object v3, p0, Lozy;->A:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_1a

    aget-object v5, v3, v1

    .line 297
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 295
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 299
    :cond_1a
    add-int/2addr v0, v2

    .line 300
    iget-object v1, p0, Lozy;->A:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 302
    :cond_1b
    iget-object v1, p0, Lozy;->B:Ljava/lang/String;

    if-eqz v1, :cond_1c

    .line 303
    const/16 v1, 0xfe

    iget-object v2, p0, Lozy;->B:Ljava/lang/String;

    .line 304
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 306
    :cond_1c
    iget-object v1, p0, Lozy;->C:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    .line 307
    const/16 v1, 0x10b

    iget-object v2, p0, Lozy;->C:Ljava/lang/Boolean;

    .line 308
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 310
    :cond_1d
    iget-object v1, p0, Lozy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    iput v0, p0, Lozy;->ai:I

    .line 312
    return v0

    :cond_1e
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lozy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 320
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 321
    sparse-switch v0, :sswitch_data_0

    .line 325
    iget-object v2, p0, Lozy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 326
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lozy;->ah:Ljava/util/List;

    .line 329
    :cond_1
    iget-object v2, p0, Lozy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    :sswitch_0
    return-object p0

    .line 336
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->h:Ljava/lang/String;

    goto :goto_0

    .line 340
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->i:Ljava/lang/String;

    goto :goto_0

    .line 344
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->b:Ljava/lang/String;

    goto :goto_0

    .line 348
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->j:Ljava/lang/String;

    goto :goto_0

    .line 352
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->k:Ljava/lang/String;

    goto :goto_0

    .line 356
    :sswitch_6
    iget-object v0, p0, Lozy;->c:Lpdi;

    if-nez v0, :cond_2

    .line 357
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lozy;->c:Lpdi;

    .line 359
    :cond_2
    iget-object v0, p0, Lozy;->c:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 363
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->l:Ljava/lang/String;

    goto :goto_0

    .line 367
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 368
    iget-object v0, p0, Lozy;->m:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 369
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 370
    iget-object v3, p0, Lozy;->m:[Loya;

    if-eqz v3, :cond_3

    .line 371
    iget-object v3, p0, Lozy;->m:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 373
    :cond_3
    iput-object v2, p0, Lozy;->m:[Loya;

    .line 374
    :goto_2
    iget-object v2, p0, Lozy;->m:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 375
    iget-object v2, p0, Lozy;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 376
    iget-object v2, p0, Lozy;->m:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 377
    invoke-virtual {p1}, Loxn;->a()I

    .line 374
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 368
    :cond_4
    iget-object v0, p0, Lozy;->m:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 380
    :cond_5
    iget-object v2, p0, Lozy;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 381
    iget-object v2, p0, Lozy;->m:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 385
    :sswitch_9
    iget-object v0, p0, Lozy;->n:Loya;

    if-nez v0, :cond_6

    .line 386
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozy;->n:Loya;

    .line 388
    :cond_6
    iget-object v0, p0, Lozy;->n:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 392
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 393
    iget-object v0, p0, Lozy;->o:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 394
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 395
    iget-object v3, p0, Lozy;->o:[Loya;

    if-eqz v3, :cond_7

    .line 396
    iget-object v3, p0, Lozy;->o:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 398
    :cond_7
    iput-object v2, p0, Lozy;->o:[Loya;

    .line 399
    :goto_4
    iget-object v2, p0, Lozy;->o:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 400
    iget-object v2, p0, Lozy;->o:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 401
    iget-object v2, p0, Lozy;->o:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 402
    invoke-virtual {p1}, Loxn;->a()I

    .line 399
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 393
    :cond_8
    iget-object v0, p0, Lozy;->o:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 405
    :cond_9
    iget-object v2, p0, Lozy;->o:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 406
    iget-object v2, p0, Lozy;->o:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 410
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 414
    :sswitch_c
    iget-object v0, p0, Lozy;->d:Loya;

    if-nez v0, :cond_a

    .line 415
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozy;->d:Loya;

    .line 417
    :cond_a
    iget-object v0, p0, Lozy;->d:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 421
    :sswitch_d
    iget-object v0, p0, Lozy;->q:Loya;

    if-nez v0, :cond_b

    .line 422
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozy;->q:Loya;

    .line 424
    :cond_b
    iget-object v0, p0, Lozy;->q:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 428
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 432
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 436
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 440
    :sswitch_11
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 444
    :sswitch_12
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 448
    :sswitch_13
    iget-object v0, p0, Lozy;->g:Loya;

    if-nez v0, :cond_c

    .line 449
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozy;->g:Loya;

    .line 451
    :cond_c
    iget-object v0, p0, Lozy;->g:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 455
    :sswitch_14
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 459
    :sswitch_15
    iget-object v0, p0, Lozy;->v:Loya;

    if-nez v0, :cond_d

    .line 460
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozy;->v:Loya;

    .line 462
    :cond_d
    iget-object v0, p0, Lozy;->v:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 466
    :sswitch_16
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 470
    :sswitch_17
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 474
    :sswitch_18
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 478
    :sswitch_19
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->z:Ljava/lang/String;

    goto/16 :goto_0

    .line 482
    :sswitch_1a
    const/16 v0, 0x73a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 483
    iget-object v0, p0, Lozy;->A:[Ljava/lang/String;

    array-length v0, v0

    .line 484
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 485
    iget-object v3, p0, Lozy;->A:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 486
    iput-object v2, p0, Lozy;->A:[Ljava/lang/String;

    .line 487
    :goto_5
    iget-object v2, p0, Lozy;->A:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    .line 488
    iget-object v2, p0, Lozy;->A:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 489
    invoke-virtual {p1}, Loxn;->a()I

    .line 487
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 492
    :cond_e
    iget-object v2, p0, Lozy;->A:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 496
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozy;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 500
    :sswitch_1c
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lozy;->C:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 321
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0xc2 -> :sswitch_c
        0xca -> :sswitch_d
        0xd2 -> :sswitch_e
        0xda -> :sswitch_f
        0xe2 -> :sswitch_10
        0xea -> :sswitch_11
        0x25a -> :sswitch_12
        0x292 -> :sswitch_13
        0x352 -> :sswitch_14
        0x5ca -> :sswitch_15
        0x71a -> :sswitch_16
        0x722 -> :sswitch_17
        0x72a -> :sswitch_18
        0x732 -> :sswitch_19
        0x73a -> :sswitch_1a
        0x7f2 -> :sswitch_1b
        0x858 -> :sswitch_1c
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 84
    iget-object v1, p0, Lozy;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 85
    const/4 v1, 0x1

    iget-object v2, p0, Lozy;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 87
    :cond_0
    iget-object v1, p0, Lozy;->i:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 88
    const/4 v1, 0x2

    iget-object v2, p0, Lozy;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 90
    :cond_1
    iget-object v1, p0, Lozy;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 91
    const/4 v1, 0x3

    iget-object v2, p0, Lozy;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 93
    :cond_2
    iget-object v1, p0, Lozy;->j:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 94
    const/4 v1, 0x4

    iget-object v2, p0, Lozy;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 96
    :cond_3
    iget-object v1, p0, Lozy;->k:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 97
    const/4 v1, 0x5

    iget-object v2, p0, Lozy;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 99
    :cond_4
    iget-object v1, p0, Lozy;->c:Lpdi;

    if-eqz v1, :cond_5

    .line 100
    const/4 v1, 0x6

    iget-object v2, p0, Lozy;->c:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 102
    :cond_5
    iget-object v1, p0, Lozy;->l:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 103
    const/4 v1, 0x7

    iget-object v2, p0, Lozy;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 105
    :cond_6
    iget-object v1, p0, Lozy;->m:[Loya;

    if-eqz v1, :cond_8

    .line 106
    iget-object v2, p0, Lozy;->m:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 107
    if-eqz v4, :cond_7

    .line 108
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 106
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 112
    :cond_8
    iget-object v1, p0, Lozy;->n:Loya;

    if-eqz v1, :cond_9

    .line 113
    const/16 v1, 0x9

    iget-object v2, p0, Lozy;->n:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 115
    :cond_9
    iget-object v1, p0, Lozy;->o:[Loya;

    if-eqz v1, :cond_b

    .line 116
    iget-object v2, p0, Lozy;->o:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 117
    if-eqz v4, :cond_a

    .line 118
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 116
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 122
    :cond_b
    iget-object v1, p0, Lozy;->p:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 123
    const/16 v1, 0xc

    iget-object v2, p0, Lozy;->p:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 125
    :cond_c
    iget-object v1, p0, Lozy;->d:Loya;

    if-eqz v1, :cond_d

    .line 126
    const/16 v1, 0x18

    iget-object v2, p0, Lozy;->d:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 128
    :cond_d
    iget-object v1, p0, Lozy;->q:Loya;

    if-eqz v1, :cond_e

    .line 129
    const/16 v1, 0x19

    iget-object v2, p0, Lozy;->q:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 131
    :cond_e
    iget-object v1, p0, Lozy;->e:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 132
    const/16 v1, 0x1a

    iget-object v2, p0, Lozy;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 134
    :cond_f
    iget-object v1, p0, Lozy;->f:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 135
    const/16 v1, 0x1b

    iget-object v2, p0, Lozy;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 137
    :cond_10
    iget-object v1, p0, Lozy;->r:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 138
    const/16 v1, 0x1c

    iget-object v2, p0, Lozy;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 140
    :cond_11
    iget-object v1, p0, Lozy;->s:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 141
    const/16 v1, 0x1d

    iget-object v2, p0, Lozy;->s:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 143
    :cond_12
    iget-object v1, p0, Lozy;->t:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 144
    const/16 v1, 0x4b

    iget-object v2, p0, Lozy;->t:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 146
    :cond_13
    iget-object v1, p0, Lozy;->g:Loya;

    if-eqz v1, :cond_14

    .line 147
    const/16 v1, 0x52

    iget-object v2, p0, Lozy;->g:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 149
    :cond_14
    iget-object v1, p0, Lozy;->u:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 150
    const/16 v1, 0x6a

    iget-object v2, p0, Lozy;->u:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 152
    :cond_15
    iget-object v1, p0, Lozy;->v:Loya;

    if-eqz v1, :cond_16

    .line 153
    const/16 v1, 0xb9

    iget-object v2, p0, Lozy;->v:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 155
    :cond_16
    iget-object v1, p0, Lozy;->w:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 156
    const/16 v1, 0xe3

    iget-object v2, p0, Lozy;->w:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 158
    :cond_17
    iget-object v1, p0, Lozy;->x:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 159
    const/16 v1, 0xe4

    iget-object v2, p0, Lozy;->x:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 161
    :cond_18
    iget-object v1, p0, Lozy;->y:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 162
    const/16 v1, 0xe5

    iget-object v2, p0, Lozy;->y:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 164
    :cond_19
    iget-object v1, p0, Lozy;->z:Ljava/lang/String;

    if-eqz v1, :cond_1a

    .line 165
    const/16 v1, 0xe6

    iget-object v2, p0, Lozy;->z:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 167
    :cond_1a
    iget-object v1, p0, Lozy;->A:[Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 168
    iget-object v1, p0, Lozy;->A:[Ljava/lang/String;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_1b

    aget-object v3, v1, v0

    .line 169
    const/16 v4, 0xe7

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 172
    :cond_1b
    iget-object v0, p0, Lozy;->B:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 173
    const/16 v0, 0xfe

    iget-object v1, p0, Lozy;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 175
    :cond_1c
    iget-object v0, p0, Lozy;->C:Ljava/lang/Boolean;

    if-eqz v0, :cond_1d

    .line 176
    const/16 v0, 0x10b

    iget-object v1, p0, Lozy;->C:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 178
    :cond_1d
    iget-object v0, p0, Lozy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 180
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lozy;->a(Loxn;)Lozy;

    move-result-object v0

    return-object v0
.end method
