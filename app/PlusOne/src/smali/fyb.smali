.class final Lfyb;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private synthetic c:Lfya;


# direct methods
.method public constructor <init>(Lfya;Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lfyb;->c:Lfya;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 170
    iput-boolean p3, p0, Lfyb;->b:Z

    .line 171
    iput-object p2, p0, Lfyb;->a:Landroid/content/Context;

    .line 172
    return-void
.end method

.method static synthetic a(Lfyb;I)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    .line 159
    const/4 v1, -0x1

    if-eq p1, v2, :cond_0

    const/4 v3, 0x3

    if-ne p1, v3, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v3, 0x6

    if-eq p1, v3, :cond_3

    iget-boolean v3, p0, Lfyb;->b:Z

    if-eqz v3, :cond_4

    if-ne p1, v0, :cond_4

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    if-eq p1, v0, :cond_1

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lfyb;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 270
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 275
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const v3, 0x7f10013b

    const/4 v0, 0x0

    .line 204
    packed-switch p1, :pswitch_data_0

    .line 224
    const/4 v1, 0x0

    move-object v2, v1

    move v1, v0

    .line 229
    :goto_0
    if-eqz v2, :cond_0

    .line 230
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 231
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lfyb;->c:Lfya;

    invoke-virtual {v3}, Lfya;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sparse-switch v1, :sswitch_data_0

    const v1, 0x7f0a090d

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    :cond_0
    return-object v2

    .line 206
    :pswitch_0
    iget-object v1, p0, Lfyb;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04009a

    .line 207
    invoke-virtual {v1, v2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 208
    const/4 v0, 0x1

    move-object v2, v1

    move v1, v0

    .line 209
    goto :goto_0

    .line 211
    :pswitch_1
    iget-boolean v1, p0, Lfyb;->b:Z

    if-nez v1, :cond_1

    .line 212
    iget-object v1, p0, Lfyb;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04009c

    .line 213
    invoke-virtual {v1, v2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 214
    const/4 v0, 0x6

    move-object v2, v1

    move v1, v0

    .line 215
    goto :goto_0

    .line 219
    :cond_1
    :pswitch_2
    iget-object v1, p0, Lfyb;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04009d

    .line 220
    invoke-virtual {v1, v2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 221
    const/4 v0, 0x2

    move-object v2, v1

    move v1, v0

    .line 222
    goto :goto_0

    .line 231
    :sswitch_0
    const v1, 0x7f0a0916

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :sswitch_1
    const v1, 0x7f0a0917

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 231
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x6 -> :sswitch_0
    .end sparse-switch
.end method
