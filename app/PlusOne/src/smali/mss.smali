.class public final Lmss;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3860
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3896
    const/4 v0, 0x0

    .line 3897
    iget-object v1, p0, Lmss;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3898
    const/4 v0, 0x1

    iget-object v1, p0, Lmss;->a:Ljava/lang/String;

    .line 3899
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3901
    :cond_0
    iget-object v1, p0, Lmss;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3902
    const/4 v1, 0x2

    iget-object v2, p0, Lmss;->b:Ljava/lang/Integer;

    .line 3903
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3905
    :cond_1
    iget-object v1, p0, Lmss;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3906
    const/4 v1, 0x3

    iget-object v2, p0, Lmss;->c:Ljava/lang/Boolean;

    .line 3907
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3909
    :cond_2
    iget-object v1, p0, Lmss;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 3910
    const/4 v1, 0x4

    iget-object v2, p0, Lmss;->d:Ljava/lang/Boolean;

    .line 3911
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3913
    :cond_3
    iget-object v1, p0, Lmss;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 3914
    const/4 v1, 0x5

    iget-object v2, p0, Lmss;->e:Ljava/lang/Integer;

    .line 3915
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3917
    :cond_4
    iget-object v1, p0, Lmss;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3918
    iput v0, p0, Lmss;->ai:I

    .line 3919
    return v0
.end method

.method public a(Loxn;)Lmss;
    .locals 2

    .prologue
    .line 3927
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3928
    sparse-switch v0, :sswitch_data_0

    .line 3932
    iget-object v1, p0, Lmss;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3933
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmss;->ah:Ljava/util/List;

    .line 3936
    :cond_1
    iget-object v1, p0, Lmss;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3938
    :sswitch_0
    return-object p0

    .line 3943
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmss;->a:Ljava/lang/String;

    goto :goto_0

    .line 3947
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmss;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 3951
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmss;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 3955
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmss;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 3959
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmss;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 3928
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3875
    iget-object v0, p0, Lmss;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3876
    const/4 v0, 0x1

    iget-object v1, p0, Lmss;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3878
    :cond_0
    iget-object v0, p0, Lmss;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3879
    const/4 v0, 0x2

    iget-object v1, p0, Lmss;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3881
    :cond_1
    iget-object v0, p0, Lmss;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3882
    const/4 v0, 0x3

    iget-object v1, p0, Lmss;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3884
    :cond_2
    iget-object v0, p0, Lmss;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 3885
    const/4 v0, 0x4

    iget-object v1, p0, Lmss;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3887
    :cond_3
    iget-object v0, p0, Lmss;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 3888
    const/4 v0, 0x5

    iget-object v1, p0, Lmss;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3890
    :cond_4
    iget-object v0, p0, Lmss;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3892
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3856
    invoke-virtual {p0, p1}, Lmss;->a(Loxn;)Lmss;

    move-result-object v0

    return-object v0
.end method
