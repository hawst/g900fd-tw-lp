.class public final Lnjy;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnjy;


# instance fields
.field public b:Lnja;

.field public c:Lnie;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6933
    const/4 v0, 0x0

    new-array v0, v0, [Lnjy;

    sput-object v0, Lnjy;->a:[Lnjy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6934
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6937
    iput-object v0, p0, Lnjy;->b:Lnja;

    .line 6940
    iput-object v0, p0, Lnjy;->c:Lnie;

    .line 6934
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6967
    const/4 v0, 0x0

    .line 6968
    iget-object v1, p0, Lnjy;->b:Lnja;

    if-eqz v1, :cond_0

    .line 6969
    const/4 v0, 0x1

    iget-object v1, p0, Lnjy;->b:Lnja;

    .line 6970
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6972
    :cond_0
    iget-object v1, p0, Lnjy;->c:Lnie;

    if-eqz v1, :cond_1

    .line 6973
    const/4 v1, 0x2

    iget-object v2, p0, Lnjy;->c:Lnie;

    .line 6974
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6976
    :cond_1
    iget-object v1, p0, Lnjy;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6977
    const/4 v1, 0x3

    iget-object v2, p0, Lnjy;->d:Ljava/lang/String;

    .line 6978
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6980
    :cond_2
    iget-object v1, p0, Lnjy;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 6981
    const/4 v1, 0x4

    iget-object v2, p0, Lnjy;->e:Ljava/lang/Boolean;

    .line 6982
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6984
    :cond_3
    iget-object v1, p0, Lnjy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6985
    iput v0, p0, Lnjy;->ai:I

    .line 6986
    return v0
.end method

.method public a(Loxn;)Lnjy;
    .locals 2

    .prologue
    .line 6994
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6995
    sparse-switch v0, :sswitch_data_0

    .line 6999
    iget-object v1, p0, Lnjy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7000
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjy;->ah:Ljava/util/List;

    .line 7003
    :cond_1
    iget-object v1, p0, Lnjy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7005
    :sswitch_0
    return-object p0

    .line 7010
    :sswitch_1
    iget-object v0, p0, Lnjy;->b:Lnja;

    if-nez v0, :cond_2

    .line 7011
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnjy;->b:Lnja;

    .line 7013
    :cond_2
    iget-object v0, p0, Lnjy;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7017
    :sswitch_2
    iget-object v0, p0, Lnjy;->c:Lnie;

    if-nez v0, :cond_3

    .line 7018
    new-instance v0, Lnie;

    invoke-direct {v0}, Lnie;-><init>()V

    iput-object v0, p0, Lnjy;->c:Lnie;

    .line 7020
    :cond_3
    iget-object v0, p0, Lnjy;->c:Lnie;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7024
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjy;->d:Ljava/lang/String;

    goto :goto_0

    .line 7028
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnjy;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 6995
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6949
    iget-object v0, p0, Lnjy;->b:Lnja;

    if-eqz v0, :cond_0

    .line 6950
    const/4 v0, 0x1

    iget-object v1, p0, Lnjy;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6952
    :cond_0
    iget-object v0, p0, Lnjy;->c:Lnie;

    if-eqz v0, :cond_1

    .line 6953
    const/4 v0, 0x2

    iget-object v1, p0, Lnjy;->c:Lnie;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6955
    :cond_1
    iget-object v0, p0, Lnjy;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6956
    const/4 v0, 0x3

    iget-object v1, p0, Lnjy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6958
    :cond_2
    iget-object v0, p0, Lnjy;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 6959
    const/4 v0, 0x4

    iget-object v1, p0, Lnjy;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6961
    :cond_3
    iget-object v0, p0, Lnjy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6963
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6930
    invoke-virtual {p0, p1}, Lnjy;->a(Loxn;)Lnjy;

    move-result-object v0

    return-object v0
.end method
