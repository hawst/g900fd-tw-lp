.class public final Llkf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Llkg;

.field private b:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 2686
    iget v1, p0, Llkf;->c:I

    .line 2687
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2688
    iget-object v2, p0, Llkf;->b:[Ljava/util/ArrayList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2687
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2690
    :cond_0
    iget-object v0, p0, Llkf;->e:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    .line 2691
    iget-object v0, p0, Llkf;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2693
    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 2668
    if-gtz p1, :cond_0

    .line 2669
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Must have at least one view type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " types reported)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2672
    :cond_0
    iget v0, p0, Llkf;->c:I

    if-ne p1, v0, :cond_1

    .line 2683
    :goto_0
    return-void

    .line 2677
    :cond_1
    new-array v1, p1, [Ljava/util/ArrayList;

    .line 2678
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_2

    .line 2679
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    .line 2678
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2681
    :cond_2
    iput p1, p0, Llkf;->c:I

    .line 2682
    iput-object v1, p0, Llkf;->b:[Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 2702
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llka;

    .line 2703
    iget v1, v0, Llka;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 2727
    :cond_0
    :goto_0
    return-void

    .line 2707
    :cond_1
    invoke-static {p1}, Liu;->b(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2708
    iget-object v1, p0, Llkf;->e:Landroid/util/SparseArray;

    if-nez v1, :cond_2

    .line 2709
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Llkf;->e:Landroid/util/SparseArray;

    .line 2711
    :cond_2
    iget-object v1, p0, Llkf;->e:Landroid/util/SparseArray;

    iget v0, v0, Llka;->c:I

    invoke-virtual {v1, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 2715
    :cond_3
    iget v1, p0, Llkf;->d:I

    if-le p2, v1, :cond_4

    .line 2716
    iput p2, p0, Llkf;->d:I

    .line 2719
    :cond_4
    iget-object v1, p0, Llkf;->b:[Ljava/util/ArrayList;

    iget v0, v0, Llka;->d:I

    aget-object v0, v1, v0

    .line 2720
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Llkf;->d:I

    if-ge v1, v2, :cond_5

    .line 2721
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2724
    :cond_5
    iget-object v0, p0, Llkf;->a:Llkg;

    if-eqz v0, :cond_0

    .line 2725
    iget-object v0, p0, Llkf;->a:Llkg;

    invoke-interface {v0, p1}, Llkg;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 2730
    iget-object v0, p0, Llkf;->e:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    .line 2731
    const/4 v0, 0x0

    .line 2738
    :cond_0
    :goto_0
    return-object v0

    .line 2734
    :cond_1
    iget-object v0, p0, Llkf;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2735
    if-eqz v0, :cond_0

    .line 2736
    iget-object v1, p0, Llkf;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 2696
    iget-object v0, p0, Llkf;->e:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 2697
    iget-object v0, p0, Llkf;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2699
    :cond_0
    return-void
.end method

.method public c(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2742
    if-gez p1, :cond_1

    .line 2754
    :cond_0
    :goto_0
    return-object v0

    .line 2746
    :cond_1
    iget-object v1, p0, Llkf;->b:[Ljava/util/ArrayList;

    aget-object v1, v1, p1

    .line 2747
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2751
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    .line 2752
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2753
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method
