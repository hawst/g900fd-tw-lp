.class public final Lppz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lppz;


# instance fields
.field private b:Lppf;

.field private c:[Lpwm;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lppz;

    sput-object v0, Lppz;->a:[Lppz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lppz;->b:Lppf;

    .line 26
    sget-object v0, Lpwm;->a:[Lpwm;

    iput-object v0, p0, Lppz;->c:[Lpwm;

    .line 20
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 65
    .line 66
    iget-object v0, p0, Lppz;->b:Lppf;

    if-eqz v0, :cond_5

    .line 67
    const/4 v0, 0x1

    iget-object v2, p0, Lppz;->b:Lppf;

    .line 68
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 70
    :goto_0
    iget-object v2, p0, Lppz;->c:[Lpwm;

    if-eqz v2, :cond_1

    .line 71
    iget-object v2, p0, Lppz;->c:[Lpwm;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 72
    if-eqz v4, :cond_0

    .line 73
    const/4 v5, 0x2

    .line 74
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 71
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 78
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Lppz;->d:Ljava/lang/String;

    .line 79
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    iget-object v1, p0, Lppz;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 81
    const/4 v1, 0x4

    iget-object v2, p0, Lppz;->e:Ljava/lang/String;

    .line 82
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_2
    iget-object v1, p0, Lppz;->f:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 85
    const/4 v1, 0x5

    iget-object v2, p0, Lppz;->f:Ljava/lang/Long;

    .line 86
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_3
    iget-object v1, p0, Lppz;->g:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 89
    const/4 v1, 0x6

    iget-object v2, p0, Lppz;->g:Ljava/lang/Long;

    .line 90
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_4
    iget-object v1, p0, Lppz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    iput v0, p0, Lppz;->ai:I

    .line 94
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lppz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 102
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 103
    sparse-switch v0, :sswitch_data_0

    .line 107
    iget-object v2, p0, Lppz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 108
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lppz;->ah:Ljava/util/List;

    .line 111
    :cond_1
    iget-object v2, p0, Lppz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    :sswitch_0
    return-object p0

    .line 118
    :sswitch_1
    iget-object v0, p0, Lppz;->b:Lppf;

    if-nez v0, :cond_2

    .line 119
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lppz;->b:Lppf;

    .line 121
    :cond_2
    iget-object v0, p0, Lppz;->b:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 125
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 126
    iget-object v0, p0, Lppz;->c:[Lpwm;

    if-nez v0, :cond_4

    move v0, v1

    .line 127
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpwm;

    .line 128
    iget-object v3, p0, Lppz;->c:[Lpwm;

    if-eqz v3, :cond_3

    .line 129
    iget-object v3, p0, Lppz;->c:[Lpwm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131
    :cond_3
    iput-object v2, p0, Lppz;->c:[Lpwm;

    .line 132
    :goto_2
    iget-object v2, p0, Lppz;->c:[Lpwm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 133
    iget-object v2, p0, Lppz;->c:[Lpwm;

    new-instance v3, Lpwm;

    invoke-direct {v3}, Lpwm;-><init>()V

    aput-object v3, v2, v0

    .line 134
    iget-object v2, p0, Lppz;->c:[Lpwm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 135
    invoke-virtual {p1}, Loxn;->a()I

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 126
    :cond_4
    iget-object v0, p0, Lppz;->c:[Lpwm;

    array-length v0, v0

    goto :goto_1

    .line 138
    :cond_5
    iget-object v2, p0, Lppz;->c:[Lpwm;

    new-instance v3, Lpwm;

    invoke-direct {v3}, Lpwm;-><init>()V

    aput-object v3, v2, v0

    .line 139
    iget-object v2, p0, Lppz;->c:[Lpwm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 143
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppz;->d:Ljava/lang/String;

    goto :goto_0

    .line 147
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppz;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 151
    :sswitch_5
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lppz;->f:Ljava/lang/Long;

    goto/16 :goto_0

    .line 155
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lppz;->g:Ljava/lang/Long;

    goto/16 :goto_0

    .line 103
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 39
    iget-object v0, p0, Lppz;->b:Lppf;

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x1

    iget-object v1, p0, Lppz;->b:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_0
    iget-object v0, p0, Lppz;->c:[Lpwm;

    if-eqz v0, :cond_2

    .line 43
    iget-object v1, p0, Lppz;->c:[Lpwm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 44
    if-eqz v3, :cond_1

    .line 45
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 43
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_2
    const/4 v0, 0x3

    iget-object v1, p0, Lppz;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 50
    iget-object v0, p0, Lppz;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 51
    const/4 v0, 0x4

    iget-object v1, p0, Lppz;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 53
    :cond_3
    iget-object v0, p0, Lppz;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 54
    const/4 v0, 0x5

    iget-object v1, p0, Lppz;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 56
    :cond_4
    iget-object v0, p0, Lppz;->g:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 57
    const/4 v0, 0x6

    iget-object v1, p0, Lppz;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 59
    :cond_5
    iget-object v0, p0, Lppz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 61
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lppz;->a(Loxn;)Lppz;

    move-result-object v0

    return-object v0
.end method
