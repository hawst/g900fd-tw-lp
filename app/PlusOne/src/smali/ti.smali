.class final Lti;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# instance fields
.field private synthetic a:Ltd;


# direct methods
.method constructor <init>(Ltd;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lti;->a:Ltd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->e(Ltd;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 570
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-virtual {v0}, Ltd;->b()Z

    .line 571
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->a(Ltd;)Lth;

    move-result-object v0

    invoke-virtual {v0}, Lth;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 572
    iget-object v1, p0, Lti;->a:Ltd;

    invoke-static {v1}, Ltd;->a(Ltd;)Lth;

    move-result-object v1

    invoke-virtual {v1}, Lth;->e()Lsx;

    move-result-object v1

    invoke-virtual {v1, v0}, Lsx;->a(Landroid/content/pm/ResolveInfo;)I

    .line 573
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->a(Ltd;)Lth;

    move-result-object v0

    invoke-virtual {v0}, Lth;->e()Lsx;

    move-result-object v0

    invoke-virtual {v0}, Lsx;->b()Landroid/content/Intent;

    move-result-object v0

    .line 574
    if-eqz v0, :cond_0

    .line 575
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 576
    iget-object v1, p0, Lti;->a:Ltd;

    invoke-virtual {v1}, Ltd;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 580
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->f(Ltd;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 579
    iget-object v0, p0, Lti;->a:Ltd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltd;->a(Ltd;Z)Z

    .line 580
    iget-object v0, p0, Lti;->a:Ltd;

    iget-object v1, p0, Lti;->a:Ltd;

    invoke-static {v1}, Ltd;->g(Ltd;)I

    move-result v1

    invoke-static {v0, v1}, Ltd;->a(Ltd;I)V

    goto :goto_0

    .line 582
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public onDismiss()V
    .locals 2

    .prologue
    .line 602
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {}, Ltd;->d()Landroid/widget/PopupWindow$OnDismissListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {}, Ltd;->d()Landroid/widget/PopupWindow$OnDismissListener;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    .line 603
    :cond_0
    iget-object v0, p0, Lti;->a:Ltd;

    iget-object v0, v0, Ltd;->a:Lhj;

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Lti;->a:Ltd;

    iget-object v0, v0, Ltd;->a:Lhj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhj;->a(Z)V

    .line 606
    :cond_1
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 538
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lth;

    .line 539
    invoke-virtual {v0, p3}, Lth;->getItemViewType(I)I

    move-result v0

    .line 540
    packed-switch v0, :pswitch_data_0

    .line 563
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 542
    :pswitch_0
    iget-object v0, p0, Lti;->a:Ltd;

    const v1, 0x7fffffff

    invoke-static {v0, v1}, Ltd;->a(Ltd;I)V

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 545
    :pswitch_1
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-virtual {v0}, Ltd;->b()Z

    .line 546
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->d(Ltd;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 548
    if-lez p3, :cond_0

    .line 549
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->a(Ltd;)Lth;

    move-result-object v0

    invoke-virtual {v0}, Lth;->e()Lsx;

    move-result-object v0

    invoke-virtual {v0, p3}, Lsx;->b(I)V

    goto :goto_0

    .line 554
    :cond_1
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->a(Ltd;)Lth;

    move-result-object v0

    invoke-virtual {v0}, Lth;->f()Z

    .line 555
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->a(Ltd;)Lth;

    move-result-object v0

    invoke-virtual {v0}, Lth;->e()Lsx;

    move-result-object v0

    invoke-virtual {v0}, Lsx;->b()Landroid/content/Intent;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_0

    .line 557
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 558
    iget-object v1, p0, Lti;->a:Ltd;

    invoke-virtual {v1}, Ltd;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 540
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 589
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->e(Ltd;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 590
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0}, Ltd;->a(Ltd;)Lth;

    move-result-object v0

    invoke-virtual {v0}, Lth;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 591
    iget-object v0, p0, Lti;->a:Ltd;

    invoke-static {v0, v2}, Ltd;->a(Ltd;Z)Z

    .line 592
    iget-object v0, p0, Lti;->a:Ltd;

    iget-object v1, p0, Lti;->a:Ltd;

    invoke-static {v1}, Ltd;->g(Ltd;)I

    move-result v1

    invoke-static {v0, v1}, Ltd;->a(Ltd;I)V

    .line 597
    :cond_0
    return v2

    .line 595
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method
