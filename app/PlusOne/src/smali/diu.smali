.class public final Ldiu;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmgw;",
        "Lmgx;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ldiv;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILdiv;)V
    .locals 6

    .prologue
    .line 35
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v3, "photossharebylink"

    new-instance v4, Lmgw;

    invoke-direct {v4}, Lmgw;-><init>()V

    new-instance v5, Lmgx;

    invoke-direct {v5}, Lmgx;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 37
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 38
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiu;->a:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Ldiu;->b:Ldiv;

    .line 40
    return-void
.end method


# virtual methods
.method protected a(Lmgw;)V
    .locals 10

    .prologue
    .line 62
    new-instance v0, Lowj;

    invoke-direct {v0}, Lowj;-><init>()V

    iput-object v0, p1, Lmgw;->a:Lowj;

    .line 63
    const/4 v1, 0x2

    iput v1, v0, Lowj;->a:I

    .line 64
    const/4 v1, 0x1

    new-array v1, v1, [Lowm;

    .line 66
    const/4 v2, 0x0

    iget-object v3, p0, Ldiu;->b:Ldiv;

    new-instance v4, Lowm;

    invoke-direct {v4}, Lowm;-><init>()V

    new-instance v5, Lowo;

    invoke-direct {v5}, Lowo;-><init>()V

    iput-object v5, v4, Lowm;->b:Lowo;

    iget-object v5, v4, Lowm;->b:Lowo;

    new-instance v6, Lotf;

    invoke-direct {v6}, Lotf;-><init>()V

    invoke-virtual {v3}, Ldiv;->a()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lotf;->b:Ljava/lang/String;

    new-instance v7, Lotg;

    invoke-direct {v7}, Lotg;-><init>()V

    iput-object v7, v6, Lotf;->c:Lotg;

    iget-object v7, v6, Lotf;->c:Lotg;

    iget-object v8, p0, Ldiu;->a:Ljava/lang/String;

    iput-object v8, v7, Lotg;->a:Ljava/lang/String;

    iget-object v7, v6, Lotf;->c:Lotg;

    invoke-virtual {v3}, Ldiv;->b()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, Lotg;->b:Ljava/lang/String;

    iput-object v6, v5, Lowo;->a:Lotf;

    aput-object v4, v1, v2

    .line 67
    iput-object v1, v0, Lowj;->b:[Lowm;

    .line 68
    return-void
.end method

.method protected a(Lmgx;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p1, Lmgx;->a:Lowp;

    iget-object v0, v0, Lowp;->a:Ljava/lang/String;

    iput-object v0, p0, Ldiu;->c:Ljava/lang/String;

    .line 73
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmgw;

    invoke-virtual {p0, p1}, Ldiu;->a(Lmgw;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmgx;

    invoke-virtual {p0, p1}, Ldiu;->a(Lmgx;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ldiu;->c:Ljava/lang/String;

    return-object v0
.end method
