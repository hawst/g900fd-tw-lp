.class public final Locx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:I

.field private c:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 640
    invoke-direct {p0}, Loxq;-><init>()V

    .line 643
    const/high16 v0, -0x80000000

    iput v0, p0, Locx;->b:I

    .line 648
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Locx;->c:[Ljava/lang/Integer;

    .line 640
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 670
    .line 671
    iget v0, p0, Locx;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_3

    .line 672
    const/4 v0, 0x1

    iget v2, p0, Locx;->b:I

    .line 673
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 675
    :goto_0
    iget-object v2, p0, Locx;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 676
    const/4 v2, 0x2

    iget-object v3, p0, Locx;->a:Ljava/lang/Integer;

    .line 677
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 679
    :cond_0
    iget-object v2, p0, Locx;->c:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Locx;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 681
    iget-object v3, p0, Locx;->c:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 683
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 681
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 685
    :cond_1
    add-int/2addr v0, v2

    .line 686
    iget-object v1, p0, Locx;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 688
    :cond_2
    iget-object v1, p0, Locx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 689
    iput v0, p0, Locx;->ai:I

    .line 690
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Locx;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 698
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 699
    sparse-switch v0, :sswitch_data_0

    .line 703
    iget-object v1, p0, Locx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 704
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Locx;->ah:Ljava/util/List;

    .line 707
    :cond_1
    iget-object v1, p0, Locx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 709
    :sswitch_0
    return-object p0

    .line 714
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 715
    if-eq v0, v3, :cond_2

    if-eqz v0, :cond_2

    const/16 v1, 0x118

    if-eq v0, v1, :cond_2

    const/16 v1, 0x168

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1b8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x208

    if-ne v0, v1, :cond_3

    .line 721
    :cond_2
    iput v0, p0, Locx;->b:I

    goto :goto_0

    .line 723
    :cond_3
    iput v3, p0, Locx;->b:I

    goto :goto_0

    .line 728
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Locx;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 732
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 733
    iget-object v0, p0, Locx;->c:[Ljava/lang/Integer;

    array-length v0, v0

    .line 734
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 735
    iget-object v2, p0, Locx;->c:[Ljava/lang/Integer;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 736
    iput-object v1, p0, Locx;->c:[Ljava/lang/Integer;

    .line 737
    :goto_1
    iget-object v1, p0, Locx;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 738
    iget-object v1, p0, Locx;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 739
    invoke-virtual {p1}, Loxn;->a()I

    .line 737
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 742
    :cond_4
    iget-object v1, p0, Locx;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 699
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 653
    iget v0, p0, Locx;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 654
    const/4 v0, 0x1

    iget v1, p0, Locx;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 656
    :cond_0
    iget-object v0, p0, Locx;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 657
    const/4 v0, 0x2

    iget-object v1, p0, Locx;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 659
    :cond_1
    iget-object v0, p0, Locx;->c:[Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 660
    iget-object v1, p0, Locx;->c:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 661
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 660
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 664
    :cond_2
    iget-object v0, p0, Locx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 666
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 636
    invoke-virtual {p0, p1}, Locx;->a(Loxn;)Locx;

    move-result-object v0

    return-object v0
.end method
