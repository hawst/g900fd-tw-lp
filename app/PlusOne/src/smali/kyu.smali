.class public final Lkyu;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lbc;
.implements Lhmm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lloj;",
        "Landroid/content/DialogInterface$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhmm;"
    }
.end annotation


# static fields
.field private static final R:[Ljava/lang/String;

.field private static final S:Landroid/util/SparseIntArray;


# instance fields
.field private Q:Landroid/widget/TextView;

.field private T:Lhee;

.field private U:Ljava/lang/String;

.field private V:I

.field private W:Landroid/widget/ViewAnimator;

.field private X:Landroid/widget/RadioGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "volume"

    aput-object v1, v0, v2

    const-string v1, "square_name"

    aput-object v1, v0, v3

    sput-object v0, Lkyu;->R:[Ljava/lang/String;

    .line 64
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 66
    sput-object v0, Lkyu;->S:Landroid/util/SparseIntArray;

    const v1, 0x7f1005d3

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 67
    sget-object v0, Lkyu;->S:Landroid/util/SparseIntArray;

    const v1, 0x7f1005d2

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 68
    sget-object v0, Lkyu;->S:Landroid/util/SparseIntArray;

    const v1, 0x7f1005d1

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 69
    sget-object v0, Lkyu;->S:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    const v2, 0x7f1005d0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 70
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0}, Lloj;-><init>()V

    .line 76
    new-instance v0, Lhmf;

    iget-object v1, p0, Lkyu;->P:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lkyu;->V:I

    return-void
.end method

.method static synthetic a(Lkyu;)Llnl;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkyu;->N:Llnl;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lt;
    .locals 3

    .prologue
    .line 91
    new-instance v0, Lkyu;

    invoke-direct {v0}, Lkyu;-><init>()V

    .line 92
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 93
    const-string v2, "square_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0, v1}, Lt;->f(Landroid/os/Bundle;)V

    .line 95
    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    packed-switch p1, :pswitch_data_0

    .line 146
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 143
    :pswitch_0
    new-instance v0, Lkys;

    iget-object v1, p0, Lkyu;->N:Llnl;

    iget-object v2, p0, Lkyu;->T:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lkyu;->U:Ljava/lang/String;

    sget-object v4, Lkyu;->R:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lkys;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0, p1}, Lloj;->a(Landroid/os/Bundle;)V

    .line 107
    invoke-virtual {p0}, Lkyu;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkyu;->U:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x1

    .line 152
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 154
    packed-switch v0, :pswitch_data_0

    .line 187
    :goto_0
    return-void

    .line 156
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkyu;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 157
    :cond_0
    new-instance v0, Lkyv;

    invoke-direct {v0, p0}, Lkyv;-><init>(Lkyu;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 169
    :cond_1
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lkyu;->V:I

    .line 171
    invoke-interface {p2, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 172
    iget-object v0, p0, Lkyu;->Q:Landroid/widget/TextView;

    iget-object v1, p0, Lkyu;->N:Llnl;

    const v2, 0x7f0a021b

    invoke-virtual {v1, v2}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    :goto_1
    iget-object v0, p0, Lkyu;->X:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    if-ne v0, v6, :cond_2

    .line 178
    iget-object v0, p0, Lkyu;->X:Landroid/widget/RadioGroup;

    sget-object v1, Lkyu;->S:Landroid/util/SparseIntArray;

    iget v2, p0, Lkyu;->V:I

    const v3, 0x7f1005d1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 181
    :cond_2
    iget-object v0, p0, Lkyu;->W:Landroid/widget/ViewAnimator;

    invoke-virtual {v0, v5}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 182
    invoke-virtual {p0}, Lkyu;->c()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 174
    :cond_3
    iget-object v0, p0, Lkyu;->Q:Landroid/widget/TextView;

    iget-object v1, p0, Lkyu;->N:Llnl;

    const v2, 0x7f0a0216

    new-array v3, v5, [Ljava/lang/Object;

    .line 175
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    .line 174
    invoke-virtual {v1, v2, v3}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lkyu;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 208
    new-instance v0, Lkqw;

    sget-object v1, Lomv;->aj:Lhmn;

    iget-object v2, p0, Lkyu;->U:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 112
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lkyu;->N:Llnl;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 115
    iget-object v0, p0, Lkyu;->N:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040201

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 116
    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 117
    const v0, 0x7f1005cf

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewAnimator;

    iput-object v0, p0, Lkyu;->W:Landroid/widget/ViewAnimator;

    .line 118
    const v0, 0x7f1002ba

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lkyu;->X:Landroid/widget/RadioGroup;

    .line 120
    new-instance v0, Landroid/widget/TextView;

    iget-object v3, p0, Lkyu;->N:Llnl;

    invoke-direct {v0, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkyu;->Q:Landroid/widget/TextView;

    .line 121
    iget-object v0, p0, Lkyu;->W:Landroid/widget/ViewAnimator;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getPaddingLeft()I

    move-result v0

    .line 122
    iget-object v3, p0, Lkyu;->Q:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 123
    iget-object v0, p0, Lkyu;->N:Llnl;

    iget-object v3, p0, Lkyu;->Q:Landroid/widget/TextView;

    const/16 v4, 0x11

    invoke-static {v0, v3, v4}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 124
    iget-object v0, p0, Lkyu;->Q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 126
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 127
    const v0, 0x7f0a0579

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    const v0, 0x7f0a0597

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 129
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 134
    invoke-super {p0}, Lloj;->g()V

    .line 135
    invoke-virtual {p0}, Lkyu;->c()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 136
    invoke-virtual {p0}, Lkyu;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 137
    return-void
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 101
    iget-object v0, p0, Lkyu;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkyu;->T:Lhee;

    .line 102
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 194
    if-ne p2, v1, :cond_0

    .line 195
    iget-object v0, p0, Lkyu;->X:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 196
    if-eq v0, v1, :cond_0

    .line 197
    sget-object v1, Lkyu;->S:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->indexOfValue(I)I

    move-result v1

    .line 198
    iget v0, p0, Lkyu;->V:I

    if-eq v0, v1, :cond_0

    .line 199
    iget-object v0, p0, Lkyu;->O:Llnh;

    const-class v2, Lkyw;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkyw;

    iget-object v2, p0, Lkyu;->U:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lkyw;->a(Ljava/lang/String;I)V

    .line 203
    :cond_0
    invoke-virtual {p0}, Lkyu;->a()V

    .line 204
    return-void
.end method
