.class public final Ljeg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljeg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljdx;

.field public final b:Landroid/net/Uri;

.field private c:Ljei;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Ljeh;

    invoke-direct {v0}, Ljeh;-><init>()V

    sput-object v0, Ljeg;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const-class v0, Ljdx;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljdx;

    iput-object v0, p0, Ljeg;->a:Ljdx;

    .line 107
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Ljeg;->b:Landroid/net/Uri;

    .line 108
    invoke-static {}, Ljei;->values()[Ljei;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Ljeg;->c:Ljei;

    .line 109
    return-void
.end method

.method private constructor <init>(Ljdx;Landroid/net/Uri;Ljei;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Ljeg;->a:Ljdx;

    .line 98
    iput-object p2, p0, Ljeg;->b:Landroid/net/Uri;

    .line 99
    if-nez p3, :cond_0

    .line 100
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "type cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    iput-object p3, p0, Ljeg;->c:Ljei;

    .line 103
    return-void
.end method

.method public static a(Landroid/net/Uri;Ljei;)Ljeg;
    .locals 2

    .prologue
    .line 53
    if-nez p0, :cond_0

    .line 54
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "contentUri cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    new-instance v0, Ljeg;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1}, Ljeg;-><init>(Ljdx;Landroid/net/Uri;Ljei;)V

    return-object v0
.end method

.method public static a(Ljdx;Landroid/net/Uri;Ljei;)Ljeg;
    .locals 1

    .prologue
    .line 91
    const-string v0, "cloudMediaId cannot be null"

    invoke-static {p0, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    const-string v0, "contentUri cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    new-instance v0, Ljeg;

    invoke-direct {v0, p0, p1, p2}, Ljeg;-><init>(Ljdx;Landroid/net/Uri;Ljei;)V

    return-object v0
.end method

.method public static a(Lonw;Landroid/net/Uri;I)Ljeg;
    .locals 3

    .prologue
    .line 64
    const-string v0, "mediaId cannot be null"

    invoke-static {p0, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string v0, "contentUri cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p0}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v1

    .line 69
    sparse-switch p2, :sswitch_data_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "cloudMediaType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported yet"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :sswitch_0
    sget-object v0, Ljei;->a:Ljei;

    .line 83
    :goto_0
    new-instance v2, Ljeg;

    invoke-direct {v2, v1, p1, v0}, Ljeg;-><init>(Ljdx;Landroid/net/Uri;Ljei;)V

    return-object v2

    .line 75
    :sswitch_1
    sget-object v0, Ljei;->b:Ljei;

    goto :goto_0

    .line 69
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x3 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 113
    if-ne p1, p0, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    instance-of v2, p1, Ljeg;

    if-nez v2, :cond_2

    move v0, v1

    .line 117
    goto :goto_0

    .line 119
    :cond_2
    check-cast p1, Ljeg;

    .line 120
    iget-object v2, p0, Ljeg;->c:Ljei;

    iget-object v3, p1, Ljeg;->c:Ljei;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Ljeg;->a:Ljdx;

    iget-object v3, p1, Ljeg;->a:Ljdx;

    .line 121
    invoke-static {v2, v3}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljeg;->b:Landroid/net/Uri;

    iget-object v3, p1, Ljeg;->b:Landroid/net/Uri;

    .line 122
    invoke-static {v2, v3}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Ljeg;->a:Ljdx;

    iget-object v1, p0, Ljeg;->b:Landroid/net/Uri;

    iget-object v2, p0, Ljeg;->c:Ljei;

    const/16 v3, 0x11

    .line 129
    invoke-static {v2, v3}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v2

    .line 128
    invoke-static {v1, v2}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v1

    .line 127
    invoke-static {v0, v1}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 134
    const-string v0, "MediaIdentifier"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Ljeg;->a:Ljdx;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Ljeg;->b:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Ljeg;->c:Ljei;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Llsh;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    iget-object v0, p0, Ljeg;->a:Ljdx;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 145
    iget-object v0, p0, Ljeg;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 146
    iget-object v0, p0, Ljeg;->c:Ljei;

    invoke-virtual {v0}, Ljei;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 147
    return-void
.end method
