.class public final Lnvn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 815
    invoke-direct {p0}, Loxq;-><init>()V

    .line 820
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnvn;->b:[Ljava/lang/String;

    .line 815
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 839
    .line 840
    iget-object v0, p0, Lnvn;->a:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 841
    const/4 v0, 0x1

    iget-object v2, p0, Lnvn;->a:Ljava/lang/Long;

    .line 842
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 844
    :goto_0
    iget-object v2, p0, Lnvn;->b:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lnvn;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 846
    iget-object v3, p0, Lnvn;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 848
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 846
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 850
    :cond_0
    add-int/2addr v0, v2

    .line 851
    iget-object v1, p0, Lnvn;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 853
    :cond_1
    iget-object v1, p0, Lnvn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 854
    iput v0, p0, Lnvn;->ai:I

    .line 855
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnvn;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 863
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 864
    sparse-switch v0, :sswitch_data_0

    .line 868
    iget-object v1, p0, Lnvn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 869
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnvn;->ah:Ljava/util/List;

    .line 872
    :cond_1
    iget-object v1, p0, Lnvn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 874
    :sswitch_0
    return-object p0

    .line 879
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnvn;->a:Ljava/lang/Long;

    goto :goto_0

    .line 883
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 884
    iget-object v0, p0, Lnvn;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 885
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 886
    iget-object v2, p0, Lnvn;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 887
    iput-object v1, p0, Lnvn;->b:[Ljava/lang/String;

    .line 888
    :goto_1
    iget-object v1, p0, Lnvn;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 889
    iget-object v1, p0, Lnvn;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 890
    invoke-virtual {p1}, Loxn;->a()I

    .line 888
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 893
    :cond_2
    iget-object v1, p0, Lnvn;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 864
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 825
    iget-object v0, p0, Lnvn;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 826
    const/4 v0, 0x1

    iget-object v1, p0, Lnvn;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 828
    :cond_0
    iget-object v0, p0, Lnvn;->b:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 829
    iget-object v1, p0, Lnvn;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 830
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 829
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 833
    :cond_1
    iget-object v0, p0, Lnvn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 835
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 811
    invoke-virtual {p0, p1}, Lnvn;->a(Loxn;)Lnvn;

    move-result-object v0

    return-object v0
.end method
