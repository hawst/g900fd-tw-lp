.class public Lcvu;
.super Llol;
.source "PG"


# instance fields
.field private N:Ldgr;

.field private O:Ldeo;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 28
    invoke-direct {p0}, Llol;-><init>()V

    .line 34
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcvu;->av:Llqm;

    sget-object v2, Ldgx;->N:Ldgo;

    new-instance v3, Lcvv;

    invoke-direct {v3, p0}, Lcvv;-><init>(Lcvu;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 40
    new-instance v0, Ldep;

    iget-object v1, p0, Lcvu;->av:Llqm;

    new-instance v2, Lcvw;

    invoke-direct {v2, p0}, Lcvw;-><init>(Lcvu;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 46
    return-void
.end method

.method static synthetic a(Lcvu;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 28
    iget-object v0, p0, Lcvu;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcvu;->O:Ldeo;

    invoke-virtual {v1, v4}, Ldeo;->c(Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Lddl;->d()Ljuf;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcvu;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Lcvu;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v1}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "resource_type"

    iget-object v2, p0, Lcvu;->O:Ldeo;

    invoke-virtual {v2}, Ldeo;->f()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcvu;->O:Ldeo;

    invoke-virtual {v1}, Ldeo;->a()Lddl;

    move-result-object v1

    invoke-interface {v1}, Lddl;->g()Lnym;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v1, Lnym;->v:Lnyh;

    if-eqz v2, :cond_0

    const-string v2, "geo_lat"

    iget-object v3, v1, Lnym;->v:Lnyh;

    iget-object v3, v3, Lnyh;->a:Ljava/lang/Double;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "geo_lon"

    iget-object v1, v1, Lnym;->v:Lnyh;

    iget-object v1, v1, Lnyh;->b:Ljava/lang/Double;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcvu;->a(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcvu;->n()Lz;

    move-result-object v0

    const v1, 0x7f05001e

    invoke-virtual {v0, v1, v4}, Lz;->overridePendingTransition(II)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcvu;)V
    .locals 3

    .prologue
    .line 28
    iget-object v1, p0, Lcvu;->N:Ldgr;

    sget-object v2, Ldgx;->N:Ldgo;

    iget-object v0, p0, Lcvu;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvu;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Ldgr;->a(Ldgu;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 77
    iget-object v0, p0, Lcvu;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcvu;->N:Ldgr;

    .line 78
    iget-object v0, p0, Lcvu;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcvu;->O:Ldeo;

    .line 79
    return-void
.end method
