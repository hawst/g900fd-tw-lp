.class abstract Ldvb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private synthetic c:Ldur;


# direct methods
.method public constructor <init>(Ldur;I)V
    .locals 1

    .prologue
    .line 1732
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Ldvb;-><init>(Ldur;II)V

    .line 1733
    return-void
.end method

.method public constructor <init>(Ldur;II)V
    .locals 0

    .prologue
    .line 1735
    iput-object p1, p0, Ldvb;->c:Ldur;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1736
    iput p2, p0, Ldvb;->a:I

    .line 1737
    iput p3, p0, Ldvb;->b:I

    .line 1738
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 1745
    iget-object v0, p0, Ldvb;->c:Ldur;

    invoke-static {v0}, Ldur;->a(Ldur;)Ldve;

    move-result-object v0

    invoke-virtual {p0}, Ldvb;->g()I

    move-result v1

    iget v2, p0, Ldvb;->a:I

    invoke-virtual {v0, v1, v2}, Ldve;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1751
    :goto_0
    return-void

    .line 1749
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0}, Ldvb;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ldvb;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ldvb;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ldvb;->d()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ldvb;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Ldvb;->f()Ljava/lang/String;

    move-result-object v8

    iget v0, p0, Ldvb;->b:I

    const/4 v9, -0x1

    if-ne v0, v9, :cond_1

    move-object v9, v7

    :goto_1
    move-object v0, p1

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1750
    iget v1, p0, Ldvb;->a:I

    invoke-virtual {p0}, Ldvb;->i()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v1, v2}, Ldvb;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;ILandroid/net/Uri;)V

    goto :goto_0

    .line 1749
    :cond_1
    iget v0, p0, Ldvb;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_1
.end method

.method protected a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;ILandroid/net/Uri;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x4

    .line 1779
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1780
    iget-object v0, p0, Ldvb;->c:Ldur;

    invoke-static {v0}, Ldur;->a(Ldur;)Ldve;

    move-result-object v0

    invoke-virtual {v0, p3}, Ldve;->a(I)I

    move-result v8

    .line 1782
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1783
    invoke-virtual {p0}, Ldvb;->g()I

    move-result v5

    .line 1785
    :goto_0
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldvb;->c:Ldur;

    invoke-static {v0}, Ldur;->a(Ldur;)Ldve;

    move-result-object v0

    invoke-virtual {v0, v5, p3}, Ldve;->a(II)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldvb;->c:Ldur;

    .line 1786
    invoke-static {v0}, Ldur;->a(Ldur;)Ldve;

    move-result-object v0

    invoke-virtual {v0}, Ldve;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1787
    iget-object v0, p0, Ldvb;->c:Ldur;

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    invoke-static/range {v0 .. v5}, Ldur;->a(Ldur;Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;Landroid/content/ContentValues;II)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1790
    :catchall_0
    move-exception v0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 1791
    iget-object v1, p0, Ldvb;->c:Ldur;

    invoke-static {v1}, Ldur;->a(Ldur;)Ldve;

    move-result-object v1

    invoke-virtual {v1, p3}, Ldve;->a(I)I

    move-result v1

    sub-int/2addr v1, v8

    .line 1792
    if-eqz p4, :cond_0

    if-eqz v1, :cond_0

    .line 1793
    iget-object v2, p0, Ldvb;->c:Ldur;

    invoke-static {v2}, Ldur;->c(Ldur;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p4, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1795
    :cond_0
    const-string v2, "MediaSyncManager"

    invoke-static {v2, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1796
    invoke-virtual {p0}, Ldvb;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {p3}, Ldur;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 1798
    invoke-static {v6, v7}, Llse;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x27

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] downloaded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1796
    :cond_1
    throw v0

    .line 1790
    :cond_2
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 1791
    iget-object v0, p0, Ldvb;->c:Ldur;

    invoke-static {v0}, Ldur;->a(Ldur;)Ldve;

    move-result-object v0

    invoke-virtual {v0, p3}, Ldve;->a(I)I

    move-result v0

    sub-int/2addr v0, v8

    .line 1792
    if-eqz p4, :cond_3

    if-eqz v0, :cond_3

    .line 1793
    iget-object v1, p0, Ldvb;->c:Ldur;

    invoke-static {v1}, Ldur;->c(Ldur;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p4, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1795
    :cond_3
    const-string v1, "MediaSyncManager"

    invoke-static {v1, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1796
    invoke-virtual {p0}, Ldvb;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ldur;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 1798
    invoke-static {v6, v7}, Llse;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x27

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] downloaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1796
    :cond_4
    return-void
.end method

.method protected abstract b()[Ljava/lang/String;
.end method

.method protected abstract c()Ljava/lang/String;
.end method

.method protected abstract d()[Ljava/lang/String;
.end method

.method protected abstract e()Ljava/lang/String;
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method protected abstract g()I
.end method

.method protected abstract h()Ljava/lang/String;
.end method

.method protected i()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1762
    const/4 v0, 0x0

    return-object v0
.end method
