.class final Leii;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic a:Lehu;


# direct methods
.method constructor <init>(Lehu;)V
    .locals 0

    .prologue
    .line 3251
    iput-object p1, p0, Leii;->a:Lehu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3252
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3264
    invoke-static {p1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    .line 3265
    :goto_0
    iget-object v3, p0, Leii;->a:Lehu;

    invoke-static {v3}, Lehu;->ai(Lehu;)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Leii;->a:Lehu;

    invoke-static {v4}, Lehu;->o(Lehu;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 3266
    iget-object v1, p0, Leii;->a:Lehu;

    invoke-static {v1}, Lehu;->aq(Lehu;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 3267
    iget-object v0, p0, Leii;->a:Lehu;

    invoke-static {v0}, Lehu;->ar(Lehu;)V

    .line 3269
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 3264
    goto :goto_0

    :cond_2
    move v1, v2

    .line 3265
    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 3260
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 3256
    return-void
.end method
