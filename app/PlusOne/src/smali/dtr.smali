.class public final enum Ldtr;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldtr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldtr;

.field public static final enum b:Ldtr;

.field public static final enum c:Ldtr;

.field public static final enum d:Ldtr;

.field private static final synthetic e:[Ldtr;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 122
    new-instance v0, Ldtr;

    const-string v1, "INITIAL_HIT_LIMIT"

    invoke-direct {v0, v1, v2}, Ldtr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtr;->a:Ldtr;

    .line 123
    new-instance v0, Ldtr;

    const-string v1, "INITIAL_COMPLETE"

    invoke-direct {v0, v1, v3}, Ldtr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtr;->b:Ldtr;

    .line 124
    new-instance v0, Ldtr;

    const-string v1, "DELTA_COMPLETE"

    invoke-direct {v0, v1, v4}, Ldtr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtr;->c:Ldtr;

    .line 125
    new-instance v0, Ldtr;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v5}, Ldtr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtr;->d:Ldtr;

    .line 121
    const/4 v0, 0x4

    new-array v0, v0, [Ldtr;

    sget-object v1, Ldtr;->a:Ldtr;

    aput-object v1, v0, v2

    sget-object v1, Ldtr;->b:Ldtr;

    aput-object v1, v0, v3

    sget-object v1, Ldtr;->c:Ldtr;

    aput-object v1, v0, v4

    sget-object v1, Ldtr;->d:Ldtr;

    aput-object v1, v0, v5

    sput-object v0, Ldtr;->e:[Ldtr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 121
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldtr;
    .locals 1

    .prologue
    .line 121
    const-class v0, Ldtr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldtr;

    return-object v0
.end method

.method public static values()[Ldtr;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Ldtr;->e:[Ldtr;

    invoke-virtual {v0}, [Ldtr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldtr;

    return-object v0
.end method
