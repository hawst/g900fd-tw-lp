.class public final Lmst;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lmsu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2667
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2670
    sget-object v0, Lmsu;->a:[Lmsu;

    iput-object v0, p0, Lmst;->a:[Lmsu;

    .line 2667
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2688
    .line 2689
    iget-object v1, p0, Lmst;->a:[Lmsu;

    if-eqz v1, :cond_1

    .line 2690
    iget-object v2, p0, Lmst;->a:[Lmsu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2691
    if-eqz v4, :cond_0

    .line 2692
    const/4 v5, 0x1

    .line 2693
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2690
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2697
    :cond_1
    iget-object v1, p0, Lmst;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2698
    iput v0, p0, Lmst;->ai:I

    .line 2699
    return v0
.end method

.method public a(Loxn;)Lmst;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2707
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2708
    sparse-switch v0, :sswitch_data_0

    .line 2712
    iget-object v2, p0, Lmst;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2713
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmst;->ah:Ljava/util/List;

    .line 2716
    :cond_1
    iget-object v2, p0, Lmst;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2718
    :sswitch_0
    return-object p0

    .line 2723
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2724
    iget-object v0, p0, Lmst;->a:[Lmsu;

    if-nez v0, :cond_3

    move v0, v1

    .line 2725
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmsu;

    .line 2726
    iget-object v3, p0, Lmst;->a:[Lmsu;

    if-eqz v3, :cond_2

    .line 2727
    iget-object v3, p0, Lmst;->a:[Lmsu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2729
    :cond_2
    iput-object v2, p0, Lmst;->a:[Lmsu;

    .line 2730
    :goto_2
    iget-object v2, p0, Lmst;->a:[Lmsu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2731
    iget-object v2, p0, Lmst;->a:[Lmsu;

    new-instance v3, Lmsu;

    invoke-direct {v3}, Lmsu;-><init>()V

    aput-object v3, v2, v0

    .line 2732
    iget-object v2, p0, Lmst;->a:[Lmsu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2733
    invoke-virtual {p1}, Loxn;->a()I

    .line 2730
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2724
    :cond_3
    iget-object v0, p0, Lmst;->a:[Lmsu;

    array-length v0, v0

    goto :goto_1

    .line 2736
    :cond_4
    iget-object v2, p0, Lmst;->a:[Lmsu;

    new-instance v3, Lmsu;

    invoke-direct {v3}, Lmsu;-><init>()V

    aput-object v3, v2, v0

    .line 2737
    iget-object v2, p0, Lmst;->a:[Lmsu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2708
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2675
    iget-object v0, p0, Lmst;->a:[Lmsu;

    if-eqz v0, :cond_1

    .line 2676
    iget-object v1, p0, Lmst;->a:[Lmsu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2677
    if-eqz v3, :cond_0

    .line 2678
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2676
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2682
    :cond_1
    iget-object v0, p0, Lmst;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2684
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0, p1}, Lmst;->a(Loxn;)Lmst;

    move-result-object v0

    return-object v0
.end method
