.class public final Ljqv;
.super Ljow;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljow;-><init>()V

    return-void
.end method

.method public static a(Lu;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 45
    invoke-virtual {p0}, Lu;->p()Lae;

    move-result-object v1

    .line 46
    invoke-virtual {v1, p2}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 47
    if-nez v0, :cond_1

    .line 48
    invoke-virtual {p0}, Lu;->n()Lz;

    move-result-object v2

    .line 49
    if-eqz v2, :cond_1

    .line 50
    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    .line 51
    if-nez p3, :cond_0

    .line 52
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 54
    :cond_0
    const-string v3, "account_id"

    invoke-virtual {p3, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    invoke-virtual {v0, p3}, Ljqv;->f(Landroid/os/Bundle;)V

    .line 56
    const/4 v3, 0x0

    invoke-virtual {v0, p0, v3}, Ljqv;->a(Lu;I)V

    .line 57
    invoke-virtual {v0, v1, p2}, Ljqv;->a(Lae;Ljava/lang/String;)V

    .line 59
    const-class v0, Ljpb;

    invoke-static {v2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpb;

    .line 60
    invoke-interface {v0, v2, p1}, Ljpb;->a(Landroid/content/Context;I)V

    .line 61
    new-instance v0, Ljrk;

    invoke-direct {v0, v2, p1}, Ljrk;-><init>(Landroid/content/Context;I)V

    .line 62
    invoke-virtual {v0}, Ljrk;->d()V

    .line 63
    const-class v1, Lhoc;

    invoke-static {v2, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    invoke-static {v2, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 66
    :cond_1
    return-void
.end method


# virtual methods
.method protected U()V
    .locals 4

    .prologue
    .line 109
    .line 110
    invoke-virtual {p0}, Ljqv;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0523

    invoke-virtual {v0, v1}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-virtual {p0}, Ljqv;->n()Lz;

    move-result-object v1

    const-string v2, "circles_add"

    const-string v3, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v1, v2, v3}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-virtual {p0, v0, v1}, Ljqv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method protected a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 104
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 70
    invoke-virtual {p0}, Ljqv;->n()Lz;

    move-result-object v2

    .line 71
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 73
    invoke-virtual {p0}, Ljqv;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 76
    const v0, 0x7f0a0522

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 77
    const v0, 0x7f0a0583

    invoke-virtual {v3, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    invoke-virtual {v3, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 79
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04007e

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 81
    const v0, 0x7f100139

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 84
    const v1, 0x7f0a051d

    invoke-virtual {v2, v1}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 85
    const v1, 0x7f0a051e

    invoke-virtual {v2, v1}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 86
    const v1, 0x7f0a051f

    invoke-virtual {v2, v1}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 87
    new-instance v9, Landroid/text/SpannableString;

    invoke-direct {v9, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 88
    const-class v1, Lkbw;

    invoke-static {v2, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkbw;

    .line 89
    invoke-interface {v1, v2, v4}, Lkbw;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v1

    .line 90
    new-instance v2, Lljt;

    invoke-virtual {p0}, Ljqv;->n()Lz;

    move-result-object v4

    invoke-direct {v2, v4, v1}, Lljt;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    .line 91
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v4, 0x21

    .line 90
    invoke-interface {v9, v2, v11, v1, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 93
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/CharSequence;

    aput-object v6, v1, v11

    const-string v2, " "

    aput-object v2, v1, v10

    const/4 v2, 0x2

    aput-object v9, v1, v2

    const/4 v2, 0x3

    const-string v4, "\n\n"

    aput-object v4, v1, v2

    const/4 v2, 0x4

    aput-object v7, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 95
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setClickable(Z)V

    .line 97
    invoke-virtual {p0}, Ljqv;->U()V

    .line 98
    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 99
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
