.class public final Lkyx;
.super Lgbz;
.source "PG"


# instance fields
.field private A:Lkyy;

.field private B:Landroid/graphics/Rect;

.field private C:Landroid/graphics/Point;

.field private D:Landroid/text/StaticLayout;

.field public y:Lkzz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lkyx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lkyx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance v0, Lkyy;

    invoke-direct {v0, p0, p1, p2, p3}, Lkyy;-><init>(Lkyx;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lkyx;->A:Lkyy;

    .line 53
    iget-object v0, p0, Lkyx;->A:Lkyy;

    invoke-virtual {v0, p0}, Lkyy;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lkyx;->B:Landroid/graphics/Rect;

    .line 56
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lkyx;->C:Landroid/graphics/Point;

    .line 57
    return-void
.end method

.method static synthetic a(Lkyx;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lkyx;->B:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic a(Lkyx;Landroid/text/StaticLayout;)Landroid/text/StaticLayout;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lkyx;->D:Landroid/text/StaticLayout;

    return-object p1
.end method

.method static synthetic b(Lkyx;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lkyx;->C:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic c(Lkyx;)Landroid/text/StaticLayout;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lkyx;->D:Landroid/text/StaticLayout;

    return-object v0
.end method


# virtual methods
.method protected a(III)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 180
    const/high16 v0, -0x80000000

    invoke-static {p3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 181
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 182
    iget-object v2, p0, Lkyx;->A:Lkyy;

    invoke-virtual {v2, v0, v1}, Lkyy;->measure(II)V

    .line 183
    iget-object v0, p0, Lkyx;->A:Lkyy;

    invoke-virtual {v0}, Lkyy;->getMeasuredHeight()I

    move-result v0

    .line 184
    iget-object v1, p0, Lkyx;->A:Lkyy;

    add-int v2, p1, p3

    add-int v3, p2, v0

    invoke-virtual {v1, p1, p2, v2, v3}, Lkyy;->layout(IIII)V

    .line 185
    add-int/2addr v0, p2

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lkyx;->A:Lkyy;

    invoke-virtual {v0}, Lkyy;->getHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 144
    invoke-super {p0}, Lgbz;->a()V

    .line 145
    iget-object v0, p0, Lkyx;->A:Lkyy;

    invoke-virtual {v0}, Lkyy;->a()V

    .line 147
    iput-object v2, p0, Lkyx;->y:Lkzz;

    .line 148
    iput-object v2, p0, Lkyx;->D:Landroid/text/StaticLayout;

    .line 149
    iget-object v0, p0, Lkyx;->B:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 150
    iget-object v0, p0, Lkyx;->C:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 151
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 155
    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 156
    invoke-static {v0}, Lkzz;->a([B)Lkzz;

    move-result-object v0

    iput-object v0, p0, Lkyx;->y:Lkzz;

    .line 157
    return-void
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    return v0
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 4

    .prologue
    .line 162
    invoke-super {p0, p1, p2, p3}, Lgbz;->a_(Landroid/database/Cursor;Llcr;I)V

    .line 163
    iget-object v0, p0, Lkyx;->A:Lkyy;

    invoke-virtual {p0, v0}, Lkyx;->removeView(Landroid/view/View;)V

    .line 164
    iget-object v0, p0, Lkyx;->A:Lkyy;

    invoke-virtual {p0, v0}, Lkyx;->addView(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lkyx;->y:Lkzz;

    invoke-virtual {v0}, Lkzz;->c()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 167
    iget-object v1, p0, Lkyx;->A:Lkyy;

    invoke-virtual {p0}, Lkyx;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v0, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkyy;->a(Lizu;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lkyx;->A:Lkyy;

    iget v1, p0, Lkyx;->o:I

    invoke-virtual {p0, p2, v1}, Lkyx;->a(Lhuk;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lkyy;->a(I)V

    .line 171
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lgbz;->b()V

    .line 127
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lkyx;->A:Lkyy;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lkyx;->A:Lkyy;

    invoke-virtual {v0}, Lkyy;->b()V

    .line 132
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lkyx;->A:Lkyy;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lkyx;->A:Lkyy;

    invoke-virtual {v0}, Lkyy;->c()V

    .line 139
    :cond_0
    invoke-super {p0}, Lgbz;->c()V

    .line 140
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lkyx;->A:Lkyy;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lkyx;->i:Lfdp;

    if-nez v0, :cond_1

    .line 196
    :cond_0
    invoke-super {p0, p1}, Lgbz;->onClick(Landroid/view/View;)V

    .line 201
    :goto_0
    return-void

    .line 200
    :cond_1
    iget-object v0, p0, Lkyx;->i:Lfdp;

    iget-object v1, p0, Lkyx;->y:Lkzz;

    invoke-virtual {v1}, Lkzz;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfdp;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
