.class final Lksn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Lksj;


# direct methods
.method constructor <init>(Lksj;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lksn;->a:Lksj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lksn;->a:Lksj;

    check-cast p2, Lkrs;

    invoke-static {v0, p2}, Lksj;->a(Lksj;Lkrs;)Lkrs;

    .line 268
    iget-object v0, p0, Lksn;->a:Lksj;

    invoke-static {v0}, Lksj;->b(Lksj;)Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    move-result-object v0

    iget-object v1, p0, Lksn;->a:Lksj;

    invoke-virtual {v1}, Lksj;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lksn;->a:Lksj;

    invoke-static {v2}, Lksj;->a(Lksj;)Lkrs;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->a(Landroid/content/Context;Lkrs;)V

    .line 270
    iget-object v0, p0, Lksn;->a:Lksj;

    invoke-static {v0}, Lksj;->a(Lksj;)Lkrs;

    move-result-object v0

    iget-object v1, p0, Lksn;->a:Lksj;

    invoke-static {v1}, Lksj;->c(Lksj;)Lksm;

    move-result-object v1

    invoke-interface {v0, v1}, Lkrs;->a(Lkrl;)V

    .line 271
    iget-object v0, p0, Lksn;->a:Lksj;

    invoke-static {v0}, Lksj;->d(Lksj;)V

    .line 272
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 276
    const-string v0, "StreamCastUiFragment"

    const-string v1, "Disconnected from service."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iget-object v0, p0, Lksn;->a:Lksj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lksj;->a(Lksj;Lkrs;)Lkrs;

    .line 278
    iget-object v0, p0, Lksn;->a:Lksj;

    invoke-virtual {v0}, Lksj;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lksn;->a:Lksj;

    invoke-virtual {v0}, Lksj;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 281
    :cond_0
    return-void
.end method
