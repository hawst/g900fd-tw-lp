.class public final Lmbm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvh;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9151
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9154
    iput-object v0, p0, Lmbm;->apiHeader:Llyq;

    .line 9157
    iput-object v0, p0, Lmbm;->a:Lnvh;

    .line 9151
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9174
    const/4 v0, 0x0

    .line 9175
    iget-object v1, p0, Lmbm;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 9176
    const/4 v0, 0x1

    iget-object v1, p0, Lmbm;->apiHeader:Llyq;

    .line 9177
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9179
    :cond_0
    iget-object v1, p0, Lmbm;->a:Lnvh;

    if-eqz v1, :cond_1

    .line 9180
    const/4 v1, 0x2

    iget-object v2, p0, Lmbm;->a:Lnvh;

    .line 9181
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9183
    :cond_1
    iget-object v1, p0, Lmbm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9184
    iput v0, p0, Lmbm;->ai:I

    .line 9185
    return v0
.end method

.method public a(Loxn;)Lmbm;
    .locals 2

    .prologue
    .line 9193
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9194
    sparse-switch v0, :sswitch_data_0

    .line 9198
    iget-object v1, p0, Lmbm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbm;->ah:Ljava/util/List;

    .line 9202
    :cond_1
    iget-object v1, p0, Lmbm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9204
    :sswitch_0
    return-object p0

    .line 9209
    :sswitch_1
    iget-object v0, p0, Lmbm;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 9210
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmbm;->apiHeader:Llyq;

    .line 9212
    :cond_2
    iget-object v0, p0, Lmbm;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9216
    :sswitch_2
    iget-object v0, p0, Lmbm;->a:Lnvh;

    if-nez v0, :cond_3

    .line 9217
    new-instance v0, Lnvh;

    invoke-direct {v0}, Lnvh;-><init>()V

    iput-object v0, p0, Lmbm;->a:Lnvh;

    .line 9219
    :cond_3
    iget-object v0, p0, Lmbm;->a:Lnvh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9194
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9162
    iget-object v0, p0, Lmbm;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 9163
    const/4 v0, 0x1

    iget-object v1, p0, Lmbm;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9165
    :cond_0
    iget-object v0, p0, Lmbm;->a:Lnvh;

    if-eqz v0, :cond_1

    .line 9166
    const/4 v0, 0x2

    iget-object v1, p0, Lmbm;->a:Lnvh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9168
    :cond_1
    iget-object v0, p0, Lmbm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9170
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9147
    invoke-virtual {p0, p1}, Lmbm;->a(Loxn;)Lmbm;

    move-result-object v0

    return-object v0
.end method
