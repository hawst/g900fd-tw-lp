.class public Lcdu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lalh;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcgh;

.field private static final c:Lcgh;


# instance fields
.field private A:I

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Lcdz;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Lamy;

.field private final f:Lcdt;

.field private g:J

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v4, 0xa

    const/4 v7, 0x6

    .line 28
    const-class v0, Lcdu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcdu;->a:Ljava/lang/String;

    .line 306
    new-instance v1, Lcgh;

    const-wide/16 v2, 0x64

    const/16 v6, 0x9

    move v5, v4

    invoke-direct/range {v1 .. v6}, Lcgh;-><init>(JIII)V

    sput-object v1, Lcdu;->b:Lcgh;

    .line 313
    new-instance v1, Lcgh;

    const-wide/16 v2, 0xa

    const/4 v4, 0x3

    move v5, v7

    move v6, v7

    invoke-direct/range {v1 .. v6}, Lcgh;-><init>(JIII)V

    sput-object v1, Lcdu;->c:Lcgh;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Lamy;Lcdt;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366
    const-string v0, "sharedPreferences"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    .line 367
    const-string v0, "deviceInfo"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamy;

    iput-object v0, p0, Lcdu;->e:Lamy;

    .line 368
    const-string v0, "analytics"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdt;

    iput-object v0, p0, Lcdu;->f:Lcdt;

    .line 369
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcdu;->B:Z

    .line 370
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "InitApp"

    invoke-interface {v0, v1, v2}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    return-void
.end method

.method private static a(Lboh;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1004
    if-nez p0, :cond_0

    .line 1005
    const-string v0, "None"

    .line 1007
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lboh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lboh;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Latu;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 914
    const-string v0, "Unknown"

    .line 915
    if-eqz p1, :cond_0

    .line 916
    iget-object v0, p1, Latu;->d:Ljava/lang/String;

    .line 918
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ZZZ)Ljava/lang/String;
    .locals 1

    .prologue
    .line 934
    if-nez p1, :cond_0

    .line 935
    const-string v0, "NotOpened"

    .line 943
    :goto_0
    return-object v0

    .line 936
    :cond_0
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 937
    const-string v0, "UsedSliderAndPreset"

    goto :goto_0

    .line 938
    :cond_1
    if-eqz p2, :cond_2

    if-nez p3, :cond_2

    .line 939
    const-string v0, "UsedSlider"

    goto :goto_0

    .line 940
    :cond_2
    if-nez p2, :cond_3

    if-eqz p3, :cond_3

    .line 941
    const-string v0, "UsedPreset"

    goto :goto_0

    .line 943
    :cond_3
    const-string v0, "Inspected"

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 958
    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 959
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 966
    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 967
    sget-object v0, Lcdu;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xe

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Value "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not set"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 971
    :goto_0
    return-void

    .line 970
    :cond_0
    invoke-direct {p0, p1}, Lcdu;->c(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcdu;->b(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Lcgh;)V
    .locals 7

    .prologue
    .line 888
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-ltz v0, :cond_0

    cmp-long v0, p3, p5

    if-lez v0, :cond_2

    .line 889
    :cond_0
    if-eqz p7, :cond_1

    .line 890
    iget-object v0, p0, Lcdu;->f:Lcdt;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object v1, p1

    move-object v2, p7

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 896
    :cond_1
    :goto_0
    return-void

    .line 894
    :cond_2
    invoke-virtual {p8, p3, p4}, Lcgh;->a(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 895
    iget-object v0, p0, Lcdu;->f:Lcdt;

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 1000
    iget-object v0, p0, Lcdu;->f:Lcdt;

    if-eqz p3, :cond_0

    const-string v3, "Yes"

    :goto_0
    if-eqz p3, :cond_1

    const-wide/16 v4, 0x1

    :goto_1
    move-object v1, p1

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 1001
    return-void

    .line 1000
    :cond_0
    const-string v3, "No"

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 963
    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    .line 1199
    invoke-virtual {p0}, Lcdu;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1200
    sget-object v0, Lcdu;->a:Ljava/lang/String;

    .line 1201
    iget-object v1, p0, Lcdu;->f:Lcdt;

    const-string v2, "MovieMakerSession"

    if-eqz p1, :cond_0

    const-string v0, "SuccessWithoutFootage"

    :goto_0
    invoke-interface {v1, v2, v0}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    :goto_1
    return-void

    .line 1201
    :cond_0
    const-string v0, "StartingWithoutFootage"

    goto :goto_0

    .line 1204
    :cond_1
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    if-eqz p1, :cond_2

    const-string v2, "SuccessWithFootage"

    :goto_2
    iget v3, p0, Lcdu;->x:I

    .line 1206
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcdu;->y:I

    int-to-long v4, v4

    .line 1204
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_1

    :cond_2
    const-string v2, "StartingWithFootage"

    goto :goto_2
.end method

.method private static a(ZCLjava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 1170
    if-eqz p0, :cond_0

    .line 1171
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1175
    :goto_0
    return-void

    .line 1173
    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static b(Lbza;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1012
    invoke-virtual {p0}, Lbza;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1014
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    move v2, v3

    .line 1016
    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v0, v6, :cond_2

    .line 1017
    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 1018
    const/16 v7, 0x5f

    if-ne v6, v7, :cond_0

    move v2, v3

    .line 1016
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1020
    :cond_0
    if-eqz v2, :cond_1

    .line 1021
    invoke-static {v6}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v2, v1

    .line 1022
    goto :goto_1

    .line 1024
    :cond_1
    invoke-static {v6}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1027
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 984
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    int-to-long v4, p2

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 985
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 988
    const-string v0, "MovieMakerSession"

    invoke-direct {p0, p1}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {p0, v0, p2, v1}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 989
    return-void
.end method

.method private c(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 974
    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const/4 v1, -0x1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static d(Lcdx;)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x3e8

    const/4 v1, 0x0

    .line 899
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 900
    iget-object v0, p0, Lcdx;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 901
    const-string v0, "\\n\\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 902
    iget-object v3, p0, Lcdx;->b:[Ljava/lang/StackTraceElement;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 903
    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 904
    const-string v5, "\\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 902
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 906
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 907
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v6, :cond_1

    .line 908
    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 910
    :cond_1
    return-object v0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 992
    iget-object v1, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 993
    sget-object v1, Lcdu;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Flag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 996
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 926
    iget v0, p0, Lcdu;->l:I

    if-nez v0, :cond_0

    iget v0, p0, Lcdu;->k:I

    if-nez v0, :cond_0

    .line 928
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcdu;->a(Z)V

    .line 930
    :cond_0
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    .line 1049
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "InitGUI"

    invoke-interface {v0, v1, v2}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 1181
    iput v1, p0, Lcdu;->x:I

    .line 1182
    iput v1, p0, Lcdu;->y:I

    .line 1183
    iput v1, p0, Lcdu;->z:I

    .line 1185
    iput-boolean v0, p0, Lcdu;->D:Z

    .line 1186
    iput-boolean v0, p0, Lcdu;->E:Z

    .line 1187
    iput-boolean v0, p0, Lcdu;->F:Z

    .line 1188
    iput-boolean v0, p0, Lcdu;->G:Z

    .line 1189
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 381
    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "sceneDelete"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcdu;->h:I

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "sceneReorder"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcdu;->i:I

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "sceneTrim"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcdu;->j:I

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "save"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcdu;->l:I

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "share"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcdu;->k:I

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "notification"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcdu;->m:I

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "playerInitialized"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->n:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "usedPreviewPlayer"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->o:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "openedStoryboardEditor"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->p:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "openedMusicDrawer"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->q:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "soundtrackChanged"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->r:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "themeChanged"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->s:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "titleEdited"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->t:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "openedDurationPicker"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->u:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "usedDurationSlider"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->v:Z

    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "usedDurationPreset"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->w:Z

    .line 382
    invoke-direct {p0}, Lcdu;->p()V

    .line 383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->B:Z

    .line 384
    return-void
.end method

.method public a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 455
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 456
    iget-boolean v0, p0, Lcdu;->D:Z

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    const-string v1, "only call logInputReceived once"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 457
    if-nez p1, :cond_1

    .line 458
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "NoInput"

    invoke-interface {v0, v1, v2}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    :goto_1
    iput-boolean v6, p0, Lcdu;->D:Z

    .line 464
    return-void

    .line 456
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 460
    :cond_1
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "InputReceived"

    .line 461
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    int-to-long v4, p1

    .line 460
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_1
.end method

.method public a(II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 471
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v2, "must start session before logging"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 472
    iget-boolean v0, p0, Lcdu;->E:Z

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    const-string v2, "only call logInputClassified once"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 473
    const-string v0, "nVideo"

    invoke-static {p2, v0, v1, p1}, Lcgp;->a(ILjava/lang/CharSequence;II)I

    .line 475
    if-nez p1, :cond_1

    .line 476
    const-string v3, "None"

    .line 484
    :goto_1
    if-nez p1, :cond_4

    const/16 v0, 0x64

    move v4, v0

    .line 485
    :goto_2
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "InputClassified"

    int-to-long v4, v4

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 486
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "VideosReceived"

    .line 487
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    int-to-long v4, p2

    .line 486
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 488
    iput-boolean v6, p0, Lcdu;->E:Z

    .line 489
    return-void

    :cond_0
    move v0, v1

    .line 472
    goto :goto_0

    .line 477
    :cond_1
    if-ne p2, p1, :cond_2

    .line 478
    const-string v3, "AllVideo"

    goto :goto_1

    .line 479
    :cond_2
    if-nez p2, :cond_3

    .line 480
    const-string v3, "NoVideo"

    goto :goto_1

    .line 482
    :cond_3
    const-string v3, "SomeVideo"

    goto :goto_1

    .line 484
    :cond_4
    mul-int/lit8 v0, p2, 0x64

    div-int/2addr v0, p1

    move v4, v0

    goto :goto_2
.end method

.method public a(IJ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 597
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v2, "must start session before logging"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 598
    invoke-virtual {p0}, Lcdu;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "only call setInputFootageDetails once"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 599
    const-string v0, "count"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcdu;->x:I

    .line 600
    const-string v0, "totalDurationUs"

    .line 601
    invoke-static {p2, p3, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v2

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    long-to-int v0, v2

    iput v0, p0, Lcdu;->y:I

    .line 602
    invoke-direct {p0, v1}, Lcdu;->a(Z)V

    .line 603
    return-void

    :cond_0
    move v0, v1

    .line 598
    goto :goto_0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 754
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "MediaScannerCompleted"

    invoke-interface {v0, v1, v2, p1, p2}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 758
    return-void
.end method

.method public a(JJZZ)V
    .locals 7

    .prologue
    const-wide/32 v4, 0xf4240

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 838
    iput-boolean v2, p0, Lcdu;->u:Z

    .line 839
    const-string v0, "openedDurationPicker"

    iget-boolean v3, p0, Lcdu;->u:Z

    invoke-direct {p0, v0, v3}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 840
    iget-boolean v0, p0, Lcdu;->v:Z

    if-nez v0, :cond_0

    if-eqz p5, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcdu;->v:Z

    .line 841
    const-string v0, "usedDurationSlider"

    iget-boolean v3, p0, Lcdu;->v:Z

    invoke-direct {p0, v0, v3}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 842
    iget-boolean v0, p0, Lcdu;->w:Z

    if-nez v0, :cond_1

    if-eqz p6, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    iput-boolean v1, p0, Lcdu;->w:Z

    .line 843
    const-string v0, "usedDurationPreset"

    iget-boolean v1, p0, Lcdu;->w:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 845
    iget v0, p0, Lcdu;->z:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 846
    div-long v0, p3, v4

    long-to-int v0, v0

    iput v0, p0, Lcdu;->z:I

    .line 848
    :cond_3
    div-long v0, p1, v4

    long-to-int v0, v0

    iput v0, p0, Lcdu;->A:I

    .line 850
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v3, "ViewDurationPicker"

    .line 851
    invoke-direct {p0, v2, p5, p6}, Lcdu;->a(ZZZ)Ljava/lang/String;

    move-result-object v2

    .line 850
    invoke-interface {v0, v1, v3, v2}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    return-void

    :cond_4
    move v0, v1

    .line 840
    goto :goto_0
.end method

.method public a(JLatu;)V
    .locals 7

    .prologue
    .line 728
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "SaveCompleted"

    const-string v3, "Successful"

    .line 731
    invoke-static {v3, p3}, Lcdu;->a(Ljava/lang/String;Latu;)Ljava/lang/String;

    move-result-object v3

    move-wide v4, p1

    .line 728
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 733
    return-void
.end method

.method public a(Laks;)V
    .locals 7

    .prologue
    .line 610
    invoke-virtual {p1}, Laks;->a()Laib;

    move-result-object v3

    .line 611
    sget-object v0, Laib;->c:Laib;

    if-eq v3, v0, :cond_1

    .line 642
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 618
    invoke-virtual {p1}, Laks;->b()J

    move-result-wide v0

    .line 624
    const-wide/32 v4, 0x4c4b40

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    .line 631
    invoke-virtual {p1}, Laks;->c()F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int v4, v0

    .line 632
    const/4 v0, 0x5

    if-lt v4, v0, :cond_2

    const/16 v0, 0x1388

    if-le v4, v0, :cond_3

    .line 637
    :cond_2
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "UnexpectedAnalysisSpeed"

    .line 638
    invoke-virtual {v3}, Laib;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xc

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ":"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    int-to-long v4, v4

    .line 637
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 640
    :cond_3
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "AnalysisSpeed"

    invoke-virtual {v3}, Laib;->toString()Ljava/lang/String;

    move-result-object v3

    int-to-long v4, v4

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public a(Latu;)V
    .locals 4

    .prologue
    .line 736
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "SaveCompleted"

    const-string v3, "Canceled"

    .line 739
    invoke-static {v3, p1}, Lcdu;->a(Ljava/lang/String;Latu;)Ljava/lang/String;

    move-result-object v3

    .line 736
    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    return-void
.end method

.method public a(Latu;Lcdx;)V
    .locals 4

    .prologue
    .line 743
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "SaveCompleted"

    const-string v3, "Failed"

    .line 746
    invoke-static {v3, p1}, Lcdu;->a(Ljava/lang/String;Latu;)Ljava/lang/String;

    move-result-object v3

    .line 743
    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "SaveError"

    .line 750
    invoke-static {p2}, Lcdu;->d(Lcdx;)Ljava/lang/String;

    move-result-object v3

    .line 747
    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    return-void
.end method

.method public a(Lbza;)V
    .locals 4

    .prologue
    .line 763
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 764
    iget v0, p0, Lcdu;->k:I

    if-nez v0, :cond_0

    .line 765
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "OutcomeShare"

    invoke-static {p1}, Lcdu;->b(Lbza;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    iget v0, p0, Lcdu;->l:I

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "OutcomeSaveAndShare"

    .line 768
    invoke-static {p1}, Lcdu;->b(Lbza;)Ljava/lang/String;

    move-result-object v3

    .line 767
    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :cond_0
    invoke-direct {p0}, Lcdu;->n()V

    .line 772
    iget v0, p0, Lcdu;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcdu;->k:I

    .line 773
    const-string v0, "share"

    iget v1, p0, Lcdu;->k:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    .line 774
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "Share"

    invoke-static {p1}, Lcdu;->b(Lbza;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    return-void
.end method

.method public a(Lbza;Lboh;Z)V
    .locals 6

    .prologue
    .line 645
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 646
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->o:Z

    .line 647
    const-string v0, "usedPreviewPlayer"

    iget-boolean v1, p0, Lcdu;->o:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 648
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "Preview"

    invoke-static {p1}, Lcdu;->b(Lbza;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "SoundtrackPlay"

    .line 650
    invoke-static {p2}, Lcdu;->a(Lboh;)Ljava/lang/String;

    move-result-object v3

    .line 649
    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    iget-object v0, p0, Lcdu;->e:Lamy;

    iget-wide v0, v0, Lamy;->a:D

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v3, v0

    .line 652
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "PlaybackOrientation"

    .line 653
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    if-eqz p3, :cond_0

    const-wide/16 v4, 0x1

    .line 652
    :goto_0
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 654
    return-void

    .line 653
    :cond_0
    const-wide/16 v4, 0x0

    goto :goto_0
.end method

.method public a(Lbza;Lboh;ZJ)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    .line 691
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v2, "must start session before logging"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 692
    iget v0, p0, Lcdu;->l:I

    if-nez v0, :cond_0

    .line 693
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v2, "MovieMakerSession"

    const-string v3, "OutcomeSave"

    invoke-static {p1}, Lcdu;->b(Lbza;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    iget v0, p0, Lcdu;->k:I

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v2, "MovieMakerSession"

    const-string v3, "OutcomeSaveAndShare"

    .line 696
    invoke-static {p1}, Lcdu;->b(Lbza;)Ljava/lang/String;

    move-result-object v4

    .line 695
    invoke-interface {v0, v2, v3, v4}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    :cond_0
    invoke-direct {p0}, Lcdu;->n()V

    .line 700
    iget v0, p0, Lcdu;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcdu;->l:I

    .line 701
    const-string v0, "save"

    iget v2, p0, Lcdu;->l:I

    invoke-direct {p0, v0, v2}, Lcdu;->a(Ljava/lang/String;I)V

    .line 702
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v2, "MovieMakerDiscrete"

    const-string v3, "Save"

    invoke-static {p1}, Lcdu;->b(Lbza;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v2, "MovieMakerDiscrete"

    const-string v3, "SoundtrackUsed"

    .line 704
    invoke-static {p2}, Lcdu;->a(Lboh;)Ljava/lang/String;

    move-result-object v4

    .line 703
    invoke-interface {v0, v2, v3, v4}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    iget-object v2, p0, Lcdu;->f:Lcdt;

    const-string v3, "MovieMakerDiscrete"

    const-string v4, "Affordance"

    if-eqz p3, :cond_1

    const-string v0, "SavePromo"

    :goto_0
    invoke-interface {v2, v3, v4, v0}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const-string v0, "MovieMakerDiscrete"

    const-string v2, "SaveWithSoundtrackChanged"

    iget-boolean v3, p0, Lcdu;->r:Z

    invoke-direct {p0, v0, v2, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 708
    const-string v0, "MovieMakerDiscrete"

    const-string v2, "SaveWithThemeChanged"

    iget-boolean v3, p0, Lcdu;->s:Z

    invoke-direct {p0, v0, v2, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 709
    const-string v0, "MovieMakerDiscrete"

    const-string v2, "SaveWithEditedTitle"

    iget-boolean v3, p0, Lcdu;->t:Z

    invoke-direct {p0, v0, v2, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 710
    const-string v2, "MovieMakerDiscrete"

    const-string v3, "SaveWithEditedScenes"

    invoke-virtual {p0}, Lcdu;->m()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v2, v3, v0}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 711
    iget v0, p0, Lcdu;->z:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_3

    move v4, v1

    .line 713
    :goto_2
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "SaveWithDurationChanged"

    int-to-long v6, v4

    .line 714
    cmp-long v3, v6, v8

    if-lez v3, :cond_4

    const-string v3, "Increased"

    :goto_3
    int-to-long v4, v4

    .line 713
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 716
    const-wide/32 v0, 0xf4240

    div-long v0, p4, v0

    long-to-int v0, v0

    .line 717
    const-string v2, "MovieMakerDiscrete"

    const-string v3, "SaveDuration"

    int-to-long v4, v0

    const-wide/16 v6, 0xe10

    const-string v8, "UnexpectedSaveDuration"

    sget-object v9, Lcdu;->c:Lcgh;

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Lcgh;)V

    .line 725
    return-void

    .line 705
    :cond_1
    const-string v0, "SaveActionBar"

    goto :goto_0

    :cond_2
    move v0, v1

    .line 710
    goto :goto_1

    .line 711
    :cond_3
    iget v0, p0, Lcdu;->A:I

    iget v1, p0, Lcdu;->z:I

    sub-int v1, v0, v1

    move v4, v1

    goto :goto_2

    .line 714
    :cond_4
    cmp-long v3, v6, v8

    if-gez v3, :cond_5

    const-string v3, "Decreased"

    goto :goto_3

    :cond_5
    const-string v3, "Default"

    goto :goto_3
.end method

.method public a(Lcdx;)V
    .locals 4

    .prologue
    .line 673
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "PlaybackError"

    .line 674
    invoke-static {p1}, Lcdu;->d(Lcdx;)Ljava/lang/String;

    move-result-object v3

    .line 673
    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    return-void
.end method

.method public a(Lcdz;)V
    .locals 2

    .prologue
    .line 810
    iget-object v0, p0, Lcdu;->H:Lcdz;

    if-eq p1, v0, :cond_0

    .line 811
    iget-object v0, p0, Lcdu;->f:Lcdt;

    invoke-virtual {p1}, Lcdz;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcdt;->a(Ljava/lang/String;)V

    .line 812
    iput-object p1, p0, Lcdu;->H:Lcdz;

    .line 814
    :cond_0
    return-void
.end method

.method public a(Lcea;)V
    .locals 4

    .prologue
    .line 450
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 451
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "SessionType"

    iget-object v3, p1, Lcea;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcdu;->f:Lcdt;

    invoke-interface {v0, p1}, Lcdt;->b(Ljava/lang/String;)V

    .line 375
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 436
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "InitApp"

    invoke-interface {v0, v1, v2}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 792
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 793
    sget-object v0, Lcdv;->a:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 807
    :goto_0
    return-void

    .line 795
    :pswitch_0
    iget v0, p0, Lcdu;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcdu;->h:I

    .line 796
    const-string v0, "sceneDelete"

    iget v1, p0, Lcdu;->h:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 799
    :pswitch_1
    iget v0, p0, Lcdu;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcdu;->i:I

    .line 800
    const-string v0, "sceneReorder"

    iget v1, p0, Lcdu;->i:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 803
    :pswitch_2
    iget v0, p0, Lcdu;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcdu;->j:I

    .line 804
    const-string v0, "sceneTrim"

    iget v1, p0, Lcdu;->j:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 793
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(II)V
    .locals 6

    .prologue
    .line 494
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 495
    const-string v0, "nDownloadedVideos"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1, p1}, Lcgp;->a(ILjava/lang/CharSequence;II)I

    .line 497
    if-nez p1, :cond_0

    .line 515
    :goto_0
    return-void

    .line 503
    :cond_0
    if-ne p1, p2, :cond_1

    .line 504
    const-string v3, "DownloadedAll"

    .line 510
    :goto_1
    mul-int/lit8 v0, p2, 0x64

    div-int v4, v0, p1

    .line 511
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "VideosDownloadedRatio"

    int-to-long v4, v4

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 513
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "VideosDownloadedCount"

    .line 514
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    int-to-long v4, p2

    .line 513
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 505
    :cond_1
    if-lez p2, :cond_2

    .line 506
    const-string v3, "DownloadedSome"

    goto :goto_1

    .line 508
    :cond_2
    const-string v3, "DownloadedNone"

    goto :goto_1
.end method

.method public b(Lcdx;)V
    .locals 4

    .prologue
    .line 678
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "CloudSavingError"

    .line 679
    invoke-static {p1}, Lcdu;->d(Lcdx;)Ljava/lang/String;

    move-result-object v3

    .line 678
    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 395
    iget-boolean v0, p0, Lcdu;->B:Z

    if-eqz v0, :cond_1

    .line 398
    invoke-virtual {p0}, Lcdu;->b()V

    .line 399
    iget-boolean v0, p0, Lcdu;->C:Z

    if-eqz v0, :cond_0

    .line 400
    invoke-direct {p0}, Lcdu;->o()V

    .line 402
    :cond_0
    iput-boolean v2, p0, Lcdu;->C:Z

    .line 404
    :cond_1
    iput-boolean v1, p0, Lcdu;->B:Z

    .line 405
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcdu;->g:J

    .line 407
    iget-object v0, p0, Lcdu;->f:Lcdt;

    invoke-interface {v0}, Lcdt;->a()V

    .line 426
    const-string v0, "sceneDelete"

    const-string v3, "SceneDelete"

    invoke-direct {p0, v0, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sceneReorder"

    const-string v3, "SceneReorder"

    invoke-direct {p0, v0, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sceneTrim"

    const-string v3, "SceneTrim"

    invoke-direct {p0, v0, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SceneEdit"

    invoke-virtual {p0}, Lcdu;->m()I

    move-result v3

    invoke-direct {p0, v0, v3}, Lcdu;->b(Ljava/lang/String;I)V

    const-string v0, "save"

    const-string v3, "CountSave"

    invoke-direct {p0, v0, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "share"

    const-string v3, "CountShare"

    invoke-direct {p0, v0, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "notification"

    const-string v3, "Notifications"

    invoke-direct {p0, v0, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "playerInitialized"

    const-string v3, "PlayerInitialized"

    invoke-direct {p0, v0, v3}, Lcdu;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "usedPreviewPlayer"

    const-string v3, "UsedPreviewPlayer"

    invoke-direct {p0, v0, v3}, Lcdu;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "openedStoryboardEditor"

    const-string v3, "OpenedStoryboardEditor"

    invoke-direct {p0, v0, v3}, Lcdu;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "openedMusicDrawer"

    const-string v3, "OpenedMusicDrawer"

    invoke-direct {p0, v0, v3}, Lcdu;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "soundtrackChanged"

    const-string v3, "SoundtrackChanged"

    invoke-direct {p0, v0, v3}, Lcdu;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "themeChanged"

    const-string v3, "ThemeChanged"

    invoke-direct {p0, v0, v3}, Lcdu;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "titleEdited"

    const-string v3, "TitleEdited"

    invoke-direct {p0, v0, v3}, Lcdu;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v3, "MovieMakerSession"

    const-string v4, "DurationPickerUsage"

    const-string v5, "openedDurationPicker"

    invoke-direct {p0, v5}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v5

    const-string v6, "usedDurationSlider"

    invoke-direct {p0, v6}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v6

    const-string v7, "usedDurationPreset"

    invoke-direct {p0, v7}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v7

    invoke-direct {p0, v5, v6, v7}, Lcdu;->a(ZZZ)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v3, v4, v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcdu;->f:Lcdt;

    const-string v5, "MovieMakerSession"

    const-string v6, "FeaturesUsed"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0x58

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "usedPreviewPlayer"

    invoke-direct {p0, v0}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v8

    const-string v0, "save"

    invoke-direct {p0, v0}, Lcdu;->c(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_0
    const-string v3, "themeChanged"

    invoke-direct {p0, v3}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v9

    const-string v3, "soundtrackChanged"

    invoke-direct {p0, v3}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v10

    const-string v3, "usedDurationSlider"

    invoke-direct {p0, v3}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "usedDurationPreset"

    invoke-direct {p0, v3}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_2
    move v3, v1

    :goto_1
    invoke-virtual {p0}, Lcdu;->m()I

    move-result v11

    if-lez v11, :cond_6

    :goto_2
    const-string v11, "titleEdited"

    invoke-direct {p0, v11}, Lcdu;->d(Ljava/lang/String;)Z

    move-result v11

    const/16 v12, 0x50

    invoke-static {v8, v12, v7}, Lcdu;->a(ZCLjava/lang/StringBuilder;)V

    const/16 v8, 0x53

    invoke-static {v0, v8, v7}, Lcdu;->a(ZCLjava/lang/StringBuilder;)V

    const/16 v0, 0x54

    invoke-static {v9, v0, v7}, Lcdu;->a(ZCLjava/lang/StringBuilder;)V

    const/16 v0, 0x4d

    invoke-static {v10, v0, v7}, Lcdu;->a(ZCLjava/lang/StringBuilder;)V

    const/16 v0, 0x44

    invoke-static {v3, v0, v7}, Lcdu;->a(ZCLjava/lang/StringBuilder;)V

    const/16 v0, 0x45

    invoke-static {v1, v0, v7}, Lcdu;->a(ZCLjava/lang/StringBuilder;)V

    const/16 v0, 0x4c

    invoke-static {v11, v0, v7}, Lcdu;->a(ZCLjava/lang/StringBuilder;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v5, v6, v0}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    iput v2, p0, Lcdu;->h:I

    iput v2, p0, Lcdu;->i:I

    iput v2, p0, Lcdu;->j:I

    iput v2, p0, Lcdu;->k:I

    iput v2, p0, Lcdu;->l:I

    iput v2, p0, Lcdu;->m:I

    iput-boolean v2, p0, Lcdu;->n:Z

    iput-boolean v2, p0, Lcdu;->o:Z

    iput-boolean v2, p0, Lcdu;->p:Z

    iput-boolean v2, p0, Lcdu;->q:Z

    iput-boolean v2, p0, Lcdu;->r:Z

    iput-boolean v2, p0, Lcdu;->s:Z

    iput-boolean v2, p0, Lcdu;->t:Z

    iput-boolean v2, p0, Lcdu;->u:Z

    iput-boolean v2, p0, Lcdu;->v:Z

    iput-boolean v2, p0, Lcdu;->w:Z

    .line 429
    const-string v0, "sceneDelete"

    iget v1, p0, Lcdu;->h:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    const-string v0, "sceneReorder"

    iget v1, p0, Lcdu;->i:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    const-string v0, "sceneTrim"

    iget v1, p0, Lcdu;->j:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    const-string v0, "save"

    iget v1, p0, Lcdu;->l:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    const-string v0, "share"

    iget v1, p0, Lcdu;->k:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    const-string v0, "notification"

    iget v1, p0, Lcdu;->m:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    const-string v0, "playerInitialized"

    iget-boolean v1, p0, Lcdu;->n:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "usedPreviewPlayer"

    iget-boolean v1, p0, Lcdu;->o:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "openedStoryboardEditor"

    iget-boolean v1, p0, Lcdu;->p:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "openedMusicDrawer"

    iget-boolean v1, p0, Lcdu;->q:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "soundtrackChanged"

    iget-boolean v1, p0, Lcdu;->r:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "themeChanged"

    iget-boolean v1, p0, Lcdu;->s:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "titleEdited"

    iget-boolean v1, p0, Lcdu;->t:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "openedDurationPicker"

    iget-boolean v1, p0, Lcdu;->u:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "usedDurationSlider"

    iget-boolean v1, p0, Lcdu;->v:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    const-string v0, "usedDurationPreset"

    iget-boolean v1, p0, Lcdu;->w:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 431
    invoke-direct {p0}, Lcdu;->p()V

    .line 432
    iget-object v0, p0, Lcdu;->e:Lamy;

    iget-wide v0, v0, Lamy;->a:D

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iget-object v1, p0, Lcdu;->f:Lcdt;

    const-string v2, "MovieMakerSession"

    const-string v3, "Start"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcdu;->f:Lcdt;

    const-string v2, "MovieMakerSession"

    const-string v3, "Source"

    if-nez p1, :cond_7

    const-string v0, "Unknown"

    :goto_3
    invoke-interface {v1, v2, v3, v0}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcdu;->f:Lcdt;

    const-string v0, "From"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-interface {v1, v0}, Lcdt;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcdu;->H:Lcdz;

    :cond_3
    sget-object v0, Lcdz;->a:Lcdz;

    invoke-virtual {p0, v0}, Lcdu;->a(Lcdz;)V

    .line 433
    return-void

    :cond_4
    move v0, v2

    .line 426
    goto/16 :goto_0

    :cond_5
    move v3, v2

    goto/16 :goto_1

    :cond_6
    move v1, v2

    goto/16 :goto_2

    :cond_7
    move-object v0, p1

    .line 432
    goto :goto_3

    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public c()V
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Lcdu;->C:Z

    if-nez v0, :cond_0

    .line 441
    invoke-direct {p0}, Lcdu;->o()V

    .line 442
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->C:Z

    .line 444
    :cond_0
    return-void
.end method

.method public c(II)V
    .locals 6

    .prologue
    .line 520
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 521
    const-string v0, "nDownloadedPhotos"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1, p1}, Lcgp;->a(ILjava/lang/CharSequence;II)I

    .line 523
    if-nez p1, :cond_0

    .line 541
    :goto_0
    return-void

    .line 529
    :cond_0
    if-ne p1, p2, :cond_1

    .line 530
    const-string v3, "DownloadedAll"

    .line 536
    :goto_1
    mul-int/lit8 v0, p2, 0x64

    div-int v4, v0, p1

    .line 537
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "PhotosDownloadedRatio"

    int-to-long v4, v4

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 539
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "PhotosDownloadedCount"

    .line 540
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    int-to-long v4, p2

    .line 539
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 531
    :cond_1
    if-lez p2, :cond_2

    .line 532
    const-string v3, "DownloadedSome"

    goto :goto_1

    .line 534
    :cond_2
    const-string v3, "DownloadedNone"

    goto :goto_1
.end method

.method public c(Lcdx;)V
    .locals 4

    .prologue
    .line 683
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "StoryboardValidationError"

    .line 686
    invoke-static {p1}, Lcdu;->d(Lcdx;)Ljava/lang/String;

    move-result-object v3

    .line 683
    invoke-interface {v0, v1, v2, v3}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 870
    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "dummy"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 871
    return-void
.end method

.method public d(II)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 544
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v2, "must start session before logging"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 545
    iget-boolean v0, p0, Lcdu;->F:Z

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    const-string v2, "only call logFootageChecked once"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 546
    sub-int v7, p1, p2

    .line 547
    const-string v0, "dropped"

    invoke-static {v7, v0, v1, p1}, Lcgp;->a(ILjava/lang/CharSequence;II)I

    .line 549
    if-nez p1, :cond_1

    .line 550
    const-string v3, "None"

    .line 558
    :goto_1
    if-nez p1, :cond_4

    move v4, v1

    .line 559
    :goto_2
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "FootageChecked"

    int-to-long v4, v4

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 560
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "VideosDiscarded"

    .line 561
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    int-to-long v4, v7

    .line 560
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 562
    iput-boolean v6, p0, Lcdu;->F:Z

    .line 563
    return-void

    :cond_0
    move v0, v1

    .line 545
    goto :goto_0

    .line 551
    :cond_1
    if-nez v7, :cond_2

    .line 552
    const-string v3, "AllOK"

    goto :goto_1

    .line 553
    :cond_2
    if-ne v7, p1, :cond_3

    .line 554
    const-string v3, "AllBad"

    goto :goto_1

    .line 556
    :cond_3
    const-string v3, "PartlyOK"

    goto :goto_1

    .line 558
    :cond_4
    mul-int/lit8 v0, v7, 0x64

    div-int v1, v0, p1

    move v4, v1

    goto :goto_2
.end method

.method public e(II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 566
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v2, "must start session before logging"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 567
    iget-boolean v0, p0, Lcdu;->G:Z

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    const-string v2, "only call logFootageOrientation once"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 568
    const-string v0, "nPortrait"

    invoke-static {p2, v0, v1, p1}, Lcgp;->a(ILjava/lang/CharSequence;II)I

    .line 570
    if-nez p1, :cond_1

    .line 571
    const-string v3, "None"

    .line 579
    :goto_1
    if-nez p1, :cond_4

    move v4, v1

    .line 580
    :goto_2
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "PortraitRatio"

    int-to-long v4, v4

    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 582
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerSession"

    const-string v2, "PortraitCount"

    .line 583
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    int-to-long v4, p2

    .line 582
    invoke-interface/range {v0 .. v5}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 584
    iput-boolean v6, p0, Lcdu;->G:Z

    .line 585
    return-void

    :cond_0
    move v0, v1

    .line 567
    goto :goto_0

    .line 572
    :cond_1
    if-nez p2, :cond_2

    .line 573
    const-string v3, "AllLandscape"

    goto :goto_1

    .line 574
    :cond_2
    if-ne p2, p1, :cond_3

    .line 575
    const-string v3, "AllPortrait"

    goto :goto_1

    .line 577
    :cond_3
    const-string v3, "MixedOrientation"

    goto :goto_1

    .line 579
    :cond_4
    mul-int/lit8 v0, p2, 0x64

    div-int v1, v0, p1

    move v4, v1

    goto :goto_2
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 606
    iget v0, p0, Lcdu;->x:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 10

    .prologue
    .line 657
    iget-boolean v0, p0, Lcdu;->n:Z

    if-nez v0, :cond_0

    .line 658
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcdu;->g:J

    sub-long v4, v0, v2

    .line 659
    const-string v2, "MovieMakerSession"

    const-string v3, "StartupDelay"

    const-wide/32 v6, 0xdbba0

    const-string v8, "UnexpectedStartupDelay"

    sget-object v9, Lcdu;->b:Lcgh;

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Lcgh;)V

    .line 668
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->n:Z

    .line 669
    const-string v0, "playerInitialized"

    iget-boolean v1, p0, Lcdu;->n:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 670
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 785
    iget-boolean v0, p0, Lcdu;->B:Z

    const-string v1, "must start session before logging"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 786
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->p:Z

    .line 787
    const-string v0, "openedStoryboardEditor"

    iget-boolean v1, p0, Lcdu;->p:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 788
    iget-object v0, p0, Lcdu;->f:Lcdt;

    const-string v1, "MovieMakerDiscrete"

    const-string v2, "ViewStoryboard"

    invoke-interface {v0, v1, v2}, Lcdt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 817
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->q:Z

    .line 818
    const-string v0, "openedMusicDrawer"

    iget-boolean v1, p0, Lcdu;->q:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 819
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 822
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->r:Z

    .line 823
    const-string v0, "soundtrackChanged"

    iget-boolean v1, p0, Lcdu;->r:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 824
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 827
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->s:Z

    .line 828
    const-string v0, "themeChanged"

    iget-boolean v1, p0, Lcdu;->s:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 829
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 832
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->t:Z

    .line 833
    const-string v0, "titleEdited"

    iget-boolean v1, p0, Lcdu;->t:Z

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;Z)V

    .line 834
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 855
    iget v0, p0, Lcdu;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcdu;->m:I

    .line 856
    const-string v0, "notification"

    iget v1, p0, Lcdu;->m:I

    invoke-direct {p0, v0, v1}, Lcdu;->a(Ljava/lang/String;I)V

    .line 857
    return-void
.end method

.method public m()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 860
    iget-object v0, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v1, "sceneDelete"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 862
    iget-object v1, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v2, "sceneReorder"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    add-int/2addr v0, v1

    .line 863
    iget-object v1, p0, Lcdu;->d:Landroid/content/SharedPreferences;

    const-string v2, "sceneTrim"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    add-int/2addr v0, v1

    .line 864
    return v0
.end method
