.class public final Lfop;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Lfsu;
.implements Lhjj;
.implements Lhmq;
.implements Lhob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Lfob;",
        ">;",
        "Lfsu;",
        "Lhjj;",
        "Lhmq;",
        "Lhob;"
    }
.end annotation


# instance fields
.field private N:Lhee;

.field private O:I

.field private P:Lfqq;

.field private Q:Lfoi;

.field private R:Landroid/widget/ListView;

.field private S:Z

.field private T:I

.field private U:Lizu;

.field private V:Lxn;

.field private final W:Licq;

.field private X:Lcne;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Llol;-><init>()V

    .line 83
    new-instance v0, Lhje;

    iget-object v1, p0, Lfop;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lfop;->O:I

    .line 95
    new-instance v0, Licq;

    iget-object v1, p0, Lfop;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a01be

    .line 96
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lfop;->W:Licq;

    .line 98
    new-instance v0, Lfoq;

    invoke-direct {v0, p0}, Lfoq;-><init>(Lfop;)V

    iput-object v0, p0, Lfop;->X:Lcne;

    .line 349
    return-void
.end method

.method static synthetic a(Lfop;)Lfoi;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lfop;->Q:Lfoi;

    return-object v0
.end method

.method static synthetic a(Lfop;Lxn;)Lxn;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lfop;->V:Lxn;

    return-object p1
.end method

.method static synthetic a(Lfop;Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 60
    iget-object v0, p0, Lfop;->at:Llnl;

    iget-object v1, p0, Lfop;->N:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {p0}, Lfop;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "story_id"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lfqt;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v0, "element_selection"

    iget-object v2, p0, Lfop;->Q:Lfoi;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v2

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Lz;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 202
    const v0, 0x7f0400e3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 203
    const/4 v0, 0x0

    .line 204
    invoke-virtual {p0}, Lfop;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "story_element_ref"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 205
    invoke-virtual {p0}, Lfop;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "story_element_ref"

    .line 206
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    new-instance v2, Lmms;

    invoke-direct {v2}, Lmms;-><init>()V

    invoke-virtual {v0, v2}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lmms;

    .line 208
    :cond_0
    new-instance v2, Lfqq;

    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v3

    invoke-direct {v2, v3, p0, v0, p0}, Lfqq;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Lmms;Lfsu;)V

    iput-object v2, p0, Lfop;->P:Lfqq;

    .line 209
    const v0, 0x7f100327

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lfop;->R:Landroid/widget/ListView;

    .line 210
    iget-object v0, p0, Lfop;->R:Landroid/widget/ListView;

    iget-object v2, p0, Lfop;->P:Lfqq;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 211
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lfob;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 216
    .line 217
    invoke-virtual {p0}, Lfop;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "story_render_sizes"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 218
    array-length v1, v0

    const-class v2, [Landroid/graphics/Point;

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Landroid/graphics/Point;

    .line 219
    new-instance v0, Lfqd;

    iget-object v1, p0, Lfop;->at:Llnl;

    iget-object v2, p0, Lfop;->N:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 220
    invoke-virtual {p0}, Lfop;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "story_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move v7, v6

    invoke-direct/range {v0 .. v8}, Lfqd;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 152
    iget-object v0, p0, Lfop;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 153
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 154
    invoke-virtual {p0}, Lfop;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 155
    if-eqz p1, :cond_0

    .line 156
    const-string v0, "story_load"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfop;->O:I

    .line 157
    const-string v0, "story_element_selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lfoi;

    iput-object v0, p0, Lfop;->Q:Lfoi;

    .line 158
    const-string v0, "restored_position"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lfop;->S:Z

    .line 159
    const-string v0, "num_items_prev"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lfop;->T:I

    .line 161
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 294
    const v0, 0x7f100042

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzx;

    .line 295
    const v1, 0x7f100091

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 296
    const v2, 0x7f100041

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljac;

    .line 297
    if-eqz v0, :cond_0

    iget-object v3, v0, Lnzx;->b:Ljava/lang/String;

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 298
    const/4 v3, 0x1

    new-array v3, v3, [Lnzx;

    .line 299
    aput-object v0, v3, v4

    .line 300
    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v0

    aget-object v4, v3, v4

    iget-object v4, v4, Lnzx;->b:Ljava/lang/String;

    invoke-static {v0, v4, v1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lfop;->U:Lizu;

    .line 301
    iget-object v0, p0, Lfop;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    new-instance v1, Lfor;

    iget-object v2, p0, Lfop;->at:Llnl;

    iget-object v4, p0, Lfop;->N:Lhee;

    .line 302
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v1, v2, v4, v3}, Lfor;-><init>(Landroid/content/Context;I[Lnzx;)V

    .line 301
    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 304
    :cond_0
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lfob;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 254
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p2, Lfob;

    invoke-virtual {p0, p2}, Lfop;->a(Lfob;)V

    return-void
.end method

.method public a(Lfob;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lfob;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 225
    if-eqz p1, :cond_4

    iget-object v0, p1, Lfob;->a:Lmmq;

    if-eqz v0, :cond_4

    .line 227
    iget-object v0, p0, Lfop;->Q:Lfoi;

    if-nez v0, :cond_0

    .line 228
    new-instance v0, Lfoi;

    invoke-direct {v0}, Lfoi;-><init>()V

    iput-object v0, p0, Lfop;->Q:Lfoi;

    .line 230
    :cond_0
    iget-object v0, p0, Lfop;->P:Lfqq;

    iget-object v1, p0, Lfop;->Q:Lfoi;

    invoke-virtual {v0, v1}, Lfqq;->a(Lfoi;)V

    .line 231
    iget-object v0, p0, Lfop;->P:Lfqq;

    invoke-virtual {v0, p1}, Lfqq;->a(Lfob;)V

    .line 232
    iget-object v0, p0, Lfop;->V:Lxn;

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lfop;->Q:Lfoi;

    invoke-virtual {v0}, Lfoi;->a()I

    move-result v0

    .line 234
    invoke-virtual {p0}, Lfop;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110008

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 236
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 234
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 237
    iget-object v0, p0, Lfop;->V:Lxn;

    invoke-virtual {v0}, Lxn;->i()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    :cond_1
    iget-object v0, p0, Lfop;->R:Landroid/widget/ListView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfop;->P:Lfqq;

    invoke-virtual {v0}, Lfqq;->a()I

    move-result v0

    if-ltz v0, :cond_3

    iget-boolean v0, p0, Lfop;->S:Z

    if-nez v0, :cond_3

    .line 240
    iput-boolean v6, p0, Lfop;->S:Z

    .line 241
    iget-object v0, p0, Lfop;->R:Landroid/widget/ListView;

    iget-object v1, p0, Lfop;->P:Lfqq;

    invoke-virtual {v1}, Lfqq;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 245
    :cond_2
    :goto_0
    iget-object v0, p0, Lfop;->W:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 250
    :goto_1
    return-void

    .line 242
    :cond_3
    iget v0, p0, Lfop;->T:I

    if-ltz v0, :cond_2

    .line 243
    iget-object v0, p0, Lfop;->R:Landroid/widget/ListView;

    iget-object v1, p0, Lfop;->P:Lfqq;

    iget v2, p0, Lfop;->T:I

    invoke-virtual {v1, v2}, Lfqq;->c(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    .line 247
    :cond_4
    iput v6, p0, Lfop;->O:I

    .line 248
    iget-object v0, p0, Lfop;->W:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 188
    const-string v0, ""

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 189
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 309
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CreateSinglePhotoTileTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    new-instance v0, Ldew;

    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lfop;->N:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ldew;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lfop;->U:Lizu;

    .line 315
    invoke-virtual {v1}, Lizu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lfop;->U:Lizu;

    .line 316
    invoke-virtual {v0, v1}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    const/16 v1, 0xa

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    .line 317
    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 318
    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Landroid/content/Context;)Ldew;

    move-result-object v0

    .line 319
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    .line 320
    invoke-virtual {p0, v0}, Lfop;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 334
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 165
    invoke-super {p0}, Llol;->aO_()V

    .line 166
    iget v1, p0, Lfop;->O:I

    if-ne v1, v0, :cond_1

    .line 167
    iget-object v0, p0, Lfop;->W:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 171
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lfop;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    iget-object v1, p0, Lfop;->X:Lcne;

    invoke-virtual {v0, v1}, Los;->a(Lxo;)Lxn;

    .line 172
    return-void

    .line 168
    :cond_1
    iget-object v1, p0, Lfop;->P:Lfqq;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lfop;->P:Lfqq;

    invoke-virtual {v1}, Lfqq;->getCount()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lfop;->W:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_0

    .line 168
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 326
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 327
    iget-object v0, p0, Lfop;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 328
    iget-object v0, p0, Lfop;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lfop;->N:Lhee;

    .line 329
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 176
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 177
    const-string v0, "story_load"

    iget v1, p0, Lfop;->O:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 178
    const-string v0, "story_element_selection"

    iget-object v1, p0, Lfop;->Q:Lfoi;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 179
    const-string v0, "restored_position"

    iget-boolean v1, p0, Lfop;->S:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 180
    iget-object v0, p0, Lfop;->P:Lfqq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfop;->R:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 181
    const-string v0, "num_items_prev"

    iget-object v1, p0, Lfop;->P:Lfqq;

    iget-object v2, p0, Lfop;->R:Landroid/widget/ListView;

    .line 182
    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lfqq;->b(I)I

    move-result v1

    .line 181
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 267
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1005e4

    if-ne v0, v1, :cond_0

    .line 268
    const v0, 0x7f10003e

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoh;

    .line 269
    check-cast p1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;

    .line 270
    iget-object v1, p0, Lfop;->Q:Lfoi;

    if-eqz v1, :cond_0

    .line 272
    iget-object v1, p0, Lfop;->Q:Lfoi;

    .line 273
    invoke-virtual {p1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->d()Z

    move-result v2

    .line 272
    invoke-virtual {v1, v0, v2}, Lfoi;->a(Lfoh;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    invoke-virtual {p1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->e()V

    .line 275
    iget-object v0, p0, Lfop;->Q:Lfoi;

    invoke-virtual {v0}, Lfoi;->a()I

    move-result v0

    .line 276
    invoke-virtual {p0}, Lfop;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110008

    new-array v3, v3, [Ljava/lang/Object;

    .line 278
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 276
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 279
    iget-object v0, p0, Lfop;->V:Lxn;

    invoke-virtual {v0}, Lxn;->i()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    invoke-virtual {p0}, Lfop;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110009

    new-array v2, v3, [Ljava/lang/Object;

    .line 284
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 282
    invoke-virtual {v0, v1, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0596

    .line 285
    invoke-virtual {p0, v1}, Lfop;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 281
    invoke-static {v6, v0, v1, v6}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 286
    invoke-virtual {p0}, Lfop;->p()Lae;

    move-result-object v1

    const-string v2, "dialog_alert"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method
