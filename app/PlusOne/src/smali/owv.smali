.class public final Lowv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lowv;


# instance fields
.field private b:[Lows;

.field private c:[Lowx;

.field private d:[Lowu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 684
    const/4 v0, 0x0

    new-array v0, v0, [Lowv;

    sput-object v0, Lowv;->a:[Lowv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 685
    invoke-direct {p0}, Loxq;-><init>()V

    .line 688
    sget-object v0, Lows;->a:[Lows;

    iput-object v0, p0, Lowv;->b:[Lows;

    .line 691
    sget-object v0, Lowx;->a:[Lowx;

    iput-object v0, p0, Lowv;->c:[Lowx;

    .line 694
    sget-object v0, Lowu;->a:[Lowu;

    iput-object v0, p0, Lowv;->d:[Lowu;

    .line 685
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 726
    .line 727
    iget-object v0, p0, Lowv;->b:[Lows;

    if-eqz v0, :cond_1

    .line 728
    iget-object v3, p0, Lowv;->b:[Lows;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 729
    if-eqz v5, :cond_0

    .line 730
    const/4 v6, 0x1

    .line 731
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 728
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 735
    :cond_2
    iget-object v2, p0, Lowv;->c:[Lowx;

    if-eqz v2, :cond_4

    .line 736
    iget-object v3, p0, Lowv;->c:[Lowx;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 737
    if-eqz v5, :cond_3

    .line 738
    const/4 v6, 0x2

    .line 739
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 736
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 743
    :cond_4
    iget-object v2, p0, Lowv;->d:[Lowu;

    if-eqz v2, :cond_6

    .line 744
    iget-object v2, p0, Lowv;->d:[Lowu;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 745
    if-eqz v4, :cond_5

    .line 746
    const/4 v5, 0x3

    .line 747
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 744
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 751
    :cond_6
    iget-object v1, p0, Lowv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 752
    iput v0, p0, Lowv;->ai:I

    .line 753
    return v0
.end method

.method public a(Loxn;)Lowv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 761
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 762
    sparse-switch v0, :sswitch_data_0

    .line 766
    iget-object v2, p0, Lowv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 767
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lowv;->ah:Ljava/util/List;

    .line 770
    :cond_1
    iget-object v2, p0, Lowv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 772
    :sswitch_0
    return-object p0

    .line 777
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 778
    iget-object v0, p0, Lowv;->b:[Lows;

    if-nez v0, :cond_3

    move v0, v1

    .line 779
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lows;

    .line 780
    iget-object v3, p0, Lowv;->b:[Lows;

    if-eqz v3, :cond_2

    .line 781
    iget-object v3, p0, Lowv;->b:[Lows;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 783
    :cond_2
    iput-object v2, p0, Lowv;->b:[Lows;

    .line 784
    :goto_2
    iget-object v2, p0, Lowv;->b:[Lows;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 785
    iget-object v2, p0, Lowv;->b:[Lows;

    new-instance v3, Lows;

    invoke-direct {v3}, Lows;-><init>()V

    aput-object v3, v2, v0

    .line 786
    iget-object v2, p0, Lowv;->b:[Lows;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 787
    invoke-virtual {p1}, Loxn;->a()I

    .line 784
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 778
    :cond_3
    iget-object v0, p0, Lowv;->b:[Lows;

    array-length v0, v0

    goto :goto_1

    .line 790
    :cond_4
    iget-object v2, p0, Lowv;->b:[Lows;

    new-instance v3, Lows;

    invoke-direct {v3}, Lows;-><init>()V

    aput-object v3, v2, v0

    .line 791
    iget-object v2, p0, Lowv;->b:[Lows;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 795
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 796
    iget-object v0, p0, Lowv;->c:[Lowx;

    if-nez v0, :cond_6

    move v0, v1

    .line 797
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lowx;

    .line 798
    iget-object v3, p0, Lowv;->c:[Lowx;

    if-eqz v3, :cond_5

    .line 799
    iget-object v3, p0, Lowv;->c:[Lowx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 801
    :cond_5
    iput-object v2, p0, Lowv;->c:[Lowx;

    .line 802
    :goto_4
    iget-object v2, p0, Lowv;->c:[Lowx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 803
    iget-object v2, p0, Lowv;->c:[Lowx;

    new-instance v3, Lowx;

    invoke-direct {v3}, Lowx;-><init>()V

    aput-object v3, v2, v0

    .line 804
    iget-object v2, p0, Lowv;->c:[Lowx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 805
    invoke-virtual {p1}, Loxn;->a()I

    .line 802
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 796
    :cond_6
    iget-object v0, p0, Lowv;->c:[Lowx;

    array-length v0, v0

    goto :goto_3

    .line 808
    :cond_7
    iget-object v2, p0, Lowv;->c:[Lowx;

    new-instance v3, Lowx;

    invoke-direct {v3}, Lowx;-><init>()V

    aput-object v3, v2, v0

    .line 809
    iget-object v2, p0, Lowv;->c:[Lowx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 813
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 814
    iget-object v0, p0, Lowv;->d:[Lowu;

    if-nez v0, :cond_9

    move v0, v1

    .line 815
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lowu;

    .line 816
    iget-object v3, p0, Lowv;->d:[Lowu;

    if-eqz v3, :cond_8

    .line 817
    iget-object v3, p0, Lowv;->d:[Lowu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 819
    :cond_8
    iput-object v2, p0, Lowv;->d:[Lowu;

    .line 820
    :goto_6
    iget-object v2, p0, Lowv;->d:[Lowu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 821
    iget-object v2, p0, Lowv;->d:[Lowu;

    new-instance v3, Lowu;

    invoke-direct {v3}, Lowu;-><init>()V

    aput-object v3, v2, v0

    .line 822
    iget-object v2, p0, Lowv;->d:[Lowu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 823
    invoke-virtual {p1}, Loxn;->a()I

    .line 820
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 814
    :cond_9
    iget-object v0, p0, Lowv;->d:[Lowu;

    array-length v0, v0

    goto :goto_5

    .line 826
    :cond_a
    iget-object v2, p0, Lowv;->d:[Lowu;

    new-instance v3, Lowu;

    invoke-direct {v3}, Lowu;-><init>()V

    aput-object v3, v2, v0

    .line 827
    iget-object v2, p0, Lowv;->d:[Lowu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 762
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 699
    iget-object v1, p0, Lowv;->b:[Lows;

    if-eqz v1, :cond_1

    .line 700
    iget-object v2, p0, Lowv;->b:[Lows;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 701
    if-eqz v4, :cond_0

    .line 702
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 700
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 706
    :cond_1
    iget-object v1, p0, Lowv;->c:[Lowx;

    if-eqz v1, :cond_3

    .line 707
    iget-object v2, p0, Lowv;->c:[Lowx;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 708
    if-eqz v4, :cond_2

    .line 709
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 707
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 713
    :cond_3
    iget-object v1, p0, Lowv;->d:[Lowu;

    if-eqz v1, :cond_5

    .line 714
    iget-object v1, p0, Lowv;->d:[Lowu;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 715
    if-eqz v3, :cond_4

    .line 716
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 714
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 720
    :cond_5
    iget-object v0, p0, Lowv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 722
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 681
    invoke-virtual {p0, p1}, Lowv;->a(Loxn;)Lowv;

    move-result-object v0

    return-object v0
.end method
