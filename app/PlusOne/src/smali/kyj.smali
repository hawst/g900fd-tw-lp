.class public final Lkyj;
.super Landroid/text/style/ClickableSpan;
.source "PG"


# instance fields
.field private synthetic a:Landroid/text/style/URLSpan;

.field private synthetic b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;Landroid/text/style/URLSpan;)V
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Lkyj;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    iput-object p2, p0, Lkyj;->a:Landroid/text/style/URLSpan;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lkyj;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Lkyk;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lkyj;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Lkyk;

    move-result-object v0

    iget-object v1, p0, Lkyj;->a:Landroid/text/style/URLSpan;

    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lkyk;->a(Landroid/net/Uri;)V

    .line 543
    :cond_0
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 547
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 548
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 549
    return-void
.end method
