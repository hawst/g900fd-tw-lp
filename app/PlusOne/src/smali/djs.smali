.class public final Ldjs;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmbc;",
        "Lmbd;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Long;

.field private static final b:Ljava/lang/Long;


# instance fields
.field private final p:[Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Z

.field private final s:Lkzl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Ldjs;->a:Ljava/lang/Long;

    .line 31
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Ldjs;->b:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 52
    const-string v3, "getactivitiesbyid"

    new-instance v4, Lmbc;

    invoke-direct {v4}, Lmbc;-><init>()V

    new-instance v5, Lmbd;

    invoke-direct {v5}, Lmbd;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 55
    iget-object v0, p0, Ldjs;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldjs;->s:Lkzl;

    .line 56
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    iput-object v0, p0, Ldjs;->p:[Ljava/lang/String;

    .line 57
    iput-object p4, p0, Ldjs;->q:Ljava/lang/String;

    .line 58
    iput-boolean p5, p0, Ldjs;->r:Z

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;I[Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 41
    const-string v4, "getactivitiesbyid"

    new-instance v5, Lmbc;

    invoke-direct {v5}, Lmbc;-><init>()V

    new-instance v6, Lmbd;

    invoke-direct {v6}, Lmbd;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 44
    iget-object v0, p0, Ldjs;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldjs;->s:Lkzl;

    .line 45
    iput-object p4, p0, Ldjs;->p:[Ljava/lang/String;

    .line 46
    iput-object p5, p0, Ldjs;->q:Ljava/lang/String;

    .line 47
    iput-boolean p6, p0, Ldjs;->r:Z

    .line 48
    return-void
.end method


# virtual methods
.method protected a(Lmbc;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 63
    new-instance v0, Lnvc;

    invoke-direct {v0}, Lnvc;-><init>()V

    iput-object v0, p1, Lmbc;->a:Lnvc;

    .line 64
    iget-object v0, p1, Lmbc;->a:Lnvc;

    .line 65
    iget-object v1, p0, Ldjs;->p:[Ljava/lang/String;

    iput-object v1, v0, Lnvc;->a:[Ljava/lang/String;

    .line 69
    iget-object v1, p0, Ldjs;->p:[Ljava/lang/String;

    array-length v1, v1

    if-le v1, v6, :cond_0

    .line 70
    new-instance v1, Lnvd;

    invoke-direct {v1}, Lnvd;-><init>()V

    iput-object v1, v0, Lnvc;->c:Lnvd;

    .line 71
    iget-object v1, v0, Lnvc;->c:Lnvd;

    sget-object v2, Ldjs;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Ldjs;->p:[Ljava/lang/String;

    array-length v4, v4

    int-to-long v4, v4

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lnvd;->b:Ljava/lang/Long;

    .line 72
    iget-object v1, v0, Lnvc;->c:Lnvd;

    sget-object v2, Ldjs;->b:Ljava/lang/Long;

    iput-object v2, v1, Lnvd;->a:Ljava/lang/Long;

    .line 75
    :cond_0
    new-instance v1, Loxz;

    invoke-direct {v1}, Loxz;-><init>()V

    iput-object v1, v0, Lnvc;->d:Loxz;

    .line 76
    iget-object v1, v0, Lnvc;->d:Loxz;

    iget-object v2, p0, Ldjs;->s:Lkzl;

    iget-object v3, p0, Ldjs;->f:Landroid/content/Context;

    iget v4, p0, Ldjs;->c:I

    invoke-interface {v2, v3, v4}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v2

    iput-object v2, v1, Loxz;->a:[I

    .line 77
    const/4 v1, 0x2

    iput v1, v0, Lnvc;->b:I

    .line 79
    iget-object v1, p0, Ldjs;->q:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 80
    iget-object v1, p0, Ldjs;->q:Ljava/lang/String;

    iget-boolean v2, p0, Ldjs;->r:Z

    .line 81
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 80
    invoke-static {v1, v2}, Llap;->a(Ljava/lang/String;Ljava/lang/Boolean;)Logy;

    move-result-object v1

    iput-object v1, v0, Lnvc;->e:Logy;

    .line 82
    iget-object v1, p0, Ldjs;->p:[Ljava/lang/String;

    array-length v1, v1

    if-ne v1, v6, :cond_1

    .line 83
    iget-object v0, v0, Lnvc;->e:Logy;

    const/4 v1, 0x0

    iput v1, v0, Logy;->b:I

    .line 86
    :cond_1
    return-void
.end method

.method protected a(Lmbd;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 90
    iget-object v0, p1, Lmbd;->a:Lnwn;

    .line 91
    iget-object v1, v0, Lnwn;->a:[Logr;

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Ldjs;->f:Landroid/content/Context;

    iget v2, p0, Ldjs;->c:I

    iget-object v3, v0, Lnwn;->a:[Logr;

    invoke-static {v1, v2, v3, v4, v4}, Llap;->a(Landroid/content/Context;I[Logr;IZ)V

    .line 94
    iget-object v0, v0, Lnwn;->a:[Logr;

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 95
    iget-object v0, v0, Logr;->i:Ljava/lang/String;

    .line 96
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lmbc;

    invoke-virtual {p0, p1}, Ldjs;->a(Lmbc;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lmbd;

    invoke-virtual {p0, p1}, Ldjs;->a(Lmbd;)V

    return-void
.end method
