.class public final Lmoj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7560
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 7576
    const/4 v0, 0x0

    .line 7577
    iget-object v1, p0, Lmoj;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 7578
    const/4 v0, 0x1

    iget-object v1, p0, Lmoj;->a:Ljava/lang/Boolean;

    .line 7579
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 7581
    :cond_0
    iget-object v1, p0, Lmoj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7582
    iput v0, p0, Lmoj;->ai:I

    .line 7583
    return v0
.end method

.method public a(Loxn;)Lmoj;
    .locals 2

    .prologue
    .line 7591
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7592
    sparse-switch v0, :sswitch_data_0

    .line 7596
    iget-object v1, p0, Lmoj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7597
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmoj;->ah:Ljava/util/List;

    .line 7600
    :cond_1
    iget-object v1, p0, Lmoj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7602
    :sswitch_0
    return-object p0

    .line 7607
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmoj;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 7592
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7567
    iget-object v0, p0, Lmoj;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 7568
    const/4 v0, 0x1

    iget-object v1, p0, Lmoj;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7570
    :cond_0
    iget-object v0, p0, Lmoj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7572
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7556
    invoke-virtual {p0, p1}, Lmoj;->a(Loxn;)Lmoj;

    move-result-object v0

    return-object v0
.end method
