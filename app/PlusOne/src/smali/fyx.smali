.class public final Lfyx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdd;


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/views/OneProfileHeader;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/OneProfileHeader;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lfyx;->a:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkda;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 464
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 491
    :goto_0
    return-void

    .line 467
    :sswitch_0
    invoke-virtual {p1}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    .line 468
    instance-of v1, v0, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 469
    check-cast v0, Landroid/graphics/Bitmap;

    .line 470
    iget-object v1, p0, Lfyx;->a:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Llho;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 471
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 483
    :goto_1
    iget-object v1, p0, Lfyx;->a:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-static {v1, v0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Lcom/google/android/apps/plus/views/OneProfileHeader;Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_0

    .line 472
    :cond_0
    instance-of v1, v0, Lirp;

    if-eqz v1, :cond_1

    .line 473
    new-instance v1, Liro;

    check-cast v0, Lirp;

    .line 474
    invoke-static {}, Lcom/google/android/apps/plus/views/OneProfileHeader;->l()Lizs;

    move-result-object v2

    invoke-virtual {v2}, Lizs;->b()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Liro;-><init>(Lirp;Landroid/graphics/Bitmap$Config;)V

    .line 475
    iget-object v0, p0, Lfyx;->a:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 476
    invoke-virtual {v1}, Liro;->getIntrinsicWidth()I

    move-result v2

    .line 477
    invoke-virtual {v1}, Liro;->getIntrinsicHeight()I

    move-result v3

    .line 476
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 475
    invoke-static {v0, v2}, Llho;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Liro;->a(Landroid/graphics/Bitmap;)V

    move-object v0, v1

    .line 480
    goto :goto_1

    .line 481
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 487
    :sswitch_1
    iget-object v0, p0, Lfyx;->a:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Z)V

    goto :goto_0

    .line 464
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 456
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 460
    return-void
.end method
