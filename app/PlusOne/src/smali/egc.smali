.class public final Legc;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lbc;
.implements Lze;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnFocusChangeListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lze;"
    }
.end annotation


# instance fields
.field private N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private ah:Lfbs;

.field private ai:Ljava/lang/Integer;

.field private aj:Landroid/support/v7/widget/SearchView;

.field private ak:Landroid/view/View;

.field private al:I

.field private am:Ljava/lang/CharSequence;

.field private an:Ljava/lang/CharSequence;

.field private ao:Z

.field private ap:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private aq:Z

.field private ar:Z

.field private as:Ljava/lang/String;

.field private aw:Z

.field private ax:Z

.field private final ay:Licq;

.field private final az:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 71
    invoke-direct {p0}, Legi;-><init>()V

    .line 107
    iput v0, p0, Legc;->al:I

    .line 115
    iput-boolean v0, p0, Legc;->aw:Z

    .line 120
    new-instance v0, Licq;

    iget-object v1, p0, Legc;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a057b

    .line 121
    invoke-virtual {v0, v1}, Licq;->a(I)Licq;

    move-result-object v0

    new-instance v1, Legd;

    invoke-direct {v1, p0}, Legd;-><init>(Legc;)V

    .line 122
    invoke-virtual {v0, v1}, Licq;->a(Lico;)Licq;

    move-result-object v0

    iput-object v0, p0, Legc;->ay:Licq;

    .line 140
    new-instance v0, Lege;

    invoke-direct {v0, p0}, Lege;-><init>(Legc;)V

    iput-object v0, p0, Legc;->az:Lfhh;

    .line 776
    return-void
.end method

.method static synthetic a(Legc;)Landroid/view/View;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Legc;->ak:Landroid/view/View;

    return-object v0
.end method

.method private a(ILfib;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 631
    iget-object v2, p0, Legc;->ai:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Legc;->ai:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_0

    instance-of v2, p2, Lfhx;

    if-nez v2, :cond_1

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    check-cast p2, Lfhx;

    .line 638
    invoke-virtual {p2}, Lfhx;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 642
    const/4 v2, 0x0

    iput-object v2, p0, Legc;->ai:Ljava/lang/Integer;

    .line 643
    invoke-virtual {p2}, Lfhx;->e()Z

    move-result v2

    iput-boolean v2, p0, Legc;->Z:Z

    .line 644
    iget-boolean v2, p0, Legc;->Z:Z

    if-eqz v2, :cond_2

    .line 645
    invoke-virtual {p0}, Legc;->o()Landroid/content/res/Resources;

    move-result-object v2

    .line 646
    const v3, 0x7f0a07ac

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 647
    invoke-virtual {p0}, Legc;->n()Lz;

    move-result-object v3

    invoke-static {v3, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 650
    :cond_2
    iget-object v2, p0, Legc;->ap:Ljava/util/HashSet;

    invoke-virtual {p2}, Lfhx;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 651
    iput-boolean v0, p0, Legc;->ao:Z

    .line 653
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v2

    iget-boolean v3, p0, Legc;->aw:Z

    if-nez v3, :cond_3

    :goto_1
    invoke-direct {p0, v2, v0}, Legc;->a(Landroid/view/View;Z)V

    .line 654
    iput-boolean v1, p0, Legc;->aw:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 653
    goto :goto_1
.end method

.method private a(Landroid/view/View;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 584
    if-nez p1, :cond_0

    .line 628
    :goto_0
    return-void

    .line 588
    :cond_0
    invoke-virtual {p0}, Legc;->U()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Legc;->ao:Z

    if-nez v0, :cond_7

    .line 589
    :cond_1
    iget-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    if-nez v0, :cond_6

    .line 590
    iget-boolean v0, p0, Legc;->ao:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Legc;->ax:Z

    if-nez v0, :cond_3

    .line 591
    iget-boolean v0, p0, Legc;->aq:Z

    if-nez v0, :cond_6

    .line 594
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Legc;->d(Landroid/view/View;)V

    .line 616
    :goto_1
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setVisibility(I)V

    .line 626
    :cond_2
    :goto_2
    invoke-virtual {p0}, Legc;->Z_()V

    .line 627
    invoke-virtual {p0}, Legc;->ag()V

    goto :goto_0

    .line 600
    :cond_3
    iget-boolean v0, p0, Legc;->aq:Z

    if-eqz v0, :cond_5

    .line 601
    iget-boolean v0, p0, Legc;->ar:Z

    if-eqz v0, :cond_4

    .line 602
    const v0, 0x7f0a064f

    invoke-virtual {p0, v0}, Legc;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 610
    :goto_3
    iget-object v1, p0, Legc;->ay:Licq;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Licq;->a(Ljava/lang/CharSequence;)Licq;

    .line 611
    iget-object v0, p0, Legc;->ay:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 604
    :cond_4
    const v0, 0x7f0a064c

    invoke-virtual {p0, v0}, Legc;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 607
    :cond_5
    invoke-virtual {p0}, Legc;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0ab9

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Legc;->an:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 614
    :cond_6
    iget-object v0, p0, Legc;->ay:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 618
    :cond_7
    iget-object v0, p0, Legc;->ay:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 619
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setVisibility(I)V

    .line 621
    if-eqz p2, :cond_2

    .line 622
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h()V

    goto :goto_2
.end method

.method static synthetic a(Legc;ILfib;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Legc;->a(ILfib;)V

    return-void
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 767
    invoke-virtual {p0}, Legc;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 768
    if-nez v0, :cond_0

    .line 774
    :goto_0
    return-void

    .line 772
    :cond_0
    const-string v1, "hide_search_view"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Legc;->aq:Z

    .line 773
    const-string v1, "search_local_videos"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legc;->ar:Z

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 2

    .prologue
    .line 419
    invoke-virtual {p0}, Legc;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 420
    if-eqz v0, :cond_0

    const-string v1, "is_videos_destination"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lhmw;->ao:Lhmw;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhmw;->M:Lhmw;

    goto :goto_0
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 687
    invoke-super {p0}, Legi;->L_()V

    .line 689
    iget-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 699
    :goto_0
    return-void

    .line 693
    :cond_0
    iput-boolean v1, p0, Legc;->Z:Z

    .line 694
    iget-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Legc;->a(Ljava/lang/CharSequence;Z)V

    .line 695
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Legc;->a(Landroid/view/View;Z)V

    .line 696
    invoke-virtual {p0}, Legc;->Z_()V

    .line 697
    iget-object v0, p0, Legc;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Legc;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 698
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 697
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Legc;->ah:Lfbs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Legc;->ah:Lfbs;

    invoke-virtual {v0}, Lfbs;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Legc;->ac:Z

    if-eqz v0, :cond_0

    .line 335
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    .line 337
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    goto :goto_0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 812
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l()Z

    move-result v0

    return v0
.end method

.method protected Y_()Z
    .locals 1

    .prologue
    .line 558
    iget-boolean v0, p0, Legc;->ac:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Legc;->am()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Legc;->X:Lctz;

    .line 559
    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Legc;->X:Lctz;

    .line 560
    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected Z_()V
    .locals 2

    .prologue
    .line 796
    invoke-super {p0}, Legi;->Z_()V

    .line 797
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    .line 798
    invoke-virtual {p0}, Legc;->J_()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Legc;->ak:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 801
    :cond_0
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 210
    iget-object v0, p0, Legc;->at:Llnl;

    .line 211
    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400de

    .line 210
    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 213
    iget-object v0, p0, Legc;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0291

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 216
    new-instance v0, Lfbs;

    iget-object v3, p0, Legc;->at:Llnl;

    iget-object v4, p0, Legc;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lfbs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Legc;->ah:Lfbs;

    .line 217
    iget-object v0, p0, Legc;->ah:Lfbs;

    invoke-virtual {v0, p0}, Lfbs;->a(Landroid/view/View$OnClickListener;)V

    .line 218
    iget-object v0, p0, Legc;->ah:Lfbs;

    invoke-virtual {v0, p0}, Lfbs;->a(Landroid/view/View$OnLongClickListener;)V

    .line 219
    iget-object v0, p0, Legc;->ah:Lfbs;

    iget-object v3, p0, Legc;->as:Ljava/lang/String;

    iget v4, p0, Legc;->al:I

    invoke-virtual {v0, v3, v4}, Lfbs;->a(Ljava/lang/String;I)V

    .line 221
    iget-object v0, p0, Legc;->ah:Lfbs;

    invoke-virtual {v0, v6}, Lfbs;->b(Z)V

    .line 223
    iget-object v0, p0, Legc;->ah:Lfbs;

    iget-boolean v3, p0, Legc;->ar:Z

    invoke-virtual {v0, v3}, Lfbs;->a(Z)V

    .line 225
    const v0, 0x7f100304

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 226
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 227
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v2, Ljvl;

    iget-object v3, p0, Legc;->at:Llnl;

    invoke-direct {v2, v3}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v2, v2, Ljvl;->a:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 228
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(Z)V

    .line 229
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v2, Legh;

    invoke-direct {v2}, Legh;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 230
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v2, p0, Legc;->ah:Lfbs;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 231
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v2, 0x7f020415

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 232
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkc;)V

    .line 234
    const v0, 0x7f10031c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Legc;->ak:Landroid/view/View;

    .line 236
    iget-boolean v0, p0, Legc;->ax:Z

    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {p0}, Legc;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v5, v7, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 248
    :goto_0
    invoke-direct {p0, v1, v5}, Legc;->a(Landroid/view/View;Z)V

    .line 249
    return-object v1

    .line 240
    :cond_0
    iget-boolean v0, p0, Legc;->ar:Z

    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {p0}, Legc;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v5, v7, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 244
    :cond_1
    invoke-virtual {p0}, Legc;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v6, v7, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 513
    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    .line 524
    :goto_0
    return-object v0

    .line 515
    :pswitch_0
    new-instance v0, Legg;

    iget-object v1, p0, Legc;->at:Llnl;

    invoke-direct {v0, v1, v4}, Legg;-><init>(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_0

    .line 519
    :pswitch_1
    new-instance v0, Lfbu;

    iget-object v1, p0, Legc;->at:Llnl;

    iget-object v2, p0, Legc;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Legc;->as:Ljava/lang/String;

    iget v5, p0, Legc;->aa:I

    invoke-direct/range {v0 .. v5}, Lfbu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;I)V

    goto :goto_0

    .line 513
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 156
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 157
    invoke-virtual {p0}, Legc;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_folders_only"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legc;->ax:Z

    .line 159
    if-eqz p1, :cond_8

    .line 160
    const-string v0, "photo_search_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    const-string v0, "photo_search_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    .line 164
    :cond_0
    const-string v0, "search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    const-string v0, "search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Legc;->al:I

    .line 168
    :cond_1
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    .line 172
    :cond_2
    const-string v0, "delayed_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 173
    const-string v0, "delayed_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Legc;->am:Ljava/lang/CharSequence;

    .line 176
    :cond_3
    const-string v0, "results_present"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 177
    const-string v0, "results_present"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legc;->ao:Z

    .line 180
    :cond_4
    const-string v0, "is_first_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 181
    const-string v0, "is_first_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legc;->aw:Z

    .line 184
    :cond_5
    const-string v0, "seen_search_modes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 185
    const-string v0, "seen_search_modes"

    .line 186
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Legc;->ap:Ljava/util/HashSet;

    .line 193
    :cond_6
    :goto_0
    iget-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 194
    iget v0, p0, Legc;->al:I

    invoke-static {v3, v0}, Ljvj;->a(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legc;->as:Ljava/lang/String;

    .line 200
    :goto_1
    invoke-direct {p0}, Legc;->ac()V

    .line 202
    iget-object v0, p0, Legc;->ap:Ljava/util/HashSet;

    if-nez v0, :cond_7

    .line 203
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Legc;->ap:Ljava/util/HashSet;

    .line 205
    :cond_7
    return-void

    .line 190
    :cond_8
    invoke-virtual {p0}, Legc;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legc;->am:Ljava/lang/CharSequence;

    goto :goto_0

    .line 196
    :cond_9
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Legc;->an:Ljava/lang/CharSequence;

    .line 197
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 196
    invoke-static {v3, v0}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legc;->as:Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 554
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 531
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 532
    packed-switch v0, :pswitch_data_0

    .line 544
    :goto_0
    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {p0}, Legc;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "query"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 546
    :cond_0
    iget-boolean v2, p0, Legc;->ao:Z

    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    or-int/2addr v0, v2

    iput-boolean v0, p0, Legc;->ao:Z

    .line 549
    :cond_1
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Legc;->a(Landroid/view/View;Z)V

    .line 550
    return-void

    .line 534
    :pswitch_0
    iget-object v2, p0, Legc;->ah:Lfbs;

    invoke-virtual {v2, p2}, Lfbs;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 539
    :pswitch_1
    iget-object v2, p0, Legc;->ah:Lfbs;

    invoke-virtual {v2, p2}, Lfbs;->b(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 546
    goto :goto_1

    .line 532
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 71
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Legc;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Z)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 744
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 745
    iget-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 746
    iget-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, p1, v6}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 748
    :cond_0
    iget-object v0, p0, Legc;->at:Llnl;

    iget-object v1, p0, Legc;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 749
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Legc;->al:I

    iget-object v5, p0, Legc;->as:Ljava/lang/String;

    .line 748
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    .line 750
    if-eqz p2, :cond_1

    .line 752
    iget-object v0, p0, Legc;->ah:Lfbs;

    invoke-virtual {v0}, Lfbs;->i()Z

    move-result v0

    .line 753
    iget-object v1, p0, Legc;->ah:Lfbs;

    invoke-virtual {v1, v6}, Lfbs;->k(Z)V

    .line 754
    iget-object v1, p0, Legc;->ah:Lfbs;

    invoke-virtual {v1, v4}, Lfbs;->a(Landroid/database/Cursor;)V

    .line 755
    iget-object v1, p0, Legc;->ah:Lfbs;

    invoke-virtual {v1, v4}, Lfbs;->b(Landroid/database/Cursor;)V

    .line 756
    iget-object v1, p0, Legc;->ah:Lfbs;

    invoke-virtual {v1, v0}, Lfbs;->k(Z)V

    .line 757
    iput-boolean v6, p0, Legc;->ao:Z

    .line 760
    :cond_1
    return-void
.end method

.method public a(Ljcl;)V
    .locals 4

    .prologue
    .line 426
    instance-of v0, p1, Ljuc;

    if-nez v0, :cond_0

    .line 427
    invoke-super {p0, p1}, Legi;->a(Ljcl;)V

    .line 453
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 431
    check-cast v0, Ljuc;

    .line 433
    invoke-virtual {v0}, Ljuc;->j()Ljava/lang/String;

    move-result-object v1

    .line 435
    if-eqz v1, :cond_1

    invoke-static {v1}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 436
    invoke-virtual {v0}, Ljuc;->f()Lizu;

    move-result-object v0

    .line 437
    iget-object v1, p0, Legc;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 438
    new-instance v2, Ldew;

    iget-object v3, p0, Legc;->at:Llnl;

    invoke-direct {v2, v3, v1}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 440
    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v1

    .line 441
    invoke-virtual {v1, v0}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    .line 442
    invoke-static {}, Ljvj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->Y:Lctq;

    .line 443
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->X:Lctz;

    .line 444
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->Y:Lctq;

    .line 445
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    const/4 v1, 0x2

    .line 446
    invoke-virtual {v0, v1}, Ldew;->c(I)Ldew;

    move-result-object v0

    iget-boolean v1, p0, Legc;->ad:Z

    .line 447
    invoke-virtual {v0, v1}, Ldew;->g(Z)Ldew;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    .line 449
    invoke-virtual {p0, v0}, Legc;->b(Landroid/content/Intent;)V

    goto :goto_0

    .line 451
    :cond_1
    invoke-super {p0, p1}, Legi;->a(Ljcl;)V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 347
    invoke-super {p0, p1}, Legi;->a(Loo;)V

    .line 349
    iget-boolean v0, p0, Legc;->aq:Z

    if-nez v0, :cond_2

    .line 350
    new-instance v0, Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1}, Loo;->i()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 351
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 353
    iget-object v1, p0, Legc;->an:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 354
    iget-object v1, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 357
    :cond_0
    invoke-virtual {p0}, Legc;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0816

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;)V

    .line 359
    invoke-virtual {v0, p0}, Landroid/support/v7/widget/SearchView;->a(Lze;)V

    .line 360
    iget-object v1, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 362
    iget-boolean v1, p0, Legc;->aq:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Legc;->am:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->requestFocus()Z

    .line 366
    :cond_1
    iput-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    .line 368
    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 369
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 370
    invoke-virtual {p1, v3}, Loo;->d(Z)V

    .line 372
    :cond_2
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 342
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 719
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 720
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 721
    iget-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 724
    iget-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 725
    iget-object v0, p0, Legc;->ap:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 728
    :cond_0
    iput-object p1, p0, Legc;->an:Ljava/lang/CharSequence;

    .line 729
    iget-object v0, p0, Legc;->ah:Lfbs;

    iget-object v1, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfbs;->a(Ljava/lang/String;)V

    .line 731
    iget v0, p0, Legc;->al:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    sget-object v0, Lhmv;->eu:Lhmv;

    move-object v1, v0

    .line 734
    :goto_0
    iget-object v0, p0, Legc;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Legc;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 735
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 734
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 737
    invoke-virtual {p0, p1, v4}, Legc;->a(Ljava/lang/CharSequence;Z)V

    .line 738
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Legc;->a(Landroid/view/View;Z)V

    .line 740
    :cond_1
    return v4

    .line 731
    :cond_2
    sget-object v0, Lhmv;->ev:Lhmv;

    move-object v1, v0

    goto :goto_0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 264
    invoke-super {p0}, Legi;->aO_()V

    .line 265
    iget-object v0, p0, Legc;->az:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 267
    iget-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 269
    invoke-virtual {p0}, Legc;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Legc;->ay:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 278
    :cond_0
    :goto_0
    iget-object v0, p0, Legc;->am:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 279
    iget-object v0, p0, Legc;->am:Ljava/lang/CharSequence;

    iput-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    .line 280
    const/4 v0, 0x0

    iput-object v0, p0, Legc;->am:Ljava/lang/CharSequence;

    .line 281
    iget-object v0, p0, Legc;->ah:Lfbs;

    iget-object v1, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfbs;->a(Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Legc;->a(Ljava/lang/CharSequence;Z)V

    .line 283
    iget-object v0, p0, Legc;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 286
    :cond_1
    iget-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Legc;->aq:Z

    if-eqz v0, :cond_3

    .line 287
    :cond_2
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100319

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 288
    if-eqz v0, :cond_3

    .line 289
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 290
    new-instance v1, Legf;

    invoke-direct {v1, v0}, Legf;-><init>(Landroid/view/View;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 298
    :cond_3
    return-void

    .line 273
    :cond_4
    iget-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 274
    iget-object v1, p0, Legc;->ai:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Legc;->a(ILfib;)V

    goto :goto_0
.end method

.method protected b(Lhjk;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 385
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 387
    invoke-direct {p0}, Legc;->ac()V

    .line 389
    iget-boolean v0, p0, Legc;->aq:Z

    if-eqz v0, :cond_1

    .line 390
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 391
    invoke-virtual {p0}, Legc;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_0

    const-string v1, "is_videos_destination"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    const v0, 0x7f0a0a7a

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 395
    :cond_0
    invoke-virtual {p0, p1, v2}, Legc;->a(Lhjk;I)V

    .line 398
    :cond_1
    const v0, 0x7f10067b

    .line 399
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 400
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 402
    invoke-virtual {p0}, Legc;->U()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Legc;->ao:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Legc;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 403
    const v0, 0x7f100696

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 405
    :cond_2
    return-void
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 376
    invoke-super {p0, p1}, Legi;->b(Loo;)V

    .line 378
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 379
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 380
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 381
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 710
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    .line 711
    iget-object v0, p0, Legc;->ah:Lfbs;

    if-eqz v0, :cond_0

    .line 712
    iget-object v1, p0, Legc;->ah:Lfbs;

    iget-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Lfbs;->a(Ljava/lang/String;)V

    .line 714
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 710
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 712
    :cond_2
    iget-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 150
    invoke-super {p0, p1}, Legi;->c(Landroid/os/Bundle;)V

    .line 151
    return-void
.end method

.method public c(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 565
    invoke-super {p0, p1}, Legi;->c(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 580
    :goto_0
    return v0

    .line 569
    :cond_0
    iget-boolean v2, p0, Legc;->ax:Z

    if-eqz v2, :cond_1

    .line 570
    invoke-virtual {p0}, Legc;->w()Lbb;

    move-result-object v2

    invoke-virtual {v2, v0, v3, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    :goto_1
    move v0, v1

    .line 580
    goto :goto_0

    .line 573
    :cond_1
    iget-boolean v2, p0, Legc;->ar:Z

    if-eqz v2, :cond_2

    .line 574
    invoke-virtual {p0}, Legc;->w()Lbb;

    move-result-object v2

    invoke-virtual {v2, v0, v3, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 577
    :cond_2
    invoke-virtual {p0}, Legc;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v1, v3, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_1
.end method

.method public c(Landroid/view/View;)Z
    .locals 8

    .prologue
    const v3, 0x7f100093

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 457
    if-eqz p1, :cond_0

    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 498
    :goto_0
    return v0

    .line 461
    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v4

    move-object v0, p1

    .line 464
    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 465
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v0

    .line 466
    invoke-virtual {p0, v0}, Legc;->a(Lizu;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    .line 467
    goto :goto_0

    .line 470
    :cond_2
    iget-object v3, p0, Legc;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 471
    const-wide/32 v6, 0x40000

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    .line 472
    new-instance v1, Ldew;

    invoke-virtual {p0}, Legc;->n()Lz;

    move-result-object v4

    invoke-direct {v1, v4, v3}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 473
    invoke-virtual {v1, v0}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    .line 474
    invoke-static {}, Ljvj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->X:Lctz;

    .line 475
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->Y:Lctq;

    .line 476
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->Y:Lctq;

    .line 477
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    const/4 v1, 0x2

    .line 478
    invoke-virtual {v0, v1}, Ldew;->c(I)Ldew;

    move-result-object v0

    .line 479
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 494
    :goto_1
    invoke-static {p1}, Llsn;->b(Landroid/view/View;)V

    .line 495
    iget-object v0, p0, Legc;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Legc;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->dZ:Lhmv;

    .line 496
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 495
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 497
    invoke-virtual {p0, v1}, Legc;->b(Landroid/content/Intent;)V

    move v0, v2

    .line 498
    goto/16 :goto_0

    .line 481
    :cond_3
    const v0, 0x7f100079

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 482
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v1

    .line 483
    goto/16 :goto_0

    .line 485
    :cond_4
    new-instance v1, Ldew;

    invoke-virtual {p0}, Legc;->n()Lz;

    move-result-object v4

    invoke-direct {v1, v4, v3}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 486
    invoke-virtual {v1, v0}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->as:Ljava/lang/String;

    .line 487
    invoke-virtual {v0, v1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->X:Lctz;

    .line 488
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->Y:Lctq;

    .line 489
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    iget-object v1, p0, Legc;->Y:Lctq;

    .line 490
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    .line 491
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method

.method public d(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 666
    iget v0, p0, Legc;->al:I

    if-eq p1, v0, :cond_0

    .line 667
    iput p1, p0, Legc;->al:I

    .line 668
    const/4 v0, 0x5

    iget v1, p0, Legc;->al:I

    invoke-static {v0, v1}, Ljvj;->a(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legc;->as:Ljava/lang/String;

    .line 670
    iget-object v0, p0, Legc;->ah:Lfbs;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Legc;->ah:Lfbs;

    iget-object v1, p0, Legc;->as:Ljava/lang/String;

    iget v2, p0, Legc;->al:I

    invoke-virtual {v0, v1, v2}, Lfbs;->a(Ljava/lang/String;I)V

    .line 672
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v1, p0, Legc;->ah:Lfbs;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 673
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h()V

    .line 674
    invoke-virtual {p0}, Legc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 677
    iget-object v0, p0, Legc;->ap:Ljava/util/HashSet;

    iget v1, p0, Legc;->al:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 678
    iget-object v0, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v3}, Legc;->a(Ljava/lang/CharSequence;Z)V

    .line 679
    invoke-virtual {p0}, Legc;->x()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Legc;->a(Landroid/view/View;Z)V

    .line 683
    :cond_0
    return-void
.end method

.method protected d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 804
    if-eqz p1, :cond_0

    .line 805
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 806
    iget-object v0, p0, Legc;->ak:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 808
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 314
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 316
    const-string v0, "search_mode"

    iget v1, p0, Legc;->al:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 317
    const-string v0, "results_present"

    iget-boolean v1, p0, Legc;->ao:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 318
    const-string v0, "is_first_request"

    iget-boolean v1, p0, Legc;->aw:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 320
    iget-object v0, p0, Legc;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 321
    const-string v0, "photo_search_request"

    iget-object v1, p0, Legc;->ai:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 324
    :cond_0
    const-string v0, "query"

    iget-object v1, p0, Legc;->an:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 325
    iget-object v0, p0, Legc;->am:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 326
    const-string v0, "delayed_query"

    iget-object v1, p0, Legc;->am:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 329
    :cond_1
    const-string v0, "seen_search_modes"

    iget-object v1, p0, Legc;->ap:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 330
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 302
    invoke-super {p0}, Legi;->g()V

    .line 303
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 304
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 308
    invoke-super {p0}, Legi;->h()V

    .line 309
    iget-object v0, p0, Legc;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 310
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 703
    if-eqz p2, :cond_0

    iget-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    if-ne p1, v0, :cond_0

    .line 704
    iget-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Legc;->b(Ljava/lang/String;)Z

    .line 706
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 503
    instance-of v1, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-eqz v1, :cond_0

    .line 504
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-virtual {p0, p1}, Legc;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    .line 505
    invoke-virtual {p0, v0}, Legc;->e(I)V

    .line 506
    const/4 v0, 0x1

    .line 508
    :cond_0
    return v0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 254
    invoke-super {p0}, Legi;->z()V

    .line 255
    iget-object v0, p0, Legc;->az:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 257
    iget-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Legc;->aj:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 260
    :cond_0
    return-void
.end method
