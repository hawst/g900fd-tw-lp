.class public final Lnim;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnja;

.field public b:[Lnil;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2375
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2378
    const/4 v0, 0x0

    iput-object v0, p0, Lnim;->a:Lnja;

    .line 2381
    sget-object v0, Lnil;->a:[Lnil;

    iput-object v0, p0, Lnim;->b:[Lnil;

    .line 2375
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2402
    .line 2403
    iget-object v0, p0, Lnim;->a:Lnja;

    if-eqz v0, :cond_2

    .line 2404
    const/4 v0, 0x1

    iget-object v2, p0, Lnim;->a:Lnja;

    .line 2405
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2407
    :goto_0
    iget-object v2, p0, Lnim;->b:[Lnil;

    if-eqz v2, :cond_1

    .line 2408
    iget-object v2, p0, Lnim;->b:[Lnil;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2409
    if-eqz v4, :cond_0

    .line 2410
    const/4 v5, 0x2

    .line 2411
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2408
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2415
    :cond_1
    iget-object v1, p0, Lnim;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2416
    iput v0, p0, Lnim;->ai:I

    .line 2417
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnim;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2425
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2426
    sparse-switch v0, :sswitch_data_0

    .line 2430
    iget-object v2, p0, Lnim;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2431
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnim;->ah:Ljava/util/List;

    .line 2434
    :cond_1
    iget-object v2, p0, Lnim;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2436
    :sswitch_0
    return-object p0

    .line 2441
    :sswitch_1
    iget-object v0, p0, Lnim;->a:Lnja;

    if-nez v0, :cond_2

    .line 2442
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnim;->a:Lnja;

    .line 2444
    :cond_2
    iget-object v0, p0, Lnim;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2448
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2449
    iget-object v0, p0, Lnim;->b:[Lnil;

    if-nez v0, :cond_4

    move v0, v1

    .line 2450
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnil;

    .line 2451
    iget-object v3, p0, Lnim;->b:[Lnil;

    if-eqz v3, :cond_3

    .line 2452
    iget-object v3, p0, Lnim;->b:[Lnil;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2454
    :cond_3
    iput-object v2, p0, Lnim;->b:[Lnil;

    .line 2455
    :goto_2
    iget-object v2, p0, Lnim;->b:[Lnil;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2456
    iget-object v2, p0, Lnim;->b:[Lnil;

    new-instance v3, Lnil;

    invoke-direct {v3}, Lnil;-><init>()V

    aput-object v3, v2, v0

    .line 2457
    iget-object v2, p0, Lnim;->b:[Lnil;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2458
    invoke-virtual {p1}, Loxn;->a()I

    .line 2455
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2449
    :cond_4
    iget-object v0, p0, Lnim;->b:[Lnil;

    array-length v0, v0

    goto :goto_1

    .line 2461
    :cond_5
    iget-object v2, p0, Lnim;->b:[Lnil;

    new-instance v3, Lnil;

    invoke-direct {v3}, Lnil;-><init>()V

    aput-object v3, v2, v0

    .line 2462
    iget-object v2, p0, Lnim;->b:[Lnil;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2426
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2386
    iget-object v0, p0, Lnim;->a:Lnja;

    if-eqz v0, :cond_0

    .line 2387
    const/4 v0, 0x1

    iget-object v1, p0, Lnim;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2389
    :cond_0
    iget-object v0, p0, Lnim;->b:[Lnil;

    if-eqz v0, :cond_2

    .line 2390
    iget-object v1, p0, Lnim;->b:[Lnil;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2391
    if-eqz v3, :cond_1

    .line 2392
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2390
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2396
    :cond_2
    iget-object v0, p0, Lnim;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2398
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2371
    invoke-virtual {p0, p1}, Lnim;->a(Loxn;)Lnim;

    move-result-object v0

    return-object v0
.end method
