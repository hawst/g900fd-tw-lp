.class public final Ljah;
.super Llol;
.source "PG"

# interfaces
.implements Lhob;
.implements Lkhf;


# instance fields
.field private N:[Ljava/lang/String;

.field private O:[Ljava/lang/String;

.field private P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private Q:Lkha;

.field private R:Lkha;

.field private S:Lkha;

.field private T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private V:Lkhl;

.field private W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private X:Lkha;

.field private Y:Lhoc;

.field private Z:Lkdv;

.field private aa:Ljgn;

.field private ab:Z

.field private ac:Lkhe;

.field private ad:Lkhr;


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 96
    invoke-direct {p0}, Llol;-><init>()V

    .line 68
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "L20"

    aput-object v1, v0, v3

    const-string v1, "L30"

    aput-object v1, v0, v4

    const-string v1, "L40"

    aput-object v1, v0, v5

    const-string v1, "L50"

    aput-object v1, v0, v6

    const-string v1, "L60"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "L70"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "L80"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "L90"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "WL20"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "WL30"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "WL40"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "WL50"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "WL60"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "WL70"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "WL80"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "WL90"

    aput-object v2, v0, v1

    iput-object v0, p0, Ljah;->N:[Ljava/lang/String;

    .line 72
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "high_speed"

    aput-object v1, v0, v3

    const-string v1, "standard_speed"

    aput-object v1, v0, v4

    const-string v1, "low_speed_network"

    aput-object v1, v0, v5

    const-string v1, "very_low_speed_network"

    aput-object v1, v0, v6

    iput-object v0, p0, Ljah;->O:[Ljava/lang/String;

    .line 87
    new-instance v0, Lhoc;

    iget-object v1, p0, Ljah;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 88
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Ljah;->Y:Lhoc;

    .line 93
    new-instance v0, Lkhe;

    iget-object v1, p0, Ljah;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    iput-object v0, p0, Ljah;->ac:Lkhe;

    .line 96
    return-void
.end method

.method static synthetic a(Ljah;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic a(Ljah;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Ljah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 285
    const-string v0, "L20"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    const v0, 0x7f0a03c2

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 324
    :goto_0
    return-object v0

    .line 287
    :cond_0
    const-string v0, "L30"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    const v0, 0x7f0a03c3

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 289
    :cond_1
    const-string v0, "L40"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    const v0, 0x7f0a03c4

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 291
    :cond_2
    const-string v0, "L50"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 292
    const v0, 0x7f0a03c5

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 293
    :cond_3
    const-string v0, "L60"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 294
    const v0, 0x7f0a03c6

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 295
    :cond_4
    const-string v0, "L70"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 296
    const v0, 0x7f0a03c7

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 297
    :cond_5
    const-string v0, "L80"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 298
    const v0, 0x7f0a03c8

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 299
    :cond_6
    const-string v0, "L90"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 300
    const v0, 0x7f0a03c9

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 301
    :cond_7
    const-string v0, "WL20"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 302
    const v0, 0x7f0a03ca

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 303
    :cond_8
    const-string v0, "WL30"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 304
    const v0, 0x7f0a03cb

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 305
    :cond_9
    const-string v0, "WL40"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 306
    const v0, 0x7f0a03cc

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 307
    :cond_a
    const-string v0, "WL50"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 308
    const v0, 0x7f0a03cd

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 309
    :cond_b
    const-string v0, "WL60"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 310
    const v0, 0x7f0a03ce

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 311
    :cond_c
    const-string v0, "WL70"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 312
    const v0, 0x7f0a03cf

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 313
    :cond_d
    const-string v0, "WL80"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 314
    const v0, 0x7f0a03d0

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 315
    :cond_e
    const-string v0, "WL90"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 316
    const v0, 0x7f0a03d1

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 317
    :cond_f
    const-string v0, "high_speed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 318
    const v0, 0x7f0a03d2

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 319
    :cond_10
    const-string v0, "standard_speed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 320
    const v0, 0x7f0a03d3

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 321
    :cond_11
    const-string v0, "low_speed_network"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 322
    const v0, 0x7f0a03d4

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 323
    :cond_12
    const-string v0, "very_low_speed_network"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 324
    const v0, 0x7f0a03d5

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 326
    :cond_13
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Value is not valid"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 337
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 339
    if-eqz v1, :cond_0

    .line 340
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 341
    aget-object v2, v1, v0

    invoke-direct {p0, v2}, Ljah;->a(Ljava/io/File;)V

    .line 340
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 347
    return-void
.end method

.method static synthetic b(Ljah;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljah;->d()V

    return-void
.end method

.method static synthetic c(Ljah;)Lkha;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->Q:Lkha;

    return-object v0
.end method

.method static synthetic d(Ljah;)Lkha;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->R:Lkha;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 279
    iget-object v3, p0, Ljah;->Q:Lkha;

    iget-object v0, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lkha;->b(Z)V

    .line 280
    iget-object v3, p0, Ljah;->R:Lkha;

    iget-object v0, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lkha;->b(Z)V

    .line 281
    iget-object v0, p0, Ljah;->S:Lkha;

    iget-object v3, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Lkha;->b(Z)V

    .line 282
    return-void

    :cond_0
    move v0, v2

    .line 279
    goto :goto_0

    :cond_1
    move v0, v2

    .line 280
    goto :goto_1

    :cond_2
    move v1, v2

    .line 281
    goto :goto_2
.end method

.method static synthetic e(Ljah;)Lkha;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->S:Lkha;

    return-object v0
.end method

.method static synthetic f(Ljah;)Llnl;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Ljah;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Ljah;->a(Ljava/io/File;)V

    return-void
.end method

.method static synthetic h(Ljah;)Lkdv;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->Z:Lkdv;

    return-object v0
.end method

.method static synthetic i(Ljah;)Lhoc;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->Y:Lhoc;

    return-object v0
.end method

.method static synthetic j(Ljah;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic k(Ljah;)Lkha;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljah;->X:Lkha;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const v7, 0x7f0a03bf

    const v5, 0x7f0f000a

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 113
    new-instance v0, Lkhr;

    iget-object v3, p0, Ljah;->at:Llnl;

    invoke-direct {v0, v3}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljah;->ad:Lkhr;

    .line 115
    iget-object v0, p0, Ljah;->ad:Lkhr;

    const v3, 0x7f0a03b7

    .line 116
    invoke-virtual {p0, v3}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 115
    invoke-virtual {v0, v3, v6}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 117
    iget-object v0, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v3, "automatic_quality_key"

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v3, p0, Ljah;->at:Llnl;

    .line 119
    invoke-static {v3}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "automatic_quality_key"

    .line 120
    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 118
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 121
    iget-object v0, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    new-instance v3, Ljai;

    invoke-direct {v3, p0}, Ljai;-><init>(Ljah;)V

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 130
    iget-object v0, p0, Ljah;->ac:Lkhe;

    iget-object v3, p0, Ljah;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v3}, Lkhe;->a(Lkhl;)Lkhl;

    .line 132
    iget-object v0, p0, Ljah;->ad:Lkhr;

    const v3, 0x7f0a03b8

    .line 133
    invoke-virtual {p0, v3}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 132
    invoke-virtual {v0, v3, v6}, Lkhr;->g(Ljava/lang/String;Ljava/lang/String;)Lkha;

    move-result-object v0

    iput-object v0, p0, Ljah;->Q:Lkha;

    .line 134
    iget-object v0, p0, Ljah;->Q:Lkha;

    const-string v3, "standard_quality_key"

    invoke-virtual {v0, v3}, Lkha;->d(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Ljah;->Q:Lkha;

    invoke-virtual {p0}, Ljah;->o()Landroid/content/res/Resources;

    move-result-object v3

    .line 136
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 135
    invoke-virtual {v0, v3}, Lkha;->a([Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Ljah;->Q:Lkha;

    iget-object v3, p0, Ljah;->N:[Ljava/lang/String;

    invoke-virtual {v0, v3}, Lkha;->b([Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Ljah;->at:Llnl;

    invoke-static {v0}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "standard_quality_key"

    const-string v4, "WL80"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    iget-object v3, p0, Ljah;->Q:Lkha;

    invoke-direct {p0, v0}, Ljah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lkha;->d(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Ljah;->Q:Lkha;

    const-string v3, "WL80"

    invoke-virtual {v0, v3}, Lkha;->a(Ljava/lang/Object;)V

    .line 142
    iget-object v0, p0, Ljah;->Q:Lkha;

    new-instance v3, Ljaj;

    invoke-direct {v3, p0}, Ljaj;-><init>(Ljah;)V

    invoke-virtual {v0, v3}, Lkha;->a(Lkhp;)V

    .line 151
    iget-object v0, p0, Ljah;->ac:Lkhe;

    iget-object v3, p0, Ljah;->Q:Lkha;

    invoke-virtual {v0, v3}, Lkhe;->a(Lkhl;)Lkhl;

    .line 153
    iget-object v0, p0, Ljah;->ad:Lkhr;

    const v3, 0x7f0a03b9

    .line 154
    invoke-virtual {p0, v3}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 153
    invoke-virtual {v0, v3, v6}, Lkhr;->g(Ljava/lang/String;Ljava/lang/String;)Lkha;

    move-result-object v0

    iput-object v0, p0, Ljah;->R:Lkha;

    .line 155
    iget-object v0, p0, Ljah;->R:Lkha;

    const-string v3, "low_quality_key"

    invoke-virtual {v0, v3}, Lkha;->d(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Ljah;->R:Lkha;

    invoke-virtual {p0}, Ljah;->o()Landroid/content/res/Resources;

    move-result-object v3

    .line 157
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 156
    invoke-virtual {v0, v3}, Lkha;->a([Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Ljah;->R:Lkha;

    iget-object v3, p0, Ljah;->N:[Ljava/lang/String;

    invoke-virtual {v0, v3}, Lkha;->b([Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Ljah;->at:Llnl;

    invoke-static {v0}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "low_quality_key"

    const-string v4, "WL60"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    iget-object v3, p0, Ljah;->R:Lkha;

    invoke-direct {p0, v0}, Ljah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lkha;->d(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Ljah;->R:Lkha;

    const-string v3, "WL60"

    invoke-virtual {v0, v3}, Lkha;->a(Ljava/lang/Object;)V

    .line 163
    iget-object v0, p0, Ljah;->R:Lkha;

    new-instance v3, Ljak;

    invoke-direct {v3, p0}, Ljak;-><init>(Ljah;)V

    invoke-virtual {v0, v3}, Lkha;->a(Lkhp;)V

    .line 171
    iget-object v0, p0, Ljah;->ac:Lkhe;

    iget-object v3, p0, Ljah;->R:Lkha;

    invoke-virtual {v0, v3}, Lkhe;->a(Lkhl;)Lkhl;

    .line 173
    iget-object v0, p0, Ljah;->ad:Lkhr;

    const v3, 0x7f0a03ba

    .line 174
    invoke-virtual {p0, v3}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-virtual {v0, v3, v6}, Lkhr;->g(Ljava/lang/String;Ljava/lang/String;)Lkha;

    move-result-object v0

    iput-object v0, p0, Ljah;->S:Lkha;

    .line 175
    iget-object v0, p0, Ljah;->S:Lkha;

    const-string v3, "very_low_quality_key"

    invoke-virtual {v0, v3}, Lkha;->d(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Ljah;->S:Lkha;

    invoke-virtual {p0}, Ljah;->o()Landroid/content/res/Resources;

    move-result-object v3

    .line 177
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 176
    invoke-virtual {v0, v3}, Lkha;->a([Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Ljah;->S:Lkha;

    iget-object v3, p0, Ljah;->N:[Ljava/lang/String;

    invoke-virtual {v0, v3}, Lkha;->b([Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Ljah;->at:Llnl;

    invoke-static {v0}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "very_low_quality_key"

    const-string v4, "WL40"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    iget-object v3, p0, Ljah;->S:Lkha;

    invoke-direct {p0, v0}, Ljah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lkha;->d(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v0, p0, Ljah;->S:Lkha;

    const-string v3, "WL40"

    invoke-virtual {v0, v3}, Lkha;->a(Ljava/lang/Object;)V

    .line 183
    iget-object v0, p0, Ljah;->S:Lkha;

    new-instance v3, Ljal;

    invoke-direct {v3, p0}, Ljal;-><init>(Ljah;)V

    invoke-virtual {v0, v3}, Lkha;->a(Lkhp;)V

    .line 192
    iget-object v0, p0, Ljah;->ac:Lkhe;

    iget-object v3, p0, Ljah;->S:Lkha;

    invoke-virtual {v0, v3}, Lkhe;->a(Lkhl;)Lkhl;

    .line 194
    invoke-direct {p0}, Ljah;->d()V

    .line 196
    iget-object v0, p0, Ljah;->ad:Lkhr;

    const v3, 0x7f0a03bc

    .line 197
    invoke-virtual {p0, v3}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 196
    invoke-virtual {v0, v3, v6}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Ljah;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 198
    iget-object v0, p0, Ljah;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v3, "overlay_key"

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Ljah;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v3, p0, Ljah;->at:Llnl;

    .line 200
    invoke-static {v3}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "overlay_key"

    .line 201
    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 199
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 202
    iget-object v0, p0, Ljah;->ac:Lkhe;

    iget-object v3, p0, Ljah;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v3}, Lkhe;->a(Lkhl;)Lkhl;

    .line 204
    iget-object v0, p0, Ljah;->ad:Lkhr;

    const v3, 0x7f0a03bd

    .line 205
    invoke-virtual {p0, v3}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 204
    invoke-virtual {v0, v3, v6}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    iput-object v0, p0, Ljah;->V:Lkhl;

    .line 206
    iget-object v0, p0, Ljah;->V:Lkhl;

    new-instance v3, Ljam;

    invoke-direct {v3, p0}, Ljam;-><init>(Ljah;)V

    invoke-virtual {v0, v3}, Lkhl;->a(Lkhq;)V

    .line 220
    iget-object v0, p0, Ljah;->ac:Lkhe;

    iget-object v3, p0, Ljah;->V:Lkhl;

    invoke-virtual {v0, v3}, Lkhe;->a(Lkhl;)Lkhl;

    .line 222
    iget-boolean v0, p0, Ljah;->ab:Z

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Ljah;->ad:Lkhr;

    .line 224
    invoke-virtual {p0, v7}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 223
    invoke-virtual {v0, v3}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v3

    .line 226
    iget-object v0, p0, Ljah;->ad:Lkhr;

    const v4, 0x7f0a03c0

    .line 227
    invoke-virtual {p0, v4}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 226
    invoke-virtual {v0, v4, v6}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Ljah;->W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 228
    iget-object v0, p0, Ljah;->W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v4, "automatic_network_key"

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Ljah;->W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v4, p0, Ljah;->at:Llnl;

    .line 230
    invoke-static {v4}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "automatic_network_key"

    .line 231
    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 229
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 232
    const v0, 0x7f0a03c1

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Ljah;->aa:Ljgn;

    .line 233
    invoke-interface {v0}, Ljgn;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0a03d6

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 234
    :goto_1
    iget-object v4, p0, Ljah;->W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v0, p0, Ljah;->W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    new-instance v4, Ljao;

    invoke-direct {v4, p0}, Ljao;-><init>(Ljah;)V

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 244
    iget-object v0, p0, Ljah;->W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 246
    iget-object v0, p0, Ljah;->ad:Lkhr;

    .line 247
    invoke-virtual {p0, v7}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 246
    invoke-virtual {v0, v4, v6}, Lkhr;->g(Ljava/lang/String;Ljava/lang/String;)Lkha;

    move-result-object v0

    iput-object v0, p0, Ljah;->X:Lkha;

    .line 248
    iget-object v0, p0, Ljah;->X:Lkha;

    const-string v4, "network_classification_key"

    invoke-virtual {v0, v4}, Lkha;->d(Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Ljah;->X:Lkha;

    invoke-virtual {p0}, Ljah;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f000b

    .line 250
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 249
    invoke-virtual {v0, v4}, Lkha;->a([Ljava/lang/CharSequence;)V

    .line 251
    iget-object v0, p0, Ljah;->X:Lkha;

    iget-object v4, p0, Ljah;->O:[Ljava/lang/String;

    invoke-virtual {v0, v4}, Lkha;->b([Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Ljah;->at:Llnl;

    invoke-static {v0}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v4, "network_classification_key"

    const-string v5, "standard_speed"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 254
    iget-object v4, p0, Ljah;->X:Lkha;

    invoke-direct {p0, v0}, Ljah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lkha;->d(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v0, p0, Ljah;->X:Lkha;

    const-string v4, "standard_speed"

    invoke-virtual {v0, v4}, Lkha;->a(Ljava/lang/Object;)V

    .line 256
    iget-object v0, p0, Ljah;->X:Lkha;

    new-instance v4, Ljap;

    invoke-direct {v4, p0}, Ljap;-><init>(Ljah;)V

    invoke-virtual {v0, v4}, Lkha;->a(Lkhp;)V

    .line 265
    iget-object v4, p0, Ljah;->X:Lkha;

    iget-object v0, p0, Ljah;->W:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Lkha;->b(Z)V

    .line 266
    iget-object v0, p0, Ljah;->X:Lkha;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 276
    :goto_3
    return-void

    .line 233
    :pswitch_0
    const v0, 0x7f0a03d6

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1
    const v0, 0x7f0a03d2

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_2
    const v0, 0x7f0a03d3

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_3
    const v0, 0x7f0a03d4

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_4
    const v0, 0x7f0a03d5

    invoke-virtual {p0, v0}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1
    move v0, v2

    .line 265
    goto :goto_2

    .line 268
    :cond_2
    iget-object v0, p0, Ljah;->ad:Lkhr;

    const v1, 0x7f0a03bb

    .line 269
    invoke-virtual {p0, v1}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 268
    invoke-virtual {v0, v1, v6}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Ljah;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 270
    iget-object v0, p0, Ljah;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v1, "metered_connection_key"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Ljah;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Ljah;->at:Llnl;

    .line 272
    invoke-static {v1}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v3, "metered_connection_key"

    .line 273
    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 271
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 274
    iget-object v0, p0, Ljah;->ac:Lkhe;

    iget-object v1, p0, Ljah;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_3

    .line 233
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 352
    iget-object v0, p0, Ljah;->at:Llnl;

    const v1, 0x7f0a03be

    .line 353
    invoke-virtual {p0, v1}, Ljah;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 352
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 354
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 355
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 101
    iget-object v0, p0, Ljah;->au:Llnh;

    const-class v1, Lkdv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    iput-object v0, p0, Ljah;->Z:Lkdv;

    .line 103
    iget-object v0, p0, Ljah;->au:Llnh;

    const-class v1, Ljgp;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgp;

    .line 104
    invoke-interface {v0}, Ljgp;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljah;->ab:Z

    .line 108
    :cond_0
    iget-object v0, p0, Ljah;->au:Llnh;

    const-class v1, Ljgn;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    iput-object v0, p0, Ljah;->aa:Ljgn;

    .line 109
    return-void
.end method
