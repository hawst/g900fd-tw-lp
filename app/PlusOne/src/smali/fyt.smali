.class public final Lfyt;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lhmm;
.implements Lljh;


# instance fields
.field public a:Lfyv;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;


# direct methods
.method static synthetic a(Lfyt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfyt;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lfyt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfyt;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lfyt;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lfyt;->e:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lfyt;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 111
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfyt;->d:Ljava/lang/String;

    .line 73
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfyt;->f:Ljava/lang/String;

    .line 74
    invoke-virtual {p0}, Lfyt;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lhee;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x3

    .line 75
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lfyt;->e:Z

    .line 77
    iget-object v0, p0, Lfyt;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lfyt;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 80
    if-eqz v0, :cond_0

    .line 81
    invoke-static {v0}, Lllq;->b([B)Ljava/util/List;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lfyt;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lfyt;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v0, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 88
    :cond_0
    new-instance v0, Lhmi;

    new-instance v1, Lfyu;

    invoke-direct {v1, p0}, Lfyu;-><init>(Lfyt;)V

    invoke-direct {v0, v1}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lfyt;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    return-void

    :cond_1
    move v0, v2

    .line 75
    goto :goto_0
.end method

.method public a(Lfyv;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lfyt;->a:Lfyv;

    .line 100
    return-void
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 104
    new-instance v0, Lkqv;

    sget-object v1, Lomu;->a:Lhmn;

    iget-object v2, p0, Lfyt;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkqv;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 63
    const v0, 0x7f100118

    invoke-virtual {p0, v0}, Lfyt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfyt;->b:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f10002e

    invoke-virtual {p0, v0}, Lfyt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lfyt;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 65
    iget-object v0, p0, Lfyt;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Z)V

    .line 66
    iget-object v1, p0, Lfyt;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lfyt;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lgcs;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcs;

    invoke-interface {v0}, Lgcs;->e()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(I)V

    .line 68
    invoke-virtual {p0, v3, v3, v3, v3}, Lfyt;->setPadding(IIII)V

    .line 69
    return-void
.end method
