.class public Lknz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlv;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkob;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lknz;->a:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lknz;->c:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lknz;->b:Ljava/util/List;

    .line 55
    return-void
.end method

.method public static a(Ljava/lang/String;)Lknz;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lknz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lknz;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lknz;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lknz;

    invoke-direct {v0, p0, p1}, Lknz;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lknz;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lhlx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lknz;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a(J)Lknz;
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lknz;->b:Ljava/util/List;

    new-instance v1, Lkob;

    sget-object v2, Lkoc;->a:Lkoc;

    invoke-direct {v1, v2, p1, p2}, Lkob;-><init>(Lkoc;J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    return-object p0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 106
    const-class v0, Lhlw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    invoke-interface {v0, p1, p0}, Lhlw;->a(Landroid/content/Context;Lhlv;)V

    .line 107
    return-void
.end method

.method public a(Lhmp;)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public b()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkob;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lknz;->b:Ljava/util/List;

    return-object v0
.end method

.method public b(J)Lknz;
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lknz;->b:Ljava/util/List;

    new-instance v1, Lkob;

    sget-object v2, Lkoc;->c:Lkoc;

    invoke-direct {v1, v2, p1, p2}, Lkob;-><init>(Lkoc;J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lknz;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lknz;->b:Ljava/util/List;

    new-instance v1, Lkob;

    sget-object v2, Lkoc;->a:Lkoc;

    invoke-direct {v1, v2, p1}, Lkob;-><init>(Lkoc;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lknz;
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lknz;->b:Ljava/util/List;

    new-instance v1, Lkob;

    sget-object v2, Lkoc;->b:Lkoc;

    invoke-direct {v1, v2, p1}, Lkob;-><init>(Lkoc;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lknz;
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lknz;->b:Ljava/util/List;

    new-instance v1, Lkob;

    sget-object v2, Lkoc;->d:Lkoc;

    invoke-direct {v1, v2, p1}, Lkob;-><init>(Lkoc;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    const-string v0, "ExternalPhotoShareEvent"

    return-object v0
.end method
