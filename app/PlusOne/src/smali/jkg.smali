.class public final Ljkg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, "com.google.android.libraries.social.notifications.SET_READ_STATES"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 21
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v0

    .line 22
    if-nez v0, :cond_0

    .line 38
    :goto_0
    return-void

    .line 26
    :cond_0
    const-string v1, "notification_key_list"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 29
    const-string v2, "new_read_state"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 32
    if-nez v2, :cond_1

    .line 33
    const-string v0, "NotsSetReadStateHandler"

    const-string v1, "Unknown read state."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 37
    :cond_1
    invoke-static {p2, v0, v1, v2, p0}, Ljkh;->a(Landroid/content/Context;Lkfo;[Ljava/lang/String;ILjjp;)V

    goto :goto_0
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Ljkg;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
