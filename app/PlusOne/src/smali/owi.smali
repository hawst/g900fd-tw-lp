.class public final Lowi;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 595
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 621
    const/4 v0, 0x0

    .line 622
    iget-object v1, p0, Lowi;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 623
    const/4 v0, 0x1

    iget-object v1, p0, Lowi;->a:Ljava/lang/Long;

    .line 624
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 626
    :cond_0
    iget-object v1, p0, Lowi;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 627
    const/4 v1, 0x2

    iget-object v2, p0, Lowi;->b:Ljava/lang/Long;

    .line 628
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 630
    :cond_1
    iget-object v1, p0, Lowi;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 631
    const/4 v1, 0x3

    iget-object v2, p0, Lowi;->c:Ljava/lang/Long;

    .line 632
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 634
    :cond_2
    iget-object v1, p0, Lowi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 635
    iput v0, p0, Lowi;->ai:I

    .line 636
    return v0
.end method

.method public a(Loxn;)Lowi;
    .locals 2

    .prologue
    .line 644
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 645
    sparse-switch v0, :sswitch_data_0

    .line 649
    iget-object v1, p0, Lowi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 650
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lowi;->ah:Ljava/util/List;

    .line 653
    :cond_1
    iget-object v1, p0, Lowi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 655
    :sswitch_0
    return-object p0

    .line 660
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lowi;->a:Ljava/lang/Long;

    goto :goto_0

    .line 664
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lowi;->b:Ljava/lang/Long;

    goto :goto_0

    .line 668
    :sswitch_3
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lowi;->c:Ljava/lang/Long;

    goto :goto_0

    .line 645
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 606
    iget-object v0, p0, Lowi;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 607
    const/4 v0, 0x1

    iget-object v1, p0, Lowi;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 609
    :cond_0
    iget-object v0, p0, Lowi;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 610
    const/4 v0, 0x2

    iget-object v1, p0, Lowi;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 612
    :cond_1
    iget-object v0, p0, Lowi;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 613
    const/4 v0, 0x3

    iget-object v1, p0, Lowi;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 615
    :cond_2
    iget-object v0, p0, Lowi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 617
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 591
    invoke-virtual {p0, p1}, Lowi;->a(Loxn;)Lowi;

    move-result-object v0

    return-object v0
.end method
