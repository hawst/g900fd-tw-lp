.class public final Lkuu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;
.implements Lkuz;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Lu;

.field private b:Landroid/content/Context;

.field private c:Lhee;


# direct methods
.method public constructor <init>(Lu;Llqr;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lkuu;->a:Lu;

    .line 42
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 43
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 47
    iput-object p1, p0, Lkuu;->b:Landroid/content/Context;

    .line 48
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkuu;->c:Lhee;

    .line 49
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 50
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 79
    const-string v0, "EditSquareMembershipTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 81
    :goto_0
    const-string v1, "SOLE_OWNER_LEAVING_SQUARE"

    invoke-static {v0, v1}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    const v0, 0x7f0a04e0

    const v1, 0x7f0a024c

    invoke-virtual {p3, v0, v1}, Lhos;->a(II)V

    .line 86
    :cond_0
    return-void

    .line 80
    :cond_1
    invoke-virtual {p2}, Lhoz;->b()Ljava/lang/Exception;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 63
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    move-object v1, v0

    .line 64
    :goto_0
    if-eqz v1, :cond_1

    .line 66
    iget-object v0, p0, Lkuu;->b:Landroid/content/Context;

    const-class v2, Lhms;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lkuu;->b:Landroid/content/Context;

    iget-object v4, p0, Lkuu;->c:Lhee;

    .line 67
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 66
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 70
    :cond_1
    new-instance v1, Lkvp;

    iget-object v0, p0, Lkuu;->b:Landroid/content/Context;

    iget-object v2, p0, Lkuu;->c:Lhee;

    .line 71
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v1, v0, v2, p1, p3}, Lkvp;-><init>(Landroid/content/Context;ILjava/lang/String;I)V

    .line 72
    invoke-virtual {v1, p2}, Lkvp;->a(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lkuu;->b:Landroid/content/Context;

    const-class v2, Lhoc;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, v1}, Lhoc;->c(Lhny;)V

    .line 74
    return-void

    .line 63
    :pswitch_1
    sget-object v0, Lhmv;->cl:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_2
    sget-object v0, Lhmv;->cm:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_3
    sget-object v0, Lhmv;->co:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_4
    sget-object v0, Lhmv;->cq:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_5
    sget-object v0, Lhmv;->cn:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_6
    sget-object v0, Lhmv;->ci:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_7
    sget-object v0, Lhmv;->cj:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_8
    sget-object v0, Lhmv;->ck:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_9
    sget-object v0, Lhmv;->cc:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_a
    sget-object v0, Lhmv;->cg:Lhmv;

    move-object v1, v0

    goto :goto_0

    :pswitch_b
    sget-object v0, Lhmv;->cp:Lhmv;

    move-object v1, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 3

    .prologue
    .line 54
    invoke-static {p5}, Lkto;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-static {p4, p1, p2, p3, p5}, Lkuy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lt;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lkuu;->a:Lu;

    invoke-virtual {v1}, Lu;->q()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 59
    :cond_0
    return-void
.end method
