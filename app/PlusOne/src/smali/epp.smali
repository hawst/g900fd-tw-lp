.class public abstract Lepp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lepm;
.implements Llrb;
.implements Llrg;


# instance fields
.field public b:I

.field public c:Landroid/content/Context;

.field private d:Lept;

.field private e:Z

.field private f:Landroid/view/View;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lepp;->g:Z

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lepp;->e:Z

    .line 53
    iput-object p1, p0, Lepp;->c:Landroid/content/Context;

    .line 54
    iput p2, p0, Lepp;->b:I

    .line 55
    iput-object p3, p0, Lepp;->d:Lept;

    .line 56
    invoke-interface {p3}, Lept;->z_()Llqr;

    move-result-object v0

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Promo host must provide a non-null lifecycle."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    iget-object v0, p0, Lepp;->d:Lept;

    invoke-interface {v0}, Lept;->z_()Llqr;

    move-result-object v0

    invoke-virtual {v0, p0}, Llqr;->a(Llrg;)Llrg;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILept;Z)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lepp;->g:Z

    .line 63
    iput-boolean p4, p0, Lepp;->e:Z

    .line 64
    iput-object p1, p0, Lepp;->c:Landroid/content/Context;

    .line 65
    iput p2, p0, Lepp;->b:I

    .line 66
    iput-object p3, p0, Lepp;->d:Lept;

    .line 67
    return-void
.end method

.method protected static a(Landroid/content/Context;ILepn;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 160
    sget-object v0, Leps;->a:[I

    add-int/lit8 v1, p3, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 174
    :goto_0
    return-void

    .line 162
    :pswitch_0
    invoke-static {p0, p1, p2}, Ldhv;->b(Landroid/content/Context;ILepn;)V

    goto :goto_0

    .line 166
    :pswitch_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 167
    new-instance v1, Ldwi;

    const-string v2, ""

    invoke-interface {v0, p4, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ldwi;-><init>(Ljava/lang/String;)V

    .line 168
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 169
    invoke-virtual {v1}, Ldwi;->c()Ldwi;

    .line 170
    invoke-virtual {v1}, Ldwi;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 171
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract a(Landroid/view/View;)V
.end method

.method protected a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method

.method public varargs a(Landroid/view/View;I[Ljava/lang/Object;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 77
    const v0, 0x7f1001b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    iget-object v1, p0, Lepp;->c:Landroid/content/Context;

    invoke-virtual {v1, p2, p3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    .line 81
    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v1, v4, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    invoke-virtual {v2}, [Landroid/text/style/URLSpan;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    .line 82
    array-length v5, v2

    move v3, v4

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v2, v3

    .line 83
    invoke-interface {v1, v6}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 84
    invoke-interface {v1, v6}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 85
    invoke-interface {v1, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 86
    new-instance v9, Lepq;

    invoke-virtual {v6}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v9, p0, v6}, Lepq;-><init>(Lepp;Ljava/lang/String;)V

    .line 93
    invoke-interface {v1, v9, v7, v8, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 82
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 98
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 237
    if-eqz p2, :cond_0

    .line 238
    const v0, 0x7f1001ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 239
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 240
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    :cond_0
    return-void
.end method

.method public a(Lfhh;)V
    .locals 1

    .prologue
    .line 271
    new-instance v0, Lepr;

    invoke-direct {v0, p1}, Lepr;-><init>(Lfhh;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 276
    return-void
.end method

.method public a(Lnyq;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 179
    invoke-virtual {p0}, Lepp;->l()I

    move-result v2

    .line 181
    if-ne v2, v1, :cond_0

    iget v3, p0, Lepp;->b:I

    if-eq v3, v4, :cond_1

    :cond_0
    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    iget v3, p0, Lepp;->b:I

    if-eq v3, v4, :cond_2

    .line 203
    :cond_1
    :goto_0
    return v0

    .line 188
    :cond_2
    sget-object v3, Leps;->a:[I

    add-int/lit8 v2, v2, -0x1

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 199
    const/4 v2, 0x0

    .line 203
    :goto_1
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ldwi;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 190
    :pswitch_0
    iget-object v2, p0, Lepp;->c:Landroid/content/Context;

    iget v3, p0, Lepp;->b:I

    invoke-virtual {p0}, Lepp;->e()Lepn;

    move-result-object v4

    invoke-static {v2, v3, v4}, Ldhv;->a(Landroid/content/Context;ILepn;)Ldwi;

    move-result-object v2

    goto :goto_1

    .line 194
    :pswitch_1
    iget-object v2, p0, Lepp;->c:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 195
    new-instance v2, Ldwi;

    invoke-virtual {p0}, Lepp;->m()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ldwi;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 188
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Landroid/view/View;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 249
    if-eqz p2, :cond_0

    .line 250
    const v0, 0x7f1001ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 251
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 252
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 318
    iget-boolean v0, p0, Lepp;->g:Z

    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p0}, Lepp;->k()V

    .line 321
    :cond_0
    return-void
.end method

.method public f()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 117
    iget-object v0, p0, Lepp;->c:Landroid/content/Context;

    iget v1, p0, Lepp;->b:I

    invoke-virtual {p0}, Lepp;->e()Lepn;

    move-result-object v2

    invoke-virtual {p0}, Lepp;->l()I

    move-result v3

    invoke-virtual {p0}, Lepp;->m()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lepp;->a(Landroid/content/Context;ILepn;ILjava/lang/String;)V

    .line 119
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    invoke-direct {v0, v5, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 120
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 121
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1, v5}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 122
    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 123
    iget-object v1, p0, Lepp;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lepp;->g:Z

    .line 125
    return-void
.end method

.method public i()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 208
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 209
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 210
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 211
    iget-object v1, p0, Lepp;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 212
    return-void
.end method

.method public j()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 216
    iget-object v0, p0, Lepp;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lepp;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 218
    iget-boolean v0, p0, Lepp;->e:Z

    if-eqz v0, :cond_3

    .line 219
    const v0, 0x7f040218

    invoke-virtual {v1, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepp;->f:Landroid/view/View;

    .line 220
    iget-object v0, p0, Lepp;->f:Landroid/view/View;

    const v2, 0x7f1001ba

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 221
    invoke-virtual {p0}, Lepp;->d()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 226
    :cond_0
    :goto_0
    iget-object v0, p0, Lepp;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Lepp;->a(Landroid/view/View;)V

    .line 227
    iget-object v0, p0, Lepp;->f:Landroid/view/View;

    const v1, 0x7f1001ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object v0, p0, Lepp;->f:Landroid/view/View;

    const v1, 0x7f1001ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    :cond_2
    iget-object v0, p0, Lepp;->f:Landroid/view/View;

    return-object v0

    .line 223
    :cond_3
    invoke-virtual {p0}, Lepp;->d()I

    move-result v0

    invoke-virtual {v1, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepp;->f:Landroid/view/View;

    goto :goto_0
.end method

.method protected k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 147
    iget-boolean v0, p0, Lepp;->g:Z

    if-nez v0, :cond_0

    .line 156
    :goto_0
    return-void

    .line 151
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lepp;->g:Z

    .line 152
    iget-object v0, p0, Lepp;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 153
    iput-object v2, p0, Lepp;->f:Landroid/view/View;

    .line 154
    iget-object v0, p0, Lepp;->d:Lept;

    invoke-interface {v0}, Lept;->ac()V

    .line 155
    iget-object v0, p0, Lepp;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lepm;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x1

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 308
    invoke-virtual {p0}, Lepp;->l()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 309
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SignedOut Promos must define a shared pref keys"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 134
    invoke-virtual {p0}, Lepp;->k()V

    .line 135
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 280
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1001ac

    if-ne v0, v1, :cond_1

    .line 281
    invoke-virtual {p0}, Lepp;->a()V

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1001ad

    if-ne v0, v1, :cond_0

    .line 283
    invoke-virtual {p0}, Lepp;->b()V

    goto :goto_0
.end method
