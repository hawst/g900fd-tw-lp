.class public abstract Lkhs;
.super Lu;
.source "PG"

# interfaces
.implements Lkie;


# instance fields
.field private N:Lkib;

.field private O:Landroid/widget/ListView;

.field private P:Z

.field private Q:Z

.field private R:Landroid/os/Handler;

.field private final S:Ljava/lang/Runnable;

.field private T:Landroid/view/View$OnKeyListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lu;-><init>()V

    .line 116
    new-instance v0, Lkht;

    invoke-direct {v0, p0}, Lkht;-><init>(Lkhs;)V

    iput-object v0, p0, Lkhs;->R:Landroid/os/Handler;

    .line 128
    new-instance v0, Lkhu;

    invoke-direct {v0, p0}, Lkhu;-><init>(Lkhs;)V

    iput-object v0, p0, Lkhs;->S:Ljava/lang/Runnable;

    .line 361
    new-instance v0, Lkhv;

    invoke-direct {v0, p0}, Lkhv;-><init>(Lkhs;)V

    iput-object v0, p0, Lkhs;->T:Landroid/view/View$OnKeyListener;

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 325
    invoke-virtual {p0}, Lkhs;->ab()Lcom/google/android/libraries/social/settings/PreferenceScreen;

    move-result-object v0

    .line 326
    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {p0}, Lkhs;->ac()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->a(Landroid/widget/ListView;)V

    .line 329
    :cond_0
    return-void
.end method

.method static synthetic a(Lkhs;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lkhs;->a()V

    return-void
.end method

.method static synthetic b(Lkhs;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lkhs;->O:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lkhs;->N:Lkib;

    invoke-virtual {v0}, Lkib;->i()V

    .line 208
    invoke-super {p0}, Lu;->A()V

    .line 209
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 159
    const v0, 0x7f0401ad

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 225
    invoke-super {p0, p1, p2, p3}, Lu;->a(IILandroid/content/Intent;)V

    .line 227
    iget-object v0, p0, Lkhs;->N:Lkib;

    invoke-virtual {v0, p1, p3}, Lkib;->a(ILandroid/content/Intent;)V

    .line 228
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 151
    invoke-super {p0, p1}, Lu;->a(Landroid/os/Bundle;)V

    .line 152
    new-instance v0, Lkib;

    invoke-virtual {p0}, Lkhs;->n()Lz;

    move-result-object v1

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lkib;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lkhs;->N:Lkib;

    .line 153
    iget-object v0, p0, Lkhs;->N:Lkib;

    invoke-virtual {v0, p0}, Lkib;->a(Lkhs;)V

    .line 154
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/settings/PreferenceScreen;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 244
    iget-object v0, p0, Lkhs;->N:Lkib;

    invoke-virtual {v0, p1}, Lkib;->a(Lcom/google/android/libraries/social/settings/PreferenceScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 245
    iput-boolean v1, p0, Lkhs;->P:Z

    .line 246
    iget-boolean v0, p0, Lkhs;->Q:Z

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lkhs;->R:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkhs;->R:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 250
    :cond_0
    return-void
.end method

.method public a(Lkhl;)Z
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p1}, Lkhl;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 292
    invoke-virtual {p0}, Lkhs;->n()Lz;

    move-result-object v0

    instance-of v0, v0, Lkhw;

    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {p0}, Lkhs;->n()Lz;

    move-result-object v0

    check-cast v0, Lkhw;

    invoke-interface {v0}, Lkhw;->a()Z

    move-result v0

    .line 296
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aa()Lkib;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lkhs;->N:Lkib;

    return-object v0
.end method

.method public ab()Lcom/google/android/libraries/social/settings/PreferenceScreen;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lkhs;->N:Lkib;

    invoke-virtual {v0}, Lkib;->d()Lcom/google/android/libraries/social/settings/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method public ac()Landroid/widget/ListView;
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lkhs;->O:Landroid/widget/ListView;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lkhs;->x()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content view not yet created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/ListView;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Content has view with id attribute \'android.R.id.list\' that is not a ListView class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lkhs;->O:Landroid/widget/ListView;

    iget-object v0, p0, Lkhs;->O:Landroid/widget/ListView;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a ListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lkhs;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lkhs;->T:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lkhs;->R:Landroid/os/Handler;

    iget-object v1, p0, Lkhs;->S:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 334
    :cond_3
    iget-object v0, p0, Lkhs;->O:Landroid/widget/ListView;

    return-object v0
.end method

.method public ae_()V
    .locals 2

    .prologue
    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lkhs;->O:Landroid/widget/ListView;

    .line 200
    iget-object v0, p0, Lkhs;->R:Landroid/os/Handler;

    iget-object v1, p0, Lkhs;->S:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 201
    iget-object v0, p0, Lkhs;->R:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 202
    invoke-super {p0}, Lu;->ae_()V

    .line 203
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0, p1}, Lu;->d(Landroid/os/Bundle;)V

    .line 167
    iget-boolean v0, p0, Lkhs;->P:Z

    if-eqz v0, :cond_0

    .line 168
    invoke-direct {p0}, Lkhs;->a()V

    .line 171
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkhs;->Q:Z

    .line 173
    if-eqz p1, :cond_1

    .line 174
    const-string v0, "settings:preferences"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_1

    .line 176
    invoke-virtual {p0}, Lkhs;->ab()Lcom/google/android/libraries/social/settings/PreferenceScreen;

    move-result-object v1

    .line 177
    if-eqz v1, :cond_1

    .line 178
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->d(Landroid/os/Bundle;)V

    .line 182
    :cond_1
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 213
    invoke-super {p0, p1}, Lu;->e(Landroid/os/Bundle;)V

    .line 215
    invoke-virtual {p0}, Lkhs;->ab()Lcom/google/android/libraries/social/settings/PreferenceScreen;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_0

    .line 217
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 218
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->b(Landroid/os/Bundle;)V

    .line 219
    const-string v0, "settings:preferences"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 221
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Lu;->g()V

    .line 187
    iget-object v0, p0, Lkhs;->N:Lkib;

    invoke-virtual {v0, p0}, Lkib;->a(Lkie;)V

    .line 188
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0}, Lu;->h()V

    .line 193
    iget-object v0, p0, Lkhs;->N:Lkib;

    invoke-virtual {v0}, Lkib;->h()V

    .line 194
    iget-object v0, p0, Lkhs;->N:Lkib;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkib;->a(Lkie;)V

    .line 195
    return-void
.end method
