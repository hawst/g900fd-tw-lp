.class public final Lpjs;
.super Loxq;
.source "PG"


# instance fields
.field private A:Ljava/lang/Boolean;

.field private B:[B

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:[B

.field private i:[B

.field private j:[B

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:[Lpjk;

.field private n:[B

.field private o:Lpjt;

.field private p:Lpjj;

.field private q:Lpjq;

.field private r:Lpjm;

.field private s:Lpjn;

.field private t:Lpjo;

.field private u:Lpjp;

.field private v:Lpjl;

.field private w:Lpjr;

.field private x:Ljava/lang/Integer;

.field private y:[B

.field private z:[B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 38
    const/high16 v0, -0x80000000

    iput v0, p0, Lpjs;->f:I

    .line 53
    sget-object v0, Lpjk;->a:[Lpjk;

    iput-object v0, p0, Lpjs;->m:[Lpjk;

    .line 58
    iput-object v1, p0, Lpjs;->o:Lpjt;

    .line 61
    iput-object v1, p0, Lpjs;->p:Lpjj;

    .line 64
    iput-object v1, p0, Lpjs;->q:Lpjq;

    .line 67
    iput-object v1, p0, Lpjs;->r:Lpjm;

    .line 70
    iput-object v1, p0, Lpjs;->s:Lpjn;

    .line 73
    iput-object v1, p0, Lpjs;->t:Lpjo;

    .line 76
    iput-object v1, p0, Lpjs;->u:Lpjp;

    .line 79
    iput-object v1, p0, Lpjs;->v:Lpjl;

    .line 82
    iput-object v1, p0, Lpjs;->w:Lpjr;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 191
    .line 192
    iget-object v0, p0, Lpjs;->a:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 193
    const/4 v0, 0x1

    iget-object v2, p0, Lpjs;->a:Ljava/lang/String;

    .line 194
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 196
    :goto_0
    iget-object v2, p0, Lpjs;->b:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 197
    const/4 v2, 0x2

    iget-object v3, p0, Lpjs;->b:Ljava/lang/Long;

    .line 198
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 200
    :cond_0
    iget-object v2, p0, Lpjs;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 201
    const/4 v2, 0x3

    iget-object v3, p0, Lpjs;->c:Ljava/lang/String;

    .line 202
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 204
    :cond_1
    iget-object v2, p0, Lpjs;->d:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 205
    const/4 v2, 0x4

    iget-object v3, p0, Lpjs;->d:Ljava/lang/Long;

    .line 206
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 208
    :cond_2
    iget-object v2, p0, Lpjs;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 209
    const/4 v2, 0x5

    iget-object v3, p0, Lpjs;->e:Ljava/lang/String;

    .line 210
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 212
    :cond_3
    iget v2, p0, Lpjs;->f:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_4

    .line 213
    const/4 v2, 0x6

    iget v3, p0, Lpjs;->f:I

    .line 214
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 216
    :cond_4
    iget-object v2, p0, Lpjs;->g:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 217
    const/4 v2, 0x7

    iget-object v3, p0, Lpjs;->g:Ljava/lang/String;

    .line 218
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 220
    :cond_5
    iget-object v2, p0, Lpjs;->h:[B

    if-eqz v2, :cond_6

    .line 221
    const/16 v2, 0x8

    iget-object v3, p0, Lpjs;->h:[B

    .line 222
    invoke-static {v2, v3}, Loxo;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 224
    :cond_6
    iget-object v2, p0, Lpjs;->i:[B

    if-eqz v2, :cond_7

    .line 225
    const/16 v2, 0x9

    iget-object v3, p0, Lpjs;->i:[B

    .line 226
    invoke-static {v2, v3}, Loxo;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 228
    :cond_7
    iget-object v2, p0, Lpjs;->j:[B

    if-eqz v2, :cond_8

    .line 229
    const/16 v2, 0xa

    iget-object v3, p0, Lpjs;->j:[B

    .line 230
    invoke-static {v2, v3}, Loxo;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 232
    :cond_8
    iget-object v2, p0, Lpjs;->k:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 233
    const/16 v2, 0xb

    iget-object v3, p0, Lpjs;->k:Ljava/lang/String;

    .line 234
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 236
    :cond_9
    iget-object v2, p0, Lpjs;->l:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 237
    const/16 v2, 0xc

    iget-object v3, p0, Lpjs;->l:Ljava/lang/String;

    .line 238
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 240
    :cond_a
    iget-object v2, p0, Lpjs;->m:[Lpjk;

    if-eqz v2, :cond_c

    .line 241
    iget-object v2, p0, Lpjs;->m:[Lpjk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 242
    if-eqz v4, :cond_b

    .line 243
    const/16 v5, 0xd

    .line 244
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 241
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 248
    :cond_c
    iget-object v1, p0, Lpjs;->n:[B

    if-eqz v1, :cond_d

    .line 249
    const/16 v1, 0xe

    iget-object v2, p0, Lpjs;->n:[B

    .line 250
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_d
    iget-object v1, p0, Lpjs;->o:Lpjt;

    if-eqz v1, :cond_e

    .line 253
    const/16 v1, 0xf

    iget-object v2, p0, Lpjs;->o:Lpjt;

    .line 254
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 256
    :cond_e
    iget-object v1, p0, Lpjs;->p:Lpjj;

    if-eqz v1, :cond_f

    .line 257
    const/16 v1, 0x10

    iget-object v2, p0, Lpjs;->p:Lpjj;

    .line 258
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_f
    iget-object v1, p0, Lpjs;->q:Lpjq;

    if-eqz v1, :cond_10

    .line 261
    const/16 v1, 0x11

    iget-object v2, p0, Lpjs;->q:Lpjq;

    .line 262
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_10
    iget-object v1, p0, Lpjs;->r:Lpjm;

    if-eqz v1, :cond_11

    .line 265
    const/16 v1, 0x12

    iget-object v2, p0, Lpjs;->r:Lpjm;

    .line 266
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_11
    iget-object v1, p0, Lpjs;->s:Lpjn;

    if-eqz v1, :cond_12

    .line 269
    const/16 v1, 0x13

    iget-object v2, p0, Lpjs;->s:Lpjn;

    .line 270
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_12
    iget-object v1, p0, Lpjs;->t:Lpjo;

    if-eqz v1, :cond_13

    .line 273
    const/16 v1, 0x14

    iget-object v2, p0, Lpjs;->t:Lpjo;

    .line 274
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 276
    :cond_13
    iget-object v1, p0, Lpjs;->u:Lpjp;

    if-eqz v1, :cond_14

    .line 277
    const/16 v1, 0x15

    iget-object v2, p0, Lpjs;->u:Lpjp;

    .line 278
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    :cond_14
    iget-object v1, p0, Lpjs;->v:Lpjl;

    if-eqz v1, :cond_15

    .line 281
    const/16 v1, 0x16

    iget-object v2, p0, Lpjs;->v:Lpjl;

    .line 282
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_15
    iget-object v1, p0, Lpjs;->w:Lpjr;

    if-eqz v1, :cond_16

    .line 285
    const/16 v1, 0x17

    iget-object v2, p0, Lpjs;->w:Lpjr;

    .line 286
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 288
    :cond_16
    iget-object v1, p0, Lpjs;->x:Ljava/lang/Integer;

    if-eqz v1, :cond_17

    .line 289
    const/16 v1, 0x18

    iget-object v2, p0, Lpjs;->x:Ljava/lang/Integer;

    .line 290
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 292
    :cond_17
    iget-object v1, p0, Lpjs;->y:[B

    if-eqz v1, :cond_18

    .line 293
    const/16 v1, 0x19

    iget-object v2, p0, Lpjs;->y:[B

    .line 294
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 296
    :cond_18
    iget-object v1, p0, Lpjs;->z:[B

    if-eqz v1, :cond_19

    .line 297
    const/16 v1, 0x1a

    iget-object v2, p0, Lpjs;->z:[B

    .line 298
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    :cond_19
    iget-object v1, p0, Lpjs;->A:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    .line 301
    const/16 v1, 0x1b

    iget-object v2, p0, Lpjs;->A:Ljava/lang/Boolean;

    .line 302
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 304
    :cond_1a
    iget-object v1, p0, Lpjs;->B:[B

    if-eqz v1, :cond_1b

    .line 305
    const/16 v1, 0x1c

    iget-object v2, p0, Lpjs;->B:[B

    .line 306
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_1b
    iget-object v1, p0, Lpjs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    iput v0, p0, Lpjs;->ai:I

    .line 310
    return v0

    :cond_1c
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpjs;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 318
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 319
    sparse-switch v0, :sswitch_data_0

    .line 323
    iget-object v2, p0, Lpjs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 324
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpjs;->ah:Ljava/util/List;

    .line 327
    :cond_1
    iget-object v2, p0, Lpjs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 329
    :sswitch_0
    return-object p0

    .line 334
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjs;->a:Ljava/lang/String;

    goto :goto_0

    .line 338
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpjs;->b:Ljava/lang/Long;

    goto :goto_0

    .line 342
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjs;->c:Ljava/lang/String;

    goto :goto_0

    .line 346
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpjs;->d:Ljava/lang/Long;

    goto :goto_0

    .line 350
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjs;->e:Ljava/lang/String;

    goto :goto_0

    .line 354
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 355
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-ne v0, v2, :cond_3

    .line 367
    :cond_2
    iput v0, p0, Lpjs;->f:I

    goto :goto_0

    .line 369
    :cond_3
    iput v4, p0, Lpjs;->f:I

    goto :goto_0

    .line 374
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjs;->g:Ljava/lang/String;

    goto :goto_0

    .line 378
    :sswitch_8
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjs;->h:[B

    goto/16 :goto_0

    .line 382
    :sswitch_9
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjs;->i:[B

    goto/16 :goto_0

    .line 386
    :sswitch_a
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjs;->j:[B

    goto/16 :goto_0

    .line 390
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjs;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 394
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjs;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 398
    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 399
    iget-object v0, p0, Lpjs;->m:[Lpjk;

    if-nez v0, :cond_5

    move v0, v1

    .line 400
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpjk;

    .line 401
    iget-object v3, p0, Lpjs;->m:[Lpjk;

    if-eqz v3, :cond_4

    .line 402
    iget-object v3, p0, Lpjs;->m:[Lpjk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 404
    :cond_4
    iput-object v2, p0, Lpjs;->m:[Lpjk;

    .line 405
    :goto_2
    iget-object v2, p0, Lpjs;->m:[Lpjk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 406
    iget-object v2, p0, Lpjs;->m:[Lpjk;

    new-instance v3, Lpjk;

    invoke-direct {v3}, Lpjk;-><init>()V

    aput-object v3, v2, v0

    .line 407
    iget-object v2, p0, Lpjs;->m:[Lpjk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 408
    invoke-virtual {p1}, Loxn;->a()I

    .line 405
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 399
    :cond_5
    iget-object v0, p0, Lpjs;->m:[Lpjk;

    array-length v0, v0

    goto :goto_1

    .line 411
    :cond_6
    iget-object v2, p0, Lpjs;->m:[Lpjk;

    new-instance v3, Lpjk;

    invoke-direct {v3}, Lpjk;-><init>()V

    aput-object v3, v2, v0

    .line 412
    iget-object v2, p0, Lpjs;->m:[Lpjk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 416
    :sswitch_e
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjs;->n:[B

    goto/16 :goto_0

    .line 420
    :sswitch_f
    iget-object v0, p0, Lpjs;->o:Lpjt;

    if-nez v0, :cond_7

    .line 421
    new-instance v0, Lpjt;

    invoke-direct {v0}, Lpjt;-><init>()V

    iput-object v0, p0, Lpjs;->o:Lpjt;

    .line 423
    :cond_7
    iget-object v0, p0, Lpjs;->o:Lpjt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 427
    :sswitch_10
    iget-object v0, p0, Lpjs;->p:Lpjj;

    if-nez v0, :cond_8

    .line 428
    new-instance v0, Lpjj;

    invoke-direct {v0}, Lpjj;-><init>()V

    iput-object v0, p0, Lpjs;->p:Lpjj;

    .line 430
    :cond_8
    iget-object v0, p0, Lpjs;->p:Lpjj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 434
    :sswitch_11
    iget-object v0, p0, Lpjs;->q:Lpjq;

    if-nez v0, :cond_9

    .line 435
    new-instance v0, Lpjq;

    invoke-direct {v0}, Lpjq;-><init>()V

    iput-object v0, p0, Lpjs;->q:Lpjq;

    .line 437
    :cond_9
    iget-object v0, p0, Lpjs;->q:Lpjq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 441
    :sswitch_12
    iget-object v0, p0, Lpjs;->r:Lpjm;

    if-nez v0, :cond_a

    .line 442
    new-instance v0, Lpjm;

    invoke-direct {v0}, Lpjm;-><init>()V

    iput-object v0, p0, Lpjs;->r:Lpjm;

    .line 444
    :cond_a
    iget-object v0, p0, Lpjs;->r:Lpjm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 448
    :sswitch_13
    iget-object v0, p0, Lpjs;->s:Lpjn;

    if-nez v0, :cond_b

    .line 449
    new-instance v0, Lpjn;

    invoke-direct {v0}, Lpjn;-><init>()V

    iput-object v0, p0, Lpjs;->s:Lpjn;

    .line 451
    :cond_b
    iget-object v0, p0, Lpjs;->s:Lpjn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 455
    :sswitch_14
    iget-object v0, p0, Lpjs;->t:Lpjo;

    if-nez v0, :cond_c

    .line 456
    new-instance v0, Lpjo;

    invoke-direct {v0}, Lpjo;-><init>()V

    iput-object v0, p0, Lpjs;->t:Lpjo;

    .line 458
    :cond_c
    iget-object v0, p0, Lpjs;->t:Lpjo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 462
    :sswitch_15
    iget-object v0, p0, Lpjs;->u:Lpjp;

    if-nez v0, :cond_d

    .line 463
    new-instance v0, Lpjp;

    invoke-direct {v0}, Lpjp;-><init>()V

    iput-object v0, p0, Lpjs;->u:Lpjp;

    .line 465
    :cond_d
    iget-object v0, p0, Lpjs;->u:Lpjp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 469
    :sswitch_16
    iget-object v0, p0, Lpjs;->v:Lpjl;

    if-nez v0, :cond_e

    .line 470
    new-instance v0, Lpjl;

    invoke-direct {v0}, Lpjl;-><init>()V

    iput-object v0, p0, Lpjs;->v:Lpjl;

    .line 472
    :cond_e
    iget-object v0, p0, Lpjs;->v:Lpjl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 476
    :sswitch_17
    iget-object v0, p0, Lpjs;->w:Lpjr;

    if-nez v0, :cond_f

    .line 477
    new-instance v0, Lpjr;

    invoke-direct {v0}, Lpjr;-><init>()V

    iput-object v0, p0, Lpjs;->w:Lpjr;

    .line 479
    :cond_f
    iget-object v0, p0, Lpjs;->w:Lpjr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 483
    :sswitch_18
    invoke-virtual {p1}, Loxn;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpjs;->x:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 487
    :sswitch_19
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjs;->y:[B

    goto/16 :goto_0

    .line 491
    :sswitch_1a
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjs;->z:[B

    goto/16 :goto_0

    .line 495
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpjs;->A:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 499
    :sswitch_1c
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjs;->B:[B

    goto/16 :goto_0

    .line 319
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc5 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe2 -> :sswitch_1c
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 97
    iget-object v0, p0, Lpjs;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    iget-object v1, p0, Lpjs;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 100
    :cond_0
    iget-object v0, p0, Lpjs;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 101
    const/4 v0, 0x2

    iget-object v1, p0, Lpjs;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 103
    :cond_1
    iget-object v0, p0, Lpjs;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 104
    const/4 v0, 0x3

    iget-object v1, p0, Lpjs;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 106
    :cond_2
    iget-object v0, p0, Lpjs;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 107
    const/4 v0, 0x4

    iget-object v1, p0, Lpjs;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 109
    :cond_3
    iget-object v0, p0, Lpjs;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 110
    const/4 v0, 0x5

    iget-object v1, p0, Lpjs;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 112
    :cond_4
    iget v0, p0, Lpjs;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_5

    .line 113
    const/4 v0, 0x6

    iget v1, p0, Lpjs;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 115
    :cond_5
    iget-object v0, p0, Lpjs;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 116
    const/4 v0, 0x7

    iget-object v1, p0, Lpjs;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 118
    :cond_6
    iget-object v0, p0, Lpjs;->h:[B

    if-eqz v0, :cond_7

    .line 119
    const/16 v0, 0x8

    iget-object v1, p0, Lpjs;->h:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 121
    :cond_7
    iget-object v0, p0, Lpjs;->i:[B

    if-eqz v0, :cond_8

    .line 122
    const/16 v0, 0x9

    iget-object v1, p0, Lpjs;->i:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 124
    :cond_8
    iget-object v0, p0, Lpjs;->j:[B

    if-eqz v0, :cond_9

    .line 125
    const/16 v0, 0xa

    iget-object v1, p0, Lpjs;->j:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 127
    :cond_9
    iget-object v0, p0, Lpjs;->k:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 128
    const/16 v0, 0xb

    iget-object v1, p0, Lpjs;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 130
    :cond_a
    iget-object v0, p0, Lpjs;->l:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 131
    const/16 v0, 0xc

    iget-object v1, p0, Lpjs;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 133
    :cond_b
    iget-object v0, p0, Lpjs;->m:[Lpjk;

    if-eqz v0, :cond_d

    .line 134
    iget-object v1, p0, Lpjs;->m:[Lpjk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_d

    aget-object v3, v1, v0

    .line 135
    if-eqz v3, :cond_c

    .line 136
    const/16 v4, 0xd

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 134
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_d
    iget-object v0, p0, Lpjs;->n:[B

    if-eqz v0, :cond_e

    .line 141
    const/16 v0, 0xe

    iget-object v1, p0, Lpjs;->n:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 143
    :cond_e
    iget-object v0, p0, Lpjs;->o:Lpjt;

    if-eqz v0, :cond_f

    .line 144
    const/16 v0, 0xf

    iget-object v1, p0, Lpjs;->o:Lpjt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 146
    :cond_f
    iget-object v0, p0, Lpjs;->p:Lpjj;

    if-eqz v0, :cond_10

    .line 147
    const/16 v0, 0x10

    iget-object v1, p0, Lpjs;->p:Lpjj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 149
    :cond_10
    iget-object v0, p0, Lpjs;->q:Lpjq;

    if-eqz v0, :cond_11

    .line 150
    const/16 v0, 0x11

    iget-object v1, p0, Lpjs;->q:Lpjq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 152
    :cond_11
    iget-object v0, p0, Lpjs;->r:Lpjm;

    if-eqz v0, :cond_12

    .line 153
    const/16 v0, 0x12

    iget-object v1, p0, Lpjs;->r:Lpjm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 155
    :cond_12
    iget-object v0, p0, Lpjs;->s:Lpjn;

    if-eqz v0, :cond_13

    .line 156
    const/16 v0, 0x13

    iget-object v1, p0, Lpjs;->s:Lpjn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 158
    :cond_13
    iget-object v0, p0, Lpjs;->t:Lpjo;

    if-eqz v0, :cond_14

    .line 159
    const/16 v0, 0x14

    iget-object v1, p0, Lpjs;->t:Lpjo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 161
    :cond_14
    iget-object v0, p0, Lpjs;->u:Lpjp;

    if-eqz v0, :cond_15

    .line 162
    const/16 v0, 0x15

    iget-object v1, p0, Lpjs;->u:Lpjp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 164
    :cond_15
    iget-object v0, p0, Lpjs;->v:Lpjl;

    if-eqz v0, :cond_16

    .line 165
    const/16 v0, 0x16

    iget-object v1, p0, Lpjs;->v:Lpjl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 167
    :cond_16
    iget-object v0, p0, Lpjs;->w:Lpjr;

    if-eqz v0, :cond_17

    .line 168
    const/16 v0, 0x17

    iget-object v1, p0, Lpjs;->w:Lpjr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 170
    :cond_17
    iget-object v0, p0, Lpjs;->x:Ljava/lang/Integer;

    if-eqz v0, :cond_18

    .line 171
    const/16 v0, 0x18

    iget-object v1, p0, Lpjs;->x:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->b(II)V

    .line 173
    :cond_18
    iget-object v0, p0, Lpjs;->y:[B

    if-eqz v0, :cond_19

    .line 174
    const/16 v0, 0x19

    iget-object v1, p0, Lpjs;->y:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 176
    :cond_19
    iget-object v0, p0, Lpjs;->z:[B

    if-eqz v0, :cond_1a

    .line 177
    const/16 v0, 0x1a

    iget-object v1, p0, Lpjs;->z:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 179
    :cond_1a
    iget-object v0, p0, Lpjs;->A:Ljava/lang/Boolean;

    if-eqz v0, :cond_1b

    .line 180
    const/16 v0, 0x1b

    iget-object v1, p0, Lpjs;->A:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 182
    :cond_1b
    iget-object v0, p0, Lpjs;->B:[B

    if-eqz v0, :cond_1c

    .line 183
    const/16 v0, 0x1c

    iget-object v1, p0, Lpjs;->B:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 185
    :cond_1c
    iget-object v0, p0, Lpjs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 187
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpjs;->a(Loxn;)Lpjs;

    move-result-object v0

    return-object v0
.end method
