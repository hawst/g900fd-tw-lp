.class public final Lmav;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmps;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18628
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18631
    iput-object v0, p0, Lmav;->apiHeader:Llyr;

    .line 18634
    iput-object v0, p0, Lmav;->a:Lmps;

    .line 18628
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 18651
    const/4 v0, 0x0

    .line 18652
    iget-object v1, p0, Lmav;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 18653
    const/4 v0, 0x1

    iget-object v1, p0, Lmav;->apiHeader:Llyr;

    .line 18654
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18656
    :cond_0
    iget-object v1, p0, Lmav;->a:Lmps;

    if-eqz v1, :cond_1

    .line 18657
    const/4 v1, 0x2

    iget-object v2, p0, Lmav;->a:Lmps;

    .line 18658
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18660
    :cond_1
    iget-object v1, p0, Lmav;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18661
    iput v0, p0, Lmav;->ai:I

    .line 18662
    return v0
.end method

.method public a(Loxn;)Lmav;
    .locals 2

    .prologue
    .line 18670
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 18671
    sparse-switch v0, :sswitch_data_0

    .line 18675
    iget-object v1, p0, Lmav;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 18676
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmav;->ah:Ljava/util/List;

    .line 18679
    :cond_1
    iget-object v1, p0, Lmav;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18681
    :sswitch_0
    return-object p0

    .line 18686
    :sswitch_1
    iget-object v0, p0, Lmav;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 18687
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmav;->apiHeader:Llyr;

    .line 18689
    :cond_2
    iget-object v0, p0, Lmav;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18693
    :sswitch_2
    iget-object v0, p0, Lmav;->a:Lmps;

    if-nez v0, :cond_3

    .line 18694
    new-instance v0, Lmps;

    invoke-direct {v0}, Lmps;-><init>()V

    iput-object v0, p0, Lmav;->a:Lmps;

    .line 18696
    :cond_3
    iget-object v0, p0, Lmav;->a:Lmps;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18671
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 18639
    iget-object v0, p0, Lmav;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 18640
    const/4 v0, 0x1

    iget-object v1, p0, Lmav;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18642
    :cond_0
    iget-object v0, p0, Lmav;->a:Lmps;

    if-eqz v0, :cond_1

    .line 18643
    const/4 v0, 0x2

    iget-object v1, p0, Lmav;->a:Lmps;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18645
    :cond_1
    iget-object v0, p0, Lmav;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 18647
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 18624
    invoke-virtual {p0, p1}, Lmav;->a(Loxn;)Lmav;

    move-result-object v0

    return-object v0
.end method
