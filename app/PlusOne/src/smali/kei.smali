.class final Lkei;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lked;


# static fields
.field private static final a:Lkef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lkej;

    invoke-direct {v0}, Lkej;-><init>()V

    sput-object v0, Lkei;->a:Lkef;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method


# virtual methods
.method public a(II)Lkee;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lkee;

    sget-object v1, Lkei;->a:Lkef;

    invoke-direct {v0, p1, p2, v1}, Lkee;-><init>(IILkef;)V

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)Lkee;
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lkei;->a(II)Lkee;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkee;Ljava/util/SortedSet;Lkeg;)Lkee;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkee;",
            "Ljava/util/SortedSet",
            "<",
            "Lkee;",
            ">;",
            "Lkeg;",
            ")",
            "Lkee;"
        }
    .end annotation

    .prologue
    .line 38
    const/4 v1, 0x0

    .line 39
    invoke-interface {p2}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkee;

    .line 40
    iget v3, v0, Lkee;->b:I

    iget v4, p1, Lkee;->b:I

    if-lt v3, v4, :cond_0

    iget v3, v0, Lkee;->a:I

    iget v4, p1, Lkee;->a:I

    if-lt v3, v4, :cond_0

    .line 41
    sget-object v3, Lkeg;->b:Lkeg;

    if-eq p3, v3, :cond_1

    iget v3, v0, Lkee;->b:I

    iget v4, p1, Lkee;->b:I

    if-ne v3, v4, :cond_0

    iget v3, v0, Lkee;->a:I

    iget v4, p1, Lkee;->a:I

    if-ne v3, v4, :cond_0

    .line 49
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Lkee;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method
