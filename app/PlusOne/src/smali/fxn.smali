.class public final Lfxn;
.super Lfxu;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:Landroid/graphics/drawable/Drawable;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static e:Ljava/lang/String;

.field private static i:Z


# instance fields
.field private b:Landroid/widget/ImageView;

.field private f:Landroid/widget/CheckBox;

.field private g:Leaq;

.field private h:Lfxf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lfxu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lfxn;->h:Lfxf;

    if-eqz v0, :cond_0

    .line 98
    iget-object v1, p0, Lfxn;->h:Lfxf;

    iget-object v0, p0, Lfxn;->g:Leaq;

    iget-boolean v0, v0, Leaq;->d:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lfxf;->a(Z)V

    .line 100
    :cond_0
    return-void

    .line 98
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 51
    invoke-super {p0, p1, p2, p3}, Lfxu;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    sget-boolean v0, Lfxn;->i:Z

    if-nez v0, :cond_0

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 55
    const v1, 0x7f0203cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfxn;->a:Landroid/graphics/drawable/Drawable;

    .line 56
    const v1, 0x7f0a074e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfxn;->d:Ljava/lang/String;

    .line 57
    const v1, 0x7f0a074f

    .line 58
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfxn;->e:Ljava/lang/String;

    .line 59
    const v1, 0x7f0a0749

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfxn;->c:Ljava/lang/String;

    .line 60
    const/4 v0, 0x1

    sput-boolean v0, Lfxn;->i:Z

    .line 63
    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxn;->b:Landroid/widget/ImageView;

    .line 64
    iget-object v0, p0, Lfxn;->b:Landroid/widget/ImageView;

    sget-object v1, Lfxn;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 66
    new-instance v0, Landroid/widget/CheckBox;

    invoke-direct {v0, p1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    .line 67
    iget-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    new-instance v1, Lfyf;

    invoke-direct {v1, v3, v3}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 71
    iget-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 72
    return-void
.end method

.method public a(Leaq;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 79
    iput-object p1, p0, Lfxn;->g:Leaq;

    .line 81
    iget-boolean v0, p1, Leaq;->f:Z

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    iget-boolean v1, p1, Leaq;->d:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 84
    iget-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setDuplicateParentStateEnabled(Z)V

    .line 85
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, Lfxn;->a(ZZ)V

    .line 91
    :goto_0
    sget-object v1, Lfxn;->c:Ljava/lang/String;

    iget-boolean v0, p1, Leaq;->f:Z

    if-eqz v0, :cond_1

    sget-object v0, Lfxn;->e:Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lfxn;->b:Landroid/widget/ImageView;

    iget-object v3, p0, Lfxn;->f:Landroid/widget/CheckBox;

    invoke-super {p0, v1, v0, v2, v3}, Lfxu;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V

    .line 93
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lfxn;->f:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 88
    invoke-virtual {p0, v2, v2}, Lfxn;->a(ZZ)V

    goto :goto_0

    .line 91
    :cond_1
    sget-object v0, Lfxn;->d:Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Lfxf;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lfxn;->h:Lfxf;

    .line 76
    return-void
.end method
