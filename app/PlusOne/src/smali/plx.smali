.class public final Lplx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lplx;


# instance fields
.field private b:Lpmx;

.field private c:Lplv;

.field private d:Lply;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x0

    new-array v0, v0, [Lplx;

    sput-object v0, Lplx;->a:[Lplx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 296
    invoke-direct {p0}, Loxq;-><init>()V

    .line 299
    iput-object v0, p0, Lplx;->b:Lpmx;

    .line 302
    iput-object v0, p0, Lplx;->c:Lplv;

    .line 305
    iput-object v0, p0, Lplx;->d:Lply;

    .line 296
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 325
    const/4 v0, 0x0

    .line 326
    iget-object v1, p0, Lplx;->b:Lpmx;

    if-eqz v1, :cond_0

    .line 327
    const/4 v0, 0x1

    iget-object v1, p0, Lplx;->b:Lpmx;

    .line 328
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 330
    :cond_0
    iget-object v1, p0, Lplx;->c:Lplv;

    if-eqz v1, :cond_1

    .line 331
    const/4 v1, 0x2

    iget-object v2, p0, Lplx;->c:Lplv;

    .line 332
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    :cond_1
    iget-object v1, p0, Lplx;->d:Lply;

    if-eqz v1, :cond_2

    .line 335
    const/4 v1, 0x4

    iget-object v2, p0, Lplx;->d:Lply;

    .line 336
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 338
    :cond_2
    iget-object v1, p0, Lplx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 339
    iput v0, p0, Lplx;->ai:I

    .line 340
    return v0
.end method

.method public a(Loxn;)Lplx;
    .locals 2

    .prologue
    .line 348
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 349
    sparse-switch v0, :sswitch_data_0

    .line 353
    iget-object v1, p0, Lplx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 354
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lplx;->ah:Ljava/util/List;

    .line 357
    :cond_1
    iget-object v1, p0, Lplx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    :sswitch_0
    return-object p0

    .line 364
    :sswitch_1
    iget-object v0, p0, Lplx;->b:Lpmx;

    if-nez v0, :cond_2

    .line 365
    new-instance v0, Lpmx;

    invoke-direct {v0}, Lpmx;-><init>()V

    iput-object v0, p0, Lplx;->b:Lpmx;

    .line 367
    :cond_2
    iget-object v0, p0, Lplx;->b:Lpmx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 371
    :sswitch_2
    iget-object v0, p0, Lplx;->c:Lplv;

    if-nez v0, :cond_3

    .line 372
    new-instance v0, Lplv;

    invoke-direct {v0}, Lplv;-><init>()V

    iput-object v0, p0, Lplx;->c:Lplv;

    .line 374
    :cond_3
    iget-object v0, p0, Lplx;->c:Lplv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 378
    :sswitch_3
    iget-object v0, p0, Lplx;->d:Lply;

    if-nez v0, :cond_4

    .line 379
    new-instance v0, Lply;

    invoke-direct {v0}, Lply;-><init>()V

    iput-object v0, p0, Lplx;->d:Lply;

    .line 381
    :cond_4
    iget-object v0, p0, Lplx;->d:Lply;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 349
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lplx;->b:Lpmx;

    if-eqz v0, :cond_0

    .line 311
    const/4 v0, 0x1

    iget-object v1, p0, Lplx;->b:Lpmx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 313
    :cond_0
    iget-object v0, p0, Lplx;->c:Lplv;

    if-eqz v0, :cond_1

    .line 314
    const/4 v0, 0x2

    iget-object v1, p0, Lplx;->c:Lplv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 316
    :cond_1
    iget-object v0, p0, Lplx;->d:Lply;

    if-eqz v0, :cond_2

    .line 317
    const/4 v0, 0x4

    iget-object v1, p0, Lplx;->d:Lply;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 319
    :cond_2
    iget-object v0, p0, Lplx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 321
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 292
    invoke-virtual {p0, p1}, Lplx;->a(Loxn;)Lplx;

    move-result-object v0

    return-object v0
.end method
