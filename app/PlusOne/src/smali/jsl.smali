.class Ljsl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhii;


# static fields
.field private static a:I

.field private static b:I

.field private static c:I

.field private static d:I


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Z

.field private g:Ljod;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 33
    sput v0, Ljsl;->a:I

    .line 34
    sput v0, Ljsl;->b:I

    .line 35
    sput v0, Ljsl;->c:I

    .line 36
    sput v0, Ljsl;->d:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljsm;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Ljsl;->e:Landroid/content/Context;

    .line 44
    invoke-virtual {p2}, Ljsm;->b()Ljod;

    move-result-object v0

    iput-object v0, p0, Ljsl;->g:Ljod;

    .line 45
    invoke-static {p1}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 46
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_child"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ljsl;->f:Z

    .line 47
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ljsl;->g:Ljod;

    invoke-interface {v0}, Ljod;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 62
    iget-object v0, p0, Ljsl;->g:Ljod;

    invoke-interface {v0}, Ljod;->c()I

    move-result v1

    .line 63
    packed-switch v1, :pswitch_data_0

    .line 72
    sget v0, Ljsl;->c:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Ljsl;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0126

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Ljsl;->c:I

    :cond_0
    sget v0, Ljsl;->c:I

    .line 75
    :goto_0
    iget-boolean v2, p0, Ljsl;->f:Z

    if-eqz v2, :cond_3

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_3

    .line 78
    :cond_1
    sget v0, Ljsl;->d:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Ljsl;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0129

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Ljsl;->d:I

    :cond_2
    sget v0, Ljsl;->d:I

    .line 80
    :cond_3
    return v0

    .line 66
    :pswitch_0
    sget v0, Ljsl;->a:I

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Ljsl;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0127

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Ljsl;->a:I

    :cond_4
    sget v0, Ljsl;->a:I

    goto :goto_0

    .line 69
    :pswitch_1
    sget v0, Ljsl;->b:I

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Ljsl;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0128

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Ljsl;->b:I

    :cond_5
    sget v0, Ljsl;->b:I

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public d()I
    .locals 1

    .prologue
    .line 85
    const/16 v0, 0x25

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x1

    return v0
.end method
