.class public final Ldju;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmbk;",
        "Lmbl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lhgw;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 31
    const-string v3, "getaudience"

    new-instance v4, Lmbk;

    invoke-direct {v4}, Lmbk;-><init>()V

    new-instance v5, Lmbl;

    invoke-direct {v5}, Lmbl;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 33
    iput-object p3, p0, Ldju;->a:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method protected a(Lmbk;)V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lnvg;

    invoke-direct {v0}, Lnvg;-><init>()V

    .line 39
    iget-object v1, p0, Ldju;->a:Ljava/lang/String;

    iput-object v1, v0, Lnvg;->a:Ljava/lang/String;

    .line 40
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnvg;->b:Ljava/lang/Boolean;

    .line 42
    iput-object v0, p1, Lmbk;->a:Lnvg;

    .line 43
    return-void
.end method

.method protected a(Lmbl;)V
    .locals 14

    .prologue
    .line 47
    iget-object v1, p1, Lmbl;->a:Lnvy;

    .line 48
    iget-object v0, v1, Lnvy;->c:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move v6, v0

    .line 50
    :goto_0
    iget-object v0, v1, Lnvy;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move v7, v0

    .line 52
    :goto_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 53
    iget-object v0, v1, Lnvy;->a:[Lofv;

    if-eqz v0, :cond_3

    .line 54
    iget-object v0, p0, Ldju;->f:Landroid/content/Context;

    iget v2, p0, Ldju;->c:I

    .line 55
    invoke-static {v0, v2}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 56
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 58
    :try_start_0
    iget-object v11, v1, Lnvy;->a:[Lofv;

    array-length v12, v11

    const/4 v0, 0x0

    move v8, v0

    :goto_2
    if-ge v8, v12, :cond_2

    aget-object v13, v11, v8

    .line 59
    new-instance v0, Ljqs;

    iget-object v1, v13, Lofv;->c:Ljava/lang/String;

    iget-object v2, v13, Lofv;->b:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, v13, Lofv;->d:Ljava/lang/String;

    .line 60
    invoke-static {v4}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 59
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object v0, v13, Lofv;->c:Ljava/lang/String;

    iget-object v1, v13, Lofv;->b:Ljava/lang/String;

    iget-object v2, v13, Lofv;->d:Ljava/lang/String;

    invoke-static {v10, v0, v1, v2}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2

    .line 48
    :cond_0
    iget-object v0, v1, Lnvy;->c:Ljava/lang/Integer;

    .line 49
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v6, v0

    goto :goto_0

    .line 50
    :cond_1
    iget-object v0, v1, Lnvy;->b:Ljava/lang/Integer;

    .line 51
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 65
    :cond_2
    :try_start_1
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 71
    :cond_3
    new-instance v0, Lhgw;

    const/4 v1, 0x0

    add-int v2, v6, v7

    invoke-direct {v0, v9, v1, v2}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;I)V

    iput-object v0, p0, Ldju;->b:Lhgw;

    .line 72
    return-void

    .line 67
    :catchall_0
    move-exception v0

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lmbk;

    invoke-virtual {p0, p1}, Ldju;->a(Lmbk;)V

    return-void
.end method

.method public b()Lhgw;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ldju;->b:Lhgw;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lmbl;

    invoke-virtual {p0, p1}, Ldju;->a(Lmbl;)V

    return-void
.end method
