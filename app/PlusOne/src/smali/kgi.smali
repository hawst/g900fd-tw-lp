.class public final Lkgi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Loxu;",
            ">;",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lkgi;->a:Ljava/util/Map;

    return-void
.end method

.method private static a(Loxu;)Ljava/lang/reflect/Field;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Loxu;",
            ">(TM;)",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 162
    sget-object v2, Lkgi;->a:Ljava/util/Map;

    monitor-enter v2

    .line 163
    :try_start_0
    sget-object v0, Lkgi;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Field;

    .line 164
    if-nez v0, :cond_0

    .line 167
    const-string v0, "apiHeader"

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 168
    sget-object v3, Lkgi;->a:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    :cond_0
    monitor-exit v2

    .line 171
    return-object v0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;I)Lkfu;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lkfo;

    invoke-direct {v0, p0, p1}, Lkfo;-><init>(Landroid/content/Context;I)V

    invoke-static {p0, v0}, Lkgi;->a(Landroid/content/Context;Lkfo;)Lkfu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lkfo;)Lkfu;
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lkfu;

    const-string v1, "plusi"

    const-string v2, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    invoke-direct {v0, p0, p1, v1, v2}, Lkfu;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static a(Landroid/content/Context;Loxu;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RS:",
            "Loxu;",
            ">(",
            "Landroid/content/Context;",
            "TRS;)V"
        }
    .end annotation

    .prologue
    .line 86
    const-class v0, Lhxw;

    .line 87
    invoke-static {p0, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxw;

    .line 88
    if-nez v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    :try_start_0
    invoke-static {p1}, Lkgi;->a(Loxu;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llyr;

    .line 98
    if-eqz v1, :cond_2

    iget-object v1, v1, Llyr;->c:Lltc;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 112
    :goto_1
    if-eqz v1, :cond_0

    .line 113
    invoke-virtual {v0, v1}, Lhxw;->a(Lltc;)V

    goto :goto_0

    .line 98
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 99
    :catch_0
    move-exception v0

    .line 101
    const-string v1, "PlusiUtils"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "No API header found in the response:\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 103
    :catch_1
    move-exception v0

    .line 105
    const-string v1, "PlusiUtils"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "No API header found in the response:\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 107
    :catch_2
    move-exception v0

    .line 109
    const-string v1, "PlusiUtils"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "No API header found in the response:\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Loxu;Ljava/lang/String;ZI)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RQ:",
            "Loxu;",
            ">(",
            "Landroid/content/Context;",
            "TRQ;",
            "Ljava/lang/String;",
            "ZI)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 66
    new-instance v2, Lpyu;

    invoke-direct {v2}, Lpyu;-><init>()V

    new-instance v0, Lpyy;

    invoke-direct {v0}, Lpyy;-><init>()V

    invoke-static {p0}, Lllp;->b(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lpyy;->a:Ljava/lang/Integer;

    invoke-static {p0}, Lllp;->c(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lpyy;->b:Ljava/lang/Integer;

    invoke-static {p0}, Lllp;->d(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lpyy;->c:Ljava/lang/Integer;

    invoke-static {p0}, Lllp;->a(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lpyu;->b:Ljava/lang/Integer;

    iput-object v0, v2, Lpyu;->f:Lpyy;

    iput-object p2, v2, Lpyu;->a:Ljava/lang/String;

    const-class v0, Lkfn;

    invoke-static {p0, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfn;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lkfn;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iput-object v0, v2, Lpyu;->d:Ljava/lang/String;

    :cond_0
    new-instance v0, Lpec;

    invoke-direct {v0}, Lpec;-><init>()V

    if-eqz p3, :cond_3

    const/16 v3, 0x64

    iput v3, v0, Lpec;->b:I

    :goto_1
    invoke-static {p0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v3}, Llsc;->b(Landroid/util/DisplayMetrics;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x3

    iput v3, v0, Lpec;->a:I

    :goto_2
    iput v4, v0, Lpec;->c:I

    iput p4, v0, Lpec;->d:I

    iput-object v0, v2, Lpyu;->e:Lpec;

    .line 70
    :try_start_0
    invoke-static {p1}, Lkgi;->a(Loxu;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 71
    new-instance v4, Llyq;

    invoke-direct {v4}, Llyq;-><init>()V

    .line 72
    iput-object v2, v4, Llyq;->b:Lpyu;

    .line 75
    if-eqz p0, :cond_1

    const-class v0, Lhxw;

    invoke-static {p0, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxw;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lhxw;->a()Ljava/lang/String;

    move-result-object v1

    :cond_1
    iput-object v1, v4, Llyq;->d:Ljava/lang/String;

    .line 77
    invoke-virtual {v3, p1, v4}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 83
    :goto_3
    return-void

    :cond_2
    move-object v0, v1

    .line 66
    goto :goto_0

    :cond_3
    invoke-static {p0}, Lhns;->a(Landroid/content/Context;)I

    move-result v3

    iput v3, v0, Lpec;->b:I

    goto :goto_1

    :cond_4
    iput v4, v0, Lpec;->a:I

    goto :goto_2

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v1, "PlusiUtils"

    const-string v2, "Failed to find apiHeader field on an http request, this should not happen"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 80
    :catch_1
    move-exception v0

    .line 81
    const-string v1, "PlusiUtils"

    const-string v2, "apiHeader field on http request was not accessible, this should not happen"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3
.end method
