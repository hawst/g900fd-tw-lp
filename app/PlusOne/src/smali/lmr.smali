.class final Llmr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llmq;


# instance fields
.field private a:Z

.field private synthetic b:Llmp;


# direct methods
.method constructor <init>(Llmp;)V
    .locals 1

    .prologue
    .line 89
    iput-object p1, p0, Llmr;->b:Llmp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Llmr;->a:Z

    return-void
.end method


# virtual methods
.method public a(I)Z
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Llmr;->b:Llmp;

    iget-object v1, v0, Llmp;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 131
    :try_start_0
    iget-object v0, p0, Llmr;->b:Llmp;

    iget v0, v0, Llmp;->c:I

    if-ge v0, p1, :cond_0

    iget-object v0, p0, Llmr;->b:Llmp;

    iget-boolean v0, v0, Llmp;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Llmr;->a:Z

    .line 126
    return-void
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Llmr;->a:Z

    return v0
.end method

.method public read(Ljava/nio/ByteBuffer;)I
    .locals 5

    .prologue
    .line 95
    iget-boolean v0, p0, Llmr;->a:Z

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 99
    :cond_0
    iget-object v0, p0, Llmr;->b:Llmp;

    iget-object v2, v0, Llmp;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 100
    :try_start_0
    iget-object v0, p0, Llmr;->b:Llmp;

    iget-object v0, v0, Llmp;->d:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 101
    if-eqz v0, :cond_2

    .line 102
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 103
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    .line 104
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 105
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 106
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 107
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 108
    iget-object v3, p0, Llmr;->b:Llmp;

    iget-object v3, v3, Llmp;->d:Ljava/util/Deque;

    invoke-interface {v3, v0}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 110
    :cond_1
    iget-object v0, p0, Llmr;->b:Llmp;

    iget-object v3, p0, Llmr;->b:Llmp;

    iget v3, v3, Llmp;->c:I

    sub-int/2addr v3, v1

    iput v3, v0, Llmp;->c:I

    .line 111
    monitor-exit v2

    move v0, v1

    .line 113
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Llmr;->b:Llmp;

    iget-object v0, v0, Llmp;->e:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    monitor-exit v2

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 113
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method
