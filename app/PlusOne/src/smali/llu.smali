.class public final Lllu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lllw;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lllw;

.field private final c:Lllx;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lllw;Lllx;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "entityName cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lllu;->a:Ljava/lang/String;

    .line 24
    const-string v0, "source cannot be null"

    invoke-static {p2, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllw;

    iput-object v0, p0, Lllu;->b:Lllw;

    .line 25
    const-string v0, "store cannot be null"

    invoke-static {p3, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllx;

    iput-object v0, p0, Lllu;->c:Lllx;

    .line 26
    invoke-interface {p2}, Lllw;->a()I

    move-result v0

    invoke-interface {p3}, Lllx;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "chunk size of store and the source must match"

    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 28
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lllu;->c:Lllx;

    invoke-interface {v0}, Lllx;->a()I

    move-result v0

    return v0
.end method

.method public a(I[B)I
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    if-ltz p1, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "chunkIndex must be non-negative"

    invoke-static {v0, v3}, Llsk;->a(ZLjava/lang/Object;)V

    .line 33
    array-length v0, p2

    iget-object v3, p0, Lllu;->c:Lllx;

    invoke-interface {v3}, Lllx;->a()I

    move-result v3

    if-lt v0, v3, :cond_2

    :goto_1
    const-string v0, "buffer must be at least chunkSizeBytes big"

    invoke-static {v1, v0}, Llsk;->a(ZLjava/lang/Object;)V

    .line 38
    :try_start_0
    iget-object v0, p0, Lllu;->c:Lllx;

    iget-object v1, p0, Lllu;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lllx;->b(Ljava/lang/String;I[B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 39
    if-lez v0, :cond_3

    move v2, v0

    .line 62
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    .line 32
    goto :goto_0

    :cond_2
    move v1, v2

    .line 33
    goto :goto_1

    .line 41
    :cond_3
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 46
    :goto_3
    iget-object v0, p0, Lllu;->b:Lllw;

    invoke-interface {v0, p1, p2}, Lllw;->a(I[B)I

    move-result v6

    .line 51
    iget-object v0, p0, Lllu;->c:Lllx;

    invoke-interface {v0}, Lllx;->a()I

    move-result v0

    if-ge v6, v0, :cond_4

    .line 53
    iget-object v0, p0, Lllu;->c:Lllx;

    iget-object v1, p0, Lllu;->a:Ljava/lang/String;

    iget-object v2, p0, Lllu;->c:Lllx;

    .line 57
    invoke-interface {v2}, Lllx;->a()I

    move-result v2

    mul-int/2addr v2, p1

    add-int/2addr v2, v6

    int-to-long v4, v2

    move v2, p1

    move-object v3, p2

    .line 53
    invoke-interface/range {v0 .. v5}, Lllx;->a(Ljava/lang/String;I[BJ)V

    :goto_4
    move v2, v6

    .line 62
    goto :goto_2

    .line 60
    :cond_4
    iget-object v0, p0, Lllu;->c:Lllx;

    iget-object v1, p0, Lllu;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lllx;->a(Ljava/lang/String;I[B)V

    goto :goto_4

    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method public a(II)Z
    .locals 3

    .prologue
    .line 86
    move v0, p1

    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_1

    .line 87
    iget-object v1, p0, Lllu;->c:Lllx;

    iget-object v2, p0, Lllu;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lllx;->a(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    iget-object v1, p0, Lllu;->b:Lllw;

    sub-int v2, v0, p1

    sub-int v2, p2, v2

    invoke-interface {v1, v0, v2}, Lllw;->a(II)Z

    move-result v0

    .line 91
    :goto_1
    return v0

    .line 86
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lllu;->b:Lllw;

    invoke-interface {v0}, Lllw;->b()V

    .line 82
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 73
    :try_start_0
    iget-object v0, p0, Lllu;->c:Lllx;

    invoke-interface {v0}, Lllx;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    iget-object v0, p0, Lllu;->b:Lllw;

    invoke-static {v0}, Llrz;->a(Ljava/io/Closeable;)V

    .line 76
    return-void

    .line 75
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lllu;->b:Lllw;

    invoke-static {v1}, Llrz;->a(Ljava/io/Closeable;)V

    throw v0
.end method
