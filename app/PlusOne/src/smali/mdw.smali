.class public final Lmdw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpez;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3643
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3646
    iput-object v0, p0, Lmdw;->apiHeader:Llyq;

    .line 3649
    iput-object v0, p0, Lmdw;->a:Lpez;

    .line 3643
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3666
    const/4 v0, 0x0

    .line 3667
    iget-object v1, p0, Lmdw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 3668
    const/4 v0, 0x1

    iget-object v1, p0, Lmdw;->apiHeader:Llyq;

    .line 3669
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3671
    :cond_0
    iget-object v1, p0, Lmdw;->a:Lpez;

    if-eqz v1, :cond_1

    .line 3672
    const/4 v1, 0x2

    iget-object v2, p0, Lmdw;->a:Lpez;

    .line 3673
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3675
    :cond_1
    iget-object v1, p0, Lmdw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3676
    iput v0, p0, Lmdw;->ai:I

    .line 3677
    return v0
.end method

.method public a(Loxn;)Lmdw;
    .locals 2

    .prologue
    .line 3685
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3686
    sparse-switch v0, :sswitch_data_0

    .line 3690
    iget-object v1, p0, Lmdw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3691
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdw;->ah:Ljava/util/List;

    .line 3694
    :cond_1
    iget-object v1, p0, Lmdw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3696
    :sswitch_0
    return-object p0

    .line 3701
    :sswitch_1
    iget-object v0, p0, Lmdw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 3702
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdw;->apiHeader:Llyq;

    .line 3704
    :cond_2
    iget-object v0, p0, Lmdw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3708
    :sswitch_2
    iget-object v0, p0, Lmdw;->a:Lpez;

    if-nez v0, :cond_3

    .line 3709
    new-instance v0, Lpez;

    invoke-direct {v0}, Lpez;-><init>()V

    iput-object v0, p0, Lmdw;->a:Lpez;

    .line 3711
    :cond_3
    iget-object v0, p0, Lmdw;->a:Lpez;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3686
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3654
    iget-object v0, p0, Lmdw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 3655
    const/4 v0, 0x1

    iget-object v1, p0, Lmdw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3657
    :cond_0
    iget-object v0, p0, Lmdw;->a:Lpez;

    if-eqz v0, :cond_1

    .line 3658
    const/4 v0, 0x2

    iget-object v1, p0, Lmdw;->a:Lpez;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3660
    :cond_1
    iget-object v0, p0, Lmdw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3662
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3639
    invoke-virtual {p0, p1}, Lmdw;->a(Loxn;)Lmdw;

    move-result-object v0

    return-object v0
.end method
