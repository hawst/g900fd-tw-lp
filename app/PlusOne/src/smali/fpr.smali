.class public final Lfpr;
.super Lu;
.source "PG"


# instance fields
.field private N:[Lfof;

.field private O:Lfof;

.field private P:Landroid/view/View;

.field private Q:Landroid/view/animation/Animation;

.field private R:Z

.field private S:I

.field private T:Landroid/content/Intent;

.field private U:Landroid/view/View;

.field private V:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lu;-><init>()V

    .line 53
    iput-boolean v0, p0, Lfpr;->R:Z

    .line 54
    iput v0, p0, Lfpr;->S:I

    .line 195
    return-void
.end method

.method private a(Lfof;II)V
    .locals 2

    .prologue
    .line 168
    invoke-virtual {p0}, Lfpr;->u_()Lu;

    move-result-object v0

    if-nez v0, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    iput p3, p0, Lfpr;->S:I

    .line 172
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lfpr;->T:Landroid/content/Intent;

    .line 173
    iget-object v0, p0, Lfpr;->T:Landroid/content/Intent;

    const-string v1, "stories_edit_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 175
    :cond_2
    iget-object v0, p0, Lfpr;->T:Landroid/content/Intent;

    const-string v1, "stories_edit_place"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic a(Lfpr;Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 35
    const v0, 0x7f100036

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfof;

    if-ne p2, v2, :cond_0

    iget-object v0, p0, Lfpr;->O:Lfof;

    :cond_0
    const/4 v1, 0x2

    if-eq p2, v1, :cond_1

    if-eq p2, v2, :cond_1

    iget-object v1, p0, Lfpr;->O:Lfof;

    invoke-virtual {v0, v1}, Lfof;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, -0x1

    invoke-direct {p0, v0, p2, v1}, Lfpr;->a(Lfof;II)V

    :goto_0
    return-void

    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lfpr;->a(Lfof;II)V

    goto :goto_0
.end method

.method static synthetic a(Lfpr;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lfpr;->R:Z

    return v0
.end method

.method static synthetic a(Lfpr;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lfpr;->R:Z

    return p1
.end method

.method static synthetic b(Lfpr;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfpr;->Q:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic c(Lfpr;)Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfpr;->P:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lfpr;)Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfpr;->U:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lfpr;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lfpr;->S:I

    return v0
.end method

.method static synthetic f(Lfpr;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfpr;->T:Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const-wide/16 v6, 0xfa

    const/4 v2, 0x0

    .line 73
    invoke-virtual {p0}, Lfpr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "stories_edit_place"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 74
    array-length v0, v3

    new-array v0, v0, [Lfof;

    iput-object v0, p0, Lfpr;->N:[Lfof;

    move v1, v2

    .line 75
    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 76
    iget-object v4, p0, Lfpr;->N:[Lfof;

    aget-object v0, v3, v1

    check-cast v0, Lfof;

    aput-object v0, v4, v1

    .line 75
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {p0}, Lfpr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "stories_current_place"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lfof;

    iput-object v0, p0, Lfpr;->O:Lfof;

    .line 79
    const v0, 0x7f040205

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfpr;->U:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lfpr;->U:Landroid/view/View;

    const v1, 0x7f1005da

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1005db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v3, Lfpw;

    iget-object v4, p0, Lfpr;->N:[Lfof;

    invoke-virtual {p0}, Lfpr;->n()Lz;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lfpw;-><init>([Lfof;Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v3, Lfps;

    invoke-direct {v3, p0}, Lfps;-><init>(Lfpr;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f1005dc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v3, Lfpt;

    invoke-direct {v3, p0}, Lfpt;-><init>(Lfpr;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lfpr;->n()Lz;

    move-result-object v0

    const v3, 0x7f050024

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lfpr;->Q:Landroid/view/animation/Animation;

    iget-object v0, p0, Lfpr;->Q:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lfpr;->n()Lz;

    move-result-object v3

    const v4, 0x7f050006

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    iget-object v0, p0, Lfpr;->Q:Landroid/view/animation/Animation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lfpr;->Q:Landroid/view/animation/Animation;

    new-instance v3, Lfpu;

    invoke-direct {v3, p0}, Lfpu;-><init>(Lfpr;)V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iput-object v1, p0, Lfpr;->P:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lfpr;->U:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lfpr;->U:Landroid/view/View;

    const v1, 0x7f1005d9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfpr;->V:Landroid/view/View;

    iget-object v0, p0, Lfpr;->V:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lfpr;->V:Landroid/view/View;

    new-instance v1, Lfpv;

    invoke-direct {v1, p0}, Lfpv;-><init>(Lfpr;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 84
    if-nez p3, :cond_1

    .line 85
    invoke-virtual {p0}, Lfpr;->n()Lz;

    move-result-object v0

    const v1, 0x7f050025

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 86
    invoke-virtual {p0}, Lfpr;->n()Lz;

    move-result-object v1

    const v2, 0x7f05000b

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    .line 87
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 88
    iget-object v1, p0, Lfpr;->P:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 91
    :cond_1
    iget-object v0, p0, Lfpr;->U:Landroid/view/View;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lfpr;->P:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpr;->Q:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lfpr;->P:Landroid/view/View;

    iget-object v1, p0, Lfpr;->Q:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 99
    :cond_0
    return-void
.end method
