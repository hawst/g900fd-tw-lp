.class public final Lkri;
.super Lu;
.source "PG"


# instance fields
.field private N:Lvp;

.field private O:Lvq;

.field private P:Ljava/lang/String;

.field private Q:Lvn;

.field private R:Landroid/support/v7/app/MediaRouteActionProvider;

.field private S:Liig;

.field private T:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lu;-><init>()V

    .line 36
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lkri;->e(Z)V

    .line 37
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p1}, Lu;->a(Landroid/app/Activity;)V

    .line 42
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Lu;->a(Landroid/os/Bundle;)V

    .line 48
    if-eqz p1, :cond_1

    .line 49
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lkri;->T:I

    .line 57
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lkri;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lvp;->a(Landroid/content/Context;)Lvp;

    move-result-object v0

    iput-object v0, p0, Lkri;->N:Lvp;

    .line 58
    new-instance v0, Lkrj;

    invoke-direct {v0}, Lkrj;-><init>()V

    iput-object v0, p0, Lkri;->O:Lvq;

    .line 59
    invoke-virtual {p0}, Lkri;->n()Lz;

    move-result-object v0

    const-class v1, Liig;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liig;

    iput-object v0, p0, Lkri;->S:Liig;

    .line 62
    return-void

    .line 51
    :cond_1
    invoke-virtual {p0}, Lkri;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    const-string v1, "account_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lkri;->T:I

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0, p1, p2}, Lu;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 102
    const v0, 0x7f120002

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 105
    const v0, 0x7f100670

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 107
    invoke-static {v0}, Lie;->b(Landroid/view/MenuItem;)Lhj;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteActionProvider;

    iput-object v0, p0, Lkri;->R:Landroid/support/v7/app/MediaRouteActionProvider;

    .line 110
    iget-object v0, p0, Lkri;->R:Landroid/support/v7/app/MediaRouteActionProvider;

    iget-object v1, p0, Lkri;->Q:Lvn;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteActionProvider;->a(Lvn;)V

    .line 111
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    const-string v0, "account_id"

    iget v1, p0, Lkri;->T:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 91
    invoke-super {p0, p1}, Lu;->e(Landroid/os/Bundle;)V

    .line 92
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 66
    invoke-super {p0}, Lu;->g()V

    .line 68
    invoke-virtual {p0}, Lkri;->n()Lz;

    move-result-object v0

    iget v1, p0, Lkri;->T:I

    invoke-static {v0, v1}, Lksd;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkri;->P:Ljava/lang/String;

    .line 70
    new-instance v0, Lvo;

    invoke-direct {v0}, Lvo;-><init>()V

    iget-object v1, p0, Lkri;->S:Liig;

    iget-object v2, p0, Lkri;->P:Ljava/lang/String;

    .line 71
    invoke-interface {v1, v2}, Liig;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lvo;->a(Ljava/lang/String;)Lvo;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lvo;->a()Lvn;

    move-result-object v0

    iput-object v0, p0, Lkri;->Q:Lvn;

    .line 72
    iget-object v0, p0, Lkri;->N:Lvp;

    iget-object v1, p0, Lkri;->Q:Lvn;

    iget-object v2, p0, Lkri;->O:Lvq;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lvp;->a(Lvn;Lvq;I)V

    .line 74
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    iput-object v2, p0, Lkri;->R:Landroid/support/v7/app/MediaRouteActionProvider;

    .line 81
    iget-object v0, p0, Lkri;->N:Lvp;

    iget-object v1, p0, Lkri;->O:Lvq;

    invoke-virtual {v0, v1}, Lvp;->a(Lvq;)V

    .line 82
    iput-object v2, p0, Lkri;->Q:Lvn;

    .line 83
    iput-object v2, p0, Lkri;->P:Ljava/lang/String;

    .line 85
    invoke-super {p0}, Lu;->h()V

    .line 86
    return-void
.end method
