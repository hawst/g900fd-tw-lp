.class final Ldfe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Lcpu;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Ldex;


# direct methods
.method constructor <init>(Ldex;)V
    .locals 0

    .prologue
    .line 1161
    iput-object p1, p0, Ldfe;->a:Ldex;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lcpu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1165
    iget-object v0, p0, Ldfe;->a:Ldex;

    invoke-virtual {v0}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "filter"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 1166
    new-instance v0, Lcpw;

    iget-object v1, p0, Ldfe;->a:Ldex;

    invoke-static {v1}, Ldex;->e(Ldex;)Llnl;

    move-result-object v1

    iget-object v2, p0, Ldfe;->a:Ldex;

    invoke-static {v2}, Ldex;->f(Ldex;)Lhee;

    move-result-object v2

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Ldfe;->a:Ldex;

    .line 1167
    invoke-virtual {v4}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "all_photos_row_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, p0, Ldfe;->a:Ldex;

    invoke-static {v5}, Ldex;->g(Ldex;)I

    move-result v5

    const/16 v6, 0x2710

    invoke-direct/range {v0 .. v7}, Lcpw;-><init>(Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/Long;III)V

    return-object v0
.end method

.method public a(Lcpu;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcpu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1174
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcpu;->a()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1175
    :goto_0
    iget-object v3, p0, Ldfe;->a:Ldex;

    if-nez v0, :cond_2

    .line 1176
    invoke-virtual {p1}, Lcpu;->b()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1177
    invoke-virtual {p1}, Lcpu;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v2, v1

    :goto_1
    if-nez v0, :cond_3

    .line 1178
    invoke-virtual {p1}, Lcpu;->c()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1179
    invoke-virtual {p1}, Lcpu;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_2
    new-instance v4, Lcod;

    invoke-direct {v4, p1}, Lcod;-><init>(Lcpu;)V

    .line 1175
    invoke-static {v3, v0, v2, v1, v4}, Ldex;->a(Ldex;ZIILhyo;)V

    .line 1181
    return-void

    .line 1174
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1177
    :cond_2
    const/4 v1, -0x1

    move v2, v1

    goto :goto_1

    .line 1179
    :cond_3
    iget-object v1, p0, Ldfe;->a:Ldex;

    invoke-static {v1}, Ldex;->g(Ldex;)I

    move-result v1

    goto :goto_2
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lcpu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1186
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1161
    check-cast p2, Lcpu;

    invoke-virtual {p0, p2}, Ldfe;->a(Lcpu;)V

    return-void
.end method
