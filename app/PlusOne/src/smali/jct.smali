.class public final Ljct;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/os/Bundle;

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private d:Ljdq;

.field private e:Ljava/lang/String;

.field private final f:Landroid/content/Context;

.field private final g:Lkfv;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljdg;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 452
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "uploadType"

    const-string v2, "resumable"

    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Ljct;->a:Landroid/os/Bundle;

    .line 460
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 462
    sput-object v0, Ljct;->b:Ljava/util/Set;

    const-string v1, "application/placeholder-image"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 463
    sget-object v0, Ljct;->b:Ljava/util/Set;

    const-string v1, "application/stitching-preview"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 467
    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Ljct;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjdg;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 489
    iput-object p1, p0, Ljct;->f:Landroid/content/Context;

    .line 490
    iput-object p3, p0, Ljct;->j:Ljdg;

    .line 491
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 492
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 494
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 495
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ljct;->h:Ljava/lang/String;

    .line 496
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ljct;->i:Ljava/lang/String;

    .line 497
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 499
    new-instance v1, Lkfv;

    const-string v2, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    invoke-direct {v1, p1, v0, v2}, Lkfv;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Ljct;->g:Lkfv;

    .line 506
    :goto_0
    return-void

    .line 502
    :cond_0
    iput-object v1, p0, Ljct;->h:Ljava/lang/String;

    .line 503
    iput-object v1, p0, Ljct;->i:Ljava/lang/String;

    .line 504
    iput-object v1, p0, Ljct;->g:Lkfv;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljdh;Ljava/lang/String;ZJLjava/lang/String;)Ljdb;
    .locals 21

    .prologue
    .line 858
    move-object/from16 v0, p0

    iget-object v6, v0, Ljct;->j:Ljdg;

    .line 859
    invoke-virtual/range {p2 .. p2}, Ljdh;->g()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Ljdh;->k()J

    move-result-wide v10

    move-wide/from16 v8, p5

    .line 858
    invoke-interface/range {v6 .. v11}, Ljdg;->a(Landroid/net/Uri;JJ)V

    .line 861
    const-string v6, "MediaUploader"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 862
    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x11

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "--- UPLOAD task: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 865
    :cond_0
    move-object/from16 v0, p2

    move-wide/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Ljdh;->a(J)Ljava/io/InputStream;

    move-result-object v16

    .line 869
    if-eqz p3, :cond_2

    .line 870
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljdh;->h()Ljbh;

    move-result-object v6

    invoke-virtual {v6}, Ljbh;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    new-instance v6, Ljcy;

    const-string v7, "Fingerprint mismatch"

    invoke-direct {v6, v7}, Ljcy;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 924
    :catch_0
    move-exception v6

    .line 925
    :try_start_1
    new-instance v7, Ljdc;

    .line 926
    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ljdd;->a(Ljdh;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v6, v8}, Ljdc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 928
    :catchall_0
    move-exception v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Ljct;->d:Ljdq;

    .line 929
    invoke-static/range {v16 .. v16}, Llrz;->a(Ljava/io/Closeable;)V

    throw v6

    .line 873
    :cond_2
    :try_start_2
    new-instance v18, Ljcu;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-wide/from16 v2, p5

    move-object/from16 v4, p2

    invoke-direct {v0, v1, v2, v3, v4}, Ljcu;-><init>(Ljct;JLjdh;)V

    .line 884
    new-instance v7, Ljdq;

    move-object/from16 v0, p0

    iget-object v8, v0, Ljct;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Ljct;->g:Lkfv;

    .line 885
    invoke-virtual/range {p2 .. p2}, Ljdh;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Ljdh;->k()J

    move-result-wide v14

    move-object/from16 v10, p1

    move-wide/from16 v12, p5

    move/from16 v17, p4

    move-object/from16 v19, p7

    invoke-direct/range {v7 .. v19}, Ljdq;-><init>(Landroid/content/Context;Lkfj;Ljava/lang/String;Ljava/lang/String;JJLjava/io/InputStream;ZLjdu;Ljava/lang/String;)V

    .line 888
    move-object/from16 v0, p0

    iput-object v7, v0, Ljct;->d:Ljdq;

    .line 890
    invoke-static {v7}, Ljct;->a(Ljdi;)Ljdi;

    .line 892
    invoke-virtual {v7}, Ljdq;->e()I

    move-result v6

    .line 893
    invoke-static {v6}, Ljct;->a(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 895
    invoke-virtual/range {p2 .. p2}, Ljdh;->k()J

    move-result-wide v8

    .line 896
    invoke-virtual/range {p2 .. p2}, Ljdh;->k()J

    move-result-wide v10

    .line 895
    move-object/from16 v0, v18

    invoke-interface {v0, v8, v9, v10, v11}, Ljdu;->a(JJ)V

    .line 897
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v7, v1}, Ljct;->a(Ljdi;Ljdh;)Ljdb;

    move-result-object v6

    .line 900
    const-wide/16 v8, 0x1

    invoke-static {v8, v9}, Ljdk;->b(J)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 928
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Ljct;->d:Ljdq;

    .line 929
    invoke-static/range {v16 .. v16}, Llrz;->a(Ljava/io/Closeable;)V

    return-object v6

    .line 905
    :cond_3
    :try_start_3
    invoke-static {v6}, Ljct;->b(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 906
    new-instance v6, Ljdf;

    const-string v7, "uploaded full stream but server returned incomplete"

    invoke-direct {v6, v7}, Ljdf;-><init>(Ljava/lang/String;)V

    throw v6

    .line 907
    :cond_4
    const/16 v8, 0x190

    if-ne v6, v8, :cond_5

    .line 911
    new-instance v7, Ljdf;

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x37

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "upload failed (bad payload, file too large) "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Ljdf;-><init>(Ljava/lang/String;)V

    throw v7

    .line 912
    :cond_5
    const/16 v8, 0x1f4

    if-lt v6, v8, :cond_6

    const/16 v8, 0x258

    if-ge v6, v8, :cond_6

    .line 915
    new-instance v7, Ljdc;

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x21

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "upload transient error"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 916
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ljdd;->a(Ljdh;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v6, v8}, Ljdc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 918
    :cond_6
    invoke-virtual {v7}, Ljdq;->h()Ljava/io/IOException;

    move-result-object v8

    if-nez v8, :cond_7

    invoke-virtual {v7}, Ljdq;->i()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 919
    :cond_7
    new-instance v6, Ljdc;

    .line 920
    invoke-virtual {v7}, Ljdq;->h()Ljava/io/IOException;

    move-result-object v7

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ljdd;->a(Ljdh;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljdc;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    throw v6

    .line 922
    :cond_8
    new-instance v7, Ljdf;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Ljdf;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method private a(Ljdi;Ljdh;)Ljdb;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 935
    if-nez p1, :cond_0

    new-instance v0, Ljdf;

    const-string v1, "null HttpEntity in response"

    invoke-direct {v0, v1}, Ljdf;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v1, "MediaUploader"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Ljdi;->f()J

    move-result-wide v2

    long-to-int v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "parseResult: length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p1}, Ljdi;->g()[B

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Loxn;->a([BII)Loxn;

    move-result-object v1

    new-instance v2, Lmkz;

    invoke-direct {v2}, Lmkz;-><init>()V

    invoke-virtual {v2, v1}, Lmkz;->a(Loxn;)Lmkz;

    iget-object v2, v2, Lmkz;->a:Lncn;

    .line 936
    if-nez v2, :cond_2

    .line 937
    new-instance v0, Ljdf;

    const-string v1, "Unable to parse UploadMediaResponse"

    invoke-direct {v0, v1}, Ljdf;-><init>(Ljava/lang/String;)V

    throw v0

    .line 939
    :cond_2
    if-eqz v2, :cond_3

    iget-object v1, v2, Lncn;->b:Lnyt;

    if-nez v1, :cond_5

    :cond_3
    move-object v1, v0

    .line 940
    :goto_0
    iget-object v5, v2, Lncn;->a:Lnym;

    .line 941
    iget-object v2, v5, Lnym;->b:Lnyl;

    if-eqz v2, :cond_6

    iget-object v0, v5, Lnym;->b:Lnyl;

    iget-object v2, v0, Lnyl;->b:Ljava/lang/String;

    .line 942
    :goto_1
    iget-object v0, v5, Lnym;->l:Lnyb;

    if-eqz v0, :cond_4

    iget-object v0, v5, Lnym;->l:Lnyb;

    iget-object v0, v0, Lnyb;->d:Ljava/lang/String;

    .line 943
    :cond_4
    iget-object v0, v5, Lnym;->n:Ljava/lang/Double;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v6

    const-wide v8, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v8

    double-to-long v3, v6

    .line 944
    new-instance v0, Ljdb;

    iget-object v5, v5, Lnym;->e:Ljava/lang/String;

    .line 945
    invoke-virtual {p2}, Ljdh;->k()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Ljdb;-><init>(Ljdn;Ljava/lang/String;JLjava/lang/String;J)V

    return-object v0

    .line 939
    :cond_5
    iget-object v1, v2, Lncn;->b:Lnyt;

    invoke-static {v1}, Ljdn;->a(Lnyt;)Ljdn;

    move-result-object v1

    goto :goto_0

    :cond_6
    move-object v2, v0

    .line 941
    goto :goto_1
.end method

.method static synthetic a(Ljct;)Ljdg;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Ljct;->j:Ljdg;

    return-object v0
.end method

.method private static a(Ljdi;)Ljdi;
    .locals 4

    .prologue
    .line 825
    invoke-virtual {p0}, Ljdi;->a()V

    .line 826
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 827
    invoke-virtual {p0}, Ljdi;->d()V

    .line 828
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljdk;->a(J)V

    .line 831
    invoke-virtual {p0}, Ljdi;->e()I

    move-result v0

    .line 832
    const/16 v1, 0x191

    if-eq v0, v1, :cond_0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_1

    .line 835
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljdi;->j()Lkfj;

    move-result-object v0

    invoke-interface {v0}, Lkfj;->a()V

    .line 836
    invoke-virtual {p0}, Ljdi;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 844
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 848
    invoke-virtual {p0}, Ljdi;->d()V

    .line 849
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljdk;->a(J)V

    .line 851
    :cond_1
    return-object p0

    .line 837
    :catch_0
    move-exception v0

    .line 838
    new-instance v1, Ljde;

    invoke-direct {v1, v0}, Ljde;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Landroid/database/Cursor;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 49
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljcw;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No content for URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljcw;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 984
    const/16 v0, 0xc8

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 988
    const/16 v0, 0x134

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljdb;
    .locals 1

    .prologue
    .line 677
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Ljct;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljdb;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljdb;
    .locals 9

    .prologue
    const/16 v6, 0x191

    .line 688
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljdd;

    const-string v1, "resumeUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resumeFingerprint"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "resumeForceResize"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "resumeContentType"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v1, v2, v3, v0}, Ljdd;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 692
    new-instance v0, Ljdh;

    iget-object v1, p0, Ljct;->f:Landroid/content/Context;

    .line 693
    invoke-virtual {v8}, Ljdd;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Ljdh;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lopf;)V

    .line 695
    invoke-virtual {v8}, Ljdd;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 696
    invoke-virtual {v0}, Ljdh;->a()V

    .line 698
    :cond_0
    new-instance v1, Ljdo;

    iget-object v2, p0, Ljct;->f:Landroid/content/Context;

    iget-object v3, p0, Ljct;->g:Lkfv;

    invoke-virtual {v8}, Ljdd;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Ljdo;-><init>(Landroid/content/Context;Lkfj;Ljava/lang/String;)V

    .line 700
    :try_start_1
    invoke-static {v1}, Ljct;->a(Ljdi;)Ljdi;

    .line 702
    invoke-virtual {v1}, Ljdo;->e()I

    move-result v2

    .line 703
    invoke-static {v2}, Ljct;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 704
    invoke-direct {p0, v1, v0}, Ljct;->a(Ljdi;Ljdh;)Ljdb;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 715
    :goto_0
    return-object v0

    .line 690
    :catch_0
    move-exception v0

    new-instance v0, Ljdc;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed decoding resume token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljdc;-><init>(Ljava/lang/String;)V

    throw v0

    .line 708
    :cond_1
    :try_start_2
    invoke-static {v2}, Ljct;->b(I)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v1}, Ljdo;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 710
    invoke-virtual {v1}, Ljdo;->c()Ljava/lang/String;

    move-result-object v1

    .line 711
    if-eqz v1, :cond_2

    sget-object v2, Ljct;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long v6, v2, v4

    .line 712
    :goto_1
    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-gez v2, :cond_4

    .line 713
    new-instance v2, Ljdc;

    const-string v3, "negative range offset: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v2, v0}, Ljdc;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 724
    :catch_1
    move-exception v0

    .line 725
    new-instance v1, Ljdc;

    invoke-direct {v1, v0, p3}, Ljdc;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    throw v1

    .line 711
    :cond_2
    const-wide/16 v6, -0x1

    goto :goto_1

    .line 713
    :cond_3
    :try_start_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 715
    :cond_4
    invoke-virtual {v8}, Ljdd;->a()Ljava/lang/String;

    move-result-object v2

    .line 716
    invoke-virtual {v8}, Ljdd;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, v0

    move-object v8, p4

    .line 715
    invoke-direct/range {v1 .. v8}, Ljct;->a(Ljava/lang/String;Ljdh;Ljava/lang/String;ZJLjava/lang/String;)Ljdb;

    move-result-object v0

    goto/16 :goto_0

    .line 717
    :cond_5
    if-ne v2, v6, :cond_6

    .line 720
    new-instance v0, Ljde;

    const/16 v1, 0x191

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljde;-><init>(Ljava/lang/String;)V

    throw v0

    .line 722
    :cond_6
    new-instance v0, Ljdc;

    invoke-virtual {v1}, Ljdo;->e()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unexpected response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljdc;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLopf;)Ljdb;
    .locals 11

    .prologue
    .line 624
    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v10}, Ljct;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLopf;Ljava/lang/String;)Ljdb;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLopf;Ljava/lang/String;)Ljdb;
    .locals 14

    .prologue
    .line 651
    const/4 v6, 0x0

    .line 653
    :try_start_0
    new-instance v0, Ljdh;

    iget-object v1, p0, Ljct;->f:Landroid/content/Context;

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Ljdh;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lopf;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 654
    if-nez p6, :cond_0

    :try_start_1
    iget-object v1, p0, Ljct;->f:Landroid/content/Context;

    invoke-static {v1, p1}, Ljdv;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 655
    invoke-virtual {v0}, Ljdh;->a()V

    .line 657
    :cond_0
    invoke-virtual {v0}, Ljdh;->e()Ljava/lang/String;

    move-result-object v2

    sget-object v1, Ljct;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljcv;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ljcv;-><init>(Ljava/lang/String;Z)V

    throw v1
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 659
    :catch_0
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    .line 660
    :goto_0
    :try_start_2
    new-instance v2, Ljcx;

    invoke-direct {v2, v0}, Ljcx;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 664
    :catchall_0
    move-exception v0

    move-object v6, v1

    .line 665
    :goto_1
    if-eqz v6, :cond_1

    :try_start_3
    invoke-virtual {v6}, Ljdh;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 666
    new-instance v1, Ljava/io/File;

    invoke-virtual {v6}, Ljdh;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 670
    :cond_1
    :goto_2
    throw v0

    .line 657
    :cond_2
    if-eqz v2, :cond_4

    :try_start_4
    const-string v1, "image/"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "video/"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_5

    new-instance v1, Ljcv;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljcv;-><init>(Ljava/lang/String;Z)V

    throw v1
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 661
    :catch_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    .line 662
    :goto_4
    :try_start_5
    new-instance v1, Ljdc;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljdc;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 664
    :catchall_1
    move-exception v0

    goto :goto_1

    .line 657
    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    .line 658
    :cond_5
    :try_start_6
    iget-object v1, p0, Ljct;->e:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Ljct;->e:Ljava/lang/String;

    :goto_5
    iget-object v2, p0, Ljct;->f:Landroid/content/Context;

    const-string v3, "plusi"

    const/4 v4, 0x1

    sget-object v5, Ljct;->a:Landroid/os/Bundle;

    invoke-static {v2, v3, v1, v4, v5}, Lkfy;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljcr;

    iget-object v2, p0, Ljct;->f:Landroid/content/Context;

    iget-object v3, p0, Ljct;->g:Lkfv;

    iget-object v10, p0, Ljct;->i:Ljava/lang/String;

    iget-object v11, p0, Ljct;->h:Ljava/lang/String;

    move-object v5, v0

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p8

    move/from16 v9, p7

    move-object/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Ljcr;-><init>(Landroid/content/Context;Lkfj;Ljava/lang/String;Ljdh;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Ljct;->a(Ljdi;)Ljdi;

    invoke-virtual {v1}, Ljcr;->e()I

    move-result v2

    invoke-static {v2}, Ljct;->a(I)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v1}, Ljcr;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-object v3, v0

    move/from16 v5, p8

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, Ljct;->a(Ljava/lang/String;Ljdh;Ljava/lang/String;ZJLjava/lang/String;)Ljdb;
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v1

    .line 665
    :try_start_7
    invoke-virtual {v0}, Ljdh;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 666
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, Ljdh;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 670
    :cond_6
    :goto_6
    return-object v1

    .line 658
    :cond_7
    if-eqz p8, :cond_8

    :try_start_8
    const-string v1, "uploadmediabackground"

    goto :goto_5

    :cond_8
    const-string v1, "uploadmedia"

    goto :goto_5

    :cond_9
    const/16 v1, 0x190

    if-ne v2, v1, :cond_a

    new-instance v1, Ljdf;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x37

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "upload failed (bad payload, file too large) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljdf;-><init>(Ljava/lang/String;)V

    throw v1

    .line 664
    :catchall_2
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto/16 :goto_1

    .line 658
    :cond_a
    const/16 v1, 0x191

    if-ne v2, v1, :cond_b

    new-instance v1, Ljde;

    const/16 v2, 0x191

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljde;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    if-eqz v2, :cond_c

    const/16 v1, 0x1f4

    if-lt v2, v1, :cond_e

    const/16 v1, 0x258

    if-ge v2, v1, :cond_e

    :cond_c
    const/16 v1, 0x1f7

    if-ne v2, v1, :cond_d

    new-instance v1, Ljdc;

    const-string v2, "Server throttle code 503"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljdc;-><init>(Ljava/lang/String;Z)V

    :goto_7
    throw v1

    :cond_d
    new-instance v1, Ljdc;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x22

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "upload transient error:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljdc;-><init>(Ljava/lang/String;)V

    goto :goto_7

    :cond_e
    new-instance v1, Ljdf;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljdf;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catch_2
    move-exception v1

    goto/16 :goto_2

    .line 661
    :catch_3
    move-exception v0

    goto/16 :goto_4

    .line 659
    :catch_4
    move-exception v0

    move-object v1, v6

    goto/16 :goto_0

    :catch_5
    move-exception v0

    goto/16 :goto_6
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Ljdb;
    .locals 10

    .prologue
    .line 599
    const/4 v2, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v0 .. v9}, Ljct;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLopf;)Ljdb;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;Z)Ljdb;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 562
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move v5, p3

    move v7, v6

    invoke-virtual/range {v0 .. v7}, Ljct;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Ljdb;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 733
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ljct;->d:Ljdq;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Ljct;->d:Ljdq;

    invoke-virtual {v0}, Ljdq;->c()V

    .line 735
    const/4 v0, 0x0

    iput-object v0, p0, Ljct;->d:Ljdq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 737
    :cond_0
    monitor-exit p0

    return-void

    .line 733
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 753
    iput-object p1, p0, Ljct;->e:Ljava/lang/String;

    .line 754
    return-void
.end method
