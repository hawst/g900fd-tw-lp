.class public final Lhwo;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lhvo;
.implements Lljh;


# static fields
.field private static j:Landroid/view/animation/Interpolator;


# instance fields
.field private final a:Lhve;

.field private b:Lhwg;

.field private c:Lhwh;

.field private d:Lhvu;

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/libraries/social/avatars/ui/AvatarView;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lhwo;->j:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-virtual {p0}, Lhwo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhve;->a(Landroid/content/Context;)Lhve;

    move-result-object v0

    iput-object v0, p0, Lhwo;->a:Lhve;

    .line 98
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhwo;->setWillNotDraw(Z)V

    .line 83
    return-void
.end method

.method private a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 372
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(II)I
    .locals 4

    .prologue
    .line 357
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    if-lt p1, p2, :cond_1

    .line 358
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x4c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "avatarIndex is out of range. avatarIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", numAvatars="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_1
    iget v0, p0, Lhwo;->f:I

    iget-object v1, p0, Lhwo;->a:Lhve;

    iget v1, v1, Lhve;->d:I

    sub-int/2addr v0, v1

    .line 362
    sub-int v1, p2, p1

    .line 363
    invoke-virtual {p0}, Lhwo;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lhss;->a(Landroid/content/Context;)I

    move-result v2

    mul-int/2addr v2, v1

    add-int/lit8 v1, v1, -0x1

    iget-object v3, p0, Lhwo;->a:Lhve;

    iget v3, v3, Lhve;->f:I

    mul-int/2addr v1, v3

    add-int/2addr v1, v2

    .line 365
    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 468
    :cond_0
    iget-object v0, p0, Lhwo;->b:Lhwg;

    if-eqz v0, :cond_1

    .line 469
    iget-object v0, p0, Lhwo;->b:Lhwg;

    invoke-virtual {v0}, Lhwg;->a()V

    .line 471
    :cond_1
    return-void
.end method

.method public a(II)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 325
    if-eqz p2, :cond_0

    iget v0, p0, Lhwo;->g:I

    if-ne p2, v0, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, p2, :cond_2

    .line 327
    :cond_0
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 347
    :cond_1
    :goto_0
    return-void

    .line 335
    :cond_2
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 337
    invoke-direct {p0, p1, p2}, Lhwo;->b(II)I

    move-result v0

    .line 338
    iget-object v1, p0, Lhwo;->i:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 339
    iget-object v1, p0, Lhwo;->i:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 340
    iget-object v1, p0, Lhwo;->i:Landroid/widget/ImageView;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setX(F)V

    .line 341
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 342
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x190

    .line 343
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lhwo;->j:Landroid/view/animation/Interpolator;

    .line 344
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 345
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method public a(Lhuv;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 113
    invoke-virtual {p0, v5}, Lhwo;->setVisibility(I)V

    iget-object v0, p0, Lhwo;->b:Lhwg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwo;->b:Lhwg;

    invoke-virtual {p0, v0}, Lhwo;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lhwo;->c:Lhwh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhwo;->c:Lhwh;

    invoke-virtual {p0, v0}, Lhwo;->removeView(Landroid/view/View;)V

    :cond_1
    iput v2, p0, Lhwo;->e:I

    iget-object v0, p0, Lhwo;->d:Lhvu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhwo;->d:Lhvu;

    invoke-virtual {p0, v0}, Lhwo;->removeView(Landroid/view/View;)V

    :cond_2
    iput v2, p0, Lhwo;->f:I

    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lhwo;->removeView(Landroid/view/View;)V

    :cond_3
    move v1, v2

    :goto_0
    iget v0, p0, Lhwo;->g:I

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lhwo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lhwo;->removeView(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    iput v2, p0, Lhwo;->g:I

    .line 115
    if-nez p1, :cond_5

    .line 195
    :goto_1
    return-void

    .line 119
    :cond_5
    invoke-virtual {p0}, Lhwo;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 121
    invoke-virtual {p1}, Lhuv;->a()Lhus;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 122
    iget-object v0, p0, Lhwo;->b:Lhwg;

    if-nez v0, :cond_6

    .line 123
    new-instance v0, Lhwg;

    invoke-direct {v0, v3}, Lhwg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwo;->b:Lhwg;

    .line 125
    :cond_6
    iget-object v0, p0, Lhwo;->b:Lhwg;

    invoke-virtual {p1}, Lhuv;->a()Lhus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhwg;->a(Lhus;)V

    .line 127
    iget-object v0, p0, Lhwo;->b:Lhwg;

    invoke-virtual {p0, v0}, Lhwo;->addView(Landroid/view/View;)V

    .line 130
    :cond_7
    invoke-virtual {p1}, Lhuv;->b()Lhuu;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 131
    iget-object v0, p0, Lhwo;->c:Lhwh;

    if-nez v0, :cond_8

    .line 132
    new-instance v0, Lhwh;

    invoke-direct {v0, v3}, Lhwh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwo;->c:Lhwh;

    .line 134
    :cond_8
    iget-object v0, p0, Lhwo;->c:Lhwh;

    invoke-virtual {p1}, Lhuv;->b()Lhuu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhwh;->a(Lhuu;)V

    .line 136
    iget-object v0, p0, Lhwo;->c:Lhwh;

    invoke-virtual {p0, v0}, Lhwo;->addView(Landroid/view/View;)V

    .line 139
    :cond_9
    iput v2, p0, Lhwo;->g:I

    .line 140
    invoke-virtual {p1}, Lhuv;->c()Lhum;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 141
    iget-object v0, p0, Lhwo;->d:Lhvu;

    if-nez v0, :cond_a

    .line 142
    new-instance v0, Lhvu;

    invoke-direct {v0, v3}, Lhvu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwo;->d:Lhvu;

    .line 144
    :cond_a
    iget-object v0, p0, Lhwo;->d:Lhvu;

    invoke-virtual {p1}, Lhuv;->c()Lhum;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhvu;->a(Lhum;)V

    .line 146
    iget-object v0, p0, Lhwo;->d:Lhvu;

    invoke-virtual {p0, v0}, Lhwo;->addView(Landroid/view/View;)V

    .line 149
    :cond_b
    invoke-virtual {p1}, Lhuv;->d()I

    move-result v0

    iput v0, p0, Lhwo;->g:I

    .line 151
    iget v0, p0, Lhwo;->g:I

    if-lez v0, :cond_10

    .line 152
    iget v0, p0, Lhwo;->g:I

    .line 153
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 152
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lhwo;->g:I

    .line 155
    iget-object v0, p0, Lhwo;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_c

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lhwo;->g:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lhwo;->h:Ljava/util/ArrayList;

    :goto_2
    move v1, v2

    .line 161
    :goto_3
    iget v0, p0, Lhwo;->g:I

    if-ge v1, v0, :cond_e

    .line 166
    iget-object v0, p0, Lhwo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_d

    .line 167
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lhwo;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    .line 168
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 169
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 171
    iget-object v4, p0, Lhwo;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 176
    :goto_4
    invoke-virtual {p1, v1}, Lhuv;->a(I)Lhuh;

    .line 178
    invoke-virtual {v0, v6, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0, v0}, Lhwo;->addView(Landroid/view/View;)V

    .line 161
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 158
    :cond_c
    iget-object v0, p0, Lhwo;->h:Ljava/util/ArrayList;

    iget v1, p0, Lhwo;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    goto :goto_2

    .line 173
    :cond_d
    iget-object v0, p0, Lhwo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    goto :goto_4

    .line 182
    :cond_e
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    if-nez v0, :cond_f

    .line 183
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    .line 184
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    const v1, 0x7f0200cf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 189
    :cond_f
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 191
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lhwo;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 193
    :cond_10
    iput v2, p0, Lhwo;->g:I

    goto/16 :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 259
    iget-object v0, p0, Lhwo;->a:Lhve;

    iget v0, v0, Lhve;->c:I

    .line 260
    iget-object v1, p0, Lhwo;->a:Lhve;

    iget v1, v1, Lhve;->j:I

    .line 261
    iput v1, p0, Lhwo;->e:I

    .line 263
    iget-object v2, p0, Lhwo;->b:Lhwg;

    invoke-direct {p0, v2}, Lhwo;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 264
    iget-object v2, p0, Lhwo;->b:Lhwg;

    iget v3, p0, Lhwo;->e:I

    iget v4, p0, Lhwo;->e:I

    iget-object v5, p0, Lhwo;->b:Lhwg;

    .line 265
    invoke-virtual {v5}, Lhwg;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lhwo;->b:Lhwg;

    .line 266
    invoke-virtual {v5}, Lhwg;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    .line 264
    invoke-virtual {v2, v3, v0, v4, v5}, Lhwg;->layout(IIII)V

    .line 267
    iget v2, p0, Lhwo;->e:I

    iget-object v3, p0, Lhwo;->b:Lhwg;

    invoke-virtual {v3}, Lhwg;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lhwo;->a:Lhve;

    iget v4, v4, Lhve;->j:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, p0, Lhwo;->e:I

    .line 270
    :cond_0
    iget-object v2, p0, Lhwo;->c:Lhwh;

    invoke-direct {p0, v2}, Lhwo;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 271
    iget-object v2, p0, Lhwo;->c:Lhwh;

    iget v3, p0, Lhwo;->e:I

    iget v4, p0, Lhwo;->e:I

    iget-object v5, p0, Lhwo;->c:Lhwh;

    .line 272
    invoke-virtual {v5}, Lhwh;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lhwo;->c:Lhwh;

    .line 273
    invoke-virtual {v5}, Lhwh;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    .line 271
    invoke-virtual {v2, v3, v0, v4, v5}, Lhwh;->layout(IIII)V

    .line 274
    iget v2, p0, Lhwo;->e:I

    iget-object v3, p0, Lhwo;->c:Lhwh;

    invoke-virtual {v3}, Lhwh;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lhwo;->a:Lhve;

    iget v4, v4, Lhve;->j:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, p0, Lhwo;->e:I

    .line 277
    :cond_1
    invoke-virtual {p0}, Lhwo;->getMeasuredWidth()I

    move-result v2

    sub-int v1, v2, v1

    .line 278
    iput v1, p0, Lhwo;->f:I

    .line 280
    iget-object v2, p0, Lhwo;->d:Lhvu;

    invoke-direct {p0, v2}, Lhwo;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 281
    iget v2, p0, Lhwo;->f:I

    iget-object v3, p0, Lhwo;->d:Lhvu;

    invoke-virtual {v3}, Lhvu;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lhwo;->f:I

    .line 282
    iget-object v2, p0, Lhwo;->d:Lhvu;

    iget v3, p0, Lhwo;->f:I

    iget-object v4, p0, Lhwo;->d:Lhvu;

    .line 283
    invoke-virtual {v4}, Lhvu;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 282
    invoke-virtual {v2, v3, v0, v1, v4}, Lhvu;->layout(IIII)V

    .line 286
    :cond_2
    iget v0, p0, Lhwo;->g:I

    if-lez v0, :cond_3

    .line 287
    const/4 v0, 0x0

    iget v1, p0, Lhwo;->g:I

    invoke-direct {p0, v0, v1}, Lhwo;->b(II)I

    move-result v0

    .line 288
    iget-object v1, p0, Lhwo;->i:Landroid/widget/ImageView;

    iget-object v2, p0, Lhwo;->a:Lhve;

    iget v2, v2, Lhve;->e:I

    iget-object v3, p0, Lhwo;->i:Landroid/widget/ImageView;

    .line 289
    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lhwo;->a:Lhve;

    iget v4, v4, Lhve;->e:I

    iget-object v5, p0, Lhwo;->i:Landroid/widget/ImageView;

    .line 291
    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 288
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 294
    iget v0, p0, Lhwo;->g:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lhwo;->g:I

    .line 295
    invoke-direct {p0, v0, v1}, Lhwo;->b(II)I

    move-result v1

    .line 297
    invoke-virtual {p0}, Lhwo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->a(Landroid/content/Context;)I

    move-result v3

    .line 300
    iget v0, p0, Lhwo;->g:I

    add-int/lit8 v0, v0, -0x1

    move v2, v1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    .line 301
    iget-object v0, p0, Lhwo;->a:Lhve;

    iget v4, v0, Lhve;->e:I

    .line 302
    add-int v5, v4, v3

    .line 304
    iget-object v0, p0, Lhwo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    add-int v6, v2, v3

    invoke-virtual {v0, v2, v4, v6, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 307
    iget-object v0, p0, Lhwo;->a:Lhve;

    iget v0, v0, Lhve;->f:I

    add-int/2addr v0, v3

    sub-int/2addr v2, v0

    .line 300
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 310
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 206
    invoke-virtual {p0}, Lhwo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lhvj;->a(Landroid/content/Context;I)I

    move-result v4

    .line 210
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 212
    iget-object v0, p0, Lhwo;->b:Lhwg;

    invoke-direct {p0, v0}, Lhwo;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 213
    iget-object v0, p0, Lhwo;->b:Lhwg;

    invoke-virtual {v0, v5, v5}, Lhwg;->measure(II)V

    .line 214
    iget-object v0, p0, Lhwo;->b:Lhwg;

    invoke-virtual {v0}, Lhwg;->getMeasuredHeight()I

    move-result v0

    .line 217
    :goto_0
    iget-object v2, p0, Lhwo;->c:Lhwh;

    invoke-direct {p0, v2}, Lhwo;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    iget-object v2, p0, Lhwo;->c:Lhwh;

    invoke-virtual {v2, v5, v5}, Lhwh;->measure(II)V

    .line 219
    iget-object v2, p0, Lhwo;->c:Lhwh;

    invoke-virtual {v2}, Lhwh;->getMeasuredHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 222
    :cond_0
    iget-object v2, p0, Lhwo;->d:Lhvu;

    invoke-direct {p0, v2}, Lhwo;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 223
    iget-object v2, p0, Lhwo;->d:Lhvu;

    invoke-virtual {v2, v5, v5}, Lhvu;->measure(II)V

    .line 224
    iget-object v2, p0, Lhwo;->d:Lhvu;

    invoke-virtual {v2}, Lhvu;->getMeasuredHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v2, v0

    .line 227
    :goto_1
    iget v0, p0, Lhwo;->g:I

    if-lez v0, :cond_3

    move v3, v1

    .line 229
    :goto_2
    iget v0, p0, Lhwo;->g:I

    if-ge v3, v0, :cond_1

    .line 230
    iget-object v0, p0, Lhwo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v5, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 229
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 234
    :cond_1
    invoke-virtual {p0}, Lhwo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->a(Landroid/content/Context;)I

    move-result v0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 236
    iget-object v3, p0, Lhwo;->i:Landroid/widget/ImageView;

    invoke-virtual {v3, v0, v0}, Landroid/widget/ImageView;->measure(II)V

    .line 237
    iget-object v0, p0, Lhwo;->i:Landroid/widget/ImageView;

    .line 238
    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 241
    :goto_3
    if-lez v0, :cond_2

    .line 242
    iget-object v2, p0, Lhwo;->a:Lhve;

    iget v2, v2, Lhve;->c:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 243
    invoke-virtual {p0, v1}, Lhwo;->setVisibility(I)V

    .line 246
    :cond_2
    invoke-virtual {p0, v4, v0}, Lhwo;->setMeasuredDimension(II)V

    .line 247
    return-void

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v2, v0

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method
