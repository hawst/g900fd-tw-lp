.class public Lhkr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llrf;
.implements Llrg;


# instance fields
.field private a:Landroid/view/View;

.field private final b:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lhkr;->b:Landroid/app/Activity;

    .line 29
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 30
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 53
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkr;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkr;->a:Landroid/view/View;

    .line 54
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lhkr;->b:Landroid/app/Activity;

    iget-object v1, p0, Lhkr;->a:Landroid/view/View;

    iget-object v2, p0, Lhkr;->a:Landroid/view/View;

    .line 56
    invoke-virtual {v2}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-static {v0, v1, v2}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/app/ActivityOptions;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 39
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const-string v0, "shared_view_name"

    invoke-virtual {p1, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 43
    :cond_0
    iput-object p1, p0, Lhkr;->a:Landroid/view/View;

    .line 44
    return-void
.end method

.method public aP_()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lhkr;->a:Landroid/view/View;

    .line 64
    return-void
.end method
