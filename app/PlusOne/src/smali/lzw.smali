.class public final Llzw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnux;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7369
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7372
    iput-object v0, p0, Llzw;->apiHeader:Llyq;

    .line 7375
    iput-object v0, p0, Llzw;->a:Lnux;

    .line 7369
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7392
    const/4 v0, 0x0

    .line 7393
    iget-object v1, p0, Llzw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 7394
    const/4 v0, 0x1

    iget-object v1, p0, Llzw;->apiHeader:Llyq;

    .line 7395
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7397
    :cond_0
    iget-object v1, p0, Llzw;->a:Lnux;

    if-eqz v1, :cond_1

    .line 7398
    const/4 v1, 0x2

    iget-object v2, p0, Llzw;->a:Lnux;

    .line 7399
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7401
    :cond_1
    iget-object v1, p0, Llzw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7402
    iput v0, p0, Llzw;->ai:I

    .line 7403
    return v0
.end method

.method public a(Loxn;)Llzw;
    .locals 2

    .prologue
    .line 7411
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7412
    sparse-switch v0, :sswitch_data_0

    .line 7416
    iget-object v1, p0, Llzw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7417
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzw;->ah:Ljava/util/List;

    .line 7420
    :cond_1
    iget-object v1, p0, Llzw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7422
    :sswitch_0
    return-object p0

    .line 7427
    :sswitch_1
    iget-object v0, p0, Llzw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 7428
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzw;->apiHeader:Llyq;

    .line 7430
    :cond_2
    iget-object v0, p0, Llzw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7434
    :sswitch_2
    iget-object v0, p0, Llzw;->a:Lnux;

    if-nez v0, :cond_3

    .line 7435
    new-instance v0, Lnux;

    invoke-direct {v0}, Lnux;-><init>()V

    iput-object v0, p0, Llzw;->a:Lnux;

    .line 7437
    :cond_3
    iget-object v0, p0, Llzw;->a:Lnux;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7412
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7380
    iget-object v0, p0, Llzw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 7381
    const/4 v0, 0x1

    iget-object v1, p0, Llzw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7383
    :cond_0
    iget-object v0, p0, Llzw;->a:Lnux;

    if-eqz v0, :cond_1

    .line 7384
    const/4 v0, 0x2

    iget-object v1, p0, Llzw;->a:Lnux;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7386
    :cond_1
    iget-object v0, p0, Llzw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7388
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7365
    invoke-virtual {p0, p1}, Llzw;->a(Loxn;)Llzw;

    move-result-object v0

    return-object v0
.end method
