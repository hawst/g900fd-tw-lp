.class public final Lmdj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lncb;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20248
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20251
    iput-object v0, p0, Lmdj;->apiHeader:Llyr;

    .line 20254
    iput-object v0, p0, Lmdj;->a:Lncb;

    .line 20248
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 20271
    const/4 v0, 0x0

    .line 20272
    iget-object v1, p0, Lmdj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 20273
    const/4 v0, 0x1

    iget-object v1, p0, Lmdj;->apiHeader:Llyr;

    .line 20274
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20276
    :cond_0
    iget-object v1, p0, Lmdj;->a:Lncb;

    if-eqz v1, :cond_1

    .line 20277
    const/4 v1, 0x2

    iget-object v2, p0, Lmdj;->a:Lncb;

    .line 20278
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20280
    :cond_1
    iget-object v1, p0, Lmdj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20281
    iput v0, p0, Lmdj;->ai:I

    .line 20282
    return v0
.end method

.method public a(Loxn;)Lmdj;
    .locals 2

    .prologue
    .line 20290
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 20291
    sparse-switch v0, :sswitch_data_0

    .line 20295
    iget-object v1, p0, Lmdj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 20296
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdj;->ah:Ljava/util/List;

    .line 20299
    :cond_1
    iget-object v1, p0, Lmdj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20301
    :sswitch_0
    return-object p0

    .line 20306
    :sswitch_1
    iget-object v0, p0, Lmdj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 20307
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmdj;->apiHeader:Llyr;

    .line 20309
    :cond_2
    iget-object v0, p0, Lmdj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20313
    :sswitch_2
    iget-object v0, p0, Lmdj;->a:Lncb;

    if-nez v0, :cond_3

    .line 20314
    new-instance v0, Lncb;

    invoke-direct {v0}, Lncb;-><init>()V

    iput-object v0, p0, Lmdj;->a:Lncb;

    .line 20316
    :cond_3
    iget-object v0, p0, Lmdj;->a:Lncb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20291
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 20259
    iget-object v0, p0, Lmdj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 20260
    const/4 v0, 0x1

    iget-object v1, p0, Lmdj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20262
    :cond_0
    iget-object v0, p0, Lmdj;->a:Lncb;

    if-eqz v0, :cond_1

    .line 20263
    const/4 v0, 0x2

    iget-object v1, p0, Lmdj;->a:Lncb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20265
    :cond_1
    iget-object v0, p0, Lmdj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 20267
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 20244
    invoke-virtual {p0, p1}, Lmdj;->a(Loxn;)Lmdj;

    move-result-object v0

    return-object v0
.end method
