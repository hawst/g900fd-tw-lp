.class public final Lndc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lndc;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [Lndc;

    sput-object v0, Lndc;->a:[Lndc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 24
    invoke-direct {p0}, Loxq;-><init>()V

    .line 42
    iput v0, p0, Lndc;->b:I

    .line 51
    iput v0, p0, Lndc;->f:I

    .line 24
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 77
    const/4 v0, 0x0

    .line 78
    iget v1, p0, Lndc;->b:I

    if-eq v1, v4, :cond_0

    .line 79
    const/4 v0, 0x1

    iget v1, p0, Lndc;->b:I

    .line 80
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 82
    :cond_0
    iget-object v1, p0, Lndc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 83
    const/4 v1, 0x2

    iget-object v2, p0, Lndc;->c:Ljava/lang/String;

    .line 84
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_1
    iget-object v1, p0, Lndc;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 87
    const/4 v1, 0x3

    iget-object v2, p0, Lndc;->d:Ljava/lang/Long;

    .line 88
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_2
    iget-object v1, p0, Lndc;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 91
    const/4 v1, 0x4

    iget-object v2, p0, Lndc;->e:Ljava/lang/Long;

    .line 92
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_3
    iget v1, p0, Lndc;->f:I

    if-eq v1, v4, :cond_4

    .line 95
    const/4 v1, 0x5

    iget v2, p0, Lndc;->f:I

    .line 96
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_4
    iget-object v1, p0, Lndc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    iput v0, p0, Lndc;->ai:I

    .line 100
    return v0
.end method

.method public a(Loxn;)Lndc;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 108
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 109
    sparse-switch v0, :sswitch_data_0

    .line 113
    iget-object v1, p0, Lndc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lndc;->ah:Ljava/util/List;

    .line 117
    :cond_1
    iget-object v1, p0, Lndc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    :sswitch_0
    return-object p0

    .line 124
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 125
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-ne v0, v4, :cond_3

    .line 128
    :cond_2
    iput v0, p0, Lndc;->b:I

    goto :goto_0

    .line 130
    :cond_3
    iput v2, p0, Lndc;->b:I

    goto :goto_0

    .line 135
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lndc;->c:Ljava/lang/String;

    goto :goto_0

    .line 139
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lndc;->d:Ljava/lang/Long;

    goto :goto_0

    .line 143
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lndc;->e:Ljava/lang/Long;

    goto :goto_0

    .line 147
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 148
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-ne v0, v1, :cond_5

    .line 154
    :cond_4
    iput v0, p0, Lndc;->f:I

    goto :goto_0

    .line 156
    :cond_5
    iput v2, p0, Lndc;->f:I

    goto :goto_0

    .line 109
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 56
    iget v0, p0, Lndc;->b:I

    if-eq v0, v4, :cond_0

    .line 57
    const/4 v0, 0x1

    iget v1, p0, Lndc;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 59
    :cond_0
    iget-object v0, p0, Lndc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 60
    const/4 v0, 0x2

    iget-object v1, p0, Lndc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_1
    iget-object v0, p0, Lndc;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 63
    const/4 v0, 0x3

    iget-object v1, p0, Lndc;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 65
    :cond_2
    iget-object v0, p0, Lndc;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 66
    const/4 v0, 0x4

    iget-object v1, p0, Lndc;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 68
    :cond_3
    iget v0, p0, Lndc;->f:I

    if-eq v0, v4, :cond_4

    .line 69
    const/4 v0, 0x5

    iget v1, p0, Lndc;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 71
    :cond_4
    iget-object v0, p0, Lndc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 73
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lndc;->a(Loxn;)Lndc;

    move-result-object v0

    return-object v0
.end method
