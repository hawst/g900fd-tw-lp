.class public final Lhzj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhzk;
.implements Lhzl;
.implements Lhzp;


# instance fields
.field private final a:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhzn;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lgu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgu",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhzj;-><init>(Llqr;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhzj;->b:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Lgu;

    invoke-direct {v0}, Lgu;-><init>()V

    iput-object v0, p0, Lhzj;->c:Lgu;

    .line 35
    if-eqz p1, :cond_0

    .line 36
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 38
    :cond_0
    return-void
.end method

.method private a(ILandroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 156
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lhzj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 157
    iget-object v0, p0, Lhzj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzn;

    invoke-interface {v0, p1, p2}, Lhzn;->a(ILandroid/os/Parcelable;)V

    .line 156
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 159
    :cond_0
    return-void
.end method

.method private a(ILjava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lhzj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 167
    iget-object v0, p0, Lhzj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzn;

    invoke-interface {v0, p1, p2}, Lhzn;->a(ILjava/util/Collection;)V

    .line 166
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 169
    :cond_0
    return-void
.end method

.method private d(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    const/16 v2, 0xaa

    .line 172
    instance-of v0, p1, Lhzm;

    if-eqz v0, :cond_1

    .line 173
    check-cast p1, Lhzm;

    iget-object v0, p0, Lhzj;->c:Lgu;

    invoke-virtual {v0, v2}, Lgu;->g(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lhzj;->c:Lgu;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v2, v1}, Lgu;->b(ILjava/lang/Object;)V

    :cond_0
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Lhzm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhzj;->c:Lgu;

    invoke-virtual {v0, v2}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    check-cast p1, Landroid/os/Parcelable;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 98
    iget-object v0, p0, Lhzj;->c:Lgu;

    invoke-virtual {v0}, Lgu;->c()V

    .line 99
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lhzj;->a(ILandroid/os/Parcelable;)V

    .line 100
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 42
    if-eqz p1, :cond_0

    .line 43
    iget-object v0, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    const-string v1, "com.google.android.libraries.social.content.multi.selection.DefaultItemSelection.values"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 45
    :cond_0
    return-void
.end method

.method public a(Lhzn;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lhzj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 67
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 68
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-direct {p0, v0}, Lhzj;->d(Landroid/os/Parcelable;)V

    .line 67
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lhzj;->a(ILjava/util/Collection;)V

    .line 72
    :cond_1
    return-void
.end method

.method public a(ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lhzj;->c:Lgu;

    invoke-virtual {v0, p1}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 110
    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 113
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Parcelable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 55
    iget-object v1, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    invoke-direct {p0, p1}, Lhzj;->d(Landroid/os/Parcelable;)V

    .line 57
    invoke-direct {p0, v0, p1}, Lhzj;->a(ILandroid/os/Parcelable;)V

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ILjava/lang/String;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0, p1, p2}, Lhzj;->a(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lhzj;->c:Lgu;

    invoke-virtual {v0, p1}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 122
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 49
    const-string v1, "com.google.android.libraries.social.content.multi.selection.DefaultItemSelection.values"

    .line 50
    invoke-virtual {p0}, Lhzj;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 49
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 51
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public b(Landroid/os/Parcelable;)Z
    .locals 4

    .prologue
    const/16 v3, 0xaa

    .line 76
    iget-object v0, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    instance-of v0, p1, Lhzm;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lhzm;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lhzm;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lhzj;->c:Lgu;

    invoke-virtual {v0, v3}, Lgu;->g(I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhzj;->c:Lgu;

    invoke-virtual {v0, v3}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhzj;->c:Lgu;

    invoke-virtual {v0, v3}, Lgu;->c(I)V

    .line 78
    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lhzj;->a(ILandroid/os/Parcelable;)V

    .line 79
    const/4 v0, 0x1

    .line 81
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public c(Landroid/os/Parcelable;)Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lhzj;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
