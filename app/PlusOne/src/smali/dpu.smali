.class public final Ldpu;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Liax;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    const-string v0, "RejectInferredPostTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 35
    iput p2, p0, Ldpu;->a:I

    .line 36
    iput-object p3, p0, Ldpu;->b:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldpu;->c:Ljava/util/ArrayList;

    .line 38
    iget-object v0, p0, Ldpu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    const-class v0, Liax;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Ldpu;->d:Liax;

    .line 40
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 10

    .prologue
    .line 44
    invoke-virtual {p0}, Ldpu;->f()Landroid/content/Context;

    move-result-object v1

    .line 46
    iget-object v0, p0, Ldpu;->d:Liax;

    iget v2, p0, Ldpu;->a:I

    const/16 v3, 0x22

    iget-object v4, p0, Ldpu;->c:Ljava/util/ArrayList;

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/16 v7, 0xd8

    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 46
    invoke-interface/range {v0 .. v9}, Liax;->a(Landroid/content/Context;IILjava/util/ArrayList;Ljava/util/ArrayList;IIJ)Z

    move-result v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p0}, Ldpu;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ldpu;->a:I

    iget-object v3, p0, Ldpu;->b:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Llap;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 59
    :cond_0
    new-instance v1, Lhoz;

    invoke-direct {v1, v0}, Lhoz;-><init>(Z)V

    return-object v1
.end method
