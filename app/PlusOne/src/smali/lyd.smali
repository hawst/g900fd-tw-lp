.class public final Llyd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llyd;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2656
    const/4 v0, 0x0

    new-array v0, v0, [Llyd;

    sput-object v0, Llyd;->a:[Llyd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2657
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2660
    const/4 v0, 0x0

    iput-object v0, p0, Llyd;->b:Llxp;

    .line 2657
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2689
    const/4 v0, 0x0

    .line 2690
    iget-object v1, p0, Llyd;->b:Llxp;

    if-eqz v1, :cond_0

    .line 2691
    const/4 v0, 0x1

    iget-object v1, p0, Llyd;->b:Llxp;

    .line 2692
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2694
    :cond_0
    iget-object v1, p0, Llyd;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2695
    const/4 v1, 0x2

    iget-object v2, p0, Llyd;->c:Ljava/lang/String;

    .line 2696
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2698
    :cond_1
    iget-object v1, p0, Llyd;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2699
    const/4 v1, 0x3

    iget-object v2, p0, Llyd;->d:Ljava/lang/String;

    .line 2700
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2702
    :cond_2
    iget-object v1, p0, Llyd;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2703
    const/4 v1, 0x4

    iget-object v2, p0, Llyd;->e:Ljava/lang/String;

    .line 2704
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2706
    :cond_3
    iget-object v1, p0, Llyd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2707
    iput v0, p0, Llyd;->ai:I

    .line 2708
    return v0
.end method

.method public a(Loxn;)Llyd;
    .locals 2

    .prologue
    .line 2716
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2717
    sparse-switch v0, :sswitch_data_0

    .line 2721
    iget-object v1, p0, Llyd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2722
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyd;->ah:Ljava/util/List;

    .line 2725
    :cond_1
    iget-object v1, p0, Llyd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2727
    :sswitch_0
    return-object p0

    .line 2732
    :sswitch_1
    iget-object v0, p0, Llyd;->b:Llxp;

    if-nez v0, :cond_2

    .line 2733
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llyd;->b:Llxp;

    .line 2735
    :cond_2
    iget-object v0, p0, Llyd;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2739
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyd;->c:Ljava/lang/String;

    goto :goto_0

    .line 2743
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyd;->d:Ljava/lang/String;

    goto :goto_0

    .line 2747
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyd;->e:Ljava/lang/String;

    goto :goto_0

    .line 2717
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2671
    iget-object v0, p0, Llyd;->b:Llxp;

    if-eqz v0, :cond_0

    .line 2672
    const/4 v0, 0x1

    iget-object v1, p0, Llyd;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2674
    :cond_0
    iget-object v0, p0, Llyd;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2675
    const/4 v0, 0x2

    iget-object v1, p0, Llyd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2677
    :cond_1
    iget-object v0, p0, Llyd;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2678
    const/4 v0, 0x3

    iget-object v1, p0, Llyd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2680
    :cond_2
    iget-object v0, p0, Llyd;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2681
    const/4 v0, 0x4

    iget-object v1, p0, Llyd;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2683
    :cond_3
    iget-object v0, p0, Llyd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2685
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2653
    invoke-virtual {p0, p1}, Llyd;->a(Loxn;)Llyd;

    move-result-object v0

    return-object v0
.end method
