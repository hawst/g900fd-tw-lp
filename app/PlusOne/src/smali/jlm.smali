.class public final Ljlm;
.super Lnx;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 24
    const v0, 0x7f0400c0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lnx;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 25
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/16 v8, 0x8

    const/4 v4, 0x0

    .line 31
    check-cast p3, Ljkx;

    .line 32
    invoke-virtual {p3}, Ljkx;->c()Llvo;

    move-result-object v6

    .line 33
    invoke-virtual {p3}, Ljkx;->e()I

    move-result v0

    .line 34
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v2, v3

    .line 37
    :goto_0
    const v0, 0x7f1002f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    iget-object v1, v6, Llvo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    const v1, 0x7f1002f2

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 42
    iget-object v5, v6, Llvo;->d:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    if-eqz v2, :cond_3

    const v5, 0x7f0b0194

    :goto_1
    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 47
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 48
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 51
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v2, :cond_4

    const v0, 0x7f0b0287

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 53
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 56
    const v0, 0x7f1001e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;

    .line 57
    const v1, 0x7f100117

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;

    .line 58
    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->setVisibility(I)V

    .line 59
    invoke-virtual {v1, v8}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->setVisibility(I)V

    .line 60
    iget-object v5, v6, Llvo;->b:[Llvm;

    array-length v5, v5

    if-lez v5, :cond_5

    .line 61
    iget-object v1, v6, Llvo;->b:[Llvm;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a([Llvm;ZZ)V

    .line 62
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->setVisibility(I)V

    .line 73
    :cond_0
    :goto_3
    const v0, 0x7f10002e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 74
    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 75
    iget-object v1, v6, Llvo;->e:[Llvl;

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v1, v6, Llvo;->e:[Llvl;

    aget-object v1, v1, v4

    iget-object v1, v1, Llvl;->b:Llvi;

    if-eqz v1, :cond_1

    .line 76
    iget-object v1, v6, Llvo;->e:[Llvl;

    aget-object v1, v1, v4

    iget-object v1, v1, Llvl;->b:Llvi;

    iget-object v1, v1, Llvi;->a:Ljava/lang/String;

    .line 77
    invoke-static {v1}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 78
    sget-object v3, Ljac;->a:Ljac;

    invoke-static {p2, v1, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 79
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 80
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->m(Z)V

    .line 83
    :cond_1
    return-void

    :cond_2
    move v2, v4

    .line 34
    goto/16 :goto_0

    .line 45
    :cond_3
    const v5, 0x7f0b0193

    goto :goto_1

    .line 51
    :cond_4
    const v0, 0x7f0b0197

    goto :goto_2

    .line 63
    :cond_5
    iget-object v0, v6, Llvo;->a:Llvi;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, v6, Llvo;->a:Llvi;

    iget-object v0, v0, Llvi;->a:Ljava/lang/String;

    .line 65
    invoke-static {v0}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 66
    sget-object v3, Ljac;->a:Ljac;

    invoke-static {p2, v0, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->a(Lizu;)V

    .line 67
    invoke-virtual {v1, v4}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->setVisibility(I)V

    .line 68
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/RoundedMediaView;->m(Z)V

    goto :goto_3
.end method
