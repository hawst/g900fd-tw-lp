.class public final Lexl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lexl;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:J

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-string v0, "[^\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]*"

    .line 82
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lexl;->a:Ljava/util/regex/Pattern;

    .line 871
    new-instance v0, Lexm;

    invoke-direct {v0}, Lexm;-><init>()V

    sput-object v0, Lexl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lexl;->b:I

    .line 105
    invoke-direct {p0, p1}, Lexl;->a(Landroid/net/Uri;)V

    .line 106
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lexl;->b:I

    .line 831
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lexl;->b:I

    .line 832
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->c:Ljava/lang/String;

    .line 833
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    .line 834
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->f:Ljava/lang/String;

    .line 835
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->g:Ljava/lang/String;

    .line 836
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->h:Ljava/lang/String;

    .line 837
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lexl;->i:J

    .line 838
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->j:Ljava/lang/String;

    .line 839
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->m:Ljava/lang/String;

    .line 840
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->n:Ljava/lang/String;

    .line 841
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->o:Ljava/lang/String;

    .line 842
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->p:Ljava/lang/String;

    .line 843
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->s:Ljava/lang/String;

    .line 844
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lexl;->b:I

    .line 109
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lexl;->a(Landroid/net/Uri;)V

    .line 110
    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x3

    const/4 v3, 0x0

    const/4 v8, 0x2

    const/4 v5, 0x1

    .line 536
    invoke-static {p1}, Ligz;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 537
    const/16 v0, 0x1f

    iput v0, p0, Lexl;->b:I

    .line 708
    :cond_0
    :goto_0
    return-void

    .line 545
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 547
    const-string v0, "authkey"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->c:Ljava/lang/String;

    .line 548
    const-string v0, "gpinv"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->d:Ljava/lang/String;

    .line 550
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 553
    if-lt v0, v8, :cond_35

    const-string v4, "u"

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 554
    invoke-interface {v1, v8, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 555
    add-int/lit8 v0, v0, -0x2

    move-object v6, v1

    move v1, v0

    .line 558
    :goto_1
    if-lez v1, :cond_13

    const-string v0, "photos"

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 559
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_2

    iput v7, p0, Lexl;->b:I

    goto :goto_0

    :cond_2
    if-ne v0, v8, :cond_6

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "fromphone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "instantupload"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/16 v0, 0xc

    iput v0, p0, Lexl;->b:I

    goto :goto_0

    :cond_4
    const-string v1, "yourphotos"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v0, 0x2c

    iput v0, p0, Lexl;->b:I

    goto :goto_0

    :cond_5
    const-string v1, "search"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    iput v0, p0, Lexl;->b:I

    goto :goto_0

    :cond_6
    if-ne v0, v7, :cond_b

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "of"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v0, 0xd

    iput v0, p0, Lexl;->b:I

    iput-object v1, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    const-string v2, "search"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v0, 0x20

    iput v0, p0, Lexl;->b:I

    iput-object v1, p0, Lexl;->p:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    const-string v2, "posts"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v1, 0xe

    iput v1, p0, Lexl;->b:I

    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    :cond_9
    const-string v2, "albums"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v1, 0x7

    iput v1, p0, Lexl;->b:I

    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    :cond_a
    const-string v2, "stories"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x2b

    iput v1, p0, Lexl;->b:I

    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    :cond_b
    const/4 v1, 0x4

    if-ne v0, v1, :cond_10

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "photos"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "photo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0x13

    iput v0, p0, Lexl;->b:I

    iput-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v3}, Lexl;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lexl;->i:J

    goto/16 :goto_0

    :cond_c
    const-string v0, "albums"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "album"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_d
    iput-object v1, p0, Lexl;->e:Ljava/lang/String;

    const-string v0, "profile"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0x11

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    :cond_e
    const-string v0, "posts"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0xe

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    :cond_f
    iput-object v3, p0, Lexl;->h:Ljava/lang/String;

    iget-object v0, p0, Lexl;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    :cond_10
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x4

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "albums"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    const-string v0, "profile"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {v3}, Lexl;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lexl;->i:J

    iget-wide v0, p0, Lexl;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    :cond_11
    const-string v0, "posts"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-static {v3}, Lexl;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lexl;->i:J

    iget-wide v0, p0, Lexl;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/16 v0, 0x13

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    :cond_12
    iput-object v2, p0, Lexl;->h:Ljava/lang/String;

    invoke-static {v3}, Lexl;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lexl;->i:J

    iget-object v0, p0, Lexl;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lexl;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 563
    :cond_13
    if-nez v1, :cond_14

    .line 564
    iput v5, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 568
    :cond_14
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 570
    const-string v4, "settings"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 578
    if-ne v1, v5, :cond_1f

    .line 579
    const-string v1, "stream"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 580
    iput v8, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 581
    :cond_15
    const-string v1, "me"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 582
    const/4 v0, 0x4

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 583
    :cond_16
    const-string v1, "circles"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 584
    const/16 v0, 0x9

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 585
    :cond_17
    const-string v1, "hot"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    const-string v1, "explore"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 586
    :cond_18
    const/16 v0, 0x19

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 587
    :cond_19
    const-string v1, "events"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 588
    const/16 v0, 0x1c

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 589
    :cond_1a
    const-string v1, "share"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 590
    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 591
    new-instance v1, Lexl;

    invoke-direct {v1, v0}, Lexl;-><init>(Ljava/lang/String;)V

    .line 592
    invoke-virtual {v1}, Lexl;->a()I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_1b

    .line 593
    invoke-virtual {v1}, Lexl;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    .line 594
    invoke-virtual {v1}, Lexl;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->g:Ljava/lang/String;

    .line 595
    const/16 v0, 0x26

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 597
    :cond_1b
    iput-object v0, p0, Lexl;->q:Ljava/lang/String;

    .line 598
    const/16 v0, 0x23

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 600
    :cond_1c
    const-string v1, "+"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 601
    iput v5, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 602
    :cond_1d
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1e

    const-string v1, "0123456789+"

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1e

    .line 603
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    .line 604
    const/16 v0, 0x14

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 606
    :cond_1e
    const/16 v0, 0x1f

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 608
    :cond_1f
    if-ne v1, v8, :cond_2a

    .line 609
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 610
    const-string v4, "posts"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_20

    const-string v4, "stream"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 611
    :cond_20
    const/4 v1, 0x5

    iput v1, p0, Lexl;->b:I

    .line 612
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 613
    :cond_21
    const-string v4, "about"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 614
    const/4 v1, 0x6

    iput v1, p0, Lexl;->b:I

    .line 615
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 616
    :cond_22
    const-string v4, "photos"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 617
    const/4 v1, 0x7

    iput v1, p0, Lexl;->b:I

    .line 618
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 619
    :cond_23
    const-string v4, "reviews"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 620
    const/16 v1, 0x8

    iput v1, p0, Lexl;->b:I

    .line 621
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 622
    :cond_24
    const-string v4, "circles"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_25

    const-string v4, "people"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_26

    :cond_25
    const-string v4, "find"

    .line 623
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 624
    const/16 v0, 0x1a

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 625
    :cond_26
    const-string v4, "events"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    move-object v0, p0

    move-object v3, v2

    move-object v4, p1

    .line 626
    invoke-direct/range {v0 .. v5}, Lexl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;I)V

    goto/16 :goto_0

    .line 628
    :cond_27
    const-string v2, "s"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    iput-object v1, p0, Lexl;->p:Ljava/lang/String;

    .line 630
    iget-object v0, p0, Lexl;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_28

    sget-object v1, Lexl;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_28

    :goto_2
    if-eqz v5, :cond_29

    .line 631
    const/16 v0, 0x25

    iput v0, p0, Lexl;->b:I

    .line 635
    :goto_3
    const-string v0, "blob"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 636
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 637
    iput-object v0, p0, Lexl;->r:Ljava/lang/String;

    goto/16 :goto_0

    :cond_28
    move v5, v3

    .line 630
    goto :goto_2

    .line 633
    :cond_29
    const/16 v0, 0x24

    iput v0, p0, Lexl;->b:I

    goto :goto_3

    .line 640
    :cond_2a
    if-ne v1, v7, :cond_32

    .line 641
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 642
    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 644
    const-string v3, "posts"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 645
    const/16 v2, 0xa

    iput v2, p0, Lexl;->b:I

    .line 646
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    .line 647
    iput-object v1, p0, Lexl;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 648
    :cond_2b
    const-string v3, "digest"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 650
    const/4 v1, 0x5

    iput v1, p0, Lexl;->b:I

    .line 651
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 652
    :cond_2c
    const-string v3, "notifications"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2d

    const-string v3, "all"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 653
    const-string v0, "mute"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 654
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 655
    const/16 v1, 0x18

    iput v1, p0, Lexl;->b:I

    .line 656
    iput-object v0, p0, Lexl;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 658
    :cond_2d
    const-string v3, "events"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 659
    const-string v0, "gallery"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    move-object v0, p0

    move-object v3, v2

    move-object v4, p1

    move v5, v8

    .line 660
    invoke-direct/range {v0 .. v5}, Lexl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;I)V

    goto/16 :goto_0

    :cond_2e
    move-object v6, p0

    move-object v8, v1

    move-object v9, v2

    move-object v10, p1

    move v11, v5

    .line 663
    invoke-direct/range {v6 .. v11}, Lexl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;I)V

    goto/16 :goto_0

    .line 666
    :cond_2f
    const-string v2, "op"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 668
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    .line 669
    invoke-direct {p0, v1}, Lexl;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 670
    :cond_30
    const-string v2, "s"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    const-string v2, "posts"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 671
    const/16 v0, 0x24

    iput v0, p0, Lexl;->b:I

    .line 672
    iput-object v7, p0, Lexl;->p:Ljava/lang/String;

    .line 673
    const-string v0, "blob"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 674
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 675
    iput-object v0, p0, Lexl;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 677
    :cond_31
    const-string v2, "stories"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 678
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    .line 679
    iput-object v1, p0, Lexl;->s:Ljava/lang/String;

    .line 680
    const/16 v0, 0x2a

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 682
    :cond_32
    const/4 v3, 0x4

    if-ne v1, v3, :cond_34

    .line 684
    const-string v1, "about"

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    const-string v1, "op"

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 685
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    .line 686
    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lexl;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 687
    :cond_33
    const-string v1, "stories"

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 692
    iput-object v0, p0, Lexl;->e:Ljava/lang/String;

    .line 693
    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lexl;->s:Ljava/lang/String;

    .line 694
    const/16 v0, 0x2a

    iput v0, p0, Lexl;->b:I

    goto/16 :goto_0

    .line 696
    :cond_34
    const/4 v3, 0x5

    if-ne v1, v3, :cond_0

    .line 697
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 698
    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Ljava/lang/String;

    .line 700
    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 701
    const/4 v3, 0x4

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 703
    const-string v6, "events"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "rsvp"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v4, p1

    .line 704
    invoke-direct/range {v0 .. v5}, Lexl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;I)V

    goto/16 :goto_0

    :cond_35
    move-object v6, v1

    move v1, v0

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;I)V
    .locals 1

    .prologue
    .line 727
    const/16 v0, 0x1b

    iput v0, p0, Lexl;->b:I

    .line 728
    iput-object p2, p0, Lexl;->n:Ljava/lang/String;

    .line 729
    iput-object p1, p0, Lexl;->j:Ljava/lang/String;

    .line 730
    iput-object p3, p0, Lexl;->m:Ljava/lang/String;

    .line 731
    const-string v0, "phid"

    invoke-virtual {p4, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lexl;->l:Z

    .line 732
    const-string v0, "gpinv"

    invoke-virtual {p4, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexl;->o:Ljava/lang/String;

    .line 733
    iput p5, p0, Lexl;->k:I

    .line 734
    return-void

    .line 731
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 517
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 518
    invoke-interface {v0, p2}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 519
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 520
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 521
    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 523
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 711
    iget v0, p0, Lexl;->b:I

    .line 712
    const-string v1, "completemyprofile"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "profilephoto"

    .line 713
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 714
    :cond_0
    const/16 v0, 0x21

    .line 722
    :cond_1
    :goto_0
    return v0

    .line 715
    :cond_2
    const-string v1, "coverphoto"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 716
    const/16 v0, 0x27

    goto :goto_0

    .line 717
    :cond_3
    const-string v1, "tagline"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 718
    const/16 v0, 0x28

    goto :goto_0

    .line 719
    :cond_4
    const-string v1, "contact"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 720
    const/16 v0, 0x29

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 820
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 822
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lexl;->b:I

    return v0
.end method

.method public a(Landroid/content/Context;ILandroid/os/Bundle;Z)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 204
    iget v0, p0, Lexl;->b:I

    packed-switch v0, :pswitch_data_0

    .line 462
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lexl;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    :cond_0
    :goto_0
    return-object v0

    .line 206
    :pswitch_1
    invoke-static {p1, p2}, Leyq;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 209
    :pswitch_2
    invoke-static {p1, p2}, Leyq;->i(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 212
    :pswitch_3
    invoke-static {p1, p2}, Leyq;->n(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 215
    :pswitch_4
    invoke-static {p1, p2}, Leyq;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 218
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "destination"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v10, "circle_info"

    new-instance v1, Levm;

    const/4 v3, 0x0

    const-string v4, "v.whatshot"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x1

    move-object v2, p1

    invoke-direct/range {v1 .. v9}, Levm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZIJ)V

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0

    .line 221
    :pswitch_6
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "g:"

    const-string v2, "gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 222
    :goto_1
    if-eqz v0, :cond_3

    .line 223
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 221
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 227
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 230
    :pswitch_7
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 233
    :pswitch_8
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "destination"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "photo_picker_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "starting_tab_index"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    .line 236
    :pswitch_9
    iget-object v0, p0, Lexl;->f:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 238
    if-eqz p3, :cond_0

    const-string v1, "com.google.android.apps.plus.HIDE_ACTION_BAR_LOGO"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    const-string v1, "com.google.android.apps.plus.HIDE_ACTION_BAR_LOGO"

    const-string v2, "com.google.android.apps.plus.HIDE_ACTION_BAR_LOGO"

    .line 240
    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 239
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 246
    :pswitch_a
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 250
    :pswitch_b
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 254
    :pswitch_c
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_4
    const/4 v3, 0x0

    const/4 v4, 0x2

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 258
    :pswitch_d
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_5
    const/4 v3, 0x0

    const/4 v4, 0x3

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 262
    :pswitch_e
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lexl;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 263
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_6
    const/4 v3, 0x0

    const/4 v4, 0x4

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    .line 266
    if-eqz p3, :cond_0

    const-string v1, "local_folders_only"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    const-string v1, "local_folders_only"

    const-string v2, "local_folders_only"

    .line 268
    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 267
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 263
    :cond_8
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 272
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 276
    :pswitch_f
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lexl;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 277
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_7
    const/4 v3, 0x0

    const/4 v4, 0x5

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    .line 280
    if-eqz p3, :cond_0

    const-string v1, "local_folders_only"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    const-string v1, "local_folders_only"

    const-string v2, "local_folders_only"

    .line 282
    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 281
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 277
    :cond_a
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_7

    .line 286
    :cond_b
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lexl;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 290
    :pswitch_10
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lexl;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 291
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_8
    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    .line 296
    :cond_d
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lexl;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 300
    :pswitch_11
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lexl;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 301
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_9
    const/4 v3, 0x0

    const/4 v4, 0x7

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_e
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_9

    .line 306
    :cond_f
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lexl;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 310
    :pswitch_12
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 316
    :pswitch_13
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lexl;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 317
    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/photos/phone/PhotosHomeActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "destination"

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "account_id"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "gaia_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_picker_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    .line 320
    :cond_10
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_a
    const/4 v3, 0x0

    const/4 v4, 0x2

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_11
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_a

    .line 326
    :pswitch_14
    iget-object v0, p0, Lexl;->p:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Leyq;->j(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 329
    :pswitch_15
    const/4 v0, 0x0

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    const-string v2, "posts"

    const-string v3, "ALBUM"

    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 332
    invoke-static {p1, p2}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 333
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    iget-object v1, p0, Lexl;->c:Ljava/lang/String;

    .line 334
    invoke-virtual {v0, v1}, Ljuj;->d(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 340
    :pswitch_16
    const/4 v0, 0x0

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    iget-object v2, p0, Lexl;->h:Ljava/lang/String;

    const-string v3, "ALBUM"

    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    invoke-static {p1, p2}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 344
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    iget-object v1, p0, Lexl;->c:Ljava/lang/String;

    .line 345
    invoke-virtual {v0, v1}, Ljuj;->d(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 346
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 351
    :pswitch_17
    const/4 v0, 0x0

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    const-string v2, "profile"

    const-string v3, "ALBUM"

    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 354
    invoke-static {p1, p2}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 355
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    iget-object v1, p0, Lexl;->c:Ljava/lang/String;

    .line 356
    invoke-virtual {v0, v1}, Ljuj;->d(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 357
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 362
    :pswitch_18
    const/4 v0, 0x0

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    iget-object v2, p0, Lexl;->h:Ljava/lang/String;

    const-string v3, "ALBUM"

    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 364
    const/4 v1, 0x3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 365
    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 366
    new-instance v1, Ldew;

    invoke-direct {v1, p1, p2}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 368
    invoke-virtual {v1, v0}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-wide v2, p0, Lexl;->i:J

    .line 369
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->b(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lexl;->c:Ljava/lang/String;

    .line 370
    invoke-virtual {v0, v1}, Ldew;->c(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 371
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 376
    :pswitch_19
    const/4 v0, 0x0

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    const-string v2, "profile"

    const-string v3, "ALBUM"

    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 378
    const/4 v1, 0x3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 379
    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 380
    new-instance v1, Ldew;

    invoke-direct {v1, p1, p2}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 382
    invoke-virtual {v1, v0}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-wide v2, p0, Lexl;->i:J

    .line 383
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->b(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lexl;->c:Ljava/lang/String;

    .line 384
    invoke-virtual {v0, v1}, Ldew;->c(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 385
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 390
    :pswitch_1a
    const/4 v0, 0x0

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    const-string v2, "posts"

    const-string v3, "ALBUM"

    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 392
    const/4 v1, 0x3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 393
    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 394
    new-instance v1, Ldew;

    invoke-direct {v1, p1, p2}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 396
    invoke-virtual {v1, v0}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-wide v2, p0, Lexl;->i:J

    .line 397
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->b(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lexl;->c:Ljava/lang/String;

    .line 398
    invoke-virtual {v0, v1}, Ldew;->c(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 404
    :pswitch_1b
    iget-object v1, p0, Lexl;->f:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "account_id"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "activity_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mute"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 407
    :pswitch_1c
    invoke-static {p1, p2}, Leyq;->d(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 413
    :pswitch_1d
    iget-object v2, p0, Lexl;->j:Ljava/lang/String;

    iget-object v3, p0, Lexl;->n:Ljava/lang/String;

    iget-boolean v0, p0, Lexl;->l:Z

    if-eqz v0, :cond_12

    const/4 v4, 0x1

    :goto_b
    iget-object v5, p0, Lexl;->o:Ljava/lang/String;

    const/high16 v6, -0x80000000

    iget-object v7, p0, Lexl;->c:Ljava/lang/String;

    iget v9, p0, Lexl;->k:I

    move-object v0, p1

    move v1, p2

    move v8, p4

    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;ZI)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_12
    const/4 v4, 0x0

    goto :goto_b

    .line 420
    :pswitch_1e
    iget-object v0, p0, Lexl;->q:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 423
    :pswitch_1f
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    const/4 v3, 0x1

    .line 424
    :goto_c
    iget-object v2, p0, Lexl;->f:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    move v1, p2

    move v4, v3

    invoke-static/range {v0 .. v6}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;ZZLhgw;[B)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 423
    :cond_13
    const/4 v3, 0x0

    goto :goto_c

    .line 428
    :pswitch_20
    iget-object v0, p0, Lexl;->p:Ljava/lang/String;

    iget-object v1, p0, Lexl;->r:Ljava/lang/String;

    invoke-static {p1, p2, v0, v1}, Leyq;->e(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 432
    :pswitch_21
    iget-object v0, p0, Lexl;->p:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Leyq;->i(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 435
    :pswitch_22
    const-class v0, Ldhu;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhu;

    .line 436
    if-eqz v0, :cond_14

    .line 437
    iget-object v3, p0, Lexl;->s:Ljava/lang/String;

    iget-object v4, p0, Lexl;->c:Ljava/lang/String;

    iget-object v5, p0, Lexl;->d:Ljava/lang/String;

    iget-object v6, p0, Lexl;->e:Ljava/lang/String;

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v6}, Ldhu;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 440
    :cond_14
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 446
    :pswitch_23
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lexl;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 447
    const-class v0, Ldhu;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhu;

    .line 448
    if-eqz v0, :cond_15

    .line 449
    invoke-virtual {v0, p1, p2}, Ldhu;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 451
    :cond_15
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 456
    :cond_16
    const-string v0, "g:"

    iget-object v1, p0, Lexl;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_17

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_d
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p1

    move v1, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_17
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_d

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_2
        :pswitch_9
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_15
        :pswitch_16
        :pswitch_18
        :pswitch_17
        :pswitch_19
        :pswitch_1a
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1b
        :pswitch_5
        :pswitch_3
        :pswitch_1d
        :pswitch_1c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_e
        :pswitch_0
        :pswitch_1e
        :pswitch_20
        :pswitch_21
        :pswitch_1f
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_22
        :pswitch_23
        :pswitch_8
    .end packed-switch
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    const v1, 0x7f0a0b55

    .line 467
    iget v0, p0, Lexl;->b:I

    packed-switch v0, :pswitch_data_0

    .line 512
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 471
    :pswitch_1
    const v0, 0x7f0a0b52

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 475
    :pswitch_2
    const v0, 0x7f0a0b54

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 480
    :pswitch_3
    const v0, 0x7f0a0b53

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 496
    :pswitch_4
    const v0, 0x7f0a0b56

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 499
    :pswitch_5
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 502
    :pswitch_6
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 505
    :pswitch_7
    const v0, 0x7f0a0b57

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 509
    :pswitch_8
    const v0, 0x7f0a0b58

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 467
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lexl;->e:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lexl;->g:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lexl;->f:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 169
    iget v1, p0, Lexl;->b:I

    packed-switch v1, :pswitch_data_0

    .line 194
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 173
    :pswitch_1
    iget-object v1, p0, Lexl;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 191
    :pswitch_2
    invoke-virtual {p0}, Lexl;->e()Z

    move-result v0

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 865
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 848
    iget v0, p0, Lexl;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 849
    iget-object v0, p0, Lexl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 850
    iget-object v0, p0, Lexl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 851
    iget-object v0, p0, Lexl;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 852
    iget-object v0, p0, Lexl;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 853
    iget-object v0, p0, Lexl;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 854
    iget-wide v0, p0, Lexl;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 855
    iget-object v0, p0, Lexl;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 856
    iget-object v0, p0, Lexl;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 857
    iget-object v0, p0, Lexl;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 858
    iget-object v0, p0, Lexl;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 859
    iget-object v0, p0, Lexl;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 860
    iget-object v0, p0, Lexl;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 861
    return-void
.end method
