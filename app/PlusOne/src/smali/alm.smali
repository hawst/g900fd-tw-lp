.class public Lalm;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lavz;",
        "Ljava/lang/Void;",
        "Lalk;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbqg;

.field private final c:Lbig;

.field private final d:Z

.field private final e:Lalp;

.field private final f:I

.field private final g:I

.field private final h:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lalm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lalm;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lbqg;Lbig;Lani;ZLalp;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 98
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 99
    const-string v0, "uriMimeTypeRetriever"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqg;

    iput-object v0, p0, Lalm;->b:Lbqg;

    .line 100
    const-string v0, "mediaExtractorFactory"

    .line 101
    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbig;

    iput-object v0, p0, Lalm;->c:Lbig;

    .line 102
    const-string v0, "inputLimits"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 103
    invoke-interface {p3}, Lani;->x()I

    move-result v0

    iput v0, p0, Lalm;->f:I

    .line 104
    invoke-interface {p3}, Lani;->z()I

    move-result v0

    iput v0, p0, Lalm;->g:I

    .line 105
    invoke-interface {p3}, Lani;->y()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lalm;->h:J

    .line 106
    iput-boolean p4, p0, Lalm;->d:Z

    .line 107
    const-string v0, "listener"

    invoke-static {p5, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalp;

    iput-object v0, p0, Lalm;->e:Lalp;

    .line 108
    return-void
.end method

.method public static a(Lbqg;Lbig;Lani;Z)Lalo;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Laln;

    invoke-direct {v0, p0, p1, p2, p3}, Laln;-><init>(Lbqg;Lbig;Lani;Z)V

    return-object v0
.end method

.method private a(Landroid/net/Uri;)Lbmx;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 172
    .line 174
    :try_start_0
    iget-object v0, p0, Lalm;->c:Lbig;

    invoke-interface {v0, p1}, Lbig;->b(Landroid/net/Uri;)Lbij;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 175
    :try_start_1
    invoke-interface {v2}, Lbij;->i()Lbmu;

    move-result-object v0

    check-cast v0, Lbmx;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 180
    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    .line 182
    :goto_0
    return-object v0

    .line 178
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    :try_start_2
    sget-object v2, Lalm;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to extract metadata for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 180
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    move-object v0, v1

    .line 182
    goto :goto_0

    .line 180
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_2
    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_2

    .line 178
    :catch_1
    move-exception v0

    move-object v0, v2

    goto :goto_1
.end method

.method private b(Landroid/net/Uri;)Lasu;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 193
    invoke-direct {p0, p1}, Lalm;->d(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    sget-object v0, Lasv;->j:Lasv;

    invoke-static {v0, p1}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    .line 217
    :goto_0
    return-object v0

    .line 199
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lalm;->c(Landroid/net/Uri;)Lboo;
    :try_end_0
    .catch Lalq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 204
    iget-wide v4, v3, Lboo;->f:J

    const-wide/32 v6, 0x1e8480

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 205
    sget-object v0, Lasv;->e:Lasv;

    invoke-static {v0, p1}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    iget-object v0, v0, Lalq;->a:Lasv;

    invoke-static {v0, p1}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    goto :goto_0

    .line 207
    :cond_1
    iget-wide v4, v3, Lboo;->f:J

    iget-wide v6, p0, Lalm;->h:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    .line 208
    sget-object v0, Lasv;->f:Lasv;

    invoke-static {v0, p1}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    goto :goto_0

    .line 210
    :cond_2
    iget-wide v4, v3, Lboo;->f:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 211
    sget-object v0, Lasv;->h:Lasv;

    invoke-static {v0, p1}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    goto :goto_0

    .line 213
    :cond_3
    iget v0, v3, Lboo;->d:I

    iget v4, p0, Lalm;->g:I

    if-le v0, v4, :cond_4

    .line 214
    sget-object v0, Lasv;->g:Lasv;

    invoke-static {v0, p1}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    goto :goto_0

    .line 217
    :cond_4
    iget v0, v3, Lbmu;->a:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iget v4, v3, Lbmu;->d:I

    iget v5, v3, Lbmu;->c:I

    if-le v4, v5, :cond_6

    :goto_2
    xor-int/2addr v0, v1

    if-eqz v0, :cond_7

    sget-object v0, Lasv;->c:Lasv;

    :goto_3
    invoke-static {v0, p1, v3}, Lasu;->a(Lasv;Landroid/net/Uri;Lbmu;)Lasu;

    move-result-object v0

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_2

    :cond_7
    sget-object v0, Lasv;->d:Lasv;

    goto :goto_3
.end method

.method private c(Landroid/net/Uri;)Lboo;
    .locals 3

    .prologue
    .line 225
    :try_start_0
    iget-object v0, p0, Lalm;->c:Lbig;

    .line 226
    invoke-interface {v0, p1}, Lbig;->a(Landroid/net/Uri;)Lbij;
    :try_end_0
    .catch Lbpr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 228
    :try_start_1
    invoke-interface {v1}, Lbij;->i()Lbmu;

    move-result-object v0

    check-cast v0, Lboo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    :try_start_2
    invoke-interface {v1}, Lbij;->a()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lbij;->a()V

    throw v0
    :try_end_2
    .catch Lbpr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 234
    :catch_0
    move-exception v0

    sget-object v0, Lalm;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "File has been deleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    new-instance v0, Lalq;

    sget-object v1, Lasv;->b:Lasv;

    invoke-direct {v0, v1}, Lalq;-><init>(Lasv;)V

    throw v0

    .line 238
    :catch_1
    move-exception v0

    sget-object v0, Lalm;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unable to extract metadata for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    new-instance v0, Lalq;

    sget-object v1, Lasv;->k:Lasv;

    invoke-direct {v0, v1}, Lalq;-><init>(Lasv;)V

    throw v0
.end method

.method private d(Landroid/net/Uri;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 246
    :try_start_0
    iget-object v1, p0, Lalm;->c:Lbig;

    invoke-interface {v1, p1}, Lbig;->c(Landroid/net/Uri;)Lbih;
    :try_end_0
    .catch Lbpr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move v1, v0

    .line 252
    :goto_0
    :try_start_1
    invoke-interface {v2}, Lbih;->f()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 253
    invoke-interface {v2, v1}, Lbih;->a(I)Landroid/media/MediaFormat;

    move-result-object v3

    .line 254
    const-string v4, "mime"

    invoke-virtual {v3, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "video/avc"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 255
    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    const/4 v0, 0x1

    .line 263
    :goto_1
    return v0

    .line 248
    :catch_0
    move-exception v1

    sget-object v1, Lalm;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to open "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 252
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 261
    :cond_1
    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    goto :goto_1

    .line 259
    :catch_1
    move-exception v1

    :try_start_2
    sget-object v1, Lalm;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failed to read "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 261
    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    throw v0
.end method


# virtual methods
.method protected varargs a([Lavz;)Lalk;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-static {}, Lalk;->a()Lall;

    move-result-object v4

    .line 113
    array-length v0, p1

    iget v2, p0, Lalm;->f:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v4, v0}, Lall;->a(I)Lall;

    .line 119
    array-length v5, p1

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_8

    aget-object v6, p1, v2

    .line 120
    invoke-virtual {p0}, Lalm;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_8

    add-int/lit8 v3, v0, 0x1

    iget v7, p0, Lalm;->f:I

    if-ge v0, v7, :cond_8

    .line 121
    iget-object v6, v6, Lavz;->a:Landroid/net/Uri;

    .line 125
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v7, "content://com.google.android.gallery3d.provider/picasa"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    sget-object v0, Lasv;->a:Lasv;

    invoke-static {v0, v6}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    invoke-virtual {v4, v0}, Lall;->a(Lasu;)Lall;

    .line 119
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lalm;->b:Lbqg;

    invoke-interface {v0, v6}, Lbqg;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 132
    if-nez v0, :cond_2

    .line 133
    sget-object v0, Lasv;->b:Lasv;

    invoke-static {v0, v6}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    invoke-virtual {v4, v0}, Lall;->a(Lasu;)Lall;

    goto :goto_1

    .line 135
    :cond_2
    iget-boolean v7, p0, Lalm;->d:Z

    if-eqz v7, :cond_4

    const-string v7, "image/"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 136
    invoke-direct {p0, v6}, Lalm;->a(Landroid/net/Uri;)Lbmx;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lasv;->m:Lasv;

    invoke-static {v0, v6}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    :goto_2
    invoke-virtual {v4, v0}, Lall;->a(Lasu;)Lall;

    goto :goto_1

    :cond_3
    sget-object v7, Lasv;->l:Lasv;

    invoke-static {v7, v6, v0}, Lasu;->a(Lasv;Landroid/net/Uri;Lbmu;)Lasu;

    move-result-object v0

    goto :goto_2

    .line 137
    :cond_4
    const-string v7, "video/"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 138
    const-string v7, "video/mp4"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "video/mpeg"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_7

    .line 139
    sget-object v0, Lasv;->i:Lasv;

    invoke-static {v0, v6}, Lasu;->a(Lasv;Landroid/net/Uri;)Lasu;

    move-result-object v0

    invoke-virtual {v4, v0}, Lall;->a(Lasu;)Lall;

    goto :goto_1

    :cond_6
    move v0, v1

    .line 138
    goto :goto_3

    .line 142
    :cond_7
    invoke-direct {p0, v6}, Lalm;->b(Landroid/net/Uri;)Lasu;

    move-result-object v0

    invoke-virtual {v4, v0}, Lall;->a(Lasu;)Lall;

    goto :goto_1

    .line 146
    :cond_8
    invoke-virtual {v4}, Lall;->a()Lalk;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lalk;)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lalm;->e:Lalp;

    invoke-interface {v0, p0, p1}, Lalp;->a(Lalm;Lalk;)V

    .line 152
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    check-cast p1, [Lavz;

    invoke-virtual {p0, p1}, Lalm;->a([Lavz;)Lalk;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lalk;

    invoke-virtual {p0, p1}, Lalm;->a(Lalk;)V

    return-void
.end method
