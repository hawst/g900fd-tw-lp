.class public final Lmkj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwk;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8422
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8425
    iput-object v0, p0, Lmkj;->apiHeader:Llyr;

    .line 8428
    iput-object v0, p0, Lmkj;->a:Lnwk;

    .line 8422
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8445
    const/4 v0, 0x0

    .line 8446
    iget-object v1, p0, Lmkj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 8447
    const/4 v0, 0x1

    iget-object v1, p0, Lmkj;->apiHeader:Llyr;

    .line 8448
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8450
    :cond_0
    iget-object v1, p0, Lmkj;->a:Lnwk;

    if-eqz v1, :cond_1

    .line 8451
    const/4 v1, 0x2

    iget-object v2, p0, Lmkj;->a:Lnwk;

    .line 8452
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8454
    :cond_1
    iget-object v1, p0, Lmkj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8455
    iput v0, p0, Lmkj;->ai:I

    .line 8456
    return v0
.end method

.method public a(Loxn;)Lmkj;
    .locals 2

    .prologue
    .line 8464
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8465
    sparse-switch v0, :sswitch_data_0

    .line 8469
    iget-object v1, p0, Lmkj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8470
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkj;->ah:Ljava/util/List;

    .line 8473
    :cond_1
    iget-object v1, p0, Lmkj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8475
    :sswitch_0
    return-object p0

    .line 8480
    :sswitch_1
    iget-object v0, p0, Lmkj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 8481
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkj;->apiHeader:Llyr;

    .line 8483
    :cond_2
    iget-object v0, p0, Lmkj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8487
    :sswitch_2
    iget-object v0, p0, Lmkj;->a:Lnwk;

    if-nez v0, :cond_3

    .line 8488
    new-instance v0, Lnwk;

    invoke-direct {v0}, Lnwk;-><init>()V

    iput-object v0, p0, Lmkj;->a:Lnwk;

    .line 8490
    :cond_3
    iget-object v0, p0, Lmkj;->a:Lnwk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8465
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8433
    iget-object v0, p0, Lmkj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 8434
    const/4 v0, 0x1

    iget-object v1, p0, Lmkj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8436
    :cond_0
    iget-object v0, p0, Lmkj;->a:Lnwk;

    if-eqz v0, :cond_1

    .line 8437
    const/4 v0, 0x2

    iget-object v1, p0, Lmkj;->a:Lnwk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8439
    :cond_1
    iget-object v0, p0, Lmkj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8441
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8418
    invoke-virtual {p0, p1}, Lmkj;->a(Loxn;)Lmkj;

    move-result-object v0

    return-object v0
.end method
