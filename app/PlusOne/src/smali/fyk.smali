.class public final Lfyk;
.super Lfyg;
.source "PG"


# static fields
.field private static b:Z

.field private static c:I

.field private static d:I

.field private static e:Landroid/graphics/Paint;


# instance fields
.field private f:Lfyl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/widget/TextView;Landroid/view/View;Lfyi;Lfyh;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct/range {p0 .. p6}, Lfyg;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/widget/TextView;Landroid/view/View;Lfyi;Lfyh;)V

    .line 48
    sget-boolean v0, Lfyk;->b:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0382

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfyk;->c:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0383

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfyk;->d:I

    const/16 v0, 0x27

    invoke-static {p1, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    sput-object v0, Lfyk;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/4 v0, 0x1

    sput-boolean v0, Lfyk;->b:Z

    .line 49
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    .line 107
    invoke-virtual {p0}, Lfyk;->a()I

    move-result v3

    .line 108
    invoke-super {p0, p1}, Lfyg;->a(I)V

    .line 109
    if-ne p1, v3, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v2, p0, Lfyk;->f:Lfyl;

    if-eqz v2, :cond_0

    .line 114
    const/4 v2, 0x0

    .line 115
    if-ne p1, v1, :cond_2

    .line 123
    :goto_1
    if-eqz v0, :cond_0

    .line 124
    iget-object v1, p0, Lfyk;->f:Lfyl;

    invoke-interface {v1, v0}, Lfyl;->h_(I)V

    goto :goto_0

    .line 117
    :cond_2
    if-ne v3, v1, :cond_3

    move v0, v1

    .line 118
    goto :goto_1

    .line 119
    :cond_3
    if-ne p1, v0, :cond_4

    .line 120
    const/4 v0, 0x3

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method a(Landroid/graphics/Canvas;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 82
    sget-object v0, Lfyk;->e:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 83
    sget v0, Lfyk;->c:I

    int-to-float v0, v0

    sget v1, Lfyk;->d:I

    int-to-float v1, v1

    sget-object v2, Lfyk;->e:Landroid/graphics/Paint;

    .line 84
    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    add-float/2addr v1, v2

    sget-object v2, Lfyk;->e:Landroid/graphics/Paint;

    .line 83
    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 87
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0, p1, p2, p3, p4}, Lfyg;->a(Landroid/widget/AbsListView;III)V

    .line 93
    return-void
.end method

.method public a(Lfyl;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lfyk;->f:Lfyl;

    .line 140
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 97
    invoke-super {p0, p1}, Lfyg;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0}, Lfyg;->b()V

    .line 55
    return-void
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 102
    invoke-super {p0, p1}, Lfyg;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Lfyg;->c()V

    .line 61
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 131
    invoke-super {p0}, Lfyg;->d()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 143
    const-string v0, ""

    iput-object v0, p0, Lfyk;->a:Ljava/lang/String;

    .line 144
    invoke-super {p0}, Lfyg;->e()V

    .line 145
    return-void
.end method
