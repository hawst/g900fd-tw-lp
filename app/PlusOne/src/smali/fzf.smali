.class public final Lfzf;
.super Llch;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Libe;
.implements Llda;


# instance fields
.field private A:Z

.field private B:Liwk;

.field private c:Llcu;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Landroid/text/StaticLayout;

.field private i:Landroid/text/StaticLayout;

.field private j:Landroid/widget/TextView;

.field private k:Llbx;

.field private l:I

.field private y:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfzf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfzf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 84
    invoke-direct {p0, p1, p2, p3}, Llch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    invoke-virtual {p0, v0}, Lfzf;->setClickable(Z)V

    .line 86
    invoke-virtual {p0, v0}, Lfzf;->setFocusable(Z)V

    .line 88
    new-instance v0, Llcu;

    invoke-direct {v0, p0}, Llcu;-><init>(Llda;)V

    iput-object v0, p0, Lfzf;->c:Llcu;

    .line 90
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    .line 92
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 94
    new-instance v1, Liwk;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-direct {v1, p1, v0}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v0, Lixj;

    .line 95
    invoke-virtual {v1, v0}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lfzf;->B:Liwk;

    .line 96
    return-void
.end method


# virtual methods
.method protected a(IIII)I
    .locals 9

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 191
    sget-object v0, Lfzf;->z:Llct;

    iget v0, v0, Llct;->aZ:I

    add-int v8, p2, v0

    .line 193
    iget-object v0, p0, Lfzf;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfzf;->A:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lfzf;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x22

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    :goto_0
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lfzf;->d:Ljava/lang/String;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move v3, p4

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lfzf;->h:Landroid/text/StaticLayout;

    iget-object v0, p0, Lfzf;->h:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v8

    move v8, v0

    .line 195
    :cond_0
    iget-object v0, p0, Lfzf;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lfzf;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v1, p0, Lfzf;->A:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x24

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    :goto_1
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lfzf;->e:Ljava/lang/String;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move v3, p4

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lfzf;->i:Landroid/text/StaticLayout;

    iget-object v0, p0, Lfzf;->i:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v8, v0

    .line 197
    :cond_1
    sget-object v0, Lfzf;->z:Llct;

    iget v0, v0, Llct;->aZ:I

    add-int/2addr v0, v8

    .line 199
    int-to-float v1, v0

    iget-boolean v0, p0, Lfzf;->A:Z

    if-eqz v0, :cond_4

    sget-object v0, Lfzf;->z:Llct;

    iget-object v0, v0, Llct;->x:Landroid/graphics/Paint;

    .line 200
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    .line 201
    :goto_2
    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 203
    iput v0, p0, Lfzf;->g:I

    .line 205
    iget v1, p0, Lfzf;->t:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 207
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 208
    :goto_3
    iget v1, p0, Lfzf;->l:I

    if-ge v7, v1, :cond_5

    .line 209
    invoke-virtual {p0, v7}, Lfzf;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 210
    invoke-virtual {v1, v2, v3}, Landroid/view/View;->measure(II)V

    .line 211
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 193
    :cond_2
    invoke-virtual {p0}, Lfzf;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x15

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    goto :goto_0

    .line 195
    :cond_3
    const/16 v1, 0xa

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    goto :goto_1

    .line 200
    :cond_4
    sget-object v0, Lfzf;->z:Llct;

    iget-object v0, v0, Llct;->w:Landroid/graphics/Paint;

    .line 201
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    goto :goto_2

    .line 214
    :cond_5
    iget-object v1, p0, Lfzf;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 215
    invoke-virtual {p0}, Lfzf;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lfzf;->j:Landroid/widget/TextView;

    iget-boolean v1, p0, Lfzf;->A:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x1f

    :goto_4
    invoke-static {v4, v5, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 218
    iget-object v1, p0, Lfzf;->j:Landroid/widget/TextView;

    iget-object v4, p0, Lfzf;->f:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v1, p0, Lfzf;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->measure(II)V

    .line 220
    iget-object v1, p0, Lfzf;->j:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_6
    return v0

    .line 215
    :cond_7
    const/16 v1, 0xb

    goto :goto_4
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 6

    .prologue
    .line 254
    .line 255
    sget-object v0, Lfzf;->z:Llct;

    iget v0, v0, Llct;->aZ:I

    add-int/2addr v0, p6

    .line 256
    iget-object v1, p0, Lfzf;->h:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    int-to-float v1, p4

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lfzf;->h:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, p4

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lfzf;->h:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_0
    iget-object v1, p0, Lfzf;->i:Landroid/text/StaticLayout;

    if-eqz v1, :cond_1

    int-to-float v1, p4

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lfzf;->i:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, p4

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lfzf;->i:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_1
    sget-object v1, Lfzf;->z:Llct;

    iget v1, v1, Llct;->aZ:I

    add-int/2addr v0, v1

    .line 262
    iget-boolean v1, p0, Lfzf;->A:Z

    if-eqz v1, :cond_2

    .line 263
    int-to-float v1, p4

    int-to-float v2, v0

    int-to-float v3, p5

    int-to-float v4, v0

    sget-object v0, Lfzf;->z:Llct;

    iget-object v5, v0, Llct;->x:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 270
    :goto_0
    invoke-virtual {p0}, Lfzf;->getHeight()I

    move-result v0

    add-int/2addr v0, p6

    .line 271
    return v0

    .line 266
    :cond_2
    const/4 v1, 0x0

    int-to-float v2, v0

    iget v3, p0, Lfzf;->t:I

    int-to-float v3, v3

    int-to-float v4, v0

    sget-object v0, Lfzf;->z:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public a(IZ)Landroid/view/View;
    .locals 7

    .prologue
    .line 176
    new-instance v0, Lgbl;

    invoke-virtual {p0}, Lfzf;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgbl;-><init>(Landroid/content/Context;)V

    .line 177
    iget-object v1, p0, Lfzf;->k:Llbx;

    invoke-virtual {v1}, Llbx;->d()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llan;

    iget v3, p0, Lfzf;->b:I

    iget-object v4, p0, Lfzf;->a:Llci;

    iget-object v5, p0, Lfzf;->y:Ljava/lang/String;

    if-eqz p2, :cond_0

    iget-object v1, p0, Lfzf;->f:Ljava/lang/String;

    .line 179
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v6, 0x1

    :goto_0
    move-object v1, p0

    .line 177
    invoke-virtual/range {v0 .. v6}, Lgbl;->a(Llch;Llan;ILlci;Ljava/lang/String;Z)V

    .line 180
    return-object v0

    .line 179
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Libd;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 393
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 394
    :goto_0
    iget v0, p0, Lfzf;->l:I

    if-ge v1, v0, :cond_1

    .line 395
    invoke-virtual {p0, v1}, Lfzf;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 396
    instance-of v3, v0, Lgbl;

    if-eqz v3, :cond_0

    .line 397
    invoke-static {v0, p1}, Llii;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 398
    check-cast v0, Lgbl;

    .line 399
    iget v3, p0, Lfzf;->b:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move v3, v2

    .line 400
    :goto_1
    new-instance v5, Libd;

    .line 401
    invoke-virtual {v0}, Lgbl;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lgbl;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v6, v0, v3}, Libd;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 400
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 399
    :pswitch_1
    const/16 v3, 0x5d

    goto :goto_1

    :pswitch_2
    const/16 v3, 0x32

    goto :goto_1

    :pswitch_3
    const/16 v3, 0xe8

    goto :goto_1

    :pswitch_4
    const/16 v3, 0x62

    goto :goto_1

    .line 404
    :cond_1
    return-object v4

    .line 399
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 323
    invoke-super {p0}, Llch;->a()V

    .line 325
    iget-object v0, p0, Lfzf;->c:Llcu;

    invoke-virtual {v0}, Llcu;->a()V

    .line 326
    invoke-virtual {p0}, Lfzf;->m()V

    .line 328
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 329
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setClickable(Z)V

    .line 332
    iput-object v1, p0, Lfzf;->d:Ljava/lang/String;

    .line 333
    iput-object v1, p0, Lfzf;->e:Ljava/lang/String;

    .line 334
    iput-object v1, p0, Lfzf;->f:Ljava/lang/String;

    .line 335
    iput-object v1, p0, Lfzf;->h:Landroid/text/StaticLayout;

    .line 336
    iput-object v1, p0, Lfzf;->i:Landroid/text/StaticLayout;

    .line 338
    iput-object v1, p0, Lfzf;->k:Llbx;

    .line 339
    iput v2, p0, Lfzf;->l:I

    .line 340
    iput-object v1, p0, Lfzf;->y:Ljava/lang/String;

    .line 341
    iput-boolean v2, p0, Lfzf;->A:Z

    .line 342
    return-void
.end method

.method protected a(Landroid/database/Cursor;Llcr;I)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    .line 108
    invoke-super {p0, p1, p2, p3}, Llch;->a(Landroid/database/Cursor;Llcr;I)V

    .line 109
    const/16 v1, 0x1b

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 111
    if-eqz v1, :cond_1

    .line 112
    invoke-static {v1}, Llbx;->a([B)Llbx;

    move-result-object v1

    iput-object v1, p0, Lfzf;->k:Llbx;

    .line 113
    iget-object v1, p0, Lfzf;->k:Llbx;

    invoke-virtual {v1}, Llbx;->d()Ljava/util/ArrayList;

    move-result-object v1

    .line 114
    iget v2, p0, Lfzf;->b:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const v0, 0x7fffffff

    :pswitch_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lfzf;->l:I

    .line 115
    iget-object v0, p0, Lfzf;->k:Llbx;

    .line 116
    invoke-virtual {v0}, Llbx;->e()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfzf;->A:Z

    .line 117
    iget-object v0, p0, Lfzf;->c:Llcu;

    iget v2, p0, Lfzf;->l:I

    invoke-virtual {v0, v1, v2}, Llcu;->a(Ljava/util/List;I)V

    .line 119
    iget-object v0, p0, Lfzf;->k:Llbx;

    invoke-virtual {v0}, Llbx;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzf;->d:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lfzf;->k:Llbx;

    invoke-virtual {v0}, Llbx;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzf;->e:Ljava/lang/String;

    .line 121
    iget-object v0, p0, Lfzf;->k:Llbx;

    invoke-virtual {v0}, Llbx;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzf;->f:Ljava/lang/String;

    .line 123
    invoke-virtual {p0}, Lfzf;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 124
    if-eqz v0, :cond_0

    .line 125
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "domain_name"

    .line 126
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzf;->y:Ljava/lang/String;

    .line 128
    :cond_0
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 130
    iget-boolean v0, p0, Lfzf;->A:Z

    if-eqz v0, :cond_3

    .line 131
    invoke-virtual {p0, p0}, Lfzf;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    :goto_1
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    sget-object v1, Lfzf;->z:Llct;

    iget v1, v1, Llct;->m:I

    sget-object v2, Lfzf;->z:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Lfzf;->z:Llct;

    iget v3, v3, Llct;->m:I

    sget-object v4, Lfzf;->z:Llct;

    iget v4, v4, Llct;->m:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 141
    :cond_1
    return-void

    .line 116
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 133
    :cond_3
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    const v1, 0x7f020416

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 135
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 114
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(ZIIII)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 228
    invoke-super/range {p0 .. p5}, Llch;->a(ZIIII)V

    .line 230
    iget v1, p0, Lfzf;->g:I

    .line 231
    iget v2, p0, Lfzf;->t:I

    const/high16 v3, 0x40000000    # 2.0f

    .line 232
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 233
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 234
    :goto_0
    iget v4, p0, Lfzf;->l:I

    if-ge v0, v4, :cond_0

    .line 235
    invoke-virtual {p0, v0}, Lfzf;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 236
    invoke-virtual {v4, v2, v3}, Landroid/view/View;->measure(II)V

    .line 237
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 238
    iget-object v6, p0, Lfzf;->q:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lfzf;->q:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget v8, p0, Lfzf;->t:I

    add-int/2addr v7, v8

    add-int v8, v1, v5

    invoke-virtual {v4, v6, v1, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 241
    add-int/2addr v1, v5

    .line 234
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_0
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 245
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    iget-object v2, p0, Lfzf;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lfzf;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lfzf;->t:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lfzf;->j:Landroid/widget/TextView;

    .line 247
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 245
    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 249
    :cond_1
    return-void
.end method

.method protected aG_()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lfzf;->A:Z

    return v0
.end method

.method public d()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 100
    iget-boolean v0, p0, Lfzf;->A:Z

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {p0}, Lfzf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Llch;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lfzf;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfzf;->addView(Landroid/view/View;)V

    .line 167
    :cond_0
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 5
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 152
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 153
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lfzf;->d:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 154
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lfzf;->e:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 155
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lfzf;->l:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 185
    invoke-virtual {p0}, Lfzf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lgbl;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public j()Llcu;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lfzf;->c:Llcu;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 346
    if-eq p1, p0, :cond_0

    iget-object v0, p0, Lfzf;->j:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfzf;->a:Llci;

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lfzf;->B:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 349
    invoke-virtual {p0}, Lfzf;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfzf;->B:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 354
    :cond_1
    :goto_0
    return-void

    .line 352
    :cond_2
    iget-object v0, p0, Lfzf;->a:Llci;

    iget v1, p0, Lfzf;->b:I

    invoke-interface {v0, v1}, Llci;->h(I)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lfzf;->c:Llcu;

    invoke-virtual {v0, p1}, Llcu;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Llch;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lfzf;->c:Llcu;

    invoke-virtual {v0, p1}, Llcu;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Llch;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
