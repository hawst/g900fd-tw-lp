.class public final Lnke;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnke;


# instance fields
.field private b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3209
    const/4 v0, 0x0

    new-array v0, v0, [Lnke;

    sput-object v0, Lnke;->a:[Lnke;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 3210
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3223
    iput v0, p0, Lnke;->b:I

    .line 3226
    iput v0, p0, Lnke;->c:I

    .line 3210
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 3243
    const/4 v0, 0x0

    .line 3244
    iget v1, p0, Lnke;->b:I

    if-eq v1, v2, :cond_0

    .line 3245
    const/4 v0, 0x1

    iget v1, p0, Lnke;->b:I

    .line 3246
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3248
    :cond_0
    iget v1, p0, Lnke;->c:I

    if-eq v1, v2, :cond_1

    .line 3249
    const/4 v1, 0x2

    iget v2, p0, Lnke;->c:I

    .line 3250
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3252
    :cond_1
    iget-object v1, p0, Lnke;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3253
    iput v0, p0, Lnke;->ai:I

    .line 3254
    return v0
.end method

.method public a(Loxn;)Lnke;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3262
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3263
    sparse-switch v0, :sswitch_data_0

    .line 3267
    iget-object v1, p0, Lnke;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3268
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnke;->ah:Ljava/util/List;

    .line 3271
    :cond_1
    iget-object v1, p0, Lnke;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3273
    :sswitch_0
    return-object p0

    .line 3278
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3279
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 3282
    :cond_2
    iput v0, p0, Lnke;->b:I

    goto :goto_0

    .line 3284
    :cond_3
    iput v2, p0, Lnke;->b:I

    goto :goto_0

    .line 3289
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3290
    if-ne v0, v2, :cond_4

    .line 3291
    iput v0, p0, Lnke;->c:I

    goto :goto_0

    .line 3293
    :cond_4
    iput v2, p0, Lnke;->c:I

    goto :goto_0

    .line 3263
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 3231
    iget v0, p0, Lnke;->b:I

    if-eq v0, v2, :cond_0

    .line 3232
    const/4 v0, 0x1

    iget v1, p0, Lnke;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3234
    :cond_0
    iget v0, p0, Lnke;->c:I

    if-eq v0, v2, :cond_1

    .line 3235
    const/4 v0, 0x2

    iget v1, p0, Lnke;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3237
    :cond_1
    iget-object v0, p0, Lnke;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3239
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3206
    invoke-virtual {p0, p1}, Lnke;->a(Loxn;)Lnke;

    move-result-object v0

    return-object v0
.end method
