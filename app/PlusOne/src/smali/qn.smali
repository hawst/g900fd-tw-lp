.class public Lqn;
.super Loo;
.source "PG"

# interfaces
.implements Lsv;


# static fields
.field private static final e:Z


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Lrb;

.field private E:Z

.field private F:Lky;

.field private G:Lky;

.field private H:Lla;

.field a:Lqr;

.field b:Lxn;

.field c:Lxo;

.field d:Z

.field private f:Landroid/content/Context;

.field private g:Landroid/content/Context;

.field private h:Lz;

.field private i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

.field private j:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private k:Ltp;

.field private l:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private m:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private n:Landroid/view/View;

.field private o:Ltx;

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lqs;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lqs;

.field private r:I

.field private s:Z

.field private t:Z

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private v:I

.field private w:Z

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    const-class v0, Lqn;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lqn;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Los;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    invoke-direct {p0}, Loo;-><init>()V

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lqn;->p:Ljava/util/ArrayList;

    .line 100
    const/4 v0, -0x1

    iput v0, p0, Lqn;->r:I

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lqn;->u:Ljava/util/ArrayList;

    .line 120
    iput v2, p0, Lqn;->x:I

    .line 122
    iput-boolean v1, p0, Lqn;->y:Z

    .line 127
    iput-boolean v1, p0, Lqn;->C:Z

    .line 135
    new-instance v0, Lqo;

    invoke-direct {v0, p0}, Lqo;-><init>(Lqn;)V

    iput-object v0, p0, Lqn;->F:Lky;

    .line 155
    new-instance v0, Lqp;

    invoke-direct {v0, p0}, Lqp;-><init>(Lqn;)V

    iput-object v0, p0, Lqn;->G:Lky;

    .line 163
    new-instance v0, Lqq;

    invoke-direct {v0, p0}, Lqq;-><init>(Lqn;)V

    iput-object v0, p0, Lqn;->H:Lla;

    .line 173
    iput-object p1, p0, Lqn;->h:Lz;

    .line 174
    invoke-virtual {p1}, Los;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    .line 176
    const v0, 0x7f100120

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a(Lsv;)V

    :cond_0
    const v0, 0x7f100122

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v4, v0, Ltp;

    if-eqz v4, :cond_2

    check-cast v0, Ltp;

    :goto_0
    iput-object v0, p0, Lqn;->k:Ltp;

    const v0, 0x7f100123

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Lqn;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    const v0, 0x7f100121

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    const v0, 0x7f100066

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, p0, Lqn;->k:Ltp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lqn;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-nez v0, :cond_4

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    instance-of v4, v0, Landroid/support/v7/widget/Toolbar;

    if-eqz v4, :cond_3

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->r()Ltp;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t make a decor toolbar out of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->b()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lqn;->f:Landroid/content/Context;

    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_1
    iput v0, p0, Lqn;->v:I

    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->m()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_a

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    iput-boolean v1, p0, Lqn;->s:Z

    :cond_5
    iget-object v0, p0, Lqn;->f:Landroid/content/Context;

    invoke-static {v0}, Lqu;->a(Landroid/content/Context;)Lqu;

    move-result-object v0

    invoke-virtual {v0}, Lqu;->f()Z

    invoke-virtual {p0}, Lqn;->h()V

    invoke-virtual {v0}, Lqu;->d()Z

    move-result v0

    invoke-direct {p0, v0}, Lqn;->n(Z)V

    iget-object v0, p0, Lqn;->f:Landroid/content/Context;

    const/4 v4, 0x0

    sget-object v5, Lqk;->a:[I

    const v6, 0x7f0100af

    invoke-virtual {v0, v4, v5, v6, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v4, 0x14

    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p0, v1}, Lqn;->f(Z)V

    :cond_6
    const/16 v1, 0x19

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    if-eqz v1, :cond_7

    int-to-float v1, v1

    invoke-virtual {p0, v1}, Lqn;->a(F)V

    :cond_7
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 177
    if-nez p2, :cond_8

    .line 178
    const v0, 0x1020002

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lqn;->n:Landroid/view/View;

    .line 180
    :cond_8
    return-void

    :cond_9
    move v0, v2

    .line 176
    goto :goto_1

    :cond_a
    move v0, v2

    goto :goto_2
.end method

.method static synthetic a(Lqn;Lrb;)Lrb;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lqn;->D:Lrb;

    return-object p1
.end method

.method static synthetic a(Lqn;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lqn;->y:Z

    return v0
.end method

.method static synthetic a(ZZZ)Z
    .locals 1

    .prologue
    .line 75
    invoke-static {p0, p1, p2}, Lqn;->b(ZZZ)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lqn;)Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lqn;->n:Landroid/view/View;

    return-object v0
.end method

.method private static b(ZZZ)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 738
    if-eqz p2, :cond_1

    .line 743
    :cond_0
    :goto_0
    return v0

    .line 740
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_0

    .line 741
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lqn;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method static synthetic d(Lqn;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method static synthetic e(Lqn;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lqn;->v:I

    return v0
.end method

.method static synthetic f(Lqn;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    return-object v0
.end method

.method static synthetic g(Lqn;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lqn;->z:Z

    return v0
.end method

.method static synthetic h(Lqn;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lqn;->A:Z

    return v0
.end method

.method static synthetic i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lqn;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    return-object v0
.end method

.method static synthetic j(Lqn;)Ltp;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lqn;->k:Ltp;

    return-object v0
.end method

.method static synthetic k(Lqn;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lqn;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic l(Lqn;)Ltx;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lqn;->o:Ltx;

    return-object v0
.end method

.method private n(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 271
    iput-boolean p1, p0, Lqn;->w:Z

    .line 273
    iget-boolean v0, p0, Lqn;->w:Z

    if-nez v0, :cond_1

    .line 274
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, v3}, Ltp;->a(Ltx;)V

    .line 275
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v3, p0, Lqn;->o:Ltx;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Ltx;)V

    .line 280
    :goto_0
    invoke-virtual {p0}, Lqn;->o()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 281
    :goto_1
    iget-object v3, p0, Lqn;->o:Ltx;

    if-eqz v3, :cond_0

    .line 282
    if-eqz v0, :cond_3

    .line 283
    iget-object v3, p0, Lqn;->o:Ltx;

    invoke-virtual {v3, v2}, Ltx;->setVisibility(I)V

    .line 284
    iget-object v3, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v3, :cond_0

    .line 285
    iget-object v3, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v3}, Liu;->l(Landroid/view/View;)V

    .line 291
    :cond_0
    :goto_2
    iget-object v4, p0, Lqn;->k:Ltp;

    iget-boolean v3, p0, Lqn;->w:Z

    if-nez v3, :cond_4

    if-eqz v0, :cond_4

    move v3, v1

    :goto_3
    invoke-interface {v4, v3}, Ltp;->a(Z)V

    .line 292
    iget-object v3, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-boolean v4, p0, Lqn;->w:Z

    if-nez v4, :cond_5

    if-eqz v0, :cond_5

    :goto_4
    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b(Z)V

    .line 293
    return-void

    .line 277
    :cond_1
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Ltx;)V

    .line 278
    iget-object v0, p0, Lqn;->k:Ltp;

    iget-object v3, p0, Lqn;->o:Ltx;

    invoke-interface {v0, v3}, Ltp;->a(Ltx;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 280
    goto :goto_1

    .line 288
    :cond_3
    iget-object v3, p0, Lqn;->o:Ltx;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Ltx;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v3, v2

    .line 291
    goto :goto_3

    :cond_5
    move v1, v2

    .line 292
    goto :goto_4
.end method

.method private o(Z)V
    .locals 3

    .prologue
    .line 749
    iget-boolean v0, p0, Lqn;->z:Z

    iget-boolean v1, p0, Lqn;->A:Z

    iget-boolean v2, p0, Lqn;->B:Z

    invoke-static {v0, v1, v2}, Lqn;->b(ZZZ)Z

    move-result v0

    .line 752
    if-eqz v0, :cond_1

    .line 753
    iget-boolean v0, p0, Lqn;->C:Z

    if-nez v0, :cond_0

    .line 754
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqn;->C:Z

    .line 755
    invoke-virtual {p0, p1}, Lqn;->k(Z)V

    .line 763
    :cond_0
    :goto_0
    return-void

    .line 758
    :cond_1
    iget-boolean v0, p0, Lqn;->C:Z

    if-eqz v0, :cond_0

    .line 759
    const/4 v0, 0x0

    iput-boolean v0, p0, Lqn;->C:Z

    .line 760
    invoke-virtual {p0, p1}, Lqn;->l(Z)V

    goto :goto_0
.end method

.method private t()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 296
    iget-object v0, p0, Lqn;->o:Ltx;

    if-eqz v0, :cond_0

    .line 317
    :goto_0
    return-void

    .line 300
    :cond_0
    new-instance v0, Ltx;

    iget-object v1, p0, Lqn;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Ltx;-><init>(Landroid/content/Context;)V

    .line 302
    iget-boolean v1, p0, Lqn;->w:Z

    if-eqz v1, :cond_1

    .line 303
    invoke-virtual {v0, v3}, Ltx;->setVisibility(I)V

    .line 304
    iget-object v1, p0, Lqn;->k:Ltp;

    invoke-interface {v1, v0}, Ltp;->a(Ltx;)V

    .line 316
    :goto_1
    iput-object v0, p0, Lqn;->o:Ltx;

    goto :goto_0

    .line 306
    :cond_1
    invoke-virtual {p0}, Lqn;->o()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 307
    invoke-virtual {v0, v3}, Ltx;->setVisibility(I)V

    .line 308
    iget-object v1, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_2

    .line 309
    iget-object v1, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v1}, Liu;->l(Landroid/view/View;)V

    .line 314
    :cond_2
    :goto_2
    iget-object v1, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Ltx;)V

    goto :goto_1

    .line 312
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ltx;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->p()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Lxo;)Lxn;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 503
    iget-object v0, p0, Lqn;->a:Lqr;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lqn;->a:Lqr;

    invoke-virtual {v0}, Lqr;->c()V

    .line 507
    :cond_0
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->c(Z)V

    .line 508
    iget-object v0, p0, Lqn;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->e()V

    .line 509
    new-instance v0, Lqr;

    invoke-direct {v0, p0, p1}, Lqr;-><init>(Lqn;Lxo;)V

    .line 510
    invoke-virtual {v0}, Lqr;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 511
    invoke-virtual {v0}, Lqr;->d()V

    .line 512
    iget-object v1, p0, Lqn;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Lxn;)V

    .line 513
    invoke-virtual {p0, v3}, Lqn;->m(Z)V

    .line 514
    iget-object v1, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v1, :cond_1

    iget v1, p0, Lqn;->v:I

    if-ne v1, v3, :cond_1

    .line 516
    iget-object v1, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 517
    iget-object v1, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 518
    iget-object v1, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_1

    .line 519
    iget-object v1, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v1}, Liu;->l(Landroid/view/View;)V

    .line 523
    :cond_1
    iget-object v1, p0, Lqn;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 524
    iput-object v0, p0, Lqn;->a:Lqr;

    .line 527
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, p1}, Liu;->f(Landroid/view/View;F)V

    .line 256
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, p1}, Liu;->f(Landroid/view/View;F)V

    .line 259
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 367
    invoke-virtual {p0}, Lqn;->i()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lqn;->k:Ltp;

    invoke-interface {v1}, Ltp;->a()Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lqn;->a(Landroid/view/View;)V

    .line 369
    return-void
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 461
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->m()I

    move-result v0

    .line 462
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    .line 463
    const/4 v1, 0x1

    iput-boolean v1, p0, Lqn;->s:Z

    .line 465
    :cond_0
    iget-object v1, p0, Lqn;->k:Ltp;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, Ltp;->a(I)V

    .line 466
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1333
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1334
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1233
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->a(Landroid/view/View;)V

    .line 1234
    return-void
.end method

.method public a(Landroid/view/View;Lop;)V
    .locals 1

    .prologue
    .line 1238
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1239
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->a(Landroid/view/View;)V

    .line 1240
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->b(Ljava/lang/CharSequence;)V

    .line 442
    return-void
.end method

.method public a(Loq;)V
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lqn;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lqn;->a(Loq;Z)V

    .line 550
    return-void
.end method

.method public a(Loq;Z)V
    .locals 3

    .prologue
    .line 559
    invoke-direct {p0}, Lqn;->t()V

    .line 560
    iget-object v0, p0, Lqn;->o:Ltx;

    invoke-virtual {v0, p1, p2}, Ltx;->a(Loq;Z)V

    .line 561
    iget-object v0, p0, Lqn;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    move-object v0, p1

    check-cast v0, Lqs;

    invoke-virtual {v0}, Lqs;->g()Lor;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action Bar Tab must have a Callback"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0, v1}, Lqs;->a(I)V

    iget-object v2, p0, Lqn;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lqn;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lqn;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqs;

    invoke-virtual {v0, v1}, Lqs;->a(I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 562
    :cond_1
    if-eqz p2, :cond_2

    .line 563
    invoke-virtual {p0, p1}, Lqn;->b(Loq;)V

    .line 565
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 373
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lqn;->a(II)V

    .line 374
    return-void

    .line 373
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->m()I

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->n()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 420
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :pswitch_0
    iget-object v0, p0, Lqn;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loq;

    invoke-virtual {p0, v0}, Lqn;->b(Loq;)V

    .line 418
    :goto_0
    return-void

    .line 417
    :pswitch_1
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->c(I)V

    goto :goto_0

    .line 412
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/graphics/drawable/Drawable;)V

    .line 470
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->c(Ljava/lang/CharSequence;)V

    .line 451
    return-void
.end method

.method public b(Loq;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 614
    invoke-virtual {p0}, Lqn;->o()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 615
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Loq;->a()I

    move-result v0

    :cond_0
    iput v0, p0, Lqn;->r:I

    .line 641
    :cond_1
    :goto_0
    return-void

    .line 619
    :cond_2
    iget-object v1, p0, Lqn;->k:Ltp;

    invoke-interface {v1}, Ltp;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    .line 622
    :goto_1
    iget-object v2, p0, Lqn;->q:Lqs;

    if-ne v2, p1, :cond_5

    .line 623
    iget-object v0, p0, Lqn;->q:Lqs;

    if-eqz v0, :cond_3

    .line 624
    iget-object v0, p0, Lqn;->q:Lqs;

    invoke-virtual {v0}, Lqs;->g()Lor;

    iget-object v0, p0, Lqn;->q:Lqs;

    .line 625
    iget-object v0, p0, Lqn;->o:Ltx;

    invoke-virtual {p1}, Loq;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Ltx;->c(I)V

    .line 638
    :cond_3
    :goto_2
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lat;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 639
    invoke-virtual {v1}, Lat;->b()I

    goto :goto_0

    .line 619
    :cond_4
    iget-object v1, p0, Lqn;->h:Lz;

    invoke-virtual {v1}, Lz;->f()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    invoke-virtual {v1}, Lat;->a()Lat;

    move-result-object v1

    goto :goto_1

    .line 628
    :cond_5
    iget-object v2, p0, Lqn;->o:Ltx;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Loq;->a()I

    move-result v0

    :cond_6
    invoke-virtual {v2, v0}, Ltx;->a(I)V

    .line 629
    iget-object v0, p0, Lqn;->q:Lqs;

    if-eqz v0, :cond_7

    .line 630
    iget-object v0, p0, Lqn;->q:Lqs;

    invoke-virtual {v0}, Lqs;->g()Lor;

    iget-object v0, p0, Lqn;->q:Lqs;

    .line 632
    :cond_7
    check-cast p1, Lqs;

    iput-object p1, p0, Lqn;->q:Lqs;

    .line 633
    iget-object v0, p0, Lqn;->q:Lqs;

    if-eqz v0, :cond_3

    .line 634
    iget-object v0, p0, Lqn;->q:Lqs;

    invoke-virtual {v0}, Lqs;->g()Lor;

    move-result-object v0

    iget-object v2, p0, Lqn;->q:Lqs;

    invoke-interface {v0, v2}, Lor;->a(Loq;)V

    goto :goto_2
.end method

.method public b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 378
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lqn;->a(II)V

    .line 379
    return-void

    .line 378
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Loq;
    .locals 1

    .prologue
    .line 579
    new-instance v0, Lqs;

    invoke-direct {v0, p0}, Lqs;-><init>(Lqn;)V

    return-object v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lqn;->f:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lqn;->a(Ljava/lang/CharSequence;)V

    .line 404
    return-void
.end method

.method public c(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->b(Landroid/graphics/drawable/Drawable;)V

    .line 905
    return-void
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->a(Ljava/lang/CharSequence;)V

    .line 447
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 383
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lqn;->a(II)V

    .line 384
    return-void

    .line 383
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    return v0
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 454
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqn;->s:Z

    .line 457
    :cond_0
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->a(I)V

    .line 458
    return-void
.end method

.method public d(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 388
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lqn;->a(II)V

    .line 389
    return-void

    .line 388
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 659
    iget-boolean v0, p0, Lqn;->z:Z

    if-eqz v0, :cond_0

    .line 660
    iput-boolean v1, p0, Lqn;->z:Z

    .line 661
    invoke-direct {p0, v1}, Lqn;->o(Z)V

    .line 663
    :cond_0
    return-void
.end method

.method public e(I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 1278
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->n()I

    move-result v0

    .line 1279
    packed-switch v0, :pswitch_data_0

    .line 1286
    :goto_0
    if-eq v0, p1, :cond_0

    iget-boolean v0, p0, Lqn;->w:Z

    if-nez v0, :cond_0

    .line 1287
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 1288
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Liu;->l(Landroid/view/View;)V

    .line 1291
    :cond_0
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->b(I)V

    .line 1292
    packed-switch p1, :pswitch_data_1

    .line 1302
    :cond_1
    :goto_1
    iget-object v3, p0, Lqn;->k:Ltp;

    if-ne p1, v6, :cond_2

    iget-boolean v0, p0, Lqn;->w:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, Ltp;->a(Z)V

    .line 1303
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-ne p1, v6, :cond_3

    iget-boolean v3, p0, Lqn;->w:Z

    if-nez v3, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b(Z)V

    .line 1304
    return-void

    .line 1281
    :pswitch_0
    invoke-virtual {p0}, Lqn;->s()I

    move-result v3

    iput v3, p0, Lqn;->r:I

    .line 1282
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lqn;->b(Loq;)V

    .line 1283
    iget-object v3, p0, Lqn;->o:Ltx;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Ltx;->setVisibility(I)V

    goto :goto_0

    .line 1294
    :pswitch_1
    invoke-direct {p0}, Lqn;->t()V

    .line 1295
    iget-object v0, p0, Lqn;->o:Ltx;

    invoke-virtual {v0, v2}, Ltx;->setVisibility(I)V

    .line 1296
    iget v0, p0, Lqn;->r:I

    if-eq v0, v5, :cond_1

    .line 1297
    iget v0, p0, Lqn;->r:I

    invoke-virtual {p0, v0}, Lqn;->b(I)V

    .line 1298
    iput v5, p0, Lqn;->r:I

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1302
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1303
    goto :goto_3

    .line 1279
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    .line 1292
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch
.end method

.method public e(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 393
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lqn;->a(II)V

    .line 394
    return-void

    .line 393
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 684
    iget-boolean v0, p0, Lqn;->z:Z

    if-nez v0, :cond_0

    .line 685
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqn;->z:Z

    .line 686
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lqn;->o(Z)V

    .line 688
    :cond_0
    return-void
.end method

.method public f(I)V
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->e(I)V

    .line 910
    return-void
.end method

.method public f(Z)V
    .locals 2

    .prologue
    .line 709
    if-eqz p1, :cond_0

    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 710
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 713
    :cond_0
    iput-boolean p1, p0, Lqn;->d:Z

    .line 714
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->c(Z)V

    .line 715
    return-void
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 919
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0, p1}, Ltp;->f(I)V

    .line 920
    return-void
.end method

.method public g(Z)V
    .locals 1

    .prologue
    .line 1341
    iget-boolean v0, p0, Lqn;->s:Z

    if-nez v0, :cond_0

    .line 1342
    invoke-virtual {p0, p1}, Lqn;->c(Z)V

    .line 1344
    :cond_0
    return-void
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 864
    invoke-virtual {p0}, Lqn;->d()I

    move-result v0

    .line 866
    iget-boolean v1, p0, Lqn;->C:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lqn;->j()I

    move-result v1

    if-ge v1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lqn;->k:Ltp;

    .line 399
    return-void
.end method

.method public h(I)V
    .locals 0

    .prologue
    .line 328
    iput p1, p0, Lqn;->x:I

    .line 329
    return-void
.end method

.method public h(Z)V
    .locals 1

    .prologue
    .line 339
    iput-boolean p1, p0, Lqn;->E:Z

    .line 340
    if-nez p1, :cond_0

    iget-object v0, p0, Lqn;->D:Lrb;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lqn;->D:Lrb;

    invoke-virtual {v0}, Lrb;->b()V

    .line 343
    :cond_0
    return-void
.end method

.method public i()Landroid/content/Context;
    .locals 4

    .prologue
    .line 882
    iget-object v0, p0, Lqn;->g:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 883
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 884
    iget-object v1, p0, Lqn;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 885
    const v2, 0x7f0100b2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 886
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 888
    if-eqz v0, :cond_1

    .line 889
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lqn;->f:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lqn;->g:Landroid/content/Context;

    .line 894
    :cond_0
    :goto_0
    iget-object v0, p0, Lqn;->g:Landroid/content/Context;

    return-object v0

    .line 891
    :cond_1
    iget-object v0, p0, Lqn;->f:Landroid/content/Context;

    iput-object v0, p0, Lqn;->g:Landroid/content/Context;

    goto :goto_0
.end method

.method public i(Z)V
    .locals 3

    .prologue
    .line 354
    iget-boolean v0, p0, Lqn;->t:Z

    if-ne p1, v0, :cond_1

    .line 363
    :cond_0
    return-void

    .line 357
    :cond_1
    iput-boolean p1, p0, Lqn;->t:Z

    .line 359
    iget-object v0, p0, Lqn;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 360
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 361
    iget-object v2, p0, Lqn;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 360
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->e()I

    move-result v0

    return v0
.end method

.method public j(Z)V
    .locals 0

    .prologue
    .line 654
    iput-boolean p1, p0, Lqn;->y:Z

    .line 655
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lqn;->f:Landroid/content/Context;

    invoke-static {v0}, Lqu;->a(Landroid/content/Context;)Lqu;

    move-result-object v0

    invoke-virtual {v0}, Lqu;->d()Z

    move-result v0

    invoke-direct {p0, v0}, Lqn;->n(Z)V

    .line 268
    return-void
.end method

.method public k(Z)V
    .locals 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 766
    iget-object v0, p0, Lqn;->D:Lrb;

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lqn;->D:Lrb;

    invoke-virtual {v0}, Lrb;->b()V

    .line 769
    :cond_0
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 771
    iget v0, p0, Lqn;->x:I

    if-nez v0, :cond_6

    sget-boolean v0, Lqn;->e:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lqn;->E:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_6

    .line 774
    :cond_1
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Liu;->b(Landroid/view/View;F)V

    .line 775
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 776
    if-eqz p1, :cond_2

    .line 777
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 778
    iget-object v2, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 779
    aget v1, v1, v6

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 781
    :cond_2
    iget-object v1, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1, v0}, Liu;->b(Landroid/view/View;F)V

    .line 782
    new-instance v1, Lrb;

    invoke-direct {v1}, Lrb;-><init>()V

    .line 783
    iget-object v2, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Liu;->j(Landroid/view/View;)Lkj;

    move-result-object v2

    invoke-virtual {v2, v4}, Lkj;->c(F)Lkj;

    move-result-object v2

    .line 784
    iget-object v3, p0, Lqn;->H:Lla;

    invoke-virtual {v2, v3}, Lkj;->a(Lla;)Lkj;

    .line 785
    invoke-virtual {v1, v2}, Lrb;->a(Lkj;)Lrb;

    .line 786
    iget-boolean v2, p0, Lqn;->y:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lqn;->n:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 787
    iget-object v2, p0, Lqn;->n:Landroid/view/View;

    invoke-static {v2, v0}, Liu;->b(Landroid/view/View;F)V

    .line 788
    iget-object v0, p0, Lqn;->n:Landroid/view/View;

    invoke-static {v0}, Liu;->j(Landroid/view/View;)Lkj;

    move-result-object v0

    invoke-virtual {v0, v4}, Lkj;->c(F)Lkj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrb;->a(Lkj;)Lrb;

    .line 790
    :cond_3
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget v0, p0, Lqn;->v:I

    if-ne v0, v6, :cond_4

    .line 791
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v2, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v0, v2}, Liu;->b(Landroid/view/View;F)V

    .line 792
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 793
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Liu;->j(Landroid/view/View;)Lkj;

    move-result-object v0

    invoke-virtual {v0, v4}, Lkj;->c(F)Lkj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrb;->a(Lkj;)Lrb;

    .line 795
    :cond_4
    iget-object v0, p0, Lqn;->f:Landroid/content/Context;

    const v2, 0x10a0006

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrb;->a(Landroid/view/animation/Interpolator;)Lrb;

    .line 797
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Lrb;->a(J)Lrb;

    .line 805
    iget-object v0, p0, Lqn;->G:Lky;

    invoke-virtual {v1, v0}, Lrb;->a(Lky;)Lrb;

    .line 806
    iput-object v1, p0, Lqn;->D:Lrb;

    .line 807
    invoke-virtual {v1}, Lrb;->a()V

    .line 821
    :goto_0
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_5

    .line 822
    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Liu;->l(Landroid/view/View;)V

    .line 824
    :cond_5
    return-void

    .line 809
    :cond_6
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v1}, Liu;->c(Landroid/view/View;F)V

    .line 810
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Liu;->b(Landroid/view/View;F)V

    .line 811
    iget-boolean v0, p0, Lqn;->y:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lqn;->n:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 812
    iget-object v0, p0, Lqn;->n:Landroid/view/View;

    invoke-static {v0, v4}, Liu;->b(Landroid/view/View;F)V

    .line 814
    :cond_7
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_8

    iget v0, p0, Lqn;->v:I

    if-ne v0, v6, :cond_8

    .line 815
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v1}, Liu;->c(Landroid/view/View;F)V

    .line 816
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Liu;->b(Landroid/view/View;F)V

    .line 817
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 819
    :cond_8
    iget-object v0, p0, Lqn;->G:Lky;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lky;->b(Landroid/view/View;)V

    goto :goto_0

    .line 777
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public l(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 827
    iget-object v0, p0, Lqn;->D:Lrb;

    if-eqz v0, :cond_0

    .line 828
    iget-object v0, p0, Lqn;->D:Lrb;

    invoke-virtual {v0}, Lrb;->b()V

    .line 831
    :cond_0
    iget v0, p0, Lqn;->x:I

    if-nez v0, :cond_5

    sget-boolean v0, Lqn;->e:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lqn;->E:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_5

    .line 833
    :cond_1
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Liu;->c(Landroid/view/View;F)V

    .line 834
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Z)V

    .line 835
    new-instance v1, Lrb;

    invoke-direct {v1}, Lrb;-><init>()V

    .line 836
    iget-object v0, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 837
    if-eqz p1, :cond_2

    .line 838
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 839
    iget-object v3, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 840
    aget v2, v2, v5

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 842
    :cond_2
    iget-object v2, p0, Lqn;->j:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Liu;->j(Landroid/view/View;)Lkj;

    move-result-object v2

    invoke-virtual {v2, v0}, Lkj;->c(F)Lkj;

    move-result-object v2

    .line 843
    iget-object v3, p0, Lqn;->H:Lla;

    invoke-virtual {v2, v3}, Lkj;->a(Lla;)Lkj;

    .line 844
    invoke-virtual {v1, v2}, Lrb;->a(Lkj;)Lrb;

    .line 845
    iget-boolean v2, p0, Lqn;->y:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lqn;->n:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 846
    iget-object v2, p0, Lqn;->n:Landroid/view/View;

    invoke-static {v2}, Liu;->j(Landroid/view/View;)Lkj;

    move-result-object v2

    invoke-virtual {v2, v0}, Lkj;->c(F)Lkj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrb;->a(Lkj;)Lrb;

    .line 848
    :cond_3
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 849
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Liu;->c(Landroid/view/View;F)V

    .line 850
    iget-object v0, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Liu;->j(Landroid/view/View;)Lkj;

    move-result-object v0

    iget-object v2, p0, Lqn;->m:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lkj;->c(F)Lkj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrb;->a(Lkj;)Lrb;

    .line 852
    :cond_4
    iget-object v0, p0, Lqn;->f:Landroid/content/Context;

    const v2, 0x10a0005

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrb;->a(Landroid/view/animation/Interpolator;)Lrb;

    .line 854
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Lrb;->a(J)Lrb;

    .line 855
    iget-object v0, p0, Lqn;->F:Lky;

    invoke-virtual {v1, v0}, Lrb;->a(Lky;)Lrb;

    .line 856
    iput-object v1, p0, Lqn;->D:Lrb;

    .line 857
    invoke-virtual {v1}, Lrb;->a()V

    .line 861
    :goto_0
    return-void

    .line 859
    :cond_5
    iget-object v0, p0, Lqn;->F:Lky;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lky;->b(Landroid/view/View;)V

    goto :goto_0

    .line 838
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public m(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 870
    if-eqz p1, :cond_2

    .line 871
    iget-boolean v0, p0, Lqn;->B:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lqn;->B:Z

    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b()V

    :cond_0
    invoke-direct {p0, v2}, Lqn;->o(Z)V

    .line 876
    :cond_1
    :goto_0
    iget-object v3, p0, Lqn;->k:Ltp;

    if-eqz p1, :cond_4

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Ltp;->d(I)V

    .line 877
    iget-object v0, p0, Lqn;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz p1, :cond_5

    :goto_2
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->b(I)V

    .line 879
    return-void

    .line 873
    :cond_2
    iget-boolean v0, p0, Lqn;->B:Z

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lqn;->B:Z

    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lqn;->i:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b()V

    :cond_3
    invoke-direct {p0, v2}, Lqn;->o(Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 876
    goto :goto_1

    :cond_5
    move v2, v1

    .line 877
    goto :goto_2
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lqn;->k:Ltp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->e()V

    .line 938
    const/4 v0, 0x1

    .line 940
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    iget-object v0, p0, Lqn;->c:Lxo;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lqn;->c:Lxo;

    iget-object v1, p0, Lqn;->b:Lxn;

    invoke-interface {v0, v1}, Lxo;->a(Lxn;)V

    .line 322
    iput-object v2, p0, Lqn;->b:Lxn;

    .line 323
    iput-object v2, p0, Lqn;->c:Lxo;

    .line 325
    :cond_0
    return-void
.end method

.method public o()I
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->n()I

    move-result v0

    return v0
.end method

.method public p()V
    .locals 1

    .prologue
    .line 676
    iget-boolean v0, p0, Lqn;->A:Z

    if-eqz v0, :cond_0

    .line 677
    const/4 v0, 0x0

    iput-boolean v0, p0, Lqn;->A:Z

    .line 678
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lqn;->o(Z)V

    .line 680
    :cond_0
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 701
    iget-boolean v0, p0, Lqn;->A:Z

    if-nez v0, :cond_0

    .line 702
    iput-boolean v1, p0, Lqn;->A:Z

    .line 703
    invoke-direct {p0, v1}, Lqn;->o(Z)V

    .line 705
    :cond_0
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lqn;->D:Lrb;

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lqn;->D:Lrb;

    invoke-virtual {v0}, Lrb;->b()V

    .line 926
    const/4 v0, 0x0

    iput-object v0, p0, Lqn;->D:Lrb;

    .line 928
    :cond_0
    return-void
.end method

.method public s()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 1249
    iget-object v1, p0, Lqn;->k:Ltp;

    invoke-interface {v1}, Ltp;->n()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1255
    :cond_0
    :goto_0
    return v0

    .line 1251
    :pswitch_0
    iget-object v1, p0, Lqn;->q:Lqs;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lqn;->q:Lqs;

    invoke-virtual {v0}, Lqs;->a()I

    move-result v0

    goto :goto_0

    .line 1253
    :pswitch_1
    iget-object v0, p0, Lqn;->k:Ltp;

    invoke-interface {v0}, Ltp;->o()I

    move-result v0

    goto :goto_0

    .line 1249
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
