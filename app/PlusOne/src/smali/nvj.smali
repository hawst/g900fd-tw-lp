.class public final Lnvj;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lods;

.field private b:[I

.field private c:[Lods;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2903
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2906
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnvj;->b:[I

    .line 2909
    sget-object v0, Lods;->a:[Lods;

    iput-object v0, p0, Lnvj;->c:[Lods;

    .line 2912
    sget-object v0, Lods;->a:[Lods;

    iput-object v0, p0, Lnvj;->a:[Lods;

    .line 2903
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2942
    .line 2943
    iget-object v0, p0, Lnvj;->b:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lnvj;->b:[I

    array-length v0, v0

    if-lez v0, :cond_5

    .line 2945
    iget-object v3, p0, Lnvj;->b:[I

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    .line 2947
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 2945
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2950
    :cond_0
    iget-object v0, p0, Lnvj;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 2952
    :goto_1
    iget-object v2, p0, Lnvj;->c:[Lods;

    if-eqz v2, :cond_2

    .line 2953
    iget-object v3, p0, Lnvj;->c:[Lods;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2954
    if-eqz v5, :cond_1

    .line 2955
    const/4 v6, 0x2

    .line 2956
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2953
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2960
    :cond_2
    iget-object v2, p0, Lnvj;->a:[Lods;

    if-eqz v2, :cond_4

    .line 2961
    iget-object v2, p0, Lnvj;->a:[Lods;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 2962
    if-eqz v4, :cond_3

    .line 2963
    const/4 v5, 0x3

    .line 2964
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2961
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2968
    :cond_4
    iget-object v1, p0, Lnvj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2969
    iput v0, p0, Lnvj;->ai:I

    .line 2970
    return v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lnvj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2978
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2979
    sparse-switch v0, :sswitch_data_0

    .line 2983
    iget-object v2, p0, Lnvj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2984
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnvj;->ah:Ljava/util/List;

    .line 2987
    :cond_1
    iget-object v2, p0, Lnvj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2989
    :sswitch_0
    return-object p0

    .line 2994
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2995
    iget-object v0, p0, Lnvj;->b:[I

    array-length v0, v0

    .line 2996
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 2997
    iget-object v3, p0, Lnvj;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2998
    iput-object v2, p0, Lnvj;->b:[I

    .line 2999
    :goto_1
    iget-object v2, p0, Lnvj;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 3000
    iget-object v2, p0, Lnvj;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 3001
    invoke-virtual {p1}, Loxn;->a()I

    .line 2999
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3004
    :cond_2
    iget-object v2, p0, Lnvj;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto :goto_0

    .line 3008
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3009
    iget-object v0, p0, Lnvj;->c:[Lods;

    if-nez v0, :cond_4

    move v0, v1

    .line 3010
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lods;

    .line 3011
    iget-object v3, p0, Lnvj;->c:[Lods;

    if-eqz v3, :cond_3

    .line 3012
    iget-object v3, p0, Lnvj;->c:[Lods;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3014
    :cond_3
    iput-object v2, p0, Lnvj;->c:[Lods;

    .line 3015
    :goto_3
    iget-object v2, p0, Lnvj;->c:[Lods;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 3016
    iget-object v2, p0, Lnvj;->c:[Lods;

    new-instance v3, Lods;

    invoke-direct {v3}, Lods;-><init>()V

    aput-object v3, v2, v0

    .line 3017
    iget-object v2, p0, Lnvj;->c:[Lods;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3018
    invoke-virtual {p1}, Loxn;->a()I

    .line 3015
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 3009
    :cond_4
    iget-object v0, p0, Lnvj;->c:[Lods;

    array-length v0, v0

    goto :goto_2

    .line 3021
    :cond_5
    iget-object v2, p0, Lnvj;->c:[Lods;

    new-instance v3, Lods;

    invoke-direct {v3}, Lods;-><init>()V

    aput-object v3, v2, v0

    .line 3022
    iget-object v2, p0, Lnvj;->c:[Lods;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 3026
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3027
    iget-object v0, p0, Lnvj;->a:[Lods;

    if-nez v0, :cond_7

    move v0, v1

    .line 3028
    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lods;

    .line 3029
    iget-object v3, p0, Lnvj;->a:[Lods;

    if-eqz v3, :cond_6

    .line 3030
    iget-object v3, p0, Lnvj;->a:[Lods;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3032
    :cond_6
    iput-object v2, p0, Lnvj;->a:[Lods;

    .line 3033
    :goto_5
    iget-object v2, p0, Lnvj;->a:[Lods;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 3034
    iget-object v2, p0, Lnvj;->a:[Lods;

    new-instance v3, Lods;

    invoke-direct {v3}, Lods;-><init>()V

    aput-object v3, v2, v0

    .line 3035
    iget-object v2, p0, Lnvj;->a:[Lods;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3036
    invoke-virtual {p1}, Loxn;->a()I

    .line 3033
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 3027
    :cond_7
    iget-object v0, p0, Lnvj;->a:[Lods;

    array-length v0, v0

    goto :goto_4

    .line 3039
    :cond_8
    iget-object v2, p0, Lnvj;->a:[Lods;

    new-instance v3, Lods;

    invoke-direct {v3}, Lods;-><init>()V

    aput-object v3, v2, v0

    .line 3040
    iget-object v2, p0, Lnvj;->a:[Lods;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2979
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2917
    iget-object v1, p0, Lnvj;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnvj;->b:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 2918
    iget-object v2, p0, Lnvj;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 2919
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 2918
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2922
    :cond_0
    iget-object v1, p0, Lnvj;->c:[Lods;

    if-eqz v1, :cond_2

    .line 2923
    iget-object v2, p0, Lnvj;->c:[Lods;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2924
    if-eqz v4, :cond_1

    .line 2925
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2923
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2929
    :cond_2
    iget-object v1, p0, Lnvj;->a:[Lods;

    if-eqz v1, :cond_4

    .line 2930
    iget-object v1, p0, Lnvj;->a:[Lods;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 2931
    if-eqz v3, :cond_3

    .line 2932
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2930
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2936
    :cond_4
    iget-object v0, p0, Lnvj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2938
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2899
    invoke-virtual {p0, p1}, Lnvj;->a(Loxn;)Lnvj;

    move-result-object v0

    return-object v0
.end method
