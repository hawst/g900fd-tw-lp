.class public final Lfiq;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ldf;",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/service/SlideshowService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/SlideshowService;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lfiq;->a:Lcom/google/android/apps/plus/service/SlideshowService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ldf;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 99
    invoke-virtual {v0}, Ldf;->d()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    check-cast p1, [Ldf;

    invoke-virtual {p0, p1}, Lfiq;->a([Ldf;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 104
    if-eqz p1, :cond_0

    .line 105
    iget-object v0, p0, Lfiq;->a:Lcom/google/android/apps/plus/service/SlideshowService;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/service/SlideshowService;->a(Lcom/google/android/apps/plus/service/SlideshowService;Ljava/lang/Object;)V

    .line 112
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Got a null cursor when trying to run slideshow."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_1
    const-string v0, "SlideshowService"

    const-string v1, "Slideshow can\'t advance because provided cursor is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
