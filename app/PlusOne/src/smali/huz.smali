.class public final Lhuz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:Lhjy;

.field private b:Lhjy;

.field private c:Lhjy;

.field private d:Landroid/view/View;

.field private e:Lhm;

.field private f:Lhjz;

.field private g:Lhva;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    if-nez p1, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ActionHandler must be passed a non-null View"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    iput-object p1, p0, Lhuz;->d:Landroid/view/View;

    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhjz;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjz;

    iput-object v0, p0, Lhuz;->f:Lhjz;

    .line 88
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 178
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 181
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 182
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLongClickable(Z)V

    .line 184
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 186
    iput-object v1, p0, Lhuz;->a:Lhjy;

    .line 187
    iput-object v1, p0, Lhuz;->b:Lhjy;

    .line 188
    iput-object v1, p0, Lhuz;->c:Lhjy;

    .line 189
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lhuz;->f:Lhjz;

    .line 198
    iget-object v0, p0, Lhuz;->g:Lhva;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lhuz;->g:Lhva;

    invoke-interface {v0}, Lhva;->b()V

    .line 201
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lhjy;)V
    .locals 3

    .prologue
    .line 97
    if-nez p1, :cond_0

    .line 98
    invoke-direct {p0}, Lhuz;->a()V

    .line 103
    :goto_0
    return-void

    .line 102
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    new-array v1, v1, [Lhjy;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lhuz;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(Lhva;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lhuz;->g:Lhva;

    .line 172
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lhjy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0}, Lhuz;->a()V

    .line 119
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 125
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 126
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjy;

    .line 128
    if-eqz v0, :cond_2

    .line 129
    invoke-interface {v0}, Lhjy;->a()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 143
    const-string v3, "CardActionHandler"

    invoke-interface {v0}, Lhjy;->a()I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x24

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Invalid activation type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 131
    :pswitch_0
    iput-object v0, p0, Lhuz;->a:Lhjy;

    goto :goto_2

    .line 135
    :pswitch_1
    iput-object v0, p0, Lhuz;->b:Lhjy;

    goto :goto_2

    .line 139
    :pswitch_2
    iput-object v0, p0, Lhuz;->c:Lhjy;

    goto :goto_2

    .line 148
    :cond_3
    iget-object v0, p0, Lhuz;->c:Lhjy;

    if-eqz v0, :cond_5

    .line 149
    iget-object v0, p0, Lhuz;->e:Lhm;

    if-nez v0, :cond_4

    .line 150
    new-instance v0, Lhm;

    iget-object v1, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lhm;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lhuz;->e:Lhm;

    .line 151
    iget-object v0, p0, Lhuz;->e:Lhm;

    invoke-virtual {v0, p0}, Lhm;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 154
    :cond_4
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 155
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 158
    :cond_5
    iget-object v0, p0, Lhuz;->a:Lhjy;

    if-eqz v0, :cond_6

    .line 159
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    new-instance v1, Lhmi;

    invoke-direct {v1, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    :cond_6
    iget-object v0, p0, Lhuz;->b:Lhjy;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    new-instance v1, Lhmj;

    invoke-direct {v1, p0}, Lhmj;-><init>(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto/16 :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 211
    iget-object v0, p0, Lhuz;->a:Lhjy;

    invoke-direct {p0}, Lhuz;->b()V

    .line 213
    :cond_0
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lhuz;->c:Lhjy;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lhuz;->c:Lhjy;

    invoke-direct {p0}, Lhuz;->b()V

    .line 266
    const/4 v0, 0x1

    .line 269
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 333
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 337
    return v1
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    return v0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 223
    iget-object v0, p0, Lhuz;->b:Lhjy;

    invoke-direct {p0}, Lhuz;->b()V

    .line 224
    const/4 v0, 0x1

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lhuz;->b:Lhjy;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performLongClick()Z

    .line 325
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 349
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lhuz;->a:Lhjy;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 283
    const/4 v0, 0x1

    .line 286
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 309
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 310
    return v1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lhuz;->d:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 248
    iget-object v0, p0, Lhuz;->e:Lhm;

    invoke-virtual {v0, p2}, Lhm;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 251
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
