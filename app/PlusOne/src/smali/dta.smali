.class public final Ldta;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/lang/Object;

.field private static b:Z

.field private static final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lovf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldta;->a:Ljava/lang/Object;

    .line 29
    const/4 v0, 0x0

    sput-boolean v0, Ldta;->b:Z

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ldta;->c:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Landroid/content/Context;I)Lovf;
    .locals 4

    .prologue
    .line 42
    sget-object v1, Ldta;->c:Ljava/util/HashMap;

    monitor-enter v1

    .line 43
    :try_start_0
    sget-object v0, Ldta;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lovf;

    .line 44
    if-nez v0, :cond_0

    .line 45
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 46
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    .line 47
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 48
    new-instance v0, Ldkj;

    new-instance v2, Lkfo;

    invoke-direct {v2, p0, p1}, Lkfo;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, p0, v2, p1}, Ldkj;-><init>(Landroid/content/Context;Lkfo;I)V

    .line 50
    invoke-virtual {v0}, Ldkj;->l()V

    .line 51
    invoke-virtual {v0}, Ldkj;->t()Z

    move-result v2

    if-nez v2, :cond_1

    .line 52
    sget-object v2, Ldta;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0}, Ldkj;->b()Lovf;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    :cond_0
    :goto_0
    sget-object v0, Ldta;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lovf;

    monitor-exit v1

    return-object v0

    .line 54
    :cond_1
    const-string v2, "EsPhotosFeatures"

    invoke-virtual {v0, v2}, Ldkj;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;IILjava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/ArrayList",
            "<",
            "Lovw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    sget-object v6, Ldta;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 72
    :try_start_0
    sget-boolean v0, Ldta;->b:Z

    if-nez v0, :cond_0

    .line 73
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 74
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 75
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 76
    new-instance v0, Ldnh;

    new-instance v2, Lkfo;

    invoke-direct {v2, p0, p1}, Lkfo;-><init>(Landroid/content/Context;I)V

    move-object v1, p0

    move v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ldnh;-><init>(Landroid/content/Context;Lkfo;IILjava/util/ArrayList;)V

    .line 79
    invoke-virtual {v0}, Ldnh;->l()V

    .line 80
    invoke-virtual {v0}, Ldnh;->t()Z

    move-result v1

    if-nez v1, :cond_1

    .line 81
    const/4 v0, 0x1

    sput-boolean v0, Ldta;->b:Z

    .line 85
    :cond_0
    :goto_0
    monitor-exit v6

    return-void

    .line 83
    :cond_1
    const-string v1, "EsPhotosFeatures"

    invoke-virtual {v0, v1}, Ldnh;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
