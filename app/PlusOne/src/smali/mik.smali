.class public final Lmik;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnep;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26971
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26974
    iput-object v0, p0, Lmik;->apiHeader:Llyq;

    .line 26977
    iput-object v0, p0, Lmik;->a:Lnep;

    .line 26971
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26994
    const/4 v0, 0x0

    .line 26995
    iget-object v1, p0, Lmik;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 26996
    const/4 v0, 0x1

    iget-object v1, p0, Lmik;->apiHeader:Llyq;

    .line 26997
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26999
    :cond_0
    iget-object v1, p0, Lmik;->a:Lnep;

    if-eqz v1, :cond_1

    .line 27000
    const/4 v1, 0x2

    iget-object v2, p0, Lmik;->a:Lnep;

    .line 27001
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27003
    :cond_1
    iget-object v1, p0, Lmik;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27004
    iput v0, p0, Lmik;->ai:I

    .line 27005
    return v0
.end method

.method public a(Loxn;)Lmik;
    .locals 2

    .prologue
    .line 27013
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27014
    sparse-switch v0, :sswitch_data_0

    .line 27018
    iget-object v1, p0, Lmik;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27019
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmik;->ah:Ljava/util/List;

    .line 27022
    :cond_1
    iget-object v1, p0, Lmik;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27024
    :sswitch_0
    return-object p0

    .line 27029
    :sswitch_1
    iget-object v0, p0, Lmik;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 27030
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmik;->apiHeader:Llyq;

    .line 27032
    :cond_2
    iget-object v0, p0, Lmik;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27036
    :sswitch_2
    iget-object v0, p0, Lmik;->a:Lnep;

    if-nez v0, :cond_3

    .line 27037
    new-instance v0, Lnep;

    invoke-direct {v0}, Lnep;-><init>()V

    iput-object v0, p0, Lmik;->a:Lnep;

    .line 27039
    :cond_3
    iget-object v0, p0, Lmik;->a:Lnep;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27014
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26982
    iget-object v0, p0, Lmik;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 26983
    const/4 v0, 0x1

    iget-object v1, p0, Lmik;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26985
    :cond_0
    iget-object v0, p0, Lmik;->a:Lnep;

    if-eqz v0, :cond_1

    .line 26986
    const/4 v0, 0x2

    iget-object v1, p0, Lmik;->a:Lnep;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26988
    :cond_1
    iget-object v0, p0, Lmik;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26990
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26967
    invoke-virtual {p0, p1}, Lmik;->a(Loxn;)Lmik;

    move-result-object v0

    return-object v0
.end method
