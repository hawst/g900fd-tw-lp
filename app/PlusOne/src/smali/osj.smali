.class public final Losj;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1640
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1653
    const/high16 v0, -0x80000000

    iput v0, p0, Losj;->a:I

    .line 1640
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1677
    const/4 v0, 0x0

    .line 1678
    iget v1, p0, Losj;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1679
    const/4 v0, 0x1

    iget v1, p0, Losj;->a:I

    .line 1680
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1682
    :cond_0
    iget-object v1, p0, Losj;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1683
    const/4 v1, 0x2

    iget-object v2, p0, Losj;->b:Ljava/lang/Boolean;

    .line 1684
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1686
    :cond_1
    iget-object v1, p0, Losj;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1687
    const/4 v1, 0x3

    iget-object v2, p0, Losj;->c:Ljava/lang/Boolean;

    .line 1688
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1690
    :cond_2
    iget-object v1, p0, Losj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1691
    iput v0, p0, Losj;->ai:I

    .line 1692
    return v0
.end method

.method public a(Loxn;)Losj;
    .locals 2

    .prologue
    .line 1700
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1701
    sparse-switch v0, :sswitch_data_0

    .line 1705
    iget-object v1, p0, Losj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1706
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losj;->ah:Ljava/util/List;

    .line 1709
    :cond_1
    iget-object v1, p0, Losj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1711
    :sswitch_0
    return-object p0

    .line 1716
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1717
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 1724
    :cond_2
    iput v0, p0, Losj;->a:I

    goto :goto_0

    .line 1726
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Losj;->a:I

    goto :goto_0

    .line 1731
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Losj;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1735
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Losj;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1701
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1662
    iget v0, p0, Losj;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1663
    const/4 v0, 0x1

    iget v1, p0, Losj;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1665
    :cond_0
    iget-object v0, p0, Losj;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1666
    const/4 v0, 0x2

    iget-object v1, p0, Losj;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1668
    :cond_1
    iget-object v0, p0, Losj;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1669
    const/4 v0, 0x3

    iget-object v1, p0, Losj;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1671
    :cond_2
    iget-object v0, p0, Losj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1673
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1636
    invoke-virtual {p0, p1}, Losj;->a(Loxn;)Losj;

    move-result-object v0

    return-object v0
.end method
