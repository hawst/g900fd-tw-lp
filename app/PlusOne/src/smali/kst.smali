.class public final Lkst;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Lkfo;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lkfd;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;I)V
    .locals 2

    .prologue
    .line 30
    const-string v0, "ReportSquareTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 32
    iput p2, p0, Lkst;->a:I

    .line 33
    new-instance v0, Lkfo;

    iget v1, p0, Lkst;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkst;->b:Lkfo;

    .line 34
    iput-object p3, p0, Lkst;->c:Ljava/lang/String;

    .line 35
    iput p4, p0, Lkst;->d:I

    .line 36
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lkst;->e:Lkfd;

    .line 37
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    .line 41
    invoke-virtual {p0}, Lkst;->f()Landroid/content/Context;

    move-result-object v0

    .line 42
    new-instance v1, Lksr;

    iget-object v2, p0, Lkst;->b:Lkfo;

    iget-object v3, p0, Lkst;->c:Ljava/lang/String;

    iget v4, p0, Lkst;->d:I

    invoke-direct {v1, v0, v2, v3, v4}, Lksr;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;I)V

    .line 44
    iget-object v2, p0, Lkst;->e:Lkfd;

    invoke-interface {v2, v1}, Lkfd;->a(Lkff;)V

    .line 46
    new-instance v2, Lhoz;

    iget v3, v1, Lkff;->i:I

    iget-object v4, v1, Lkff;->k:Ljava/lang/Exception;

    invoke-virtual {v1}, Lksr;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a055e

    .line 47
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    :goto_0
    invoke-direct {v2, v3, v4, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v2

    .line 47
    :cond_0
    const v1, 0x7f0a055d

    .line 48
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lkst;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a055c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
