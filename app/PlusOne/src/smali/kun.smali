.class public final Lkun;
.super Ldyj;
.source "PG"

# interfaces
.implements Lbc;
.implements Lhmm;
.implements Lhob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldyj;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhmm;",
        "Lhob;"
    }
.end annotation


# static fields
.field private static final R:[Ljava/lang/String;

.field private static S:[Ljava/lang/String;

.field private static T:[I


# instance fields
.field private U:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "volume"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "notifications_enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "disable_subscription"

    aput-object v2, v0, v1

    sput-object v0, Lkun;->R:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Ldyj;-><init>()V

    .line 54
    new-instance v0, Lhmf;

    iget-object v1, p0, Lkun;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 55
    return-void
.end method


# virtual methods
.method protected Z()I
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x4

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 98
    invoke-super {p0, p1, p2, p3}, Ldyj;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 101
    const v0, 0x7f100207

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 102
    const v2, 0x7f0a070b

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 104
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    packed-switch p1, :pswitch_data_0

    .line 114
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 111
    :pswitch_0
    new-instance v0, Lkys;

    iget-object v1, p0, Lkun;->at:Llnl;

    iget v2, p0, Lkun;->N:I

    iget-object v3, p0, Lkun;->U:Ljava/lang/String;

    sget-object v4, Lkun;->R:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lkys;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 67
    invoke-super {p0, p1}, Ldyj;->a(Landroid/os/Bundle;)V

    .line 69
    sget-object v0, Lkun;->S:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 70
    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    const v2, 0x7f0a0705

    .line 71
    invoke-virtual {p0, v2}, Lkun;->e_(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const v1, 0x7f0a0706

    .line 72
    invoke-virtual {p0, v1}, Lkun;->e_(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const v2, 0x7f0a0707

    .line 73
    invoke-virtual {p0, v2}, Lkun;->e_(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f0a0708

    .line 74
    invoke-virtual {p0, v2}, Lkun;->e_(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lkun;->S:[Ljava/lang/String;

    .line 76
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lkun;->T:[I

    .line 84
    :cond_0
    sget-object v0, Lkun;->S:[Ljava/lang/String;

    iput-object v0, p0, Lkun;->P:[Ljava/lang/String;

    .line 85
    sget-object v0, Lkun;->T:[I

    iput-object v0, p0, Lkun;->Q:[I

    .line 87
    invoke-virtual {p0}, Lkun;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 88
    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkun;->U:Ljava/lang/String;

    .line 90
    if-nez p1, :cond_1

    .line 91
    invoke-virtual {p0}, Lkun;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 93
    :cond_1
    return-void

    .line 76
    :array_0
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 120
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 122
    packed-switch v0, :pswitch_data_0

    .line 137
    :goto_0
    return-void

    .line 124
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkun;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 125
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    :cond_0
    invoke-virtual {p0, v1}, Lkun;->d(I)V

    goto :goto_0

    .line 129
    :cond_1
    const/4 v0, 0x2

    .line 130
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 132
    :goto_1
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 133
    :goto_2
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 135
    new-instance v5, Ldyq;

    if-nez v0, :cond_4

    :goto_3
    invoke-direct {v5, v4, v1, v3}, Ldyq;-><init>(IZZ)V

    invoke-virtual {p0, v5}, Lkun;->a(Ldyq;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 130
    goto :goto_1

    :cond_3
    move v3, v2

    .line 132
    goto :goto_2

    :cond_4
    move v1, v2

    .line 135
    goto :goto_3

    .line 122
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lkun;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 149
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lkun;->d(I)V

    .line 152
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method protected aa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lkun;->U:Ljava/lang/String;

    return-object v0
.end method

.method protected ac()I
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x4

    return v0
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 190
    new-instance v0, Lkqw;

    sget-object v1, Lomv;->B:Lhmn;

    iget-object v2, p0, Lkun;->U:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method protected ad()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 181
    const-string v0, "extra_square_id"

    invoke-virtual {p0}, Lkun;->aa()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0, p1}, Ldyj;->c(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lkun;->au:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 63
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method
