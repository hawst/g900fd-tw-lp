.class public final Lmhb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lngw;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33208
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33211
    iput-object v0, p0, Lmhb;->apiHeader:Llyr;

    .line 33214
    iput-object v0, p0, Lmhb;->a:Lngw;

    .line 33208
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33231
    const/4 v0, 0x0

    .line 33232
    iget-object v1, p0, Lmhb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 33233
    const/4 v0, 0x1

    iget-object v1, p0, Lmhb;->apiHeader:Llyr;

    .line 33234
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33236
    :cond_0
    iget-object v1, p0, Lmhb;->a:Lngw;

    if-eqz v1, :cond_1

    .line 33237
    const/4 v1, 0x2

    iget-object v2, p0, Lmhb;->a:Lngw;

    .line 33238
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33240
    :cond_1
    iget-object v1, p0, Lmhb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33241
    iput v0, p0, Lmhb;->ai:I

    .line 33242
    return v0
.end method

.method public a(Loxn;)Lmhb;
    .locals 2

    .prologue
    .line 33250
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33251
    sparse-switch v0, :sswitch_data_0

    .line 33255
    iget-object v1, p0, Lmhb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33256
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhb;->ah:Ljava/util/List;

    .line 33259
    :cond_1
    iget-object v1, p0, Lmhb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33261
    :sswitch_0
    return-object p0

    .line 33266
    :sswitch_1
    iget-object v0, p0, Lmhb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 33267
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmhb;->apiHeader:Llyr;

    .line 33269
    :cond_2
    iget-object v0, p0, Lmhb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33273
    :sswitch_2
    iget-object v0, p0, Lmhb;->a:Lngw;

    if-nez v0, :cond_3

    .line 33274
    new-instance v0, Lngw;

    invoke-direct {v0}, Lngw;-><init>()V

    iput-object v0, p0, Lmhb;->a:Lngw;

    .line 33276
    :cond_3
    iget-object v0, p0, Lmhb;->a:Lngw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33251
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33219
    iget-object v0, p0, Lmhb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 33220
    const/4 v0, 0x1

    iget-object v1, p0, Lmhb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33222
    :cond_0
    iget-object v0, p0, Lmhb;->a:Lngw;

    if-eqz v0, :cond_1

    .line 33223
    const/4 v0, 0x2

    iget-object v1, p0, Lmhb;->a:Lngw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33225
    :cond_1
    iget-object v0, p0, Lmhb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33227
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33204
    invoke-virtual {p0, p1}, Lmhb;->a(Loxn;)Lmhb;

    move-result-object v0

    return-object v0
.end method
