.class public final Lmhr;
.super Loxq;
.source "PG"


# instance fields
.field private a:Louv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26080
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26083
    iput-object v0, p0, Lmhr;->apiHeader:Llyr;

    .line 26086
    iput-object v0, p0, Lmhr;->a:Louv;

    .line 26080
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26103
    const/4 v0, 0x0

    .line 26104
    iget-object v1, p0, Lmhr;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 26105
    const/4 v0, 0x1

    iget-object v1, p0, Lmhr;->apiHeader:Llyr;

    .line 26106
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26108
    :cond_0
    iget-object v1, p0, Lmhr;->a:Louv;

    if-eqz v1, :cond_1

    .line 26109
    const/4 v1, 0x2

    iget-object v2, p0, Lmhr;->a:Louv;

    .line 26110
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26112
    :cond_1
    iget-object v1, p0, Lmhr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26113
    iput v0, p0, Lmhr;->ai:I

    .line 26114
    return v0
.end method

.method public a(Loxn;)Lmhr;
    .locals 2

    .prologue
    .line 26122
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26123
    sparse-switch v0, :sswitch_data_0

    .line 26127
    iget-object v1, p0, Lmhr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26128
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhr;->ah:Ljava/util/List;

    .line 26131
    :cond_1
    iget-object v1, p0, Lmhr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26133
    :sswitch_0
    return-object p0

    .line 26138
    :sswitch_1
    iget-object v0, p0, Lmhr;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 26139
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmhr;->apiHeader:Llyr;

    .line 26141
    :cond_2
    iget-object v0, p0, Lmhr;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26145
    :sswitch_2
    iget-object v0, p0, Lmhr;->a:Louv;

    if-nez v0, :cond_3

    .line 26146
    new-instance v0, Louv;

    invoke-direct {v0}, Louv;-><init>()V

    iput-object v0, p0, Lmhr;->a:Louv;

    .line 26148
    :cond_3
    iget-object v0, p0, Lmhr;->a:Louv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26123
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26091
    iget-object v0, p0, Lmhr;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 26092
    const/4 v0, 0x1

    iget-object v1, p0, Lmhr;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26094
    :cond_0
    iget-object v0, p0, Lmhr;->a:Louv;

    if-eqz v0, :cond_1

    .line 26095
    const/4 v0, 0x2

    iget-object v1, p0, Lmhr;->a:Louv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26097
    :cond_1
    iget-object v0, p0, Lmhr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26099
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26076
    invoke-virtual {p0, p1}, Lmhr;->a(Loxn;)Lmhr;

    move-result-object v0

    return-object v0
.end method
