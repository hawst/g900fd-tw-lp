.class public final Ldmo;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmhy;",
        "Lmhz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:[I


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Lkfo;[I)V
    .locals 6

    .prologue
    .line 48
    const-string v3, "readphotosfeatures"

    new-instance v4, Lmhy;

    invoke-direct {v4}, Lmhy;-><init>()V

    new-instance v5, Lmhz;

    invoke-direct {v5}, Lmhz;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 51
    iput-object p3, p0, Ldmo;->a:[I

    .line 52
    return-void
.end method


# virtual methods
.method protected a(Lmhy;)V
    .locals 5

    .prologue
    .line 56
    new-instance v1, Lovh;

    invoke-direct {v1}, Lovh;-><init>()V

    .line 58
    iput-object v1, p1, Lmhy;->a:Lovh;

    .line 60
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ldmo;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 61
    iget-object v2, p0, Ldmo;->a:[I

    aget v2, v2, v0

    packed-switch v2, :pswitch_data_0

    .line 70
    iget-object v2, p0, Ldmo;->a:[I

    aget v2, v2, v0

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x33

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unknown ReadPhotosFeature feature mask: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :pswitch_0
    new-instance v2, Lovg;

    invoke-direct {v2}, Lovg;-><init>()V

    iput-object v2, v1, Lovh;->a:Lovg;

    goto :goto_1

    .line 66
    :pswitch_1
    new-instance v2, Love;

    invoke-direct {v2}, Love;-><init>()V

    iput-object v2, v1, Lovh;->b:Love;

    .line 67
    iget-object v2, v1, Lovh;->b:Love;

    new-instance v3, Loul;

    invoke-direct {v3}, Loul;-><init>()V

    iput-object v3, v2, Love;->a:Loul;

    goto :goto_1

    .line 74
    :cond_0
    return-void

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lmhy;

    invoke-virtual {p0, p1}, Ldmo;->a(Lmhy;)V

    return-void
.end method

.method public b()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Ldmo;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmhz;

    .line 82
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 83
    if-eqz v0, :cond_0

    iget-object v1, v0, Lmhz;->a:Lovi;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lmhz;->a:Lovi;

    iget-object v1, v1, Lovi;->b:Lovd;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lmhz;->a:Lovi;

    iget-object v1, v1, Lovi;->b:Lovd;

    iget-object v1, v1, Lovd;->a:[Louo;

    if-nez v1, :cond_1

    :cond_0
    move-object v0, v8

    .line 97
    :goto_0
    return-object v0

    .line 87
    :cond_1
    iget-object v0, v0, Lmhz;->a:Lovi;

    iget-object v0, v0, Lovi;->b:Lovd;

    iget-object v10, v0, Lovd;->a:[Louo;

    array-length v11, v10

    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v11, :cond_3

    aget-object v7, v10, v9

    .line 88
    iget-object v0, v7, Louo;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 89
    new-instance v0, Ldug;

    iget-object v1, v7, Louo;->d:Ljava/lang/String;

    iget-object v2, v7, Louo;->e:Ljava/lang/String;

    iget-object v3, v7, Louo;->c:Ljava/lang/String;

    iget-object v4, v7, Louo;->b:Ljava/lang/Integer;

    .line 91
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, v7, Louo;->f:Loun;

    iget-object v5, v5, Loun;->a:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, v7, Louo;->f:Loun;

    iget-object v6, v6, Loun;->b:Ljava/lang/Integer;

    .line 92
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, v7, Louo;->f:Loun;

    iget-object v7, v7, Loun;->c:Ljava/lang/Boolean;

    .line 93
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, Ldug;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 94
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_3
    move-object v0, v8

    .line 97
    goto :goto_0
.end method
