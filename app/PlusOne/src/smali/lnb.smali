.class public final Llnb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/io/File;

.field private final c:Lkeh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkeh;)V
    .locals 3

    .prologue
    .line 57
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "reencode_jpeg_queue"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Llnb;-><init>(Landroid/content/Context;Lkeh;Landroid/os/Looper;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkeh;Landroid/os/Looper;)V
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "jpeg_encoder_queue_temp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Llnb;->b:Ljava/io/File;

    .line 41
    iput-object p2, p0, Llnb;->c:Lkeh;

    .line 42
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Llnc;

    invoke-direct {v1, p0}, Llnc;-><init>(Llnb;)V

    invoke-direct {v0, p3, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Llnb;->a:Landroid/os/Handler;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/webp/WebpDecoder$Config;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Llnb;->c:Lkeh;

    iget v1, p1, Lcom/google/android/webp/WebpDecoder$Config;->a:I

    iget v2, p1, Lcom/google/android/webp/WebpDecoder$Config;->b:I

    invoke-virtual {v0, v1, v2}, Lkeh;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 123
    if-nez v0, :cond_0

    .line 124
    iget v0, p1, Lcom/google/android/webp/WebpDecoder$Config;->a:I

    iget v1, p1, Lcom/google/android/webp/WebpDecoder$Config;->b:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 126
    :cond_0
    return-object v0
.end method

.method protected a(Lcom/google/android/webp/WebpDecoder$Config;Ljava/nio/ByteBuffer;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Llnb;->a(Lcom/google/android/webp/WebpDecoder$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 117
    invoke-static {p2, v0}, Lcom/google/android/webp/WebpDecoder;->a(Ljava/nio/ByteBuffer;Landroid/graphics/Bitmap;)Z

    move-result v1

    .line 118
    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/nio/ByteBuffer;)Lcom/google/android/webp/WebpDecoder$Config;
    .locals 1

    .prologue
    .line 82
    invoke-static {p1}, Lcom/google/android/webp/WebpDecoder;->getConfig(Ljava/nio/ByteBuffer;)Lcom/google/android/webp/WebpDecoder$Config;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Llnb;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 69
    return-void
.end method

.method protected a(Landroid/graphics/Bitmap;)Z
    .locals 4

    .prologue
    .line 86
    const/4 v2, 0x0

    .line 88
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v3, p0, Llnb;->b:Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 94
    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 102
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 90
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 91
    :goto_2
    :try_start_3
    const-string v2, "JpegEncodeQueue"

    const-string v3, "Temp file not found trying to write decoded bitmap"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 92
    if-eqz v1, :cond_0

    .line 96
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 99
    :cond_0
    :goto_3
    const/4 v0, 0x0

    goto :goto_1

    .line 94
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    if-eqz v1, :cond_1

    .line 96
    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 99
    :cond_1
    :goto_5
    throw v0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_5

    .line 94
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 90
    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method protected b(Ljava/io/File;)Ljava/nio/ByteBuffer;
    .locals 6

    .prologue
    .line 72
    const/4 v1, 0x0

    .line 74
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0}, Llrx;->a(Ljava/io/File;Z)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 75
    :catch_0
    move-exception v2

    .line 76
    const-string v3, "JpegEncodeQueue"

    const-string v4, "Unable to obtain byte buffer for file path: "

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected c(Ljava/io/File;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 106
    iget-object v0, p0, Llnb;->b:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "JpegEncodeQueue"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    const-string v0, "JpegEncodeQueue"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const-string v0, "Unable to rename temp file to: "

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_2
    const-string v0, "JpegEncodeQueue"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "Re-encoded file to jpeg path: "

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected d(Ljava/io/File;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 130
    invoke-virtual {p0, p1}, Llnb;->b(Ljava/io/File;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 131
    if-nez v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    invoke-virtual {p0, v0}, Llnb;->a(Ljava/nio/ByteBuffer;)Lcom/google/android/webp/WebpDecoder$Config;

    move-result-object v1

    .line 136
    if-nez v1, :cond_3

    .line 137
    const-string v0, "JpegEncodeQueue"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const-string v0, "Unable to obtain header info for webp from file, path: "

    .line 139
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :cond_3
    invoke-virtual {p0, v1, v0}, Llnb;->a(Lcom/google/android/webp/WebpDecoder$Config;Ljava/nio/ByteBuffer;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 146
    if-nez v0, :cond_5

    .line 147
    const-string v0, "JpegEncodeQueue"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const-string v0, "Unable to decode webp from file, path: "

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_5
    invoke-virtual {p0, v0}, Llnb;->a(Landroid/graphics/Bitmap;)Z

    move-result v1

    .line 154
    iget-object v2, p0, Llnb;->c:Lkeh;

    invoke-virtual {v2, v0}, Lkeh;->a(Landroid/graphics/Bitmap;)V

    .line 156
    if-nez v1, :cond_7

    .line 157
    const-string v0, "JpegEncodeQueue"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "Failed to write decoded bitmap to temp file, original path: "

    .line 159
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_6
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 164
    :cond_7
    invoke-virtual {p0, p1}, Llnb;->c(Ljava/io/File;)V

    goto/16 :goto_0
.end method
