.class final Lhfx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lhfw;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lhfw;
    .locals 4

    .prologue
    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 147
    const-class v0, Lhxc;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhxc;

    .line 148
    const-class v1, Ljqs;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Ljqs;

    .line 149
    const-class v2, Lkxr;

    .line 150
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lkxr;

    .line 151
    if-eqz v0, :cond_0

    .line 152
    new-instance v1, Lhfw;

    invoke-direct {v1}, Lhfw;-><init>()V

    invoke-virtual {v1, v0, v3}, Lhfw;->a(Lhxc;I)Lhfw;

    move-result-object v0

    .line 158
    :goto_0
    return-object v0

    .line 153
    :cond_0
    if-eqz v1, :cond_1

    .line 154
    new-instance v0, Lhfw;

    invoke-direct {v0}, Lhfw;-><init>()V

    invoke-virtual {v0, v1, v3}, Lhfw;->a(Ljqs;I)Lhfw;

    move-result-object v0

    goto :goto_0

    .line 155
    :cond_1
    if-eqz v2, :cond_2

    .line 156
    new-instance v0, Lhfw;

    invoke-direct {v0}, Lhfw;-><init>()V

    invoke-virtual {v0, v2}, Lhfw;->a(Lkxr;)Lhfw;

    move-result-object v0

    goto :goto_0

    .line 158
    :cond_2
    new-instance v0, Lhfw;

    invoke-direct {v0}, Lhfw;-><init>()V

    goto :goto_0
.end method

.method public a(I)[Lhfw;
    .locals 1

    .prologue
    .line 163
    new-array v0, p1, [Lhfw;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lhfx;->a(Landroid/os/Parcel;)Lhfw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lhfx;->a(I)[Lhfw;

    move-result-object v0

    return-object v0
.end method
