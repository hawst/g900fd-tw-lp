.class public final Lfoi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lfoi;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lfok;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lfok;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lfoj;

    invoke-direct {v0}, Lfoj;-><init>()V

    sput-object v0, Lfoi;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfoi;->a:Ljava/util/HashSet;

    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfoi;->b:Ljava/util/HashSet;

    .line 36
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfoi;->a:Ljava/util/HashSet;

    .line 40
    iget-object v0, p0, Lfoi;->a:Ljava/util/HashSet;

    invoke-direct {p0, p1, v0}, Lfoi;->b(Landroid/os/Parcel;Ljava/util/HashSet;)V

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfoi;->b:Ljava/util/HashSet;

    .line 42
    iget-object v0, p0, Lfoi;->b:Ljava/util/HashSet;

    invoke-direct {p0, p1, v0}, Lfoi;->b(Landroid/os/Parcel;Ljava/util/HashSet;)V

    .line 43
    return-void
.end method

.method private a(Landroid/os/Parcel;Ljava/util/HashSet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/HashSet",
            "<",
            "Lfok;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-virtual {p2}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfok;

    .line 130
    new-instance v2, Lhyt;

    .line 131
    iget-object v0, v0, Lfok;->a:Lmms;

    invoke-direct {v2, v0}, Lhyt;-><init>(Loxu;)V

    const/4 v0, 0x0

    .line 130
    invoke-virtual {p1, v2, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 133
    :cond_0
    return-void
.end method

.method private b(Landroid/os/Parcel;Ljava/util/HashSet;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/HashSet",
            "<",
            "Lfok;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 137
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 138
    const-class v0, Lhyt;

    .line 139
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    new-instance v3, Lmms;

    invoke-direct {v3}, Lmms;-><init>()V

    .line 140
    invoke-virtual {v0, v3}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lmms;

    .line 141
    new-instance v3, Lfok;

    invoke-direct {v3, v0}, Lfok;-><init>(Lmms;)V

    invoke-virtual {p2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 137
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 143
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lfoi;->d:I

    return v0
.end method

.method public a(Z)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Collection",
            "<",
            "Lmms;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 69
    if-eqz p1, :cond_0

    iget-object v0, p0, Lfoi;->b:Ljava/util/HashSet;

    .line 71
    :goto_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfok;

    .line 72
    iget-object v0, v0, Lfok;->a:Lmms;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 69
    :cond_0
    iget-object v0, p0, Lfoi;->a:Ljava/util/HashSet;

    goto :goto_0

    .line 74
    :cond_1
    return-object v1
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 50
    iput p1, p0, Lfoi;->c:I

    .line 51
    iget v0, p0, Lfoi;->c:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfoi;->e:Z

    .line 56
    :cond_0
    return-void
.end method

.method public a(Lfoh;)Z
    .locals 2

    .prologue
    .line 105
    new-instance v0, Lfok;

    .line 106
    invoke-virtual {p1}, Lfoh;->i()Lmms;

    move-result-object v1

    invoke-direct {v0, v1}, Lfok;-><init>(Lmms;)V

    .line 107
    iget-object v1, p0, Lfoi;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    const/4 v0, 0x1

    .line 113
    :goto_0
    return v0

    .line 110
    :cond_0
    iget-object v1, p0, Lfoi;->b:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    const/4 v0, 0x0

    goto :goto_0

    .line 113
    :cond_1
    invoke-virtual {p1}, Lfoh;->g()Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lfoh;Z)Z
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 82
    iget-boolean v0, p0, Lfoi;->e:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lfoh;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lfoi;->c:I

    add-int/lit8 v0, v0, -0x1

    const/4 v3, 0x5

    if-ge v0, v3, :cond_0

    .line 84
    const/4 v1, 0x0

    .line 101
    :goto_0
    return v1

    .line 86
    :cond_0
    new-instance v3, Lfok;

    .line 87
    invoke-virtual {p1}, Lfoh;->i()Lmms;

    move-result-object v0

    invoke-direct {v3, v0}, Lfok;-><init>(Lmms;)V

    .line 88
    invoke-virtual {p1}, Lfoh;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfoi;->b:Ljava/util/HashSet;

    .line 90
    :goto_1
    invoke-virtual {p1}, Lfoh;->g()Z

    move-result v4

    if-ne v4, p2, :cond_4

    .line 92
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 97
    :goto_2
    invoke-virtual {p1}, Lfoh;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    iget v3, p0, Lfoi;->c:I

    if-eqz p2, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    iput v0, p0, Lfoi;->c:I

    .line 100
    :cond_1
    iget v0, p0, Lfoi;->d:I

    if-eqz p2, :cond_2

    move v2, v1

    :cond_2
    add-int/2addr v0, v2

    iput v0, p0, Lfoi;->d:I

    goto :goto_0

    .line 88
    :cond_3
    iget-object v0, p0, Lfoi;->a:Ljava/util/HashSet;

    goto :goto_1

    .line 95
    :cond_4
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    move v0, v2

    .line 98
    goto :goto_3
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lfoi;->d:I

    .line 60
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lfoi;->a:Ljava/util/HashSet;

    invoke-direct {p0, p1, v0}, Lfoi;->a(Landroid/os/Parcel;Ljava/util/HashSet;)V

    .line 124
    iget-object v0, p0, Lfoi;->b:Ljava/util/HashSet;

    invoke-direct {p0, p1, v0}, Lfoi;->a(Landroid/os/Parcel;Ljava/util/HashSet;)V

    .line 125
    return-void
.end method
