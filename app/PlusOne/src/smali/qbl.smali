.class public final Lqbl;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1816
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1819
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lqbl;->a:[Ljava/lang/Long;

    .line 1816
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1835
    .line 1836
    iget-object v1, p0, Lqbl;->a:[Ljava/lang/Long;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lqbl;->a:[Ljava/lang/Long;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 1838
    iget-object v2, p0, Lqbl;->a:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1840
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Loxo;->f(J)I

    move-result v4

    add-int/2addr v1, v4

    .line 1838
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1843
    :cond_0
    iget-object v0, p0, Lqbl;->a:[Ljava/lang/Long;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 1845
    :cond_1
    iget-object v1, p0, Lqbl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1846
    iput v0, p0, Lqbl;->ai:I

    .line 1847
    return v0
.end method

.method public a(Loxn;)Lqbl;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1855
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1856
    sparse-switch v0, :sswitch_data_0

    .line 1860
    iget-object v1, p0, Lqbl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1861
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbl;->ah:Ljava/util/List;

    .line 1864
    :cond_1
    iget-object v1, p0, Lqbl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1866
    :sswitch_0
    return-object p0

    .line 1871
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1872
    iget-object v0, p0, Lqbl;->a:[Ljava/lang/Long;

    array-length v0, v0

    .line 1873
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 1874
    iget-object v2, p0, Lqbl;->a:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1875
    iput-object v1, p0, Lqbl;->a:[Ljava/lang/Long;

    .line 1876
    :goto_1
    iget-object v1, p0, Lqbl;->a:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1877
    iget-object v1, p0, Lqbl;->a:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1878
    invoke-virtual {p1}, Loxn;->a()I

    .line 1876
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1881
    :cond_2
    iget-object v1, p0, Lqbl;->a:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1856
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    .line 1824
    iget-object v0, p0, Lqbl;->a:[Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1825
    iget-object v1, p0, Lqbl;->a:[Ljava/lang/Long;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1826
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v4, v6, v7}, Loxo;->b(IJ)V

    .line 1825
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1829
    :cond_0
    iget-object v0, p0, Lqbl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1831
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1812
    invoke-virtual {p0, p1}, Lqbl;->a(Loxn;)Lqbl;

    move-result-object v0

    return-object v0
.end method
