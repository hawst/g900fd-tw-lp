.class public final Lcso;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcsp;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcso;->a:Ljava/util/ArrayList;

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcso;->c:I

    return-void
.end method


# virtual methods
.method public a(Lcoo;)F
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 78
    iget-object v0, p1, Lcoo;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 79
    :goto_0
    invoke-virtual {p0}, Lcso;->c()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcso;->d()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    .line 81
    :goto_1
    iget v5, p1, Lcoo;->a:I

    sget-object v6, Lcoq;->j:Lcoq;

    invoke-virtual {v6}, Lcoq;->ordinal()I

    move-result v6

    if-ne v5, v6, :cond_0

    if-eq v0, v1, :cond_3

    :cond_0
    move v0, v4

    .line 91
    :goto_2
    return v0

    :cond_1
    move v0, v2

    .line 78
    goto :goto_0

    :cond_2
    move v1, v2

    .line 79
    goto :goto_1

    .line 86
    :cond_3
    if-eqz v1, :cond_5

    .line 87
    iget-object v0, p1, Lcoo;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 88
    invoke-virtual {p0}, Lcso;->c()J

    move-result-wide v6

    cmp-long v2, v6, v0

    if-gtz v2, :cond_4

    invoke-virtual {p0}, Lcso;->d()J

    move-result-wide v6

    cmp-long v0, v0, v6

    if-gtz v0, :cond_4

    move v0, v3

    goto :goto_2

    :cond_4
    move v0, v4

    goto :goto_2

    :cond_5
    move v0, v3

    .line 91
    goto :goto_2
.end method

.method public a()Lcoo;
    .locals 5

    .prologue
    .line 20
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 21
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 23
    iget-object v0, p0, Lcso;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsp;

    .line 24
    invoke-virtual {v0}, Lcsp;->b()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 25
    invoke-virtual {v0}, Lcsp;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 28
    :cond_0
    new-instance v0, Lcoo;

    sget-object v3, Lcoq;->j:Lcoq;

    invoke-virtual {v3}, Lcoq;->ordinal()I

    move-result v3

    invoke-direct {v0, v3, v2, v1}, Lcoo;-><init>(ILjava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public a(Lcsp;)V
    .locals 2

    .prologue
    .line 35
    iget v0, p0, Lcso;->c:I

    invoke-virtual {p1}, Lcsp;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcso;->c:I

    .line 36
    iget-object v0, p0, Lcso;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcso;->b:Z

    .line 65
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcso;->c:I

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcso;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcso;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Lcso;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsp;

    invoke-virtual {v0}, Lcsp;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcso;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcso;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsp;

    invoke-virtual {v0}, Lcsp;->d()J

    move-result-wide v0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcso;->b:Z

    return v0
.end method
