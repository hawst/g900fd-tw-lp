.class public final Lpvy;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    const/high16 v0, -0x80000000

    iput v0, p0, Lpvy;->a:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x0

    .line 40
    iget v1, p0, Lpvy;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 41
    const/4 v0, 0x1

    iget v1, p0, Lpvy;->a:I

    .line 42
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 44
    :cond_0
    iget-object v1, p0, Lpvy;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 45
    const/4 v1, 0x2

    iget-object v2, p0, Lpvy;->b:Ljava/lang/Integer;

    .line 46
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_1
    iget-object v1, p0, Lpvy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    iput v0, p0, Lpvy;->ai:I

    .line 50
    return v0
.end method

.method public a(Loxn;)Lpvy;
    .locals 2

    .prologue
    .line 58
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 59
    sparse-switch v0, :sswitch_data_0

    .line 63
    iget-object v1, p0, Lpvy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpvy;->ah:Ljava/util/List;

    .line 67
    :cond_1
    iget-object v1, p0, Lpvy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    :sswitch_0
    return-object p0

    .line 74
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 75
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 79
    :cond_2
    iput v0, p0, Lpvy;->a:I

    goto :goto_0

    .line 81
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpvy;->a:I

    goto :goto_0

    .line 86
    :sswitch_2
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpvy;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27
    iget v0, p0, Lpvy;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 28
    const/4 v0, 0x1

    iget v1, p0, Lpvy;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 30
    :cond_0
    iget-object v0, p0, Lpvy;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x2

    iget-object v1, p0, Lpvy;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 33
    :cond_1
    iget-object v0, p0, Lpvy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 35
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpvy;->a(Loxn;)Lpvy;

    move-result-object v0

    return-object v0
.end method
