.class public final Lplb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lphc;

.field private b:Lplc;

.field private c:Lpwz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 169
    invoke-direct {p0}, Loxq;-><init>()V

    .line 172
    iput-object v0, p0, Lplb;->b:Lplc;

    .line 175
    iput-object v0, p0, Lplb;->a:Lphc;

    .line 178
    iput-object v0, p0, Lplb;->c:Lpwz;

    .line 169
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    iget-object v1, p0, Lplb;->b:Lplc;

    if-eqz v1, :cond_0

    .line 200
    const/4 v0, 0x1

    iget-object v1, p0, Lplb;->b:Lplc;

    .line 201
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 203
    :cond_0
    iget-object v1, p0, Lplb;->a:Lphc;

    if-eqz v1, :cond_1

    .line 204
    const/4 v1, 0x2

    iget-object v2, p0, Lplb;->a:Lphc;

    .line 205
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_1
    iget-object v1, p0, Lplb;->c:Lpwz;

    if-eqz v1, :cond_2

    .line 208
    const/4 v1, 0x3

    iget-object v2, p0, Lplb;->c:Lpwz;

    .line 209
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_2
    iget-object v1, p0, Lplb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    iput v0, p0, Lplb;->ai:I

    .line 213
    return v0
.end method

.method public a(Loxn;)Lplb;
    .locals 2

    .prologue
    .line 221
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 222
    sparse-switch v0, :sswitch_data_0

    .line 226
    iget-object v1, p0, Lplb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lplb;->ah:Ljava/util/List;

    .line 230
    :cond_1
    iget-object v1, p0, Lplb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    :sswitch_0
    return-object p0

    .line 237
    :sswitch_1
    iget-object v0, p0, Lplb;->b:Lplc;

    if-nez v0, :cond_2

    .line 238
    new-instance v0, Lplc;

    invoke-direct {v0}, Lplc;-><init>()V

    iput-object v0, p0, Lplb;->b:Lplc;

    .line 240
    :cond_2
    iget-object v0, p0, Lplb;->b:Lplc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 244
    :sswitch_2
    iget-object v0, p0, Lplb;->a:Lphc;

    if-nez v0, :cond_3

    .line 245
    new-instance v0, Lphc;

    invoke-direct {v0}, Lphc;-><init>()V

    iput-object v0, p0, Lplb;->a:Lphc;

    .line 247
    :cond_3
    iget-object v0, p0, Lplb;->a:Lphc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 251
    :sswitch_3
    iget-object v0, p0, Lplb;->c:Lpwz;

    if-nez v0, :cond_4

    .line 252
    new-instance v0, Lpwz;

    invoke-direct {v0}, Lpwz;-><init>()V

    iput-object v0, p0, Lplb;->c:Lpwz;

    .line 254
    :cond_4
    iget-object v0, p0, Lplb;->c:Lpwz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 222
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lplb;->b:Lplc;

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x1

    iget-object v1, p0, Lplb;->b:Lplc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 186
    :cond_0
    iget-object v0, p0, Lplb;->a:Lphc;

    if-eqz v0, :cond_1

    .line 187
    const/4 v0, 0x2

    iget-object v1, p0, Lplb;->a:Lphc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 189
    :cond_1
    iget-object v0, p0, Lplb;->c:Lpwz;

    if-eqz v0, :cond_2

    .line 190
    const/4 v0, 0x3

    iget-object v1, p0, Lplb;->c:Lpwz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 192
    :cond_2
    iget-object v0, p0, Lplb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 194
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lplb;->a(Loxn;)Lplb;

    move-result-object v0

    return-object v0
.end method
