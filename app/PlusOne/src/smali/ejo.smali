.class public final Lejo;
.super Lhye;
.source "PG"


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "parent_id"

    aput-object v2, v0, v1

    sput-object v0, Lejo;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-static {p4}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 34
    iput p2, p0, Lejo;->c:I

    .line 35
    iput-object p3, p0, Lejo;->d:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lejo;->e:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 41
    invoke-virtual {p0}, Lejo;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lejo;->c:I

    invoke-static {v0, v1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 44
    const-string v1, "all_tiles"

    sget-object v2, Lejo;->b:[Ljava/lang/String;

    const-string v3, "view_id = ? AND tile_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lejo;->d:Ljava/lang/String;

    aput-object v7, v4, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lejo;->e:Ljava/lang/String;

    aput-object v7, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
