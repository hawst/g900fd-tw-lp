.class public final Lfdr;
.super Lhyd;
.source "PG"


# instance fields
.field private e:Lehu;

.field private f:Llcr;

.field private g:Lkzp;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Landroid/text/Spanned;

.field private l:Landroid/text/Spanned;

.field private m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/text/Spanned;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lehu;Llcr;Lkzp;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 49
    iput-object p2, p0, Lfdr;->e:Lehu;

    .line 50
    iput-object p3, p0, Lfdr;->f:Llcr;

    .line 51
    iput-object p4, p0, Lfdr;->g:Lkzp;

    .line 52
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v4, -0x1

    .line 74
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 75
    packed-switch v0, :pswitch_data_0

    .line 90
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    .line 77
    :pswitch_0
    iget-object v0, p0, Lfdr;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iget-boolean v1, p0, Lfdr;->i:Z

    if-eqz v1, :cond_0

    new-instance v1, Llji;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0151

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v1, v4, v2}, Llji;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v1, Llji;

    const/4 v2, -0x2

    invoke-direct {v1, v4, v2}, Llji;-><init>(II)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0321

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d019b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v1, p0, Lfdr;->i:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0988

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lfdr;->h:Ljava/lang/String;

    aput-object v5, v3, v4

    iget-object v4, p0, Lfdr;->h:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0119

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 80
    :pswitch_1
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    const/16 v1, 0xa

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 82
    invoke-static {p1, v0, v2, v3}, Lfvh;->a(Landroid/content/Context;Ljava/lang/String;J)Landroid/view/ViewGroup;

    move-result-object v0

    goto/16 :goto_0

    .line 86
    :pswitch_2
    new-instance v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/text/Spanned;Landroid/text/Spanned;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/Spanned;",
            "Landroid/text/Spanned;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/text/Spanned;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199
    iput-object p1, p0, Lfdr;->k:Landroid/text/Spanned;

    .line 200
    iput-object p2, p0, Lfdr;->l:Landroid/text/Spanned;

    .line 201
    iput-object p3, p0, Lfdr;->m:Ljava/util/HashMap;

    .line 202
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 97
    instance-of v0, p1, Lkdd;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 98
    check-cast v0, Lkdd;

    invoke-interface {v0}, Lkdd;->c()V

    .line 101
    :cond_0
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 102
    packed-switch v0, :pswitch_data_0

    .line 158
    :goto_0
    instance-of v0, p1, Lkdd;

    if-eqz v0, :cond_1

    .line 163
    check-cast p1, Lkdd;

    invoke-interface {p1}, Lkdd;->b()V

    .line 165
    :cond_1
    return-void

    :pswitch_0
    move-object v0, p1

    .line 104
    check-cast v0, Lgbz;

    .line 105
    iget-object v1, p0, Lfdr;->e:Lehu;

    invoke-virtual {v1}, Lehu;->d()Lfdp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbz;->a(Lfdp;)Lgbz;

    .line 106
    iget-object v1, p0, Lfdr;->f:Llcr;

    iget-object v2, p0, Lfdr;->f:Llcr;

    iget v2, v2, Llcr;->a:I

    iget-object v3, p0, Lfdr;->e:Lehu;

    .line 107
    invoke-virtual {v3}, Lehu;->X()I

    move-result v3

    .line 106
    invoke-virtual {v0, p3, v1, v2, v3}, Lgbz;->a(Landroid/database/Cursor;Llcr;II)Lldq;

    .line 109
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbz;->e(Z)V

    .line 110
    iget-object v1, p0, Lfdr;->g:Lkzp;

    invoke-virtual {v0, v1}, Lgbz;->a(Lkzp;)V

    .line 112
    iget-boolean v1, p0, Lfdr;->j:Z

    if-eqz v1, :cond_3

    .line 113
    iget-object v1, p0, Lfdr;->k:Landroid/text/Spanned;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 114
    iget-object v1, p0, Lfdr;->k:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Lgbz;->a(Landroid/text/Spanned;)V

    .line 116
    :cond_2
    iget-object v1, p0, Lfdr;->l:Landroid/text/Spanned;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 117
    iget-object v1, p0, Lfdr;->l:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Lgbz;->b(Landroid/text/Spanned;)V

    .line 121
    :cond_3
    iget-object v1, p0, Lfdr;->f:Llcr;

    invoke-virtual {v0, v1}, Lgbz;->a(Lhuk;)V

    .line 122
    iget-object v1, p0, Lfdr;->e:Lehu;

    invoke-virtual {v0}, Lgbz;->A()I

    move-result v0

    invoke-virtual {v1, v0}, Lehu;->c(I)V

    goto :goto_0

    :pswitch_1
    move-object v0, p1

    .line 127
    check-cast v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;

    .line 129
    new-instance v1, Llji;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Llji;-><init>(II)V

    .line 131
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    const/4 v1, 0x5

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 134
    const/4 v1, 0x0

    .line 135
    iget-boolean v3, p0, Lfdr;->j:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lfdr;->m:Ljava/util/HashMap;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lfdr;->m:Ljava/util/HashMap;

    .line 136
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 137
    iget-object v1, p0, Lfdr;->m:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Spanned;

    .line 140
    :cond_4
    const/16 v2, 0x9

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 141
    const/4 v2, 0x6

    .line 143
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 142
    invoke-static {v2}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 144
    const/4 v3, 0x2

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x3

    .line 145
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    .line 147
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 146
    invoke-static {v7}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 144
    invoke-virtual {v0, v3, v6, v7}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const/4 v3, 0x5

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 149
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v3, v2

    :goto_1
    const-wide/16 v8, 0x1

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    move v2, v1

    :goto_2
    const-wide/16 v8, 0x2

    and-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v1, v4, v8

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    .line 148
    :goto_3
    invoke-virtual {v0, v6, v3, v2, v1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(Ljava/lang/String;Landroid/text/Spanned;ZZ)V

    .line 152
    const/4 v1, 0x7

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(J)V

    .line 153
    const/16 v1, 0x8

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a([B)V

    .line 154
    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->requestLayout()V

    .line 156
    iget-object v1, p0, Lfdr;->e:Lehu;

    invoke-virtual {v1}, Lehu;->W()Lldh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(Lldh;)Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;

    goto/16 :goto_0

    :cond_5
    move-object v3, v1

    .line 149
    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    move v2, v1

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lfdr;->h:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public a(Lkzp;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lfdr;->g:Lkzp;

    .line 206
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lfdr;->i:Z

    .line 60
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lfdr;->j:Z

    if-eq p1, v0, :cond_0

    .line 187
    iput-boolean p1, p0, Lfdr;->j:Z

    .line 188
    invoke-virtual {p0}, Lfdr;->notifyDataSetChanged()V

    .line 190
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lfdr;->j:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lfdr;->k:Landroid/text/Spanned;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdr;->l:Landroid/text/Spanned;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdr;->m:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfdr;->m:Ljava/util/HashMap;

    .line 194
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 209
    invoke-virtual {p0}, Lfdr;->getCount()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lfdr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x3

    return v0
.end method
