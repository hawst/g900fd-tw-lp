.class public final Lexq;
.super Lhya;
.source "PG"


# instance fields
.field private a:Lfxf;

.field private b:Leaq;

.field private c:Z

.field private d:Lexs;

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ldrx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Lexs;Lfxf;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0, p1, v1}, Lhya;-><init>(Landroid/content/Context;B)V

    .line 82
    invoke-virtual {p0, v1, v1}, Lexq;->b(ZZ)V

    .line 83
    invoke-virtual {p0, v1, v1}, Lexq;->b(ZZ)V

    .line 85
    iput-object p3, p0, Lexq;->d:Lexs;

    .line 86
    iput-object p4, p0, Lexq;->a:Lfxf;

    .line 88
    invoke-static {p1}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 89
    invoke-static {v0}, Llsc;->b(Landroid/util/DisplayMetrics;)Z

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lexq;->c:Z

    .line 92
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(I)V

    .line 93
    invoke-static {p1}, Llcm;->b(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 95
    invoke-static {p1}, Llcm;->a(Landroid/content/Context;)I

    move-result v0

    .line 96
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 97
    invoke-virtual {p2, v0, v1, v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setPadding(IIII)V

    .line 99
    new-instance v0, Lexr;

    invoke-direct {v0}, Lexr;-><init>()V

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 107
    return-void
.end method


# virtual methods
.method protected a(II)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 322
    const/4 v1, 0x4

    .line 324
    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 349
    :goto_1
    :sswitch_0
    return v0

    .line 326
    :pswitch_0
    const/4 v0, 0x0

    .line 327
    goto :goto_1

    .line 329
    :pswitch_1
    invoke-virtual {p0, v0}, Lexq;->l(I)Landroid/database/Cursor;

    move-result-object v2

    .line 331
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 332
    invoke-interface {v2, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 333
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 335
    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 346
    :sswitch_1
    const/4 v0, 0x3

    goto :goto_1

    .line 337
    :sswitch_2
    const/4 v0, 0x2

    .line 338
    goto :goto_1

    .line 324
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 335
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0x64 -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 112
    const/4 v0, 0x0

    .line 114
    packed-switch p2, :pswitch_data_0

    .line 160
    :cond_0
    :goto_0
    return-object v0

    .line 116
    :pswitch_0
    new-instance v0, Lfxq;

    invoke-direct {v0, p1}, Lfxq;-><init>(Landroid/content/Context;)V

    .line 117
    const v1, 0x7f1000a4

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    goto :goto_0

    .line 120
    :pswitch_1
    iget-boolean v1, p0, Lexq;->c:Z

    .line 124
    const/4 v3, 0x1

    :try_start_0
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 126
    :goto_1
    sparse-switch v3, :sswitch_data_0

    .line 150
    :goto_2
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 151
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :catch_0
    move-exception v3

    move v3, v2

    goto :goto_1

    .line 133
    :sswitch_0
    new-instance v0, Lfxi;

    invoke-direct {v0, p1}, Lfxi;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 136
    :sswitch_1
    new-instance v0, Lfxh;

    invoke-direct {v0, p1}, Lfxh;-><init>(Landroid/content/Context;)V

    move v1, v2

    .line 138
    goto :goto_2

    .line 143
    :sswitch_2
    new-instance v0, Lfxg;

    invoke-direct {v0, p1}, Lfxg;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 146
    :sswitch_3
    new-instance v0, Lfwp;

    invoke-direct {v0, p1}, Lfwp;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 126
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lexq;->a(ILandroid/database/Cursor;)V

    .line 308
    return-void
.end method

.method public a(Landroid/database/Cursor;Leaq;)V
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lexq;->a(ILandroid/database/Cursor;)V

    .line 303
    iput-object p2, p0, Lexq;->b:Leaq;

    .line 304
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    .line 169
    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 176
    :pswitch_0
    :try_start_0
    move-object v0, p1

    check-cast v0, Lfxq;

    move-object v2, v0

    .line 178
    const/4 v3, 0x1

    const/4 v4, 0x2

    invoke-static {p3, v3, v4}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;

    move-result-object v3

    .line 182
    const/4 v4, 0x3

    .line 183
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 185
    const/4 v4, 0x0

    .line 187
    if-eqz v5, :cond_2

    .line 188
    invoke-static {v5}, Llah;->a([B)Llah;

    move-result-object v4

    .line 191
    :cond_2
    const/16 v5, 0x9

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 194
    if-eqz v3, :cond_0

    .line 195
    iget-object v6, p0, Lexq;->b:Leaq;

    iget-object v7, p0, Lexq;->a:Lfxf;

    invoke-virtual/range {v2 .. v7}, Lfxq;->a(Lidh;Llah;Ljava/lang/String;Leaq;Lfxf;)V

    goto :goto_0

    .line 229
    :catch_0
    move-exception v2

    goto :goto_0

    .line 201
    :pswitch_1
    const/4 v3, 0x1

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 203
    sparse-switch v3, :sswitch_data_0

    .line 220
    :goto_1
    iget-object v2, p0, Lexq;->d:Lexs;

    if-eqz v2, :cond_0

    .line 225
    iget-object v2, p0, Lexq;->d:Lexs;

    invoke-interface {v2, p4}, Lexs;->c(I)V

    goto :goto_0

    .line 205
    :sswitch_0
    check-cast p1, Lfxi;

    .line 206
    new-instance v3, Lfyc;

    invoke-direct {v3}, Lfyc;-><init>()V

    const/4 v4, 0x2

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfyc;->b:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfyc;->a:Ljava/lang/String;

    const/16 v4, 0x9

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfyc;->c:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lfyc;->d:J

    const/4 v4, 0x7

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfyc;->e:Ljava/lang/String;

    iget-object v4, p0, Lexq;->a:Lfxf;

    iget-boolean v5, p0, Lexq;->c:Z

    if-nez v5, :cond_3

    :goto_2
    invoke-virtual {p1, v3, v4, v2}, Lfxi;->a(Lfyc;Lfxf;Z)V

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 210
    :sswitch_1
    move-object v0, p1

    check-cast v0, Lfxh;

    move-object v2, v0

    .line 211
    const/4 v3, 0x2

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x9

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x8

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    const/4 v6, 0x6

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v6, 0x5

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    const/4 v6, 0x4

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iget-object v10, p0, Lexq;->a:Lfxf;

    invoke-virtual/range {v2 .. v10}, Lfxh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;[BLfxf;)V

    goto :goto_1

    .line 218
    :sswitch_2
    move-object v0, p1

    check-cast v0, Lfxg;

    move-object v2, v0

    .line 219
    const/4 v3, 0x1

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x4

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    sget-object v6, Ldrm;->b:Lesn;

    const/4 v7, 0x5

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    invoke-virtual {v6, v7}, Lesn;->a([B)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ldrt;

    iget-object v6, v6, Ldrt;->people:Ljava/util/List;

    iget-object v7, p0, Lexq;->e:Ljava/util/HashMap;

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move v9, v7

    :goto_3
    if-ltz v9, :cond_5

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ldrv;

    iget-object v8, v7, Ldrv;->gaiaId:Ljava/lang/String;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lexq;->e:Ljava/util/HashMap;

    iget-object v10, v7, Ldrv;->gaiaId:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ldrx;

    if-eqz v8, :cond_4

    iget-object v10, v8, Ldrx;->a:Ljava/lang/String;

    iput-object v10, v7, Ldrv;->name:Ljava/lang/String;

    iget-object v8, v8, Ldrx;->b:Ljava/lang/String;

    iput-object v8, v7, Ldrv;->avatarUrl:Ljava/lang/String;

    :cond_4
    add-int/lit8 v7, v9, -0x1

    move v9, v7

    goto :goto_3

    :cond_5
    iget-object v7, p0, Lexq;->a:Lfxf;

    invoke-virtual/range {v2 .. v7}, Lfxg;->a(IJLjava/util/List;Lfxf;)V
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 174
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 203
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ldrx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 311
    iput-object p1, p0, Lexq;->e:Ljava/util/HashMap;

    .line 312
    invoke-virtual {p0}, Lexq;->notifyDataSetChanged()V

    .line 313
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Lexq;->c:Z

    return v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x5

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    return v0
.end method
