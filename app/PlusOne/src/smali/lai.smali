.class public final Llai;
.super Lllq;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lhua;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Llai;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:[Ljava/lang/String;

.field private b:[Ljava/lang/String;

.field private c:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 174
    new-instance v0, Llaj;

    invoke-direct {v0}, Llaj;-><init>()V

    sput-object v0, Llai;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lllq;-><init>()V

    .line 29
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Lllq;-><init>()V

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 33
    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, p0, Llai;->a:[Ljava/lang/String;

    .line 34
    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, p0, Llai;->b:[Ljava/lang/String;

    .line 35
    new-array v0, v4, [Z

    iput-object v0, p0, Llai;->c:[Z

    move v3, v2

    .line 36
    :goto_0
    if-ge v3, v4, :cond_1

    .line 37
    iget-object v0, p0, Llai;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v3

    .line 38
    iget-object v0, p0, Llai;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v3

    .line 39
    iget-object v5, p0, Llai;->c:[Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_1
    aput-boolean v0, v5, v3

    .line 36
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 39
    goto :goto_1

    .line 41
    :cond_1
    return-void
.end method

.method public constructor <init>(Logb;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lllq;-><init>()V

    .line 44
    iget-object v0, p1, Logb;->a:[Logp;

    if-nez v0, :cond_1

    .line 45
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Llai;->a:[Ljava/lang/String;

    .line 46
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Llai;->b:[Ljava/lang/String;

    .line 47
    new-array v0, v1, [Z

    iput-object v0, p0, Llai;->c:[Z

    .line 60
    :cond_0
    return-void

    .line 51
    :cond_1
    iget-object v0, p1, Logb;->a:[Logp;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Llai;->a:[Ljava/lang/String;

    .line 52
    iget-object v0, p1, Logb;->a:[Logp;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Llai;->b:[Ljava/lang/String;

    .line 53
    iget-object v0, p1, Logb;->a:[Logp;

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Llai;->c:[Z

    .line 55
    iget-object v5, p1, Logb;->a:[Logp;

    array-length v6, v5

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v6, :cond_0

    aget-object v0, v5, v2

    .line 56
    iget-object v4, p0, Llai;->a:[Ljava/lang/String;

    iget-object v7, v0, Logp;->c:Ljava/lang/String;

    aput-object v7, v4, v3

    .line 57
    iget-object v4, p0, Llai;->b:[Ljava/lang/String;

    iget-object v7, v0, Logp;->b:Ljava/lang/String;

    aput-object v7, v4, v3

    .line 58
    iget-object v7, p0, Llai;->c:[Z

    add-int/lit8 v4, v3, 0x1

    iget v0, v0, Logp;->d:I

    const/4 v8, 0x2

    if-ne v0, v8, :cond_2

    const/4 v0, 0x1

    :goto_1
    aput-boolean v0, v7, v3

    .line 55
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v4

    goto :goto_0

    :cond_2
    move v0, v1

    .line 58
    goto :goto_1
.end method

.method public static a(Llai;[Z)Llai;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 127
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 128
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 129
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 130
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 131
    aget-boolean v2, p1, v0

    if-nez v2, :cond_0

    .line 132
    iget-object v2, p0, Llai;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v2, p0, Llai;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    iget-object v2, p0, Llai;->c:[Z

    aget-boolean v2, v2, v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 139
    if-nez v6, :cond_2

    .line 140
    const/4 v0, 0x0

    .line 155
    :goto_1
    return-object v0

    .line 142
    :cond_2
    new-instance v2, Llai;

    invoke-direct {v2}, Llai;-><init>()V

    .line 144
    new-array v0, v6, [Ljava/lang/String;

    iput-object v0, v2, Llai;->a:[Ljava/lang/String;

    .line 145
    iget-object v0, v2, Llai;->a:[Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 147
    new-array v0, v6, [Ljava/lang/String;

    iput-object v0, v2, Llai;->b:[Ljava/lang/String;

    .line 148
    iget-object v0, v2, Llai;->b:[Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 150
    new-array v0, v6, [Z

    iput-object v0, v2, Llai;->c:[Z

    .line 151
    :goto_2
    if-ge v1, v6, :cond_3

    .line 152
    iget-object v3, v2, Llai;->c:[Z

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    aput-boolean v0, v3, v1

    .line 151
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 155
    goto :goto_1
.end method

.method public static a([B)Llai;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 85
    if-nez p0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 102
    :goto_0
    return-object v0

    .line 89
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 90
    new-instance v4, Llai;

    invoke-direct {v4}, Llai;-><init>()V

    .line 92
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 93
    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, v4, Llai;->a:[Ljava/lang/String;

    .line 94
    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, v4, Llai;->b:[Ljava/lang/String;

    .line 95
    new-array v0, v0, [Z

    iput-object v0, v4, Llai;->c:[Z

    move v0, v1

    .line 96
    :goto_1
    iget-object v2, v4, Llai;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 97
    iget-object v2, v4, Llai;->a:[Ljava/lang/String;

    invoke-static {v5}, Llai;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v0

    .line 98
    iget-object v2, v4, Llai;->b:[Ljava/lang/String;

    invoke-static {v5}, Llai;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v0

    .line 99
    iget-object v6, v4, Llai;->c:[Z

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    if-ne v2, v3, :cond_1

    move v2, v3

    :goto_2
    aput-boolean v2, v6, v0

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 99
    goto :goto_2

    :cond_2
    move-object v0, v4

    .line 102
    goto :goto_0
.end method

.method public static a(Llai;)[B
    .locals 5

    .prologue
    .line 63
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x40

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 64
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 66
    iget-object v0, p0, Llai;->a:[Ljava/lang/String;

    array-length v3, v0

    .line 67
    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 69
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 70
    iget-object v4, p0, Llai;->a:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {v2, v4}, Llai;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 71
    iget-object v4, p0, Llai;->b:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {v2, v4}, Llai;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 72
    iget-object v4, p0, Llai;->c:[Z

    aget-boolean v4, v4, v0

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 76
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 77
    return-object v0
.end method

.method public static a(Logb;)[B
    .locals 1

    .prologue
    .line 81
    new-instance v0, Llai;

    invoke-direct {v0, p0}, Llai;-><init>(Logb;)V

    invoke-static {v0}, Llai;->a(Llai;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Llai;->a:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Llai;->a:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Llai;->c:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Llai;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Llai;->b:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 166
    iget-object v0, p0, Llai;->a:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    move v0, v1

    .line 167
    :goto_0
    iget-object v2, p0, Llai;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 168
    iget-object v2, p0, Llai;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 169
    iget-object v2, p0, Llai;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 170
    iget-object v2, p0, Llai;->c:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 170
    goto :goto_1

    .line 172
    :cond_1
    return-void
.end method
