.class public final Lprr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lprr;


# instance fields
.field public b:[Lppg;

.field private c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    new-array v0, v0, [Lprr;

    sput-object v0, Lprr;->a:[Lprr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Loxq;-><init>()V

    .line 100
    sget-object v0, Lppg;->a:[Lppg;

    iput-object v0, p0, Lprr;->b:[Lppg;

    .line 103
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lprr;->c:[Ljava/lang/String;

    .line 97
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 126
    .line 127
    iget-object v0, p0, Lprr;->b:[Lppg;

    if-eqz v0, :cond_1

    .line 128
    iget-object v3, p0, Lprr;->b:[Lppg;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 129
    if-eqz v5, :cond_0

    .line 130
    const/4 v6, 0x1

    .line 131
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 128
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 135
    :cond_2
    iget-object v2, p0, Lprr;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lprr;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 137
    iget-object v3, p0, Lprr;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 139
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 137
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 141
    :cond_3
    add-int/2addr v0, v2

    .line 142
    iget-object v1, p0, Lprr;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 144
    :cond_4
    iget-object v1, p0, Lprr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    iput v0, p0, Lprr;->ai:I

    .line 146
    return v0
.end method

.method public a(Loxn;)Lprr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 154
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 155
    sparse-switch v0, :sswitch_data_0

    .line 159
    iget-object v2, p0, Lprr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 160
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lprr;->ah:Ljava/util/List;

    .line 163
    :cond_1
    iget-object v2, p0, Lprr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    :sswitch_0
    return-object p0

    .line 170
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 171
    iget-object v0, p0, Lprr;->b:[Lppg;

    if-nez v0, :cond_3

    move v0, v1

    .line 172
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lppg;

    .line 173
    iget-object v3, p0, Lprr;->b:[Lppg;

    if-eqz v3, :cond_2

    .line 174
    iget-object v3, p0, Lprr;->b:[Lppg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 176
    :cond_2
    iput-object v2, p0, Lprr;->b:[Lppg;

    .line 177
    :goto_2
    iget-object v2, p0, Lprr;->b:[Lppg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 178
    iget-object v2, p0, Lprr;->b:[Lppg;

    new-instance v3, Lppg;

    invoke-direct {v3}, Lppg;-><init>()V

    aput-object v3, v2, v0

    .line 179
    iget-object v2, p0, Lprr;->b:[Lppg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 180
    invoke-virtual {p1}, Loxn;->a()I

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 171
    :cond_3
    iget-object v0, p0, Lprr;->b:[Lppg;

    array-length v0, v0

    goto :goto_1

    .line 183
    :cond_4
    iget-object v2, p0, Lprr;->b:[Lppg;

    new-instance v3, Lppg;

    invoke-direct {v3}, Lppg;-><init>()V

    aput-object v3, v2, v0

    .line 184
    iget-object v2, p0, Lprr;->b:[Lppg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 188
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 189
    iget-object v0, p0, Lprr;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 190
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 191
    iget-object v3, p0, Lprr;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 192
    iput-object v2, p0, Lprr;->c:[Ljava/lang/String;

    .line 193
    :goto_3
    iget-object v2, p0, Lprr;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 194
    iget-object v2, p0, Lprr;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 195
    invoke-virtual {p1}, Loxn;->a()I

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 198
    :cond_5
    iget-object v2, p0, Lprr;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 155
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 108
    iget-object v1, p0, Lprr;->b:[Lppg;

    if-eqz v1, :cond_1

    .line 109
    iget-object v2, p0, Lprr;->b:[Lppg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 110
    if-eqz v4, :cond_0

    .line 111
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 109
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    :cond_1
    iget-object v1, p0, Lprr;->c:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 116
    iget-object v1, p0, Lprr;->c:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 117
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 120
    :cond_2
    iget-object v0, p0, Lprr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 122
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lprr;->a(Loxn;)Lprr;

    move-result-object v0

    return-object v0
.end method
