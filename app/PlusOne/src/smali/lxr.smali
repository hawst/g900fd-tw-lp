.class public final Llxr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxr;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Llyf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3141
    const/4 v0, 0x0

    new-array v0, v0, [Llxr;

    sput-object v0, Llxr;->a:[Llxr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3142
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3145
    iput-object v0, p0, Llxr;->b:Llxp;

    .line 3156
    iput-object v0, p0, Llxr;->g:Llyf;

    .line 3142
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3185
    const/4 v0, 0x0

    .line 3186
    iget-object v1, p0, Llxr;->b:Llxp;

    if-eqz v1, :cond_0

    .line 3187
    const/4 v0, 0x1

    iget-object v1, p0, Llxr;->b:Llxp;

    .line 3188
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3190
    :cond_0
    iget-object v1, p0, Llxr;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3191
    const/4 v1, 0x2

    iget-object v2, p0, Llxr;->c:Ljava/lang/String;

    .line 3192
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3194
    :cond_1
    iget-object v1, p0, Llxr;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3195
    const/4 v1, 0x3

    iget-object v2, p0, Llxr;->d:Ljava/lang/String;

    .line 3196
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3198
    :cond_2
    iget-object v1, p0, Llxr;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3199
    const/4 v1, 0x4

    iget-object v2, p0, Llxr;->f:Ljava/lang/String;

    .line 3200
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3202
    :cond_3
    iget-object v1, p0, Llxr;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3203
    const/4 v1, 0x5

    iget-object v2, p0, Llxr;->e:Ljava/lang/String;

    .line 3204
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3206
    :cond_4
    iget-object v1, p0, Llxr;->g:Llyf;

    if-eqz v1, :cond_5

    .line 3207
    const/4 v1, 0x6

    iget-object v2, p0, Llxr;->g:Llyf;

    .line 3208
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3210
    :cond_5
    iget-object v1, p0, Llxr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3211
    iput v0, p0, Llxr;->ai:I

    .line 3212
    return v0
.end method

.method public a(Loxn;)Llxr;
    .locals 2

    .prologue
    .line 3220
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3221
    sparse-switch v0, :sswitch_data_0

    .line 3225
    iget-object v1, p0, Llxr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3226
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxr;->ah:Ljava/util/List;

    .line 3229
    :cond_1
    iget-object v1, p0, Llxr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3231
    :sswitch_0
    return-object p0

    .line 3236
    :sswitch_1
    iget-object v0, p0, Llxr;->b:Llxp;

    if-nez v0, :cond_2

    .line 3237
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxr;->b:Llxp;

    .line 3239
    :cond_2
    iget-object v0, p0, Llxr;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3243
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxr;->c:Ljava/lang/String;

    goto :goto_0

    .line 3247
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxr;->d:Ljava/lang/String;

    goto :goto_0

    .line 3251
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxr;->f:Ljava/lang/String;

    goto :goto_0

    .line 3255
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxr;->e:Ljava/lang/String;

    goto :goto_0

    .line 3259
    :sswitch_6
    iget-object v0, p0, Llxr;->g:Llyf;

    if-nez v0, :cond_3

    .line 3260
    new-instance v0, Llyf;

    invoke-direct {v0}, Llyf;-><init>()V

    iput-object v0, p0, Llxr;->g:Llyf;

    .line 3262
    :cond_3
    iget-object v0, p0, Llxr;->g:Llyf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3221
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3161
    iget-object v0, p0, Llxr;->b:Llxp;

    if-eqz v0, :cond_0

    .line 3162
    const/4 v0, 0x1

    iget-object v1, p0, Llxr;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3164
    :cond_0
    iget-object v0, p0, Llxr;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3165
    const/4 v0, 0x2

    iget-object v1, p0, Llxr;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3167
    :cond_1
    iget-object v0, p0, Llxr;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3168
    const/4 v0, 0x3

    iget-object v1, p0, Llxr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3170
    :cond_2
    iget-object v0, p0, Llxr;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3171
    const/4 v0, 0x4

    iget-object v1, p0, Llxr;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3173
    :cond_3
    iget-object v0, p0, Llxr;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3174
    const/4 v0, 0x5

    iget-object v1, p0, Llxr;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3176
    :cond_4
    iget-object v0, p0, Llxr;->g:Llyf;

    if-eqz v0, :cond_5

    .line 3177
    const/4 v0, 0x6

    iget-object v1, p0, Llxr;->g:Llyf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3179
    :cond_5
    iget-object v0, p0, Llxr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3181
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3138
    invoke-virtual {p0, p1}, Llxr;->a(Loxn;)Llxr;

    move-result-object v0

    return-object v0
.end method
