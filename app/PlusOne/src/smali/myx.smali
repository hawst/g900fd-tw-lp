.class public final Lmyx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmye;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 213
    invoke-direct {p0}, Loxq;-><init>()V

    .line 216
    const/4 v0, 0x0

    iput-object v0, p0, Lmyx;->a:Lmye;

    .line 213
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 250
    const/4 v0, 0x0

    .line 251
    iget-object v1, p0, Lmyx;->a:Lmye;

    if-eqz v1, :cond_0

    .line 252
    const/4 v0, 0x1

    iget-object v1, p0, Lmyx;->a:Lmye;

    .line 253
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 255
    :cond_0
    iget-object v1, p0, Lmyx;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 256
    const/4 v1, 0x2

    iget-object v2, p0, Lmyx;->b:Ljava/lang/String;

    .line 257
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_1
    iget-object v1, p0, Lmyx;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 260
    const/4 v1, 0x3

    iget-object v2, p0, Lmyx;->c:Ljava/lang/String;

    .line 261
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_2
    iget-object v1, p0, Lmyx;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 264
    const/4 v1, 0x4

    iget-object v2, p0, Lmyx;->d:Ljava/lang/String;

    .line 265
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    :cond_3
    iget-object v1, p0, Lmyx;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 268
    const/4 v1, 0x5

    iget-object v2, p0, Lmyx;->e:Ljava/lang/Boolean;

    .line 269
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 271
    :cond_4
    iget-object v1, p0, Lmyx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    iput v0, p0, Lmyx;->ai:I

    .line 273
    return v0
.end method

.method public a(Loxn;)Lmyx;
    .locals 2

    .prologue
    .line 281
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 282
    sparse-switch v0, :sswitch_data_0

    .line 286
    iget-object v1, p0, Lmyx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 287
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyx;->ah:Ljava/util/List;

    .line 290
    :cond_1
    iget-object v1, p0, Lmyx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 292
    :sswitch_0
    return-object p0

    .line 297
    :sswitch_1
    iget-object v0, p0, Lmyx;->a:Lmye;

    if-nez v0, :cond_2

    .line 298
    new-instance v0, Lmye;

    invoke-direct {v0}, Lmye;-><init>()V

    iput-object v0, p0, Lmyx;->a:Lmye;

    .line 300
    :cond_2
    iget-object v0, p0, Lmyx;->a:Lmye;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 304
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmyx;->b:Ljava/lang/String;

    goto :goto_0

    .line 308
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmyx;->c:Ljava/lang/String;

    goto :goto_0

    .line 312
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmyx;->d:Ljava/lang/String;

    goto :goto_0

    .line 316
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmyx;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 282
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lmyx;->a:Lmye;

    if-eqz v0, :cond_0

    .line 230
    const/4 v0, 0x1

    iget-object v1, p0, Lmyx;->a:Lmye;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 232
    :cond_0
    iget-object v0, p0, Lmyx;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 233
    const/4 v0, 0x2

    iget-object v1, p0, Lmyx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 235
    :cond_1
    iget-object v0, p0, Lmyx;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 236
    const/4 v0, 0x3

    iget-object v1, p0, Lmyx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 238
    :cond_2
    iget-object v0, p0, Lmyx;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 239
    const/4 v0, 0x4

    iget-object v1, p0, Lmyx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 241
    :cond_3
    iget-object v0, p0, Lmyx;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 242
    const/4 v0, 0x5

    iget-object v1, p0, Lmyx;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 244
    :cond_4
    iget-object v0, p0, Lmyx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 246
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0, p1}, Lmyx;->a(Loxn;)Lmyx;

    move-result-object v0

    return-object v0
.end method
