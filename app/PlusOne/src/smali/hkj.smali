.class public Lhkj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqy;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lhkk;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lhkm;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhkj;->b:Ljava/util/Set;

    .line 74
    iput-object p1, p0, Lhkj;->a:Landroid/app/Activity;

    .line 76
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 77
    return-void
.end method


# virtual methods
.method public a(I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lhkb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lhkj;->c:Lhkm;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhkm;->a(Ljava/lang/Integer;)Ljava/util/List;

    move-result-object v0

    .line 108
    if-nez v0, :cond_0

    .line 109
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 111
    :cond_0
    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 116
    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lhkj;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkk;

    .line 118
    new-instance v3, Lhkb;

    invoke-direct {v3, p1, p2, p3}, Lhkb;-><init>(IILandroid/content/Intent;)V

    invoke-interface {v0, v3}, Lhkk;->a(Lhkb;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x1

    .line 124
    :goto_0
    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Lhkj;->c:Lhkm;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lhkb;

    invoke-direct {v2, p1, p2, p3}, Lhkb;-><init>(IILandroid/content/Intent;)V

    invoke-virtual {v0, v1, v2}, Lhkm;->a(Ljava/lang/Integer;Lhkb;)V

    .line 127
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lhkj;->a:Landroid/app/Activity;

    invoke-virtual {v0, p2, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 103
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 81
    if-eqz p1, :cond_0

    .line 82
    const-string v0, "com.google.android.apps.photos.activityresult.ActivityResultManager.Results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhkm;

    iput-object v0, p0, Lhkj;->c:Lhkm;

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_0
    new-instance v0, Lhkm;

    invoke-direct {v0}, Lhkm;-><init>()V

    iput-object v0, p0, Lhkj;->c:Lhkm;

    goto :goto_0
.end method

.method a(Lhkk;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lhkj;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    const-string v0, "com.google.android.apps.photos.activityresult.ActivityResultManager.Results"

    iget-object v1, p0, Lhkj;->c:Lhkm;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 91
    return-void
.end method

.method b(Lhkk;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lhkj;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method
