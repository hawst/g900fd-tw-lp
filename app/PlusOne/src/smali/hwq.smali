.class public final Lhwq;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private final a:Lhvx;

.field private b:Z

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:Lhvf;

.field private f:Lhwf;

.field private g:Z

.field private h:Lhvy;

.field private i:Z

.field private j:Landroid/widget/TextView;

.field private k:I

.field private l:I

.field private m:Z

.field private n:Landroid/widget/TextView;

.field private o:Z

.field private p:Landroid/widget/TextView;

.field private q:I

.field private r:Landroid/view/View;

.field private s:Lhwo;

.field private t:I

.field private u:Z

.field private v:Lhvm;

.field private w:Lhwb;

.field private x:Lhuz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 135
    invoke-virtual {p0}, Lhwq;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 137
    invoke-static {v0}, Lhvx;->a(Landroid/content/Context;)Lhvx;

    move-result-object v1

    iput-object v1, p0, Lhwq;->a:Lhvx;

    .line 139
    invoke-virtual {p0, v2}, Lhwq;->setWillNotDraw(Z)V

    .line 141
    const v1, 0x7fffffff

    iput v1, p0, Lhwq;->q:I

    iput v1, p0, Lhwq;->k:I

    .line 143
    new-instance v1, Lhvf;

    invoke-direct {v1, v0}, Lhvf;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhwq;->e:Lhvf;

    .line 144
    new-instance v1, Lhwf;

    invoke-direct {v1, v0}, Lhwf;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhwq;->f:Lhwf;

    .line 145
    new-instance v1, Lhwo;

    invoke-direct {v1, v0}, Lhwo;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhwq;->s:Lhwo;

    .line 146
    new-instance v1, Lhwb;

    invoke-direct {v1, v0}, Lhwb;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhwq;->w:Lhwb;

    .line 148
    iget-object v0, p0, Lhwq;->e:Lhvf;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    .line 149
    iget-object v0, p0, Lhwq;->f:Lhwf;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    .line 150
    iget-object v0, p0, Lhwq;->s:Lhwo;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    .line 151
    iget-object v0, p0, Lhwq;->w:Lhwb;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    .line 153
    iput-boolean v2, p0, Lhwq;->b:Z

    .line 121
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 687
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwq;->b:Z

    .line 692
    const v0, 0x7fffffff

    iput v0, p0, Lhwq;->q:I

    iput v0, p0, Lhwq;->k:I

    .line 694
    iget-object v0, p0, Lhwq;->e:Lhvf;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lhwq;->e:Lhvf;

    invoke-virtual {v0}, Lhvf;->a()V

    .line 698
    :cond_0
    iget-object v0, p0, Lhwq;->h:Lhvy;

    if-eqz v0, :cond_1

    .line 699
    iget-object v0, p0, Lhwq;->h:Lhvy;

    invoke-virtual {v0}, Lhvy;->a()V

    .line 702
    :cond_1
    iget-object v0, p0, Lhwq;->r:Landroid/view/View;

    instance-of v0, v0, Lljh;

    if-eqz v0, :cond_2

    .line 703
    iget-object v0, p0, Lhwq;->r:Landroid/view/View;

    check-cast v0, Lljh;

    invoke-interface {v0}, Lljh;->a()V

    .line 706
    :cond_2
    iget-object v0, p0, Lhwq;->s:Lhwo;

    if-eqz v0, :cond_3

    .line 707
    iget-object v0, p0, Lhwq;->s:Lhwo;

    invoke-virtual {v0}, Lhwo;->a()V

    .line 710
    :cond_3
    iget-object v0, p0, Lhwq;->v:Lhvm;

    if-eqz v0, :cond_4

    .line 711
    iget-object v0, p0, Lhwq;->v:Lhvm;

    invoke-virtual {v0}, Lhvm;->a()V

    .line 713
    :cond_4
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 337
    iput p1, p0, Lhwq;->k:I

    .line 339
    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    iget v1, p0, Lhwq;->k:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 342
    :cond_0
    return-void
.end method

.method public a(Lhuw;)V
    .locals 10

    .prologue
    const/16 v8, 0x19

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 212
    iput v3, p0, Lhwq;->d:I

    iget-boolean v0, p0, Lhwq;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwq;->h:Lhvy;

    invoke-virtual {v0, v3}, Lhvy;->a(Z)V

    iget-object v0, p0, Lhwq;->h:Lhvy;

    invoke-virtual {p0, v0}, Lhwq;->removeView(Landroid/view/View;)V

    :cond_0
    iput-boolean v3, p0, Lhwq;->g:Z

    invoke-virtual {p0, v3}, Lhwq;->a(Z)V

    iget-boolean v0, p0, Lhwq;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwq;->removeView(Landroid/view/View;)V

    :cond_1
    iput-boolean v3, p0, Lhwq;->i:Z

    iput v3, p0, Lhwq;->l:I

    iget-boolean v0, p0, Lhwq;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwq;->removeView(Landroid/view/View;)V

    :cond_2
    iput-boolean v3, p0, Lhwq;->m:Z

    iget-boolean v0, p0, Lhwq;->o:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwq;->removeView(Landroid/view/View;)V

    :cond_3
    iput-boolean v3, p0, Lhwq;->o:Z

    iget-object v0, p0, Lhwq;->r:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhwq;->r:Landroid/view/View;

    invoke-virtual {p0, v0}, Lhwq;->removeView(Landroid/view/View;)V

    iput-object v4, p0, Lhwq;->r:Landroid/view/View;

    :cond_4
    iget-boolean v0, p0, Lhwq;->u:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhwq;->v:Lhvm;

    invoke-virtual {v0, v3}, Lhvm;->a(Z)V

    iget-object v0, p0, Lhwq;->v:Lhvm;

    invoke-virtual {v0, v4}, Lhvm;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->v:Lhvm;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lhvm;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->v:Lhvm;

    invoke-virtual {p0, v0}, Lhwq;->removeView(Landroid/view/View;)V

    :cond_5
    iput-boolean v3, p0, Lhwq;->u:Z

    iput v3, p0, Lhwq;->t:I

    iget-object v0, p0, Lhwq;->x:Lhuz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhwq;->x:Lhuz;

    invoke-virtual {v0, v4}, Lhuz;->a(Lhjy;)V

    .line 214
    :cond_6
    if-nez p1, :cond_8

    .line 268
    :cond_7
    :goto_0
    return-void

    .line 217
    :cond_8
    iput-boolean v2, p0, Lhwq;->b:Z

    .line 219
    iget-object v0, p0, Lhwq;->e:Lhvf;

    invoke-virtual {p1}, Lhuw;->b()Lhul;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhvf;->a(Lhul;)V

    .line 220
    iget-object v0, p0, Lhwq;->e:Lhvf;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lhvf;->a(I)V

    .line 222
    iget-object v0, p0, Lhwq;->f:Lhwf;

    invoke-virtual {p1}, Lhuw;->c()Lhuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhwf;->a(Lhuf;)V

    .line 223
    iget-object v0, p0, Lhwq;->f:Lhwf;

    invoke-virtual {v0, v2}, Lhwf;->a(I)V

    .line 225
    invoke-virtual {p1}, Lhuw;->h()Z

    move-result v0

    iput-boolean v0, p0, Lhwq;->g:Z

    .line 226
    iget-boolean v0, p0, Lhwq;->g:Z

    if-eqz v0, :cond_a

    .line 227
    iget-object v0, p0, Lhwq;->h:Lhvy;

    if-nez v0, :cond_9

    .line 228
    new-instance v0, Lhvy;

    invoke-virtual {p0}, Lhwq;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhvy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwq;->h:Lhvy;

    .line 230
    :cond_9
    iget-object v0, p0, Lhwq;->h:Lhvy;

    invoke-virtual {p1}, Lhuw;->d()Lhut;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhvy;->a(Lhut;)V

    .line 232
    iget-object v0, p0, Lhwq;->h:Lhvy;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    .line 235
    :cond_a
    invoke-virtual {p0}, Lhwq;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lhwq;->a:Lhvx;

    iget v5, v0, Lhvx;->d:I

    invoke-virtual {p1}, Lhuw;->i()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lhwq;->i:Z

    iget-boolean v0, p0, Lhwq;->i:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    if-nez v0, :cond_b

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-static {v1, v0, v8}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v3, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_b
    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    iget v7, p0, Lhwq;->k:I

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    :cond_c
    invoke-virtual {p1}, Lhuw;->j()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lhwq;->m:Z

    iget-boolean v0, p0, Lhwq;->m:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    if-nez v0, :cond_d

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-static {v1, v0, v8}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    iget-object v7, p0, Lhwq;->a:Lhvx;

    iget v7, v7, Lhvx;->i:I

    invoke-virtual {v0, v5, v3, v5, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_d
    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    :cond_e
    invoke-virtual {p1}, Lhuw;->k()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lhwq;->o:Z

    iget-boolean v0, p0, Lhwq;->o:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    if-nez v0, :cond_f

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-static {v1, v0, v8}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v3, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_f
    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    iget v1, p0, Lhwq;->q:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    .line 237
    :cond_10
    invoke-virtual {p1}, Lhuw;->l()Lhup;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_15

    .line 239
    invoke-virtual {p0}, Lhwq;->getContext()Landroid/content/Context;

    move-result-object v6

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "createAndBindCard() should not be passed a null data model."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    move v0, v3

    .line 235
    goto/16 :goto_1

    :cond_12
    move v0, v3

    goto/16 :goto_2

    :cond_13
    move v0, v3

    goto :goto_3

    .line 239
    :cond_14
    invoke-interface {v0}, Lhup;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-class v1, Lhvw;

    invoke-static {v6, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhvw;

    invoke-interface {v0}, Lhup;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Lhvw;->a(Ljava/lang/Object;)Llno;

    move-result-object v1

    check-cast v1, Lhvv;

    if-eqz v1, :cond_20

    invoke-interface {v1}, Lhvv;->a()Landroid/view/View;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lhwq;->r:Landroid/view/View;

    .line 241
    iget-object v0, p0, Lhwq;->r:Landroid/view/View;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    .line 244
    :cond_15
    iget-object v0, p0, Lhwq;->s:Lhwo;

    invoke-virtual {p1}, Lhuw;->e()Lhuv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhwo;->a(Lhuv;)V

    .line 246
    invoke-virtual {p1}, Lhuw;->f()Lhuo;

    move-result-object v0

    if-eqz v0, :cond_21

    :goto_5
    iput-boolean v2, p0, Lhwq;->u:Z

    .line 247
    iget-boolean v0, p0, Lhwq;->u:Z

    if-eqz v0, :cond_17

    .line 248
    iget-object v0, p0, Lhwq;->v:Lhvm;

    if-nez v0, :cond_16

    .line 249
    new-instance v0, Lhvm;

    invoke-virtual {p0}, Lhwq;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhvm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwq;->v:Lhvm;

    .line 251
    :cond_16
    iget-object v0, p0, Lhwq;->v:Lhvm;

    invoke-virtual {p1}, Lhuw;->f()Lhuo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhvm;->a(Lhuo;)V

    .line 252
    iget-object v0, p0, Lhwq;->v:Lhvm;

    iget-object v1, p0, Lhwq;->s:Lhwo;

    invoke-virtual {v0, v1}, Lhvm;->a(Lhvo;)V

    .line 254
    iget-object v0, p0, Lhwq;->v:Lhvm;

    invoke-virtual {p0, v0}, Lhwq;->addView(Landroid/view/View;)V

    .line 257
    :cond_17
    iget-object v0, p0, Lhwq;->w:Lhwb;

    invoke-virtual {p1}, Lhuw;->g()Lhux;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhwb;->a(Lhux;)V

    .line 259
    invoke-virtual {p1}, Lhuw;->m()Lhjy;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_7

    .line 262
    iget-object v1, p0, Lhwq;->x:Lhuz;

    if-nez v1, :cond_18

    .line 263
    new-instance v1, Lhuz;

    invoke-direct {v1, p0}, Lhuz;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lhwq;->x:Lhuz;

    .line 266
    :cond_18
    iget-object v1, p0, Lhwq;->x:Lhuz;

    invoke-virtual {v1, v0}, Lhuz;->a(Lhjy;)V

    goto/16 :goto_0

    .line 239
    :pswitch_0
    instance-of v1, v0, Lhuw;

    if-eqz v1, :cond_19

    new-instance v1, Lhwq;

    invoke-direct {v1, v6}, Lhwq;-><init>(Landroid/content/Context;)V

    check-cast v0, Lhuw;

    invoke-virtual {v1, v0}, Lhwq;->a(Lhuw;)V

    move-object v0, v1

    goto :goto_4

    :cond_19
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempt to bind an unsupported data model instance to card of type Social Post Card"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    instance-of v1, v0, Lhui;

    if-eqz v1, :cond_1e

    check-cast v0, Lhui;

    const-wide/16 v4, 0x0

    invoke-virtual {v0}, Lhui;->g()I

    move-result v1

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Lhui;->f()I

    move-result v1

    invoke-virtual {v0}, Lhui;->g()I

    move-result v4

    div-int/2addr v1, v4

    int-to-double v4, v1

    :cond_1a
    invoke-virtual {v0}, Lhui;->h()Lhuj;

    move-result-object v1

    invoke-virtual {v0}, Lhui;->i()Lhuj;

    move-result-object v7

    if-nez v1, :cond_1b

    if-eqz v7, :cond_1c

    :cond_1b
    new-instance v1, Lhuy;

    invoke-direct {v1, v6}, Lhuy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lhuy;->a(Lhui;)V

    move-object v0, v1

    goto/16 :goto_4

    :cond_1c
    invoke-virtual {v0}, Lhui;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1d

    invoke-virtual {v0}, Lhui;->f()I

    move-result v1

    const/16 v7, 0x190

    if-le v1, v7, :cond_1d

    const-wide/high16 v8, 0x4004000000000000L    # 2.5

    cmpg-double v1, v4, v8

    if-gtz v1, :cond_1d

    const-wide v8, 0x3fe1c71c71c71c72L    # 0.5555555555555556

    cmpl-double v1, v4, v8

    if-ltz v1, :cond_1d

    new-instance v1, Lhwc;

    invoke-direct {v1, v6}, Lhwc;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lhwc;->a(Lhui;)V

    move-object v0, v1

    goto/16 :goto_4

    :cond_1d
    new-instance v1, Lhuy;

    invoke-direct {v1, v6}, Lhuy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lhuy;->a(Lhui;)V

    move-object v0, v1

    goto/16 :goto_4

    :cond_1e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempt to bind an unsupported data model instance to card of type Basic Card"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    instance-of v1, v0, Lhur;

    if-eqz v1, :cond_1f

    new-instance v1, Lhwi;

    invoke-direct {v1, v6}, Lhwi;-><init>(Landroid/content/Context;)V

    check-cast v0, Lhur;

    invoke-virtual {v1, v0}, Lhwi;->a(Lhur;)V

    move-object v0, v1

    goto/16 :goto_4

    :cond_1f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempt to bind an unsupported data model instance to card of type Media Card"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_20
    const-string v1, "ContentCardFactory"

    invoke-interface {v0}, Lhup;->a()I

    move-result v0

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x22

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unsupported card type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v4

    goto/16 :goto_4

    :cond_21
    move v2, v3

    .line 246
    goto/16 :goto_5

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 164
    if-eqz p1, :cond_0

    .line 165
    iget-object v0, p0, Lhwq;->a:Lhvx;

    iget v0, v0, Lhvx;->e:I

    invoke-virtual {p0, v0}, Lhwq;->setBackgroundColor(I)V

    .line 173
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v0, p0, Lhwq;->c:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lhwq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0}, Lhwq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020069

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {}, Llsj;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhwq;->a:Lhvx;

    iget v2, v2, Lhvx;->f:I

    invoke-static {v1, v2, v0}, Lkct;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_1
    iput-object v0, p0, Lhwq;->c:Landroid/graphics/drawable/Drawable;

    :cond_2
    iget-object v0, p0, Lhwq;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lhwq;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 349
    iput p1, p0, Lhwq;->q:I

    .line 351
    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lhwq;->p:Landroid/widget/TextView;

    iget v1, p0, Lhwq;->q:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 354
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 538
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 540
    iget-object v0, p0, Lhwq;->a:Lhvx;

    iget v6, v0, Lhvx;->d:I

    .line 541
    invoke-virtual {p0}, Lhwq;->getWidth()I

    move-result v7

    .line 544
    iget-boolean v0, p0, Lhwq;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhwq;->o:Z

    if-eqz v0, :cond_1

    .line 545
    :cond_0
    const/4 v1, 0x0

    iget v0, p0, Lhwq;->l:I

    int-to-float v2, v0

    int-to-float v3, v7

    iget v0, p0, Lhwq;->l:I

    int-to-float v4, v0

    iget-object v0, p0, Lhwq;->a:Lhvx;

    iget-object v5, v0, Lhvx;->t:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 550
    :cond_1
    iget-boolean v0, p0, Lhwq;->u:Z

    if-eqz v0, :cond_2

    .line 551
    iget v0, p0, Lhwq;->t:I

    iget-object v1, p0, Lhwq;->a:Lhvx;

    iget v1, v1, Lhvx;->u:I

    sub-int/2addr v0, v1

    .line 552
    int-to-float v1, v6

    int-to-float v2, v0

    sub-int v3, v7, v6

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v0, p0, Lhwq;->a:Lhvx;

    iget-object v5, v0, Lhvx;->t:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 555
    :cond_2
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 449
    iget-object v0, p0, Lhwq;->e:Lhvf;

    invoke-virtual {v0}, Lhvf;->getMeasuredHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 450
    iget-object v0, p0, Lhwq;->e:Lhvf;

    iget-object v2, p0, Lhwq;->e:Lhvf;

    invoke-virtual {v2}, Lhvf;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lhwq;->e:Lhvf;

    .line 451
    invoke-virtual {v3}, Lhvf;->getMeasuredHeight()I

    move-result v3

    .line 450
    invoke-virtual {v0, v1, v1, v2, v3}, Lhvf;->layout(IIII)V

    .line 454
    :cond_0
    invoke-virtual {p0}, Lhwq;->getMeasuredWidth()I

    move-result v5

    .line 455
    iget v3, p0, Lhwq;->d:I

    .line 456
    iget-object v0, p0, Lhwq;->f:Lhwf;

    if-nez v0, :cond_c

    move v0, v1

    .line 457
    :goto_0
    iget-object v2, p0, Lhwq;->h:Lhvy;

    if-nez v2, :cond_d

    move v4, v1

    .line 459
    :goto_1
    if-lez v0, :cond_1

    .line 464
    if-ge v0, v4, :cond_f

    .line 465
    sub-int v2, v4, v0

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    .line 468
    :goto_2
    iget-object v6, p0, Lhwq;->f:Lhwf;

    iget-object v7, p0, Lhwq;->f:Lhwf;

    invoke-virtual {v7}, Lhwf;->getMeasuredWidth()I

    move-result v7

    add-int v8, v2, v0

    invoke-virtual {v6, v1, v2, v7, v8}, Lhwf;->layout(IIII)V

    .line 471
    :cond_1
    if-lez v4, :cond_2

    .line 476
    if-ge v4, v0, :cond_e

    .line 477
    sub-int v2, v0, v4

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    .line 480
    :goto_3
    iget-object v6, p0, Lhwq;->h:Lhvy;

    iget-object v7, p0, Lhwq;->h:Lhvy;

    invoke-virtual {v7}, Lhvy;->getMeasuredWidth()I

    move-result v7

    sub-int v7, v5, v7

    add-int v8, v2, v4

    invoke-virtual {v6, v7, v2, v5, v8}, Lhvy;->layout(IIII)V

    .line 484
    :cond_2
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v3

    .line 485
    iput v0, p0, Lhwq;->l:I

    .line 487
    iget-boolean v2, p0, Lhwq;->i:Z

    if-eqz v2, :cond_3

    .line 488
    iget-object v2, p0, Lhwq;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v2, v1, v0, v5, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 489
    iget-object v2, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lhwq;->l:I

    .line 493
    :cond_3
    iget-boolean v2, p0, Lhwq;->m:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lhwq;->o:Z

    if-eqz v2, :cond_5

    .line 494
    :cond_4
    iget-object v2, p0, Lhwq;->a:Lhvx;

    iget v2, v2, Lhvx;->d:I

    add-int/2addr v0, v2

    .line 497
    :cond_5
    iget-boolean v2, p0, Lhwq;->m:Z

    if-eqz v2, :cond_6

    .line 498
    iget-object v2, p0, Lhwq;->n:Landroid/widget/TextView;

    iget-object v3, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v2, v1, v0, v5, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 499
    iget-object v2, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 502
    :cond_6
    iget-boolean v2, p0, Lhwq;->o:Z

    if-eqz v2, :cond_7

    .line 503
    iget-object v2, p0, Lhwq;->p:Landroid/widget/TextView;

    iget-object v3, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v2, v1, v0, v5, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 504
    iget-object v2, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 507
    :cond_7
    iget-object v2, p0, Lhwq;->r:Landroid/view/View;

    if-eqz v2, :cond_8

    .line 508
    iget-object v2, p0, Lhwq;->r:Landroid/view/View;

    iget-object v3, p0, Lhwq;->a:Lhvx;

    iget v3, v3, Lhvx;->d:I

    iget-object v4, p0, Lhwq;->a:Lhvx;

    iget v4, v4, Lhvx;->d:I

    sub-int v4, v5, v4

    iget-object v5, p0, Lhwq;->r:Landroid/view/View;

    .line 509
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    .line 508
    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 510
    iget-object v2, p0, Lhwq;->r:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 513
    :cond_8
    iget-object v2, p0, Lhwq;->s:Lhwo;

    invoke-virtual {v2}, Lhwo;->getMeasuredHeight()I

    move-result v2

    if-lez v2, :cond_9

    .line 514
    iget-object v2, p0, Lhwq;->s:Lhwo;

    iget-object v3, p0, Lhwq;->s:Lhwo;

    invoke-virtual {v3}, Lhwo;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lhwq;->s:Lhwo;

    .line 515
    invoke-virtual {v4}, Lhwo;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 514
    invoke-virtual {v2, v1, v0, v3, v4}, Lhwo;->layout(IIII)V

    .line 516
    iget-object v2, p0, Lhwq;->s:Lhwo;

    invoke-virtual {v2}, Lhwo;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 519
    :cond_9
    iget-boolean v2, p0, Lhwq;->u:Z

    if-eqz v2, :cond_a

    .line 520
    iput v0, p0, Lhwq;->t:I

    .line 521
    iget-object v2, p0, Lhwq;->v:Lhvm;

    iget-object v3, p0, Lhwq;->v:Lhvm;

    invoke-virtual {v3}, Lhvm;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lhwq;->v:Lhvm;

    .line 522
    invoke-virtual {v4}, Lhvm;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 521
    invoke-virtual {v2, v1, v0, v3, v4}, Lhvm;->layout(IIII)V

    .line 524
    iget-object v2, p0, Lhwq;->v:Lhvm;

    invoke-virtual {v2}, Lhvm;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 527
    :cond_a
    iget-object v2, p0, Lhwq;->w:Lhwb;

    invoke-virtual {v2}, Lhwb;->getMeasuredHeight()I

    move-result v2

    if-lez v2, :cond_b

    .line 528
    iget-object v2, p0, Lhwq;->w:Lhwb;

    iget-object v3, p0, Lhwq;->w:Lhwb;

    invoke-virtual {v3}, Lhwb;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lhwq;->w:Lhwb;

    .line 529
    invoke-virtual {v4}, Lhwb;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 528
    invoke-virtual {v2, v1, v0, v3, v4}, Lhwb;->layout(IIII)V

    .line 531
    :cond_b
    return-void

    .line 456
    :cond_c
    iget-object v0, p0, Lhwq;->f:Lhwf;

    invoke-virtual {v0}, Lhwf;->getMeasuredHeight()I

    move-result v0

    goto/16 :goto_0

    .line 457
    :cond_d
    iget-object v2, p0, Lhwq;->h:Lhvy;

    invoke-virtual {v2}, Lhvy;->getMeasuredHeight()I

    move-result v2

    move v4, v2

    goto/16 :goto_1

    :cond_e
    move v2, v3

    goto/16 :goto_3

    :cond_f
    move v2, v3

    goto/16 :goto_2
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 365
    iget-boolean v0, p0, Lhwq;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " expected to have been bound with valid data. Was bind() called?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_0
    invoke-virtual {p0}, Lhwq;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lhvj;->a(Landroid/content/Context;I)I

    move-result v3

    .line 371
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 372
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 374
    iget-object v0, p0, Lhwq;->e:Lhvf;

    invoke-virtual {v0, v5, v4}, Lhvf;->measure(II)V

    .line 375
    iget-object v0, p0, Lhwq;->s:Lhwo;

    invoke-virtual {v0, v5, v4}, Lhwo;->measure(II)V

    .line 376
    iget-object v0, p0, Lhwq;->w:Lhwb;

    invoke-virtual {v0, v5, v4}, Lhwb;->measure(II)V

    .line 378
    iget-object v0, p0, Lhwq;->e:Lhvf;

    invoke-virtual {v0}, Lhvf;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lhwq;->d:I

    .line 383
    iget-boolean v0, p0, Lhwq;->g:Z

    if-eqz v0, :cond_8

    .line 385
    div-int/lit8 v0, v3, 0x5

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 386
    iget-object v2, p0, Lhwq;->h:Lhvy;

    invoke-virtual {v2, v0, v4}, Lhvy;->measure(II)V

    .line 387
    iget-object v0, p0, Lhwq;->h:Lhvy;

    invoke-virtual {v0}, Lhvy;->getMeasuredHeight()I

    move-result v2

    .line 388
    iget-object v0, p0, Lhwq;->h:Lhvy;

    invoke-virtual {v0}, Lhvy;->getMeasuredWidth()I

    move-result v0

    .line 391
    :goto_0
    sub-int v0, v3, v0

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 393
    iget-object v7, p0, Lhwq;->f:Lhwf;

    invoke-virtual {v7, v0, v4}, Lhwf;->measure(II)V

    .line 395
    iget-object v0, p0, Lhwq;->f:Lhwf;

    invoke-virtual {v0}, Lhwf;->getMeasuredHeight()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 396
    iget-boolean v2, p0, Lhwq;->i:Z

    if-eqz v2, :cond_1

    iget-object v1, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v5, v4}, Landroid/widget/TextView;->measure(II)V

    iget-object v1, p0, Lhwq;->j:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    :cond_1
    iget-boolean v2, p0, Lhwq;->m:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lhwq;->o:Z

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lhwq;->a:Lhvx;

    iget v2, v2, Lhvx;->d:I

    add-int/2addr v1, v2

    :cond_3
    iget-boolean v2, p0, Lhwq;->m:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v4}, Landroid/widget/TextView;->measure(II)V

    iget-object v2, p0, Lhwq;->n:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    :cond_4
    iget-boolean v2, p0, Lhwq;->o:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v4}, Landroid/widget/TextView;->measure(II)V

    iget-object v2, p0, Lhwq;->p:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    :cond_5
    add-int/2addr v0, v1

    iget-object v1, p0, Lhwq;->s:Lhwo;

    .line 397
    invoke-virtual {v1}, Lhwo;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lhwq;->w:Lhwb;

    .line 398
    invoke-virtual {v1}, Lhwb;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v6

    .line 400
    iget-object v1, p0, Lhwq;->r:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 401
    iget-object v1, p0, Lhwq;->a:Lhvx;

    iget v1, v1, Lhvx;->d:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v3, v1

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 403
    iget-object v2, p0, Lhwq;->r:Landroid/view/View;

    invoke-virtual {v2, v1, v4}, Landroid/view/View;->measure(II)V

    .line 404
    iget-object v1, p0, Lhwq;->r:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 407
    :cond_6
    iget-boolean v1, p0, Lhwq;->u:Z

    if-eqz v1, :cond_7

    .line 408
    iget-object v1, p0, Lhwq;->v:Lhvm;

    invoke-virtual {v1, v5, v4}, Lhvm;->measure(II)V

    .line 409
    iget-object v1, p0, Lhwq;->v:Lhvm;

    invoke-virtual {v1}, Lhvm;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 412
    :cond_7
    invoke-virtual {p0, v3, v0}, Lhwq;->setMeasuredDimension(II)V

    .line 413
    return-void

    :cond_8
    move v0, v1

    move v2, v1

    goto/16 :goto_0
.end method
