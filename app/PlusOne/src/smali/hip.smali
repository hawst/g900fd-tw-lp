.class public final Lhip;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llql;
.implements Llrd;
.implements Llrg;


# instance fields
.field private a:Lu;

.field private b:Lhee;

.field private c:Lhin;

.field private d:Z


# direct methods
.method public constructor <init>(Lu;Llqr;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lhip;->a:Lu;

    .line 65
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 66
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 110
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lhip;->b:Lhee;

    .line 111
    const-class v0, Lhin;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhin;

    iput-object v0, p0, Lhip;->c:Lhin;

    .line 112
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 70
    iget-object v0, p0, Lhip;->b:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v4

    .line 72
    const-string v0, "is_default_restricted"

    .line 73
    invoke-interface {v4, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    .line 77
    iget-object v1, p0, Lhip;->a:Lu;

    invoke-virtual {v1}, Lu;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 78
    if-eqz p2, :cond_1

    .line 79
    const-string v0, "DomainRestrictionToggleMixin.DISABLE_DOMAIN_RESTRICTION_TOGGLE"

    .line 80
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhip;->d:Z

    .line 81
    const-string v0, "DomainRestrictionToggleMixin.RESTRICT_TO_DOMAIN"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    move v1, v0

    .line 90
    :goto_0
    const v0, 0x7f100160

    .line 91
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;

    .line 93
    if-eqz v0, :cond_0

    const-string v2, "is_dasher_account"

    invoke-interface {v4, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 94
    iget-object v2, p0, Lhip;->c:Lhin;

    invoke-virtual {v2, v0}, Lhin;->a(Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;)V

    .line 95
    iget-boolean v2, p0, Lhip;->d:Z

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->b(Z)V

    .line 96
    const-string v2, "domain_name"

    invoke-interface {v4, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->a(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->a(Z)V

    .line 98
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->setVisibility(I)V

    .line 100
    :cond_0
    return-void

    .line 82
    :cond_1
    if-eqz v1, :cond_2

    .line 83
    const-string v2, "DomainRestrictionToggleMixin.DISABLE_DOMAIN_RESTRICTION_TOGGLE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lhip;->d:Z

    .line 84
    const-string v2, "DomainRestrictionToggleMixin.RESTRICT_TO_DOMAIN"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    move v1, v0

    goto :goto_0

    .line 86
    :cond_2
    iput-boolean v3, p0, Lhip;->d:Z

    move v1, v0

    .line 87
    goto :goto_0

    :cond_3
    move v2, v3

    .line 95
    goto :goto_1
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 104
    const-string v0, "DomainRestrictionToggleMixin.RESTRICT_TO_DOMAIN"

    iget-object v1, p0, Lhip;->c:Lhin;

    invoke-virtual {v1}, Lhin;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    const-string v0, "DomainRestrictionToggleMixin.DISABLE_DOMAIN_RESTRICTION_TOGGLE"

    iget-boolean v1, p0, Lhip;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    return-void
.end method
