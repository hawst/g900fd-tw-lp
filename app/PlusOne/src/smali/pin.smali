.class public final Lpin;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpin;


# instance fields
.field private b:Lpio;

.field private c:Lpio;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:[Lpiq;

.field private g:[Lpip;

.field private h:Ljava/lang/Float;

.field private i:Ljava/lang/Float;

.field private j:Ljava/lang/Float;

.field private k:Ljava/lang/Float;

.field private l:Ljava/lang/Float;

.field private m:Ljava/lang/Float;

.field private n:Ljava/lang/Float;

.field private o:Ljava/lang/Float;

.field private p:Ljava/lang/Float;

.field private q:Ljava/lang/Float;

.field private r:Ljava/lang/Float;

.field private s:Ljava/lang/Float;

.field private t:Ljava/lang/Float;

.field private u:Ljava/lang/Float;

.field private v:Ljava/lang/Float;

.field private w:Ljava/lang/Float;

.field private x:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lpin;

    sput-object v0, Lpin;->a:[Lpin;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 557
    iput-object v0, p0, Lpin;->b:Lpio;

    .line 560
    iput-object v0, p0, Lpin;->c:Lpio;

    .line 567
    sget-object v0, Lpiq;->a:[Lpiq;

    iput-object v0, p0, Lpin;->f:[Lpiq;

    .line 570
    sget-object v0, Lpip;->a:[Lpip;

    iput-object v0, p0, Lpin;->g:[Lpip;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 692
    .line 693
    iget-object v0, p0, Lpin;->b:Lpio;

    if-eqz v0, :cond_18

    .line 694
    const/4 v0, 0x1

    iget-object v2, p0, Lpin;->b:Lpio;

    .line 695
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 697
    :goto_0
    iget-object v2, p0, Lpin;->c:Lpio;

    if-eqz v2, :cond_0

    .line 698
    const/4 v2, 0x2

    iget-object v3, p0, Lpin;->c:Lpio;

    .line 699
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 701
    :cond_0
    iget-object v2, p0, Lpin;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 702
    const/4 v2, 0x3

    iget-object v3, p0, Lpin;->d:Ljava/lang/Integer;

    .line 703
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 705
    :cond_1
    iget-object v2, p0, Lpin;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 706
    const/4 v2, 0x4

    iget-object v3, p0, Lpin;->e:Ljava/lang/Integer;

    .line 707
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 709
    :cond_2
    iget-object v2, p0, Lpin;->f:[Lpiq;

    if-eqz v2, :cond_4

    .line 710
    iget-object v3, p0, Lpin;->f:[Lpiq;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 711
    if-eqz v5, :cond_3

    .line 712
    const/4 v6, 0x5

    .line 713
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 710
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 717
    :cond_4
    iget-object v2, p0, Lpin;->g:[Lpip;

    if-eqz v2, :cond_6

    .line 718
    iget-object v2, p0, Lpin;->g:[Lpip;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 719
    if-eqz v4, :cond_5

    .line 720
    const/4 v5, 0x6

    .line 721
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 718
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 725
    :cond_6
    iget-object v1, p0, Lpin;->h:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 726
    const/4 v1, 0x7

    iget-object v2, p0, Lpin;->h:Ljava/lang/Float;

    .line 727
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 729
    :cond_7
    iget-object v1, p0, Lpin;->i:Ljava/lang/Float;

    if-eqz v1, :cond_8

    .line 730
    const/16 v1, 0x8

    iget-object v2, p0, Lpin;->i:Ljava/lang/Float;

    .line 731
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 733
    :cond_8
    iget-object v1, p0, Lpin;->j:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 734
    const/16 v1, 0x9

    iget-object v2, p0, Lpin;->j:Ljava/lang/Float;

    .line 735
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 737
    :cond_9
    iget-object v1, p0, Lpin;->k:Ljava/lang/Float;

    if-eqz v1, :cond_a

    .line 738
    const/16 v1, 0xa

    iget-object v2, p0, Lpin;->k:Ljava/lang/Float;

    .line 739
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 741
    :cond_a
    iget-object v1, p0, Lpin;->l:Ljava/lang/Float;

    if-eqz v1, :cond_b

    .line 742
    const/16 v1, 0xb

    iget-object v2, p0, Lpin;->l:Ljava/lang/Float;

    .line 743
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 745
    :cond_b
    iget-object v1, p0, Lpin;->m:Ljava/lang/Float;

    if-eqz v1, :cond_c

    .line 746
    const/16 v1, 0xc

    iget-object v2, p0, Lpin;->m:Ljava/lang/Float;

    .line 747
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 749
    :cond_c
    iget-object v1, p0, Lpin;->n:Ljava/lang/Float;

    if-eqz v1, :cond_d

    .line 750
    const/16 v1, 0xd

    iget-object v2, p0, Lpin;->n:Ljava/lang/Float;

    .line 751
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 753
    :cond_d
    iget-object v1, p0, Lpin;->o:Ljava/lang/Float;

    if-eqz v1, :cond_e

    .line 754
    const/16 v1, 0xe

    iget-object v2, p0, Lpin;->o:Ljava/lang/Float;

    .line 755
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 757
    :cond_e
    iget-object v1, p0, Lpin;->p:Ljava/lang/Float;

    if-eqz v1, :cond_f

    .line 758
    const/16 v1, 0xf

    iget-object v2, p0, Lpin;->p:Ljava/lang/Float;

    .line 759
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 761
    :cond_f
    iget-object v1, p0, Lpin;->q:Ljava/lang/Float;

    if-eqz v1, :cond_10

    .line 762
    const/16 v1, 0x10

    iget-object v2, p0, Lpin;->q:Ljava/lang/Float;

    .line 763
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 765
    :cond_10
    iget-object v1, p0, Lpin;->r:Ljava/lang/Float;

    if-eqz v1, :cond_11

    .line 766
    const/16 v1, 0x11

    iget-object v2, p0, Lpin;->r:Ljava/lang/Float;

    .line 767
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 769
    :cond_11
    iget-object v1, p0, Lpin;->s:Ljava/lang/Float;

    if-eqz v1, :cond_12

    .line 770
    const/16 v1, 0x12

    iget-object v2, p0, Lpin;->s:Ljava/lang/Float;

    .line 771
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 773
    :cond_12
    iget-object v1, p0, Lpin;->t:Ljava/lang/Float;

    if-eqz v1, :cond_13

    .line 774
    const/16 v1, 0x13

    iget-object v2, p0, Lpin;->t:Ljava/lang/Float;

    .line 775
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 777
    :cond_13
    iget-object v1, p0, Lpin;->u:Ljava/lang/Float;

    if-eqz v1, :cond_14

    .line 778
    const/16 v1, 0x14

    iget-object v2, p0, Lpin;->u:Ljava/lang/Float;

    .line 779
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 781
    :cond_14
    iget-object v1, p0, Lpin;->v:Ljava/lang/Float;

    if-eqz v1, :cond_15

    .line 782
    const/16 v1, 0x15

    iget-object v2, p0, Lpin;->v:Ljava/lang/Float;

    .line 783
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 785
    :cond_15
    iget-object v1, p0, Lpin;->w:Ljava/lang/Float;

    if-eqz v1, :cond_16

    .line 786
    const/16 v1, 0x16

    iget-object v2, p0, Lpin;->w:Ljava/lang/Float;

    .line 787
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 789
    :cond_16
    iget-object v1, p0, Lpin;->x:Ljava/lang/Float;

    if-eqz v1, :cond_17

    .line 790
    const/16 v1, 0x17

    iget-object v2, p0, Lpin;->x:Ljava/lang/Float;

    .line 791
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 793
    :cond_17
    iget-object v1, p0, Lpin;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 794
    iput v0, p0, Lpin;->ai:I

    .line 795
    return v0

    :cond_18
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpin;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 803
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 804
    sparse-switch v0, :sswitch_data_0

    .line 808
    iget-object v2, p0, Lpin;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 809
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpin;->ah:Ljava/util/List;

    .line 812
    :cond_1
    iget-object v2, p0, Lpin;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 814
    :sswitch_0
    return-object p0

    .line 819
    :sswitch_1
    iget-object v0, p0, Lpin;->b:Lpio;

    if-nez v0, :cond_2

    .line 820
    new-instance v0, Lpio;

    invoke-direct {v0}, Lpio;-><init>()V

    iput-object v0, p0, Lpin;->b:Lpio;

    .line 822
    :cond_2
    iget-object v0, p0, Lpin;->b:Lpio;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 826
    :sswitch_2
    iget-object v0, p0, Lpin;->c:Lpio;

    if-nez v0, :cond_3

    .line 827
    new-instance v0, Lpio;

    invoke-direct {v0}, Lpio;-><init>()V

    iput-object v0, p0, Lpin;->c:Lpio;

    .line 829
    :cond_3
    iget-object v0, p0, Lpin;->c:Lpio;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 833
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpin;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 837
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpin;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 841
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 842
    iget-object v0, p0, Lpin;->f:[Lpiq;

    if-nez v0, :cond_5

    move v0, v1

    .line 843
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpiq;

    .line 844
    iget-object v3, p0, Lpin;->f:[Lpiq;

    if-eqz v3, :cond_4

    .line 845
    iget-object v3, p0, Lpin;->f:[Lpiq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 847
    :cond_4
    iput-object v2, p0, Lpin;->f:[Lpiq;

    .line 848
    :goto_2
    iget-object v2, p0, Lpin;->f:[Lpiq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 849
    iget-object v2, p0, Lpin;->f:[Lpiq;

    new-instance v3, Lpiq;

    invoke-direct {v3}, Lpiq;-><init>()V

    aput-object v3, v2, v0

    .line 850
    iget-object v2, p0, Lpin;->f:[Lpiq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 851
    invoke-virtual {p1}, Loxn;->a()I

    .line 848
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 842
    :cond_5
    iget-object v0, p0, Lpin;->f:[Lpiq;

    array-length v0, v0

    goto :goto_1

    .line 854
    :cond_6
    iget-object v2, p0, Lpin;->f:[Lpiq;

    new-instance v3, Lpiq;

    invoke-direct {v3}, Lpiq;-><init>()V

    aput-object v3, v2, v0

    .line 855
    iget-object v2, p0, Lpin;->f:[Lpiq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 859
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 860
    iget-object v0, p0, Lpin;->g:[Lpip;

    if-nez v0, :cond_8

    move v0, v1

    .line 861
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpip;

    .line 862
    iget-object v3, p0, Lpin;->g:[Lpip;

    if-eqz v3, :cond_7

    .line 863
    iget-object v3, p0, Lpin;->g:[Lpip;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 865
    :cond_7
    iput-object v2, p0, Lpin;->g:[Lpip;

    .line 866
    :goto_4
    iget-object v2, p0, Lpin;->g:[Lpip;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 867
    iget-object v2, p0, Lpin;->g:[Lpip;

    new-instance v3, Lpip;

    invoke-direct {v3}, Lpip;-><init>()V

    aput-object v3, v2, v0

    .line 868
    iget-object v2, p0, Lpin;->g:[Lpip;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 869
    invoke-virtual {p1}, Loxn;->a()I

    .line 866
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 860
    :cond_8
    iget-object v0, p0, Lpin;->g:[Lpip;

    array-length v0, v0

    goto :goto_3

    .line 872
    :cond_9
    iget-object v2, p0, Lpin;->g:[Lpip;

    new-instance v3, Lpip;

    invoke-direct {v3}, Lpip;-><init>()V

    aput-object v3, v2, v0

    .line 873
    iget-object v2, p0, Lpin;->g:[Lpip;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 877
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->h:Ljava/lang/Float;

    goto/16 :goto_0

    .line 881
    :sswitch_8
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->i:Ljava/lang/Float;

    goto/16 :goto_0

    .line 885
    :sswitch_9
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->j:Ljava/lang/Float;

    goto/16 :goto_0

    .line 889
    :sswitch_a
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->k:Ljava/lang/Float;

    goto/16 :goto_0

    .line 893
    :sswitch_b
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->l:Ljava/lang/Float;

    goto/16 :goto_0

    .line 897
    :sswitch_c
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->m:Ljava/lang/Float;

    goto/16 :goto_0

    .line 901
    :sswitch_d
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->n:Ljava/lang/Float;

    goto/16 :goto_0

    .line 905
    :sswitch_e
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->o:Ljava/lang/Float;

    goto/16 :goto_0

    .line 909
    :sswitch_f
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->p:Ljava/lang/Float;

    goto/16 :goto_0

    .line 913
    :sswitch_10
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->q:Ljava/lang/Float;

    goto/16 :goto_0

    .line 917
    :sswitch_11
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->r:Ljava/lang/Float;

    goto/16 :goto_0

    .line 921
    :sswitch_12
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->s:Ljava/lang/Float;

    goto/16 :goto_0

    .line 925
    :sswitch_13
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->t:Ljava/lang/Float;

    goto/16 :goto_0

    .line 929
    :sswitch_14
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->u:Ljava/lang/Float;

    goto/16 :goto_0

    .line 933
    :sswitch_15
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->v:Ljava/lang/Float;

    goto/16 :goto_0

    .line 937
    :sswitch_16
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->w:Ljava/lang/Float;

    goto/16 :goto_0

    .line 941
    :sswitch_17
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpin;->x:Ljava/lang/Float;

    goto/16 :goto_0

    .line 804
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x4d -> :sswitch_9
        0x55 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6d -> :sswitch_d
        0x75 -> :sswitch_e
        0x7d -> :sswitch_f
        0x85 -> :sswitch_10
        0x8d -> :sswitch_11
        0x95 -> :sswitch_12
        0x9d -> :sswitch_13
        0xa5 -> :sswitch_14
        0xad -> :sswitch_15
        0xb5 -> :sswitch_16
        0xbd -> :sswitch_17
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 609
    iget-object v1, p0, Lpin;->b:Lpio;

    if-eqz v1, :cond_0

    .line 610
    const/4 v1, 0x1

    iget-object v2, p0, Lpin;->b:Lpio;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 612
    :cond_0
    iget-object v1, p0, Lpin;->c:Lpio;

    if-eqz v1, :cond_1

    .line 613
    const/4 v1, 0x2

    iget-object v2, p0, Lpin;->c:Lpio;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 615
    :cond_1
    iget-object v1, p0, Lpin;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 616
    const/4 v1, 0x3

    iget-object v2, p0, Lpin;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 618
    :cond_2
    iget-object v1, p0, Lpin;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 619
    const/4 v1, 0x4

    iget-object v2, p0, Lpin;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 621
    :cond_3
    iget-object v1, p0, Lpin;->f:[Lpiq;

    if-eqz v1, :cond_5

    .line 622
    iget-object v2, p0, Lpin;->f:[Lpiq;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 623
    if-eqz v4, :cond_4

    .line 624
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 622
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 628
    :cond_5
    iget-object v1, p0, Lpin;->g:[Lpip;

    if-eqz v1, :cond_7

    .line 629
    iget-object v1, p0, Lpin;->g:[Lpip;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 630
    if-eqz v3, :cond_6

    .line 631
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 629
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 635
    :cond_7
    iget-object v0, p0, Lpin;->h:Ljava/lang/Float;

    if-eqz v0, :cond_8

    .line 636
    const/4 v0, 0x7

    iget-object v1, p0, Lpin;->h:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 638
    :cond_8
    iget-object v0, p0, Lpin;->i:Ljava/lang/Float;

    if-eqz v0, :cond_9

    .line 639
    const/16 v0, 0x8

    iget-object v1, p0, Lpin;->i:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 641
    :cond_9
    iget-object v0, p0, Lpin;->j:Ljava/lang/Float;

    if-eqz v0, :cond_a

    .line 642
    const/16 v0, 0x9

    iget-object v1, p0, Lpin;->j:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 644
    :cond_a
    iget-object v0, p0, Lpin;->k:Ljava/lang/Float;

    if-eqz v0, :cond_b

    .line 645
    const/16 v0, 0xa

    iget-object v1, p0, Lpin;->k:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 647
    :cond_b
    iget-object v0, p0, Lpin;->l:Ljava/lang/Float;

    if-eqz v0, :cond_c

    .line 648
    const/16 v0, 0xb

    iget-object v1, p0, Lpin;->l:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 650
    :cond_c
    iget-object v0, p0, Lpin;->m:Ljava/lang/Float;

    if-eqz v0, :cond_d

    .line 651
    const/16 v0, 0xc

    iget-object v1, p0, Lpin;->m:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 653
    :cond_d
    iget-object v0, p0, Lpin;->n:Ljava/lang/Float;

    if-eqz v0, :cond_e

    .line 654
    const/16 v0, 0xd

    iget-object v1, p0, Lpin;->n:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 656
    :cond_e
    iget-object v0, p0, Lpin;->o:Ljava/lang/Float;

    if-eqz v0, :cond_f

    .line 657
    const/16 v0, 0xe

    iget-object v1, p0, Lpin;->o:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 659
    :cond_f
    iget-object v0, p0, Lpin;->p:Ljava/lang/Float;

    if-eqz v0, :cond_10

    .line 660
    const/16 v0, 0xf

    iget-object v1, p0, Lpin;->p:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 662
    :cond_10
    iget-object v0, p0, Lpin;->q:Ljava/lang/Float;

    if-eqz v0, :cond_11

    .line 663
    const/16 v0, 0x10

    iget-object v1, p0, Lpin;->q:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 665
    :cond_11
    iget-object v0, p0, Lpin;->r:Ljava/lang/Float;

    if-eqz v0, :cond_12

    .line 666
    const/16 v0, 0x11

    iget-object v1, p0, Lpin;->r:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 668
    :cond_12
    iget-object v0, p0, Lpin;->s:Ljava/lang/Float;

    if-eqz v0, :cond_13

    .line 669
    const/16 v0, 0x12

    iget-object v1, p0, Lpin;->s:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 671
    :cond_13
    iget-object v0, p0, Lpin;->t:Ljava/lang/Float;

    if-eqz v0, :cond_14

    .line 672
    const/16 v0, 0x13

    iget-object v1, p0, Lpin;->t:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 674
    :cond_14
    iget-object v0, p0, Lpin;->u:Ljava/lang/Float;

    if-eqz v0, :cond_15

    .line 675
    const/16 v0, 0x14

    iget-object v1, p0, Lpin;->u:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 677
    :cond_15
    iget-object v0, p0, Lpin;->v:Ljava/lang/Float;

    if-eqz v0, :cond_16

    .line 678
    const/16 v0, 0x15

    iget-object v1, p0, Lpin;->v:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 680
    :cond_16
    iget-object v0, p0, Lpin;->w:Ljava/lang/Float;

    if-eqz v0, :cond_17

    .line 681
    const/16 v0, 0x16

    iget-object v1, p0, Lpin;->w:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 683
    :cond_17
    iget-object v0, p0, Lpin;->x:Ljava/lang/Float;

    if-eqz v0, :cond_18

    .line 684
    const/16 v0, 0x17

    iget-object v1, p0, Lpin;->x:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 686
    :cond_18
    iget-object v0, p0, Lpin;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 688
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpin;->a(Loxn;)Lpin;

    move-result-object v0

    return-object v0
.end method
