.class public final Lmda;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntd;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16441
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16444
    iput-object v0, p0, Lmda;->apiHeader:Llyq;

    .line 16447
    iput-object v0, p0, Lmda;->a:Lntd;

    .line 16441
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 16464
    const/4 v0, 0x0

    .line 16465
    iget-object v1, p0, Lmda;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 16466
    const/4 v0, 0x1

    iget-object v1, p0, Lmda;->apiHeader:Llyq;

    .line 16467
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16469
    :cond_0
    iget-object v1, p0, Lmda;->a:Lntd;

    if-eqz v1, :cond_1

    .line 16470
    const/4 v1, 0x2

    iget-object v2, p0, Lmda;->a:Lntd;

    .line 16471
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16473
    :cond_1
    iget-object v1, p0, Lmda;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16474
    iput v0, p0, Lmda;->ai:I

    .line 16475
    return v0
.end method

.method public a(Loxn;)Lmda;
    .locals 2

    .prologue
    .line 16483
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 16484
    sparse-switch v0, :sswitch_data_0

    .line 16488
    iget-object v1, p0, Lmda;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 16489
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmda;->ah:Ljava/util/List;

    .line 16492
    :cond_1
    iget-object v1, p0, Lmda;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16494
    :sswitch_0
    return-object p0

    .line 16499
    :sswitch_1
    iget-object v0, p0, Lmda;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 16500
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmda;->apiHeader:Llyq;

    .line 16502
    :cond_2
    iget-object v0, p0, Lmda;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16506
    :sswitch_2
    iget-object v0, p0, Lmda;->a:Lntd;

    if-nez v0, :cond_3

    .line 16507
    new-instance v0, Lntd;

    invoke-direct {v0}, Lntd;-><init>()V

    iput-object v0, p0, Lmda;->a:Lntd;

    .line 16509
    :cond_3
    iget-object v0, p0, Lmda;->a:Lntd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16484
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 16452
    iget-object v0, p0, Lmda;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 16453
    const/4 v0, 0x1

    iget-object v1, p0, Lmda;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16455
    :cond_0
    iget-object v0, p0, Lmda;->a:Lntd;

    if-eqz v0, :cond_1

    .line 16456
    const/4 v0, 0x2

    iget-object v1, p0, Lmda;->a:Lntd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16458
    :cond_1
    iget-object v0, p0, Lmda;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 16460
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16437
    invoke-virtual {p0, p1}, Lmda;->a(Loxn;)Lmda;

    move-result-object v0

    return-object v0
.end method
