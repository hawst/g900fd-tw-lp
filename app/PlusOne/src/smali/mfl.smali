.class public final Lmfl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlt;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1294
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1297
    iput-object v0, p0, Lmfl;->apiHeader:Llyr;

    .line 1300
    iput-object v0, p0, Lmfl;->a:Lnlt;

    .line 1294
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1317
    const/4 v0, 0x0

    .line 1318
    iget-object v1, p0, Lmfl;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 1319
    const/4 v0, 0x1

    iget-object v1, p0, Lmfl;->apiHeader:Llyr;

    .line 1320
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1322
    :cond_0
    iget-object v1, p0, Lmfl;->a:Lnlt;

    if-eqz v1, :cond_1

    .line 1323
    const/4 v1, 0x2

    iget-object v2, p0, Lmfl;->a:Lnlt;

    .line 1324
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1326
    :cond_1
    iget-object v1, p0, Lmfl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1327
    iput v0, p0, Lmfl;->ai:I

    .line 1328
    return v0
.end method

.method public a(Loxn;)Lmfl;
    .locals 2

    .prologue
    .line 1336
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1337
    sparse-switch v0, :sswitch_data_0

    .line 1341
    iget-object v1, p0, Lmfl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1342
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfl;->ah:Ljava/util/List;

    .line 1345
    :cond_1
    iget-object v1, p0, Lmfl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1347
    :sswitch_0
    return-object p0

    .line 1352
    :sswitch_1
    iget-object v0, p0, Lmfl;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 1353
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfl;->apiHeader:Llyr;

    .line 1355
    :cond_2
    iget-object v0, p0, Lmfl;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1359
    :sswitch_2
    iget-object v0, p0, Lmfl;->a:Lnlt;

    if-nez v0, :cond_3

    .line 1360
    new-instance v0, Lnlt;

    invoke-direct {v0}, Lnlt;-><init>()V

    iput-object v0, p0, Lmfl;->a:Lnlt;

    .line 1362
    :cond_3
    iget-object v0, p0, Lmfl;->a:Lnlt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1337
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1305
    iget-object v0, p0, Lmfl;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 1306
    const/4 v0, 0x1

    iget-object v1, p0, Lmfl;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1308
    :cond_0
    iget-object v0, p0, Lmfl;->a:Lnlt;

    if-eqz v0, :cond_1

    .line 1309
    const/4 v0, 0x2

    iget-object v1, p0, Lmfl;->a:Lnlt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1311
    :cond_1
    iget-object v0, p0, Lmfl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1313
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1290
    invoke-virtual {p0, p1}, Lmfl;->a(Loxn;)Lmfl;

    move-result-object v0

    return-object v0
.end method
