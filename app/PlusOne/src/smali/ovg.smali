.class public final Lovg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Loux;

.field private b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0}, Loxq;-><init>()V

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lovg;->a:Loux;

    .line 206
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    iget-object v1, p0, Lovg;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 230
    const/4 v0, 0x1

    iget-object v1, p0, Lovg;->b:Ljava/lang/Boolean;

    .line 231
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 233
    :cond_0
    iget-object v1, p0, Lovg;->a:Loux;

    if-eqz v1, :cond_1

    .line 234
    const/4 v1, 0x2

    iget-object v2, p0, Lovg;->a:Loux;

    .line 235
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_1
    iget-object v1, p0, Lovg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    iput v0, p0, Lovg;->ai:I

    .line 239
    return v0
.end method

.method public a(Loxn;)Lovg;
    .locals 2

    .prologue
    .line 247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 248
    sparse-switch v0, :sswitch_data_0

    .line 252
    iget-object v1, p0, Lovg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lovg;->ah:Ljava/util/List;

    .line 256
    :cond_1
    iget-object v1, p0, Lovg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    :sswitch_0
    return-object p0

    .line 263
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lovg;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 267
    :sswitch_2
    iget-object v0, p0, Lovg;->a:Loux;

    if-nez v0, :cond_2

    .line 268
    new-instance v0, Loux;

    invoke-direct {v0}, Loux;-><init>()V

    iput-object v0, p0, Lovg;->a:Loux;

    .line 270
    :cond_2
    iget-object v0, p0, Lovg;->a:Loux;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 248
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lovg;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 217
    const/4 v0, 0x1

    iget-object v1, p0, Lovg;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 219
    :cond_0
    iget-object v0, p0, Lovg;->a:Loux;

    if-eqz v0, :cond_1

    .line 220
    const/4 v0, 0x2

    iget-object v1, p0, Lovg;->a:Loux;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 222
    :cond_1
    iget-object v0, p0, Lovg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 224
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Lovg;->a(Loxn;)Lovg;

    move-result-object v0

    return-object v0
.end method
