.class final Ljgc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljgb;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/ImageView;

.field private final c:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/ImageView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/view/View;

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Ljgc;->a:Landroid/view/View;

    .line 36
    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ljgc;->b:Landroid/widget/ImageView;

    .line 37
    const v0, 0x7f10033d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Ljgc;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 38
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ljgc;->d:Landroid/widget/TextView;

    .line 39
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ljgc;->e:Landroid/widget/TextView;

    .line 40
    const v0, 0x7f10033f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ljgc;->f:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f100340

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ljgc;->g:Landroid/widget/ImageView;

    .line 42
    const v0, 0x7f100341

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ljgc;->h:Landroid/widget/ImageView;

    .line 43
    const v0, 0x1020001

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ljgc;->i:Landroid/widget/ImageView;

    .line 44
    const v0, 0x7f100342

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ljgc;->j:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f10016d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ljgc;->k:Landroid/view/View;

    .line 46
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/widget/TextView;I)V
    .locals 1

    .prologue
    .line 93
    invoke-static {p1, p2, p3}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 94
    invoke-static {p1, p3}, Llib;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 95
    return-void
.end method


# virtual methods
.method public a(Lhmk;)Lhmk;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Ljgc;->a:Landroid/view/View;

    invoke-static {v0, p1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 146
    iget-object v1, p0, Ljgc;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 147
    iget-object v1, p0, Ljgc;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 148
    iget-object v1, p0, Ljgc;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 149
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Ljgc;->l:Z

    .line 150
    return-void
.end method

.method public a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 114
    if-eqz p2, :cond_3

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljgc;->l:Z

    .line 116
    iget-object v0, p0, Ljgc;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 117
    iget-object v0, p0, Ljgc;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v1, p0, Ljgc;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203d4

    .line 118
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 117
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->f(Landroid/graphics/drawable/Drawable;)V

    .line 119
    iget-object v0, p0, Ljgc;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Ljgc;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 124
    :goto_0
    iget-object v0, p0, Ljgc;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Ljgc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Ljgc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    :cond_0
    iget-object v0, p0, Ljgc;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 131
    iget-object v0, p0, Ljgc;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Ljgc;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :cond_1
    if-nez p7, :cond_2

    .line 135
    iget-object v0, p0, Ljgc;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 137
    :cond_2
    return-void

    .line 122
    :cond_3
    invoke-virtual {p0, p1}, Ljgc;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;IZZ)V
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v1, 0x4

    const/16 v5, 0x1a

    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 53
    iput-boolean v0, p0, Ljgc;->l:Z

    iget-object v2, p0, Ljgc;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Ljgc;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Ljgc;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Ljgc;->h:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Ljgc;->i:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Ljgc;->j:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Ljgc;->k:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Ljgc;->a:Landroid/view/View;

    const v3, 0x3a5748f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 54
    packed-switch p2, :pswitch_data_0

    .line 85
    :goto_0
    if-eqz p3, :cond_0

    .line 86
    iget-object v2, p0, Ljgc;->i:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    :cond_0
    iget-object v2, p0, Ljgc;->k:Landroid/view/View;

    if-eqz p4, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 90
    return-void

    .line 56
    :pswitch_0
    iget-object v2, p0, Ljgc;->d:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2, v5}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 57
    iget-object v2, p0, Ljgc;->e:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2, v6}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    goto :goto_0

    .line 60
    :pswitch_1
    iget-object v2, p0, Ljgc;->d:Landroid/widget/TextView;

    const/16 v3, 0xc

    invoke-direct {p0, p1, v2, v3}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    goto :goto_0

    .line 63
    :pswitch_2
    iget-object v2, p0, Ljgc;->d:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2, v1}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 64
    iget-object v2, p0, Ljgc;->e:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2, v6}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 65
    iget-object v2, p0, Ljgc;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 68
    :pswitch_3
    iget-object v2, p0, Ljgc;->d:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2, v5}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 69
    iget-object v2, p0, Ljgc;->f:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2, v5}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 72
    iget-object v2, p0, Ljgc;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 73
    iget-object v2, p0, Ljgc;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 77
    :pswitch_4
    iget-object v2, p0, Ljgc;->d:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2, v5}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 78
    iget-object v2, p0, Ljgc;->h:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 82
    :pswitch_5
    iget-object v2, p0, Ljgc;->d:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2, v1}, Ljgc;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 89
    goto :goto_1

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Ljgc;->l:Z

    return v0
.end method
