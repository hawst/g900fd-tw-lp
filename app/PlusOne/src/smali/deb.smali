.class public Ldeb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljlw",
        "<",
        "Ldeb;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<",
            "Ldeb;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldea;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lhov;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldeb;->b:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Ljls;

    invoke-direct {v0, p0, p1}, Ljls;-><init>(Ljava/lang/Object;Lhov;)V

    iput-object v0, p0, Ldeb;->a:Ljlx;

    .line 21
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ldeb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Ldea;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldeb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldea;

    return-object v0
.end method

.method public a(Ldea;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ldeb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    invoke-virtual {p0}, Ldeb;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 31
    return-void
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Ldeb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Ldeb;->a:Ljlx;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ldeb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 43
    invoke-virtual {p0}, Ldeb;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 44
    return-void
.end method
