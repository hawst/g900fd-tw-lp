.class public final Lfnq;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhei;

.field private final c:Lkfd;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    const-string v0, "DeleteStoryTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 43
    iput-object p1, p0, Lfnq;->a:Landroid/content/Context;

    .line 44
    iput p2, p0, Lfnq;->d:I

    .line 45
    iput-object p3, p0, Lfnq;->e:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lfnq;->f:Ljava/lang/String;

    .line 47
    iput p5, p0, Lfnq;->h:I

    .line 48
    iput-object p6, p0, Lfnq;->i:Ljava/lang/String;

    .line 49
    iput-object p7, p0, Lfnq;->j:Ljava/lang/String;

    .line 51
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfnq;->b:Lhei;

    .line 52
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lfnq;->c:Lkfd;

    .line 53
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 8

    .prologue
    .line 57
    iget-object v0, p0, Lfnq;->b:Lhei;

    iget v1, p0, Lfnq;->d:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v3

    .line 58
    new-instance v0, Lfnj;

    iget-object v1, p0, Lfnq;->a:Landroid/content/Context;

    new-instance v2, Lkfo;

    const-string v4, "account_name"

    .line 59
    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfnq;->e:Ljava/lang/String;

    iget-object v4, p0, Lfnq;->f:Ljava/lang/String;

    iget v5, p0, Lfnq;->h:I

    invoke-direct/range {v0 .. v5}, Lfnj;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 62
    iget-object v1, p0, Lfnq;->c:Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 63
    invoke-virtual {v0}, Lfnj;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    iget-object v1, p0, Lfnq;->a:Landroid/content/Context;

    iget v2, p0, Lfnq;->d:I

    iget-object v3, p0, Lfnq;->e:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lfoa;->b(Landroid/content/Context;ILjava/lang/String;)V

    .line 65
    iget-object v1, p0, Lfnq;->a:Landroid/content/Context;

    iget v2, p0, Lfnq;->d:I

    iget-object v3, p0, Lfnq;->i:Ljava/lang/String;

    const/16 v4, 0x8

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lfnq;->j:Ljava/lang/String;

    aput-object v7, v5, v6

    .line 66
    invoke-static {v4, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 65
    invoke-static {v1, v2, v3, v4}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    new-instance v1, Lhoz;

    .line 69
    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    iget-object v4, v0, Lkff;->j:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 70
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "delete_succeeded"

    invoke-virtual {v0}, Lfnj;->t()Z

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 71
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "delete_type"

    iget v3, p0, Lfnq;->h:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 72
    return-object v1
.end method
