.class public final Ldng;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmjg;",
        "Lmjh;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Lnzi;

.field private final q:Ljava/lang/String;

.field private final r:Z

.field private s:Lnzi;

.field private t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnzi;Z)V
    .locals 6

    .prologue
    .line 40
    const-string v3, "setphotoeditlist"

    new-instance v4, Lmjg;

    invoke-direct {v4}, Lmjg;-><init>()V

    new-instance v5, Lmjh;

    invoke-direct {v5}, Lmjh;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 42
    iput-object p3, p0, Ldng;->a:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Ldng;->b:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Ldng;->p:Lnzi;

    .line 45
    iput-object p5, p0, Ldng;->q:Ljava/lang/String;

    .line 46
    iput-boolean p7, p0, Ldng;->r:Z

    .line 47
    return-void
.end method


# virtual methods
.method protected a(Lmjg;)V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lnes;

    invoke-direct {v0}, Lnes;-><init>()V

    iput-object v0, p1, Lmjg;->a:Lnes;

    .line 52
    iget-object v1, p1, Lmjg;->a:Lnes;

    .line 53
    iget-object v0, p0, Ldng;->p:Lnzi;

    iput-object v0, v1, Lnes;->c:Lnzi;

    .line 54
    iget-object v0, p0, Ldng;->b:Ljava/lang/String;

    iput-object v0, v1, Lnes;->a:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Ldng;->a:Ljava/lang/String;

    iput-object v0, v1, Lnes;->b:Ljava/lang/String;

    .line 56
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, v1, Lnes;->d:Ljava/lang/Boolean;

    .line 57
    iget-boolean v0, p0, Ldng;->r:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, v1, Lnes;->f:Ljava/lang/Boolean;

    .line 58
    new-instance v0, Lnea;

    invoke-direct {v0}, Lnea;-><init>()V

    iput-object v0, v1, Lnes;->e:Lnea;

    .line 59
    iget-object v0, v1, Lnes;->e:Lnea;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnea;->a:Ljava/lang/Boolean;

    .line 60
    return-void

    .line 57
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method protected a(Lmjh;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 64
    iget-object v4, p1, Lmjh;->a:Lnft;

    .line 65
    iget-object v0, v4, Lnft;->b:Lnzi;

    iput-object v0, p0, Ldng;->s:Lnzi;

    .line 66
    iget-object v0, v4, Lnft;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Ldng;->t:Z

    .line 67
    iget-object v0, v4, Lnft;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    .line 69
    new-instance v0, Lnzu;

    invoke-direct {v0}, Lnzu;-><init>()V

    .line 70
    iget-object v1, v4, Lnft;->c:Lnym;

    iput-object v1, v0, Lnzu;->b:Lnym;

    .line 71
    sget-object v1, Lnzu;->a:Loxr;

    invoke-virtual {v3, v1, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 72
    new-instance v0, Lnyl;

    invoke-direct {v0}, Lnyl;-><init>()V

    iput-object v0, v3, Lnzx;->f:Lnyl;

    .line 73
    iget-object v0, v4, Lnft;->c:Lnym;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lnft;->c:Lnym;

    iget-object v0, v0, Lnym;->b:Lnyl;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, v3, Lnzx;->f:Lnyl;

    iget-object v1, v4, Lnft;->c:Lnym;

    iget-object v1, v1, Lnym;->b:Lnyl;

    iget-object v1, v1, Lnyl;->b:Ljava/lang/String;

    iput-object v1, v0, Lnyl;->b:Ljava/lang/String;

    .line 75
    iget-object v0, v3, Lnzx;->f:Lnyl;

    iget-object v1, v4, Lnft;->c:Lnym;

    iget-object v1, v1, Lnym;->b:Lnyl;

    iget-object v1, v1, Lnyl;->c:Ljava/lang/Integer;

    iput-object v1, v0, Lnyl;->c:Ljava/lang/Integer;

    .line 76
    iget-object v0, v3, Lnzx;->f:Lnyl;

    iget-object v1, v4, Lnft;->c:Lnym;

    iget-object v1, v1, Lnym;->b:Lnyl;

    iget-object v1, v1, Lnyl;->d:Ljava/lang/Integer;

    iput-object v1, v0, Lnyl;->d:Ljava/lang/Integer;

    .line 83
    :cond_0
    iget-object v0, p0, Ldng;->q:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Ldng;->q:Ljava/lang/String;

    iput-object v0, v3, Lnzx;->b:Ljava/lang/String;

    .line 87
    iget-object v0, p0, Ldng;->f:Landroid/content/Context;

    iget v1, p0, Ldng;->c:I

    const/4 v2, 0x2

    invoke-static {v0, v1, v3, v2}, Ljvj;->a(Landroid/content/Context;ILnzx;I)V

    .line 90
    :cond_1
    iget-object v0, v4, Lnft;->c:Lnym;

    if-eqz v0, :cond_2

    iget-object v0, v4, Lnft;->c:Lnym;

    iget-object v0, v0, Lnym;->h:Lnyz;

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Ldng;->f:Landroid/content/Context;

    iget v1, p0, Ldng;->c:I

    new-array v2, v5, [Lnzx;

    aput-object v3, v2, v6

    new-array v3, v6, [Lotf;

    iget-object v4, v4, Lnft;->c:Lnym;

    iget-object v4, v4, Lnym;->h:Lnyz;

    invoke-static/range {v0 .. v5}, Ljvd;->a(Landroid/content/Context;I[Lnzx;[Lotf;Lnyz;Z)V

    .line 95
    :cond_2
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmjg;

    invoke-virtual {p0, p1}, Ldng;->a(Lmjg;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmjh;

    invoke-virtual {p0, p1}, Ldng;->a(Lmjh;)V

    return-void
.end method

.method public d()Lnzi;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Ldng;->s:Lnzi;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Ldng;->t:Z

    return v0
.end method
