.class public final Lnas;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 186
    invoke-direct {p0}, Loxq;-><init>()V

    .line 191
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnas;->b:[Ljava/lang/String;

    .line 186
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 208
    const/4 v0, 0x1

    iget-object v2, p0, Lnas;->a:Ljava/lang/String;

    .line 210
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 211
    iget-object v2, p0, Lnas;->b:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lnas;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 213
    iget-object v3, p0, Lnas;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 215
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 213
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 217
    :cond_0
    add-int/2addr v0, v2

    .line 218
    iget-object v1, p0, Lnas;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 220
    :cond_1
    iget-object v1, p0, Lnas;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    iput v0, p0, Lnas;->ai:I

    .line 222
    return v0
.end method

.method public a(Loxn;)Lnas;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 230
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 231
    sparse-switch v0, :sswitch_data_0

    .line 235
    iget-object v1, p0, Lnas;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 236
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnas;->ah:Ljava/util/List;

    .line 239
    :cond_1
    iget-object v1, p0, Lnas;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    :sswitch_0
    return-object p0

    .line 246
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnas;->a:Ljava/lang/String;

    goto :goto_0

    .line 250
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 251
    iget-object v0, p0, Lnas;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 252
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 253
    iget-object v2, p0, Lnas;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 254
    iput-object v1, p0, Lnas;->b:[Ljava/lang/String;

    .line 255
    :goto_1
    iget-object v1, p0, Lnas;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 256
    iget-object v1, p0, Lnas;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 257
    invoke-virtual {p1}, Loxn;->a()I

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 260
    :cond_2
    iget-object v1, p0, Lnas;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 231
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 196
    const/4 v0, 0x1

    iget-object v1, p0, Lnas;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 197
    iget-object v0, p0, Lnas;->b:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 198
    iget-object v1, p0, Lnas;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 199
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    :cond_0
    iget-object v0, p0, Lnas;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 204
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0, p1}, Lnas;->a(Loxn;)Lnas;

    move-result-object v0

    return-object v0
.end method
