.class public final Lddx;
.super Lddr;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lddx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private D:Lhei;

.field private final E:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lddy;

    invoke-direct {v0}, Lddy;-><init>()V

    sput-object v0, Lddx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lddr;-><init>(Landroid/os/Parcel;)V

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddx;->E:Ljava/lang/String;

    .line 97
    return-void
.end method

.method protected constructor <init>(Ldds;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lddr;-><init>(Ldds;)V

    .line 91
    check-cast p1, Lddz;

    iget-object v0, p1, Lddz;->a:Ljava/lang/String;

    iput-object v0, p0, Lddx;->E:Ljava/lang/String;

    .line 92
    return-void
.end method

.method static synthetic a(Lddx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lddx;->E:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 163
    sget-object v0, Ldba;->N:Lloz;

    const/4 v0, 0x0

    return v0
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    return v0
.end method

.method public L()Z
    .locals 1

    .prologue
    .line 178
    invoke-super {p0}, Lddr;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lddx;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()Z
    .locals 2

    .prologue
    .line 168
    iget v0, p0, Lddx;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lddx;->D:Lhei;

    iget v1, p0, Lddx;->b:I

    .line 169
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    :goto_0
    invoke-virtual {p0}, Lddx;->j()Ljava/lang/String;

    move-result-object v1

    .line 172
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 173
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public S()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lddx;->h:Lnym;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lddx;->h:Lnym;

    iget-object v0, v0, Lnym;->b:Lnyl;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lddx;->h:Lnym;

    iget-object v0, v0, Lnym;->b:Lnyl;

    iget-object v0, v0, Lnyl;->c:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    .line 187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public T()I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lddx;->h:Lnym;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lddx;->h:Lnym;

    iget-object v0, v0, Lnym;->b:Lnyl;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lddx;->h:Lnym;

    iget-object v0, v0, Lnym;->b:Lnyl;

    iget-object v0, v0, Lnyl;->d:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    .line 196
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public V()Ldds;
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lddz;

    invoke-direct {v0}, Lddz;-><init>()V

    return-object v0
.end method

.method public a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lddx;->C:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 115
    :goto_0
    invoke-virtual {p0}, Lddx;->e()Lnzi;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v1, v0}, Lddx;->a(Landroid/content/Context;ILnzi;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 202
    invoke-super {p0, p1}, Lddr;->a(Landroid/content/Context;)V

    .line 203
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lddx;->D:Lhei;

    .line 204
    return-void
.end method

.method public b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 120
    .line 121
    invoke-virtual {p0}, Lddx;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    sget v3, Ljvv;->c:I

    sget v4, Ljvv;->c:I

    const/16 v5, 0x120

    move-object v0, p1

    .line 120
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;III)Landroid/net/Uri;

    move-result-object v0

    .line 125
    const-string v1, "image/jpeg"

    invoke-virtual {p0, p1, v0, v1}, Lddx;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljuf;
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Lddx;->i:Ljcn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lddx;->E:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 131
    :cond_0
    const/4 v0, 0x0

    .line 135
    :goto_0
    return-object v0

    .line 134
    :cond_1
    iget-object v0, p0, Lddx;->i:Ljcn;

    new-instance v1, Ljug;

    .line 135
    iget-object v2, p0, Lddx;->E:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljue;

    invoke-virtual {p0}, Lddx;->a()Lizu;

    move-result-object v3

    invoke-direct {v2, v3}, Ljue;-><init>(Lizu;)V

    invoke-virtual {v0, v1, v2}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v0

    check-cast v0, Ljuc;

    goto :goto_0
.end method

.method public d()Ljuf;
    .locals 11

    .prologue
    .line 140
    iget-object v0, p0, Lddx;->a:Landroid/os/Bundle;

    const-string v1, "view_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    new-instance v1, Ljuc;

    iget-object v0, p0, Lddx;->E:Ljava/lang/String;

    if-nez v0, :cond_0

    move-object v3, v2

    .line 142
    :goto_0
    invoke-virtual {p0}, Lddx;->a()Lizu;

    move-result-object v4

    invoke-virtual {p0}, Lddx;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lddx;->n()J

    move-result-wide v6

    invoke-virtual {p0}, Lddx;->X()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-direct/range {v1 .. v10}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;Ljava/lang/String;JJLnzi;)V

    return-object v1

    .line 141
    :cond_0
    iget-object v3, p0, Lddx;->E:Ljava/lang/String;

    goto :goto_0
.end method

.method public v()Z
    .locals 4

    .prologue
    .line 147
    invoke-super {p0}, Lddr;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lddx;->x:J

    const-wide/32 v2, 0x8000000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0, p1, p2}, Lddr;->writeToParcel(Landroid/os/Parcel;I)V

    .line 102
    iget-object v0, p0, Lddx;->E:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    return-void
.end method
