.class public final Lhbt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhbw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    .line 531
    iget-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    new-instance v1, Lhbv;

    sget-object v2, Lphe;->a:Loxr;

    invoke-direct {v1, v2}, Lhbv;-><init>(Loxr;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 534
    iget-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    new-instance v1, Lhbu;

    sget-object v2, Lpfs;->a:Loxr;

    invoke-direct {v1, v2}, Lhbu;-><init>(Loxr;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 537
    sget-object v0, Lpjw;->a:Loxr;

    invoke-direct {p0, v0}, Lhbt;->a(Loxr;)V

    .line 538
    iget-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    new-instance v1, Lhbz;

    sget-object v2, Lpop;->a:Loxr;

    invoke-direct {v1, v2}, Lhbz;-><init>(Loxr;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    iget-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    new-instance v1, Lhca;

    sget-object v2, Lpot;->a:Loxr;

    invoke-direct {v1, v2}, Lhca;-><init>(Loxr;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    iget-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    new-instance v1, Lhcc;

    sget-object v2, Lpuy;->a:Loxr;

    invoke-direct {v1, v2}, Lhcc;-><init>(Loxr;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    sget-object v0, Lpsl;->a:Loxr;

    invoke-direct {p0, v0}, Lhbt;->a(Loxr;)V

    .line 547
    sget-object v0, Lpli;->a:Loxr;

    invoke-direct {p0, v0}, Lhbt;->a(Loxr;)V

    .line 550
    sget-object v0, Lptm;->a:Loxr;

    invoke-direct {p0, v0}, Lhbt;->a(Loxr;)V

    .line 551
    iget-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    new-instance v1, Lhcb;

    sget-object v2, Lptx;->a:Loxr;

    invoke-direct {v1, v2}, Lhcb;-><init>(Loxr;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    iget-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    new-instance v1, Lhcd;

    sget-object v2, Lpxd;->a:Loxr;

    invoke-direct {v1, v2}, Lhcd;-><init>(Loxr;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 557
    return-void
.end method

.method private a(Loxr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxq;",
            ">(",
            "Loxr",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 518
    iget-object v0, p0, Lhbt;->a:Ljava/util/ArrayList;

    new-instance v1, Lhby;

    invoke-direct {v1}, Lhby;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    return-void
.end method

.method public static a(Lnzi;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 704
    if-eqz p0, :cond_0

    iget-object v2, p0, Lnzi;->b:Lpla;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->a:[Lpme;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->a:[Lpme;

    array-length v2, v2

    if-nez v2, :cond_2

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_5

    .line 716
    :cond_1
    :goto_1
    return v0

    .line 704
    :cond_2
    iget-object v2, p0, Lnzi;->b:Lpla;

    iget-object v3, v2, Lpla;->a:[Lpme;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    invoke-static {v5, v0}, Lhbt;->a(Lpme;I)Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v0

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_0

    .line 710
    :cond_5
    iget-object v2, p0, Lnzi;->b:Lpla;

    iget-object v3, v2, Lpla;->a:[Lpme;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 711
    invoke-static {v5, v0}, Lhbt;->a(Lpme;I)Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lhbt;->a(Lpme;I)Z

    move-result v5

    if-nez v5, :cond_6

    move v0, v1

    .line 712
    goto :goto_1

    .line 710
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public static a(Lpme;)Z
    .locals 1

    .prologue
    .line 664
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lhbt;->a(Lpme;I)Z

    move-result v0

    return v0
.end method

.method public static a(Lpme;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 646
    iget-object v1, p0, Lpme;->b:Lpmd;

    if-nez v1, :cond_1

    .line 657
    :cond_0
    :goto_0
    return v0

    .line 649
    :cond_1
    iget-object v1, p0, Lpme;->b:Lpmd;

    iget-object v1, v1, Lpmd;->a:[I

    if-eqz v1, :cond_0

    .line 652
    iget-object v1, p0, Lpme;->b:Lpmd;

    iget-object v2, v1, Lpmd;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 653
    if-ne v4, p1, :cond_2

    .line 654
    const/4 v0, 0x1

    goto :goto_0

    .line 652
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static b(Lpme;)Z
    .locals 1

    .prologue
    .line 671
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lhbt;->a(Lpme;I)Z

    move-result v0

    return v0
.end method
