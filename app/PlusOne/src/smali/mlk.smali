.class public final Lmlk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlw;

.field public b:Lmok;

.field public c:Lmlj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1156
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1159
    iput-object v0, p0, Lmlk;->a:Lmlw;

    .line 1162
    iput-object v0, p0, Lmlk;->b:Lmok;

    .line 1165
    iput-object v0, p0, Lmlk;->c:Lmlj;

    .line 1156
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1185
    const/4 v0, 0x0

    .line 1186
    iget-object v1, p0, Lmlk;->a:Lmlw;

    if-eqz v1, :cond_0

    .line 1187
    const/4 v0, 0x1

    iget-object v1, p0, Lmlk;->a:Lmlw;

    .line 1188
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1190
    :cond_0
    iget-object v1, p0, Lmlk;->b:Lmok;

    if-eqz v1, :cond_1

    .line 1191
    const/4 v1, 0x2

    iget-object v2, p0, Lmlk;->b:Lmok;

    .line 1192
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1194
    :cond_1
    iget-object v1, p0, Lmlk;->c:Lmlj;

    if-eqz v1, :cond_2

    .line 1195
    const/4 v1, 0x3

    iget-object v2, p0, Lmlk;->c:Lmlj;

    .line 1196
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1198
    :cond_2
    iget-object v1, p0, Lmlk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1199
    iput v0, p0, Lmlk;->ai:I

    .line 1200
    return v0
.end method

.method public a(Loxn;)Lmlk;
    .locals 2

    .prologue
    .line 1208
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1209
    sparse-switch v0, :sswitch_data_0

    .line 1213
    iget-object v1, p0, Lmlk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1214
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmlk;->ah:Ljava/util/List;

    .line 1217
    :cond_1
    iget-object v1, p0, Lmlk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1219
    :sswitch_0
    return-object p0

    .line 1224
    :sswitch_1
    iget-object v0, p0, Lmlk;->a:Lmlw;

    if-nez v0, :cond_2

    .line 1225
    new-instance v0, Lmlw;

    invoke-direct {v0}, Lmlw;-><init>()V

    iput-object v0, p0, Lmlk;->a:Lmlw;

    .line 1227
    :cond_2
    iget-object v0, p0, Lmlk;->a:Lmlw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1231
    :sswitch_2
    iget-object v0, p0, Lmlk;->b:Lmok;

    if-nez v0, :cond_3

    .line 1232
    new-instance v0, Lmok;

    invoke-direct {v0}, Lmok;-><init>()V

    iput-object v0, p0, Lmlk;->b:Lmok;

    .line 1234
    :cond_3
    iget-object v0, p0, Lmlk;->b:Lmok;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1238
    :sswitch_3
    iget-object v0, p0, Lmlk;->c:Lmlj;

    if-nez v0, :cond_4

    .line 1239
    new-instance v0, Lmlj;

    invoke-direct {v0}, Lmlj;-><init>()V

    iput-object v0, p0, Lmlk;->c:Lmlj;

    .line 1241
    :cond_4
    iget-object v0, p0, Lmlk;->c:Lmlj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1209
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1170
    iget-object v0, p0, Lmlk;->a:Lmlw;

    if-eqz v0, :cond_0

    .line 1171
    const/4 v0, 0x1

    iget-object v1, p0, Lmlk;->a:Lmlw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1173
    :cond_0
    iget-object v0, p0, Lmlk;->b:Lmok;

    if-eqz v0, :cond_1

    .line 1174
    const/4 v0, 0x2

    iget-object v1, p0, Lmlk;->b:Lmok;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1176
    :cond_1
    iget-object v0, p0, Lmlk;->c:Lmlj;

    if-eqz v0, :cond_2

    .line 1177
    const/4 v0, 0x3

    iget-object v1, p0, Lmlk;->c:Lmlj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1179
    :cond_2
    iget-object v0, p0, Lmlk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1181
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1152
    invoke-virtual {p0, p1}, Lmlk;->a(Loxn;)Lmlk;

    move-result-object v0

    return-object v0
.end method
