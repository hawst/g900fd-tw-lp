.class public final Lfns;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Z

.field private final h:[Landroid/graphics/Point;

.field private final i:Lhei;

.field private final j:Lkfd;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 38
    const-string v0, "GetStoryTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    iput p2, p0, Lfns;->a:I

    .line 40
    iput-object p3, p0, Lfns;->b:Ljava/lang/String;

    .line 41
    iput-object p4, p0, Lfns;->c:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Lfns;->d:Ljava/lang/String;

    .line 43
    iput-boolean p6, p0, Lfns;->e:Z

    .line 44
    iput-boolean p7, p0, Lfns;->f:Z

    .line 45
    iput-object p8, p0, Lfns;->h:[Landroid/graphics/Point;

    .line 46
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfns;->i:Lhei;

    .line 47
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lfns;->j:Lkfd;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 52
    iget-object v0, p0, Lfns;->i:Lhei;

    iget v1, p0, Lfns;->a:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v3

    .line 53
    new-instance v0, Lfnm;

    invoke-virtual {p0}, Lfns;->f()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lkfo;

    const-string v4, "account_name"

    .line 54
    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v10}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfns;->b:Ljava/lang/String;

    iget-object v4, p0, Lfns;->c:Ljava/lang/String;

    iget-object v5, p0, Lfns;->d:Ljava/lang/String;

    iget-boolean v6, p0, Lfns;->e:Z

    iget-boolean v7, p0, Lfns;->f:Z

    iget-object v8, p0, Lfns;->h:[Landroid/graphics/Point;

    invoke-direct/range {v0 .. v9}, Lfnm;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;Z)V

    .line 57
    iget-object v1, p0, Lfns;->j:Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 59
    invoke-virtual {v0}, Lfnm;->t()Z

    move-result v1

    if-nez v1, :cond_1

    .line 60
    invoke-virtual {v0}, Lfnm;->D()Loxu;

    move-result-object v1

    check-cast v1, Lmdh;

    iget-object v1, v1, Lmdh;->a:Lmlr;

    iget-object v1, v1, Lmlr;->a:Lmmq;

    .line 63
    invoke-virtual {p0}, Lfns;->f()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lfns;->a:I

    invoke-static {v2, v3, v1, v9}, Lfoa;->a(Landroid/content/Context;ILmmq;Z)V

    .line 66
    :goto_0
    new-instance v2, Lhoz;

    iget v3, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v2, v3, v0, v10}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 67
    invoke-virtual {v2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "has_share"

    if-eqz v1, :cond_0

    iget-object v4, v1, Lmmq;->g:Lmlz;

    if-eqz v4, :cond_0

    iget-object v1, v1, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    if-eqz v1, :cond_0

    :goto_1
    invoke-virtual {v0, v3, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 69
    return-object v2

    .line 67
    :cond_0
    const/4 v9, 0x0

    goto :goto_1

    :cond_1
    move-object v1, v10

    goto :goto_0
.end method
