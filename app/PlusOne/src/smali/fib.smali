.class public Lfib;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x7b27e76ee69820faL


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 35
    const/16 v0, 0xc8

    const-string v1, "Ok"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lfib;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    .line 36
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lfib;->a:I

    .line 54
    iput-object p2, p0, Lfib;->b:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lfib;->c:Ljava/lang/Exception;

    .line 56
    return-void
.end method

.method public constructor <init>(Lkff;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iget v0, p1, Lkff;->i:I

    iput v0, p0, Lfib;->a:I

    .line 65
    iget-object v0, p1, Lkff;->j:Ljava/lang/String;

    iput-object v0, p0, Lfib;->b:Ljava/lang/String;

    .line 66
    iget-object v0, p1, Lkff;->k:Ljava/lang/Exception;

    iput-object v0, p0, Lfib;->c:Ljava/lang/Exception;

    .line 67
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3

    .prologue
    .line 42
    if-eqz p1, :cond_0

    const/16 v0, 0xc8

    move v1, v0

    :goto_0
    if-eqz p1, :cond_1

    const-string v0, "Ok"

    :goto_1
    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lfib;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    .line 43
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "Error"

    goto :goto_1
.end method


# virtual methods
.method public c()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lfib;->a:I

    return v0
.end method

.method public d()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lfib;->c:Ljava/lang/Exception;

    return-object v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 94
    iget v0, p0, Lfib;->a:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 99
    const-string v0, "ServiceResult(errorCode=%d, reasonPhrase=%s, exception=%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lfib;->a:I

    .line 100
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lfib;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lfib;->c:Ljava/lang/Exception;

    aput-object v3, v1, v2

    .line 99
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
