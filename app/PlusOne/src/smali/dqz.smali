.class final Ldqz;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Landroid/graphics/Bitmap;

.field private synthetic b:Ldqx;


# direct methods
.method constructor <init>(Ldqx;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Ldqz;->b:Ldqx;

    iput-object p2, p0, Ldqz;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 381
    iget-object v0, p0, Ldqz;->b:Ldqx;

    iget-object v1, v0, Ldqx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0}, Ldqx;->j()Lizu;

    move-result-object v3

    invoke-virtual {v3}, Lizu;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Ldqx;->j()Lizu;

    move-result-object v3

    invoke-virtual {v3}, Lizu;->e()Landroid/net/Uri;

    move-result-object v3

    :try_start_0
    invoke-virtual {v1, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    new-instance v3, Lidp;

    invoke-direct {v3}, Lidp;-><init>()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v3, v1}, Lidp;->a(Ljava/io/InputStream;)V

    sget v4, Lidp;->n:I

    invoke-virtual {v3, v4}, Lidp;->d(I)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v0, Ldqx;->f:I

    :cond_0
    sget v4, Lidp;->c:I

    invoke-virtual {v3, v4}, Lidp;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Ldqx;->g:Ljava/lang/String;

    sget v4, Lidp;->d:I

    invoke-virtual {v3, v4}, Lidp;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Ldqx;->h:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 382
    :cond_1
    :goto_1
    return-object v2

    .line 381
    :catch_0
    move-exception v0

    :try_start_4
    const-string v3, "DefaultEditorProvider"

    const-string v4, "Failed to read Exif data."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_2
    :try_start_5
    const-string v3, "DefaultEditorProvider"

    const-string v4, "Failed to open file."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v1, :cond_1

    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "DefaultEditorProvider"

    const-string v3, "Failed to close file."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_3
    move-exception v0

    const-string v1, "DefaultEditorProvider"

    const-string v3, "Failed to close file."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_2
    :goto_4
    throw v0

    :catch_4
    move-exception v1

    const-string v2, "DefaultEditorProvider"

    const-string v3, "Failed to close file."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 386
    new-instance v1, Ldxa;

    invoke-direct {v1}, Ldxa;-><init>()V

    .line 387
    iget-object v0, p0, Ldqz;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Ldxa;->a(Landroid/graphics/Bitmap;)V

    .line 388
    iget-object v0, p0, Ldqz;->b:Ldqx;

    iget-object v0, v0, Ldqx;->e:Lnzi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqz;->b:Ldqx;

    .line 389
    iget-object v0, v0, Ldqx;->e:Lnzi;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 388
    :goto_0
    invoke-virtual {v1, v0}, Ldxa;->a([B)V

    .line 390
    iget-object v0, p0, Ldqz;->b:Ldqx;

    iget-object v0, v0, Ldqx;->b:Ldxb;

    invoke-interface {v0, v1}, Ldxb;->a(Ldxa;)V

    .line 391
    return-void

    .line 389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 378
    invoke-virtual {p0}, Ldqz;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 378
    invoke-virtual {p0}, Ldqz;->b()V

    return-void
.end method
