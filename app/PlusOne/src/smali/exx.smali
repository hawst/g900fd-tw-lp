.class public final Lexx;
.super Ljfy;
.source "PG"


# instance fields
.field private c:[Ldwz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljfy;-><init>()V

    .line 31
    const/4 v0, 0x0

    new-array v0, v0, [Ldwz;

    iput-object v0, p0, Lexx;->c:[Ldwz;

    .line 37
    const-string v0, "explore"

    invoke-virtual {p0, v0}, Lexx;->b(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method private j(I)I
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lexx;->av_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    add-int/lit8 p1, p1, -0x1

    .line 58
    :cond_0
    return p1
.end method

.method private k(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 71
    const-string v0, ""

    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    iget-object v0, p0, Lexx;->c:[Ldwz;

    invoke-direct {p0, p1}, Lexx;->j(I)I

    move-result v1

    aget-object v0, v0, v1

    iget-object v0, v0, Ldwz;->a:Ljava/lang/String;

    goto :goto_0

    .line 66
    :pswitch_1
    iget-object v0, p0, Lexx;->a:Landroid/content/Context;

    const v1, 0x7f0a0529

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :pswitch_2
    iget-object v0, p0, Lexx;->a:Landroid/content/Context;

    const v1, 0x7f0a06f9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private l(I)I
    .locals 1

    .prologue
    .line 142
    if-nez p1, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 149
    :goto_0
    return v0

    .line 144
    :cond_0
    iget-object v0, p0, Lexx;->c:[Ldwz;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_1

    .line 145
    const/4 v0, 0x1

    goto :goto_0

    .line 146
    :cond_1
    iget-object v0, p0, Lexx;->c:[Ldwz;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    if-ne p1, v0, :cond_2

    .line 147
    const/4 v0, 0x2

    goto :goto_0

    .line 149
    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private m(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 300
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    if-ne p1, v0, :cond_1

    .line 301
    :cond_0
    iget-object v0, p0, Lexx;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lexx;->b:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    .line 302
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    .line 304
    :cond_1
    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lexx;->c:[Ldwz;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public a(I)Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 92
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v6

    .line 93
    const/4 v0, 0x3

    if-ne v6, v0, :cond_1

    .line 94
    iget-object v0, p0, Lexx;->a:Landroid/content/Context;

    iget v1, p0, Lexx;->b:I

    .line 95
    invoke-direct {p0, p1}, Lexx;->k(I)Ljava/lang/String;

    move-result-object v2

    .line 94
    invoke-static {v0, v1, v2}, Leyq;->i(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 114
    :cond_0
    :goto_0
    return-object v3

    .line 96
    :cond_1
    if-eqz v6, :cond_0

    .line 99
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lexx;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    const-string v1, "destination"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 102
    const/4 v1, 0x1

    if-ne v6, v1, :cond_3

    .line 103
    const-string v10, "circle_info"

    new-instance v1, Levm;

    iget-object v2, p0, Lexx;->a:Landroid/content/Context;

    const-string v4, "v.whatshot"

    int-to-long v8, v6

    move v6, v5

    move v7, v5

    invoke-direct/range {v1 .. v9}, Levm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZIJ)V

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_2
    :goto_1
    move-object v3, v0

    .line 114
    goto :goto_0

    .line 107
    :cond_3
    const/4 v1, 0x2

    if-ne v6, v1, :cond_2

    .line 108
    const-string v10, "circle_info"

    new-instance v1, Levm;

    iget-object v2, p0, Lexx;->a:Landroid/content/Context;

    const-string v4, "v.nearby"

    int-to-long v8, v6

    move v6, v5

    move v7, v5

    invoke-direct/range {v1 .. v9}, Levm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZIJ)V

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public a(ILjgb;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 248
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v8

    .line 249
    invoke-direct {p0, v8}, Lexx;->m(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x0

    .line 253
    :goto_1
    invoke-direct {p0, p1}, Lexx;->k(I)Ljava/lang/String;

    move-result-object v3

    .line 254
    invoke-virtual {p0}, Lexx;->r()Z

    move-result v7

    move-object v0, p2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    .line 252
    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 255
    const/4 v0, 0x1

    if-ne v8, v0, :cond_2

    .line 256
    new-instance v0, Lhmk;

    sget-object v1, Lomy;->o:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p2, v0}, Ljgb;->a(Lhmk;)Lhmk;

    goto :goto_0

    .line 252
    :pswitch_0
    iget-object v0, p0, Lexx;->c:[Ldwz;

    invoke-direct {p0, p1}, Lexx;->j(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Ldwz;->b:I

    invoke-virtual {p0, v0}, Lexx;->i(I)I

    move-result v1

    goto :goto_1

    :pswitch_1
    const v1, 0x7f02055b

    goto :goto_1

    :pswitch_2
    const v1, 0x7f020511

    goto :goto_1

    .line 258
    :cond_2
    const/4 v0, 0x2

    if-ne v8, v0, :cond_3

    .line 259
    new-instance v0, Lhmk;

    sget-object v1, Lomy;->k:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p2, v0}, Ljgb;->a(Lhmk;)Lhmk;

    goto :goto_0

    .line 261
    :cond_3
    const/4 v0, 0x3

    if-ne v8, v0, :cond_0

    .line 262
    new-instance v0, Lhmk;

    sget-object v1, Lomy;->n:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p2, v0}, Ljgb;->a(Lhmk;)Lhmk;

    goto :goto_0

    .line 252
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public a(JLjgb;)V
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p0, p1, p2}, Lexx;->d(J)I

    move-result v0

    invoke-virtual {p0, v0, p3}, Lexx;->a(ILjgb;)V

    .line 292
    return-void
.end method

.method public a(Ljfz;J)V
    .locals 4

    .prologue
    .line 269
    invoke-virtual {p0, p2, p3}, Lexx;->d(J)I

    move-result v0

    .line 270
    invoke-direct {p0, v0}, Lexx;->k(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0}, Lexx;->f(I)I

    move-result v2

    .line 271
    invoke-virtual {p0, v0}, Lexx;->g(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0}, Lexx;->h(I)Landroid/content/Intent;

    move-result-object v0

    .line 270
    invoke-virtual {p1, v1, v2, v3, v0}, Ljfz;->a(Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;)V

    .line 272
    return-void
.end method

.method public a(J)Z
    .locals 1

    .prologue
    .line 279
    invoke-virtual {p0, p1, p2}, Lexx;->d(J)I

    move-result v0

    invoke-virtual {p0, v0}, Lexx;->c(I)Z

    move-result v0

    return v0
.end method

.method public as_()V
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lexx;->a:Landroid/content/Context;

    iget v1, p0, Lexx;->b:I

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Ldwy;->a(Landroid/content/Context;II)[Ldwz;

    move-result-object v0

    .line 44
    array-length v1, v0

    if-lez v1, :cond_0

    .line 45
    iput-object v0, p0, Lexx;->c:[Ldwz;

    .line 47
    :cond_0
    return-void
.end method

.method public at_()V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method public av_()Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x1

    return v0
.end method

.method public b(J)I
    .locals 1

    .prologue
    .line 283
    invoke-virtual {p0, p1, p2}, Lexx;->d(J)I

    move-result v0

    invoke-virtual {p0, v0}, Lexx;->d(I)I

    move-result v0

    return v0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    return v0
.end method

.method public c(J)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lexx;->d(J)I

    move-result v0

    invoke-virtual {p0, v0}, Lexx;->a(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 222
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v1

    .line 223
    if-nez v1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v0

    .line 226
    :cond_1
    if-ltz p1, :cond_0

    iget-object v2, p0, Lexx;->c:[Ldwz;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x3

    if-ge p1, v2, :cond_0

    .line 227
    invoke-direct {p0, v1}, Lexx;->m(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d(I)I
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 234
    const/4 v0, 0x6

    .line 236
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(J)I
    .locals 3

    .prologue
    .line 179
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 180
    const/4 v0, 0x0

    .line 190
    :goto_0
    return v0

    .line 181
    :cond_0
    const-wide/16 v0, 0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 182
    iget-object v0, p0, Lexx;->c:[Ldwz;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_1
    const-wide/16 v0, 0x2

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 184
    iget-object v0, p0, Lexx;->c:[Ldwz;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 185
    :cond_2
    const-wide/16 v0, 0x3

    cmp-long v0, p1, v0

    if-ltz v0, :cond_3

    iget-object v0, p0, Lexx;->c:[Ldwz;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_3

    .line 187
    long-to-int v0, p1

    add-int/lit8 v0, v0, -0x3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final e(I)J
    .locals 2

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v0

    .line 157
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 158
    int-to-long v0, v0

    .line 160
    :goto_0
    return-wide v0

    :cond_0
    add-int/lit8 v0, p1, 0x3

    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    goto :goto_0
.end method

.method public f(I)I
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 120
    const v0, 0x7f020336

    .line 122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lexx;->a:Landroid/content/Context;

    const v1, 0x7f0a0811

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 127
    iget-object v0, p0, Lexx;->a:Landroid/content/Context;

    const v1, 0x7f0a06fb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lexx;->l(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 134
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 135
    const-string v1, "people_view_type"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 138
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(I)I
    .locals 1

    .prologue
    .line 195
    packed-switch p1, :pswitch_data_0

    .line 201
    :pswitch_0
    const v0, 0x7f020556

    :goto_0
    return v0

    .line 197
    :pswitch_1
    const v0, 0x7f020557

    goto :goto_0

    .line 199
    :pswitch_2
    const v0, 0x7f020555

    goto :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method
