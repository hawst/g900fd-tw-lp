.class public final Lavo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lavu;
.implements Lavv;
.implements Lavw;
.implements Lavx;
.implements Lavy;
.implements Lawb;
.implements Lawc;
.implements Lawd;
.implements Lawf;
.implements Lawh;
.implements Lawi;
.implements Lawj;
.implements Lawl;
.implements Lawm;
.implements Lawn;
.implements Lawp;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:[Lavz;

.field private static final d:[Lbuf;

.field private static final e:[Landroid/net/Uri;


# instance fields
.field private final f:Lavq;

.field private final g:Lavt;

.field private final h:Lama;

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lavs;",
            "Ljava/util/List",
            "<",
            "Lawk;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    const-class v0, Lavo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lavo;->a:Ljava/lang/String;

    .line 98
    const-class v0, Lavo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lavo;->b:Ljava/lang/String;

    .line 100
    new-array v0, v1, [Lavz;

    sput-object v0, Lavo;->c:[Lavz;

    .line 101
    new-array v0, v1, [Lbuf;

    sput-object v0, Lavo;->d:[Lbuf;

    .line 102
    new-array v0, v1, [Landroid/net/Uri;

    sput-object v0, Lavo;->e:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 759
    invoke-static {}, Lavo;->bo()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lavo;->i:Ljava/util/Map;

    .line 771
    if-eqz p1, :cond_0

    sget-object v0, Lavo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 772
    sget-object v0, Lavo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lavq;

    iput-object v0, p0, Lavo;->f:Lavq;

    .line 773
    new-instance v0, Lavt;

    invoke-direct {v0}, Lavt;-><init>()V

    iput-object v0, p0, Lavo;->g:Lavt;

    .line 774
    invoke-direct {p0}, Lavo;->br()V

    .line 781
    :goto_0
    iget-object v1, p0, Lavo;->g:Lavt;

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v2, v0, Lavq;->u:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, Lavt;->m:Z

    .line 784
    new-instance v0, Lavp;

    invoke-direct {v0, p0}, Lavp;-><init>(Lavo;)V

    iput-object v0, p0, Lavo;->h:Lama;

    .line 790
    return-void

    .line 776
    :cond_0
    new-instance v0, Lavq;

    invoke-direct {v0}, Lavq;-><init>()V

    iput-object v0, p0, Lavo;->f:Lavq;

    .line 777
    new-instance v0, Lavt;

    invoke-direct {v0}, Lavt;-><init>()V

    iput-object v0, p0, Lavo;->g:Lavt;

    goto :goto_0

    .line 781
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lavo;)Lavq;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lavo;->f:Lavq;

    return-object v0
.end method

.method private a(Lavs;Lawk;)V
    .locals 3

    .prologue
    .line 2481
    iget-object v0, p0, Lavo;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const-string v1, "listener"

    const/4 v2, 0x0

    invoke-static {p2, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483
    return-void
.end method

.method private a(Lawg;)V
    .locals 3

    .prologue
    .line 1336
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->a:Lawg;

    .line 1337
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->p:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1338
    return-void
.end method

.method private a(Ljava/lang/Iterable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lavs;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2534
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavs;

    .line 2537
    sget-object v2, Lavs;->d:Lavs;

    if-ne v0, v2, :cond_1

    .line 2538
    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->l:Lbmk;

    const-string v3, "detailedSoundtrack"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2541
    :cond_1
    iget-object v2, p0, Lavo;->i:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawk;

    .line 2542
    invoke-interface {v0}, Lawk;->a()V

    goto :goto_0

    .line 2545
    :cond_2
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1610
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->h:Ljava/util/List;

    .line 1612
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1613
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1614
    iget-object v2, p0, Lavo;->f:Lavq;

    iget-object v2, v2, Lavq;->i:Ljava/util/List;

    invoke-static {v0}, Lavz;->a(Landroid/net/Uri;)Lavz;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1616
    :cond_0
    invoke-direct {p0}, Lavo;->br()V

    .line 1617
    iget-object v0, p0, Lavo;->g:Lavt;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lavt;->g:J

    .line 1618
    invoke-direct {p0}, Lavo;->bp()V

    .line 1619
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->i:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lavs;->k:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1621
    return-void
.end method

.method private varargs a([Lavs;)V
    .locals 1

    .prologue
    .line 2528
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lavo;->a(Ljava/lang/Iterable;)V

    .line 2529
    return-void
.end method

.method private b(Lavs;Lawk;)V
    .locals 2

    .prologue
    .line 2488
    iget-object v0, p0, Lavo;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 2490
    const-string v1, "Can\'t remove listener that wasn\'t added"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 2491
    return-void
.end method

.method static synthetic bm()[Landroid/net/Uri;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lavo;->e:[Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic bn()[Lavz;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lavo;->c:[Lavz;

    return-object v0
.end method

.method private static bo()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lavs;",
            "Ljava/util/List",
            "<",
            "Lawk;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 762
    new-instance v1, Ljava/util/EnumMap;

    const-class v0, Lavs;

    invoke-direct {v1, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 764
    invoke-static {}, Lavs;->values()[Lavs;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 765
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 764
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 767
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private bp()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2032
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->d:Z

    if-eqz v0, :cond_0

    .line 2033
    iget-object v0, p0, Lavo;->g:Lavt;

    .line 2034
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean v2, v0, Lavt;->d:Z

    .line 2035
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    sget-object v1, Lavs;->c:Lavs;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2037
    :cond_0
    return-void
.end method

.method private bq()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2271
    .line 2272
    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 2273
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    .line 2274
    iget-boolean v4, v0, Lavz;->b:Z

    if-nez v4, :cond_0

    .line 2275
    const/4 v1, 0x1

    .line 2276
    iget-object v4, p0, Lavo;->f:Lavq;

    iget-object v4, v4, Lavq;->i:Ljava/util/List;

    iget-object v0, v0, Lavz;->a:Landroid/net/Uri;

    .line 2277
    invoke-static {v0}, Lavz;->a(Landroid/net/Uri;)Lavz;

    move-result-object v0

    .line 2276
    invoke-interface {v4, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move v0, v1

    .line 2272
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2280
    :cond_1
    return v1
.end method

.method private br()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2468
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 2469
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2470
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2471
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2472
    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2473
    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2471
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2475
    :cond_0
    iget-object v0, p0, Lavo;->g:Lavt;

    iput v1, v0, Lavt;->c:I

    .line 2476
    return-void
.end method

.method private bs()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 2494
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v4, v0, Lavq;->u:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    .line 2495
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->k:Lboh;

    const-string v3, "soundtrack"

    invoke-static {v0, v3, v6}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2496
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->m:Z

    const-string v3, "soundtrackReadyForStoryboard"

    invoke-static {v0, v3}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2498
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->n:Z

    const-string v3, "soundtrackReadyForPreview"

    invoke-static {v0, v3}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2501
    :cond_0
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->s:Lbrc;

    if-nez v0, :cond_1

    .line 2502
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->k:Lboh;

    const-string v3, "musicLibrary must be set before soundtrack"

    invoke-static {v0, v3, v6}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2505
    :cond_1
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->k:Lboh;

    if-eqz v0, :cond_5

    .line 2506
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v4, v0, Lavq;->u:J

    const-string v0, "soundtrackId"

    iget-object v3, p0, Lavo;->g:Lavt;

    iget-object v3, v3, Lavt;->k:Lboh;

    .line 2507
    invoke-virtual {v3}, Lboh;->c()J

    move-result-wide v6

    .line 2506
    invoke-static {v4, v5, v0, v6, v7}, Lcgp;->a(JLjava/lang/CharSequence;J)V

    .line 2511
    :goto_0
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->l:Lbmk;

    if-eqz v0, :cond_6

    .line 2512
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->k:Lboh;

    const-string v3, "soundtrack"

    iget-object v4, p0, Lavo;->g:Lavt;

    iget-object v4, v4, Lavt;->l:Lbmk;

    .line 2513
    invoke-virtual {v4}, Lbmk;->b()Lboh;

    move-result-object v4

    .line 2512
    invoke-static {v0, v3, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 2518
    :cond_2
    :goto_1
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->l:Lbmk;

    if-nez v0, :cond_3

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v4, v0, Lavq;->u:J

    cmp-long v0, v4, v8

    if-eqz v0, :cond_3

    .line 2520
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->d:Z

    if-nez v0, :cond_8

    :goto_2
    const-string v0, "!storyboardReady"

    invoke-static {v1, v0}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2522
    :cond_3
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->m:Z

    if-eqz v0, :cond_4

    .line 2523
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->n:Z

    const-string v1, "soundtrack ready for preview"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2525
    :cond_4
    return-void

    .line 2509
    :cond_5
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->l:Lbmk;

    const-string v3, "detailedSoundtrack"

    invoke-static {v0, v3, v6}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2514
    :cond_6
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v4, v0, Lavq;->u:J

    cmp-long v0, v4, v8

    if-eqz v0, :cond_2

    .line 2516
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->m:Z

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    const-string v3, "!soundtrackReady"

    invoke-static {v0, v3}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v1, v2

    .line 2520
    goto :goto_2
.end method

.method private static c(Landroid/content/Intent;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1461
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "android.intent.extra.STREAM"

    .line 1462
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1463
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1464
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/net/Uri;

    aput-object v0, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1485
    :cond_0
    :goto_0
    return-object v0

    .line 1465
    :cond_1
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "android.intent.extra.STREAM"

    .line 1466
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1467
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1468
    instance-of v2, v0, Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 1469
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 1471
    :cond_2
    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1472
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1473
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1474
    array-length v3, v2

    .line 1475
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1476
    :goto_1
    if-ge v1, v3, :cond_0

    .line 1477
    aget-object v4, v2, v1

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1476
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1485
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Lbue;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1031
    iget v0, p1, Lbue;->a:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 1033
    iget-object v3, p1, Lbue;->c:[Lbuf;

    .line 1037
    array-length v0, v3

    iget-object v2, p0, Lavo;->f:Lavq;

    iget-object v2, v2, Lavq;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 1068
    :cond_0
    :goto_0
    return v1

    .line 1040
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v3

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 1041
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_4

    .line 1042
    aget-object v4, v3, v0

    iget-object v4, v4, Lbuf;->b:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1041
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1045
    :cond_2
    iget-object v0, p1, Lbue;->e:[Lbuf;

    array-length v0, v0

    iget-object v2, p1, Lbue;->d:[Lbuf;

    array-length v2, v2

    add-int/2addr v0, v2

    .line 1050
    iget-object v2, p0, Lavo;->f:Lavq;

    iget-object v2, v2, Lavq;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 1053
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 1054
    :goto_2
    iget-object v3, p1, Lbue;->d:[Lbuf;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 1055
    iget-object v3, p1, Lbue;->d:[Lbuf;

    aget-object v3, v3, v0

    iget-object v3, v3, Lbuf;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1054
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    .line 1057
    :goto_3
    iget-object v3, p1, Lbue;->e:[Lbuf;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 1058
    iget-object v3, p1, Lbue;->e:[Lbuf;

    aget-object v3, v3, v0

    iget-object v3, v3, Lbuf;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1057
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1061
    :cond_4
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1063
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1064
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    :goto_4
    if-ge v1, v4, :cond_5

    .line 1065
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    iget-object v0, v0, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1064
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1067
    :cond_5
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1068
    invoke-interface {v3, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/16 :goto_0
.end method

.method private e(Landroid/net/Uri;)I
    .locals 3

    .prologue
    .line 1508
    const-string v0, "uri"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 1509
    const/4 v1, 0x0

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1510
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    iget-object v0, v0, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1514
    :goto_1
    return v0

    .line 1509
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1514
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private h(I)V
    .locals 4

    .prologue
    .line 2451
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    const-string v1, "index"

    iget-object v2, p0, Lavo;->f:Lavq;

    iget-object v2, v2, Lavq;->i:Ljava/util/List;

    .line 2452
    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    .line 2451
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2453
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->a:Ljava/util/List;

    const-string v1, "index"

    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->a:Ljava/util/List;

    .line 2454
    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    .line 2453
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2455
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->b:Ljava/util/List;

    const-string v1, "index"

    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->b:Ljava/util/List;

    .line 2456
    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    .line 2455
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkr;

    .line 2457
    if-nez v0, :cond_0

    .line 2458
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v1, v0, Lavt;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lavt;->c:I

    .line 2460
    :cond_0
    iget-object v0, p0, Lavo;->g:Lavt;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lavt;->g:J

    .line 2461
    return-void
.end method

.method private v(Z)V
    .locals 1

    .prologue
    .line 2308
    iget-object v0, p0, Lavo;->f:Lavq;

    invoke-virtual {v0, p1}, Lavq;->a(Z)V

    .line 2309
    iget-object v0, p0, Lavo;->g:Lavt;

    invoke-virtual {v0}, Lavt;->a()V

    .line 2310
    invoke-direct {p0}, Lavo;->br()V

    .line 2311
    invoke-direct {p0}, Lavo;->bs()V

    .line 2314
    invoke-static {}, Lavs;->values()[Lavs;

    move-result-object v0

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2315
    return-void
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1952
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->v:Ljava/lang/String;

    return-object v0
.end method

.method public A(Lawk;)V
    .locals 1

    .prologue
    .line 1202
    sget-object v0, Lavs;->l:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1203
    return-void
.end method

.method public B(Lawk;)V
    .locals 1

    .prologue
    .line 1207
    sget-object v0, Lavs;->m:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1208
    return-void
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 1977
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->w:Z

    return v0
.end method

.method public C()Lama;
    .locals 1

    .prologue
    .line 1982
    iget-object v0, p0, Lavo;->h:Lama;

    return-object v0
.end method

.method public C(Lawk;)V
    .locals 1

    .prologue
    .line 1212
    sget-object v0, Lavs;->m:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1213
    return-void
.end method

.method public D(Lawk;)V
    .locals 1

    .prologue
    .line 1217
    sget-object v0, Lavs;->o:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1218
    return-void
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 1991
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->s:Z

    return v0
.end method

.method public E(Lawk;)V
    .locals 1

    .prologue
    .line 1222
    sget-object v0, Lavs;->o:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1223
    return-void
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 2001
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->e:Lboi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Lboi;
    .locals 1

    .prologue
    .line 2006
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->e:Lboi;

    return-object v0
.end method

.method public F(Lawk;)V
    .locals 1

    .prologue
    .line 2857
    sget-object v0, Lavs;->n:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 2858
    return-void
.end method

.method public G(Lawk;)V
    .locals 1

    .prologue
    .line 2862
    sget-object v0, Lavs;->n:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 2863
    return-void
.end method

.method public G()Z
    .locals 1

    .prologue
    .line 2011
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->d:Z

    return v0
.end method

.method public H(Lawk;)V
    .locals 1

    .prologue
    .line 2946
    sget-object v0, Lavs;->q:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 2947
    return-void
.end method

.method public H()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2016
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->e:Lboi;

    if-nez v0, :cond_1

    .line 2028
    :cond_0
    :goto_0
    return v1

    .line 2019
    :cond_1
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v2, v0, Lavt;->e:Lboi;

    move v0, v1

    .line 2020
    :goto_1
    invoke-virtual {v2}, Lboi;->e()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2021
    invoke-virtual {v2, v0}, Lboi;->b(I)Lbmd;

    move-result-object v3

    .line 2022
    iget-object v4, v3, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->a:Lbmg;

    if-ne v4, v5, :cond_2

    iget-object v3, v3, Lbmd;->e:Ljeg;

    .line 2023
    invoke-virtual {v2, v3}, Lboi;->a(Ljeg;)Lbon;

    move-result-object v3

    .line 2024
    invoke-virtual {v3}, Lbon;->g()Lboo;

    move-result-object v3

    iget-boolean v3, v3, Lboo;->g:Z

    if-eqz v3, :cond_2

    .line 2025
    const/4 v1, 0x1

    goto :goto_0

    .line 2020
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public I()Lbyx;
    .locals 6

    .prologue
    .line 2062
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->f:Lbyx;

    if-nez v0, :cond_0

    .line 2063
    iget-object v0, p0, Lavo;->g:Lavt;

    new-instance v1, Lbyx;

    new-instance v2, Lbyt;

    new-instance v3, Lbyk;

    invoke-direct {v3}, Lbyk;-><init>()V

    sget-object v4, Lbyu;->a:Lbyu;

    invoke-direct {v2, v3, v4}, Lbyt;-><init>(Lbyh;Lbyu;)V

    .line 2067
    invoke-virtual {p0}, Lavo;->C()Lama;

    move-result-object v3

    new-instance v4, Lbzu;

    iget-object v5, p0, Lavo;->h:Lama;

    invoke-direct {v4, v5}, Lbzu;-><init>(Lama;)V

    invoke-direct {v1, v2, v3, v4}, Lbyx;-><init>(Lbyt;Lama;Lbzx;)V

    iput-object v1, v0, Lavt;->f:Lbyx;

    .line 2070
    :cond_0
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->f:Lbyx;

    return-object v0
.end method

.method public I(Lawk;)V
    .locals 1

    .prologue
    .line 2951
    sget-object v0, Lavs;->q:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 2952
    return-void
.end method

.method public J()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2082
    iget-object v0, p0, Lavo;->f:Lavq;

    invoke-static {v0}, Lavq;->b(Lavq;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public J(Lawk;)V
    .locals 1

    .prologue
    .line 2956
    sget-object v0, Lavs;->p:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 2957
    return-void
.end method

.method public K(Lawk;)V
    .locals 1

    .prologue
    .line 2961
    sget-object v0, Lavs;->p:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 2962
    return-void
.end method

.method public K()Z
    .locals 1

    .prologue
    .line 2087
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->h:Z

    return v0
.end method

.method public L()J
    .locals 2

    .prologue
    .line 2097
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v0, v0, Lavq;->S:J

    return-wide v0
.end method

.method public L(Lawk;)V
    .locals 1

    .prologue
    .line 2966
    sget-object v0, Lavs;->s:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 2967
    return-void
.end method

.method public M()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 2107
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->j:Landroid/graphics/RectF;

    return-object v0
.end method

.method public M(Lawk;)V
    .locals 1

    .prologue
    .line 2971
    sget-object v0, Lavs;->s:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 2972
    return-void
.end method

.method public N()Z
    .locals 1

    .prologue
    .line 2117
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->i:Z

    return v0
.end method

.method public O()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2127
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->y:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public P()Z
    .locals 1

    .prologue
    .line 2155
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Q()I
    .locals 2

    .prologue
    .line 2160
    invoke-virtual {p0}, Lavo;->P()Z

    move-result v0

    const-string v1, "canUndo"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2161
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v0, v0, Lavt;->z:I

    return v0
.end method

.method public R()V
    .locals 2

    .prologue
    .line 2166
    invoke-virtual {p0}, Lavo;->P()Z

    move-result v0

    const-string v1, "canUndo"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2167
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->y:Ljava/util/List;

    invoke-virtual {p0, v0}, Lavo;->b(Ljava/util/List;)V

    .line 2168
    invoke-virtual {p0}, Lavo;->S()V

    .line 2169
    return-void
.end method

.method public S()V
    .locals 2

    .prologue
    .line 2173
    invoke-virtual {p0}, Lavo;->P()Z

    move-result v0

    const-string v1, "canUndo"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2174
    iget-object v0, p0, Lavo;->g:Lavt;

    const/4 v1, 0x0

    iput-object v1, v0, Lavt;->y:Ljava/util/List;

    .line 2175
    iget-object v0, p0, Lavo;->g:Lavt;

    const/4 v1, -0x1

    iput v1, v0, Lavt;->z:I

    .line 2176
    return-void
.end method

.method public T()V
    .locals 4

    .prologue
    .line 2188
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    .line 2189
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lavo;->f:Lavq;

    iget v2, v2, Lavq;->z:I

    if-le v1, v2, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2190
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 2192
    :cond_0
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0}, Lbor;->e()J

    move-result-wide v0

    .line 2193
    iget-object v2, p0, Lavo;->f:Lavq;

    iget-object v2, v2, Lavq;->x:Lbor;

    invoke-virtual {v2}, Lbor;->f()Z

    move-result v2

    .line 2194
    iget-object v3, p0, Lavo;->f:Lavq;

    iget-object v3, v3, Lavq;->x:Lbor;

    invoke-virtual {v3}, Lbor;->j()V

    .line 2195
    iget-object v3, p0, Lavo;->f:Lavq;

    iget-object v3, v3, Lavq;->x:Lbor;

    invoke-virtual {v3, v0, v1}, Lbor;->a(J)V

    .line 2196
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0, v2}, Lbor;->a(Z)V

    .line 2200
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    invoke-virtual {p0, v0}, Lavo;->a(Ljava/util/Collection;)V

    .line 2201
    invoke-direct {p0}, Lavo;->bp()V

    .line 2202
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->a:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lavs;->b:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2205
    return-void
.end method

.method public U()V
    .locals 2

    .prologue
    .line 2209
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->y:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Lavq;->z:I

    .line 2210
    return-void
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 2214
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    .line 2215
    invoke-virtual {v0}, Lbor;->d()Lbox;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    .line 2216
    invoke-virtual {v0}, Lbor;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    .line 2217
    invoke-virtual {v0}, Lbor;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public W()J
    .locals 2

    .prologue
    .line 2229
    invoke-virtual {p0}, Lavo;->F()Lboi;

    move-result-object v0

    .line 2230
    if-eqz v0, :cond_0

    .line 2231
    invoke-virtual {v0}, Lboi;->n()J

    move-result-wide v0

    .line 2233
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0}, Lbor;->e()J

    move-result-wide v0

    goto :goto_0
.end method

.method public X()J
    .locals 2

    .prologue
    .line 2238
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0}, Lbor;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public Y()J
    .locals 2

    .prologue
    .line 2285
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-wide v0, v0, Lavt;->g:J

    return-wide v0
.end method

.method public Z()V
    .locals 1

    .prologue
    .line 2298
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lavo;->v(Z)V

    .line 2299
    return-void
.end method

.method public a(I)Lavz;
    .locals 3

    .prologue
    .line 1495
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    const-string v1, "index"

    iget-object v2, p0, Lavo;->f:Lavq;

    iget-object v2, v2, Lavq;->i:Ljava/util/List;

    .line 1496
    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    .line 1495
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    return-object v0
.end method

.method public a()Lbue;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 794
    new-instance v3, Lbue;

    invoke-direct {v3}, Lbue;-><init>()V

    .line 795
    const/4 v0, 0x2

    iput v0, v3, Lbue;->a:I

    .line 797
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->A:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iput v0, v3, Lbue;->b:I

    .line 799
    new-instance v4, Ljava/util/HashSet;

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 800
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 801
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 802
    :goto_0
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 803
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    .line 804
    iget-object v7, v0, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v4, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 805
    invoke-virtual {v0}, Lavz;->a()Lbuf;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 802
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 807
    :cond_0
    invoke-virtual {v0}, Lavz;->a()Lbuf;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 810
    :cond_1
    sget-object v0, Lavo;->d:[Lbuf;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbuf;

    iput-object v0, v3, Lbue;->d:[Lbuf;

    .line 811
    sget-object v0, Lavo;->d:[Lbuf;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbuf;

    iput-object v0, v3, Lbue;->e:[Lbuf;

    .line 813
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->t:Lbza;

    iget v0, v0, Lbza;->m:I

    iput v0, v3, Lbue;->f:I

    .line 814
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v0, v0, Lavq;->u:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 815
    new-instance v0, Lbun;

    invoke-direct {v0}, Lbun;-><init>()V

    iput-object v0, v3, Lbue;->g:Lbun;

    .line 816
    iget-object v0, v3, Lbue;->g:Lbun;

    iget-object v1, p0, Lavo;->f:Lavq;

    iget-wide v4, v1, Lavq;->u:J

    iput-wide v4, v0, Lbun;->a:J

    .line 818
    :cond_2
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->v:Ljava/lang/String;

    iput-object v0, v3, Lbue;->h:Ljava/lang/String;

    .line 820
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 821
    if-lez v4, :cond_5

    .line 824
    iget-object v0, p0, Lavo;->f:Lavq;

    invoke-static {v0}, Lavq;->a(Lavq;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 825
    new-array v6, v5, [Lbty;

    move v1, v2

    .line 826
    :goto_2
    if-ge v1, v5, :cond_3

    .line 827
    iget-object v0, p0, Lavo;->f:Lavq;

    .line 828
    invoke-static {v0}, Lavq;->a(Lavq;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 827
    invoke-static {v0}, Lbpm;->a(Lbmd;)Lbty;

    move-result-object v0

    aput-object v0, v6, v1

    .line 826
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 830
    :cond_3
    iput-object v6, v3, Lbue;->i:[Lbty;

    .line 832
    new-array v1, v4, [Lbut;

    .line 833
    :goto_3
    if-ge v2, v4, :cond_4

    .line 834
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbph;

    invoke-interface {v0}, Lbph;->a()Lbut;

    move-result-object v0

    aput-object v0, v1, v2

    .line 833
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 836
    :cond_4
    iput-object v1, v3, Lbue;->j:[Lbut;

    .line 839
    :cond_5
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    if-eqz v0, :cond_6

    .line 840
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0}, Lbor;->a()Lbua;

    move-result-object v0

    iput-object v0, v3, Lbue;->k:Lbua;

    .line 842
    :cond_6
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->L:Z

    iput-boolean v0, v3, Lbue;->l:Z

    .line 843
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->M:Z

    iput-boolean v0, v3, Lbue;->m:Z

    .line 844
    return-object v3
.end method

.method public a(ILbmu;)V
    .locals 2

    .prologue
    .line 1691
    const-string v0, "metadata"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 1692
    const-string v0, "index"

    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->i:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    .line 1693
    const-string v0, "index"

    iget-object v1, p0, Lavo;->g:Lavt;

    iget-object v1, v1, Lavt;->a:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    .line 1694
    iget-object v0, p2, Lbmu;->e:Lbmv;

    sget-object v1, Lbmv;->a:Lbmv;

    if-ne v0, v1, :cond_0

    .line 1695
    instance-of v0, p2, Lboo;

    const-string v1, "metadata instanceof VideoMetadata"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 1697
    :cond_0
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1698
    return-void
.end method

.method public a(J)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 1811
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->s:Lbrc;

    const-string v3, "musicLibrary"

    invoke-static {v0, v3, v4}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1813
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-wide p1, v0, Lavq;->u:J

    .line 1814
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-object v4, v0, Lavt;->k:Lboh;

    .line 1815
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-object v4, v0, Lavt;->l:Lbmk;

    .line 1816
    iget-object v3, p0, Lavo;->g:Lavt;

    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lavt;->m:Z

    .line 1817
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean v2, v0, Lavt;->o:Z

    .line 1818
    invoke-direct {p0}, Lavo;->bs()V

    .line 1819
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    sget-object v3, Lavs;->d:Lavs;

    aput-object v3, v0, v2

    sget-object v2, Lavs;->e:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1820
    return-void

    :cond_0
    move v0, v2

    .line 1816
    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 1348
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean v1, v0, Lavq;->g:Z

    .line 1349
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1351
    const-string v0, "source_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1352
    iget-object v0, p0, Lavo;->f:Lavq;

    const-string v4, "source_id"

    .line 1353
    invoke-virtual {p1, v4, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lavq;->c:I

    .line 1354
    iget-object v0, p0, Lavo;->f:Lavq;

    const-string v4, "source_name"

    .line 1355
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lavq;->d:Ljava/lang/String;

    .line 1360
    :goto_0
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1361
    iget-object v0, p0, Lavo;->f:Lavq;

    const-string v4, "account_id"

    const/4 v5, -0x1

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lavq;->e:I

    .line 1367
    :goto_1
    iget-object v0, p0, Lavo;->g:Lavt;

    const-string v4, "com.google.android.apps.moviemaker.app.ResumeSaving"

    .line 1368
    invoke-virtual {p1, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v0, Lavt;->A:Z

    .line 1370
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1372
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "cluster_id"

    .line 1373
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1374
    sget-object v0, Lawg;->b:Lawg;

    invoke-direct {p0, v0}, Lavo;->a(Lawg;)V

    .line 1375
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean v1, v0, Lavq;->b:Z

    .line 1376
    invoke-virtual {p0}, Lavo;->l()V

    .line 1378
    const-string v0, "cluster_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljed;

    invoke-virtual {p0, v0}, Lavo;->a(Ljed;)V

    .line 1379
    invoke-virtual {p0, v8}, Lavo;->e(Ljava/lang/String;)V

    .line 1380
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object v8, v0, Lavq;->P:Ljdz;

    .line 1381
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object v8, v0, Lavq;->Q:Landroid/net/Uri;

    .line 1382
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object v8, v0, Lavq;->R:Ljava/lang/String;

    .line 1384
    iget-object v0, p0, Lavo;->f:Lavq;

    const-string v2, "force_download_and_analysis"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lavq;->f:Z

    .line 1449
    :cond_0
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1450
    sget-object v0, Lavo;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Started with missing intent extras: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1452
    :cond_1
    return-void

    .line 1357
    :cond_2
    const-string v0, "source_id"

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1364
    :cond_3
    const-string v0, "account_id"

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1386
    :cond_4
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "aam_photo_id"

    .line 1387
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1388
    const-string v0, "aam_owner_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1389
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EXTRA_AAM_PHOTO_ID without EXTRA_AAM_OWNER_GAIA_ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1393
    :cond_5
    invoke-static {p1}, Lavo;->c(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 1394
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-eq v4, v1, :cond_6

    .line 1395
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 1396
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "One URI required in cloud movie intent. Got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1398
    :cond_6
    iget-object v4, p0, Lavo;->f:Lavq;

    .line 1399
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    const-string v5, "cloudAamThumbnailUrl"

    .line 1398
    invoke-static {v0, v5, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, v4, Lavq;->Q:Landroid/net/Uri;

    .line 1402
    iget-object v0, p0, Lavo;->f:Lavq;

    new-instance v4, Ljdz;

    const-string v5, "aam_photo_id"

    const-wide/16 v6, 0x0

    .line 1403
    invoke-virtual {p1, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string v6, "aam_owner_id"

    .line 1404
    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljdz;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    iput-object v4, v0, Lavq;->P:Ljdz;

    .line 1405
    iget-object v0, p0, Lavo;->f:Lavq;

    const-string v4, "aadm_media_key"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lavq;->R:Ljava/lang/String;

    .line 1407
    sget-object v0, Lawg;->c:Lawg;

    invoke-direct {p0, v0}, Lavo;->a(Lawg;)V

    .line 1408
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean v1, v0, Lavq;->b:Z

    .line 1409
    invoke-virtual {p0}, Lavo;->l()V

    .line 1411
    invoke-virtual {p0, v8}, Lavo;->a(Ljed;)V

    .line 1412
    invoke-virtual {p0, v8}, Lavo;->e(Ljava/lang/String;)V

    .line 1414
    sget-object v0, Lavo;->a:Ljava/lang/String;

    const-string v0, "Started moviemaker with photo ID %s."

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lavo;->f:Lavq;

    iget-object v4, v4, Lavq;->P:Ljdz;

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_2

    .line 1418
    :cond_7
    sget-object v0, Lawg;->b:Lawg;

    invoke-direct {p0, v0}, Lavo;->a(Lawg;)V

    .line 1422
    invoke-static {}, Lapm;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1423
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    .line 1424
    invoke-virtual {v0, p1}, Ljfb;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 1425
    if-nez v0, :cond_8

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 1430
    :cond_8
    :goto_3
    const-string v4, "session_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1431
    invoke-virtual {p0}, Lavo;->aP()Ljava/lang/String;

    move-result-object v5

    .line 1433
    invoke-virtual {p0, v8}, Lavo;->a(Ljed;)V

    .line 1434
    invoke-virtual {p0, v4}, Lavo;->e(Ljava/lang/String;)V

    .line 1435
    iget-object v6, p0, Lavo;->f:Lavq;

    iput-object v8, v6, Lavq;->P:Ljdz;

    .line 1436
    iget-object v6, p0, Lavo;->f:Lavq;

    iput-object v8, v6, Lavq;->Q:Landroid/net/Uri;

    .line 1437
    iget-object v6, p0, Lavo;->f:Lavq;

    iput-object v8, v6, Lavq;->R:Ljava/lang/String;

    .line 1440
    new-instance v6, Ljava/util/ArrayList;

    new-instance v7, Ljava/util/LinkedHashSet;

    invoke-direct {v7, v0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1442
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1443
    invoke-static {v5, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    .line 1444
    :goto_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-nez v0, :cond_0

    .line 1445
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean v1, v0, Lavq;->b:Z

    .line 1446
    invoke-direct {p0, v6}, Lavo;->a(Ljava/util/ArrayList;)V

    goto/16 :goto_2

    .line 1427
    :cond_9
    invoke-static {p1}, Lavo;->c(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    goto :goto_3

    :cond_a
    move v0, v2

    .line 1443
    goto :goto_4
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1783
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->x:Landroid/graphics/Bitmap;

    .line 1784
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-object p1, v0, Lavt;->x:Landroid/graphics/Bitmap;

    .line 1785
    iget-object v0, p0, Lavo;->g:Lavt;

    .line 1786
    return-void
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 2112
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->j:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 2113
    return-void
.end method

.method public a(Landroid/net/Uri;Lbkr;)V
    .locals 3

    .prologue
    .line 1723
    invoke-direct {p0, p1}, Lavo;->e(Landroid/net/Uri;)I

    move-result v0

    const-string v1, "index"

    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->a:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    .line 1726
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkr;

    .line 1728
    if-nez v0, :cond_0

    .line 1729
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v2, v0, Lavt;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lavt;->c:I

    .line 1731
    :cond_0
    if-nez p2, :cond_1

    .line 1732
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v2, v0, Lavt;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lavt;->c:I

    .line 1734
    :cond_1
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->b:Ljava/util/List;

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1735
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->i:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1736
    return-void
.end method

.method public a(Landroid/net/Uri;Lbmu;)V
    .locals 3

    .prologue
    .line 1702
    invoke-direct {p0, p1}, Lavo;->e(Landroid/net/Uri;)I

    move-result v1

    .line 1703
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "uri not found"

    invoke-static {v0, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 1704
    invoke-virtual {p0, v1, p2}, Lavo;->a(ILbmu;)V

    .line 1705
    return-void

    .line 1703
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1986
    sget-object v0, Lavo;->b:Ljava/lang/String;

    iget-object v1, p0, Lavo;->f:Lavq;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1987
    return-void
.end method

.method public a(Latu;)V
    .locals 2

    .prologue
    .line 2707
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2708
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->G:Latu;

    .line 2709
    return-void
.end method

.method public a(Lavz;Lbmu;Lbkr;)V
    .locals 4

    .prologue
    .line 1626
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1627
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1628
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->b:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1630
    if-nez p3, :cond_0

    .line 1631
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v1, v0, Lavt;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lavt;->c:I

    .line 1635
    :cond_0
    iget-object v0, p0, Lavo;->g:Lavt;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lavt;->g:J

    .line 1636
    invoke-direct {p0}, Lavo;->bp()V

    .line 1637
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->i:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lavs;->k:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1639
    return-void
.end method

.method public a(Lawk;)V
    .locals 1

    .prologue
    .line 1073
    sget-object v0, Lavs;->a:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1074
    return-void
.end method

.method public a(Lbmk;)V
    .locals 6

    .prologue
    .line 1865
    invoke-virtual {p1}, Lbmk;->b()Lboh;

    move-result-object v0

    invoke-virtual {v0}, Lboh;->c()J

    move-result-wide v0

    const-string v2, "track id"

    iget-object v3, p0, Lavo;->f:Lavq;

    iget-wide v3, v3, Lavq;->u:J

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcec;->a(JLjava/lang/CharSequence;JLjava/lang/CharSequence;)V

    .line 1867
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->l:Lbmk;

    if-eq v0, p1, :cond_0

    .line 1868
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-object p1, v0, Lavt;->l:Lbmk;

    .line 1869
    invoke-direct {p0}, Lavo;->bs()V

    .line 1870
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->e:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1872
    :cond_0
    return-void
.end method

.method public a(Lboh;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1824
    if-nez p1, :cond_2

    const-wide/16 v0, 0x0

    .line 1825
    :goto_0
    iget-object v4, p0, Lavo;->f:Lavq;

    iget-wide v4, v4, Lavq;->u:J

    cmp-long v4, v0, v4

    if-eqz v4, :cond_3

    move v4, v2

    .line 1828
    :goto_1
    if-eqz v4, :cond_0

    iget-object v5, p0, Lavo;->g:Lavt;

    iget-boolean v5, v5, Lavt;->o:Z

    if-nez v5, :cond_0

    .line 1829
    iget-object v5, p0, Lavo;->g:Lavt;

    iget-object v6, p0, Lavo;->g:Lavt;

    iget-object v6, v6, Lavt;->k:Lboh;

    iput-object v6, v5, Lavt;->r:Lboh;

    .line 1831
    :cond_0
    iget-object v5, p0, Lavo;->f:Lavq;

    iput-wide v0, v5, Lavq;->u:J

    .line 1832
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-object p1, v0, Lavt;->k:Lboh;

    .line 1833
    iget-object v0, p0, Lavo;->g:Lavt;

    const/4 v1, 0x0

    iput-object v1, v0, Lavt;->l:Lbmk;

    .line 1834
    iget-object v1, p0, Lavo;->g:Lavt;

    if-nez p1, :cond_4

    move v0, v2

    :goto_2
    iput-boolean v0, v1, Lavt;->m:Z

    .line 1835
    iget-object v1, p0, Lavo;->g:Lavt;

    if-nez p1, :cond_5

    move v0, v2

    :goto_3
    iput-boolean v0, v1, Lavt;->n:Z

    .line 1836
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean v3, v0, Lavt;->o:Z

    .line 1837
    invoke-direct {p0}, Lavo;->bp()V

    .line 1838
    invoke-direct {p0}, Lavo;->bs()V

    .line 1839
    if-eqz v4, :cond_1

    .line 1840
    new-array v0, v2, [Lavs;

    sget-object v1, Lavs;->d:Lavs;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1842
    :cond_1
    new-array v0, v2, [Lavs;

    sget-object v1, Lavs;->e:Lavs;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1843
    return-void

    .line 1824
    :cond_2
    invoke-virtual {p1}, Lboh;->c()J

    move-result-wide v0

    goto :goto_0

    :cond_3
    move v4, v3

    .line 1825
    goto :goto_1

    :cond_4
    move v0, v3

    .line 1834
    goto :goto_2

    :cond_5
    move v0, v3

    .line 1835
    goto :goto_3
.end method

.method public a(Lboi;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2041
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-object p1, v0, Lavt;->e:Lboi;

    .line 2042
    iget-object v0, p0, Lavo;->g:Lavt;

    .line 2043
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean v1, v0, Lavt;->d:Z

    .line 2044
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean v2, v0, Lavt;->C:Z

    .line 2045
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean v2, v0, Lavt;->B:Z

    .line 2048
    iget-object v3, p0, Lavo;->g:Lavt;

    invoke-virtual {p0}, Lavo;->r()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lavt;->m:Z

    .line 2049
    invoke-direct {p0}, Lavo;->bs()V

    .line 2051
    const/4 v0, 0x3

    new-array v0, v0, [Lavs;

    sget-object v3, Lavs;->c:Lavs;

    aput-object v3, v0, v2

    sget-object v2, Lavs;->e:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lavs;->s:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2055
    return-void

    :cond_0
    move v0, v2

    .line 2048
    goto :goto_0
.end method

.method public a(Lbph;)V
    .locals 3

    .prologue
    .line 2140
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2141
    invoke-direct {p0}, Lavo;->bp()V

    .line 2142
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->b:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2143
    return-void
.end method

.method public a(Lbrc;)V
    .locals 3

    .prologue
    .line 1905
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-object p1, v0, Lavt;->s:Lbrc;

    .line 1906
    invoke-direct {p0}, Lavo;->bs()V

    .line 1911
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->f:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1912
    return-void
.end method

.method public a(Lbue;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 851
    iget v0, p1, Lbue;->a:I

    if-ne v0, v3, :cond_1

    .line 856
    iget-object v0, p1, Lbue;->c:[Lbuf;

    move-object v5, v0

    move v6, v3

    .line 862
    :goto_0
    if-eqz v5, :cond_0

    array-length v0, v5

    if-nez v0, :cond_2

    .line 889
    :cond_0
    :goto_1
    return-void

    .line 859
    :cond_1
    iget-object v0, p1, Lbue;->e:[Lbuf;

    move-object v5, v0

    move v6, v2

    goto :goto_0

    .line 866
    :cond_2
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    move v1, v2

    .line 867
    :goto_2
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 868
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    iget-object v0, v0, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 867
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 872
    :cond_3
    array-length v8, v5

    move v4, v2

    move v0, v2

    :goto_3
    if-ge v4, v8, :cond_7

    aget-object v9, v5, v4

    .line 873
    if-eqz v6, :cond_4

    iget-boolean v1, v9, Lbuf;->c:Z

    if-nez v1, :cond_6

    :cond_4
    move v1, v3

    .line 876
    :goto_4
    if-eqz v1, :cond_5

    iget-object v1, v9, Lbuf;->b:Ljava/lang/String;

    invoke-interface {v7, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 877
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-static {v9}, Lavz;->a(Lbuf;)Lavz;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 878
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->a:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 879
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->b:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 880
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v1, v0, Lavt;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lavt;->c:I

    move v0, v3

    .line 872
    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_6
    move v1, v2

    .line 873
    goto :goto_4

    .line 885
    :cond_7
    if-eqz v0, :cond_0

    .line 886
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    sget-object v1, Lavs;->i:Lavs;

    aput-object v1, v0, v2

    sget-object v1, Lavs;->k:Lavs;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    goto :goto_1
.end method

.method public a(Lbza;)V
    .locals 3

    .prologue
    .line 1768
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->t:Lbza;

    if-eq p1, v0, :cond_0

    .line 1769
    iget-object v1, p0, Lavo;->f:Lavq;

    const-string v0, "selectedTheme"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbza;

    iput-object v0, v1, Lavq;->t:Lbza;

    .line 1770
    invoke-direct {p0}, Lavo;->bp()V

    .line 1771
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->j:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1773
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1957
    const-string v0, "movieTitle"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 1958
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->v:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1959
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962
    const-string p1, ""

    .line 1964
    :cond_0
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->v:Ljava/lang/String;

    .line 1965
    invoke-direct {p0}, Lavo;->bp()V

    .line 1966
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->g:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1968
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 2899
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p2, v0, Lavq;->m:[B

    .line 2900
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->n:Ljava/lang/String;

    .line 2901
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->t:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2902
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 1662
    .line 1663
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v2, v4

    :goto_0
    if-ltz v3, :cond_0

    .line 1664
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    iget-object v0, v0, Lavz;->a:Landroid/net/Uri;

    invoke-interface {p1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1665
    invoke-direct {p0, v3}, Lavo;->h(I)V

    move v0, v1

    .line 1663
    :goto_1
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 1669
    :cond_0
    if-eqz v2, :cond_1

    .line 1670
    invoke-direct {p0}, Lavo;->bp()V

    .line 1671
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    sget-object v2, Lavs;->i:Lavs;

    aput-object v2, v0, v4

    sget-object v2, Lavs;->k:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1674
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2075
    iget-object v1, p0, Lavo;->f:Lavq;

    const-string v0, "immutableClipsBeforeUserEdits"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lavq;->a(Lavq;Ljava/util/List;)Ljava/util/List;

    .line 2077
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1529
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1530
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1531
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1532
    if-eqz v0, :cond_0

    .line 1534
    invoke-virtual {v2, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1540
    :cond_1
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1541
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    .line 1542
    iget-object v1, v0, Lavz;->a:Landroid/net/Uri;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1543
    if-eqz v1, :cond_2

    .line 1545
    iget-boolean v5, v0, Lavz;->b:Z

    if-nez v5, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1549
    :cond_3
    invoke-virtual {v0, v1}, Lavz;->c(Landroid/net/Uri;)Lavz;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1554
    :cond_4
    invoke-virtual {p0}, Lavo;->bd()Lawg;

    move-result-object v0

    sget-object v1, Lawg;->c:Lawg;

    if-ne v0, v1, :cond_7

    .line 1555
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->j:Ljava/util/Map;

    .line 1556
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v4

    .line 1557
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1558
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljej;

    iget-object v1, v1, Ljej;->a:Landroid/net/Uri;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1559
    if-eqz v1, :cond_5

    .line 1561
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    new-instance v7, Ljej;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    iget-object v0, v0, Ljej;->b:Ljel;

    invoke-direct {v7, v1, v0}, Ljej;-><init>(Landroid/net/Uri;Ljel;)V

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1565
    :cond_6
    invoke-virtual {p0, v4}, Lavo;->b(Ljava/util/Map;)V

    .line 1568
    :cond_7
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1569
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1570
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1571
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1573
    invoke-direct {p0}, Lavo;->br()V

    .line 1574
    return-void
.end method

.method public a(Ljed;)V
    .locals 1

    .prologue
    .line 2764
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->O:Ljed;

    .line 2765
    return-void
.end method

.method public a(Ljfd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2879
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->l:Ljfd;

    .line 2880
    iget-object v0, p1, Ljfd;->b:Lood;

    .line 2881
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcge;->a(Loxu;)[B

    move-result-object v0

    .line 2882
    :goto_0
    iget-object v1, p1, Ljfd;->c:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lavo;->a(Ljava/lang/String;[B)V

    .line 2885
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean v2, v0, Lavq;->p:Z

    .line 2886
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    sget-object v1, Lavs;->q:Lavs;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2887
    return-void

    .line 2881
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lood;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2921
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-object p1, v0, Lavt;->p:Lood;

    .line 2922
    new-array v0, v3, [Lavs;

    sget-object v1, Lavs;->r:Lavs;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2923
    new-array v0, v3, [Lavs;

    sget-object v1, Lavs;->t:Lavs;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2924
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 1756
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->r:Z

    .line 1757
    return-void
.end method

.method public a([Lavz;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1578
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    array-length v4, p1

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, p1, v0

    iget-object v6, v5, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Duplicate input URI found: "

    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v5, v5, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1579
    :cond_2
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1580
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1581
    array-length v4, p1

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_4

    aget-object v5, p1, v3

    .line 1582
    if-eqz v5, :cond_3

    move v0, v1

    :goto_3
    const-string v6, "uris should not contain null values"

    invoke-static {v0, v6}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 1583
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    iget-object v6, v5, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1584
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1581
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 1582
    goto :goto_3

    .line 1586
    :cond_4
    invoke-direct {p0}, Lavo;->br()V

    .line 1588
    invoke-direct {p0}, Lavo;->bp()V

    .line 1589
    iget-object v0, p0, Lavo;->g:Lavt;

    const-wide/16 v4, -0x1

    iput-wide v4, v0, Lavt;->g:J

    .line 1590
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    sget-object v3, Lavs;->i:Lavs;

    aput-object v3, v0, v2

    sget-object v2, Lavs;->k:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1592
    return-void
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 1501
    invoke-direct {p0, p1}, Lavo;->e(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aA()Z
    .locals 3

    .prologue
    .line 2596
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->A:Z

    .line 2597
    iget-object v1, p0, Lavo;->g:Lavt;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lavt;->A:Z

    .line 2598
    return v0
.end method

.method public aB()Z
    .locals 2

    .prologue
    .line 2610
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2611
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->C:Z

    return v0
.end method

.method public aC()Z
    .locals 1

    .prologue
    .line 2622
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->D:Z

    return v0
.end method

.method public aD()Z
    .locals 2

    .prologue
    .line 2633
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2634
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->E:Z

    return v0
.end method

.method public aE()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2645
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2646
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->I:Ljava/lang/String;

    return-object v0
.end method

.method public aF()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2657
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2658
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->J:Ljava/lang/String;

    return-object v0
.end method

.method public aG()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2669
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2670
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->K:Ljava/lang/String;

    return-object v0
.end method

.method public aH()J
    .locals 2

    .prologue
    .line 2681
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2682
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v0, v0, Lavq;->H:J

    return-wide v0
.end method

.method public aI()Latu;
    .locals 3

    .prologue
    .line 2694
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2695
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->G:Latu;

    const-string v1, "mPersistentState.savingSize"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 2696
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->G:Latu;

    return-object v0
.end method

.method public aJ()Z
    .locals 2

    .prologue
    .line 2701
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2702
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->G:Latu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aK()V
    .locals 2

    .prologue
    .line 2713
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2714
    iget-object v0, p0, Lavo;->f:Lavq;

    const/4 v1, 0x0

    iput-object v1, v0, Lavq;->G:Latu;

    .line 2715
    return-void
.end method

.method public aL()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2720
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->q:Landroid/net/Uri;

    return-object v0
.end method

.method public aM()V
    .locals 2

    .prologue
    .line 2732
    iget-object v0, p0, Lavo;->f:Lavq;

    const/4 v1, 0x0

    iput-object v1, v0, Lavq;->q:Landroid/net/Uri;

    .line 2733
    return-void
.end method

.method public aN()Z
    .locals 1

    .prologue
    .line 2742
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->M:Z

    return v0
.end method

.method public aO()Ljed;
    .locals 1

    .prologue
    .line 2748
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->O:Ljed;

    return-object v0
.end method

.method public aP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2759
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->N:Ljava/lang/String;

    return-object v0
.end method

.method public aQ()Z
    .locals 1

    .prologue
    .line 2769
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->O:Ljed;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aR()Ljdz;
    .locals 1

    .prologue
    .line 2775
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->P:Ljdz;

    return-object v0
.end method

.method public aS()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2781
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->Q:Landroid/net/Uri;

    return-object v0
.end method

.method public aT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2787
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->R:Ljava/lang/String;

    return-object v0
.end method

.method public aU()Z
    .locals 1

    .prologue
    .line 2792
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->f:Z

    return v0
.end method

.method public aV()Z
    .locals 1

    .prologue
    .line 2797
    invoke-virtual {p0}, Lavo;->aQ()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aW()Z
    .locals 1

    .prologue
    .line 2804
    invoke-virtual {p0}, Lavo;->az()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lavo;->aC()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aX()Z
    .locals 1

    .prologue
    .line 2809
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->T:Z

    return v0
.end method

.method public aY()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2824
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->U:Landroid/content/Intent;

    return-object v0
.end method

.method public aZ()Z
    .locals 1

    .prologue
    .line 2829
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->F:Z

    return v0
.end method

.method public aa()V
    .locals 1

    .prologue
    .line 2303
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lavo;->v(Z)V

    .line 2304
    return-void
.end method

.method public ab()Z
    .locals 1

    .prologue
    .line 2319
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->L:Z

    return v0
.end method

.method public ac()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2333
    invoke-virtual {p0}, Lavo;->O()Ljava/util/List;

    move-result-object v0

    .line 2334
    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->x:Lbor;

    iget-object v2, p0, Lavo;->f:Lavq;

    .line 2337
    invoke-static {v2}, Lavq;->a(Lavq;)Ljava/util/List;

    move-result-object v2

    .line 2334
    invoke-static {v1, v0, v2}, Lbpi;->a(Lbor;Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    .line 2344
    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->y:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lavo;->f:Lavq;

    iput v3, v1, Lavq;->z:I

    invoke-direct {p0}, Lavo;->bp()V

    new-array v1, v4, [Lavs;

    sget-object v2, Lavs;->b:Lavs;

    aput-object v2, v1, v3

    invoke-direct {p0, v1}, Lavo;->a([Lavs;)V

    .line 2345
    if-eqz v0, :cond_0

    .line 2346
    invoke-direct {p0}, Lavo;->bp()V

    .line 2347
    new-array v1, v4, [Lavs;

    sget-object v2, Lavs;->a:Lavs;

    aput-object v2, v1, v3

    invoke-direct {p0, v1}, Lavo;->a([Lavs;)V

    .line 2349
    :cond_0
    iget-object v1, p0, Lavo;->f:Lavq;

    invoke-static {v1}, Lavq;->b(Lavq;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2350
    iget-object v1, p0, Lavo;->f:Lavq;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lavq;->b(Lavq;Ljava/util/List;)Ljava/util/List;

    .line 2351
    new-array v1, v4, [Lavs;

    sget-object v2, Lavs;->l:Lavs;

    aput-object v2, v1, v3

    invoke-direct {p0, v1}, Lavo;->a([Lavs;)V

    .line 2353
    :cond_1
    return v0
.end method

.method public ad()Z
    .locals 1

    .prologue
    .line 2358
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->m:Z

    return v0
.end method

.method public ae()Z
    .locals 1

    .prologue
    .line 2363
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->n:Z

    return v0
.end method

.method public af()Lavx;
    .locals 0

    .prologue
    .line 2367
    return-object p0
.end method

.method public ag()Lawc;
    .locals 0

    .prologue
    .line 2371
    return-object p0
.end method

.method public ah()Lawf;
    .locals 0

    .prologue
    .line 2376
    return-object p0
.end method

.method public ai()Lawm;
    .locals 0

    .prologue
    .line 2381
    return-object p0
.end method

.method public aj()Lawj;
    .locals 0

    .prologue
    .line 2386
    return-object p0
.end method

.method public ak()Lawn;
    .locals 0

    .prologue
    .line 2391
    return-object p0
.end method

.method public al()Lavy;
    .locals 0

    .prologue
    .line 2396
    return-object p0
.end method

.method public am()Lawp;
    .locals 0

    .prologue
    .line 2401
    return-object p0
.end method

.method public an()Lawl;
    .locals 0

    .prologue
    .line 2406
    return-object p0
.end method

.method public ao()Lavv;
    .locals 0

    .prologue
    .line 2411
    return-object p0
.end method

.method public ap()Lawi;
    .locals 0

    .prologue
    .line 2416
    return-object p0
.end method

.method public aq()Lawh;
    .locals 0

    .prologue
    .line 2421
    return-object p0
.end method

.method public ar()Lavw;
    .locals 0

    .prologue
    .line 2426
    return-object p0
.end method

.method public as()Lawb;
    .locals 0

    .prologue
    .line 2431
    return-object p0
.end method

.method public at()Lawd;
    .locals 0

    .prologue
    .line 2436
    return-object p0
.end method

.method public au()Lavu;
    .locals 0

    .prologue
    .line 2441
    return-object p0
.end method

.method public av()J
    .locals 2

    .prologue
    .line 2561
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-wide v0, v0, Lavt;->v:J

    return-wide v0
.end method

.method public aw()J
    .locals 2

    .prologue
    .line 2566
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-wide v0, v0, Lavt;->w:J

    return-wide v0
.end method

.method public ax()Z
    .locals 1

    .prologue
    .line 2576
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->u:Z

    return v0
.end method

.method public ay()I
    .locals 1

    .prologue
    .line 2586
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v0, v0, Lavt;->t:I

    return v0
.end method

.method public az()Z
    .locals 1

    .prologue
    .line 2591
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    return v0
.end method

.method public b(Landroid/net/Uri;)Lbmu;
    .locals 3

    .prologue
    .line 1684
    invoke-direct {p0, p1}, Lavo;->e(Landroid/net/Uri;)I

    move-result v1

    .line 1685
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "uri not found"

    invoke-static {v0, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 1686
    invoke-virtual {p0, v1}, Lavo;->c(I)Lbmu;

    move-result-object v0

    return-object v0

    .line 1685
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1272
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v0, v0, Lavt;->c:I

    if-lez v0, :cond_0

    .line 1273
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 1281
    :goto_0
    return-object v0

    .line 1275
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1276
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1277
    invoke-virtual {p0, v1}, Lavo;->c(I)Lbmu;

    move-result-object v0

    iget-object v0, v0, Lbmu;->e:Lbmv;

    sget-object v3, Lbmv;->a:Lbmv;

    if-ne v0, v3, :cond_1

    .line 1278
    invoke-virtual {p0, v1}, Lavo;->c(I)Lbmu;

    move-result-object v3

    iget-object v0, v3, Lbmu;->e:Lbmv;

    const-string v4, "metadata.type"

    sget-object v5, Lbmv;->a:Lbmv;

    invoke-static {v0, v4, v5}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Lavo;->a(I)Lavz;

    move-result-object v4

    invoke-virtual {p0}, Lavo;->bd()Lawg;

    move-result-object v0

    sget-object v5, Lawg;->c:Lawg;

    if-ne v0, v5, :cond_2

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->k:Ljava/util/Map;

    iget-object v5, v4, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdx;

    iget-object v5, v4, Lavz;->a:Landroid/net/Uri;

    sget-object v6, Ljei;->b:Ljei;

    invoke-static {v0, v5, v6}, Ljeg;->a(Ljdx;Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v0

    :goto_2
    new-instance v5, Lbon;

    invoke-virtual {v3}, Lbmu;->a()Lboo;

    move-result-object v3

    invoke-virtual {p0, v1}, Lavo;->d(I)Lbkr;

    move-result-object v6

    iget-boolean v4, v4, Lavz;->b:Z

    invoke-direct {v5, v0, v3, v6, v4}, Lbon;-><init>(Ljeg;Lboo;Lbkr;Z)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1276
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1278
    :cond_2
    iget-object v0, v4, Lavz;->a:Landroid/net/Uri;

    sget-object v5, Ljei;->b:Ljei;

    invoke-static {v0, v5}, Ljeg;->a(Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 1281
    goto :goto_0
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 1643
    invoke-direct {p0, p1}, Lavo;->h(I)V

    .line 1644
    invoke-direct {p0}, Lavo;->bp()V

    .line 1645
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->i:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lavs;->k:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1647
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 2102
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-wide p1, v0, Lavq;->S:J

    .line 2103
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2819
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->U:Landroid/content/Intent;

    .line 2820
    return-void
.end method

.method public b(Lawk;)V
    .locals 1

    .prologue
    .line 1078
    sget-object v0, Lavs;->a:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1079
    return-void
.end method

.method public b(Lbue;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 893
    invoke-direct {p0}, Lavo;->bp()V

    .line 895
    const-class v0, Lavs;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v4

    .line 898
    iget-object v0, p0, Lavo;->f:Lavq;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lavq;->a(Lavq;Ljava/util/List;)Ljava/util/List;

    .line 900
    iget-object v0, p0, Lavo;->f:Lavq;

    invoke-static {v0}, Lavq;->b(Lavq;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 901
    iget-object v0, p0, Lavo;->f:Lavq;

    invoke-static {v0, v1}, Lavq;->b(Lavq;Ljava/util/List;)Ljava/util/List;

    .line 902
    sget-object v0, Lavs;->l:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 905
    :cond_0
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 906
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 907
    iget-object v0, p0, Lavo;->f:Lavq;

    iput v2, v0, Lavq;->z:I

    .line 908
    sget-object v0, Lavs;->b:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 911
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    iget-object v0, p1, Lbue;->j:[Lbut;

    array-length v0, v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 912
    iget-object v6, p1, Lbue;->j:[Lbut;

    array-length v7, v6

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_2

    aget-object v0, v6, v3

    .line 913
    iget v8, v0, Lbut;->b:I

    packed-switch v8, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    iget v0, v0, Lbut;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid user edit type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v8, v0, Lbut;->c:Lbup;

    new-instance v0, Lboz;

    iget-object v8, v8, Lbup;->a:Lbty;

    invoke-static {v8}, Lbpm;->a(Lbty;)Lbmd;

    move-result-object v8

    invoke-direct {v0, v8}, Lboz;-><init>(Lbmd;)V

    :goto_1
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 912
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 913
    :pswitch_1
    iget-object v8, v0, Lbut;->d:Lbuq;

    new-instance v0, Lbpb;

    iget v9, v8, Lbuq;->a:I

    iget-object v8, v8, Lbuq;->b:Lbty;

    invoke-static {v8}, Lbpm;->a(Lbty;)Lbmd;

    move-result-object v8

    invoke-direct {v0, v9, v8}, Lbpb;-><init>(ILbmd;)V

    goto :goto_1

    :pswitch_2
    iget-object v8, v0, Lbut;->e:Lbur;

    new-instance v0, Lbpd;

    iget v9, v8, Lbur;->a:I

    iget-object v8, v8, Lbur;->b:Lbty;

    invoke-static {v8}, Lbpm;->a(Lbty;)Lbmd;

    move-result-object v8

    invoke-direct {v0, v9, v8}, Lbpd;-><init>(ILbmd;)V

    goto :goto_1

    :pswitch_3
    iget-object v8, v0, Lbut;->f:Lbus;

    new-instance v0, Lbpf;

    iget v9, v8, Lbus;->a:I

    iget v8, v8, Lbus;->b:I

    invoke-direct {v0, v9, v8}, Lbpf;-><init>(II)V

    goto :goto_1

    :pswitch_4
    iget-object v0, v0, Lbut;->g:Lbuu;

    invoke-static {v0}, Lbpi;->a(Lbuu;)Lbpk;

    move-result-object v0

    goto :goto_1

    .line 916
    :cond_2
    iget-object v0, p1, Lbue;->k:Lbua;

    if-eqz v0, :cond_3

    new-instance v0, Lbor;

    iget-object v1, p1, Lbue;->k:Lbua;

    invoke-direct {v0, v1}, Lbor;-><init>(Lbua;)V

    move-object v1, v0

    .line 920
    :cond_3
    invoke-direct {p0, p1}, Lavo;->c(Lbue;)Z

    move-result v0

    .line 921
    if-eqz v0, :cond_a

    .line 922
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->A:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v2, p1, Lbue;->b:I

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 923
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 925
    iget-object v0, p1, Lbue;->i:[Lbty;

    invoke-static {v0}, Lbpm;->a([Lbty;)Ljava/util/List;

    move-result-object v0

    .line 927
    iget-object v2, p0, Lavo;->f:Lavq;

    invoke-static {v2, v0}, Lavq;->b(Lavq;Ljava/util/List;)Ljava/util/List;

    .line 928
    sget-object v0, Lavs;->l:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 930
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 931
    sget-object v0, Lavs;->b:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 933
    :cond_4
    if-eqz v1, :cond_5

    .line 934
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0, v1}, Lbor;->a(Lbor;)Z

    .line 935
    sget-object v0, Lavs;->a:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 980
    :cond_5
    :goto_2
    iget v0, p1, Lbue;->f:I

    invoke-static {v0}, Lbza;->b(I)Lbza;

    move-result-object v0

    .line 982
    if-eqz v0, :cond_6

    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->t:Lbza;

    if-eq v0, v1, :cond_6

    .line 983
    iget-object v1, p0, Lavo;->f:Lavq;

    iput-object v0, v1, Lavq;->t:Lbza;

    .line 984
    sget-object v0, Lavs;->j:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 987
    :cond_6
    iget-object v0, p1, Lbue;->g:Lbun;

    if-eqz v0, :cond_7

    .line 991
    iget-object v0, p1, Lbue;->g:Lbun;

    iget-wide v0, v0, Lbun;->a:J

    .line 993
    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->s:Lbrc;

    if-eqz v2, :cond_14

    .line 994
    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->s:Lbrc;

    invoke-virtual {v2, v0, v1}, Lbrc;->a(J)Lboh;

    move-result-object v0

    .line 996
    invoke-virtual {p0, v0}, Lavo;->a(Lboh;)V

    .line 1002
    :cond_7
    :goto_3
    iget-object v0, p1, Lbue;->h:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1003
    iget-object v0, p1, Lbue;->h:Ljava/lang/String;

    .line 1004
    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1005
    iget-object v1, p0, Lavo;->f:Lavq;

    iput-object v0, v1, Lavq;->v:Ljava/lang/String;

    .line 1006
    invoke-direct {p0}, Lavo;->bp()V

    .line 1007
    sget-object v0, Lavs;->g:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1011
    :cond_8
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->L:Z

    iget-boolean v1, p1, Lbue;->l:Z

    if-eq v0, v1, :cond_9

    .line 1012
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v1, p1, Lbue;->l:Z

    iput-boolean v1, v0, Lavq;->L:Z

    .line 1013
    sget-object v0, Lavs;->h:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1015
    :cond_9
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v1, p1, Lbue;->m:Z

    iput-boolean v1, v0, Lavq;->M:Z

    .line 1019
    iget-object v0, p0, Lavo;->f:Lavq;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lavq;->w:Z

    .line 1021
    invoke-direct {p0, v4}, Lavo;->a(Ljava/lang/Iterable;)V

    .line 1022
    return-void

    .line 946
    :cond_a
    iget-object v3, p1, Lbue;->c:[Lbuf;

    array-length v6, v3

    move v0, v2

    :goto_4
    if-ge v0, v6, :cond_c

    aget-object v7, v3, v0

    .line 947
    sget-object v8, Lavo;->a:Ljava/lang/String;

    const-string v8, "Stored input URI (V1): "

    iget-object v7, v7, Lbuf;->b:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_b

    invoke-virtual {v8, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 946
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 947
    :cond_b
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 949
    :cond_c
    iget-object v3, p1, Lbue;->d:[Lbuf;

    array-length v6, v3

    move v0, v2

    :goto_6
    if-ge v0, v6, :cond_e

    aget-object v7, v3, v0

    .line 950
    sget-object v8, Lavo;->a:Ljava/lang/String;

    const-string v8, "Stored original input URI: "

    iget-object v7, v7, Lbuf;->b:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_d

    invoke-virtual {v8, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 949
    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 950
    :cond_d
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_7

    .line 952
    :cond_e
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    .line 953
    sget-object v6, Lavo;->a:Ljava/lang/String;

    iget-object v0, v0, Lavz;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xf

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "New input URI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 956
    :cond_f
    iget-object v0, p1, Lbue;->i:[Lbty;

    invoke-static {v0}, Lbpm;->a([Lbty;)Ljava/util/List;

    move-result-object v3

    .line 959
    invoke-direct {p0}, Lavo;->bq()Z

    move-result v0

    .line 960
    if-eqz v0, :cond_10

    .line 961
    sget-object v0, Lavs;->i:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 963
    :cond_10
    if-nez v1, :cond_12

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    :goto_9
    invoke-static {v0, v5, v3}, Lbpi;->a(Lbor;Ljava/util/List;Ljava/util/List;)Z

    .line 968
    if-eqz v1, :cond_11

    .line 969
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0, v1}, Lbor;->a(Lbor;)Z

    .line 972
    :cond_11
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 973
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v1, v2

    :goto_a
    if-ge v1, v5, :cond_13

    .line 974
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavz;

    iget-object v0, v0, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 973
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    :cond_12
    move-object v0, v1

    .line 963
    goto :goto_9

    .line 976
    :cond_13
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0, v3}, Lbor;->a(Ljava/util/Collection;)V

    .line 977
    sget-object v0, Lavs;->a:Lavs;

    invoke-virtual {v4, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 998
    :cond_14
    sget-object v0, Lavo;->a:Ljava/lang/String;

    goto/16 :goto_3

    .line 913
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2651
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2652
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->I:Ljava/lang/String;

    .line 2653
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2132
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2133
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2134
    invoke-direct {p0}, Lavo;->bp()V

    .line 2135
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->b:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2136
    return-void
.end method

.method public b(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2906
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2907
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2908
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2909
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2910
    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v3, v1, Lavq;->k:Ljava/util/Map;

    .line 2911
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljej;

    iget-object v1, v1, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljdx;

    .line 2912
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " is referred to by two cloud media IDs"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcec;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    goto :goto_0

    .line 2915
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->q:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2916
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 1860
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->q:Z

    .line 1861
    return-void
.end method

.method public ba()I
    .locals 1

    .prologue
    .line 2842
    iget-object v0, p0, Lavo;->f:Lavq;

    iget v0, v0, Lavq;->e:I

    return v0
.end method

.method public bb()V
    .locals 2

    .prologue
    .line 2847
    iget-object v0, p0, Lavo;->f:Lavq;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lavq;->V:Z

    .line 2848
    return-void
.end method

.method public bc()Z
    .locals 1

    .prologue
    .line 2852
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->V:Z

    return v0
.end method

.method public bd()Lawg;
    .locals 1

    .prologue
    .line 2867
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->a:Lawg;

    return-object v0
.end method

.method public be()Ljfd;
    .locals 1

    .prologue
    .line 2873
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->l:Ljfd;

    return-object v0
.end method

.method public bf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2892
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->n:Ljava/lang/String;

    return-object v0
.end method

.method public bg()Lood;
    .locals 1

    .prologue
    .line 2929
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->p:Lood;

    return-object v0
.end method

.method public bh()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2941
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->j:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public bi()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2981
    iget-object v1, p0, Lavo;->f:Lavq;

    iget-object v1, v1, Lavq;->a:Lawg;

    sget-object v2, Lawg;->c:Lawg;

    if-eq v1, v2, :cond_1

    .line 2996
    :cond_0
    :goto_0
    return v0

    .line 2987
    :cond_1
    iget-object v1, p0, Lavo;->f:Lavq;

    iget-boolean v1, v1, Lavq;->p:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lavo;->f:Lavq;

    iget-boolean v1, v1, Lavq;->V:Z

    if-eqz v1, :cond_0

    .line 2992
    :cond_2
    invoke-virtual {p0}, Lavo;->bg()Lood;

    move-result-object v1

    .line 2993
    if-eqz v1, :cond_0

    .line 2997
    invoke-static {v1}, Lcge;->a(Loxu;)[B

    move-result-object v1

    iget-object v2, p0, Lavo;->f:Lavq;

    iget-object v2, v2, Lavq;->m:[B

    .line 2996
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3004
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->o:Ljava/lang/String;

    return-object v0
.end method

.method public bk()Z
    .locals 1

    .prologue
    .line 3014
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->B:Z

    return v0
.end method

.method public bl()Z
    .locals 1

    .prologue
    .line 3027
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->C:Z

    return v0
.end method

.method public c(Landroid/net/Uri;)Lbkr;
    .locals 2

    .prologue
    .line 1715
    invoke-direct {p0, p1}, Lavo;->e(Landroid/net/Uri;)I

    move-result v0

    .line 1718
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lavo;->g:Lavt;

    iget-object v1, v1, Lavt;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkr;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)Lbmu;
    .locals 3

    .prologue
    .line 1678
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->a:Ljava/util/List;

    const-string v1, "index"

    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->a:Ljava/util/List;

    .line 1679
    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    .line 1678
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmu;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1287
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v0, v0, Lavt;->c:I

    if-lez v0, :cond_0

    .line 1288
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 1297
    :goto_0
    return-object v0

    .line 1291
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1292
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1293
    invoke-virtual {p0, v2}, Lavo;->c(I)Lbmu;

    move-result-object v0

    iget-object v0, v0, Lbmu;->e:Lbmv;

    sget-object v1, Lbmv;->b:Lbmv;

    if-ne v0, v1, :cond_1

    .line 1294
    invoke-virtual {p0, v2}, Lavo;->c(I)Lbmu;

    move-result-object v1

    iget-object v0, v1, Lbmu;->e:Lbmv;

    const-string v3, "metadata.type"

    sget-object v5, Lbmv;->b:Lbmv;

    invoke-static {v0, v3, v5}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    invoke-virtual {p0, v2}, Lavo;->a(I)Lavz;

    move-result-object v5

    invoke-virtual {p0}, Lavo;->bd()Lawg;

    move-result-object v0

    sget-object v3, Lawg;->c:Lawg;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->k:Ljava/util/Map;

    iget-object v3, v5, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdx;

    iget-object v3, v5, Lavz;->a:Landroid/net/Uri;

    sget-object v6, Ljei;->a:Ljei;

    invoke-static {v0, v3, v6}, Ljeg;->a(Ljdx;Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v0

    move-object v3, v0

    :goto_2
    new-instance v6, Lbmw;

    move-object v0, v1

    check-cast v0, Lbmx;

    invoke-virtual {p0, v2}, Lavo;->d(I)Lbkr;

    move-result-object v1

    iget-boolean v5, v5, Lavz;->b:Z

    invoke-direct {v6, v3, v0, v1, v5}, Lbmw;-><init>(Ljeg;Lbmx;Lbkr;Z)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1292
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1294
    :cond_2
    iget-object v0, v5, Lavz;->a:Landroid/net/Uri;

    sget-object v3, Ljei;->a:Ljei;

    invoke-static {v0, v3}, Ljeg;->a(Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    :cond_3
    move-object v0, v4

    .line 1297
    goto :goto_0
.end method

.method public c(J)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2243
    invoke-direct {p0}, Lavo;->bq()Z

    move-result v3

    .line 2245
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0}, Lbor;->e()J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-eqz v0, :cond_5

    .line 2246
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0, p1, p2}, Lbor;->a(J)V

    move v0, v1

    .line 2249
    :goto_0
    invoke-virtual {p0}, Lavo;->aV()Z

    move-result v4

    .line 2250
    iget-object v5, p0, Lavo;->f:Lavq;

    iget-object v5, v5, Lavq;->x:Lbor;

    invoke-virtual {v5}, Lbor;->f()Z

    move-result v5

    if-eq v5, v4, :cond_0

    .line 2251
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    invoke-virtual {v0, v4}, Lbor;->a(Z)V

    move v0, v1

    .line 2254
    :cond_0
    if-nez v3, :cond_1

    if-eqz v0, :cond_2

    .line 2255
    :cond_1
    invoke-direct {p0}, Lavo;->bp()V

    .line 2257
    :cond_2
    if-eqz v3, :cond_3

    .line 2258
    new-array v3, v1, [Lavs;

    sget-object v4, Lavs;->i:Lavs;

    aput-object v4, v3, v2

    invoke-direct {p0, v3}, Lavo;->a([Lavs;)V

    .line 2260
    :cond_3
    if-eqz v0, :cond_4

    .line 2261
    new-array v0, v1, [Lavs;

    sget-object v1, Lavs;->a:Lavs;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2263
    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method public c(Lawk;)V
    .locals 1

    .prologue
    .line 1083
    sget-object v0, Lavs;->i:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1084
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2663
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2664
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->J:Ljava/lang/String;

    .line 2665
    return-void
.end method

.method public c(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1876
    if-eq p1, v2, :cond_0

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v4, v0, Lavq;->u:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Can\'t set soundtrack not ready if there is no soundtrack"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 1880
    if-eqz p1, :cond_1

    .line 1881
    iget-object v0, p0, Lavo;->g:Lavt;

    const/4 v3, 0x0

    iput-object v3, v0, Lavt;->r:Lboh;

    .line 1883
    :cond_1
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->m:Z

    if-eq v0, p1, :cond_2

    .line 1884
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->m:Z

    .line 1885
    invoke-direct {p0}, Lavo;->bs()V

    .line 1886
    new-array v0, v2, [Lavs;

    sget-object v2, Lavs;->e:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1888
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 1876
    goto :goto_0
.end method

.method public d(I)Lbkr;
    .locals 3

    .prologue
    .line 1709
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->b:Ljava/util/List;

    const-string v1, "index"

    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->b:Ljava/util/List;

    .line 1710
    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    .line 1709
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkr;

    return-object v0
.end method

.method public d(J)V
    .locals 3

    .prologue
    .line 2290
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-wide p1, v0, Lavt;->g:J

    .line 2291
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->k:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2292
    return-void
.end method

.method public d(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 2727
    iget-object v1, p0, Lavo;->f:Lavq;

    const-string v0, "uri"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, v1, Lavq;->q:Landroid/net/Uri;

    .line 2728
    return-void
.end method

.method public d(Lawk;)V
    .locals 1

    .prologue
    .line 1088
    sget-object v0, Lavs;->i:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1089
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2675
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2676
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->K:Ljava/lang/String;

    .line 2677
    return-void
.end method

.method public d(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1892
    if-eq p1, v2, :cond_0

    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v4, v0, Lavq;->u:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Can\'t set soundtrack not ready if there is no soundtrack"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 1896
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->n:Z

    if-eq v0, p1, :cond_1

    .line 1897
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->n:Z

    .line 1898
    invoke-direct {p0}, Lavo;->bs()V

    .line 1899
    new-array v0, v2, [Lavs;

    sget-object v2, Lavs;->e:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1901
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1892
    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 1302
    iget-object v0, p0, Lavo;->g:Lavt;

    iget v0, v0, Lavt;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 1307
    iget-object v0, p0, Lavo;->g:Lavt;

    invoke-virtual {v0}, Lavt;->b()V

    .line 1308
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lavo;->b(J)V

    .line 1309
    return-void
.end method

.method public e(J)V
    .locals 5

    .prologue
    .line 2549
    iget-object v0, p0, Lavo;->g:Lavt;

    const-string v1, "startTimeUs"

    invoke-static {p1, p2, v1}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v2

    iput-wide v2, v0, Lavt;->v:J

    .line 2551
    return-void
.end method

.method public e(Lawk;)V
    .locals 1

    .prologue
    .line 1093
    sget-object v0, Lavs;->j:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1094
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2753
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->N:Ljava/lang/String;

    .line 2754
    return-void
.end method

.method public e(Z)V
    .locals 3

    .prologue
    .line 1946
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->o:Z

    .line 1947
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->e:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1948
    return-void
.end method

.method public e(I)Z
    .locals 3

    .prologue
    .line 1740
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->b:Ljava/util/List;

    const-string v1, "index"

    iget-object v2, p0, Lavo;->g:Lavt;

    iget-object v2, v2, Lavt;->b:Ljava/util/List;

    .line 1741
    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    .line 1740
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(I)V
    .locals 3

    .prologue
    .line 2148
    iget-object v0, p0, Lavo;->g:Lavt;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lavo;->f:Lavq;

    iget-object v2, v2, Lavq;->y:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2149
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lavt;->y:Ljava/util/List;

    .line 2150
    iget-object v0, p0, Lavo;->g:Lavt;

    iput p1, v0, Lavt;->z:I

    .line 2151
    return-void
.end method

.method public f(J)V
    .locals 5

    .prologue
    .line 2555
    iget-object v0, p0, Lavo;->g:Lavt;

    const-string v1, "endTimeUs"

    invoke-static {p1, p2, v1}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v2

    iput-wide v2, v0, Lavt;->w:J

    .line 2557
    return-void
.end method

.method public f(Lawk;)V
    .locals 1

    .prologue
    .line 1098
    sget-object v0, Lavs;->j:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1099
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3009
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-object p1, v0, Lavq;->o:Ljava/lang/String;

    .line 3010
    return-void
.end method

.method public f(Z)V
    .locals 1

    .prologue
    .line 1972
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->w:Z

    .line 1973
    return-void
.end method

.method public f()Z
    .locals 3

    .prologue
    .line 1313
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->b:Z

    .line 1314
    iget-object v1, p0, Lavo;->f:Lavq;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lavq;->b:Z

    .line 1315
    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1320
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 2581
    iget-object v0, p0, Lavo;->g:Lavt;

    iput p1, v0, Lavt;->t:I

    .line 2582
    return-void
.end method

.method public g(J)V
    .locals 5

    .prologue
    .line 2687
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2688
    iget-object v0, p0, Lavo;->f:Lavq;

    const-string v1, "timestampMs"

    .line 2689
    invoke-static {p1, p2, v1}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v2

    iput-wide v2, v0, Lavq;->H:J

    .line 2690
    return-void
.end method

.method public g(Lawk;)V
    .locals 1

    .prologue
    .line 1103
    sget-object v0, Lavs;->b:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1104
    return-void
.end method

.method public g(Z)V
    .locals 1

    .prologue
    .line 1996
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->s:Z

    .line 1997
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 1325
    iget-object v0, p0, Lavo;->f:Lavq;

    iget v0, v0, Lavq;->c:I

    return v0
.end method

.method public h(Lawk;)V
    .locals 1

    .prologue
    .line 1108
    sget-object v0, Lavs;->b:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1109
    return-void
.end method

.method public h(Z)V
    .locals 1

    .prologue
    .line 2092
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->h:Z

    .line 2093
    return-void
.end method

.method public i(Lawk;)V
    .locals 1

    .prologue
    .line 1113
    sget-object v0, Lavs;->c:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1114
    return-void
.end method

.method public i(Z)V
    .locals 1

    .prologue
    .line 2122
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->i:Z

    .line 2123
    return-void
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 1332
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->g:Z

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 1490
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public j(Lawk;)V
    .locals 1

    .prologue
    .line 1118
    sget-object v0, Lavs;->c:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1119
    return-void
.end method

.method public j(Z)V
    .locals 3

    .prologue
    .line 2324
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->L:Z

    if-eq v0, p1, :cond_0

    .line 2325
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->L:Z

    .line 2326
    invoke-direct {p0}, Lavo;->bp()V

    .line 2327
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->h:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2329
    :cond_0
    return-void
.end method

.method public k(Lawk;)V
    .locals 1

    .prologue
    .line 1123
    sget-object v0, Lavs;->r:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1124
    return-void
.end method

.method public k(Z)V
    .locals 1

    .prologue
    .line 2571
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->u:Z

    .line 2572
    return-void
.end method

.method public k()[Lavz;
    .locals 2

    .prologue
    .line 1519
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    sget-object v1, Lavo;->c:[Lavz;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lavz;

    return-object v0
.end method

.method public l()V
    .locals 4

    .prologue
    .line 1651
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1652
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1653
    invoke-direct {p0}, Lavo;->br()V

    .line 1654
    iget-object v0, p0, Lavo;->g:Lavt;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lavt;->g:J

    .line 1655
    invoke-direct {p0}, Lavo;->bp()V

    .line 1656
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->i:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lavs;->k:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1658
    return-void
.end method

.method public l(Lawk;)V
    .locals 1

    .prologue
    .line 1128
    sget-object v0, Lavs;->r:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1129
    return-void
.end method

.method public l(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2603
    iget-object v0, p0, Lavo;->f:Lavq;

    invoke-virtual {v0, p1}, Lavq;->b(Z)V

    .line 2604
    new-array v0, v3, [Lavs;

    sget-object v1, Lavs;->m:Lavs;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2605
    new-array v0, v3, [Lavs;

    sget-object v1, Lavs;->o:Lavs;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2606
    return-void
.end method

.method public m()Lbor;
    .locals 1

    .prologue
    .line 1746
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->x:Lbor;

    return-object v0
.end method

.method public m(Lawk;)V
    .locals 1

    .prologue
    .line 1132
    sget-object v0, Lavs;->t:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1133
    return-void
.end method

.method public m(Z)V
    .locals 2

    .prologue
    .line 2616
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2617
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->C:Z

    .line 2618
    return-void
.end method

.method public n(Lawk;)V
    .locals 1

    .prologue
    .line 1137
    sget-object v0, Lavs;->d:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1138
    return-void
.end method

.method public n(Z)V
    .locals 2

    .prologue
    .line 2627
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2628
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->D:Z

    .line 2629
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 1751
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->r:Z

    return v0
.end method

.method public o()Lbza;
    .locals 1

    .prologue
    .line 1761
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-object v0, v0, Lavq;->t:Lbza;

    return-object v0
.end method

.method public o(Lawk;)V
    .locals 1

    .prologue
    .line 1142
    sget-object v0, Lavs;->d:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1143
    return-void
.end method

.method public o(Z)V
    .locals 2

    .prologue
    .line 2639
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->B:Z

    const-string v1, "mPersistentState.saving"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 2640
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->E:Z

    .line 2641
    return-void
.end method

.method public p()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1777
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->x:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public p(Lawk;)V
    .locals 1

    .prologue
    .line 1147
    sget-object v0, Lavs;->e:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1148
    return-void
.end method

.method public p(Z)V
    .locals 1

    .prologue
    .line 2737
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->M:Z

    .line 2738
    return-void
.end method

.method public q()J
    .locals 2

    .prologue
    .line 1800
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v0, v0, Lavq;->u:J

    return-wide v0
.end method

.method public q(Lawk;)V
    .locals 1

    .prologue
    .line 1152
    sget-object v0, Lavs;->e:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1153
    return-void
.end method

.method public q(Z)V
    .locals 1

    .prologue
    .line 2814
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->T:Z

    .line 2815
    return-void
.end method

.method public r(Lawk;)V
    .locals 1

    .prologue
    .line 1157
    sget-object v0, Lavs;->f:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1158
    return-void
.end method

.method public r(Z)V
    .locals 3

    .prologue
    .line 2834
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-boolean v0, v0, Lavq;->F:Z

    if-eq p1, v0, :cond_0

    .line 2835
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->F:Z

    .line 2836
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->n:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 2838
    :cond_0
    return-void
.end method

.method public r()Z
    .locals 4

    .prologue
    .line 1805
    iget-object v0, p0, Lavo;->f:Lavq;

    iget-wide v0, v0, Lavq;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()V
    .locals 3

    .prologue
    .line 1847
    invoke-direct {p0}, Lavo;->bp()V

    .line 1848
    invoke-direct {p0}, Lavo;->bs()V

    .line 1849
    iget-object v0, p0, Lavo;->g:Lavt;

    const/4 v1, 0x0

    iput-object v1, v0, Lavt;->l:Lbmk;

    .line 1850
    const/4 v0, 0x2

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->d:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lavs;->e:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 1851
    return-void
.end method

.method public s(Lawk;)V
    .locals 1

    .prologue
    .line 1162
    sget-object v0, Lavs;->f:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1163
    return-void
.end method

.method public s(Z)V
    .locals 1

    .prologue
    .line 2976
    iget-object v0, p0, Lavo;->f:Lavq;

    iput-boolean p1, v0, Lavq;->p:Z

    .line 2977
    return-void
.end method

.method public t(Lawk;)V
    .locals 1

    .prologue
    .line 1167
    sget-object v0, Lavs;->g:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1168
    return-void
.end method

.method public t(Z)V
    .locals 3

    .prologue
    .line 3019
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->B:Z

    if-eq v0, p1, :cond_0

    .line 3020
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->B:Z

    .line 3021
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->s:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 3023
    :cond_0
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 1855
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->q:Z

    return v0
.end method

.method public u()Lbrc;
    .locals 1

    .prologue
    .line 1916
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->s:Lbrc;

    return-object v0
.end method

.method public u(Lawk;)V
    .locals 1

    .prologue
    .line 1172
    sget-object v0, Lavs;->g:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1173
    return-void
.end method

.method public u(Z)V
    .locals 3

    .prologue
    .line 3033
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->C:Z

    if-eq v0, p1, :cond_0

    .line 3034
    iget-object v0, p0, Lavo;->g:Lavt;

    iput-boolean p1, v0, Lavt;->C:Z

    .line 3035
    const/4 v0, 0x1

    new-array v0, v0, [Lavs;

    const/4 v1, 0x0

    sget-object v2, Lavs;->s:Lavs;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lavo;->a([Lavs;)V

    .line 3037
    :cond_0
    return-void
.end method

.method public v()Lbmk;
    .locals 1

    .prologue
    .line 1921
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->l:Lbmk;

    return-object v0
.end method

.method public v(Lawk;)V
    .locals 1

    .prologue
    .line 1177
    sget-object v0, Lavs;->h:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1178
    return-void
.end method

.method public w()Lboh;
    .locals 1

    .prologue
    .line 1926
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->k:Lboh;

    return-object v0
.end method

.method public w(Lawk;)V
    .locals 1

    .prologue
    .line 1182
    sget-object v0, Lavs;->h:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1183
    return-void
.end method

.method public x()Lboh;
    .locals 1

    .prologue
    .line 1931
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-object v0, v0, Lavt;->r:Lboh;

    return-object v0
.end method

.method public x(Lawk;)V
    .locals 1

    .prologue
    .line 1187
    sget-object v0, Lavs;->k:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1188
    return-void
.end method

.method public y()V
    .locals 2

    .prologue
    .line 1936
    iget-object v0, p0, Lavo;->g:Lavt;

    const/4 v1, 0x0

    iput-object v1, v0, Lavt;->r:Lboh;

    .line 1937
    return-void
.end method

.method public y(Lawk;)V
    .locals 1

    .prologue
    .line 1192
    sget-object v0, Lavs;->k:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->b(Lavs;Lawk;)V

    .line 1193
    return-void
.end method

.method public z(Lawk;)V
    .locals 1

    .prologue
    .line 1197
    sget-object v0, Lavs;->l:Lavs;

    invoke-direct {p0, v0, p1}, Lavo;->a(Lavs;Lawk;)V

    .line 1198
    return-void
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 1941
    iget-object v0, p0, Lavo;->g:Lavt;

    iget-boolean v0, v0, Lavt;->o:Z

    return v0
.end method
