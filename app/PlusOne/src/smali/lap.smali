.class public final Llap;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lloz;

.field private static final b:Ljava/lang/Integer;

.field private static final c:Lloy;

.field private static final d:[Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static f:[I

.field private static g:Lloy;

.field private static h:Lhxa;

.field private static final i:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 112
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Llap;->b:Ljava/lang/Integer;

    .line 119
    new-instance v0, Lloz;

    const-string v1, "enable_owner_response"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Llap;->a:Lloz;

    .line 122
    new-instance v0, Llrt;

    const-string v1, "debug.plus.shadow_cards"

    invoke-direct {v0, v1}, Llrt;-><init>(Ljava/lang/String;)V

    .line 124
    invoke-virtual {v0}, Llrr;->a()Llrr;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Llrr;->b()Llrr;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Llrr;->c()Lloy;

    move-result-object v0

    sput-object v0, Llap;->c:Lloy;

    .line 148
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "avatar_url"

    aput-object v1, v0, v4

    sput-object v0, Llap;->d:[Ljava/lang/String;

    .line 168
    const-string v0, "-1"

    const-string v1, "-1"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, v3, v3}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Llap;->e:Ljava/lang/String;

    .line 212
    new-instance v0, Lloy;

    const-string v1, "debug.stream.prefetchV2Check"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Llap;->g:Lloy;

    .line 215
    new-instance v0, Llaq;

    invoke-direct {v0}, Llaq;-><init>()V

    sput-object v0, Llap;->h:Lhxa;

    .line 1972
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "unique_activity_id"

    aput-object v1, v0, v3

    const-string v1, "modified"

    aput-object v1, v0, v4

    const-string v1, "data_state"

    aput-object v1, v0, v5

    sput-object v0, Llap;->i:[Ljava/lang/String;

    return-void
.end method

.method public static a(J)I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2935
    const-wide/16 v0, 0x100

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2936
    const v0, 0x7f0a04aa

    .line 2958
    :goto_0
    return v0

    .line 2937
    :cond_0
    const-wide/16 v0, 0x400

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2938
    const v0, 0x7f0a04ab

    goto :goto_0

    .line 2939
    :cond_1
    const-wide/16 v0, 0x40

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 2940
    const v0, 0x7f0a04a7

    goto :goto_0

    .line 2941
    :cond_2
    const-wide/16 v0, 0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 2942
    const v0, 0x7f0a04a9

    goto :goto_0

    .line 2943
    :cond_3
    const-wide/16 v0, 0x804

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    .line 2945
    const v0, 0x7f0a04ad

    goto :goto_0

    .line 2946
    :cond_4
    const-wide/16 v0, 0x20

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 2947
    const v0, 0x7f0a04a8

    goto :goto_0

    .line 2948
    :cond_5
    const-wide/32 v0, 0x20000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 2949
    const v0, 0x7f0a04af

    goto :goto_0

    .line 2950
    :cond_6
    const-wide/16 v0, 0x8

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    .line 2951
    const v0, 0x7f0a04ac

    goto :goto_0

    .line 2952
    :cond_7
    const-wide/16 v0, 0x1000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    .line 2953
    const v0, 0x7f0a04ae

    goto :goto_0

    .line 2955
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;J[Lpyk;IZLjava/lang/String;ILjava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Lkzl;Ljava/lang/String;)I
    .locals 15

    .prologue
    .line 812
    if-eqz p8, :cond_0

    const/4 v11, 0x1

    .line 813
    :goto_0
    if-eqz p10, :cond_1

    const/4 v12, 0x1

    .line 819
    :goto_1
    const/4 v2, 0x0

    move v14, v2

    move/from16 v10, p9

    :goto_2
    move-object/from16 v0, p5

    array-length v2, v0

    if-ge v14, v2, :cond_5

    .line 820
    aget-object v6, p5, v14

    .line 821
    if-nez v6, :cond_2

    .line 822
    const-string v2, "EsPostsData"

    const-string v3, "null stream item"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    :goto_3
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_2

    .line 812
    :cond_0
    const/4 v11, 0x0

    goto :goto_0

    .line 813
    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    .line 827
    :cond_2
    iget v2, v6, Lpyk;->b:I

    .line 828
    move-object/from16 v0, p12

    invoke-interface {v0, v2}, Lkzl;->a(I)Lkzk;

    move-result-object v2

    .line 829
    if-nez v2, :cond_3

    .line 830
    const-string v2, "EsPostsData"

    const-string v3, "unknown stream type"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_3
    move-object v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p11

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move-object/from16 v13, p13

    .line 833
    invoke-interface/range {v2 .. v13}, Lkzk;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lpyk;Ljava/lang/String;JIZZLjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 835
    add-int/lit8 v10, v10, 0x1

    .line 839
    :cond_4
    sget-object v2, Llap;->c:Lloy;

    goto :goto_3

    .line 854
    :cond_5
    const/4 v2, 0x0

    move v9, v2

    :goto_4
    move-object/from16 v0, p5

    array-length v2, v0

    if-ge v9, v2, :cond_7

    .line 855
    aget-object v6, p5, v9

    .line 858
    iget v2, v6, Lpyk;->b:I

    .line 859
    move-object/from16 v0, p12

    invoke-interface {v0, v2}, Lkzl;->a(I)Lkzk;

    move-result-object v2

    .line 860
    if-nez v2, :cond_6

    .line 861
    const-string v2, "EsPostsData"

    const-string v3, "unknown stream type"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    :goto_5
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_4

    :cond_6
    move-object v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p11

    move/from16 v7, p6

    move/from16 v8, p7

    .line 864
    invoke-interface/range {v2 .. v8}, Lkzk;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lpyk;IZ)V

    .line 868
    sget-object v2, Llap;->c:Lloy;

    goto :goto_5

    .line 880
    :cond_7
    return v10
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[BZ)I
    .locals 10

    .prologue
    .line 1264
    const/4 v8, 0x0

    .line 1265
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1266
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1268
    :try_start_0
    const-string v1, "activities"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "content_flags"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "unique_activity_id"

    aput-object v4, v2, v3

    const-string v3, "event_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1272
    :try_start_1
    new-instance v3, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v3, v1}, Landroid/content/ContentValues;-><init>(I)V

    move v1, v8

    .line 1273
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1274
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1275
    const/4 v6, 0x1

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1278
    if-eqz p4, :cond_1

    .line 1279
    const-wide/32 v8, 0x40000

    and-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-eqz v4, :cond_0

    .line 1284
    :goto_1
    const-string v4, "embed"

    invoke-virtual {v3, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1289
    const-string v4, "activities"

    const-string v5, "activity_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v6, v7, v8

    invoke-virtual {v0, v4, v3, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 1291
    invoke-static {p0, v6}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1294
    :catchall_0
    move-exception v1

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1298
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 1283
    :cond_1
    const-wide/16 v8, 0x100

    and-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-eqz v4, :cond_0

    goto :goto_1

    .line 1294
    :cond_2
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1296
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1298
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1300
    return v1
.end method

.method private static a(Logr;J)J
    .locals 9

    .prologue
    .line 1957
    iget-object v0, p0, Logr;->h:[Lofa;

    if-eqz v0, :cond_0

    .line 1958
    iget-object v1, p0, Logr;->h:[Lofa;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1959
    iget-object v4, v3, Lofa;->k:Ljava/lang/Long;

    .line 1960
    invoke-static {v4}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v4

    iget-object v3, v3, Lofa;->e:Ljava/lang/Long;

    .line 1961
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1960
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 1959
    invoke-static {p1, p2, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    .line 1958
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1964
    :cond_0
    iget-object v0, p0, Logr;->C:Loae;

    if-eqz v0, :cond_1

    .line 1965
    iget-object v0, p0, Logr;->C:Loae;

    iget-object v0, v0, Loae;->b:Ljava/lang/Double;

    .line 1966
    invoke-static {v0}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v0

    double-to-long v0, v0

    .line 1965
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    .line 1969
    :cond_1
    return-wide p1
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    .line 349
    invoke-static {p1, p2, p4}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 351
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, p3, v0, p4, p0}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a([Lofa;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lofa;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lofa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2587
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2588
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, p0, v0

    .line 2589
    iget-object v4, v3, Lofa;->i:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v3, Lofa;->j:Ljava/lang/Boolean;

    .line 2590
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2591
    :cond_0
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2588
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2594
    :cond_2
    return-object v1
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;[Logr;)Ljava/util/HashMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "[",
            "Logr;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Llar;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1987
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1988
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1989
    const-string v0, "unique_activity_id IN ("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1990
    array-length v1, p1

    move v0, v4

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 1991
    const-string v7, "?,"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1992
    iget-object v2, v2, Logr;->af:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1990
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1994
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1995
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1997
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 1998
    const-string v1, "activities"

    sget-object v2, Llap;->i:[Ljava/lang/String;

    .line 1999
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v0, v4, [Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    .line 1998
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2002
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2003
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2004
    new-instance v2, Llar;

    invoke-direct {v2}, Llar;-><init>()V

    .line 2005
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Llar;->a:J

    .line 2006
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Llar;->b:I

    .line 2007
    invoke-virtual {v8, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2010
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2012
    return-object v8
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZZZJ[Ljava/lang/String;)Lkff;
    .locals 28

    .prologue
    .line 508
    const-class v6, Llfn;

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Llfn;

    const-wide/16 v8, 0x2

    const-string v7, "doActivityStreamSync start"

    move/from16 v0, p1

    invoke-interface {v6, v0, v8, v9, v7}, Llfn;->a(IJLjava/lang/String;)V

    .line 510
    const-string v6, "EsPostsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 511
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 512
    move-object/from16 v0, p4

    move-object/from16 v1, p3

    move/from16 v2, p2

    invoke-static {v0, v1, v6, v7, v2}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v6

    .line 513
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x3f

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "doActivityStreamSync starting sync stream: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", count: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p9

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 517
    :cond_0
    new-instance v7, Llav;

    if-eqz p15, :cond_1

    move-object/from16 v24, p18

    :goto_0
    move-object/from16 v8, p0

    move/from16 v9, p1

    move/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move/from16 v14, p6

    move-object/from16 v15, p7

    move-object/from16 v16, p8

    move/from16 v17, p9

    move-object/from16 v18, p10

    move-object/from16 v19, p11

    move-object/from16 v20, p12

    move/from16 v21, p14

    move-wide/from16 v22, p16

    invoke-direct/range {v7 .. v24}, Llav;-><init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZJ[Ljava/lang/String;)V

    .line 521
    invoke-virtual {v7}, Llav;->l()V

    .line 523
    iget-object v6, v7, Lkff;->k:Ljava/lang/Exception;

    if-eqz v6, :cond_2

    .line 524
    iget-object v6, v7, Lkff;->k:Ljava/lang/Exception;

    throw v6

    .line 517
    :cond_1
    const/16 v24, 0x0

    goto :goto_0

    .line 527
    :cond_2
    invoke-virtual {v7}, Llav;->t()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 528
    new-instance v6, Ljava/io/IOException;

    iget v8, v7, Lkff;->i:I

    iget-object v7, v7, Lkff;->j:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x15

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Error: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 532
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 533
    invoke-virtual {v7}, Llav;->D()Loxu;

    move-result-object v6

    check-cast v6, Lmbf;

    iget-object v9, v6, Lmbf;->a:Loda;

    .line 535
    move/from16 v0, p2

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, p3

    move/from16 v4, p6

    invoke-static {v0, v1, v2, v3, v4}, Llap;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v26

    .line 538
    if-eqz p14, :cond_5

    sget-object v11, Llap;->e:Ljava/lang/String;

    .line 540
    :goto_1
    if-nez p15, :cond_4

    .line 541
    const-string v6, "EsPostsData"

    const/4 v8, 0x4

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 542
    iget-object v6, v9, Loda;->a:Logi;

    iget-object v6, v6, Logi;->c:Ljava/lang/String;

    move-object/from16 v0, p7

    invoke-static {v0, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    .line 544
    if-eqz v6, :cond_6

    const-string v6, "!!!!!"

    .line 545
    :goto_2
    invoke-virtual {v7}, Llav;->k()J

    move-result-wide v14

    iget-object v8, v9, Loda;->a:Logi;

    iget-object v8, v8, Logi;->c:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    iget-object v8, v9, Loda;->a:Logi;

    iget-object v8, v8, Logi;->b:[Lpyk;

    if-nez v8, :cond_7

    const-string v8, "0"

    .line 547
    :goto_3
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 548
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v18, v18, 0x52

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v17, "Sent token "

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v17, " at time "

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v14, " and received token "

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, " with "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " activities for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    :cond_4
    if-nez p7, :cond_8

    if-nez p8, :cond_8

    iget-object v6, v9, Loda;->c:Lodc;

    if-eqz v6, :cond_8

    .line 554
    iget-object v6, v9, Loda;->c:Lodc;

    iget-object v6, v6, Lodc;->a:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide p16

    move-wide/from16 v24, p16

    .line 556
    :goto_4
    iget-object v6, v9, Loda;->a:Logi;

    iget-object v14, v6, Logi;->b:[Lpyk;

    const/4 v15, 0x3

    iget-object v6, v9, Loda;->a:Logi;

    iget-object v0, v6, Logi;->c:Ljava/lang/String;

    move-object/from16 v17, v0

    iget-object v6, v9, Loda;->a:Logi;

    iget-object v0, v6, Logi;->d:[B

    move-object/from16 v18, v0

    const/16 v22, 0x0

    move-object/from16 v9, p0

    move/from16 v10, p1

    move-object/from16 v16, p7

    move-object/from16 v19, p12

    move/from16 v20, p13

    move-object/from16 v21, p5

    move/from16 v23, p14

    invoke-static/range {v9 .. v26}, Llap;->a(Landroid/content/Context;ILjava/lang/String;J[Lpyk;ILjava/lang/String;Ljava/lang/String;[BLkfp;ZLjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;)V

    .line 561
    const-class v6, Llfn;

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Llfn;

    const-wide/16 v8, 0x2

    const-string v10, "doActivityStreamSync end"

    move/from16 v0, p1

    invoke-interface {v6, v0, v8, v9, v10}, Llfn;->a(IJLjava/lang/String;)V

    .line 563
    return-object v7

    :cond_5
    move-object/from16 v11, v26

    .line 538
    goto/16 :goto_1

    .line 544
    :cond_6
    const-string v6, ""

    goto/16 :goto_2

    .line 545
    :cond_7
    iget-object v8, v9, Loda;->a:Logi;

    iget-object v8, v8, Logi;->b:[Lpyk;

    array-length v8, v8

    .line 547
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto/16 :goto_3

    :cond_8
    move-wide/from16 v24, p16

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Llah;
    .locals 3

    .prologue
    .line 2190
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x35

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> plusOneComment activity id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", commentId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2195
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2196
    invoke-static {v1, p3}, Llap;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;

    move-result-object v0

    .line 2197
    if-nez v0, :cond_1

    .line 2198
    const/4 v0, 0x0

    .line 2205
    :goto_0
    return-object v0

    .line 2201
    :cond_1
    invoke-virtual {v0, p4}, Llah;->a(Z)V

    .line 2202
    invoke-static {v1, p3, v0}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Llah;)V

    .line 2203
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Z)Llah;
    .locals 3

    .prologue
    .line 2069
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2070
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x25

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> plusOnePost activity id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2073
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2074
    invoke-static {v1, p2}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;

    move-result-object v0

    .line 2076
    if-nez v0, :cond_1

    .line 2077
    const/4 v0, 0x0

    .line 2086
    :goto_0
    return-object v0

    .line 2080
    :cond_1
    invoke-virtual {v0, p3}, Llah;->a(Z)V

    .line 2082
    const/4 v2, 0x1

    invoke-static {p0, v1, p2, v0, v2}, Llap;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Llah;Z)V

    .line 2084
    invoke-static {p0, p1, p2, v0}, Llap;->a(Landroid/content/Context;ILjava/lang/String;Llah;)V

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2016
    const-string v1, "activities"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "plus_one_data"

    aput-object v0, v2, v6

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2019
    if-nez v1, :cond_0

    .line 2033
    :goto_0
    return-object v5

    .line 2023
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2024
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2026
    new-instance v5, Llah;

    invoke-direct {v5}, Llah;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2033
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2028
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Llah;->a([B)Llah;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 2033
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2031
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZLlaz;J[Ljava/lang/String;)Llau;
    .locals 27

    .prologue
    .line 384
    move/from16 v0, p2

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, p3

    move/from16 v4, p6

    invoke-static {v0, v1, v2, v3, v4}, Llap;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v25

    .line 387
    if-eqz p14, :cond_4

    .line 388
    const-string v6, "EsPostsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 389
    const-string v6, "Activities fetch cached: "

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 391
    :cond_0
    :goto_0
    invoke-virtual/range {p14 .. p14}, Llaz;->b()Loda;

    move-result-object v10

    .line 392
    iget-object v6, v10, Loda;->c:Lodc;

    if-nez v6, :cond_3

    const-wide/16 v6, 0x0

    move-wide v8, v6

    .line 393
    :goto_1
    iget-object v6, v10, Loda;->a:Logi;

    iget-object v7, v6, Logi;->b:[Lpyk;

    const-class v6, Llfn;

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Llfn;

    const-wide/16 v12, 0x2

    const-string v11, "updateTopStreamFromCache start"

    move/from16 v0, p1

    invoke-interface {v6, v0, v12, v13, v11}, Llfn;->a(IJLjava/lang/String;)V

    invoke-static/range {p0 .. p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    if-eqz p12, :cond_1

    :try_start_0
    array-length v6, v7

    move-object/from16 v0, p12

    invoke-virtual {v0, v6}, Lkfp;->l(I)V

    :cond_1
    const-string v6, "activity_streams"

    const-string v7, "stream_key=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object v25, v12, v13

    invoke-virtual {v11, v6, v7, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    const-string v7, "stream_key"

    move-object/from16 v0, v25

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "activity_streams"

    const-string v12, "stream_key=?"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    sget-object v15, Llap;->e:Ljava/lang/String;

    aput-object v15, v13, v14

    invoke-virtual {v11, v7, v6, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static/range {v25 .. v25}, Llbi;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v11, 0x0

    invoke-virtual {v6, v7, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const-class v6, Llfn;

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Llfn;

    const-wide/16 v12, 0x2

    const-string v7, "updateTopStreamFromCache end"

    move/from16 v0, p1

    invoke-interface {v6, v0, v12, v13, v7}, Llfn;->a(IJLjava/lang/String;)V

    .line 395
    new-instance v6, Llau;

    iget-object v7, v10, Loda;->a:Logi;

    iget-object v7, v7, Logi;->c:Ljava/lang/String;

    iget-object v10, v10, Loda;->a:Logi;

    iget-object v10, v10, Logi;->d:[B

    invoke-direct {v6, v7, v10, v8, v9}, Llau;-><init>(Ljava/lang/String;[BJ)V

    .line 409
    :goto_2
    return-object v6

    .line 389
    :cond_2
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 392
    :cond_3
    iget-object v6, v10, Loda;->c:Lodc;

    iget-object v6, v6, Lodc;->a:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-wide v8, v6

    goto/16 :goto_1

    .line 393
    :catchall_0
    move-exception v6

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6

    .line 399
    :cond_4
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v6, p0

    move/from16 v7, p1

    move/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    move/from16 v15, p9

    move-object/from16 v16, p10

    move-object/from16 v17, p11

    move-object/from16 v18, p12

    move/from16 v19, p13

    move-wide/from16 v22, p15

    move-object/from16 v24, p17

    .line 400
    invoke-static/range {v6 .. v24}, Llap;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZZZJ[Ljava/lang/String;)Lkff;

    move-result-object v6

    check-cast v6, Llav;

    .line 405
    const-string v7, "EsPostsData"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 406
    const-string v7, "Activities fetch NOT cached: "

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_6

    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 408
    :cond_5
    :goto_3
    new-instance v7, Llau;

    invoke-virtual {v6}, Llav;->i()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Llav;->j()[B

    move-result-object v9

    .line 409
    invoke-virtual {v6}, Llav;->k()J

    move-result-wide v10

    invoke-direct {v7, v8, v9, v10, v11}, Llau;-><init>(Ljava/lang/String;[BJ)V

    move-object v6, v7

    goto :goto_2

    .line 406
    :cond_6
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static a([B)Lnwr;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3185
    if-nez p0, :cond_0

    move-object v0, v1

    .line 3192
    :goto_0
    return-object v0

    .line 3190
    :cond_0
    :try_start_0
    new-instance v0, Lnwr;

    invoke-direct {v0}, Lnwr;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnwr;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3192
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Boolean;)Logy;
    .locals 3

    .prologue
    .line 2969
    new-instance v0, Logy;

    invoke-direct {v0}, Logy;-><init>()V

    .line 2970
    const/16 v1, 0x19

    iput v1, v0, Logy;->a:I

    .line 2971
    iput-object p1, v0, Logy;->c:Ljava/lang/Boolean;

    .line 2972
    const/4 v1, 0x2

    iput v1, v0, Logy;->b:I

    .line 2973
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    iput-object v1, v0, Logy;->d:[Ljava/lang/String;

    .line 2975
    return-object v0
.end method

.method public static a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;[Logr;IZ)V
    .locals 26

    .prologue
    .line 1316
    move-object/from16 v0, p3

    array-length v2, v0

    if-nez v2, :cond_1

    .line 1842
    :cond_0
    return-void

    .line 1320
    :cond_1
    invoke-static/range {p2 .. p3}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;[Logr;)Ljava/util/HashMap;

    move-result-object v20

    .line 1322
    const/4 v2, 0x1

    move/from16 v0, p4

    if-eq v0, v2, :cond_1e

    const/4 v2, 0x1

    move v13, v2

    .line 1325
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Liai;

    .line 1324
    invoke-static {v2, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Liai;

    .line 1328
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 1329
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v22, v0

    const/4 v2, 0x0

    move/from16 v19, v2

    :goto_1
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    aget-object v23, p3, v19

    .line 1330
    move-object/from16 v0, v23

    iget-object v0, v0, Logr;->af:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 1331
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1332
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->k:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->g:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->y:Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x3a

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, ">>>>> Unique activity id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", author id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", updated: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", read: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1338
    :cond_2
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->B:Ljava/lang/Long;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->g:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 1339
    move-object/from16 v0, v23

    invoke-static {v0, v6, v7}, Llap;->a(Logr;J)J

    move-result-wide v16

    .line 1341
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->af:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Llar;

    .line 1342
    if-nez p5, :cond_4

    .line 1343
    if-eqz v11, :cond_3

    iget-wide v2, v11, Llar;->a:J

    cmp-long v2, v16, v2

    if-nez v2, :cond_3

    iget v2, v11, Llar;->b:I

    if-eqz v2, :cond_1f

    :cond_3
    const/4 v2, 0x1

    .line 1345
    :goto_2
    if-eqz v2, :cond_1d

    .line 1346
    :cond_4
    const-wide/16 v4, 0x0

    .line 1352
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->k:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->e:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v8, v0, Logr;->l:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3, v8}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1354
    const-class v2, Llbj;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Llbj;

    .line 1355
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->k:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->e:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v8, v0, Logr;->l:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-interface {v12, v0, v2, v3, v8}, Llbj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1358
    invoke-virtual/range {v21 .. v21}, Landroid/content/ContentValues;->clear()V

    .line 1360
    const-wide/16 v2, 0x0

    .line 1361
    move-object/from16 v0, v23

    iget-object v8, v0, Logr;->o:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1362
    const-wide/16 v2, 0x1

    .line 1364
    :cond_5
    move-object/from16 v0, v23

    iget-object v8, v0, Logr;->T:Ljava/lang/Boolean;

    invoke-static {v8}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v8

    if-eqz v8, :cond_7

    move-object/from16 v0, v23

    iget v8, v0, Logr;->S:I

    const/high16 v9, -0x80000000

    if-eq v8, v9, :cond_6

    move-object/from16 v0, v23

    iget v8, v0, Logr;->S:I

    if-nez v8, :cond_7

    .line 1367
    :cond_6
    const-wide/16 v8, 0x2

    or-long/2addr v2, v8

    .line 1369
    :cond_7
    move-object/from16 v0, v23

    iget-object v8, v0, Logr;->g:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_8

    .line 1370
    const-wide/16 v6, 0x100

    or-long/2addr v2, v6

    .line 1372
    :cond_8
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->q:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1373
    const-wide/16 v6, 0x4

    or-long/2addr v2, v6

    .line 1375
    :cond_9
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->J:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1376
    const-wide/16 v6, 0x8

    or-long/2addr v2, v6

    .line 1378
    :cond_a
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->E:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1379
    const-wide/16 v6, 0x10

    or-long/2addr v2, v6

    .line 1381
    :cond_b
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->F:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1382
    const-wide/16 v6, 0x20

    or-long/2addr v2, v6

    .line 1384
    :cond_c
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->p:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1385
    const-wide/16 v6, 0x40

    or-long/2addr v2, v6

    .line 1387
    :cond_d
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->y:Ljava/lang/Boolean;

    if-eqz v6, :cond_e

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->y:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1388
    :cond_e
    const-wide/16 v6, 0x80

    or-long/2addr v2, v6

    .line 1390
    :cond_f
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->s:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1391
    const-wide/16 v6, 0x800

    or-long/2addr v2, v6

    .line 1393
    :cond_10
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->f:Logq;

    if-eqz v6, :cond_11

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->f:Logq;

    iget-object v6, v6, Logq;->a:Ljava/lang/Boolean;

    .line 1394
    invoke-static {v6}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1395
    const-wide/16 v6, 0x200

    or-long/2addr v2, v6

    .line 1399
    :cond_11
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->R:Ljava/lang/Boolean;

    invoke-static {v6}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v6

    if-eqz v6, :cond_5d

    .line 1400
    const-wide/16 v6, 0x400

    or-long/2addr v2, v6

    move-wide v14, v2

    .line 1402
    :goto_3
    const-string v2, "activity_flags"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1404
    const-string v2, "embed"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1405
    const-string v2, "embed_appinvite"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1406
    const-string v2, "embed_deep_link"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1407
    const-string v2, "payload"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1408
    const-string v2, "feedback_state"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1410
    const-string v2, "activity_id"

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->i:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    const-string v2, "unique_activity_id"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    const-string v2, "author_id"

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->k:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414
    const-string v2, "author_annotation"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1415
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->Z:Lpeo;

    if-eqz v2, :cond_12

    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->Z:Lpeo;

    iget-object v2, v2, Lpeo;->b:[Lpen;

    if-eqz v2, :cond_12

    .line 1416
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->Z:Lpeo;

    sget-object v3, Llap;->h:Lhxa;

    .line 1419
    invoke-static {}, Lhwr;->b()Lhxa;

    move-result-object v6

    .line 1420
    invoke-static {}, Lhwr;->b()Lhxa;

    move-result-object v7

    .line 1416
    invoke-static {v2, v3, v6, v7}, Lhwr;->a(Lpeo;Lhxa;Lhxa;Lhxa;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 1422
    const-string v3, "author_annotation"

    invoke-static {v2}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1425
    :cond_12
    const-string v2, "source_id"

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->j:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1426
    const-string v2, "source_name"

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->d:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1428
    const-string v3, "data_state"

    if-eqz v13, :cond_20

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1431
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->Q:Lpdt;

    if-eqz v2, :cond_21

    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->M:Loya;

    if-eqz v2, :cond_13

    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->M:Loya;

    sget-object v3, Loym;->a:Loxr;

    .line 1432
    invoke-virtual {v2, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_21

    .line 1433
    :cond_13
    const-string v2, "loc"

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->Q:Lpdt;

    invoke-static {v3}, Llae;->a(Lpdt;)[B

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1434
    const-wide/16 v2, 0x8

    .line 1439
    :goto_5
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->Y:Lpeo;

    if-eqz v4, :cond_22

    .line 1440
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->Y:Lpeo;

    invoke-static {v4}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 1442
    const-string v5, "annotation"

    invoke-static {v4}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1443
    const-wide/16 v4, 0x2

    or-long/2addr v2, v4

    .line 1449
    :goto_6
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->X:Lpeo;

    if-eqz v4, :cond_23

    .line 1450
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->X:Lpeo;

    invoke-static {v4}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 1452
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1453
    const-string v6, "title"

    invoke-static {v4}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1454
    const-wide/16 v6, 0x1

    or-long/2addr v2, v6

    .line 1460
    :goto_7
    const/4 v8, 0x0

    .line 1461
    const/4 v7, 0x0

    .line 1462
    const/4 v6, 0x0

    .line 1463
    const/4 v4, 0x0

    .line 1466
    move-object/from16 v0, v23

    iget-object v9, v0, Logr;->w:Lofv;

    if-eqz v9, :cond_24

    move-object/from16 v0, v23

    iget-object v9, v0, Logr;->t:Ljava/lang/String;

    if-eqz v9, :cond_24

    move-object/from16 v0, v23

    iget-object v9, v0, Logr;->i:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v0, v0, Logr;->t:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 1467
    move-object/from16 v0, v18

    invoke-static {v9, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_24

    .line 1468
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->w:Lofv;

    iget-object v8, v6, Lofv;->c:Ljava/lang/String;

    .line 1469
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->w:Lofv;

    iget-object v7, v6, Lofv;->b:Ljava/lang/String;

    .line 1470
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->w:Lofv;

    iget-object v6, v6, Lofv;->d:Ljava/lang/String;

    .line 1471
    move-object/from16 v0, v23

    iget-object v9, v0, Logr;->N:Ljava/lang/Boolean;

    invoke-static {v9}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 1472
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->t:Ljava/lang/String;

    .line 1483
    :cond_14
    :goto_8
    const-string v9, "original_author_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1484
    const-string v8, "original_author_name"

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    const-string v7, "original_author_avatar_url"

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1486
    const-string v6, "original_activity_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    const-string v4, "total_comment_count"

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->K:Ljava/lang/Integer;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1489
    const-string v6, "acl_display"

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->o:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_25

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->x:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-nez v4, :cond_25

    const v4, 0x7f0a04a3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_9
    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1491
    const-string v4, "created"

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->g:Ljava/lang/Long;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1492
    const-string v4, "modified"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1494
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->D:Loff;

    if-eqz v4, :cond_15

    .line 1495
    const-string v4, "domain"

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->D:Loff;

    iget-object v6, v6, Loff;->a:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    :cond_15
    const-string v4, "birthday"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1499
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->H:Lohc;

    if-eqz v4, :cond_16

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->H:Lohc;

    iget-object v4, v4, Lohc;->b:Lohb;

    if-eqz v4, :cond_16

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->H:Lohc;

    iget-object v4, v4, Lohc;->b:Lohb;

    iget-object v4, v4, Lohb;->a:Loal;

    if-eqz v4, :cond_16

    .line 1501
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->H:Lohc;

    iget-object v4, v4, Lohc;->b:Lohb;

    iget-object v4, v4, Lohb;->a:Loal;

    .line 1502
    iget-object v6, v4, Loal;->a:Lodo;

    iget-object v6, v6, Lodo;->c:Ljava/lang/String;

    .line 1503
    iget-object v7, v4, Loal;->a:Lodo;

    iget-object v7, v7, Lodo;->b:Ljava/lang/String;

    .line 1504
    invoke-static {v6, v7}, Lkzm;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 1505
    new-instance v8, Lkzm;

    iget-object v9, v4, Loal;->b:Ljava/lang/Integer;

    iget-object v4, v4, Loal;->a:Lodo;

    iget-object v4, v4, Lodo;->f:Ljava/lang/String;

    invoke-direct {v8, v6, v7, v9, v4}, Lkzm;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 1507
    const-string v4, "birthday"

    invoke-static {v8}, Lkzm;->a(Lkzm;)[B

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1512
    :cond_16
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->C:Loae;

    if-eqz v4, :cond_2c

    .line 1513
    const-string v4, "plus_one_data"

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->C:Loae;

    invoke-static {v6}, Llah;->a(Loae;)[B

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1518
    :goto_a
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->O:Logh;

    .line 1519
    if-eqz v4, :cond_17

    .line 1520
    const-string v6, "square_update"

    invoke-static {v4}, Llal;->a(Logh;)[B

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1521
    const-wide/16 v6, 0x4000

    or-long/2addr v2, v6

    .line 1524
    :cond_17
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->P:Logh;

    .line 1525
    if-eqz v4, :cond_2d

    .line 1526
    const-string v6, "square_reshare_update"

    .line 1527
    invoke-static {v4}, Llal;->a(Logh;)[B

    move-result-object v4

    .line 1526
    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1532
    :goto_b
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->V:Logb;

    if-eqz v4, :cond_2e

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->V:Logb;

    iget-object v4, v4, Logb;->a:[Logp;

    if-eqz v4, :cond_2e

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->V:Logb;

    iget-object v4, v4, Logb;->a:[Logp;

    array-length v4, v4

    if-lez v4, :cond_2e

    .line 1534
    const-string v4, "relateds"

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->V:Logb;

    invoke-static {v6}, Llai;->a(Logb;)[B

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1539
    :goto_c
    const-string v4, "num_reshares"

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->L:Ljava/lang/Integer;

    .line 1540
    invoke-static {v6}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1539
    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1542
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->M:Loya;

    if-eqz v4, :cond_5c

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->M:Loya;

    iget-object v4, v4, Loya;->d:Loyp;

    if-eqz v4, :cond_5c

    .line 1543
    const-string v4, "embed_deep_link"

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->M:Loya;

    iget-object v6, v6, Loya;->d:Loyp;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 1544
    invoke-static {v6, v7, v8}, Lkzs;->a(Loyp;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v6

    .line 1543
    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1545
    const-wide/16 v6, 0x800

    or-long/2addr v2, v6

    move-wide v6, v2

    .line 1549
    :goto_d
    const/4 v2, 0x0

    .line 1550
    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->M:Loya;

    if-eqz v3, :cond_5b

    .line 1551
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->M:Loya;

    sget-object v3, Loyj;->a:Loxr;

    invoke-virtual {v2, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Loyj;

    move-object v4, v2

    .line 1553
    :goto_e
    if-eqz v4, :cond_30

    iget-object v2, v4, Loyj;->c:Loyo;

    if-eqz v2, :cond_30

    iget-object v2, v4, Loyj;->c:Loyo;

    iget-object v2, v2, Loyo;->a:Loyp;

    if-eqz v2, :cond_30

    .line 1555
    iget-object v3, v4, Loyj;->b:Loya;

    .line 1556
    iget-object v2, v4, Loyj;->d:Loyh;

    if-nez v2, :cond_2f

    const/4 v2, 0x0

    .line 1558
    :goto_f
    const-string v8, "embed_appinvite"

    iget-object v9, v4, Loyj;->c:Loyo;

    iget-object v9, v9, Loyo;->a:Loyp;

    iget-object v4, v4, Loyj;->c:Loyo;

    iget-object v4, v4, Loyo;->b:Ljava/lang/String;

    .line 1559
    invoke-static {v9, v4, v2}, Lkzs;->a(Loyp;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    .line 1558
    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1563
    const-wide/16 v8, 0x2000

    or-long v16, v6, v8

    move-object v4, v3

    .line 1568
    :goto_10
    if-eqz v4, :cond_5a

    .line 1570
    const/16 v18, 0x0

    .line 1572
    sget-object v2, Lpdp;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_31

    .line 1574
    new-instance v3, Lkzv;

    sget-object v2, Lpdp;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpdp;

    invoke-direct {v3, v2}, Lkzv;-><init>(Lpdp;)V

    move-object v4, v3

    move-wide/from16 v2, v16

    .line 1779
    :goto_11
    if-eqz v4, :cond_19

    .line 1780
    const-string v5, "embed"

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v7, 0x40

    invoke-direct {v6, v7}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v7, Ljava/io/DataOutputStream;

    invoke-direct {v7, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v4, v7}, Lkzv;->a(Ljava/io/DataOutputStream;)V

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v7}, Ljava/io/DataOutputStream;->close()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1782
    invoke-virtual {v4}, Lkzv;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 1783
    invoke-virtual {v4}, Lkzv;->n()Z

    move-result v5

    if-eqz v5, :cond_54

    .line 1784
    const-wide/16 v6, 0x80

    or-long/2addr v2, v6

    .line 1790
    :cond_18
    :goto_12
    invoke-virtual {v4}, Lkzv;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_19

    .line 1791
    invoke-virtual {v4}, Lkzv;->n()Z

    move-result v4

    if-nez v4, :cond_19

    .line 1792
    const-wide/16 v4, 0x4

    or-long/2addr v2, v4

    .line 1797
    :cond_19
    :goto_13
    const-string v4, "content_flags"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1799
    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->h:[Lofa;

    .line 1800
    if-eqz v3, :cond_55

    .line 1801
    new-instance v2, Llam;

    invoke-static {v3}, Llap;->a([Lofa;)Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x3

    invoke-direct {v2, v4, v5}, Llam;-><init>(Ljava/util/List;I)V

    .line 1803
    const-string v4, "comment"

    invoke-static {v2}, Llam;->a(Llam;)[B

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1808
    :goto_14
    const-wide/16 v4, 0x1

    and-long/2addr v4, v14

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_1a

    const-wide/16 v4, 0x400

    and-long/2addr v4, v14

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_1b

    .line 1810
    :cond_1a
    const-string v2, "permalink"

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->W:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1813
    :cond_1b
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->ad:Logw;

    .line 1814
    if-eqz v2, :cond_1c

    iget-object v4, v2, Logw;->a:Lnwr;

    if-eqz v4, :cond_1c

    .line 1815
    const-string v4, "promoted_post_data"

    iget-object v2, v2, Logw;->a:Lnwr;

    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1819
    :cond_1c
    if-eqz v11, :cond_56

    .line 1820
    const-string v2, "activities"

    const-string v4, "unique_activity_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v24, v5, v6

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1828
    :goto_15
    if-nez v13, :cond_1d

    .line 1829
    if-eqz v3, :cond_58

    array-length v2, v3

    if-lez v2, :cond_58

    .line 1830
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->i:Ljava/lang/String;

    if-nez v11, :cond_57

    const/4 v2, 0x1

    :goto_16
    move-object/from16 v0, p2

    invoke-static {v0, v4, v3, v2, v12}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lofa;ZLlbj;)V

    .line 1329
    :cond_1d
    :goto_17
    add-int/lit8 v2, v19, 0x1

    move/from16 v19, v2

    goto/16 :goto_1

    .line 1322
    :cond_1e
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_0

    .line 1343
    :cond_1f
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1428
    :cond_20
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 1436
    :cond_21
    const-string v2, "loc"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    move-wide v2, v4

    goto/16 :goto_5

    .line 1445
    :cond_22
    const-string v4, "annotation"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1456
    :cond_23
    const-string v4, "title"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1457
    const-string v5, ""

    goto/16 :goto_7

    .line 1474
    :cond_24
    move-object/from16 v0, v23

    iget-object v9, v0, Logr;->v:Lofv;

    if-eqz v9, :cond_14

    move-object/from16 v0, v23

    iget-object v9, v0, Logr;->u:Ljava/lang/String;

    if-eqz v9, :cond_14

    move-object/from16 v0, v23

    iget-object v9, v0, Logr;->i:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v0, v0, Logr;->u:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 1475
    move-object/from16 v0, v18

    invoke-static {v9, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_14

    .line 1476
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->v:Lofv;

    iget-object v8, v6, Lofv;->c:Ljava/lang/String;

    .line 1477
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->v:Lofv;

    iget-object v7, v6, Lofv;->b:Ljava/lang/String;

    .line 1478
    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->v:Lofv;

    iget-object v6, v6, Lofv;->d:Ljava/lang/String;

    .line 1479
    move-object/from16 v0, v23

    iget-object v9, v0, Logr;->N:Ljava/lang/Boolean;

    invoke-static {v9}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 1480
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->u:Ljava/lang/String;

    goto/16 :goto_8

    .line 1489
    :cond_25
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->x:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_27

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->R:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_27

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->D:Loff;

    if-eqz v4, :cond_26

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->D:Loff;

    iget-object v4, v4, Loff;->a:Ljava/lang/String;

    goto/16 :goto_9

    :cond_26
    const-string v4, ""

    goto/16 :goto_9

    :cond_27
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->z:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-nez v4, :cond_28

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->G:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_29

    :cond_28
    const v4, 0x7f0a049f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    :cond_29
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->I:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_2a

    const v4, 0x7f0a04a4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    :cond_2a
    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->ae:Logd;

    if-eqz v4, :cond_2b

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->ae:Logd;

    iget v4, v4, Logd;->a:I

    const/4 v7, 0x1

    if-ne v4, v7, :cond_2b

    const v4, 0x7f0a04a2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    :cond_2b
    const v4, 0x7f0a04a0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    .line 1515
    :cond_2c
    const-string v4, "plus_one_data"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1529
    :cond_2d
    const-string v4, "square_reshare_update"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1536
    :cond_2e
    const-string v4, "relateds"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 1556
    :cond_2f
    iget-object v2, v4, Loyj;->d:Loyh;

    iget-object v2, v2, Loyh;->a:Ljava/lang/String;

    goto/16 :goto_f

    .line 1565
    :cond_30
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->M:Loya;

    move-object v4, v2

    move-wide/from16 v16, v6

    goto/16 :goto_10

    .line 1577
    :cond_31
    sget-object v2, Loyv;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_32

    .line 1579
    new-instance v3, Licj;

    sget-object v2, Loyv;->a:Loxr;

    .line 1580
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Loyv;

    invoke-direct {v3, v2}, Licj;-><init>(Loyv;)V

    .line 1581
    const-string v2, "embed"

    invoke-static {v3}, Licj;->a(Licj;)[B

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1582
    const-wide/32 v2, 0x20000

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    .line 1584
    goto/16 :goto_11

    :cond_32
    sget-object v2, Lpbu;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_33

    .line 1586
    sget-object v2, Lpbu;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpbu;

    .line 1587
    new-instance v3, Lkzv;

    invoke-direct {v3, v2}, Lkzv;-><init>(Lpbu;)V

    .line 1589
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->i:Ljava/lang/String;

    .line 1590
    invoke-static {v2, v3}, Llco;->a(Ljava/lang/String;Lkzv;)Lhtc;

    move-result-object v2

    .line 1591
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-interface {v10, v0, v1, v2}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;)V

    move-object v4, v3

    move-wide/from16 v2, v16

    .line 1593
    goto/16 :goto_11

    :cond_33
    sget-object v2, Lpbo;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_34

    .line 1595
    sget-object v2, Lpbo;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpbo;

    .line 1597
    new-instance v3, Lkzr;

    invoke-direct {v3, v2}, Lkzr;-><init>(Lpbo;)V

    .line 1598
    const-string v2, "embed"

    invoke-static {v3}, Lkzr;->a(Lkzr;)[B

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1599
    const-wide/16 v4, 0x40

    or-long v16, v16, v4

    .line 1601
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->i:Ljava/lang/String;

    .line 1602
    invoke-static {v2, v3}, Llco;->a(Ljava/lang/String;Lkzr;)Lhsz;

    move-result-object v2

    .line 1603
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-interface {v10, v0, v1, v2}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;)V

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    .line 1605
    goto/16 :goto_11

    :cond_34
    sget-object v2, Lpdl;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_35

    .line 1607
    new-instance v3, Lkzv;

    sget-object v2, Lpdl;->a:Loxr;

    .line 1608
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpdl;

    invoke-direct {v3, v2}, Lkzv;-><init>(Lpdl;)V

    move-object v4, v3

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1610
    :cond_35
    sget-object v2, Loym;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_36

    .line 1612
    const-string v3, "loc"

    sget-object v2, Loym;->a:Loxr;

    .line 1613
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Loym;

    invoke-static {v2}, Llae;->a(Loym;)[B

    move-result-object v2

    .line 1612
    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1614
    const-wide/16 v2, 0x18

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1617
    :cond_36
    sget-object v2, Lpal;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_37

    .line 1619
    const-string v3, "embed"

    sget-object v2, Lpal;->a:Loxr;

    .line 1620
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Loxu;

    .line 1619
    invoke-static {v2}, Lpal;->a(Loxu;)[B

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1621
    const-wide/16 v2, 0x1000

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1623
    :cond_37
    sget-object v2, Lpbx;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_38

    .line 1626
    sget-object v2, Lpbx;->a:Loxr;

    .line 1627
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpbx;

    .line 1629
    iget-object v3, v2, Lpbx;->c:Loya;

    if-eqz v3, :cond_59

    iget-object v3, v2, Lpbx;->b:Loya;

    if-eqz v3, :cond_59

    .line 1632
    iget-object v3, v2, Lpbx;->c:Loya;

    sget-object v4, Lpbl;->a:Loxr;

    invoke-virtual {v3, v4}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpbl;

    .line 1633
    new-instance v4, Lhta;

    move-object/from16 v0, v23

    invoke-direct {v4, v0, v3}, Lhta;-><init>(Logr;Lpbl;)V

    .line 1634
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-interface {v10, v0, v1, v4}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1635
    const-string v4, "event_id"

    iget-object v3, v3, Lpbl;->g:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1637
    iget-object v2, v2, Lpbx;->b:Loya;

    sget-object v3, Lpbo;->a:Loxr;

    .line 1638
    invoke-virtual {v2, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpbo;

    .line 1640
    const-string v3, "embed"

    .line 1641
    invoke-static {v2}, Lkzr;->a(Lpbo;)[B

    move-result-object v4

    .line 1640
    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1642
    const-wide/16 v4, 0x40

    or-long v4, v4, v16

    .line 1644
    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v3, v2, v10}, Llap;->a(Landroid/content/Context;ILjava/lang/String;Lpbo;Liai;)V

    move-wide v2, v4

    :goto_18
    move-object/from16 v4, v18

    .line 1648
    goto/16 :goto_11

    :cond_38
    sget-object v2, Lpbl;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_39

    .line 1650
    sget-object v2, Lpbl;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpbl;

    .line 1651
    new-instance v3, Lhta;

    move-object/from16 v0, v23

    invoke-direct {v3, v0, v2}, Lhta;-><init>(Logr;Lpbl;)V

    .line 1652
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-interface {v10, v0, v1, v3}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1654
    const-string v3, "event_id"

    iget-object v4, v2, Lpbl;->g:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655
    const-string v3, "embed"

    const-class v4, Lpbl;

    .line 1656
    invoke-static {v2, v4}, Lhyr;->a(Loxu;Ljava/lang/Class;)[B

    move-result-object v2

    .line 1655
    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1657
    const-wide/16 v2, 0x100

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    .line 1659
    goto/16 :goto_11

    :cond_39
    sget-object v2, Lozp;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3a

    .line 1661
    sget-object v2, Lozp;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lozp;

    .line 1662
    new-instance v3, Lhtb;

    move-object/from16 v0, v23

    invoke-direct {v3, v0, v2}, Lhtb;-><init>(Logr;Lozp;)V

    .line 1664
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-interface {v10, v0, v1, v3}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1666
    const-string v3, "event_id"

    iget-object v4, v2, Lozp;->g:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667
    const-string v3, "embed"

    const-class v4, Lozp;

    .line 1668
    invoke-static {v2, v4}, Lhyr;->a(Loxu;Ljava/lang/Class;)[B

    move-result-object v2

    .line 1667
    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1669
    const-wide/32 v2, 0x40000

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    .line 1671
    goto/16 :goto_11

    :cond_3a
    sget-object v2, Lpar;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3b

    .line 1673
    const-string v3, "embed"

    sget-object v2, Lpar;->a:Loxr;

    .line 1674
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpar;

    .line 1673
    new-instance v4, Lkzy;

    invoke-direct {v4, v2}, Lkzy;-><init>(Lpar;)V

    invoke-static {v4}, Lkzy;->a(Lkzy;)[B

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1675
    const-wide/16 v2, 0x400

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1677
    :cond_3b
    sget-object v2, Lpau;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3c

    .line 1679
    const-string v3, "embed"

    sget-object v2, Lpau;->a:Loxr;

    .line 1680
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpau;

    .line 1679
    new-instance v4, Lkzy;

    invoke-direct {v4, v2}, Lkzy;-><init>(Lpau;)V

    invoke-static {v4}, Lkzy;->a(Lkzy;)[B

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1681
    const-wide/16 v2, 0x400

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1683
    :cond_3c
    sget-object v2, Lozi;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3d

    .line 1685
    const-string v3, "embed"

    sget-object v2, Lozi;->a:Loxr;

    .line 1686
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lozi;

    .line 1685
    new-instance v4, Lkzt;

    invoke-direct {v4, v2}, Lkzt;-><init>(Lozi;)V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v6, v4, Lkzt;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Lkzt;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v6, v4, Lkzt;->b:Ljava/util/ArrayList;

    invoke-static {v5, v6}, Lkzt;->b(Ljava/io/DataOutputStream;Ljava/util/List;)V

    iget-object v6, v4, Lkzt;->c:Ljava/util/ArrayList;

    invoke-static {v5, v6}, Lkzt;->b(Ljava/io/DataOutputStream;Ljava/util/List;)V

    iget-object v6, v4, Lkzt;->d:Ljava/util/ArrayList;

    invoke-static {v5, v6}, Lkzt;->b(Ljava/io/DataOutputStream;Ljava/util/List;)V

    iget-object v6, v4, Lkzt;->e:Ljava/lang/String;

    invoke-static {v5, v6}, Lkzt;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget v6, v4, Lkzt;->g:I

    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v6, v4, Lkzt;->f:Ljava/lang/String;

    invoke-static {v5, v6}, Lkzt;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v6, v4, Lkzt;->h:Ljava/lang/String;

    invoke-static {v5, v6}, Lkzt;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-boolean v6, v4, Lkzt;->i:Z

    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-boolean v4, v4, Lkzt;->j:Z

    invoke-virtual {v5, v4}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1687
    const-wide/16 v2, 0x200

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1689
    :cond_3d
    sget-object v2, Lpcx;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3e

    .line 1691
    const-string v3, "embed"

    sget-object v2, Lpcx;->a:Loxr;

    .line 1692
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpcx;

    .line 1691
    invoke-static {v2}, Lkzz;->a(Lpcx;)[B

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1693
    const-wide/32 v2, 0x8000

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1695
    :cond_3e
    sget-object v2, Lpcr;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3f

    .line 1697
    const-string v3, "embed"

    sget-object v2, Lpcr;->a:Loxr;

    .line 1698
    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpcr;

    .line 1697
    invoke-static {v2}, Lkzz;->a(Lpcr;)[B

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1699
    const-wide/32 v2, 0x10000

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1701
    :cond_3f
    sget-object v2, Lozy;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_47

    .line 1703
    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->H:Lohc;

    if-eqz v2, :cond_40

    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->H:Lohc;

    iget-object v2, v2, Lohc;->b:Lohb;

    if-eqz v2, :cond_40

    move-object/from16 v0, v23

    iget-object v2, v0, Logr;->H:Lohc;

    iget-object v2, v2, Lohc;->b:Lohb;

    iget-object v2, v2, Lohb;->b:Loby;

    move-object v6, v2

    .line 1706
    :goto_19
    if-eqz v6, :cond_41

    iget-object v2, v6, Loby;->b:Lobz;

    .line 1709
    :goto_1a
    const-string v25, "embed"

    sget-object v3, Lozy;->a:Loxr;

    .line 1710
    invoke-virtual {v4, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lozy;

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->ab:Llti;

    if-eqz v4, :cond_42

    move-object/from16 v0, v23

    iget-object v4, v0, Logr;->ab:Llti;

    iget v4, v4, Llti;->a:I

    :goto_1b
    if-eqz v6, :cond_43

    iget-object v6, v6, Loby;->a:Ljava/lang/String;

    :goto_1c
    if-eqz v2, :cond_44

    iget-object v7, v2, Lobz;->a:Ljava/lang/String;

    :goto_1d
    if-eqz v2, :cond_45

    iget-object v2, v2, Lobz;->b:Ljava/lang/Long;

    .line 1716
    invoke-static {v2}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v8

    .line 1709
    :goto_1e
    if-nez v3, :cond_46

    const/4 v2, 0x0

    :goto_1f
    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1718
    const-wide/32 v2, 0x80000

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    .line 1720
    goto/16 :goto_11

    .line 1703
    :cond_40
    const/4 v2, 0x0

    move-object v6, v2

    goto :goto_19

    .line 1706
    :cond_41
    const/4 v2, 0x0

    goto :goto_1a

    .line 1710
    :cond_42
    const/4 v4, 0x0

    goto :goto_1b

    :cond_43
    const/4 v6, 0x0

    goto :goto_1c

    :cond_44
    const/4 v7, 0x0

    goto :goto_1d

    .line 1716
    :cond_45
    const-wide/16 v8, 0x0

    goto :goto_1e

    .line 1709
    :cond_46
    new-instance v2, Lkzu;

    invoke-direct/range {v2 .. v9}, Lkzu;-><init>(Lozy;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x200

    invoke-direct {v3, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v5, v2, Lkzu;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v2, Lkzu;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v2, Lkzu;->c:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v2, Lkzu;->d:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget v5, v2, Lkzu;->e:I

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v5, v2, Lkzu;->f:F

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeFloat(F)V

    iget v5, v2, Lkzu;->g:I

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v5, v2, Lkzu;->h:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v2, Lkzu;->i:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v2, Lkzu;->j:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v2, Lkzu;->k:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v2, Lkzu;->l:Ljava/lang/String;

    invoke-static {v4, v5}, Lkzu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-wide v6, v2, Lkzu;->m:J

    invoke-virtual {v4, v6, v7}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    goto :goto_1f

    .line 1720
    :cond_47
    sget-object v2, Lpcd;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4d

    .line 1721
    sget-object v2, Lpcd;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpcd;

    .line 1722
    iget-object v3, v2, Lpcd;->e:[Loya;

    if-eqz v3, :cond_49

    .line 1723
    const/4 v3, 0x0

    iget-object v4, v2, Lpcd;->e:[Loya;

    array-length v5, v4

    move v4, v3

    :goto_20
    if-ge v4, v5, :cond_49

    .line 1724
    iget-object v3, v2, Lpcd;->e:[Loya;

    aget-object v3, v3, v4

    sget-object v6, Lpca;->a:Loxr;

    invoke-virtual {v3, v6}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpca;

    .line 1725
    if-eqz v3, :cond_48

    iget-object v6, v3, Lpca;->e:Loya;

    if-eqz v6, :cond_48

    .line 1726
    iget-object v3, v3, Lpca;->e:Loya;

    sget-object v6, Lpbu;->a:Loxr;

    invoke-virtual {v3, v6}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpbu;

    .line 1728
    new-instance v6, Lkzv;

    invoke-direct {v6, v3}, Lkzv;-><init>(Lpbu;)V

    .line 1729
    iget-object v3, v3, Lpbu;->f:Ljava/lang/String;

    invoke-virtual {v6, v3}, Lkzv;->b(Ljava/lang/String;)V

    .line 1730
    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->i:Ljava/lang/String;

    .line 1731
    invoke-static {v3, v6}, Llco;->a(Ljava/lang/String;Lkzv;)Lhtc;

    move-result-object v3

    .line 1732
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-interface {v10, v0, v1, v3}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1723
    :cond_48
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_20

    .line 1737
    :cond_49
    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->o:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4a

    .line 1738
    const/16 v3, 0x9

    .line 1744
    :goto_21
    const-string v4, "embed"

    if-nez v2, :cond_4c

    const/4 v2, 0x0

    :goto_22
    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1745
    const-wide/32 v2, 0x200000

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    .line 1746
    goto/16 :goto_11

    .line 1739
    :cond_4a
    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->R:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4b

    move-object/from16 v0, v23

    iget-object v3, v0, Logr;->D:Loff;

    if-eqz v3, :cond_4b

    .line 1740
    const/16 v3, 0x8

    goto :goto_21

    .line 1742
    :cond_4b
    const v3, 0x7fffffff

    goto :goto_21

    .line 1744
    :cond_4c
    new-instance v5, Lkzw;

    invoke-direct {v5, v2, v3}, Lkzw;-><init>(Lpcd;I)V

    invoke-static {v5}, Lkzw;->a(Lkzw;)[B

    move-result-object v2

    goto :goto_22

    .line 1746
    :cond_4d
    sget-object v2, Loyf;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4e

    .line 1747
    new-instance v3, Lkzv;

    sget-object v2, Loyf;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Loyf;

    invoke-direct {v3, v2}, Lkzv;-><init>(Loyf;)V

    move-object v4, v3

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1751
    :cond_4e
    const-class v2, Llds;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v3

    .line 1753
    const/4 v2, 0x0

    .line 1754
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_50

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llds;

    .line 1755
    invoke-interface {v2, v4}, Llds;->a(Loya;)Lgs;

    move-result-object v2

    .line 1756
    if-eqz v2, :cond_4f

    :cond_50
    move-object v3, v2

    .line 1757
    if-eqz v3, :cond_51

    .line 1762
    const-string v4, "embed"

    iget-object v2, v3, Lgs;->a:Ljava/lang/Object;

    check-cast v2, [B

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1763
    iget-object v2, v3, Lgs;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    or-long v16, v16, v2

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1764
    :cond_51
    sget-object v2, Lpdg;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_52

    .line 1765
    new-instance v3, Lkzv;

    sget-object v2, Lpdg;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpdg;

    invoke-direct {v3, v2}, Lkzv;-><init>(Lpdg;)V

    move-object v4, v3

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1767
    :cond_52
    sget-object v2, Lpdd;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_53

    .line 1770
    new-instance v3, Lkzv;

    sget-object v2, Lpdd;->a:Loxr;

    invoke-virtual {v4, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpdd;

    invoke-direct {v3, v2}, Lkzv;-><init>(Lpdd;)V

    move-object v4, v3

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1774
    :cond_53
    const-string v2, "EsPostsData"

    const-string v3, "Found an embed we don\'t understand without a THING!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v4, v18

    move-wide/from16 v2, v16

    goto/16 :goto_11

    .line 1786
    :cond_54
    const-wide/16 v6, 0x20

    or-long/2addr v2, v6

    goto/16 :goto_12

    .line 1805
    :cond_55
    const-string v2, "comment"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_14

    .line 1823
    :cond_56
    const-string v2, "activities"

    const-string v4, "unique_activity_id"

    const/4 v5, 0x5

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto/16 :goto_15

    .line 1830
    :cond_57
    const/4 v2, 0x0

    goto/16 :goto_16

    .line 1832
    :cond_58
    if-eqz v11, :cond_1d

    .line 1835
    const-string v2, "activity_comments"

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v23

    iget-object v6, v0, Logr;->i:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_17

    :cond_59
    move-wide/from16 v2, v16

    goto/16 :goto_18

    :cond_5a
    move-wide/from16 v2, v16

    goto/16 :goto_13

    :cond_5b
    move-object v4, v2

    goto/16 :goto_e

    :cond_5c
    move-wide v6, v2

    goto/16 :goto_d

    :cond_5d
    move-wide v14, v2

    goto/16 :goto_3
.end method

.method public static a(Landroid/content/Context;ILhtd;)V
    .locals 5

    .prologue
    .line 2123
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2124
    new-instance v1, Llah;

    iget-object v2, p2, Lhtd;->b:Ljava/lang/String;

    iget v3, p2, Lhtd;->c:I

    iget-boolean v4, p2, Lhtd;->d:Z

    invoke-direct {v1, v2, v3, v4}, Llah;-><init>(Ljava/lang/String;IZ)V

    .line 2127
    iget-object v2, p2, Lhtd;->a:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p0, v0, v2, v1, v3}, Llap;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Llah;Z)V

    .line 2129
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 2323
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2324
    const-string v0, ">>>>> deleteActivity id: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 2327
    :cond_0
    :goto_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2330
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2333
    :try_start_0
    invoke-static {v1, p2}, Llap;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2335
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    .line 2337
    const-string v3, "activity_streams"

    const-string v4, "unique_activity_id IN (SELECT unique_activity_id FROM activities WHERE activity_id = ?)"

    invoke-virtual {v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2340
    const-string v3, "activities"

    const-string v4, "activity_id=?"

    invoke-virtual {v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2342
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2344
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2348
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2349
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2350
    invoke-static {v0}, Llbi;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    .line 2324
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2344
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 2352
    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;J[Lpyk;ILjava/lang/String;Ljava/lang/String;[BLkfp;ZLjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;)V
    .locals 17

    .prologue
    .line 915
    const-class v2, Llfn;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llfn;

    const-wide/16 v4, 0x2

    const-string v3, "updateStreamItems start"

    move/from16 v0, p1

    invoke-interface {v2, v0, v4, v5, v3}, Llfn;->a(IJLjava/lang/String;)V

    .line 917
    if-nez p5, :cond_8

    .line 918
    const/4 v2, 0x0

    new-array v8, v2, [Lpyk;

    .line 922
    :goto_0
    invoke-static/range {p0 .. p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v14

    .line 924
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 927
    :try_start_0
    const-class v2, Lkzl;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lkzl;

    .line 929
    if-nez p7, :cond_0

    .line 930
    move/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v15, v14, v1, v8}, Llap;->a(ILkzl;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lpyk;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    if-nez p11, :cond_0

    .line 997
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1006
    :goto_1
    return-void

    .line 938
    :cond_0
    :try_start_1
    invoke-static/range {p7 .. p8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 939
    const/16 p8, 0x0

    .line 942
    :cond_1
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 943
    array-length v2, v8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x49

    invoke-static/range {p8 .. p8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "updateStreamItems: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " received items: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,new token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,old token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 949
    :cond_2
    if-eqz p10, :cond_3

    if-nez p14, :cond_3

    .line 950
    array-length v2, v8

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Lkfp;->l(I)V

    .line 954
    :cond_3
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 955
    const/4 v12, 0x0

    .line 957
    const-string v2, "activity_streams"

    const-string v3, "stream_key=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v14, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_2
    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v9, p6

    move/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v13, p13

    move-object/from16 v16, p17

    .line 967
    invoke-static/range {v3 .. v16}, Llap;->a(Landroid/content/Context;ILjava/lang/String;J[Lpyk;IZLjava/lang/String;ILjava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Lkzl;Ljava/lang/String;)I

    move-result v2

    .line 971
    new-instance v3, Landroid/content/ContentValues;

    const/4 v4, 0x5

    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 973
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 974
    const-string v4, "token"

    move-object/from16 v0, p8

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    const-string v4, "stream_token"

    move-object/from16 v0, p9

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 976
    const-wide/16 v4, 0x0

    cmp-long v4, p15, v4

    if-lez v4, :cond_4

    .line 977
    const-string v4, "server_timestamp"

    invoke-static/range {p15 .. p16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 979
    :cond_4
    const-string v4, "activity_streams"

    const-string v5, "stream_key=? AND sort_index=0"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-virtual {v14, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 985
    add-int/lit8 v2, v2, -0x1

    .line 986
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 987
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 988
    const-string v4, "last_activity"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 989
    const-string v4, "activity_streams"

    const-string v5, "stream_key=? AND sort_index=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x1

    .line 992
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    .line 989
    invoke-virtual {v14, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 995
    :cond_5
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 997
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1001
    if-nez p14, :cond_6

    .line 1002
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static/range {p2 .. p2}, Llbi;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1004
    :cond_6
    const-class v2, Llfn;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llfn;

    const-wide/16 v4, 0x2

    const-string v3, "updateStreamItems end"

    move/from16 v0, p1

    invoke-interface {v2, v0, v4, v5, v3}, Llfn;->a(IJLjava/lang/String;)V

    goto/16 :goto_1

    .line 960
    :cond_7
    :try_start_2
    const-string v2, "SELECT count(*) FROM activity_streams WHERE stream_key=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v14, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v2

    long-to-int v12, v2

    goto/16 :goto_2

    .line 997
    :catchall_0
    move-exception v2

    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    :cond_8
    move-object/from16 v8, p5

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2099
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1f

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> update post plusone id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2103
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2104
    invoke-static {v0, p2}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;

    move-result-object v1

    .line 2106
    if-nez v1, :cond_2

    .line 2119
    :cond_1
    :goto_0
    return-void

    .line 2110
    :cond_2
    invoke-virtual {v1}, Llah;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2114
    invoke-virtual {v1, p3}, Llah;->a(Ljava/lang/String;)V

    .line 2115
    const/4 v2, 0x0

    invoke-static {p0, v0, p2, v1, v2}, Llap;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Llah;Z)V

    .line 2118
    invoke-static {p0, p1, p2, v1}, Llap;->a(Landroid/content/Context;ILjava/lang/String;Llah;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2219
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x38

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> updateCommentPlusOneId activity id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", comment id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2224
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2225
    invoke-static {v0, p3}, Llap;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;

    move-result-object v1

    .line 2226
    if-nez v1, :cond_2

    .line 2237
    :cond_1
    :goto_0
    return-void

    .line 2230
    :cond_2
    invoke-virtual {v1}, Llah;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2234
    invoke-virtual {v1, p4}, Llah;->a(Ljava/lang/String;)V

    .line 2235
    invoke-static {v0, p3, v1}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Llah;)V

    .line 2236
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;Llah;)V
    .locals 4

    .prologue
    .line 2133
    new-instance v1, Lhtd;

    .line 2134
    invoke-virtual {p3}, Llah;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Llah;->b()I

    move-result v2

    invoke-virtual {p3}, Llah;->c()Z

    move-result v3

    invoke-direct {v1, p2, v0, v2, v3}, Lhtd;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 2135
    const-class v0, Liai;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liai;

    .line 2136
    const-class v2, Llba;

    invoke-interface {v0, p0, p1, v1, v2}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;Ljava/lang/Class;)V

    .line 2137
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Llai;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3089
    .line 3090
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 3091
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3094
    if-nez p3, :cond_1

    const/4 v2, 0x0

    .line 3096
    :goto_0
    :try_start_0
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 3097
    const-string v5, "relateds"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3098
    const-string v2, "activities"

    const-string v5, "activity_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3100
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3105
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3108
    :goto_1
    if-eqz v0, :cond_0

    .line 3109
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 3111
    :cond_0
    return-void

    .line 3095
    :cond_1
    :try_start_1
    invoke-static {p3}, Llai;->a(Llai;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3102
    :catch_0
    move-exception v0

    .line 3103
    :try_start_2
    const-string v2, "EsPostsData"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Could not serialize DbRelateds "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3105
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v0, v1

    .line 3106
    goto :goto_1

    .line 3105
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lofa;)V
    .locals 5

    .prologue
    .line 2504
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2505
    iget-object v0, p3, Lofa;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, ">>>> insertComment: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for activity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2509
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2511
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2513
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2514
    invoke-static {p3, p2, v0}, Llap;->a(Lofa;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 2516
    const-string v2, "activity_comments"

    const-string v3, "activity_id"

    const/4 v4, 0x5

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 2519
    const/4 v0, 0x1

    invoke-static {v1, p2, v0}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 2521
    iget-object v0, p3, Lofa;->g:Ljava/lang/String;

    iget-object v2, p3, Lofa;->b:Ljava/lang/String;

    iget-object v3, p3, Lofa;->m:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2524
    const-class v0, Llbj;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llbj;

    iget-object v2, p3, Lofa;->g:Ljava/lang/String;

    iget-object v3, p3, Lofa;->b:Ljava/lang/String;

    iget-object v4, p3, Lofa;->m:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Llbj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2527
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2529
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2531
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2532
    return-void

    .line 2531
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;Lpbo;Liai;)V
    .locals 10

    .prologue
    .line 1852
    if-eqz p3, :cond_0

    iget-object v0, p3, Lpbo;->e:[Loya;

    if-nez v0, :cond_1

    .line 1912
    :cond_0
    return-void

    .line 1856
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    iget-object v0, p3, Lpbo;->e:[Loya;

    array-length v0, v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1858
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p3, Lpbo;->e:[Loya;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1859
    iget-object v1, p3, Lpbo;->e:[Loya;

    aget-object v1, v1, v0

    sget-object v2, Lpbu;->a:Loxr;

    invoke-virtual {v1, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1858
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1862
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1863
    new-instance v7, Lpbo;

    invoke-direct {v7}, Lpbo;-><init>()V

    .line 1864
    iget-object v0, p3, Lpbo;->b:Ljava/lang/String;

    iput-object v0, v7, Lpbo;->b:Ljava/lang/String;

    .line 1868
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1869
    const/4 v3, 0x1

    .line 1870
    const/4 v2, 0x0

    .line 1871
    const/4 v1, 0x0

    .line 1873
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1874
    const/4 v0, 0x0

    move v4, v0

    :goto_2
    if-ge v4, v8, :cond_5

    .line 1875
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpbu;

    .line 1876
    if-eqz v3, :cond_3

    .line 1877
    iget-object v2, v0, Lpbu;->h:Ljava/lang/String;

    .line 1878
    iget-object v1, v0, Lpbu;->g:Ljava/lang/String;

    .line 1879
    const/4 v3, 0x0

    .line 1882
    :cond_3
    iget-object v9, v0, Lpbu;->h:Ljava/lang/String;

    invoke-static {v2, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v9, v0, Lpbu;->g:Ljava/lang/String;

    .line 1883
    invoke-static {v1, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1884
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1874
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 1888
    :cond_5
    iput-object v2, v7, Lpbo;->d:Ljava/lang/String;

    .line 1889
    iput-object v1, v7, Lpbo;->c:Ljava/lang/String;

    .line 1890
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpbo;->f:Ljava/lang/Integer;

    .line 1891
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Loya;

    .line 1892
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 1893
    new-instance v2, Loya;

    invoke-direct {v2}, Loya;-><init>()V

    aput-object v2, v1, v0

    .line 1894
    aget-object v2, v1, v0

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v8, 0x158

    aput v8, v3, v4

    iput-object v3, v2, Loya;->b:[I

    .line 1895
    aget-object v2, v1, v0

    sget-object v3, Lpbu;->a:Loxr;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 1892
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1897
    :cond_6
    iput-object v1, v7, Lpbo;->e:[Loya;

    .line 1899
    new-instance v0, Lkzr;

    invoke-direct {v0, v7}, Lkzr;-><init>(Lpbo;)V

    .line 1902
    invoke-static {p2, v0}, Llco;->a(Ljava/lang/String;Lkzr;)Lhsz;

    move-result-object v0

    .line 1903
    invoke-interface {p4, p0, p1, v0}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1905
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 1906
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 1908
    const/4 v0, 0x0

    iput-object v0, v7, Lpbo;->d:Ljava/lang/String;

    .line 1909
    const/4 v0, 0x0

    iput-object v0, v7, Lpbo;->c:Ljava/lang/String;

    .line 1910
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpbo;->f:Ljava/lang/Integer;

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[Logr;ILjava/lang/String;Ljava/lang/String;Lkfp;Z)V
    .locals 14

    .prologue
    .line 1072
    if-nez p3, :cond_9

    .line 1073
    const/4 v2, 0x0

    new-array v5, v2, [Logr;

    .line 1076
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1079
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1080
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1083
    if-nez p5, :cond_0

    .line 1084
    :try_start_0
    move-object/from16 v0, p2

    invoke-static {v4, v0, v5}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Logr;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1164
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1170
    :goto_1
    return-void

    .line 1091
    :cond_0
    :try_start_1
    invoke-static/range {p5 .. p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1092
    const/16 p6, 0x0

    .line 1095
    :cond_1
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1096
    array-length v2, v5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x53

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "updateStreamActivities: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " received activities: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,new token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,old token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1102
    :cond_2
    if-eqz p7, :cond_3

    .line 1103
    array-length v2, v5

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Lkfp;->l(I)V

    .line 1107
    :cond_3
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1108
    const/4 v3, 0x0

    .line 1110
    const-string v2, "activity_streams"

    const-string v8, "stream_key=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    invoke-virtual {v4, v2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1120
    :goto_2
    new-instance v8, Landroid/content/ContentValues;

    const/4 v2, 0x5

    invoke-direct {v8, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 1121
    array-length v2, v5

    add-int/2addr v2, v3

    add-int/lit8 v9, v2, -0x1

    .line 1122
    array-length v10, v5

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v10, :cond_5

    aget-object v11, v5, v2

    .line 1123
    const-string v12, "stream_key"

    move-object/from16 v0, p2

    invoke-virtual {v8, v12, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    const-string v12, "unique_activity_id"

    iget-object v13, v11, Logr;->af:Ljava/lang/String;

    invoke-virtual {v8, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1125
    const-string v12, "sort_index"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v8, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1126
    const-string v12, "context_specific_data"

    const/4 v13, 0x0

    .line 1127
    move/from16 v0, p8

    invoke-static {v11, v0, v13}, Lkzp;->a(Logr;ZZ)[B

    move-result-object v11

    .line 1126
    invoke-virtual {v8, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1128
    const-string v11, "stream_fetch_timestamp"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1129
    const-string v11, "activity_streams"

    const-string v12, "unique_activity_id"

    const/4 v13, 0x5

    invoke-virtual {v4, v11, v12, v8, v13}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1132
    add-int/lit8 v3, v3, 0x1

    .line 1122
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1113
    :cond_4
    const-string v2, "SELECT count(*) FROM activity_streams WHERE stream_key=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p2, v3, v8

    invoke-static {v4, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v3, v2

    goto :goto_2

    .line 1136
    :cond_5
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 1137
    const-string v2, "token"

    move-object/from16 v0, p6

    invoke-virtual {v8, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    const-string v2, "activity_streams"

    const-string v3, "stream_key=? AND sort_index=0"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-virtual {v4, v2, v8, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1145
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1146
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 1147
    const-string v2, "last_activity"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1148
    const-string v2, "activity_streams"

    const-string v3, "stream_key=? AND sort_index=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x1

    .line 1151
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    .line 1148
    invoke-virtual {v4, v2, v8, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1154
    :cond_6
    array-length v2, v5

    if-lez v2, :cond_8

    .line 1155
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1156
    array-length v2, v5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x39

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "updateStreamActivities: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " inserting activities:"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1159
    :cond_7
    const/4 v7, 0x0

    move-object v2, p0

    move v3, p1

    move/from16 v6, p4

    invoke-static/range {v2 .. v7}, Llap;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;[Logr;IZ)V

    .line 1162
    :cond_8
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1164
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1168
    invoke-static/range {p2 .. p2}, Llbi;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1169
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_1

    .line 1164
    :catchall_0
    move-exception v2

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    :cond_9
    move-object/from16 v5, p3

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILlae;Ljava/lang/String;ILkfp;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 281
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-static {v7, v7, p2, v8, v9}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v0

    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x41

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "doNearbyActivitiesSync starting sync stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 288
    :cond_0
    new-instance v0, Llaw;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Llaw;-><init>(Landroid/content/Context;ILlae;Ljava/lang/String;ILkfp;)V

    .line 290
    invoke-virtual {v0}, Llaw;->l()V

    .line 292
    iget-object v1, v0, Lkff;->k:Ljava/lang/Exception;

    if-eqz v1, :cond_1

    .line 293
    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    throw v0

    .line 296
    :cond_1
    invoke-virtual {v0}, Llaw;->t()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 297
    new-instance v1, Ljava/io/IOException;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 301
    :cond_2
    invoke-virtual {v0}, Llaw;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmfr;

    iget-object v0, v0, Lmfr;->a:Lnnt;

    .line 302
    invoke-static {v7, v7, p2, v8, v9}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v2

    .line 304
    iget-object v1, v0, Lnnt;->a:Logi;

    iget-object v3, v1, Logi;->a:[Logr;

    const/4 v4, 0x1

    iget-object v0, v0, Lnnt;->a:Logi;

    iget-object v6, v0, Logi;->c:Ljava/lang/String;

    move-object v0, p0

    move v1, p1

    move-object v5, p3

    move-object v7, p5

    invoke-static/range {v0 .. v8}, Llap;->a(Landroid/content/Context;ILjava/lang/String;[Logr;ILjava/lang/String;Ljava/lang/String;Lkfp;Z)V

    .line 307
    return-void
.end method

.method public static a(Landroid/content/Context;I[Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2284
    const-string v0, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2285
    array-length v2, p2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p2, v0

    .line 2287
    const-string v4, "\t"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 2286
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2287
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 2292
    :cond_1
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2293
    const-string v0, ") WHERE activity_id IN("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2294
    const/4 v0, 0x1

    .line 2295
    array-length v4, p2

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, p2, v2

    .line 2296
    if-eqz v0, :cond_2

    move v0, v1

    .line 2301
    :goto_3
    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2295
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2299
    :cond_2
    const/16 v6, 0x2c

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2303
    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2305
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2306
    const-string v0, "UPDATE activities SET activity_flags=(activity_flags | 128"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2308
    invoke-static {v3}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2307
    :goto_4
    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2313
    return-void

    .line 2308
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;I[Logr;IZ)V
    .locals 6

    .prologue
    .line 1186
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1188
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1191
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move-object v3, p2

    move v4, p3

    :try_start_0
    invoke-static/range {v0 .. v5}, Llap;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;[Logr;IZ)V

    .line 1193
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1195
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1198
    if-eqz p4, :cond_0

    .line 1200
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 1201
    aget-object v1, p2, v0

    .line 1202
    iget-object v1, v1, Logr;->i:Ljava/lang/String;

    invoke-static {p0, v1}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1195
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 1205
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Llah;Z)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 2041
    :try_start_0
    invoke-static {p3}, Llah;->a(Llah;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2047
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 2048
    const-string v2, "plus_one_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2049
    const-string v0, "activities"

    const-string v2, "activity_id=?"

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2052
    if-eqz p4, :cond_0

    .line 2053
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2055
    :cond_0
    :goto_0
    return-void

    .line 2042
    :catch_0
    move-exception v0

    .line 2043
    const-string v1, "EsPostsData"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not serialize DbPlusOneData "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2986
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2987
    invoke-static {p1}, Llbi;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2988
    sget-object v1, Llbi;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2989
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 13

    .prologue
    const/4 v0, 0x3

    const/4 v12, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 2752
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 2754
    new-array v11, v12, [Ljava/lang/String;

    aput-object p1, v11, v9

    .line 2756
    const-string v1, "comments_view"

    sget-object v2, Llbh;->a:[Ljava/lang/String;

    const-string v3, "activity_id=? AND (comment_flags&1=0)"

    new-array v4, v12, [Ljava/lang/String;

    aput-object p1, v4, v9

    const-string v7, "created DESC"

    .line 2762
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v6, v5

    .line 2756
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2763
    const-string v1, "activities"

    new-array v2, v12, [Ljava/lang/String;

    const-string v0, "total_comment_count"

    aput-object v0, v2, v9

    const-string v3, "activity_id=?"

    move-object v0, p0

    move-object v4, v11

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2767
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2768
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 2770
    :goto_0
    if-eqz p2, :cond_0

    .line 2771
    const-string v2, "total_comment_count"

    add-int/2addr v0, p2

    .line 2772
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2771
    invoke-virtual {v10, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2775
    :cond_0
    new-instance v0, Llam;

    const/4 v2, 0x3

    invoke-direct {v0, v8, v2}, Llam;-><init>(Landroid/database/Cursor;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2779
    :try_start_1
    invoke-static {v0}, Llam;->a(Llam;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 2783
    :goto_1
    :try_start_2
    const-string v0, "comment"

    invoke-virtual {v10, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2785
    const-string v0, "activities"

    const-string v2, "activity_id=?"

    invoke-virtual {p0, v0, v10, v2, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2788
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2789
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2790
    return-void

    .line 2788
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2789
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2781
    :catch_0
    move-exception v0

    goto :goto_1

    :cond_1
    move v0, v9

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2463
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2492
    :cond_0
    :goto_0
    return-void

    .line 2467
    :cond_1
    invoke-static {p3}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2468
    const-string v1, "activity_contacts"

    sget-object v2, Llap;->d:[Ljava/lang/String;

    const-string v3, "gaia_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2471
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2472
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2473
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2474
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2475
    invoke-static {v8, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 2480
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2483
    const-string v0, "EsPostsData"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> Inserting gaiaId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2487
    :cond_3
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v9}, Landroid/content/ContentValues;-><init>(I)V

    .line 2488
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2489
    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490
    const-string v1, "avatar_url"

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2491
    const-string v1, "activity_contacts"

    invoke-virtual {p0, v1, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_0

    .line 2480
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Llah;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 2165
    :try_start_0
    invoke-static {p2}, Llah;->a(Llah;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2171
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 2172
    const-string v2, "plus_one_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2173
    const-string v0, "activity_comments"

    const-string v2, "comment_id=?"

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2175
    :goto_0
    return-void

    .line 2166
    :catch_0
    move-exception v0

    .line 2167
    const-string v1, "EsPostsData"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not serialize DbPlusOneData "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lofa;ZLlbj;)V
    .locals 10

    .prologue
    .line 2546
    if-nez p3, :cond_0

    .line 2547
    const-string v0, "activity_comments"

    const-string v1, "activity_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2551
    :cond_0
    if-eqz p2, :cond_1

    array-length v0, p2

    if-nez v0, :cond_2

    .line 2577
    :cond_1
    return-void

    .line 2556
    :cond_2
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2557
    invoke-static {p2}, Llap;->a([Lofa;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lofa;

    .line 2558
    const-string v3, "EsPostsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2559
    iget-object v3, v0, Lofa;->f:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lofa;->g:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lofa;->c:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lofa;->e:Ljava/lang/Long;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x48

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "    >>>>> insertComments comment id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", author id: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", created: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2565
    :cond_3
    invoke-static {v0, p1, v1}, Llap;->a(Lofa;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 2567
    const-string v3, "activity_comments"

    const-string v4, "activity_id"

    const/4 v5, 0x5

    invoke-virtual {p0, v3, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 2571
    iget-object v3, v0, Lofa;->g:Ljava/lang/String;

    iget-object v4, v0, Lofa;->b:Ljava/lang/String;

    iget-object v5, v0, Lofa;->m:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2574
    iget-object v3, v0, Lofa;->g:Ljava/lang/String;

    iget-object v4, v0, Lofa;->b:Ljava/lang/String;

    iget-object v0, v0, Lofa;->m:Ljava/lang/String;

    invoke-interface {p4, p0, v3, v4, v0}, Llbj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0
.end method

.method private static a(Lofa;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 2802
    invoke-virtual {p2}, Landroid/content/ContentValues;->clear()V

    .line 2804
    iget-object v2, p0, Lofa;->f:Ljava/lang/String;

    .line 2805
    const-wide/16 v0, 0x0

    .line 2806
    const-string v3, "activity_id"

    invoke-virtual {p2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2807
    const-string v3, "comment_id"

    invoke-virtual {p2, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2808
    const-string v2, "author_id"

    iget-object v3, p0, Lofa;->g:Ljava/lang/String;

    invoke-virtual {p2, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2809
    iget-object v2, p0, Lofa;->n:Lpeo;

    invoke-static {v2}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 2810
    const-string v3, "content"

    invoke-static {v2}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v2

    invoke-virtual {p2, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2811
    const-string v2, "created"

    iget-object v3, p0, Lofa;->e:Ljava/lang/Long;

    invoke-virtual {p2, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2812
    iget-object v2, p0, Lofa;->d:Logq;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lofa;->d:Logq;

    iget-object v2, v2, Logq;->a:Ljava/lang/Boolean;

    .line 2813
    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2814
    const-wide/16 v0, 0x2

    .line 2816
    :cond_0
    const-string v2, "comment_flags"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2817
    iget-object v0, p0, Lofa;->l:Loae;

    if-eqz v0, :cond_1

    .line 2819
    :try_start_0
    iget-object v0, p0, Lofa;->l:Loae;

    invoke-static {v0}, Llah;->a(Loae;)[B

    move-result-object v0

    .line 2820
    const-string v1, "plus_one_data"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2825
    :cond_1
    :goto_0
    return-void

    .line 2822
    :catch_0
    move-exception v0

    const-string v0, "plus_one_data"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(ILkzl;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lpyk;)Z
    .locals 11

    .prologue
    const/16 v3, 0x1e

    const/4 v5, 0x0

    const/4 v1, 0x3

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 745
    const-string v0, "EsPostsData"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 746
    const-string v0, "haveStreamItemsChanged: Comparing items in stream with key: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 748
    :goto_0
    array-length v0, p4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "\tNew stream items: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 751
    :cond_0
    array-length v0, p4

    if-nez v0, :cond_2

    move v0, v9

    .line 793
    :goto_1
    return v0

    .line 746
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 756
    :cond_2
    const-string v1, "activity_streams"

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "unique_activity_id"

    aput-object v0, v2, v10

    const-string v3, "stream_key=?"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p3, v4, v10

    const-string v7, "sort_index ASC"

    array-length v0, p4

    .line 760
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    move-object v0, p2

    move-object v6, v5

    .line 756
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 762
    :try_start_0
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 763
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "\tOld stream items: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 766
    :cond_3
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 768
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 769
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 770
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 790
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    move v1, v10

    .line 774
    :goto_3
    :try_start_1
    array-length v0, p4

    if-ge v1, v0, :cond_c

    .line 775
    aget-object v0, p4, v1

    .line 776
    if-nez v0, :cond_6

    move-object v0, v5

    .line 777
    :goto_4
    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 778
    const-string v1, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 779
    const-string v1, "\t=====> New stream item was not found: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 782
    :cond_5
    :goto_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move v0, v9

    goto/16 :goto_1

    .line 776
    :cond_6
    :try_start_2
    iget v4, v0, Lpyk;->b:I

    invoke-interface {p1, v4}, Lkzl;->a(I)Lkzk;

    move-result-object v4

    if-nez v4, :cond_7

    const-string v0, "EsPostsData"

    const-string v4, "unknown stream type"

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v5

    goto :goto_4

    :cond_7
    invoke-interface {v4, p0, v0}, Lkzk;->a(ILpyk;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 779
    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 784
    :cond_9
    const-string v4, "EsPostsData"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 785
    const-string v4, "\tFound in db: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 774
    :cond_a
    :goto_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 785
    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    .line 790
    :cond_c
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 793
    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZJ[Ljava/lang/String;)Z
    .locals 26

    .prologue
    .line 438
    const/16 v19, 0x0

    const/16 v20, 0x1

    move-object/from16 v6, p0

    move/from16 v7, p1

    move/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    move/from16 v15, p9

    move-object/from16 v16, p10

    move-object/from16 v17, p11

    move-object/from16 v18, p12

    move/from16 v21, p13

    move-wide/from16 v22, p14

    move-object/from16 v24, p16

    .line 439
    invoke-static/range {v6 .. v24}, Llap;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZZZJ[Ljava/lang/String;)Lkff;

    move-result-object v6

    check-cast v6, Llav;

    .line 445
    invoke-virtual {v6}, Llav;->t()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 446
    const/4 v6, 0x0

    .line 476
    :goto_0
    return v6

    .line 449
    :cond_0
    move/from16 v0, p2

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, p3

    move/from16 v4, p6

    invoke-static {v0, v1, v2, v3, v4}, Llap;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 450
    invoke-virtual {v6}, Llav;->D()Loxu;

    move-result-object v6

    check-cast v6, Lmbf;

    iget-object v6, v6, Lmbf;->a:Loda;

    .line 451
    if-eqz p13, :cond_3

    .line 452
    iget-object v8, v6, Loda;->a:Logi;

    iget-object v8, v8, Logi;->b:[Lpyk;

    array-length v8, v8

    if-nez v8, :cond_1

    .line 453
    const/4 v6, 0x0

    goto :goto_0

    .line 458
    :cond_1
    const-string v8, "EsPostsData"

    const/4 v9, 0x4

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 459
    const-string v8, "prefetchNewStreamActivities:checkIfNewPosts = "

    iget-object v9, v6, Loda;->a:Logi;

    iget-object v9, v9, Logi;->b:[Lpyk;

    .line 460
    invoke-static {v9}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 459
    :cond_2
    :goto_1
    sget-object v8, Llap;->g:Lloy;

    .line 465
    :cond_3
    invoke-static {}, Llay;->a()Llay;

    move-result-object v8

    new-instance v9, Llaz;

    invoke-direct {v9, v6}, Llaz;-><init>(Loda;)V

    move/from16 v0, p1

    invoke-virtual {v8, v0, v7, v9}, Llay;->a(ILjava/lang/String;Llaz;)V

    .line 476
    const/4 v6, 0x1

    goto :goto_0

    .line 460
    :cond_4
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[Logr;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 579
    if-eqz p3, :cond_0

    array-length v0, p3

    if-nez v0, :cond_2

    .line 580
    :cond_0
    const/4 v0, 0x0

    .line 590
    :cond_1
    :goto_0
    invoke-static {p0, p1, p2, v0}, Llap;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 582
    :cond_2
    aget-object v0, p3, v4

    iget-object v0, v0, Logr;->af:Ljava/lang/String;

    .line 583
    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 584
    const-string v1, "Stream: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 585
    :goto_1
    const-string v1, "\tAuthor: "

    aget-object v2, p3, v4

    iget-object v2, v2, Logr;->e:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 586
    :goto_2
    const-string v1, "\tCollapsed Body: "

    aget-object v2, p3, v4

    iget-object v2, v2, Logr;->m:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 587
    :goto_3
    const-string v1, "\tTitle: "

    aget-object v2, p3, v4

    iget-object v2, v2, Logr;->n:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 584
    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 585
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 586
    :cond_5
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 587
    :cond_6
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Logr;)Z
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/16 v3, 0x19

    const/4 v1, 0x3

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 675
    const-string v0, "EsPostsData"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    const-string v0, "haveUpdatesStreamChanged: Comparing updates in stream with key: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 678
    :goto_0
    array-length v0, p2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "\tNew updates: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 681
    :cond_0
    array-length v0, p2

    if-nez v0, :cond_2

    move v0, v9

    .line 722
    :goto_1
    return v0

    .line 676
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 686
    :cond_2
    const-string v1, "activity_streams"

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "unique_activity_id"

    aput-object v0, v2, v10

    const-string v3, "stream_key=? AND unique_activity_id NOT LIKE \'~typeprefix~%\'"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v10

    const-string v7, "sort_index ASC"

    array-length v0, p2

    .line 690
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v6, v5

    .line 686
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 692
    :try_start_0
    const-string v0, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 693
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "\tOld updates: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 696
    :cond_3
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 698
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 699
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 700
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 719
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 704
    :cond_4
    :try_start_1
    array-length v3, p2

    move v0, v10

    :goto_3
    if-ge v0, v3, :cond_a

    aget-object v4, p2, v0

    .line 705
    iget-object v4, v4, Logr;->af:Ljava/lang/String;

    .line 706
    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 707
    const-string v0, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 708
    const-string v0, "\t=====> New update was not found: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 711
    :cond_5
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v9

    goto/16 :goto_1

    .line 708
    :cond_6
    :try_start_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 713
    :cond_7
    const-string v5, "EsPostsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 714
    const-string v5, "\tFound in db: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 704
    :cond_8
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 714
    :cond_9
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 719
    :cond_a
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 722
    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;)[I
    .locals 4

    .prologue
    .line 236
    sget-object v0, Llap;->f:[I

    if-nez v0, :cond_0

    .line 237
    const-class v0, Lkzl;

    .line 238
    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    invoke-interface {v0}, Lkzl;->b()Ljava/util/ArrayList;

    move-result-object v2

    .line 239
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    const-string v0, "EsPostsData"

    const-string v1, "No registered stream item extensions"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :cond_0
    sget-object v0, Llap;->f:[I

    return-object v0

    .line 242
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [I

    sput-object v0, Llap;->f:[I

    .line 243
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Llap;->f:[I

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 244
    sget-object v3, Llap;->f:[I

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 243
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2140
    const-string v1, "activity_comments"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "plus_one_data"

    aput-object v0, v2, v6

    const-string v3, "comment_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2143
    if-nez v1, :cond_0

    .line 2157
    :goto_0
    return-object v5

    .line 2147
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2148
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2150
    new-instance v5, Llah;

    invoke-direct {v5}, Llah;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2157
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2152
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Llah;->a([B)Llah;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 2157
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2155
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 2708
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2710
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v0, v2

    .line 2714
    :try_start_0
    const-string v2, "SELECT activity_id FROM activity_comments WHERE comment_id = ?"

    .line 2715
    invoke-static {v1, v2, v0}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2723
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2724
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, ">>>> deleteComment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2730
    :try_start_1
    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 2731
    const-string v3, "comment_id IN("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2732
    invoke-static {p2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2733
    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2735
    const-string v3, "activity_comments"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2737
    const/4 v2, -0x1

    invoke-static {v1, v0, v2}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 2739
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2741
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2744
    if-eqz v0, :cond_1

    .line 2745
    invoke-static {p0, v0}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2747
    :cond_1
    :goto_0
    return-void

    .line 2717
    :catch_0
    move-exception v0

    const-string v0, "EsPostsData"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2718
    const-string v0, "WARNING: could not find photo for the comment: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2741
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Lofa;)V
    .locals 5

    .prologue
    .line 2607
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2608
    iget-object v0, p3, Lofa;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x21

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, ">>>> editComment: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for activity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2611
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2613
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2615
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2616
    invoke-static {p3, p2, v0}, Llap;->a(Lofa;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 2618
    iget-object v2, p3, Lofa;->f:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuffer;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v4, "comment_id IN("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {v2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v2, 0x29

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v2, "activity_comments"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2620
    iget-object v0, p3, Lofa;->g:Ljava/lang/String;

    iget-object v2, p3, Lofa;->b:Ljava/lang/String;

    iget-object v3, p3, Lofa;->m:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2623
    const-class v0, Llbj;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llbj;

    iget-object v2, p3, Lofa;->g:Ljava/lang/String;

    iget-object v3, p3, Lofa;->b:Ljava/lang/String;

    iget-object v4, p3, Lofa;->m:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Llbj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2626
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2628
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2630
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2631
    return-void

    .line 2630
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2249
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250
    const-string v0, ">>>>> muteActivity id: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 2253
    :cond_0
    :goto_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2255
    if-eqz p3, :cond_2

    .line 2256
    const-string v0, " | 64"

    .line 2261
    :goto_1
    const-string v2, "UPDATE activities SET activity_flags=(activity_flags"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ") WHERE activity_id=\'"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\'"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2265
    invoke-static {v1, p2}, Llap;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2266
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2267
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2268
    invoke-static {v0}, Llbi;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_2

    .line 2250
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2258
    :cond_2
    const-string v0, " & -65"

    goto/16 :goto_1

    .line 2272
    :cond_3
    invoke-static {p2}, Llbi;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2273
    return-void
.end method

.method public static b(Landroid/content/Context;I[Logr;IZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1221
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1223
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1225
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1226
    invoke-static {v2, p2}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;[Logr;)Ljava/util/HashMap;

    move-result-object v3

    move v0, v6

    .line 1227
    :goto_0
    array-length v4, p2

    if-ge v0, v4, :cond_1

    .line 1228
    aget-object v4, p2, v0

    .line 1229
    iget-object v5, v4, Logr;->af:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1230
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1227
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1236
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Logr;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Logr;

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v4, p3

    .line 1235
    invoke-static/range {v0 .. v5}, Llap;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;[Logr;IZ)V

    .line 1239
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1241
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1244
    if-eqz p4, :cond_2

    move v0, v6

    .line 1246
    :goto_1
    array-length v1, p2

    if-ge v0, v1, :cond_2

    .line 1247
    aget-object v1, p2, v0

    .line 1248
    iget-object v1, v1, Logr;->i:Ljava/lang/String;

    invoke-static {p0, v1}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1246
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1241
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 1251
    :cond_2
    return-void
.end method

.method private static b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 606
    invoke-static {p0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 609
    const-string v1, "activity_streams"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "unique_activity_id"

    aput-object v3, v2, v10

    const-string v3, "stream_key=? AND unique_activity_id NOT LIKE \'~typeprefix~%\'"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p2, v4, v10

    const-string v7, "sort_index ASC"

    sget-object v6, Llap;->b:Ljava/lang/Integer;

    .line 613
    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v6, v5

    .line 609
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 615
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 616
    const-string v0, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 617
    const-string v0, "hasStreamChanged: no local activities, server activity: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 621
    :cond_0
    :goto_0
    if-eqz p3, :cond_3

    move v0, v9

    :goto_1
    move v9, v0

    .line 656
    :cond_1
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 659
    return v9

    .line 617
    :cond_2
    :try_start_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 656
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    move v0, v10

    .line 621
    goto :goto_1

    .line 622
    :cond_4
    if-eqz p3, :cond_1

    .line 631
    :cond_5
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 632
    const-string v0, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 633
    const-string v0, "\t"

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 635
    :cond_6
    :goto_3
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v9

    .line 637
    :goto_4
    if-eqz v0, :cond_a

    .line 642
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 643
    const-string v2, "hasStreamChanged: no change, found: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 653
    :cond_7
    :goto_5
    if-eqz v0, :cond_1

    move v9, v10

    goto :goto_2

    .line 633
    :cond_8
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 643
    :cond_9
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 647
    :cond_a
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 648
    const-string v2, "hasStreamChanged: changed, not found: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_5

    :cond_b
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    :cond_c
    move v0, v10

    goto :goto_4
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 3000
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 3002
    const-string v2, "activity_streams"

    sget-object v3, Llat;->a:[Ljava/lang/String;

    const-string v4, "unique_activity_id IN (SELECT unique_activity_id FROM activities WHERE activity_id = ?)"

    new-array v5, v1, [Ljava/lang/String;

    aput-object p1, v5, v0

    move-object v0, p0

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3005
    if-nez v1, :cond_0

    move-object v0, v10

    .line 3017
    :goto_0
    return-object v0

    .line 3010
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3011
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 3014
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v10

    .line 3017
    goto :goto_0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 2364
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2d

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> updateActivityIsSpam id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isSpam: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2368
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2370
    if-eqz p3, :cond_1

    .line 2371
    const-string v0, " | 2"

    .line 2376
    :goto_0
    const-string v2, "UPDATE activities SET activity_flags=(activity_flags"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ") WHERE activity_id=\'"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\'"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2379
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2380
    return-void

    .line 2373
    :cond_1
    const-string v0, " & -3"

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 2837
    invoke-static {p0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2839
    const-string v1, "activities"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "plus_one_data"

    aput-object v3, v2, v8

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v8

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2844
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2845
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Llah;->a([B)Llah;

    move-result-object v0

    .line 2846
    if-eqz v0, :cond_0

    .line 2847
    invoke-virtual {v0}, Llah;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2851
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2854
    :goto_0
    return v0

    .line 2851
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 2854
    goto :goto_0

    .line 2851
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 2384
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3b

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> updateActivityCanComment id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " canViewerComment: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2389
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2391
    if-eqz p3, :cond_1

    .line 2392
    const-string v0, " | 4"

    .line 2397
    :goto_0
    const-string v2, "UPDATE activities SET activity_flags=(activity_flags"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ") WHERE activity_id=\'"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\'"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2400
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2401
    return-void

    .line 2394
    :cond_1
    const-string v0, " & -5"

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2868
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2872
    :cond_0
    :goto_0
    return v0

    .line 2871
    :cond_1
    invoke-static {p0, p1, p2}, Llap;->e(Landroid/content/Context;ILjava/lang/String;)Lkzs;

    move-result-object v1

    .line 2872
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lkzs;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;ILjava/lang/String;)Lkzs;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 2886
    invoke-static {p0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2887
    const-string v1, "activities"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "embed_appinvite"

    aput-object v3, v2, v6

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2892
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2893
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lkzs;->a([B)Lkzs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 2896
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2899
    :goto_0
    return-object v5

    .line 2896
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static e(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 2405
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2406
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x31

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> updateActivityCanShare id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " canShare: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2410
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2412
    if-eqz p3, :cond_1

    .line 2413
    const-string v0, " | 8"

    .line 2418
    :goto_0
    const-string v2, "UPDATE activities SET activity_flags=(activity_flags"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ") WHERE activity_id=\'"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\'"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2421
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2422
    return-void

    .line 2415
    :cond_1
    const-string v0, " & -9"

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2910
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2912
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2913
    const-string v2, "stream_key IN("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2914
    invoke-static {p2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2915
    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2917
    const-string v2, "activity_streams"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2918
    invoke-static {p2}, Llbi;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2919
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2921
    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2922
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x31

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "deleteActivityStream deleted streams: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2924
    :cond_0
    return-void
.end method

.method public static f(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 2434
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3d

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> updateActivityIsStrangerPost id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isStrangerPost: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2439
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2441
    if-eqz p3, :cond_1

    .line 2442
    const-string v0, " | 32"

    .line 2447
    :goto_0
    const-string v2, "UPDATE activities SET activity_flags=(activity_flags"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ") WHERE author_id=\'"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\'"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2450
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2451
    return-void

    .line 2444
    :cond_1
    const-string v0, " & -33"

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 2644
    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2645
    const-string v0, ">>>> updateCommentFlagged: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 2648
    :cond_0
    :goto_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2651
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object p2, v0, v3

    .line 2653
    :try_start_0
    const-string v2, "SELECT activity_id FROM activity_comments WHERE comment_id = ?"

    invoke-static {v1, v2, v0}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2662
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2665
    if-eqz p3, :cond_4

    .line 2666
    :try_start_1
    const-string v0, " | 1"

    .line 2670
    :goto_1
    const-string v3, "UPDATE activity_comments SET comment_flags=(comment_flags"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ") WHERE comment_id=\'"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\'"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2676
    const/4 v0, 0x0

    invoke-static {v1, v2, v0}, Llap;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 2677
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2679
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2681
    if-eqz v2, :cond_1

    .line 2682
    invoke-static {p0, v2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2684
    :cond_1
    :goto_2
    return-void

    .line 2645
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2656
    :catch_0
    move-exception v0

    const-string v0, "EsPostsData"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2657
    const-string v0, "WARNING: could not find activity id for the comment: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2

    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 2668
    :cond_4
    :try_start_2
    const-string v0, " & -2"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 2679
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static h(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v1, 0x1

    .line 3030
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3032
    const-string v2, "activity_streams"

    sget-object v3, Llas;->a:[Ljava/lang/String;

    const-string v4, "unique_activity_id IN (SELECT unique_activity_id FROM activities WHERE activity_id = ?)"

    new-array v5, v1, [Ljava/lang/String;

    aput-object p2, v5, v10

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 3036
    if-nez v3, :cond_1

    .line 3077
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v2, v10

    .line 3043
    :cond_2
    :goto_1
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3044
    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    .line 3046
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 3048
    invoke-static {v4}, Lkzp;->a([B)Lkzp;

    move-result-object v4

    .line 3049
    if-eqz v4, :cond_2

    .line 3050
    invoke-virtual {v4}, Lkzp;->l()Llao;

    move-result-object v6

    .line 3051
    if-eqz v6, :cond_2

    .line 3052
    invoke-virtual {v6, p3}, Llao;->a(Z)V

    .line 3053
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3055
    :try_start_1
    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 3056
    const-string v7, "context_specific_data"

    .line 3057
    invoke-static {v4}, Lkzp;->a(Lkzp;)[B

    move-result-object v4

    .line 3056
    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3058
    const-string v4, "activity_streams"

    const-string v7, "stream_key=? AND unique_activity_id IN (SELECT unique_activity_id FROM activities WHERE activity_id = ?)"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v5, 0x1

    aput-object p2, v8, v5

    invoke-virtual {v0, v4, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3061
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3066
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v2, v1

    .line 3067
    goto :goto_1

    .line 3066
    :catch_0
    move-exception v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3072
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 3066
    :catchall_1
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3072
    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 3074
    if-eqz v2, :cond_0

    .line 3075
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v10, 0x0

    .line 3123
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3125
    const-string v2, "activity_streams"

    sget-object v3, Llas;->a:[Ljava/lang/String;

    const-string v4, "unique_activity_id IN (SELECT unique_activity_id FROM activities WHERE activity_id = ?)"

    new-array v5, v1, [Ljava/lang/String;

    aput-object p2, v5, v10

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 3129
    if-nez v4, :cond_1

    .line 3181
    :cond_0
    :goto_0
    return-void

    .line 3139
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    move v2, v10

    move v3, v10

    .line 3140
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 3141
    add-int/lit8 v3, v3, 0x1

    .line 3142
    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 3144
    const/4 v6, 0x0

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 3146
    invoke-static {v5}, Lkzp;->a([B)Lkzp;

    move-result-object v5

    .line 3147
    if-eqz v5, :cond_3

    .line 3148
    invoke-virtual {v5}, Lkzp;->n()Llac;

    move-result-object v7

    .line 3149
    if-eqz v7, :cond_2

    .line 3150
    invoke-virtual {v7, p3}, Llac;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3152
    :try_start_1
    new-instance v7, Landroid/content/ContentValues;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/content/ContentValues;-><init>(I)V

    .line 3153
    const-string v8, "context_specific_data"

    .line 3154
    invoke-static {v5}, Lkzp;->a(Lkzp;)[B

    move-result-object v5

    .line 3153
    invoke-virtual {v7, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3155
    const-string v5, "Updating activity to %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v8, v9

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 3156
    const-string v5, "activity_streams"

    const-string v8, "stream_key=? AND unique_activity_id IN (SELECT unique_activity_id FROM activities WHERE activity_id = ?)"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v6, v9, v11

    const/4 v6, 0x1

    aput-object p2, v9, v6

    invoke-virtual {v0, v5, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3159
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3164
    :cond_2
    :try_start_2
    const-string v5, "EsPostsData"

    const-string v6, "Failed to update inferred post because DbInferredGraphPost is null."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3175
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3176
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v1

    .line 3167
    :cond_3
    :try_start_3
    const-string v5, "EsPostsData"

    const-string v6, "Failed to update inferred post because DbContextSpecificData is null."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3170
    :cond_4
    if-ne v2, v3, :cond_5

    .line 3172
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3175
    :goto_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3176
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 3178
    if-eqz v1, :cond_0

    .line 3179
    invoke-static {p0, p2}, Llap;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 3162
    :catch_0
    move-exception v5

    goto :goto_1

    :cond_5
    move v1, v10

    goto :goto_2
.end method
