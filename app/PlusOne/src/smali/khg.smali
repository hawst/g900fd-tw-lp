.class public final Lkhg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Los;

.field private b:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Los;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lkhg;->a:Los;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/Switch;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lkhg;->b:Landroid/widget/Switch;

    return-object v0
.end method

.method public a(Lcom/google/android/libraries/social/settings/CheckBoxPreference;Z)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 35
    if-eqz p1, :cond_1

    .line 36
    if-nez p2, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_2

    .line 39
    :cond_0
    new-instance v0, Landroid/widget/Switch;

    iget-object v1, p0, Lkhg;->a:Los;

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkhg;->b:Landroid/widget/Switch;

    .line 44
    :goto_0
    iget-object v0, p0, Lkhg;->a:Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lkhg;->a:Los;

    invoke-virtual {v1}, Los;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d02a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 47
    iget-object v2, p0, Lkhg;->b:Landroid/widget/Switch;

    invoke-virtual {v2, v3, v3, v1, v3}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 48
    invoke-virtual {v0, v5, v5}, Loo;->a(II)V

    .line 50
    iget-object v1, p0, Lkhg;->b:Landroid/widget/Switch;

    new-instance v2, Lop;

    const v3, 0x800015

    invoke-direct {v2, v4, v4, v3}, Lop;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Loo;->a(Landroid/view/View;Lop;)V

    .line 55
    iget-object v0, p0, Lkhg;->b:Landroid/widget/Switch;

    new-instance v1, Lkhh;

    invoke-direct {v1, p1}, Lkhh;-><init>(Lcom/google/android/libraries/social/settings/CheckBoxPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 72
    iget-object v0, p0, Lkhg;->b:Landroid/widget/Switch;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 74
    :cond_1
    return-void

    .line 41
    :cond_2
    iget-object v0, p0, Lkhg;->a:Los;

    invoke-virtual {v0}, Los;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040032

    const/4 v2, 0x0

    .line 42
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lkhg;->b:Landroid/widget/Switch;

    goto :goto_0
.end method
