.class public final Locw;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 346
    invoke-direct {p0}, Loxq;-><init>()V

    .line 349
    const/high16 v0, -0x80000000

    iput v0, p0, Locw;->a:I

    .line 346
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 373
    const/4 v0, 0x0

    .line 374
    iget v1, p0, Locw;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 375
    const/4 v0, 0x1

    iget v1, p0, Locw;->a:I

    .line 376
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 378
    :cond_0
    iget-object v1, p0, Locw;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 379
    const/4 v1, 0x2

    iget-object v2, p0, Locw;->b:Ljava/lang/Integer;

    .line 380
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 382
    :cond_1
    iget-object v1, p0, Locw;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 383
    const/4 v1, 0x3

    iget-object v2, p0, Locw;->c:Ljava/lang/Integer;

    .line 384
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 386
    :cond_2
    iget-object v1, p0, Locw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 387
    iput v0, p0, Locw;->ai:I

    .line 388
    return v0
.end method

.method public a(Loxn;)Locw;
    .locals 2

    .prologue
    .line 396
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 397
    sparse-switch v0, :sswitch_data_0

    .line 401
    iget-object v1, p0, Locw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 402
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Locw;->ah:Ljava/util/List;

    .line 405
    :cond_1
    iget-object v1, p0, Locw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    :sswitch_0
    return-object p0

    .line 412
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 413
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 415
    :cond_2
    iput v0, p0, Locw;->a:I

    goto :goto_0

    .line 417
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Locw;->a:I

    goto :goto_0

    .line 422
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Locw;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 426
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Locw;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 397
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 358
    iget v0, p0, Locw;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 359
    const/4 v0, 0x1

    iget v1, p0, Locw;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 361
    :cond_0
    iget-object v0, p0, Locw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 362
    const/4 v0, 0x2

    iget-object v1, p0, Locw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 364
    :cond_1
    iget-object v0, p0, Locw;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 365
    const/4 v0, 0x3

    iget-object v1, p0, Locw;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 367
    :cond_2
    iget-object v0, p0, Locw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 369
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 342
    invoke-virtual {p0, p1}, Locw;->a(Loxn;)Locw;

    move-result-object v0

    return-object v0
.end method
