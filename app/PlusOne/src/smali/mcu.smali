.class public final Lmcu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnle;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 241
    invoke-direct {p0}, Loxq;-><init>()V

    .line 244
    iput-object v0, p0, Lmcu;->apiHeader:Llyq;

    .line 247
    iput-object v0, p0, Lmcu;->a:Lnle;

    .line 241
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x0

    .line 265
    iget-object v1, p0, Lmcu;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 266
    const/4 v0, 0x1

    iget-object v1, p0, Lmcu;->apiHeader:Llyq;

    .line 267
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 269
    :cond_0
    iget-object v1, p0, Lmcu;->a:Lnle;

    if-eqz v1, :cond_1

    .line 270
    const/4 v1, 0x2

    iget-object v2, p0, Lmcu;->a:Lnle;

    .line 271
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_1
    iget-object v1, p0, Lmcu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Lmcu;->ai:I

    .line 275
    return v0
.end method

.method public a(Loxn;)Lmcu;
    .locals 2

    .prologue
    .line 283
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 284
    sparse-switch v0, :sswitch_data_0

    .line 288
    iget-object v1, p0, Lmcu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 289
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcu;->ah:Ljava/util/List;

    .line 292
    :cond_1
    iget-object v1, p0, Lmcu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    :sswitch_0
    return-object p0

    .line 299
    :sswitch_1
    iget-object v0, p0, Lmcu;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 300
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmcu;->apiHeader:Llyq;

    .line 302
    :cond_2
    iget-object v0, p0, Lmcu;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 306
    :sswitch_2
    iget-object v0, p0, Lmcu;->a:Lnle;

    if-nez v0, :cond_3

    .line 307
    new-instance v0, Lnle;

    invoke-direct {v0}, Lnle;-><init>()V

    iput-object v0, p0, Lmcu;->a:Lnle;

    .line 309
    :cond_3
    iget-object v0, p0, Lmcu;->a:Lnle;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 284
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lmcu;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 253
    const/4 v0, 0x1

    iget-object v1, p0, Lmcu;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 255
    :cond_0
    iget-object v0, p0, Lmcu;->a:Lnle;

    if-eqz v0, :cond_1

    .line 256
    const/4 v0, 0x2

    iget-object v1, p0, Lmcu;->a:Lnle;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 258
    :cond_1
    iget-object v0, p0, Lmcu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 260
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0, p1}, Lmcu;->a(Loxn;)Lmcu;

    move-result-object v0

    return-object v0
.end method
