.class public final Lmpu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lmpm;

.field public d:Lock;

.field private e:Ljava/lang/String;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    iput-object v0, p0, Lmpu;->c:Lmpm;

    .line 22
    iput-object v0, p0, Lmpu;->d:Lock;

    .line 25
    const/high16 v0, -0x80000000

    iput v0, p0, Lmpu;->f:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 55
    iget-object v1, p0, Lmpu;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 56
    const/4 v0, 0x1

    iget-object v1, p0, Lmpu;->a:Ljava/lang/String;

    .line 57
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 59
    :cond_0
    iget-object v1, p0, Lmpu;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 60
    const/4 v1, 0x2

    iget-object v2, p0, Lmpu;->b:Ljava/lang/String;

    .line 61
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_1
    iget-object v1, p0, Lmpu;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 64
    const/4 v1, 0x3

    iget-object v2, p0, Lmpu;->e:Ljava/lang/String;

    .line 65
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_2
    iget-object v1, p0, Lmpu;->d:Lock;

    if-eqz v1, :cond_3

    .line 68
    const/4 v1, 0x4

    iget-object v2, p0, Lmpu;->d:Lock;

    .line 69
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_3
    iget v1, p0, Lmpu;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 72
    const/4 v1, 0x5

    iget v2, p0, Lmpu;->f:I

    .line 73
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_4
    iget-object v1, p0, Lmpu;->c:Lmpm;

    if-eqz v1, :cond_5

    .line 76
    const/4 v1, 0x6

    iget-object v2, p0, Lmpu;->c:Lmpm;

    .line 77
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_5
    iget-object v1, p0, Lmpu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    iput v0, p0, Lmpu;->ai:I

    .line 81
    return v0
.end method

.method public a(Loxn;)Lmpu;
    .locals 2

    .prologue
    .line 89
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 90
    sparse-switch v0, :sswitch_data_0

    .line 94
    iget-object v1, p0, Lmpu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 95
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmpu;->ah:Ljava/util/List;

    .line 98
    :cond_1
    iget-object v1, p0, Lmpu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    :sswitch_0
    return-object p0

    .line 105
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpu;->a:Ljava/lang/String;

    goto :goto_0

    .line 109
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpu;->b:Ljava/lang/String;

    goto :goto_0

    .line 113
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpu;->e:Ljava/lang/String;

    goto :goto_0

    .line 117
    :sswitch_4
    iget-object v0, p0, Lmpu;->d:Lock;

    if-nez v0, :cond_2

    .line 118
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Lmpu;->d:Lock;

    .line 120
    :cond_2
    iget-object v0, p0, Lmpu;->d:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 124
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 125
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 142
    :cond_3
    iput v0, p0, Lmpu;->f:I

    goto/16 :goto_0

    .line 144
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lmpu;->f:I

    goto/16 :goto_0

    .line 149
    :sswitch_6
    iget-object v0, p0, Lmpu;->c:Lmpm;

    if-nez v0, :cond_5

    .line 150
    new-instance v0, Lmpm;

    invoke-direct {v0}, Lmpm;-><init>()V

    iput-object v0, p0, Lmpu;->c:Lmpm;

    .line 152
    :cond_5
    iget-object v0, p0, Lmpu;->c:Lmpm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 90
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lmpu;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Lmpu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 33
    :cond_0
    iget-object v0, p0, Lmpu;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 34
    const/4 v0, 0x2

    iget-object v1, p0, Lmpu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 36
    :cond_1
    iget-object v0, p0, Lmpu;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 37
    const/4 v0, 0x3

    iget-object v1, p0, Lmpu;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 39
    :cond_2
    iget-object v0, p0, Lmpu;->d:Lock;

    if-eqz v0, :cond_3

    .line 40
    const/4 v0, 0x4

    iget-object v1, p0, Lmpu;->d:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_3
    iget v0, p0, Lmpu;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 43
    const/4 v0, 0x5

    iget v1, p0, Lmpu;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 45
    :cond_4
    iget-object v0, p0, Lmpu;->c:Lmpm;

    if-eqz v0, :cond_5

    .line 46
    const/4 v0, 0x6

    iget-object v1, p0, Lmpu;->c:Lmpm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 48
    :cond_5
    iget-object v0, p0, Lmpu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 50
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmpu;->a(Loxn;)Lmpu;

    move-result-object v0

    return-object v0
.end method
