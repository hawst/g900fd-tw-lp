.class public final Lknt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lknt;->a:Landroid/content/Context;

    .line 24
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lknt;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Missing required field: personId."

    invoke-static {v0, v1}, Llsk;->b(ZLjava/lang/Object;)V

    .line 34
    iget-object v0, p0, Lknt;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 35
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v1

    .line 37
    iget-object v0, p0, Lknt;->a:Landroid/content/Context;

    const-class v2, Lirh;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lirh;

    .line 39
    invoke-interface {v0}, Lirh;->a()Lire;

    move-result-object v0

    iget-object v2, p0, Lknt;->b:Ljava/lang/String;

    .line 40
    invoke-interface {v0, v2}, Lire;->c(Ljava/lang/String;)Lire;

    move-result-object v0

    const-string v2, "account_name"

    .line 41
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lire;->a(Ljava/lang/String;)Lire;

    move-result-object v0

    iget-object v2, p0, Lknt;->a:Landroid/content/Context;

    .line 42
    invoke-static {v2}, Lhns;->a(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, v2}, Lire;->a(I)Lire;

    move-result-object v0

    .line 43
    const-string v2, "is_plus_page"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lire;->b(Ljava/lang/String;)Lire;

    .line 46
    :cond_0
    invoke-interface {v0}, Lire;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 32
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lknt;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lknt;->b:Ljava/lang/String;

    .line 28
    return-object p0
.end method
