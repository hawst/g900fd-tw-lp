.class public final Lokc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojb;

.field public b:Lojc;

.field public c:Loik;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 661
    invoke-direct {p0}, Loxq;-><init>()V

    .line 664
    iput-object v0, p0, Lokc;->a:Lojb;

    .line 667
    iput-object v0, p0, Lokc;->b:Lojc;

    .line 670
    iput-object v0, p0, Lokc;->c:Loik;

    .line 661
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 690
    const/4 v0, 0x0

    .line 691
    iget-object v1, p0, Lokc;->a:Lojb;

    if-eqz v1, :cond_0

    .line 692
    const/4 v0, 0x1

    iget-object v1, p0, Lokc;->a:Lojb;

    .line 693
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 695
    :cond_0
    iget-object v1, p0, Lokc;->b:Lojc;

    if-eqz v1, :cond_1

    .line 696
    const/4 v1, 0x2

    iget-object v2, p0, Lokc;->b:Lojc;

    .line 697
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 699
    :cond_1
    iget-object v1, p0, Lokc;->c:Loik;

    if-eqz v1, :cond_2

    .line 700
    const/4 v1, 0x3

    iget-object v2, p0, Lokc;->c:Loik;

    .line 701
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 703
    :cond_2
    iget-object v1, p0, Lokc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 704
    iput v0, p0, Lokc;->ai:I

    .line 705
    return v0
.end method

.method public a(Loxn;)Lokc;
    .locals 2

    .prologue
    .line 713
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 714
    sparse-switch v0, :sswitch_data_0

    .line 718
    iget-object v1, p0, Lokc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 719
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lokc;->ah:Ljava/util/List;

    .line 722
    :cond_1
    iget-object v1, p0, Lokc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 724
    :sswitch_0
    return-object p0

    .line 729
    :sswitch_1
    iget-object v0, p0, Lokc;->a:Lojb;

    if-nez v0, :cond_2

    .line 730
    new-instance v0, Lojb;

    invoke-direct {v0}, Lojb;-><init>()V

    iput-object v0, p0, Lokc;->a:Lojb;

    .line 732
    :cond_2
    iget-object v0, p0, Lokc;->a:Lojb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 736
    :sswitch_2
    iget-object v0, p0, Lokc;->b:Lojc;

    if-nez v0, :cond_3

    .line 737
    new-instance v0, Lojc;

    invoke-direct {v0}, Lojc;-><init>()V

    iput-object v0, p0, Lokc;->b:Lojc;

    .line 739
    :cond_3
    iget-object v0, p0, Lokc;->b:Lojc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 743
    :sswitch_3
    iget-object v0, p0, Lokc;->c:Loik;

    if-nez v0, :cond_4

    .line 744
    new-instance v0, Loik;

    invoke-direct {v0}, Loik;-><init>()V

    iput-object v0, p0, Lokc;->c:Loik;

    .line 746
    :cond_4
    iget-object v0, p0, Lokc;->c:Loik;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 714
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, Lokc;->a:Lojb;

    if-eqz v0, :cond_0

    .line 676
    const/4 v0, 0x1

    iget-object v1, p0, Lokc;->a:Lojb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 678
    :cond_0
    iget-object v0, p0, Lokc;->b:Lojc;

    if-eqz v0, :cond_1

    .line 679
    const/4 v0, 0x2

    iget-object v1, p0, Lokc;->b:Lojc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 681
    :cond_1
    iget-object v0, p0, Lokc;->c:Loik;

    if-eqz v0, :cond_2

    .line 682
    const/4 v0, 0x3

    iget-object v1, p0, Lokc;->c:Loik;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 684
    :cond_2
    iget-object v0, p0, Lokc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 686
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 657
    invoke-virtual {p0, p1}, Lokc;->a(Loxn;)Lokc;

    move-result-object v0

    return-object v0
.end method
