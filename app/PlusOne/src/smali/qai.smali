.class public final Lqai;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1689
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1692
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqai;->a:[Ljava/lang/String;

    .line 1689
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1713
    .line 1714
    iget-object v0, p0, Lqai;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1715
    const/4 v0, 0x1

    iget-object v2, p0, Lqai;->b:Ljava/lang/Integer;

    .line 1716
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1718
    :goto_0
    iget-object v2, p0, Lqai;->a:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lqai;->a:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1720
    iget-object v3, p0, Lqai;->a:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 1722
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1720
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1724
    :cond_0
    add-int/2addr v0, v2

    .line 1725
    iget-object v1, p0, Lqai;->a:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1727
    :cond_1
    iget-object v1, p0, Lqai;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1728
    iput v0, p0, Lqai;->ai:I

    .line 1729
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lqai;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1737
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1738
    sparse-switch v0, :sswitch_data_0

    .line 1742
    iget-object v1, p0, Lqai;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1743
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqai;->ah:Ljava/util/List;

    .line 1746
    :cond_1
    iget-object v1, p0, Lqai;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748
    :sswitch_0
    return-object p0

    .line 1753
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqai;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1757
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1758
    iget-object v0, p0, Lqai;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 1759
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1760
    iget-object v2, p0, Lqai;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1761
    iput-object v1, p0, Lqai;->a:[Ljava/lang/String;

    .line 1762
    :goto_1
    iget-object v1, p0, Lqai;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1763
    iget-object v1, p0, Lqai;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1764
    invoke-virtual {p1}, Loxn;->a()I

    .line 1762
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1767
    :cond_2
    iget-object v1, p0, Lqai;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1738
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1699
    iget-object v0, p0, Lqai;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1700
    const/4 v0, 0x1

    iget-object v1, p0, Lqai;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1702
    :cond_0
    iget-object v0, p0, Lqai;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1703
    iget-object v1, p0, Lqai;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1704
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 1703
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1707
    :cond_1
    iget-object v0, p0, Lqai;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1709
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1685
    invoke-virtual {p0, p1}, Lqai;->a(Loxn;)Lqai;

    move-result-object v0

    return-object v0
.end method
