.class public final Llxu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxu;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3959
    const/4 v0, 0x0

    new-array v0, v0, [Llxu;

    sput-object v0, Llxu;->a:[Llxu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3960
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3963
    const/4 v0, 0x0

    iput-object v0, p0, Llxu;->b:Llxp;

    .line 3960
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3992
    const/4 v0, 0x0

    .line 3993
    iget-object v1, p0, Llxu;->b:Llxp;

    if-eqz v1, :cond_0

    .line 3994
    const/4 v0, 0x1

    iget-object v1, p0, Llxu;->b:Llxp;

    .line 3995
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3997
    :cond_0
    iget-object v1, p0, Llxu;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3998
    const/4 v1, 0x2

    iget-object v2, p0, Llxu;->c:Ljava/lang/String;

    .line 3999
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4001
    :cond_1
    iget-object v1, p0, Llxu;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4002
    const/4 v1, 0x3

    iget-object v2, p0, Llxu;->e:Ljava/lang/String;

    .line 4003
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4005
    :cond_2
    iget-object v1, p0, Llxu;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4006
    const/4 v1, 0x4

    iget-object v2, p0, Llxu;->d:Ljava/lang/String;

    .line 4007
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4009
    :cond_3
    iget-object v1, p0, Llxu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4010
    iput v0, p0, Llxu;->ai:I

    .line 4011
    return v0
.end method

.method public a(Loxn;)Llxu;
    .locals 2

    .prologue
    .line 4019
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4020
    sparse-switch v0, :sswitch_data_0

    .line 4024
    iget-object v1, p0, Llxu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4025
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxu;->ah:Ljava/util/List;

    .line 4028
    :cond_1
    iget-object v1, p0, Llxu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4030
    :sswitch_0
    return-object p0

    .line 4035
    :sswitch_1
    iget-object v0, p0, Llxu;->b:Llxp;

    if-nez v0, :cond_2

    .line 4036
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxu;->b:Llxp;

    .line 4038
    :cond_2
    iget-object v0, p0, Llxu;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4042
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxu;->c:Ljava/lang/String;

    goto :goto_0

    .line 4046
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxu;->e:Ljava/lang/String;

    goto :goto_0

    .line 4050
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxu;->d:Ljava/lang/String;

    goto :goto_0

    .line 4020
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3974
    iget-object v0, p0, Llxu;->b:Llxp;

    if-eqz v0, :cond_0

    .line 3975
    const/4 v0, 0x1

    iget-object v1, p0, Llxu;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3977
    :cond_0
    iget-object v0, p0, Llxu;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3978
    const/4 v0, 0x2

    iget-object v1, p0, Llxu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3980
    :cond_1
    iget-object v0, p0, Llxu;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3981
    const/4 v0, 0x3

    iget-object v1, p0, Llxu;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3983
    :cond_2
    iget-object v0, p0, Llxu;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3984
    const/4 v0, 0x4

    iget-object v1, p0, Llxu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3986
    :cond_3
    iget-object v0, p0, Llxu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3988
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3956
    invoke-virtual {p0, p1}, Llxu;->a(Loxn;)Llxu;

    move-result-object v0

    return-object v0
.end method
