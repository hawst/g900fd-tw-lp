.class public Lhsu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llpq;
.implements Llrg;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhsw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhsu;->a:Ljava/util/List;

    .line 81
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 82
    return-void
.end method


# virtual methods
.method public a(Lhsw;)Lhsu;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lhsu;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "BackNavigationHandler already on stack."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Lhsu;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    return-object p0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lhsu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 107
    iget-object v0, p0, Lhsu;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsw;

    .line 108
    invoke-interface {v0}, Lhsw;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x1

    .line 112
    :goto_1
    return v0

    .line 106
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 112
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b(Lhsw;)Lhsu;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lhsu;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 101
    return-object p0
.end method
