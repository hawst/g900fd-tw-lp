.class public final Lpad;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpad;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Lpad;

    sput-object v0, Lpad;->a:[Lpad;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    const/high16 v0, -0x80000000

    iput v0, p0, Lpad;->b:I

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 47
    iget v1, p0, Lpad;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 48
    const/4 v0, 0x1

    iget v1, p0, Lpad;->b:I

    .line 49
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 51
    :cond_0
    iget-object v1, p0, Lpad;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 52
    const/4 v1, 0x2

    iget-object v2, p0, Lpad;->c:Ljava/lang/String;

    .line 53
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_1
    iget-object v1, p0, Lpad;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 56
    const/4 v1, 0x3

    iget-object v2, p0, Lpad;->d:Ljava/lang/String;

    .line 57
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    :cond_2
    iget-object v1, p0, Lpad;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 60
    const/4 v1, 0x4

    iget-object v2, p0, Lpad;->e:Ljava/lang/String;

    .line 61
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_3
    iget-object v1, p0, Lpad;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    iput v0, p0, Lpad;->ai:I

    .line 65
    return v0
.end method

.method public a(Loxn;)Lpad;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 73
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 74
    sparse-switch v0, :sswitch_data_0

    .line 78
    iget-object v1, p0, Lpad;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpad;->ah:Ljava/util/List;

    .line 82
    :cond_1
    iget-object v1, p0, Lpad;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :sswitch_0
    return-object p0

    .line 89
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 90
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 92
    :cond_2
    iput v0, p0, Lpad;->b:I

    goto :goto_0

    .line 94
    :cond_3
    iput v2, p0, Lpad;->b:I

    goto :goto_0

    .line 99
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpad;->c:Ljava/lang/String;

    goto :goto_0

    .line 103
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpad;->d:Ljava/lang/String;

    goto :goto_0

    .line 107
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpad;->e:Ljava/lang/String;

    goto :goto_0

    .line 74
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28
    iget v0, p0, Lpad;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 29
    const/4 v0, 0x1

    iget v1, p0, Lpad;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 31
    :cond_0
    iget-object v0, p0, Lpad;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Lpad;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 34
    :cond_1
    iget-object v0, p0, Lpad;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Lpad;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 37
    :cond_2
    iget-object v0, p0, Lpad;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 38
    const/4 v0, 0x4

    iget-object v1, p0, Lpad;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    :cond_3
    iget-object v0, p0, Lpad;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 42
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lpad;->a(Loxn;)Lpad;

    move-result-object v0

    return-object v0
.end method
