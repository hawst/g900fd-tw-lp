.class final Ljlo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Ligy;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Ligv;

.field private c:Livx;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 93
    const-string v1, "notification_tag"

    .line 94
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    const-string v2, "notifications"

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "gaiaid"

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    aget-object v0, v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Ljlo;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Ljlo;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_0

    .line 63
    iget-object v1, p0, Ljlo;->c:Livx;

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    const/4 v3, 0x0

    .line 64
    invoke-virtual {v2, v0, v3}, Liwg;->a(Ljava/lang/String;Ljava/lang/String;)Liwg;

    move-result-object v0

    .line 63
    invoke-virtual {v1, v0}, Livx;->a(Liwg;)V

    .line 71
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Ljlo;->c:Livx;

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Liwl;

    .line 68
    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    .line 67
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Ljlo;->a:Landroid/app/Activity;

    .line 55
    iput-object p3, p0, Ljlo;->b:Ligv;

    .line 56
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Ljlo;->c:Livx;

    .line 57
    return-void
.end method

.method public a(ZIIII)V
    .locals 5

    .prologue
    .line 77
    iget-object v0, p0, Ljlo;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Ljlo;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.apps.plus.settings.NOTIFICATION_SETTINGS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    iget-object v0, p0, Ljlo;->a:Landroid/app/Activity;

    const-class v4, Lhei;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    if-eqz v3, :cond_0

    invoke-virtual {v1, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_0
    invoke-direct {p0, v2}, Ljlo;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v0, v2}, Lhei;->b(Ljava/lang/String;)I

    move-result p5

    :cond_1
    const/4 v0, -0x1

    if-ne p5, v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    iget-object v1, p0, Ljlo;->b:Ligv;

    invoke-interface {v1, v0}, Ligv;->a(Landroid/content/Intent;)V

    .line 78
    :goto_1
    return-void

    .line 77
    :cond_2
    const-string v0, "account_id"

    invoke-virtual {v1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljlo;->b:Ligv;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_1
.end method
