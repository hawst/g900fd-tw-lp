.class public final Lfxj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field private static A:Ljava/lang/String;

.field private static B:Ljava/lang/String;

.field private static C:I

.field private static D:I

.field private static a:I

.field private static b:F

.field private static c:I

.field private static d:I

.field private static e:Z

.field private static f:Lizs;

.field private static g:Landroid/graphics/Bitmap;

.field private static h:Landroid/graphics/Paint;

.field private static i:Landroid/graphics/Paint;

.field private static j:Landroid/graphics/Paint;

.field private static k:Landroid/text/TextPaint;

.field private static l:Landroid/text/TextPaint;

.field private static m:Landroid/text/TextPaint;

.field private static n:Landroid/text/TextPaint;

.field private static o:Landroid/text/TextPaint;

.field private static p:Landroid/text/TextPaint;

.field private static q:Landroid/text/TextPaint;

.field private static r:Landroid/graphics/Bitmap;

.field private static s:Landroid/graphics/Bitmap;

.field private static t:Landroid/graphics/Bitmap;

.field private static u:Landroid/graphics/Bitmap;

.field private static v:Landroid/graphics/Bitmap;

.field private static w:Landroid/graphics/Bitmap;

.field private static x:Landroid/graphics/Bitmap;

.field private static y:Landroid/graphics/Bitmap;

.field private static z:Landroid/graphics/drawable/NinePatchDrawable;


# instance fields
.field private E:Z

.field private F:Lidh;

.field private G:Lltp;

.field private H:Z

.field private I:Lfwo;

.field private J:Lizu;

.field private K:Lkda;

.field private L:Landroid/graphics/Rect;

.field private M:Lfwt;

.field private N:Landroid/graphics/Bitmap;

.field private O:Landroid/graphics/Rect;

.field private P:Landroid/text/StaticLayout;

.field private Q:Landroid/graphics/Point;

.field private R:Landroid/graphics/Bitmap;

.field private S:Landroid/graphics/Rect;

.field private T:Landroid/graphics/Rect;

.field private U:Landroid/graphics/Bitmap;

.field private V:[F

.field private W:Landroid/text/StaticLayout;

.field private X:Landroid/graphics/Point;

.field private Y:Landroid/text/StaticLayout;

.field private Z:Landroid/graphics/Point;

.field private aa:Landroid/text/StaticLayout;

.field private ab:Landroid/graphics/Point;

.field private ac:Landroid/text/StaticLayout;

.field private ad:Landroid/graphics/Point;

.field private ae:Ljava/lang/CharSequence;

.field private af:Landroid/text/StaticLayout;

.field private ag:Landroid/graphics/Point;

.field private ah:Lfwu;

.field private ai:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const v6, 0x7f0d024c

    const v5, 0x7f0d024b

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lidk;->a(Landroid/content/Context;)V

    .line 146
    sget-boolean v0, Lfxj;->e:Z

    if-nez v0, :cond_0

    .line 147
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 148
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 149
    const-class v0, Lizs;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lfxj;->f:Lizs;

    .line 150
    new-instance v0, Landroid/graphics/Paint;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lfxj;->h:Landroid/graphics/Paint;

    .line 151
    const v0, 0x7f0d02f2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxj;->a:I

    .line 152
    const v0, 0x7f0d02ba

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lfxj;->b:F

    .line 154
    const v0, 0x7f0d02b7

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxj;->c:I

    .line 155
    const v0, 0x7f0d02b8

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxj;->d:I

    .line 156
    invoke-static {v1, v7}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->g:Landroid/graphics/Bitmap;

    .line 158
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 159
    sput-object v0, Lfxj;->i:Landroid/graphics/Paint;

    const v3, 0x7f0b02eb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 160
    sget-object v0, Lfxj;->i:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 162
    const/16 v0, 0x11

    invoke-static {v1, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    sput-object v0, Lfxj;->k:Landroid/text/TextPaint;

    .line 165
    const/16 v0, 0x19

    invoke-static {v1, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    .line 167
    sput-object v0, Lfxj;->l:Landroid/text/TextPaint;

    invoke-static {v0, v6}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 169
    const/16 v0, 0xe

    invoke-static {v1, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    .line 172
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 173
    sput-object v3, Lfxj;->m:Landroid/text/TextPaint;

    const v4, 0x7f0b0141

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 174
    sget-object v3, Lfxj;->m:Landroid/text/TextPaint;

    invoke-static {v3, v5}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 176
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 177
    sput-object v3, Lfxj;->p:Landroid/text/TextPaint;

    const v4, 0x7f0b030c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 178
    sget-object v3, Lfxj;->p:Landroid/text/TextPaint;

    invoke-static {v3, v5}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 180
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 181
    sput-object v3, Lfxj;->o:Landroid/text/TextPaint;

    const v4, 0x7f0b013d

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 182
    sget-object v3, Lfxj;->o:Landroid/text/TextPaint;

    invoke-static {v3, v5}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 184
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 185
    sput-object v3, Lfxj;->n:Landroid/text/TextPaint;

    const v0, 0x7f0b030d

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 186
    sget-object v0, Lfxj;->n:Landroid/text/TextPaint;

    invoke-static {v0, v5}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 188
    new-instance v0, Landroid/text/TextPaint;

    sget-object v3, Lfxj;->l:Landroid/text/TextPaint;

    invoke-direct {v0, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 189
    sput-object v0, Lfxj;->q:Landroid/text/TextPaint;

    const v3, 0x7f0b013a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 190
    sget-object v0, Lfxj;->q:Landroid/text/TextPaint;

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 191
    sget-object v0, Lfxj;->q:Landroid/text/TextPaint;

    invoke-static {v0, v6}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 193
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 194
    sput-object v0, Lfxj;->j:Landroid/graphics/Paint;

    const v3, 0x7f0b030b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 195
    sget-object v0, Lfxj;->j:Landroid/graphics/Paint;

    const v3, 0x7f0d02b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 198
    const v0, 0x7f0203c2

    invoke-static {v2, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->r:Landroid/graphics/Bitmap;

    .line 200
    const v0, 0x7f0203ca

    invoke-static {v2, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->s:Landroid/graphics/Bitmap;

    .line 202
    const v0, 0x7f0203c9

    invoke-static {v2, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->t:Landroid/graphics/Bitmap;

    .line 205
    const v0, 0x7f0203cc

    invoke-static {v2, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->w:Landroid/graphics/Bitmap;

    .line 207
    const v0, 0x7f0203cd

    invoke-static {v2, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->x:Landroid/graphics/Bitmap;

    .line 209
    const v0, 0x7f0203ce

    invoke-static {v2, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->y:Landroid/graphics/Bitmap;

    .line 212
    const v0, 0x7f0203d1

    invoke-static {v2, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->u:Landroid/graphics/Bitmap;

    .line 213
    const v0, 0x7f0203c8

    invoke-static {v2, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxj;->v:Landroid/graphics/Bitmap;

    .line 216
    const v0, 0x7f0a090a

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfxj;->B:Ljava/lang/String;

    .line 217
    const v0, 0x7f0a073a

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfxj;->A:Ljava/lang/String;

    .line 219
    const v0, 0x7f0200ad

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lfxj;->z:Landroid/graphics/drawable/NinePatchDrawable;

    .line 222
    invoke-static {v1}, Ldrm;->a(Landroid/content/Context;)I

    move-result v0

    .line 223
    sput v0, Lfxj;->C:I

    invoke-static {v0}, Ldrm;->a(I)I

    move-result v0

    sput v0, Lfxj;->D:I

    .line 224
    sput-boolean v7, Lfxj;->e:Z

    .line 227
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfxj;->L:Landroid/graphics/Rect;

    .line 229
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfxj;->O:Landroid/graphics/Rect;

    .line 230
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxj;->Q:Landroid/graphics/Point;

    .line 231
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfxj;->S:Landroid/graphics/Rect;

    .line 232
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfxj;->T:Landroid/graphics/Rect;

    .line 234
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxj;->X:Landroid/graphics/Point;

    .line 235
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxj;->Z:Landroid/graphics/Point;

    .line 236
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxj;->ab:Landroid/graphics/Point;

    .line 237
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxj;->ad:Landroid/graphics/Point;

    .line 238
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxj;->ag:Landroid/graphics/Point;

    .line 239
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lfxj;->V:[F

    .line 240
    return-void
.end method

.method private a(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;
    .locals 11

    .prologue
    .line 319
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v0, p1

    move v1, p2

    move v2, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move/from16 v10, p7

    invoke-static/range {v0 .. v10}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(IIII)I
    .locals 12

    .prologue
    .line 338
    iget-object v0, p0, Lfxj;->I:Lfwo;

    invoke-virtual {v0}, Lfwo;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 342
    iget-object v0, p0, Lfxj;->F:Lidh;

    invoke-virtual {v0}, Lidh;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    const/4 v0, 0x0

    .line 397
    :goto_0
    return v0

    .line 346
    :cond_0
    iget-object v0, p0, Lfxj;->F:Lidh;

    invoke-static {v0}, Ldrm;->a(Lidh;)I

    move-result v0

    .line 347
    packed-switch v0, :pswitch_data_0

    .line 370
    :pswitch_0
    iget-boolean v0, p0, Lfxj;->H:Z

    if-nez v0, :cond_2

    const v0, 0x7f0a071b

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 372
    sget-object v9, Lfxj;->m:Landroid/text/TextPaint;

    .line 373
    sget-object v0, Lfxj;->w:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    .line 377
    :goto_2
    sget v0, Lfxj;->b:F

    iget-object v1, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 378
    sub-int v0, p2, v11

    .line 379
    iget-object v1, p0, Lfxj;->S:Landroid/graphics/Rect;

    add-int v2, p1, p3

    iget-object v3, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    add-int v3, p1, p3

    iget-object v4, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    .line 380
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 379
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 382
    iget-object v0, p0, Lfxj;->S:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v0, p3, v0

    sub-int v2, v0, p4

    .line 384
    add-int v1, p2, p4

    const/4 v3, 0x0

    iget-object v4, p0, Lfxj;->N:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lfxj;->O:Landroid/graphics/Rect;

    iget-object v8, p0, Lfxj;->Q:Landroid/graphics/Point;

    const/4 v10, 0x1

    move v0, p1

    move/from16 v6, p4

    invoke-static/range {v0 .. v10}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxj;->P:Landroid/text/StaticLayout;

    .line 387
    iget-object v0, p0, Lfxj;->Q:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lfxj;->P:Landroid/text/StaticLayout;

    .line 388
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    sub-int v1, v0, p2

    iget-object v0, p0, Lfxj;->N:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfxj;->O:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, p2

    .line 387
    :goto_3
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, p2

    add-int v0, v0, p4

    .line 392
    iget-object v1, p0, Lfxj;->V:[F

    const/4 v3, 0x0

    int-to-float v4, p1

    aput v4, v1, v3

    .line 393
    iget-object v1, p0, Lfxj;->V:[F

    const/4 v3, 0x1

    int-to-float v4, v0

    aput v4, v1, v3

    .line 394
    iget-object v1, p0, Lfxj;->V:[F

    const/4 v3, 0x2

    add-int/2addr v2, p1

    int-to-float v2, v2

    aput v2, v1, v3

    .line 395
    iget-object v1, p0, Lfxj;->V:[F

    const/4 v2, 0x3

    int-to-float v3, v0

    aput v3, v1, v2

    .line 397
    sub-int/2addr v0, p2

    iget-object v1, p0, Lfxj;->S:Landroid/graphics/Rect;

    .line 398
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v1, v11

    .line 397
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_0

    .line 350
    :pswitch_1
    iget-boolean v0, p0, Lfxj;->H:Z

    if-nez v0, :cond_1

    const v0, 0x7f0a071d

    :goto_4
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 352
    sget-object v9, Lfxj;->p:Landroid/text/TextPaint;

    .line 353
    sget-object v0, Lfxj;->r:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxj;->N:Landroid/graphics/Bitmap;

    .line 354
    sget-object v0, Lfxj;->x:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 350
    :cond_1
    const v0, 0x7f0a071e

    goto :goto_4

    .line 358
    :pswitch_2
    const v0, 0x7f0a0720

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 359
    sget-object v9, Lfxj;->o:Landroid/text/TextPaint;

    .line 360
    sget-object v0, Lfxj;->s:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxj;->N:Landroid/graphics/Bitmap;

    .line 361
    sget-object v0, Lfxj;->y:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 364
    :pswitch_3
    const v0, 0x7f0a071f

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 365
    sget-object v9, Lfxj;->n:Landroid/text/TextPaint;

    .line 366
    sget-object v0, Lfxj;->t:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxj;->N:Landroid/graphics/Bitmap;

    .line 367
    sget-object v0, Lfxj;->w:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 370
    :cond_2
    const v0, 0x7f0a071c

    goto/16 :goto_1

    .line 388
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 347
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(IIIII)I
    .locals 11

    .prologue
    .line 426
    invoke-virtual {p0, p1, p2, p3, p4}, Lfxj;->a(IIII)I

    move-result v0

    add-int/2addr v0, p2

    .line 432
    add-int v2, v0, p4

    .line 433
    iget-object v0, p0, Lfxj;->F:Lidh;

    invoke-virtual {v0}, Lidh;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lfxj;->X:Landroid/graphics/Point;

    sget-object v6, Lfxj;->k:Landroid/text/TextPaint;

    const/4 v7, 0x1

    move-object v0, p0

    move v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v7}, Lfxj;->a(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxj;->W:Landroid/text/StaticLayout;

    .line 435
    iget-object v0, p0, Lfxj;->W:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 438
    iget-object v1, p0, Lfxj;->ae:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 439
    add-int v2, v0, p5

    .line 440
    iget-object v4, p0, Lfxj;->ae:Ljava/lang/CharSequence;

    iget-object v5, p0, Lfxj;->ag:Landroid/graphics/Point;

    sget-object v6, Lfxj;->l:Landroid/text/TextPaint;

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v7}, Lfxj;->a(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxj;->af:Landroid/text/StaticLayout;

    .line 442
    iget-object v0, p0, Lfxj;->af:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 446
    :cond_0
    add-int v8, v0, p5

    .line 448
    const/4 v5, 0x0

    .line 450
    iget-object v0, p0, Lfxj;->F:Lidh;

    invoke-virtual {v0}, Lidh;->l()Loyy;

    move-result-object v9

    .line 451
    if-eqz v9, :cond_1

    iget-object v0, v9, Loyy;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, v9, Loyy;->c:Ljava/lang/String;

    invoke-static {v0}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    .line 455
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 456
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 457
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    iget-object v3, v9, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v6, 0x1

    .line 456
    invoke-static/range {v0 .. v6}, Lidi;->a(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z

    move-result v0

    .line 460
    iget-object v1, p0, Lfxj;->I:Lfwo;

    .line 461
    invoke-virtual {v1}, Lfwo;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 460
    invoke-static {v1, v9, v2, v3, v0}, Lidi;->a(Landroid/content/Context;Loyy;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v4

    .line 463
    iget-object v5, p0, Lfxj;->Z:Landroid/graphics/Point;

    sget-object v6, Lfxj;->l:Landroid/text/TextPaint;

    const/4 v7, 0x1

    move-object v0, p0

    move v1, p1

    move v2, v8

    move v3, p3

    invoke-direct/range {v0 .. v7}, Lfxj;->a(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxj;->Y:Landroid/text/StaticLayout;

    .line 466
    iget-object v0, p0, Lfxj;->Y:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int v1, v8, v0

    .line 468
    iget-object v0, p0, Lfxj;->F:Lidh;

    invoke-virtual {v0}, Lidh;->b()Lozp;

    move-result-object v8

    .line 469
    if-eqz v8, :cond_4

    const/4 v0, 0x1

    .line 471
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 472
    iget-object v3, v9, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 473
    iget-object v3, v9, Loyy;->c:Ljava/lang/String;

    invoke-static {v3, v2, v0}, Lidk;->a(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;

    move-result-object v4

    .line 476
    if-eqz v4, :cond_8

    .line 477
    add-int v2, v1, p5

    .line 478
    iget-object v5, p0, Lfxj;->ab:Landroid/graphics/Point;

    sget-object v6, Lfxj;->l:Landroid/text/TextPaint;

    const/4 v7, 0x1

    move-object v0, p0

    move v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v7}, Lfxj;->a(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxj;->aa:Landroid/text/StaticLayout;

    .line 480
    iget-object v0, p0, Lfxj;->aa:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 484
    :goto_1
    invoke-static {v8}, Ldrm;->d(Lozp;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 485
    add-int v9, v0, p5

    .line 486
    new-instance v0, Lfwu;

    iget-object v1, p0, Lfxj;->I:Lfwo;

    invoke-virtual {v1}, Lfwo;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lfxj;->A:Ljava/lang/String;

    sget-object v4, Lfxj;->q:Landroid/text/TextPaint;

    sget-object v5, Lfxj;->z:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v6, Lfxj;->z:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v7, 0x0

    move v8, p1

    invoke-direct/range {v0 .. v9}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;II)V

    iput-object v0, p0, Lfxj;->ah:Lfwu;

    .line 489
    iget-object v0, p0, Lfxj;->ah:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v9

    move v1, v0

    .line 493
    :goto_2
    const/4 v7, 0x0

    .line 494
    iget-object v0, p0, Lfxj;->F:Lidh;

    invoke-virtual {v0}, Lidh;->o()Lpao;

    move-result-object v0

    .line 495
    iget-object v2, p0, Lfxj;->F:Lidh;

    invoke-virtual {v2}, Lidh;->i()Lpbj;

    move-result-object v2

    iget-object v2, v2, Lpbj;->g:Lltn;

    .line 497
    if-eqz v0, :cond_5

    .line 498
    iget-object v2, v0, Lpao;->b:Ljava/lang/String;

    .line 499
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, v0, Lpao;->d:Loya;

    if-eqz v3, :cond_6

    .line 500
    iget-object v0, v0, Lpao;->d:Loya;

    sget-object v3, Lpcg;->a:Loxr;

    .line 501
    invoke-virtual {v0, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcg;

    .line 502
    if-eqz v0, :cond_6

    .line 503
    iget-object v0, v0, Lpcg;->c:Ljava/lang/String;

    .line 506
    :goto_3
    sget-object v2, Lfxj;->u:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lfxj;->U:Landroid/graphics/Bitmap;

    move-object v7, v0

    .line 512
    :cond_2
    :goto_4
    if-eqz v7, :cond_3

    .line 513
    add-int v1, v1, p5

    .line 515
    const/4 v3, 0x0

    iget-object v4, p0, Lfxj;->U:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lfxj;->T:Landroid/graphics/Rect;

    iget-object v8, p0, Lfxj;->ad:Landroid/graphics/Point;

    sget-object v9, Lfxj;->l:Landroid/text/TextPaint;

    const/4 v10, 0x1

    move v0, p1

    move v2, p3

    move v6, p4

    invoke-static/range {v0 .. v10}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxj;->ac:Landroid/text/StaticLayout;

    .line 519
    iget-object v0, p0, Lfxj;->ac:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v1, v0

    .line 522
    :cond_3
    sub-int v0, v1, p2

    return v0

    .line 469
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 507
    :cond_5
    if-eqz v2, :cond_2

    .line 508
    sget-object v7, Lfxj;->B:Ljava/lang/String;

    .line 509
    sget-object v0, Lfxj;->v:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxj;->U:Landroid/graphics/Bitmap;

    goto :goto_4

    :cond_6
    move-object v0, v2

    goto :goto_3

    :cond_7
    move v1, v0

    goto :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public a(IILandroid/graphics/Canvas;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 571
    .line 572
    add-int v1, p1, p2

    .line 574
    iget-boolean v0, p0, Lfxj;->E:Z

    if-nez v0, :cond_1

    .line 673
    :cond_0
    :goto_0
    return p1

    .line 579
    :cond_1
    iget-object v0, p0, Lfxj;->K:Lkda;

    if-eqz v0, :cond_3

    .line 580
    iget-object v0, p0, Lfxj;->K:Lkda;

    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 582
    if-eqz v0, :cond_3

    iget-object v2, p0, Lfxj;->L:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v2, v1, :cond_2

    iget-boolean v2, p0, Lfxj;->ai:Z

    if-eqz v2, :cond_3

    .line 583
    :cond_2
    iget-object v2, p0, Lfxj;->L:Landroid/graphics/Rect;

    sget-object v3, Lfxj;->h:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v5, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 588
    :cond_3
    iget-object v0, p0, Lfxj;->M:Lfwt;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v0}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-le v0, v1, :cond_4

    iget-boolean v0, p0, Lfxj;->ai:Z

    if-eqz v0, :cond_6

    .line 589
    :cond_4
    iget-object v0, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v0}, Lfwt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 591
    if-nez v0, :cond_5

    .line 592
    sget-object v0, Lfxj;->g:Landroid/graphics/Bitmap;

    .line 595
    :cond_5
    iget-object v2, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v2}, Lfwt;->a()Landroid/graphics/Rect;

    move-result-object v2

    sget-object v3, Lfxj;->h:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v5, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 597
    iget-object v0, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v0}, Lfwt;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 598
    iget-object v0, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v0, p3}, Lfwt;->a(Landroid/graphics/Canvas;)V

    .line 602
    :cond_6
    iget-object v0, p0, Lfxj;->P:Landroid/text/StaticLayout;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lfxj;->S:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lfxj;->O:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lfxj;->Q:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    iget-object v4, p0, Lfxj;->P:Landroid/text/StaticLayout;

    .line 603
    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 602
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-le v0, v1, :cond_7

    iget-boolean v0, p0, Lfxj;->ai:Z

    if-eqz v0, :cond_9

    .line 606
    :cond_7
    iget-object v0, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lfxj;->S:Landroid/graphics/Rect;

    invoke-virtual {p3, v0, v5, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 609
    iget-object v0, p0, Lfxj;->P:Landroid/text/StaticLayout;

    iget-object v2, p0, Lfxj;->Q:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v2, p3}, Lfxj;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 611
    iget-object v0, p0, Lfxj;->N:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_8

    .line 612
    iget-object v0, p0, Lfxj;->N:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lfxj;->O:Landroid/graphics/Rect;

    invoke-virtual {p3, v0, v5, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 616
    :cond_8
    iget-object v0, p0, Lfxj;->V:[F

    sget-object v2, Lfxj;->j:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v2}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    .line 620
    :cond_9
    iget-object v0, p0, Lfxj;->X:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lfxj;->W:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    if-le v0, v1, :cond_a

    iget-boolean v0, p0, Lfxj;->ai:Z

    if-eqz v0, :cond_b

    .line 621
    :cond_a
    iget-object v0, p0, Lfxj;->W:Landroid/text/StaticLayout;

    iget-object v2, p0, Lfxj;->X:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v2, p3}, Lfxj;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 625
    :cond_b
    iget-object v0, p0, Lfxj;->Z:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lfxj;->Y:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 627
    if-le v0, v1, :cond_c

    iget-boolean v2, p0, Lfxj;->ai:Z

    if-eqz v2, :cond_d

    .line 628
    :cond_c
    iget-object v2, p0, Lfxj;->Y:Landroid/text/StaticLayout;

    iget-object v3, p0, Lfxj;->Z:Landroid/graphics/Point;

    invoke-virtual {p0, v2, v3, p3}, Lfxj;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    move p1, v0

    .line 633
    :cond_d
    iget-object v0, p0, Lfxj;->aa:Landroid/text/StaticLayout;

    if-eqz v0, :cond_f

    .line 634
    iget-object v0, p0, Lfxj;->ab:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lfxj;->aa:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 636
    if-le v0, v1, :cond_e

    iget-boolean v2, p0, Lfxj;->ai:Z

    if-eqz v2, :cond_f

    .line 637
    :cond_e
    iget-object v2, p0, Lfxj;->aa:Landroid/text/StaticLayout;

    iget-object v3, p0, Lfxj;->ab:Landroid/graphics/Point;

    invoke-virtual {p0, v2, v3, p3}, Lfxj;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    move p1, v0

    .line 642
    :cond_f
    iget-object v0, p0, Lfxj;->ah:Lfwu;

    if-eqz v0, :cond_11

    .line 643
    iget-object v0, p0, Lfxj;->ah:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 645
    if-le v0, v1, :cond_10

    iget-boolean v2, p0, Lfxj;->ai:Z

    if-eqz v2, :cond_11

    .line 646
    :cond_10
    iget-object v2, p0, Lfxj;->ah:Lfwu;

    invoke-virtual {v2, p3}, Lfwu;->a(Landroid/graphics/Canvas;)V

    move p1, v0

    .line 652
    :cond_11
    iget-object v0, p0, Lfxj;->ac:Landroid/text/StaticLayout;

    if-eqz v0, :cond_13

    .line 653
    iget-object v0, p0, Lfxj;->T:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lfxj;->ac:Landroid/text/StaticLayout;

    .line 654
    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    iget-object v3, p0, Lfxj;->ad:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v3

    .line 653
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 656
    if-le v0, v1, :cond_12

    iget-boolean v2, p0, Lfxj;->ai:Z

    if-eqz v2, :cond_13

    .line 657
    :cond_12
    iget-object v2, p0, Lfxj;->ac:Landroid/text/StaticLayout;

    iget-object v3, p0, Lfxj;->ad:Landroid/graphics/Point;

    invoke-virtual {p0, v2, v3, p3}, Lfxj;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 658
    iget-object v2, p0, Lfxj;->U:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lfxj;->T:Landroid/graphics/Rect;

    invoke-virtual {p3, v2, v5, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move p1, v0

    .line 664
    :cond_13
    iget-object v0, p0, Lfxj;->ae:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lfxj;->ag:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lfxj;->af:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 667
    if-le v0, v1, :cond_14

    iget-boolean v1, p0, Lfxj;->ai:Z

    if-eqz v1, :cond_0

    .line 668
    :cond_14
    iget-object v1, p0, Lfxj;->af:Landroid/text/StaticLayout;

    iget-object v2, p0, Lfxj;->ag:Landroid/graphics/Point;

    invoke-virtual {p0, v1, v2, p3}, Lfxj;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    move p1, v0

    .line 669
    goto/16 :goto_0
.end method

.method public a(IIZI)I
    .locals 10

    .prologue
    .line 536
    iget-object v0, p0, Lfxj;->I:Lfwo;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfxj;->E:Z

    if-nez v0, :cond_1

    .line 537
    :cond_0
    const/4 v0, 0x0

    .line 567
    :goto_0
    return v0

    .line 539
    :cond_1
    iput-boolean p3, p0, Lfxj;->ai:Z

    .line 541
    sget v4, Lfxj;->c:I

    .line 542
    mul-int/lit8 v0, v4, 0x2

    .line 543
    sget v5, Lfxj;->d:I

    .line 545
    invoke-static {p4}, Ldrm;->a(I)I

    move-result v2

    .line 550
    invoke-virtual {p0, p1, p2, p4, v2}, Lfxj;->b(IIII)V

    .line 552
    sget v1, Lfxj;->a:I

    .line 553
    add-int v3, p1, v4

    .line 554
    add-int v6, p2, v2

    sub-int/2addr v6, v0

    .line 556
    iget-object v7, p0, Lfxj;->M:Lfwt;

    add-int v8, v3, v1

    add-int v9, v6, v1

    invoke-virtual {v7, v3, v6, v8, v9}, Lfwt;->a(IIII)V

    .line 559
    add-int/2addr v1, v3

    add-int/2addr v1, v4

    .line 560
    add-int/2addr v2, p2

    .line 561
    add-int v3, p1, p4

    sub-int/2addr v3, v1

    sub-int/2addr v3, v0

    move-object v0, p0

    .line 565
    invoke-virtual/range {v0 .. v5}, Lfxj;->a(IIIII)I

    move-result v0

    .line 567
    add-int/2addr v0, v2

    sub-int/2addr v0, p2

    goto :goto_0
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 279
    iget-boolean v0, p0, Lfxj;->E:Z

    if-nez v0, :cond_0

    .line 315
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lfxj;->I:Lfwo;

    iget-object v1, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v0, v1}, Lfwo;->b(Llip;)V

    .line 284
    iput-object v2, p0, Lfxj;->M:Lfwt;

    .line 285
    iput-object v2, p0, Lfxj;->G:Lltp;

    .line 286
    iput-object v2, p0, Lfxj;->F:Lidh;

    .line 287
    iput-object v2, p0, Lfxj;->I:Lfwo;

    .line 288
    iput-object v2, p0, Lfxj;->N:Landroid/graphics/Bitmap;

    .line 289
    iput-object v2, p0, Lfxj;->R:Landroid/graphics/Bitmap;

    .line 291
    iget-object v0, p0, Lfxj;->L:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 293
    iget-object v0, p0, Lfxj;->O:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 294
    iget-object v0, p0, Lfxj;->Q:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 295
    iget-object v0, p0, Lfxj;->S:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 296
    iget-object v0, p0, Lfxj;->T:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 298
    iput-object v2, p0, Lfxj;->W:Landroid/text/StaticLayout;

    .line 299
    iput-object v2, p0, Lfxj;->Y:Landroid/text/StaticLayout;

    .line 300
    iput-object v2, p0, Lfxj;->aa:Landroid/text/StaticLayout;

    .line 301
    iput-object v2, p0, Lfxj;->ac:Landroid/text/StaticLayout;

    .line 302
    iput-object v2, p0, Lfxj;->P:Landroid/text/StaticLayout;

    .line 303
    iput-object v2, p0, Lfxj;->af:Landroid/text/StaticLayout;

    .line 304
    iput-object v2, p0, Lfxj;->ah:Lfwu;

    .line 306
    iput-object v2, p0, Lfxj;->U:Landroid/graphics/Bitmap;

    .line 308
    iget-object v0, p0, Lfxj;->X:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 309
    iget-object v0, p0, Lfxj;->Z:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 310
    iget-object v0, p0, Lfxj;->ab:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 311
    iget-object v0, p0, Lfxj;->ad:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 312
    iget-object v0, p0, Lfxj;->ag:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 313
    iput-object v2, p0, Lfxj;->ae:Ljava/lang/CharSequence;

    .line 314
    iput-boolean v3, p0, Lfxj;->E:Z

    goto :goto_0
.end method

.method public a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 324
    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p2, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 325
    invoke-virtual {p1, p3}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 326
    iget v0, p2, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p2, Landroid/graphics/Point;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 327
    return-void
.end method

.method public a(Lfwo;Lidh;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 6

    .prologue
    .line 250
    invoke-virtual {p0}, Lfxj;->c()V

    .line 251
    invoke-virtual {p0}, Lfxj;->a()V

    .line 253
    iput-object p2, p0, Lfxj;->F:Lidh;

    .line 254
    iput-boolean p3, p0, Lfxj;->H:Z

    .line 255
    iget-object v0, p0, Lfxj;->F:Lidh;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfxj;->E:Z

    .line 257
    iget-boolean v0, p0, Lfxj;->E:Z

    if-eqz v0, :cond_1

    .line 258
    iput-object p1, p0, Lfxj;->I:Lfwo;

    .line 260
    iget-object v0, p0, Lfxj;->F:Lidh;

    invoke-virtual {v0}, Lidh;->i()Lpbj;

    move-result-object v0

    iget-object v0, v0, Lpbj;->d:Llto;

    invoke-static {v0}, Ldrm;->a(Llto;)Lltp;

    move-result-object v0

    iput-object v0, p0, Lfxj;->G:Lltp;

    .line 262
    iget-object v0, p0, Lfxj;->G:Lltp;

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {p1}, Lfwo;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfxj;->G:Lltp;

    iget-object v1, v1, Lltp;->d:Ljava/lang/String;

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v0, v1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lfxj;->J:Lizu;

    .line 267
    :cond_0
    new-instance v0, Lfwt;

    iget-object v1, p0, Lfxj;->I:Lfwo;

    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object v2, p4

    move-object v3, p5

    invoke-direct/range {v0 .. v5}, Lfwt;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lfxj;->M:Lfwt;

    .line 269
    iput-object p6, p0, Lfxj;->ae:Ljava/lang/CharSequence;

    .line 270
    iget-object v0, p0, Lfxj;->I:Lfwo;

    iget-object v1, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v0, v1}, Lfwo;->a(Llip;)V

    .line 272
    :cond_1
    invoke-virtual {p0}, Lfxj;->b()V

    .line 273
    return-void

    .line 255
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lkda;)V
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lfxj;->I:Lfwo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxj;->K:Lkda;

    if-ne p1, v0, :cond_0

    .line 711
    iget-object v0, p0, Lfxj;->I:Lfwo;

    invoke-virtual {v0}, Lfwo;->invalidate()V

    .line 713
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lfxj;->I:Lfwo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxj;->I:Lfwo;

    invoke-static {v0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    invoke-virtual {p0}, Lfxj;->d()V

    .line 701
    :cond_0
    return-void
.end method

.method protected b(IIII)V
    .locals 3

    .prologue
    .line 409
    iget-object v0, p0, Lfxj;->L:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lfxj;->L:Landroid/graphics/Rect;

    .line 410
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-ne v0, p4, :cond_0

    iget-object v0, p0, Lfxj;->L:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lfxj;->L:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-eq v0, p1, :cond_1

    :cond_0
    iget-object v0, p0, Lfxj;->G:Lltp;

    if-eqz v0, :cond_1

    .line 412
    iget-object v0, p0, Lfxj;->L:Landroid/graphics/Rect;

    add-int v1, p1, p3

    add-int v2, p2, p4

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 414
    :cond_1
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 705
    invoke-virtual {p0}, Lfxj;->e()V

    .line 706
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 677
    iget-object v0, p0, Lfxj;->M:Lfwt;

    if-eqz v0, :cond_0

    .line 678
    iget-object v0, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v0}, Lfwt;->b()V

    .line 680
    :cond_0
    iget-object v0, p0, Lfxj;->J:Lizu;

    if-eqz v0, :cond_1

    .line 681
    sget-object v0, Lfxj;->f:Lizs;

    iget-object v1, p0, Lfxj;->J:Lizu;

    sget v2, Lfxj;->C:I

    sget v3, Lfxj;->D:I

    invoke-virtual {v0, v1, v2, v3, p0}, Lizs;->b(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lfxj;->K:Lkda;

    .line 684
    :cond_1
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lfxj;->M:Lfwt;

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lfxj;->M:Lfwt;

    invoke-virtual {v0}, Lfwt;->c()V

    .line 690
    :cond_0
    iget-object v0, p0, Lfxj;->K:Lkda;

    if-eqz v0, :cond_1

    .line 691
    iget-object v0, p0, Lfxj;->K:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 692
    const/4 v0, 0x0

    iput-object v0, p0, Lfxj;->K:Lkda;

    .line 694
    :cond_1
    return-void
.end method
