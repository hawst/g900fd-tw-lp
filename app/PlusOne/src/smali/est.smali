.class public final Lest;
.super Lmn;
.source "PG"


# instance fields
.field private synthetic e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 66
    iput-object p1, p0, Lest;->e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    .line 67
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Lmn;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 68
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Lhxf;

    invoke-direct {v0, p1}, Lhxf;-><init>(Landroid/content/Context;)V

    .line 73
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhxf;->g(Z)V

    .line 74
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhxf;->a(Z)V

    .line 75
    iget-object v1, p0, Lest;->e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    invoke-virtual {v0, v1}, Lhxf;->a(Llio;)V

    .line 76
    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 81
    move-object v0, p1

    check-cast v0, Lhxf;

    .line 82
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 83
    const/4 v2, 0x4

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 84
    const/4 v3, 0x2

    .line 86
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    .line 87
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lest;->e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    .line 88
    invoke-static {v5}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->a(Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;)I

    move-result v5

    invoke-static {p2, v5, v2}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v5

    .line 84
    invoke-virtual/range {v0 .. v5}, Lhxf;->a(Ljava/lang/String;ILjava/lang/String;IZ)V

    .line 89
    iget-object v2, p0, Lest;->e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->b(Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lhxf;->setChecked(Z)V

    .line 90
    iget-object v2, p0, Lest;->e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->c(Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    iget-object v2, p0, Lest;->e:Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    if-nez v1, :cond_0

    const v1, 0x7f0a0693

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lhxf;->a(Ljava/lang/String;)V

    .line 91
    return-void

    .line 90
    :cond_0
    const v3, 0x7f11005d

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
