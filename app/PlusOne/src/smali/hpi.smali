.class public final Lhpi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lief;

.field public static final b:Lief;

.field public static final c:Lief;

.field public static final d:Lief;

.field public static final e:Lief;

.field public static final f:Lief;

.field public static final g:Lief;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 15
    new-instance v0, Lief;

    const-string v1, "debug.plus.instant_share"

    const-string v2, "true"

    const-string v3, "63a6dd90"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lhpi;->a:Lief;

    .line 24
    new-instance v0, Lief;

    const-string v1, "debug.plus.instant_share_video"

    const-string v2, "true"

    const-string v3, "2848510c"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lhpi;->b:Lief;

    .line 33
    new-instance v0, Lief;

    const-string v1, "debug.plus.upload_media_bg"

    const-string v2, "false"

    const-string v3, "3949ac2b"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lhpi;->c:Lief;

    .line 41
    new-instance v0, Lief;

    const-string v1, "debug.photos.allow_device_mgmt"

    const-string v2, "false"

    const-string v3, "bd4bcefc"

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lhpi;->d:Lief;

    .line 50
    new-instance v0, Lief;

    const-string v1, "debug.photos.max_video_upload"

    const-wide/32 v2, 0x9600000

    .line 52
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "c20dbbc8"

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lhpi;->e:Lief;

    .line 60
    new-instance v0, Lief;

    const-string v1, "debug.photos.gms_ab_kill"

    const-string v2, "false"

    const-string v3, "cf8ab61a"

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lhpi;->f:Lief;

    .line 69
    new-instance v0, Lief;

    const-string v1, "debug.photos.view_photos_of_me"

    const-string v2, "false"

    const-string v3, "1fc8d78d"

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 82
    new-instance v0, Lief;

    const-string v1, "debug.photos.close_to_quota"

    const-wide/32 v2, 0x40000000

    .line 84
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "5ec52a1"

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 92
    new-instance v0, Lief;

    const-string v1, "debug.plus.turn_off_ab_notif"

    const-string v2, "false"

    const-string v3, "a1f420d7"

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lhpi;->g:Lief;

    return-void
.end method
