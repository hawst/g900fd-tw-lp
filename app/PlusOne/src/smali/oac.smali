.class public final Loac;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 242
    const/4 v0, 0x0

    .line 243
    iget-object v1, p0, Loac;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 244
    const/4 v0, 0x1

    iget-object v1, p0, Loac;->a:Ljava/lang/String;

    .line 245
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 247
    :cond_0
    iget-object v1, p0, Loac;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 248
    const/4 v1, 0x2

    iget-object v2, p0, Loac;->b:Ljava/lang/String;

    .line 249
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    :cond_1
    iget-object v1, p0, Loac;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 252
    const/4 v1, 0x3

    iget-object v2, p0, Loac;->c:Ljava/lang/String;

    .line 253
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_2
    iget-object v1, p0, Loac;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 256
    const/4 v1, 0x4

    iget-object v2, p0, Loac;->d:Ljava/lang/Integer;

    .line 257
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_3
    iget-object v1, p0, Loac;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 260
    const/4 v1, 0x5

    iget-object v2, p0, Loac;->e:Ljava/lang/String;

    .line 261
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_4
    iget-object v1, p0, Loac;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    iput v0, p0, Loac;->ai:I

    .line 265
    return v0
.end method

.method public a(Loxn;)Loac;
    .locals 2

    .prologue
    .line 273
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 274
    sparse-switch v0, :sswitch_data_0

    .line 278
    iget-object v1, p0, Loac;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 279
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loac;->ah:Ljava/util/List;

    .line 282
    :cond_1
    iget-object v1, p0, Loac;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    :sswitch_0
    return-object p0

    .line 289
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loac;->a:Ljava/lang/String;

    goto :goto_0

    .line 293
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loac;->b:Ljava/lang/String;

    goto :goto_0

    .line 297
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loac;->c:Ljava/lang/String;

    goto :goto_0

    .line 301
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loac;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 305
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loac;->e:Ljava/lang/String;

    goto :goto_0

    .line 274
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Loac;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 222
    const/4 v0, 0x1

    iget-object v1, p0, Loac;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 224
    :cond_0
    iget-object v0, p0, Loac;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 225
    const/4 v0, 0x2

    iget-object v1, p0, Loac;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 227
    :cond_1
    iget-object v0, p0, Loac;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 228
    const/4 v0, 0x3

    iget-object v1, p0, Loac;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 230
    :cond_2
    iget-object v0, p0, Loac;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 231
    const/4 v0, 0x4

    iget-object v1, p0, Loac;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 233
    :cond_3
    iget-object v0, p0, Loac;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 234
    const/4 v0, 0x5

    iget-object v1, p0, Loac;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 236
    :cond_4
    iget-object v0, p0, Loac;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 238
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Loac;->a(Loxn;)Loac;

    move-result-object v0

    return-object v0
.end method
