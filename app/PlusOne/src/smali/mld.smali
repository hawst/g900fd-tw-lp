.class public final Lmld;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfx;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24622
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24625
    iput-object v0, p0, Lmld;->apiHeader:Llyr;

    .line 24628
    iput-object v0, p0, Lmld;->a:Lnfx;

    .line 24622
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 24645
    const/4 v0, 0x0

    .line 24646
    iget-object v1, p0, Lmld;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 24647
    const/4 v0, 0x1

    iget-object v1, p0, Lmld;->apiHeader:Llyr;

    .line 24648
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24650
    :cond_0
    iget-object v1, p0, Lmld;->a:Lnfx;

    if-eqz v1, :cond_1

    .line 24651
    const/4 v1, 0x2

    iget-object v2, p0, Lmld;->a:Lnfx;

    .line 24652
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24654
    :cond_1
    iget-object v1, p0, Lmld;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24655
    iput v0, p0, Lmld;->ai:I

    .line 24656
    return v0
.end method

.method public a(Loxn;)Lmld;
    .locals 2

    .prologue
    .line 24664
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24665
    sparse-switch v0, :sswitch_data_0

    .line 24669
    iget-object v1, p0, Lmld;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24670
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmld;->ah:Ljava/util/List;

    .line 24673
    :cond_1
    iget-object v1, p0, Lmld;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24675
    :sswitch_0
    return-object p0

    .line 24680
    :sswitch_1
    iget-object v0, p0, Lmld;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 24681
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmld;->apiHeader:Llyr;

    .line 24683
    :cond_2
    iget-object v0, p0, Lmld;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24687
    :sswitch_2
    iget-object v0, p0, Lmld;->a:Lnfx;

    if-nez v0, :cond_3

    .line 24688
    new-instance v0, Lnfx;

    invoke-direct {v0}, Lnfx;-><init>()V

    iput-object v0, p0, Lmld;->a:Lnfx;

    .line 24690
    :cond_3
    iget-object v0, p0, Lmld;->a:Lnfx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24665
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24633
    iget-object v0, p0, Lmld;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 24634
    const/4 v0, 0x1

    iget-object v1, p0, Lmld;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24636
    :cond_0
    iget-object v0, p0, Lmld;->a:Lnfx;

    if-eqz v0, :cond_1

    .line 24637
    const/4 v0, 0x2

    iget-object v1, p0, Lmld;->a:Lnfx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24639
    :cond_1
    iget-object v0, p0, Lmld;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 24641
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 24618
    invoke-virtual {p0, p1}, Lmld;->a(Loxn;)Lmld;

    move-result-object v0

    return-object v0
.end method
