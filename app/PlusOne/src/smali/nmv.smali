.class public final Lnmv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnmv;


# instance fields
.field public b:I

.field public c:Lnni;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    new-array v0, v0, [Lnmv;

    sput-object v0, Lnmv;->a:[Lnmv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 229
    invoke-direct {p0}, Loxq;-><init>()V

    .line 232
    const/high16 v0, -0x80000000

    iput v0, p0, Lnmv;->b:I

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lnmv;->c:Lnni;

    .line 229
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 252
    const/4 v0, 0x0

    .line 253
    iget v1, p0, Lnmv;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 254
    const/4 v0, 0x1

    iget v1, p0, Lnmv;->b:I

    .line 255
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 257
    :cond_0
    iget-object v1, p0, Lnmv;->c:Lnni;

    if-eqz v1, :cond_1

    .line 258
    const/4 v1, 0x2

    iget-object v2, p0, Lnmv;->c:Lnni;

    .line 259
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_1
    iget-object v1, p0, Lnmv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    iput v0, p0, Lnmv;->ai:I

    .line 263
    return v0
.end method

.method public a(Loxn;)Lnmv;
    .locals 2

    .prologue
    .line 271
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 272
    sparse-switch v0, :sswitch_data_0

    .line 276
    iget-object v1, p0, Lnmv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnmv;->ah:Ljava/util/List;

    .line 280
    :cond_1
    iget-object v1, p0, Lnmv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    :sswitch_0
    return-object p0

    .line 287
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 288
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 290
    :cond_2
    iput v0, p0, Lnmv;->b:I

    goto :goto_0

    .line 292
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnmv;->b:I

    goto :goto_0

    .line 297
    :sswitch_2
    iget-object v0, p0, Lnmv;->c:Lnni;

    if-nez v0, :cond_4

    .line 298
    new-instance v0, Lnni;

    invoke-direct {v0}, Lnni;-><init>()V

    iput-object v0, p0, Lnmv;->c:Lnni;

    .line 300
    :cond_4
    iget-object v0, p0, Lnmv;->c:Lnni;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 272
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 240
    iget v0, p0, Lnmv;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 241
    const/4 v0, 0x1

    iget v1, p0, Lnmv;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 243
    :cond_0
    iget-object v0, p0, Lnmv;->c:Lnni;

    if-eqz v0, :cond_1

    .line 244
    const/4 v0, 0x2

    iget-object v1, p0, Lnmv;->c:Lnni;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 246
    :cond_1
    iget-object v0, p0, Lnmv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 248
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lnmv;->a(Loxn;)Lnmv;

    move-result-object v0

    return-object v0
.end method
