.class public final Lpea;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpea;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:[Lpdz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Lpea;

    sput-object v0, Lpea;->a:[Lpea;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpea;->c:[Ljava/lang/String;

    .line 24
    sget-object v0, Lpdz;->a:[Lpdz;

    iput-object v0, p0, Lpea;->d:[Lpdz;

    .line 16
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 48
    const/4 v0, 0x1

    iget-object v2, p0, Lpea;->b:Ljava/lang/String;

    .line 50
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 51
    iget-object v2, p0, Lpea;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lpea;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 53
    iget-object v4, p0, Lpea;->c:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 55
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 53
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 57
    :cond_0
    add-int/2addr v0, v3

    .line 58
    iget-object v2, p0, Lpea;->c:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 60
    :cond_1
    iget-object v2, p0, Lpea;->d:[Lpdz;

    if-eqz v2, :cond_3

    .line 61
    iget-object v2, p0, Lpea;->d:[Lpdz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 62
    if-eqz v4, :cond_2

    .line 63
    const/4 v5, 0x3

    .line 64
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 61
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 68
    :cond_3
    iget-object v1, p0, Lpea;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    iput v0, p0, Lpea;->ai:I

    .line 70
    return v0
.end method

.method public a(Loxn;)Lpea;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 78
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 79
    sparse-switch v0, :sswitch_data_0

    .line 83
    iget-object v2, p0, Lpea;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 84
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpea;->ah:Ljava/util/List;

    .line 87
    :cond_1
    iget-object v2, p0, Lpea;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    :sswitch_0
    return-object p0

    .line 94
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpea;->b:Ljava/lang/String;

    goto :goto_0

    .line 98
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 99
    iget-object v0, p0, Lpea;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 100
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 101
    iget-object v3, p0, Lpea;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 102
    iput-object v2, p0, Lpea;->c:[Ljava/lang/String;

    .line 103
    :goto_1
    iget-object v2, p0, Lpea;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 104
    iget-object v2, p0, Lpea;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 105
    invoke-virtual {p1}, Loxn;->a()I

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 108
    :cond_2
    iget-object v2, p0, Lpea;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 112
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 113
    iget-object v0, p0, Lpea;->d:[Lpdz;

    if-nez v0, :cond_4

    move v0, v1

    .line 114
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lpdz;

    .line 115
    iget-object v3, p0, Lpea;->d:[Lpdz;

    if-eqz v3, :cond_3

    .line 116
    iget-object v3, p0, Lpea;->d:[Lpdz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    :cond_3
    iput-object v2, p0, Lpea;->d:[Lpdz;

    .line 119
    :goto_3
    iget-object v2, p0, Lpea;->d:[Lpdz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 120
    iget-object v2, p0, Lpea;->d:[Lpdz;

    new-instance v3, Lpdz;

    invoke-direct {v3}, Lpdz;-><init>()V

    aput-object v3, v2, v0

    .line 121
    iget-object v2, p0, Lpea;->d:[Lpdz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 122
    invoke-virtual {p1}, Loxn;->a()I

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 113
    :cond_4
    iget-object v0, p0, Lpea;->d:[Lpdz;

    array-length v0, v0

    goto :goto_2

    .line 125
    :cond_5
    iget-object v2, p0, Lpea;->d:[Lpdz;

    new-instance v3, Lpdz;

    invoke-direct {v3}, Lpdz;-><init>()V

    aput-object v3, v2, v0

    .line 126
    iget-object v2, p0, Lpea;->d:[Lpdz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 79
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 29
    const/4 v1, 0x1

    iget-object v2, p0, Lpea;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 30
    iget-object v1, p0, Lpea;->c:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31
    iget-object v2, p0, Lpea;->c:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 32
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 31
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    :cond_0
    iget-object v1, p0, Lpea;->d:[Lpdz;

    if-eqz v1, :cond_2

    .line 36
    iget-object v1, p0, Lpea;->d:[Lpdz;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 37
    if-eqz v3, :cond_1

    .line 38
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 36
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_2
    iget-object v0, p0, Lpea;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 44
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lpea;->a(Loxn;)Lpea;

    move-result-object v0

    return-object v0
.end method
