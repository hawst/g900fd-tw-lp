.class public final Llwb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llwb;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Integer;

.field public d:Llwa;

.field private e:Ljava/lang/Double;

.field private f:Ljava/lang/String;

.field private g:Llwd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    new-array v0, v0, [Llwb;

    sput-object v0, Llwb;->a:[Llwb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26
    iput-object v0, p0, Llwb;->g:Llwd;

    .line 29
    iput-object v0, p0, Llwb;->d:Llwa;

    .line 15
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    iget-object v1, p0, Llwb;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 60
    const/4 v0, 0x1

    iget-object v1, p0, Llwb;->b:Ljava/lang/Boolean;

    .line 61
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 63
    :cond_0
    iget-object v1, p0, Llwb;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 64
    const/4 v1, 0x2

    iget-object v2, p0, Llwb;->c:Ljava/lang/Integer;

    .line 65
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_1
    iget-object v1, p0, Llwb;->e:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 68
    const/4 v1, 0x3

    iget-object v2, p0, Llwb;->e:Ljava/lang/Double;

    .line 69
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 71
    :cond_2
    iget-object v1, p0, Llwb;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 72
    const/4 v1, 0x4

    iget-object v2, p0, Llwb;->f:Ljava/lang/String;

    .line 73
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_3
    iget-object v1, p0, Llwb;->g:Llwd;

    if-eqz v1, :cond_4

    .line 76
    const/4 v1, 0x5

    iget-object v2, p0, Llwb;->g:Llwd;

    .line 77
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_4
    iget-object v1, p0, Llwb;->d:Llwa;

    if-eqz v1, :cond_5

    .line 80
    const/4 v1, 0x6

    iget-object v2, p0, Llwb;->d:Llwa;

    .line 81
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_5
    iget-object v1, p0, Llwb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    iput v0, p0, Llwb;->ai:I

    .line 85
    return v0
.end method

.method public a(Loxn;)Llwb;
    .locals 2

    .prologue
    .line 93
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 94
    sparse-switch v0, :sswitch_data_0

    .line 98
    iget-object v1, p0, Llwb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llwb;->ah:Ljava/util/List;

    .line 102
    :cond_1
    iget-object v1, p0, Llwb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    :sswitch_0
    return-object p0

    .line 109
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llwb;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 113
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Llwb;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 117
    :sswitch_3
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Llwb;->e:Ljava/lang/Double;

    goto :goto_0

    .line 121
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llwb;->f:Ljava/lang/String;

    goto :goto_0

    .line 125
    :sswitch_5
    iget-object v0, p0, Llwb;->g:Llwd;

    if-nez v0, :cond_2

    .line 126
    new-instance v0, Llwd;

    invoke-direct {v0}, Llwd;-><init>()V

    iput-object v0, p0, Llwb;->g:Llwd;

    .line 128
    :cond_2
    iget-object v0, p0, Llwb;->g:Llwd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 132
    :sswitch_6
    iget-object v0, p0, Llwb;->d:Llwa;

    if-nez v0, :cond_3

    .line 133
    new-instance v0, Llwa;

    invoke-direct {v0}, Llwa;-><init>()V

    iput-object v0, p0, Llwb;->d:Llwa;

    .line 135
    :cond_3
    iget-object v0, p0, Llwb;->d:Llwa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 94
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Llwb;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 35
    const/4 v0, 0x1

    iget-object v1, p0, Llwb;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 37
    :cond_0
    iget-object v0, p0, Llwb;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 38
    const/4 v0, 0x2

    iget-object v1, p0, Llwb;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 40
    :cond_1
    iget-object v0, p0, Llwb;->e:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 41
    const/4 v0, 0x3

    iget-object v1, p0, Llwb;->e:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 43
    :cond_2
    iget-object v0, p0, Llwb;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 44
    const/4 v0, 0x4

    iget-object v1, p0, Llwb;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_3
    iget-object v0, p0, Llwb;->g:Llwd;

    if-eqz v0, :cond_4

    .line 47
    const/4 v0, 0x5

    iget-object v1, p0, Llwb;->g:Llwd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 49
    :cond_4
    iget-object v0, p0, Llwb;->d:Llwa;

    if-eqz v0, :cond_5

    .line 50
    const/4 v0, 0x6

    iget-object v1, p0, Llwb;->d:Llwa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 52
    :cond_5
    iget-object v0, p0, Llwb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 54
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Llwb;->a(Loxn;)Llwb;

    move-result-object v0

    return-object v0
.end method
