.class public final Loub;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Louz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 923
    invoke-direct {p0}, Loxq;-><init>()V

    .line 926
    sget-object v0, Louz;->a:[Louz;

    iput-object v0, p0, Loub;->a:[Louz;

    .line 923
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 944
    .line 945
    iget-object v1, p0, Loub;->a:[Louz;

    if-eqz v1, :cond_1

    .line 946
    iget-object v2, p0, Loub;->a:[Louz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 947
    if-eqz v4, :cond_0

    .line 948
    const/4 v5, 0x1

    .line 949
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 946
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 953
    :cond_1
    iget-object v1, p0, Loub;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 954
    iput v0, p0, Loub;->ai:I

    .line 955
    return v0
.end method

.method public a(Loxn;)Loub;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 963
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 964
    sparse-switch v0, :sswitch_data_0

    .line 968
    iget-object v2, p0, Loub;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 969
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loub;->ah:Ljava/util/List;

    .line 972
    :cond_1
    iget-object v2, p0, Loub;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 974
    :sswitch_0
    return-object p0

    .line 979
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 980
    iget-object v0, p0, Loub;->a:[Louz;

    if-nez v0, :cond_3

    move v0, v1

    .line 981
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Louz;

    .line 982
    iget-object v3, p0, Loub;->a:[Louz;

    if-eqz v3, :cond_2

    .line 983
    iget-object v3, p0, Loub;->a:[Louz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 985
    :cond_2
    iput-object v2, p0, Loub;->a:[Louz;

    .line 986
    :goto_2
    iget-object v2, p0, Loub;->a:[Louz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 987
    iget-object v2, p0, Loub;->a:[Louz;

    new-instance v3, Louz;

    invoke-direct {v3}, Louz;-><init>()V

    aput-object v3, v2, v0

    .line 988
    iget-object v2, p0, Loub;->a:[Louz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 989
    invoke-virtual {p1}, Loxn;->a()I

    .line 986
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 980
    :cond_3
    iget-object v0, p0, Loub;->a:[Louz;

    array-length v0, v0

    goto :goto_1

    .line 992
    :cond_4
    iget-object v2, p0, Loub;->a:[Louz;

    new-instance v3, Louz;

    invoke-direct {v3}, Louz;-><init>()V

    aput-object v3, v2, v0

    .line 993
    iget-object v2, p0, Loub;->a:[Louz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 964
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 931
    iget-object v0, p0, Loub;->a:[Louz;

    if-eqz v0, :cond_1

    .line 932
    iget-object v1, p0, Loub;->a:[Louz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 933
    if-eqz v3, :cond_0

    .line 934
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 932
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 938
    :cond_1
    iget-object v0, p0, Loub;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 940
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 919
    invoke-virtual {p0, p1}, Loub;->a(Loxn;)Loub;

    move-result-object v0

    return-object v0
.end method
