.class public final Leuk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llqz;
.implements Llrc;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lhee;

.field private c:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Leuk;->a:Landroid/app/Activity;

    .line 39
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 40
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leuk;->b:Lhee;

    .line 45
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 49
    if-nez p1, :cond_0

    .line 50
    iget-object v0, p0, Leuk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.notifications.notif_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leuk;->c:Ljava/lang/String;

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    const-string v0, "notification_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leuk;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 65
    iget-object v0, p0, Leuk;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leuk;->b:Lhee;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leuk;->b:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Leuk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 67
    const-string v1, "com.google.android.libraries.social.notifications.updated_version"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 68
    const-string v1, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    .line 69
    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 71
    new-instance v0, Ldph;

    iget-object v1, p0, Leuk;->a:Landroid/app/Activity;

    iget-object v2, p0, Leuk;->b:Lhee;

    .line 72
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Leuk;->c:Ljava/lang/String;

    const/4 v7, 0x1

    const-string v9, "GPLUS_APP_V3"

    invoke-direct/range {v0 .. v9}, Ldph;-><init>(Landroid/content/Context;ILjava/lang/String;JZZZLjava/lang/String;)V

    .line 75
    iget-object v1, p0, Leuk;->a:Landroid/app/Activity;

    invoke-static {v1, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Leuk;->c:Ljava/lang/String;

    .line 80
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 59
    const-string v0, "notification_id"

    iget-object v1, p0, Leuk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-void
.end method
