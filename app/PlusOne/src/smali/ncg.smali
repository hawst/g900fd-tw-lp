.class public final Lncg;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 163
    invoke-direct {p0}, Loxq;-><init>()V

    .line 168
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lncg;->b:[I

    .line 163
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 187
    .line 188
    iget-object v0, p0, Lncg;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 189
    const/4 v0, 0x1

    iget-object v2, p0, Lncg;->a:Ljava/lang/Boolean;

    .line 190
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 192
    :goto_0
    iget-object v2, p0, Lncg;->b:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lncg;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 194
    iget-object v3, p0, Lncg;->b:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 196
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 198
    :cond_0
    add-int/2addr v0, v2

    .line 199
    iget-object v1, p0, Lncg;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 201
    :cond_1
    iget-object v1, p0, Lncg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 202
    iput v0, p0, Lncg;->ai:I

    .line 203
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lncg;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 211
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 212
    sparse-switch v0, :sswitch_data_0

    .line 216
    iget-object v1, p0, Lncg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 217
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lncg;->ah:Ljava/util/List;

    .line 220
    :cond_1
    iget-object v1, p0, Lncg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    :sswitch_0
    return-object p0

    .line 227
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lncg;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 231
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 232
    iget-object v0, p0, Lncg;->b:[I

    array-length v0, v0

    .line 233
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 234
    iget-object v2, p0, Lncg;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 235
    iput-object v1, p0, Lncg;->b:[I

    .line 236
    :goto_1
    iget-object v1, p0, Lncg;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 237
    iget-object v1, p0, Lncg;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 238
    invoke-virtual {p1}, Loxn;->a()I

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 241
    :cond_2
    iget-object v1, p0, Lncg;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 212
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 173
    iget-object v0, p0, Lncg;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x1

    iget-object v1, p0, Lncg;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 176
    :cond_0
    iget-object v0, p0, Lncg;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lncg;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 177
    iget-object v1, p0, Lncg;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 178
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_1
    iget-object v0, p0, Lncg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 183
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0, p1}, Lncg;->a(Loxn;)Lncg;

    move-result-object v0

    return-object v0
.end method
