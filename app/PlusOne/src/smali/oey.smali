.class public final Loey;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loey;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7739
    const/4 v0, 0x0

    new-array v0, v0, [Loey;

    sput-object v0, Loey;->a:[Loey;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7740
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7776
    const/4 v0, 0x0

    .line 7777
    iget-object v1, p0, Loey;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7778
    const/4 v0, 0x1

    iget-object v1, p0, Loey;->b:Ljava/lang/String;

    .line 7779
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7781
    :cond_0
    iget-object v1, p0, Loey;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 7782
    const/4 v1, 0x2

    iget-object v2, p0, Loey;->c:Ljava/lang/String;

    .line 7783
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7785
    :cond_1
    iget-object v1, p0, Loey;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 7786
    const/4 v1, 0x3

    iget-object v2, p0, Loey;->d:Ljava/lang/String;

    .line 7787
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7789
    :cond_2
    iget-object v1, p0, Loey;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 7790
    const/4 v1, 0x4

    iget-object v2, p0, Loey;->e:Ljava/lang/String;

    .line 7791
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7793
    :cond_3
    iget-object v1, p0, Loey;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 7794
    const/4 v1, 0x5

    iget-object v2, p0, Loey;->f:Ljava/lang/Boolean;

    .line 7795
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7797
    :cond_4
    iget-object v1, p0, Loey;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7798
    iput v0, p0, Loey;->ai:I

    .line 7799
    return v0
.end method

.method public a(Loxn;)Loey;
    .locals 2

    .prologue
    .line 7807
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7808
    sparse-switch v0, :sswitch_data_0

    .line 7812
    iget-object v1, p0, Loey;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7813
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loey;->ah:Ljava/util/List;

    .line 7816
    :cond_1
    iget-object v1, p0, Loey;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7818
    :sswitch_0
    return-object p0

    .line 7823
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loey;->b:Ljava/lang/String;

    goto :goto_0

    .line 7827
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loey;->c:Ljava/lang/String;

    goto :goto_0

    .line 7831
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loey;->d:Ljava/lang/String;

    goto :goto_0

    .line 7835
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loey;->e:Ljava/lang/String;

    goto :goto_0

    .line 7839
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loey;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 7808
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7755
    iget-object v0, p0, Loey;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 7756
    const/4 v0, 0x1

    iget-object v1, p0, Loey;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7758
    :cond_0
    iget-object v0, p0, Loey;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 7759
    const/4 v0, 0x2

    iget-object v1, p0, Loey;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7761
    :cond_1
    iget-object v0, p0, Loey;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 7762
    const/4 v0, 0x3

    iget-object v1, p0, Loey;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7764
    :cond_2
    iget-object v0, p0, Loey;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 7765
    const/4 v0, 0x4

    iget-object v1, p0, Loey;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7767
    :cond_3
    iget-object v0, p0, Loey;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 7768
    const/4 v0, 0x5

    iget-object v1, p0, Loey;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7770
    :cond_4
    iget-object v0, p0, Loey;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7772
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7736
    invoke-virtual {p0, p1}, Loey;->a(Loxn;)Loey;

    move-result-object v0

    return-object v0
.end method
