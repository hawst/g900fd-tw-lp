.class public final Lnjh;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnjh;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5932
    const/4 v0, 0x0

    new-array v0, v0, [Lnjh;

    sput-object v0, Lnjh;->a:[Lnjh;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5933
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5969
    const/4 v0, 0x0

    .line 5970
    iget-object v1, p0, Lnjh;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5971
    const/4 v0, 0x1

    iget-object v1, p0, Lnjh;->b:Ljava/lang/String;

    .line 5972
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5974
    :cond_0
    iget-object v1, p0, Lnjh;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5975
    const/4 v1, 0x2

    iget-object v2, p0, Lnjh;->c:Ljava/lang/String;

    .line 5976
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5978
    :cond_1
    iget-object v1, p0, Lnjh;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 5979
    const/4 v1, 0x3

    iget-object v2, p0, Lnjh;->d:Ljava/lang/Boolean;

    .line 5980
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5982
    :cond_2
    iget-object v1, p0, Lnjh;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 5983
    const/4 v1, 0x4

    iget-object v2, p0, Lnjh;->e:Ljava/lang/Boolean;

    .line 5984
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5986
    :cond_3
    iget-object v1, p0, Lnjh;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 5987
    const/4 v1, 0x5

    iget-object v2, p0, Lnjh;->f:Ljava/lang/Integer;

    .line 5988
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5990
    :cond_4
    iget-object v1, p0, Lnjh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5991
    iput v0, p0, Lnjh;->ai:I

    .line 5992
    return v0
.end method

.method public a(Loxn;)Lnjh;
    .locals 2

    .prologue
    .line 6000
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6001
    sparse-switch v0, :sswitch_data_0

    .line 6005
    iget-object v1, p0, Lnjh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6006
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjh;->ah:Ljava/util/List;

    .line 6009
    :cond_1
    iget-object v1, p0, Lnjh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6011
    :sswitch_0
    return-object p0

    .line 6016
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjh;->b:Ljava/lang/String;

    goto :goto_0

    .line 6020
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjh;->c:Ljava/lang/String;

    goto :goto_0

    .line 6024
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnjh;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 6028
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnjh;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 6032
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnjh;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 6001
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5948
    iget-object v0, p0, Lnjh;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5949
    const/4 v0, 0x1

    iget-object v1, p0, Lnjh;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5951
    :cond_0
    iget-object v0, p0, Lnjh;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5952
    const/4 v0, 0x2

    iget-object v1, p0, Lnjh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5954
    :cond_1
    iget-object v0, p0, Lnjh;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 5955
    const/4 v0, 0x3

    iget-object v1, p0, Lnjh;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5957
    :cond_2
    iget-object v0, p0, Lnjh;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 5958
    const/4 v0, 0x4

    iget-object v1, p0, Lnjh;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5960
    :cond_3
    iget-object v0, p0, Lnjh;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 5961
    const/4 v0, 0x5

    iget-object v1, p0, Lnjh;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5963
    :cond_4
    iget-object v0, p0, Lnjh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5965
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5929
    invoke-virtual {p0, p1}, Lnjh;->a(Loxn;)Lnjh;

    move-result-object v0

    return-object v0
.end method
