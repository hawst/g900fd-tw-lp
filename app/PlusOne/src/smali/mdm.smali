.class public final Lmdm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lncd;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21463
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21466
    iput-object v0, p0, Lmdm;->apiHeader:Llyq;

    .line 21469
    iput-object v0, p0, Lmdm;->a:Lncd;

    .line 21463
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 21486
    const/4 v0, 0x0

    .line 21487
    iget-object v1, p0, Lmdm;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 21488
    const/4 v0, 0x1

    iget-object v1, p0, Lmdm;->apiHeader:Llyq;

    .line 21489
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21491
    :cond_0
    iget-object v1, p0, Lmdm;->a:Lncd;

    if-eqz v1, :cond_1

    .line 21492
    const/4 v1, 0x2

    iget-object v2, p0, Lmdm;->a:Lncd;

    .line 21493
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21495
    :cond_1
    iget-object v1, p0, Lmdm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21496
    iput v0, p0, Lmdm;->ai:I

    .line 21497
    return v0
.end method

.method public a(Loxn;)Lmdm;
    .locals 2

    .prologue
    .line 21505
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 21506
    sparse-switch v0, :sswitch_data_0

    .line 21510
    iget-object v1, p0, Lmdm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 21511
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdm;->ah:Ljava/util/List;

    .line 21514
    :cond_1
    iget-object v1, p0, Lmdm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21516
    :sswitch_0
    return-object p0

    .line 21521
    :sswitch_1
    iget-object v0, p0, Lmdm;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 21522
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdm;->apiHeader:Llyq;

    .line 21524
    :cond_2
    iget-object v0, p0, Lmdm;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21528
    :sswitch_2
    iget-object v0, p0, Lmdm;->a:Lncd;

    if-nez v0, :cond_3

    .line 21529
    new-instance v0, Lncd;

    invoke-direct {v0}, Lncd;-><init>()V

    iput-object v0, p0, Lmdm;->a:Lncd;

    .line 21531
    :cond_3
    iget-object v0, p0, Lmdm;->a:Lncd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21506
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 21474
    iget-object v0, p0, Lmdm;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 21475
    const/4 v0, 0x1

    iget-object v1, p0, Lmdm;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21477
    :cond_0
    iget-object v0, p0, Lmdm;->a:Lncd;

    if-eqz v0, :cond_1

    .line 21478
    const/4 v0, 0x2

    iget-object v1, p0, Lmdm;->a:Lncd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21480
    :cond_1
    iget-object v0, p0, Lmdm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 21482
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 21459
    invoke-virtual {p0, p1}, Lmdm;->a(Loxn;)Lmdm;

    move-result-object v0

    return-object v0
.end method
