.class public final Lesb;
.super Lfvi;
.source "PG"


# instance fields
.field private b:Lerj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lfvq;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lfvi;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lfvq;)V

    .line 24
    return-void
.end method

.method static synthetic a(Lesb;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lesb;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 114
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v1

    .line 130
    :goto_0
    return v0

    :cond_1
    move-object p1, v0

    .line 119
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_3

    .line 120
    if-nez v0, :cond_1

    move v0, v1

    .line 122
    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 130
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lesb;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lesb;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic c(Lesb;)Lerj;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lesb;->b:Lerj;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/widget/ListView;ILjava/util/HashMap;)Ljava/lang/Float;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;)",
            "Ljava/lang/Float;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lesb;->b:Lerj;

    invoke-virtual {v0, p2}, Lerj;->getItemId(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 136
    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 34
    new-instance v0, Lesc;

    invoke-direct {v0, p0}, Lesc;-><init>(Lesb;)V

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 54
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 60
    if-eqz p1, :cond_0

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lesb;->a(Landroid/view/View;Z)V

    .line 62
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v3, v0

    const-wide/16 v4, 0xc8

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lesb;->a(Landroid/view/View;FJZ)V

    .line 64
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 144
    if-eqz p1, :cond_1

    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 146
    if-eqz v0, :cond_1

    .line 147
    const v1, 0x7f1003b4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 148
    if-eqz v4, :cond_0

    .line 149
    if-eqz p2, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 152
    :cond_0
    const v1, 0x7f1003b5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_1

    .line 154
    if-eqz p2, :cond_3

    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 158
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 149
    goto :goto_0

    :cond_3
    move v3, v2

    .line 154
    goto :goto_1
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 180
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 183
    :cond_0
    return-void
.end method

.method public a(Lerj;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lesb;->b:Lerj;

    .line 28
    return-void
.end method

.method public a(F)Z
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 70
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 80
    invoke-virtual {p0, p1}, Lesb;->b(Landroid/view/View;)Landroid/view/View;

    move-result-object v3

    .line 81
    if-nez v3, :cond_0

    .line 108
    :goto_0
    return-void

    .line 85
    :cond_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 87
    iget-object v0, p0, Lesb;->a:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/ListView;

    .line 88
    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v5

    move v1, v2

    .line 89
    :goto_1
    iget-object v6, p0, Lesb;->a:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 90
    iget-object v6, p0, Lesb;->a:Landroid/view/ViewGroup;

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 91
    add-int v7, v5, v1

    .line 92
    iget-object v8, p0, Lesb;->b:Lerj;

    invoke-virtual {v8, v7}, Lerj;->getItemId(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 93
    if-eq v6, v3, :cond_1

    .line 94
    invoke-virtual {p0, v6}, Lesb;->f(Landroid/view/View;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 99
    :cond_2
    iget-object v1, p0, Lesb;->a:Landroid/view/ViewGroup;

    invoke-static {v1, v3}, Lesb;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 100
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 101
    iget-object v3, p0, Lesb;->b:Lerj;

    instance-of v3, v3, Lerj;

    if-eqz v3, :cond_3

    .line 102
    iget-object v3, p0, Lesb;->b:Lerj;

    invoke-virtual {v3, v1}, Lerj;->b(I)V

    .line 103
    invoke-virtual {p0, v0, v4, v2}, Lesb;->a(Landroid/widget/ListView;Ljava/util/HashMap;Z)V

    .line 107
    :cond_3
    invoke-virtual {p0}, Lesb;->c()V

    goto :goto_0
.end method

.method public d(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    return v0
.end method
