.class public final Lmxo;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmxp;

.field private b:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 249
    invoke-direct {p0}, Loxq;-><init>()V

    .line 459
    sget-object v0, Lmxp;->a:[Lmxp;

    iput-object v0, p0, Lmxo;->a:[Lmxp;

    .line 462
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lmxo;->b:[Ljava/lang/Integer;

    .line 249
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 485
    .line 486
    iget-object v0, p0, Lmxo;->a:[Lmxp;

    if-eqz v0, :cond_1

    .line 487
    iget-object v3, p0, Lmxo;->a:[Lmxp;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 488
    if-eqz v5, :cond_0

    .line 489
    const/4 v6, 0x1

    .line 490
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 487
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 494
    :cond_2
    iget-object v2, p0, Lmxo;->b:[Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lmxo;->b:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 496
    iget-object v3, p0, Lmxo;->b:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 498
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 496
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 500
    :cond_3
    add-int/2addr v0, v2

    .line 501
    iget-object v1, p0, Lmxo;->b:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 503
    :cond_4
    iget-object v1, p0, Lmxo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 504
    iput v0, p0, Lmxo;->ai:I

    .line 505
    return v0
.end method

.method public a(Loxn;)Lmxo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 513
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 514
    sparse-switch v0, :sswitch_data_0

    .line 518
    iget-object v2, p0, Lmxo;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 519
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmxo;->ah:Ljava/util/List;

    .line 522
    :cond_1
    iget-object v2, p0, Lmxo;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    :sswitch_0
    return-object p0

    .line 529
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 530
    iget-object v0, p0, Lmxo;->a:[Lmxp;

    if-nez v0, :cond_3

    move v0, v1

    .line 531
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmxp;

    .line 532
    iget-object v3, p0, Lmxo;->a:[Lmxp;

    if-eqz v3, :cond_2

    .line 533
    iget-object v3, p0, Lmxo;->a:[Lmxp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 535
    :cond_2
    iput-object v2, p0, Lmxo;->a:[Lmxp;

    .line 536
    :goto_2
    iget-object v2, p0, Lmxo;->a:[Lmxp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 537
    iget-object v2, p0, Lmxo;->a:[Lmxp;

    new-instance v3, Lmxp;

    invoke-direct {v3}, Lmxp;-><init>()V

    aput-object v3, v2, v0

    .line 538
    iget-object v2, p0, Lmxo;->a:[Lmxp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 539
    invoke-virtual {p1}, Loxn;->a()I

    .line 536
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 530
    :cond_3
    iget-object v0, p0, Lmxo;->a:[Lmxp;

    array-length v0, v0

    goto :goto_1

    .line 542
    :cond_4
    iget-object v2, p0, Lmxo;->a:[Lmxp;

    new-instance v3, Lmxp;

    invoke-direct {v3}, Lmxp;-><init>()V

    aput-object v3, v2, v0

    .line 543
    iget-object v2, p0, Lmxo;->a:[Lmxp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 547
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 548
    iget-object v0, p0, Lmxo;->b:[Ljava/lang/Integer;

    array-length v0, v0

    .line 549
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    .line 550
    iget-object v3, p0, Lmxo;->b:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 551
    iput-object v2, p0, Lmxo;->b:[Ljava/lang/Integer;

    .line 552
    :goto_3
    iget-object v2, p0, Lmxo;->b:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 553
    iget-object v2, p0, Lmxo;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 554
    invoke-virtual {p1}, Loxn;->a()I

    .line 552
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 557
    :cond_5
    iget-object v2, p0, Lmxo;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 514
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 467
    iget-object v1, p0, Lmxo;->a:[Lmxp;

    if-eqz v1, :cond_1

    .line 468
    iget-object v2, p0, Lmxo;->a:[Lmxp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 469
    if-eqz v4, :cond_0

    .line 470
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 468
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 474
    :cond_1
    iget-object v1, p0, Lmxo;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 475
    iget-object v1, p0, Lmxo;->b:[Ljava/lang/Integer;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 476
    const/4 v4, 0x2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 475
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 479
    :cond_2
    iget-object v0, p0, Lmxo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 481
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0, p1}, Lmxo;->a(Loxn;)Lmxo;

    move-result-object v0

    return-object v0
.end method
