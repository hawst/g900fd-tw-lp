.class public final Leuu;
.super Lhyd;
.source "PG"


# static fields
.field private static e:Landroid/graphics/Bitmap;

.field private static f:Landroid/graphics/Bitmap;

.field private static g:Landroid/graphics/Bitmap;

.field private static h:Landroid/graphics/Bitmap;

.field private static i:Landroid/graphics/Bitmap;

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:I


# instance fields
.field private final n:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final o:I

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Leuu;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 77
    iput p3, p0, Leuu;->o:I

    .line 78
    iput-object p4, p0, Leuu;->q:Ljava/lang/String;

    .line 80
    sget-object v0, Leuu;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 82
    const v1, 0x7f0d0359

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Leuu;->j:I

    .line 84
    const v1, 0x7f0d035b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Leuu;->k:I

    .line 86
    const v1, 0x7f0d035a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Leuu;->m:I

    .line 88
    const v1, 0x7f0d035c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Leuu;->l:I

    .line 90
    const v1, 0x7f020311

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Leuu;->e:Landroid/graphics/Bitmap;

    .line 91
    const v1, 0x7f020182

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Leuu;->f:Landroid/graphics/Bitmap;

    .line 92
    const v1, 0x7f02030b

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Leuu;->g:Landroid/graphics/Bitmap;

    .line 93
    const v1, 0x7f0201a7

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Leuu;->h:Landroid/graphics/Bitmap;

    .line 94
    const v1, 0x7f0201cb

    .line 95
    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Leuu;->i:Landroid/graphics/Bitmap;

    .line 97
    :cond_0
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 254
    if-nez p0, :cond_0

    .line 292
    :goto_0
    return-void

    .line 263
    :cond_0
    packed-switch p1, :pswitch_data_0

    move v1, v0

    move v2, v0

    move v3, v0

    .line 291
    :goto_1
    invoke-virtual {p0, v3, v2, v1, v0}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 265
    :pswitch_0
    sget v3, Leuu;->j:I

    .line 266
    sget v1, Leuu;->m:I

    .line 267
    sget v2, Leuu;->k:I

    goto :goto_1

    .line 271
    :pswitch_1
    sget v2, Leuu;->k:I

    .line 272
    sget v3, Leuu;->j:I

    .line 273
    sget v1, Leuu;->m:I

    goto :goto_1

    .line 277
    :pswitch_2
    sget v0, Leuu;->k:I

    .line 278
    :pswitch_3
    sget v3, Leuu;->j:I

    .line 285
    sget v2, Leuu;->m:I

    .line 286
    sget v1, Leuu;->l:I

    move v4, v1

    move v1, v2

    move v2, v0

    move v0, v4

    goto :goto_1

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Leuu;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Leuu;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 183
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 184
    const v1, 0x7f04003d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Leuu;->r:Landroid/view/View$OnClickListener;

    .line 101
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 105
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Leuu;->getCount()I

    move-result v3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {p1, v0}, Leuu;->a(Landroid/view/View;I)V

    .line 107
    const v0, 0x7f100117

    .line 108
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 109
    const/4 v3, 0x5

    .line 110
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v6, Ljac;->a:Ljac;

    invoke-static {p2, v3, v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v3

    .line 109
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 113
    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 115
    const v0, 0x7f100118

    .line 116
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 118
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    const/4 v0, 0x6

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 122
    iget-object v3, p0, Leuu;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f11003b

    new-array v2, v2, [Ljava/lang/Object;

    .line 123
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v4

    .line 122
    invoke-virtual {v3, v7, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 124
    const v0, 0x7f100185

    .line 125
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 127
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    invoke-interface {p3, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    move v3, v0

    .line 133
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 157
    const/4 v0, 0x0

    move-object v2, v0

    .line 160
    :goto_3
    const v0, 0x7f100184

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 161
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 162
    if-eqz v2, :cond_4

    :goto_4
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 164
    const v0, 0x7f10007b

    invoke-virtual {p1, v0, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 165
    const v0, 0x7f10007c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 166
    const v0, 0x7f10007f

    .line 167
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 166
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 170
    const/16 v0, 0xa

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    .line 172
    invoke-static {v0}, Ljux;->a([B)Ljux;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Ljux;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    const v1, 0x7f10007d

    invoke-virtual {v0}, Ljux;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 178
    :cond_0
    iget-object v0, p0, Leuu;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    return-void

    :cond_1
    move v0, v2

    .line 105
    goto/16 :goto_0

    :cond_2
    move v3, v5

    .line 119
    goto/16 :goto_1

    .line 131
    :cond_3
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    move v3, v0

    goto :goto_2

    .line 137
    :pswitch_0
    sget-object v0, Leuu;->g:Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 138
    goto :goto_3

    .line 141
    :pswitch_1
    sget-object v0, Leuu;->i:Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 142
    goto :goto_3

    .line 145
    :pswitch_2
    sget-object v0, Leuu;->f:Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 146
    goto :goto_3

    .line 149
    :pswitch_3
    sget-object v0, Leuu;->e:Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 150
    goto :goto_3

    .line 153
    :pswitch_4
    sget-object v0, Leuu;->h:Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 154
    goto :goto_3

    :cond_4
    move v4, v5

    .line 162
    goto :goto_4

    .line 133
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 194
    if-nez p1, :cond_0

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 195
    :goto_0
    const-string v1, "resume_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leuu;->p:Ljava/lang/String;

    .line 197
    invoke-super {p0, p1}, Lhyd;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 194
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 203
    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 206
    iget-object v1, p0, Leuu;->p:Ljava/lang/String;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 211
    if-eqz p1, :cond_0

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0}, Leuu;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 212
    :cond_0
    const/4 v0, -0x1

    .line 214
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 220
    iget-object v1, p0, Leuu;->p:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-super {p0}, Lhyd;->getCount()I

    move-result v1

    sub-int/2addr v1, p1

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_0

    .line 221
    iget-object v1, p0, Leuu;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    new-instance v1, Leuv;

    iget-object v2, p0, Leuu;->c:Landroid/content/Context;

    iget v3, p0, Leuu;->o:I

    iget-object v4, p0, Leuu;->q:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p0, v4}, Leuv;-><init>(Landroid/content/Context;ILeuu;Ljava/lang/String;)V

    .line 224
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_1

    .line 225
    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Leuu;->p:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 232
    :cond_0
    :goto_0
    if-nez p1, :cond_3

    .line 233
    iget-object v1, p0, Leuu;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04003f

    invoke-virtual {v1, v2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 235
    iget-object v2, p0, Leuu;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    invoke-virtual {p0}, Leuu;->getCount()I

    move-result v2

    if-le v2, v5, :cond_2

    :goto_1
    invoke-static {v1, v0}, Leuu;->a(Landroid/view/View;I)V

    move-object v0, v1

    .line 250
    :goto_2
    return-object v0

    .line 227
    :cond_1
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v5, [Ljava/lang/String;

    iget-object v4, p0, Leuu;->p:Ljava/lang/String;

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 236
    :cond_2
    const/4 v0, 0x3

    goto :goto_1

    .line 241
    :cond_3
    iget-object v1, p0, Leuu;->p:Ljava/lang/String;

    if-eqz v1, :cond_4

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0}, Leuu;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 242
    iget-object v1, p0, Leuu;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04003e

    invoke-virtual {v1, v2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 244
    const/4 v1, 0x2

    invoke-static {v0, v1}, Leuu;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 250
    :cond_4
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0, p2, p3}, Lhyd;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    return v0
.end method
