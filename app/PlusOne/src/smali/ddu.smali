.class public final Lddu;
.super Lddr;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lddu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final D:Ljava/lang/String;

.field private final E:I

.field private final F:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lddv;

    invoke-direct {v0}, Lddv;-><init>()V

    sput-object v0, Lddu;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lddr;-><init>(Landroid/os/Parcel;)V

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddu;->D:Ljava/lang/String;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lddu;->E:I

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lddu;->F:I

    .line 120
    return-void
.end method

.method protected constructor <init>(Ldds;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lddr;-><init>(Ldds;)V

    .line 109
    check-cast p1, Lddw;

    .line 110
    iget-object v0, p1, Lddw;->a:Ljava/lang/String;

    iput-object v0, p0, Lddu;->D:Ljava/lang/String;

    .line 111
    iget v0, p1, Lddw;->b:I

    iput v0, p0, Lddu;->E:I

    .line 112
    iget v0, p1, Lddw;->c:I

    iput v0, p0, Lddu;->F:I

    .line 113
    return-void
.end method

.method static synthetic a(Lddu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lddu;->D:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lddu;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lddu;->E:I

    return v0
.end method

.method static synthetic c(Lddu;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lddu;->F:I

    return v0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    return v0
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x1

    return v0
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public L()Z
    .locals 1

    .prologue
    .line 188
    invoke-super {p0}, Lddr;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lddu;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public P()Z
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lddu;->c:Ljava/util/EnumMap;

    sget-object v1, Lddt;->a:Lddt;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lddu;->c:Ljava/util/EnumMap;

    sget-object v1, Lddt;->a:Lddt;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v0

    .line 200
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lddr;->P()Z

    move-result v0

    goto :goto_0
.end method

.method public S()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lddu;->E:I

    return v0
.end method

.method public T()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lddu;->F:I

    return v0
.end method

.method public V()Ldds;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lddw;

    invoke-direct {v0}, Lddw;-><init>()V

    return-object v0
.end method

.method public a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p0}, Lddu;->e()Lnzi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lddu;->a(Landroid/content/Context;ILnzi;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lddu;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "image/*"

    invoke-virtual {p0, p1, v0, v1}, Lddu;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljuf;
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lddu;->i:Ljcn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lddu;->D:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 138
    :cond_0
    const/4 v0, 0x0

    .line 142
    :goto_0
    return-object v0

    .line 141
    :cond_1
    iget-object v0, p0, Lddu;->i:Ljcn;

    new-instance v1, Ljug;

    .line 142
    iget-object v2, p0, Lddu;->D:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljue;

    invoke-virtual {p0}, Lddu;->a()Lizu;

    move-result-object v3

    invoke-direct {v2, v3}, Ljue;-><init>(Lizu;)V

    invoke-virtual {v0, v1, v2}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v0

    check-cast v0, Ljuc;

    goto :goto_0
.end method

.method public d(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lddu;->c:Ljava/util/EnumMap;

    sget-object v1, Lddt;->a:Lddt;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lddu;->c:Ljava/util/EnumMap;

    sget-object v1, Lddt;->a:Lddt;

    .line 217
    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v1

    .line 216
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/plus/phone/VideoViewActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 219
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lddr;->d(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Ljuf;
    .locals 11

    .prologue
    .line 147
    new-instance v1, Ljuc;

    iget-object v0, p0, Lddu;->a:Landroid/os/Bundle;

    const-string v2, "view_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lddu;->D:Ljava/lang/String;

    .line 148
    invoke-virtual {p0}, Lddu;->a()Lizu;

    move-result-object v4

    invoke-virtual {p0}, Lddu;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lddu;->n()J

    move-result-wide v6

    invoke-virtual {p0}, Lddu;->X()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-direct/range {v1 .. v10}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;Ljava/lang/String;JJLnzi;)V

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0, p1, p2}, Lddr;->writeToParcel(Landroid/os/Parcel;I)V

    .line 125
    iget-object v0, p0, Lddu;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget v0, p0, Lddu;->E:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    iget v0, p0, Lddu;->F:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    return-void
.end method
