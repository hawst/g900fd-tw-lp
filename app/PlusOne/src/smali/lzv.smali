.class public final Llzv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvx;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7288
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7291
    iput-object v0, p0, Llzv;->apiHeader:Llyr;

    .line 7294
    iput-object v0, p0, Llzv;->a:Lnvx;

    .line 7288
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7311
    const/4 v0, 0x0

    .line 7312
    iget-object v1, p0, Llzv;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 7313
    const/4 v0, 0x1

    iget-object v1, p0, Llzv;->apiHeader:Llyr;

    .line 7314
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7316
    :cond_0
    iget-object v1, p0, Llzv;->a:Lnvx;

    if-eqz v1, :cond_1

    .line 7317
    const/4 v1, 0x2

    iget-object v2, p0, Llzv;->a:Lnvx;

    .line 7318
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7320
    :cond_1
    iget-object v1, p0, Llzv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7321
    iput v0, p0, Llzv;->ai:I

    .line 7322
    return v0
.end method

.method public a(Loxn;)Llzv;
    .locals 2

    .prologue
    .line 7330
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7331
    sparse-switch v0, :sswitch_data_0

    .line 7335
    iget-object v1, p0, Llzv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7336
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzv;->ah:Ljava/util/List;

    .line 7339
    :cond_1
    iget-object v1, p0, Llzv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7341
    :sswitch_0
    return-object p0

    .line 7346
    :sswitch_1
    iget-object v0, p0, Llzv;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 7347
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzv;->apiHeader:Llyr;

    .line 7349
    :cond_2
    iget-object v0, p0, Llzv;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7353
    :sswitch_2
    iget-object v0, p0, Llzv;->a:Lnvx;

    if-nez v0, :cond_3

    .line 7354
    new-instance v0, Lnvx;

    invoke-direct {v0}, Lnvx;-><init>()V

    iput-object v0, p0, Llzv;->a:Lnvx;

    .line 7356
    :cond_3
    iget-object v0, p0, Llzv;->a:Lnvx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7331
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7299
    iget-object v0, p0, Llzv;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 7300
    const/4 v0, 0x1

    iget-object v1, p0, Llzv;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7302
    :cond_0
    iget-object v0, p0, Llzv;->a:Lnvx;

    if-eqz v0, :cond_1

    .line 7303
    const/4 v0, 0x2

    iget-object v1, p0, Llzv;->a:Lnvx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7305
    :cond_1
    iget-object v0, p0, Llzv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7307
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7284
    invoke-virtual {p0, p1}, Llzv;->a(Loxn;)Llzv;

    move-result-object v0

    return-object v0
.end method
