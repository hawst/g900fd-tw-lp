.class public final Lofd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lofd;


# instance fields
.field public b:Lofe;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5088
    const/4 v0, 0x0

    new-array v0, v0, [Lofd;

    sput-object v0, Lofd;->a:[Lofd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5089
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5092
    const/4 v0, 0x0

    iput-object v0, p0, Lofd;->b:Lofe;

    .line 5089
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5111
    const/4 v0, 0x0

    .line 5112
    iget-object v1, p0, Lofd;->b:Lofe;

    if-eqz v1, :cond_0

    .line 5113
    const/4 v0, 0x1

    iget-object v1, p0, Lofd;->b:Lofe;

    .line 5114
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5116
    :cond_0
    iget-object v1, p0, Lofd;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5117
    const/4 v1, 0x2

    iget-object v2, p0, Lofd;->c:Ljava/lang/String;

    .line 5118
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5120
    :cond_1
    iget-object v1, p0, Lofd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5121
    iput v0, p0, Lofd;->ai:I

    .line 5122
    return v0
.end method

.method public a(Loxn;)Lofd;
    .locals 2

    .prologue
    .line 5130
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5131
    sparse-switch v0, :sswitch_data_0

    .line 5135
    iget-object v1, p0, Lofd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofd;->ah:Ljava/util/List;

    .line 5139
    :cond_1
    iget-object v1, p0, Lofd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5141
    :sswitch_0
    return-object p0

    .line 5146
    :sswitch_1
    iget-object v0, p0, Lofd;->b:Lofe;

    if-nez v0, :cond_2

    .line 5147
    new-instance v0, Lofe;

    invoke-direct {v0}, Lofe;-><init>()V

    iput-object v0, p0, Lofd;->b:Lofe;

    .line 5149
    :cond_2
    iget-object v0, p0, Lofd;->b:Lofe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5153
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofd;->c:Ljava/lang/String;

    goto :goto_0

    .line 5131
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5099
    iget-object v0, p0, Lofd;->b:Lofe;

    if-eqz v0, :cond_0

    .line 5100
    const/4 v0, 0x1

    iget-object v1, p0, Lofd;->b:Lofe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5102
    :cond_0
    iget-object v0, p0, Lofd;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5103
    const/4 v0, 0x2

    iget-object v1, p0, Lofd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5105
    :cond_1
    iget-object v0, p0, Lofd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5107
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5085
    invoke-virtual {p0, p1}, Lofd;->a(Loxn;)Lofd;

    move-result-object v0

    return-object v0
.end method
