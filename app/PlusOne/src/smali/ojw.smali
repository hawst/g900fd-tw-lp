.class public final Lojw;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lois;

.field public b:[Loiu;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;

.field private e:Loiy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1210
    const v0, 0x28d1cac

    new-instance v1, Lojx;

    invoke-direct {v1}, Lojx;-><init>()V

    .line 1215
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 1214
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1211
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1218
    sget-object v0, Lois;->a:[Lois;

    iput-object v0, p0, Lojw;->a:[Lois;

    .line 1221
    sget-object v0, Loiu;->a:[Loiu;

    iput-object v0, p0, Lojw;->b:[Loiu;

    .line 1228
    const/4 v0, 0x0

    iput-object v0, p0, Lojw;->e:Loiy;

    .line 1211
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1262
    .line 1263
    iget-object v0, p0, Lojw;->a:[Lois;

    if-eqz v0, :cond_1

    .line 1264
    iget-object v3, p0, Lojw;->a:[Lois;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1265
    if-eqz v5, :cond_0

    .line 1266
    const/4 v6, 0x1

    .line 1267
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1264
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1271
    :cond_2
    iget-object v2, p0, Lojw;->b:[Loiu;

    if-eqz v2, :cond_4

    .line 1272
    iget-object v2, p0, Lojw;->b:[Loiu;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1273
    if-eqz v4, :cond_3

    .line 1274
    const/4 v5, 0x2

    .line 1275
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1272
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1279
    :cond_4
    iget-object v1, p0, Lojw;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1280
    const/4 v1, 0x3

    iget-object v2, p0, Lojw;->c:Ljava/lang/String;

    .line 1281
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1283
    :cond_5
    iget-object v1, p0, Lojw;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1284
    const/4 v1, 0x4

    iget-object v2, p0, Lojw;->d:Ljava/lang/Boolean;

    .line 1285
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1287
    :cond_6
    iget-object v1, p0, Lojw;->e:Loiy;

    if-eqz v1, :cond_7

    .line 1288
    const/4 v1, 0x5

    iget-object v2, p0, Lojw;->e:Loiy;

    .line 1289
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1291
    :cond_7
    iget-object v1, p0, Lojw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1292
    iput v0, p0, Lojw;->ai:I

    .line 1293
    return v0
.end method

.method public a(Loxn;)Lojw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1301
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1302
    sparse-switch v0, :sswitch_data_0

    .line 1306
    iget-object v2, p0, Lojw;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1307
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lojw;->ah:Ljava/util/List;

    .line 1310
    :cond_1
    iget-object v2, p0, Lojw;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1312
    :sswitch_0
    return-object p0

    .line 1317
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1318
    iget-object v0, p0, Lojw;->a:[Lois;

    if-nez v0, :cond_3

    move v0, v1

    .line 1319
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lois;

    .line 1320
    iget-object v3, p0, Lojw;->a:[Lois;

    if-eqz v3, :cond_2

    .line 1321
    iget-object v3, p0, Lojw;->a:[Lois;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1323
    :cond_2
    iput-object v2, p0, Lojw;->a:[Lois;

    .line 1324
    :goto_2
    iget-object v2, p0, Lojw;->a:[Lois;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1325
    iget-object v2, p0, Lojw;->a:[Lois;

    new-instance v3, Lois;

    invoke-direct {v3}, Lois;-><init>()V

    aput-object v3, v2, v0

    .line 1326
    iget-object v2, p0, Lojw;->a:[Lois;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1327
    invoke-virtual {p1}, Loxn;->a()I

    .line 1324
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1318
    :cond_3
    iget-object v0, p0, Lojw;->a:[Lois;

    array-length v0, v0

    goto :goto_1

    .line 1330
    :cond_4
    iget-object v2, p0, Lojw;->a:[Lois;

    new-instance v3, Lois;

    invoke-direct {v3}, Lois;-><init>()V

    aput-object v3, v2, v0

    .line 1331
    iget-object v2, p0, Lojw;->a:[Lois;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1335
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1336
    iget-object v0, p0, Lojw;->b:[Loiu;

    if-nez v0, :cond_6

    move v0, v1

    .line 1337
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loiu;

    .line 1338
    iget-object v3, p0, Lojw;->b:[Loiu;

    if-eqz v3, :cond_5

    .line 1339
    iget-object v3, p0, Lojw;->b:[Loiu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1341
    :cond_5
    iput-object v2, p0, Lojw;->b:[Loiu;

    .line 1342
    :goto_4
    iget-object v2, p0, Lojw;->b:[Loiu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 1343
    iget-object v2, p0, Lojw;->b:[Loiu;

    new-instance v3, Loiu;

    invoke-direct {v3}, Loiu;-><init>()V

    aput-object v3, v2, v0

    .line 1344
    iget-object v2, p0, Lojw;->b:[Loiu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1345
    invoke-virtual {p1}, Loxn;->a()I

    .line 1342
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1336
    :cond_6
    iget-object v0, p0, Lojw;->b:[Loiu;

    array-length v0, v0

    goto :goto_3

    .line 1348
    :cond_7
    iget-object v2, p0, Lojw;->b:[Loiu;

    new-instance v3, Loiu;

    invoke-direct {v3}, Loiu;-><init>()V

    aput-object v3, v2, v0

    .line 1349
    iget-object v2, p0, Lojw;->b:[Loiu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1353
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojw;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 1357
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojw;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1361
    :sswitch_5
    iget-object v0, p0, Lojw;->e:Loiy;

    if-nez v0, :cond_8

    .line 1362
    new-instance v0, Loiy;

    invoke-direct {v0}, Loiy;-><init>()V

    iput-object v0, p0, Lojw;->e:Loiy;

    .line 1364
    :cond_8
    iget-object v0, p0, Lojw;->e:Loiy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1302
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1233
    iget-object v1, p0, Lojw;->a:[Lois;

    if-eqz v1, :cond_1

    .line 1234
    iget-object v2, p0, Lojw;->a:[Lois;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1235
    if-eqz v4, :cond_0

    .line 1236
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1234
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1240
    :cond_1
    iget-object v1, p0, Lojw;->b:[Loiu;

    if-eqz v1, :cond_3

    .line 1241
    iget-object v1, p0, Lojw;->b:[Loiu;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1242
    if-eqz v3, :cond_2

    .line 1243
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1241
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1247
    :cond_3
    iget-object v0, p0, Lojw;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1248
    const/4 v0, 0x3

    iget-object v1, p0, Lojw;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1250
    :cond_4
    iget-object v0, p0, Lojw;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1251
    const/4 v0, 0x4

    iget-object v1, p0, Lojw;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1253
    :cond_5
    iget-object v0, p0, Lojw;->e:Loiy;

    if-eqz v0, :cond_6

    .line 1254
    const/4 v0, 0x5

    iget-object v1, p0, Lojw;->e:Loiy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1256
    :cond_6
    iget-object v0, p0, Lojw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1258
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1207
    invoke-virtual {p0, p1}, Lojw;->a(Loxn;)Lojw;

    move-result-object v0

    return-object v0
.end method
