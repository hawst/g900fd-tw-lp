.class public final Leef;
.super Llol;
.source "PG"

# interfaces
.implements Lfyr;
.implements Lhjj;
.implements Lhmq;
.implements Lhsw;


# instance fields
.field private N:Lhee;

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/Integer;

.field private Q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljuf;",
            ">;"
        }
    .end annotation
.end field

.field private R:Lhgw;

.field private S:Z

.field private T:Lfal;

.field private U:Lcom/google/android/apps/plus/views/MiniShareTouchHandler;

.field private V:Landroid/view/View;

.field private W:Landroid/view/View;

.field private X:Landroid/content/Intent;

.field private Y:Landroid/content/Intent;

.field private Z:Ljava/lang/String;

.field private aa:Landroid/view/animation/Animation;

.field private ab:Landroid/widget/ListView;

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lhhh;",
            ">;"
        }
    .end annotation
.end field

.field private ag:Lfan;

.field private ah:Lhoc;

.field private ai:Leer;

.field private final aj:Leeq;

.field private final ak:Lhov;

.field private final al:Landroid/view/View$OnClickListener;

.field private final am:Landroid/view/View$OnClickListener;

.field private final an:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private final ao:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ljava/util/List",
            "<",
            "Ljad;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Llol;-><init>()V

    .line 157
    new-instance v0, Leeq;

    invoke-direct {v0, p0}, Leeq;-><init>(Leef;)V

    iput-object v0, p0, Leef;->aj:Leeq;

    .line 158
    new-instance v0, Lhov;

    .line 159
    invoke-virtual {p0}, Leef;->z_()Llqr;

    move-result-object v1

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Leef;->ak:Lhov;

    .line 162
    new-instance v0, Lhje;

    iget-object v1, p0, Leef;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    .line 163
    new-instance v0, Lhsx;

    iget-object v1, p0, Leef;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lhsx;-><init>(Llqr;Lhsw;)V

    .line 262
    new-instance v0, Leeg;

    invoke-direct {v0, p0}, Leeg;-><init>(Leef;)V

    iput-object v0, p0, Leef;->al:Landroid/view/View$OnClickListener;

    .line 299
    new-instance v0, Leeh;

    invoke-direct {v0, p0}, Leeh;-><init>(Leef;)V

    iput-object v0, p0, Leef;->am:Landroid/view/View$OnClickListener;

    .line 390
    new-instance v0, Leei;

    invoke-direct {v0, p0}, Leei;-><init>(Leef;)V

    iput-object v0, p0, Leef;->an:Lbc;

    .line 416
    new-instance v0, Leek;

    invoke-direct {v0, p0}, Leek;-><init>(Leef;)V

    iput-object v0, p0, Leef;->ao:Lbc;

    .line 985
    return-void
.end method

.method static synthetic A(Leef;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->Z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Leef;Landroid/app/Activity;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 98
    iget-object v0, p0, Leef;->ag:Lfan;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leef;->ag:Lfan;

    iget-object v1, p0, Leef;->N:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lfan;->a(Landroid/app/Activity;ILhgw;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Leef;->N:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    const-string v1, ""

    invoke-static {p1, v0, v1}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v3, :cond_2

    const-string v0, "android.intent.extra.STREAM"

    invoke-direct {p0, v3}, Leef;->a(Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    :goto_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    invoke-direct {p0, v0, v3}, Leef;->a(Ljuf;Z)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method static synthetic a(Leef;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Leef;->X:Landroid/content/Intent;

    return-object p1
.end method

.method private a(Ljuf;Z)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 688
    if-eqz p1, :cond_2

    .line 689
    invoke-interface {p1}, Ljuf;->f()Lizu;

    move-result-object v0

    .line 690
    if-eqz v0, :cond_2

    .line 691
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 692
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    .line 711
    :goto_0
    return-object v0

    .line 693
    :cond_0
    if-eqz p2, :cond_1

    .line 695
    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 697
    :cond_1
    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v1

    .line 698
    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v2

    .line 699
    if-eqz v2, :cond_2

    .line 701
    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Ljac;)Ljava/lang/String;

    move-result-object v3

    .line 702
    iget-object v0, p0, Leef;->at:Llnl;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Landroid/net/Uri;

    move-result-object v0

    .line 704
    invoke-virtual {p0}, Leef;->n()Lz;

    move-result-object v1

    const-string v2, "android.intent.action.SEND"

    .line 703
    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 711
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Leef;Leer;)Leer;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Leef;->ai:Leer;

    return-object p1
.end method

.method static synthetic a(Leef;)Lhgw;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->R:Lhgw;

    return-object v0
.end method

.method static synthetic a(Leef;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Leef;->P:Ljava/lang/Integer;

    return-object p1
.end method

.method private a(Z)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 715
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 717
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 718
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 720
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    .line 721
    if-eqz v0, :cond_0

    .line 722
    invoke-direct {p0, v0, p1}, Leef;->a(Ljuf;Z)Landroid/net/Uri;

    move-result-object v0

    .line 723
    if-eqz v0, :cond_0

    .line 724
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 718
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 730
    :cond_1
    return-object v2
.end method

.method static synthetic a(Leef;Lhgw;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1}, Leef;->a(Lhgw;)V

    return-void
.end method

.method static synthetic a(Leef;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1}, Leef;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lhgw;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 846
    const-string v0, "com.google.android.apps.plus"

    invoke-direct {p0, v0}, Leef;->b(Ljava/lang/String;)V

    .line 848
    iget-object v0, p0, Leef;->ag:Lfan;

    if-eqz v0, :cond_1

    .line 849
    iget-object v0, p0, Leef;->ag:Lfan;

    invoke-virtual {p0}, Leef;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leef;->N:Lhee;

    .line 850
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 849
    invoke-virtual {v0, v1, v2, p1}, Lfan;->a(Landroid/app/Activity;ILhgw;)Landroid/content/Intent;

    move-result-object v0

    .line 851
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Leef;->a(Landroid/content/Intent;I)V

    .line 875
    :cond_0
    :goto_0
    return-void

    .line 852
    :cond_1
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 853
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    .line 854
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    invoke-interface {v0}, Ljuf;->i()J

    move-result-wide v4

    const-wide/32 v6, 0x44000

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 857
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    .line 858
    invoke-virtual {v0}, Lizu;->b()Ljava/lang/String;

    move-result-object v1

    .line 859
    iget-object v2, p0, Leef;->at:Llnl;

    iget-object v3, p0, Leef;->N:Lhee;

    .line 860
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {v0}, Lizu;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 859
    invoke-static {v2, v3, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leef;->P:Ljava/lang/Integer;

    .line 861
    iput-object p1, p0, Leef;->R:Lhgw;

    .line 863
    const v0, 0x7f0a08b1

    invoke-virtual {p0, v0}, Leef;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Leef;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 854
    goto :goto_1

    .line 865
    :cond_3
    iget-object v0, p0, Leef;->at:Llnl;

    iget-object v1, p0, Leef;->N:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, p1}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;Lhgw;)Landroid/content/Intent;

    move-result-object v0

    .line 867
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Leef;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 869
    :cond_4
    iget-object v0, p0, Leef;->Y:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 870
    if-eqz p1, :cond_5

    .line 871
    iget-object v0, p0, Leef;->Y:Landroid/content/Intent;

    const-string v1, "extra_acl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 873
    :cond_5
    iget-object v0, p0, Leef;->Y:Landroid/content/Intent;

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Leef;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 884
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 885
    invoke-static {v0, p1, v1}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 886
    invoke-virtual {p0}, Leef;->p()Lae;

    move-result-object v1

    const-string v2, "hmsf_pending"

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 887
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 800
    iget-object v0, p0, Leef;->ag:Lfan;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Leef;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "link_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Leef;Landroid/content/ComponentName;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v0, v1, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v3

    sget-object v4, Ljac;->b:Ljac;

    if-eq v3, v4, :cond_3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "com.facebook.katana"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.whatsapp"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.google.android.apps.inbox"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.google.android.gm"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.google.android.email"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Leef;Landroid/content/pm/ResolveInfo;)Z
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Leef;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f0a024d

    invoke-virtual {p0, v1}, Leef;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Leef;Z)Z
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Leef;->ad:Z

    return p1
.end method

.method static synthetic b(Leef;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->aa:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic b(Leef;Z)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0, p1}, Leef;->a(Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Leef;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1}, Leef;->a(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 919
    iget-object v0, p0, Leef;->at:Llnl;

    const-class v1, Ldhu;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhu;

    .line 920
    new-instance v2, Ldpg;

    iget-object v1, p0, Leef;->at:Llnl;

    invoke-direct {v2, v1, p1}, Ldpg;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 921
    iget-object v1, p0, Leef;->Q:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 923
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ldpg;->a(Ljava/util/Collection;)V

    .line 939
    :cond_0
    :goto_0
    invoke-virtual {p0}, Leef;->n()Lz;

    move-result-object v0

    invoke-static {v0, v2}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 940
    return-void

    .line 924
    :cond_1
    iget-object v1, p0, Leef;->Y:Landroid/content/Intent;

    if-eqz v1, :cond_4

    .line 926
    iget-object v0, p0, Leef;->Y:Landroid/content/Intent;

    const-string v1, "target_album_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 927
    const-wide/16 v0, 0x0

    .line 929
    :try_start_0
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 935
    :cond_2
    :goto_1
    invoke-virtual {v2, v0, v1}, Ldpg;->a(J)V

    goto :goto_0

    .line 931
    :catch_0
    move-exception v4

    const-string v4, "HostedMiniShareFragment"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 932
    const-string v4, "Invalid AlbumId: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 936
    :cond_4
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Leef;->k()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldhu;->a(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 937
    invoke-virtual {p0}, Leef;->k()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldhu;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldpg;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Leef;Landroid/content/pm/ResolveInfo;)Z
    .locals 4

    .prologue
    .line 98
    invoke-virtual {p0}, Leef;->o()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "share_to_clx_label"

    const-string v2, "string"

    iget-object v3, p0, Leef;->at:Llnl;

    invoke-virtual {v3}, Llnl;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Leef;->at:Llnl;

    invoke-virtual {v1}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Leef;)Landroid/view/View;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->V:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Leef;Z)Z
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Leef;->ac:Z

    return p1
.end method

.method static synthetic d(Leef;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->P:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic e(Leef;)V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Leef;->p()Lae;

    move-result-object v0

    const-string v1, "hmsf_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lt;->a()V

    :cond_0
    return-void
.end method

.method static synthetic f(Leef;)Llnl;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Leef;)Lhee;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->N:Lhee;

    return-object v0
.end method

.method static synthetic h(Leef;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->af:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic i(Leef;)Llnl;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->at:Llnl;

    return-object v0
.end method

.method static synthetic j(Leef;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Leef;)Leer;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->ai:Leer;

    return-object v0
.end method

.method static synthetic l(Leef;)Lbc;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->ao:Lbc;

    return-object v0
.end method

.method static synthetic m(Leef;)V
    .locals 6

    .prologue
    .line 98
    iget-object v0, p0, Leef;->ai:Leer;

    invoke-virtual {v0}, Leer;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ldiv;

    iget-object v0, p0, Leef;->ai:Leer;

    invoke-virtual {v0}, Leer;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v0, ""

    invoke-direct {v2, v4, v5, v0}, Ldiv;-><init>(JLjava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Leef;->b(Ljava/lang/String;)V

    iget-object v0, p0, Leef;->ai:Leer;

    invoke-virtual {v0}, Leer;->c()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f10006c

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iput-object v0, p0, Leef;->X:Landroid/content/Intent;

    new-instance v0, Ldoo;

    iget-object v1, p0, Leef;->at:Llnl;

    iget-object v3, p0, Leef;->N:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const v4, 0x7f0a057b

    invoke-virtual {p0, v4}, Leef;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v3, v2, v4}, Ldoo;-><init>(Landroid/content/Context;ILdiv;Ljava/lang/String;)V

    iget-object v1, p0, Leef;->ah:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    return-void
.end method

.method static synthetic n(Leef;)Llnh;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->au:Llnh;

    return-object v0
.end method

.method static synthetic o(Leef;)Lfan;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->ag:Lfan;

    return-object v0
.end method

.method static synthetic p(Leef;)Lbc;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->an:Lbc;

    return-object v0
.end method

.method static synthetic q(Leef;)Llnl;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->at:Llnl;

    return-object v0
.end method

.method static synthetic r(Leef;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->X:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic s(Leef;)Lhov;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->ak:Lhov;

    return-object v0
.end method

.method static synthetic t(Leef;)Llnl;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->at:Llnl;

    return-object v0
.end method

.method static synthetic u(Leef;)Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Leef;->S:Z

    return v0
.end method

.method static synthetic v(Leef;)Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Leef;->ae:Z

    return v0
.end method

.method static synthetic w(Leef;)Lfal;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Leef;->T:Lfal;

    return-object v0
.end method

.method static synthetic x(Leef;)V
    .locals 2

    .prologue
    .line 98
    iget-boolean v0, p0, Leef;->ad:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Leef;->ac:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leef;->ab:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leef;->ab:Landroid/widget/ListView;

    iget-object v1, p0, Leef;->T:Lfal;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Leef;->ab:Landroid/widget/ListView;

    new-instance v1, Lees;

    invoke-direct {v1}, Lees;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    :cond_0
    return-void
.end method

.method static synthetic y(Leef;)Ljava/util/ArrayList;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Leef;->ag:Lfan;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leef;->ag:Lfan;

    invoke-virtual {p0}, Leef;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Lfan;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    iget-object v0, p0, Leef;->Q:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    iget-object v2, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v6, :cond_2

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Ljac;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    invoke-virtual {p0}, Leef;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "resource_type"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_3

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Leef;->at:Llnl;

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljvs;->b:Ljvs;

    invoke-static {v2, v3, v4}, Ljvq;->a(Landroid/content/Context;Ljava/lang/String;Ljvs;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Leef;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "geo_lat"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "latitude"

    invoke-virtual {p0}, Leef;->k()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "geo_lat"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    :cond_4
    invoke-virtual {p0}, Leef;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "geo_lon"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "longitude"

    invoke-virtual {p0}, Leef;->k()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "geo_lon"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    :cond_5
    const-string v3, "application/vnd.google.panorama360+jpg"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Ljac;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0}, Leef;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "text/plain"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Leef;->Z:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method static synthetic z(Leef;)Z
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Leef;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 684
    const/4 v0, 0x0

    return-object v0
.end method

.method public V()Z
    .locals 2

    .prologue
    .line 841
    iget-object v0, p0, Leef;->V:Landroid/view/View;

    iget-object v1, p0, Leef;->aa:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 842
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 512
    invoke-virtual {p0}, Leef;->n()Lz;

    move-result-object v0

    .line 513
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 515
    const v1, 0x7f0400db

    invoke-virtual {p1, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 517
    const v1, 0x7f1002a9

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;

    iput-object v1, p0, Leef;->U:Lcom/google/android/apps/plus/views/MiniShareTouchHandler;

    .line 518
    const v1, 0x7f10015f

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Leef;->V:Landroid/view/View;

    .line 520
    const v1, 0x7f100372

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Leef;->W:Landroid/view/View;

    .line 521
    iget-object v1, p0, Leef;->W:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 523
    iget-object v1, p0, Leef;->U:Lcom/google/android/apps/plus/views/MiniShareTouchHandler;

    iget-object v2, p0, Leef;->V:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a(Landroid/view/View;)V

    .line 524
    iget-object v1, p0, Leef;->U:Lcom/google/android/apps/plus/views/MiniShareTouchHandler;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/MiniShareTouchHandler;->a(Lfyr;)V

    .line 526
    const v1, 0x7f05001d

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Leef;->aa:Landroid/view/animation/Animation;

    .line 527
    iget-object v1, p0, Leef;->aa:Landroid/view/animation/Animation;

    const v2, 0x7f050006

    invoke-virtual {v1, v0, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    .line 528
    iget-object v1, p0, Leef;->aa:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 530
    iget-object v1, p0, Leef;->aa:Landroid/view/animation/Animation;

    new-instance v2, Leel;

    invoke-direct {v2, p0, v0}, Leel;-><init>(Leef;Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 555
    if-nez p3, :cond_6

    .line 556
    iget-object v1, p0, Leef;->O:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 565
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/16 v1, 0x20

    invoke-static {v1}, Llsu;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x15

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Leef;->O:Ljava/lang/String;

    .line 586
    :cond_0
    :goto_0
    iget-object v1, p0, Leef;->au:Llnh;

    const-class v2, Lfan;

    invoke-virtual {v1, v2}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfan;

    .line 587
    invoke-virtual {v1, v6}, Lfan;->a(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 588
    iput-object v1, p0, Leef;->ag:Lfan;

    .line 593
    :cond_2
    iget-object v1, p0, Leef;->ag:Lfan;

    if-nez v1, :cond_4

    .line 594
    const-string v1, "shareables"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 595
    const-string v1, "shareables"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Leef;->Q:Ljava/util/ArrayList;

    .line 597
    :cond_3
    const-string v1, "link_url"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 598
    const-string v1, "album_id"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 599
    const-string v1, "album_owner_id"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 600
    const-string v1, "cluster_id"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 601
    iget-object v1, p0, Leef;->N:Lhee;

    .line 602
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v5}, Ljvj;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 601
    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Leef;->Y:Landroid/content/Intent;

    .line 604
    const-string v1, "link_url"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Leef;->Z:Ljava/lang/String;

    .line 608
    :cond_4
    const-string v1, "activity_is_public"

    invoke-virtual {v6, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Leef;->ae:Z

    .line 610
    new-instance v1, Lfal;

    invoke-direct {v1, v0}, Lfal;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Leef;->T:Lfal;

    .line 612
    iget-object v0, p0, Leef;->T:Lfal;

    iget-object v1, p0, Leef;->al:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v10, v1}, Lfal;->a(ILandroid/view/View$OnClickListener;)V

    .line 614
    iget-object v0, p0, Leef;->T:Lfal;

    iget-object v1, p0, Leef;->al:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v8, v1}, Lfal;->a(ILandroid/view/View$OnClickListener;)V

    .line 616
    iget-object v0, p0, Leef;->T:Lfal;

    const/4 v1, 0x2

    iget-object v2, p0, Leef;->am:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lfal;->a(ILandroid/view/View$OnClickListener;)V

    .line 619
    const v0, 0x102000a

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Leef;->ab:Landroid/widget/ListView;

    .line 621
    invoke-virtual {p0}, Leef;->w()Lbb;

    move-result-object v0

    .line 622
    iget-object v1, p0, Leef;->N:Lhee;

    invoke-interface {v1}, Lhee;->f()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 623
    new-instance v1, Leeo;

    invoke-direct {v1, p0}, Leeo;-><init>(Leef;)V

    invoke-virtual {v0, v10, v9, v1}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 628
    :goto_1
    iget-object v1, p0, Leef;->X:Landroid/content/Intent;

    if-eqz v1, :cond_5

    iget-object v1, p0, Leef;->ag:Lfan;

    if-eqz v1, :cond_5

    iget-object v1, p0, Leef;->ag:Lfan;

    iget-object v2, p0, Leef;->X:Landroid/content/Intent;

    .line 629
    invoke-virtual {v1, v2}, Lfan;->c(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 630
    const/4 v1, 0x3

    iget-object v2, p0, Leef;->an:Lbc;

    invoke-virtual {v0, v1, v9, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 633
    :cond_5
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Leef;->af:Landroid/util/SparseArray;

    .line 634
    iget-object v0, p0, Leef;->au:Llnh;

    const-class v1, Lhhh;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhh;

    .line 635
    iget-object v2, p0, Leef;->af:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lhhh;->a()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 568
    :cond_6
    const-string v1, "attachments"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Leef;->Q:Ljava/util/ArrayList;

    .line 569
    const-string v1, "activity_id"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 570
    const-string v1, "activity_id"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Leef;->O:Ljava/lang/String;

    .line 572
    :cond_7
    const-string v1, "pending_request"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 573
    const-string v1, "pending_request"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Leef;->P:Ljava/lang/Integer;

    .line 575
    :cond_8
    const-string v1, "restrict_to_domain"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 576
    const-string v1, "restrict_to_domain"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Leef;->S:Z

    .line 578
    :cond_9
    const-string v1, "reshare_audience"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 579
    const-string v1, "reshare_audience"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lhgw;

    iput-object v1, p0, Leef;->R:Lhgw;

    .line 581
    :cond_a
    const-string v1, "intent_to_start"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 582
    const-string v1, "intent_to_start"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    iput-object v1, p0, Leef;->X:Landroid/content/Intent;

    goto/16 :goto_0

    .line 625
    :cond_b
    iput-boolean v8, p0, Leef;->ad:Z

    goto/16 :goto_1

    .line 638
    :cond_c
    return-object v7
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 805
    invoke-virtual {p0}, Leef;->n()Lz;

    move-result-object v0

    .line 806
    packed-switch p1, :pswitch_data_0

    .line 833
    invoke-super {p0, p1, p2, p3}, Llol;->a(IILandroid/content/Intent;)V

    .line 834
    :goto_0
    return-void

    .line 808
    :pswitch_0
    if-ne p2, v1, :cond_0

    .line 809
    const-string v0, "extra_acl"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 811
    invoke-direct {p0, v0}, Leef;->a(Lhgw;)V

    goto :goto_0

    .line 813
    :cond_0
    invoke-virtual {p0}, Leef;->n()Lz;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lz;->setResult(I)V

    .line 814
    invoke-virtual {p0}, Leef;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    goto :goto_0

    .line 821
    :pswitch_1
    if-ne p2, v1, :cond_1

    .line 822
    invoke-virtual {v0, p2}, Lz;->setResult(I)V

    .line 824
    :cond_1
    new-instance v0, Leem;

    invoke-direct {v0, p0}, Leem;-><init>(Leef;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 806
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 177
    return-void
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Leef;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 169
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 643
    invoke-super {p0}, Llol;->aO_()V

    .line 645
    iget-object v0, p0, Leef;->V:Landroid/view/View;

    const v1, 0x7f10015e

    .line 646
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    .line 648
    invoke-virtual {p0}, Leef;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0375

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 647
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a(I)V

    .line 649
    invoke-virtual {p0}, Leef;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    new-instance v3, Leep;

    invoke-direct {v3, p0}, Leep;-><init>(Leef;)V

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 650
    iget-object v0, p0, Leef;->aj:Leeq;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 651
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 907
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 908
    iget-object v0, p0, Leef;->V:Landroid/view/View;

    iget-object v1, p0, Leef;->aa:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 910
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1015
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 1016
    iget-object v0, p0, Leef;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 1017
    iget-object v0, p0, Leef;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leef;->N:Lhee;

    .line 1019
    iget-object v0, p0, Leef;->ah:Lhoc;

    if-nez v0, :cond_0

    .line 1020
    invoke-virtual {p0}, Leef;->h_()Llnh;

    move-result-object v0

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Leef;->ah:Lhoc;

    .line 1023
    :cond_0
    iget-object v0, p0, Leef;->ah:Lhoc;

    new-instance v1, Leen;

    invoke-direct {v1, p0}, Leen;-><init>(Leef;)V

    invoke-virtual {v0, v1}, Lhoc;->a(Lhob;)Lhoc;

    .line 1050
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 662
    const-string v0, "attachments"

    iget-object v1, p0, Leef;->Q:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 663
    iget-object v0, p0, Leef;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 664
    const-string v0, "activity_id"

    iget-object v1, p0, Leef;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    :cond_0
    iget-object v0, p0, Leef;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 667
    const-string v0, "pending_request"

    iget-object v1, p0, Leef;->P:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 669
    :cond_1
    iget-boolean v0, p0, Leef;->S:Z

    if-eqz v0, :cond_2

    .line 670
    const-string v0, "restrict_to_domain"

    iget-boolean v1, p0, Leef;->S:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 672
    :cond_2
    iget-object v0, p0, Leef;->R:Lhgw;

    if-eqz v0, :cond_3

    .line 673
    const-string v0, "reshare_audience"

    iget-object v1, p0, Leef;->R:Lhgw;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 675
    :cond_3
    iget-object v0, p0, Leef;->X:Landroid/content/Intent;

    if-eqz v0, :cond_4

    .line 676
    const-string v0, "intent_to_start"

    iget-object v1, p0, Leef;->X:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 678
    :cond_4
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 679
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 655
    invoke-super {p0}, Llol;->z()V

    .line 657
    iget-object v0, p0, Leef;->aj:Leeq;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 658
    return-void
.end method
