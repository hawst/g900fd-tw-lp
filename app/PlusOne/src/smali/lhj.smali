.class public Llhj;
.super Llhb;
.source "PG"


# instance fields
.field public final g:Landroid/view/View;

.field private final h:J

.field private final i:I

.field private final j:[I

.field private final k:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 85
    const-wide/16 v0, 0xc8

    invoke-direct {p0, p1, p2, v0, v1}, Llhj;-><init>(Landroid/view/View;ZJ)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/view/View;ZJ)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 68
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 69
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 68
    :goto_0
    invoke-direct {p0, v2, v0, p2}, Llhb;-><init>(Landroid/content/Context;IZ)V

    .line 27
    new-array v0, v1, [I

    iput-object v0, p0, Llhj;->j:[I

    .line 30
    new-instance v0, Llhk;

    invoke-direct {v0, p0}, Llhk;-><init>(Llhj;)V

    iput-object v0, p0, Llhj;->k:Landroid/view/animation/Animation$AnimationListener;

    .line 71
    iput-object p1, p0, Llhj;->g:Landroid/view/View;

    .line 72
    iput-wide p3, p0, Llhj;->h:J

    .line 73
    iget-object v0, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Llhj;->i:I

    .line 74
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(ILandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 139
    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    :cond_0
    return-object v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 117
    iget-object v1, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 118
    :goto_0
    if-ne p1, v1, :cond_2

    .line 129
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v0

    .line 117
    goto :goto_0

    .line 122
    :cond_2
    iget-object v1, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setClickable(Z)V

    .line 123
    iget-object v1, p0, Llhj;->g:Landroid/view/View;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 126
    if-eqz p1, :cond_0

    .line 127
    iget-object v0, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_1

    .line 123
    :cond_3
    const/16 v0, 0x8

    goto :goto_2
.end method

.method protected b(Z)F
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 96
    if-eqz p1, :cond_0

    move v0, v1

    .line 97
    :goto_0
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget v3, p0, Llhj;->d:F

    invoke-direct {v2, v1, v1, v3, v0}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 98
    iget-wide v4, p0, Llhj;->h:J

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 99
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 100
    iget-object v1, p0, Llhj;->k:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 101
    iget-object v1, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 102
    return v0

    .line 96
    :cond_0
    invoke-virtual {p0}, Llhj;->g()F

    move-result v0

    goto :goto_0
.end method

.method protected e()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 91
    return-void
.end method

.method g()F
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Llhj;->g:Landroid/view/View;

    iget-object v1, p0, Llhj;->j:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 109
    iget-object v0, p0, Llhj;->j:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    .line 110
    iget-boolean v1, p0, Llhj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Llhj;->g:Landroid/view/View;

    .line 111
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Llhj;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    neg-int v0, v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Llhj;->i:I

    sub-int v0, v1, v0

    int-to-float v0, v0

    goto :goto_0
.end method
