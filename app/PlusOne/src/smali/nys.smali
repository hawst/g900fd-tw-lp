.class public final Lnys;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnys;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lnyz;

.field public d:I

.field public e:Lnyo;

.field public f:Lnyp;

.field public g:Lnyz;

.field public h:[Lnyz;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Boolean;

.field public k:I

.field private l:Lnzm;

.field private m:Lnzm;

.field private n:[Ljava/lang/String;

.field private o:[Lnzm;

.field private p:Ljava/lang/Boolean;

.field private q:Lnzd;

.field private r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3735
    const/4 v0, 0x0

    new-array v0, v0, [Lnys;

    sput-object v0, Lnys;->a:[Lnys;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 3736
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3755
    iput-object v1, p0, Lnys;->c:Lnyz;

    .line 3758
    iput-object v1, p0, Lnys;->l:Lnzm;

    .line 3761
    iput v2, p0, Lnys;->d:I

    .line 3764
    iput-object v1, p0, Lnys;->e:Lnyo;

    .line 3767
    iput-object v1, p0, Lnys;->f:Lnyp;

    .line 3770
    iput-object v1, p0, Lnys;->g:Lnyz;

    .line 3773
    iput-object v1, p0, Lnys;->m:Lnzm;

    .line 3776
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnys;->n:[Ljava/lang/String;

    .line 3779
    sget-object v0, Lnyz;->a:[Lnyz;

    iput-object v0, p0, Lnys;->h:[Lnyz;

    .line 3782
    sget-object v0, Lnzm;->a:[Lnzm;

    iput-object v0, p0, Lnys;->o:[Lnzm;

    .line 3791
    iput v2, p0, Lnys;->k:I

    .line 3794
    iput-object v1, p0, Lnys;->q:Lnzd;

    .line 3736
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 3866
    const/4 v0, 0x1

    iget-object v2, p0, Lnys;->b:Ljava/lang/String;

    .line 3868
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3869
    iget-object v2, p0, Lnys;->c:Lnyz;

    if-eqz v2, :cond_0

    .line 3870
    const/4 v2, 0x2

    iget-object v3, p0, Lnys;->c:Lnyz;

    .line 3871
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3873
    :cond_0
    iget v2, p0, Lnys;->d:I

    if-eq v2, v7, :cond_1

    .line 3874
    const/4 v2, 0x3

    iget v3, p0, Lnys;->d:I

    .line 3875
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3877
    :cond_1
    iget-object v2, p0, Lnys;->e:Lnyo;

    if-eqz v2, :cond_2

    .line 3878
    const/4 v2, 0x4

    iget-object v3, p0, Lnys;->e:Lnyo;

    .line 3879
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3881
    :cond_2
    iget-object v2, p0, Lnys;->g:Lnyz;

    if-eqz v2, :cond_3

    .line 3882
    const/4 v2, 0x5

    iget-object v3, p0, Lnys;->g:Lnyz;

    .line 3883
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3885
    :cond_3
    iget-object v2, p0, Lnys;->n:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lnys;->n:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 3887
    iget-object v4, p0, Lnys;->n:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    .line 3889
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 3887
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3891
    :cond_4
    add-int/2addr v0, v3

    .line 3892
    iget-object v2, p0, Lnys;->n:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3894
    :cond_5
    iget-object v2, p0, Lnys;->h:[Lnyz;

    if-eqz v2, :cond_7

    .line 3895
    iget-object v3, p0, Lnys;->h:[Lnyz;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 3896
    if-eqz v5, :cond_6

    .line 3897
    const/4 v6, 0x7

    .line 3898
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 3895
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3902
    :cond_7
    iget-object v2, p0, Lnys;->i:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    .line 3903
    const/16 v2, 0x8

    iget-object v3, p0, Lnys;->i:Ljava/lang/Boolean;

    .line 3904
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3906
    :cond_8
    iget-object v2, p0, Lnys;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 3907
    const/16 v2, 0x9

    iget-object v3, p0, Lnys;->j:Ljava/lang/Boolean;

    .line 3908
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3910
    :cond_9
    iget v2, p0, Lnys;->k:I

    if-eq v2, v7, :cond_a

    .line 3911
    const/16 v2, 0xa

    iget v3, p0, Lnys;->k:I

    .line 3912
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3914
    :cond_a
    iget-object v2, p0, Lnys;->q:Lnzd;

    if-eqz v2, :cond_b

    .line 3915
    const/16 v2, 0xb

    iget-object v3, p0, Lnys;->q:Lnzd;

    .line 3916
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3918
    :cond_b
    iget-object v2, p0, Lnys;->f:Lnyp;

    if-eqz v2, :cond_c

    .line 3919
    const/16 v2, 0xc

    iget-object v3, p0, Lnys;->f:Lnyp;

    .line 3920
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3922
    :cond_c
    iget-object v2, p0, Lnys;->p:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    .line 3923
    const/16 v2, 0xd

    iget-object v3, p0, Lnys;->p:Ljava/lang/Boolean;

    .line 3924
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3926
    :cond_d
    iget-object v2, p0, Lnys;->r:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 3927
    const/16 v2, 0xe

    iget-object v3, p0, Lnys;->r:Ljava/lang/String;

    .line 3928
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3930
    :cond_e
    iget-object v2, p0, Lnys;->l:Lnzm;

    if-eqz v2, :cond_f

    .line 3931
    const/16 v2, 0xf

    iget-object v3, p0, Lnys;->l:Lnzm;

    .line 3932
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3934
    :cond_f
    iget-object v2, p0, Lnys;->m:Lnzm;

    if-eqz v2, :cond_10

    .line 3935
    const/16 v2, 0x10

    iget-object v3, p0, Lnys;->m:Lnzm;

    .line 3936
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3938
    :cond_10
    iget-object v2, p0, Lnys;->o:[Lnzm;

    if-eqz v2, :cond_12

    .line 3939
    iget-object v2, p0, Lnys;->o:[Lnzm;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_12

    aget-object v4, v2, v1

    .line 3940
    if-eqz v4, :cond_11

    .line 3941
    const/16 v5, 0x11

    .line 3942
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3939
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3946
    :cond_12
    iget-object v1, p0, Lnys;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3947
    iput v0, p0, Lnys;->ai:I

    .line 3948
    return v0
.end method

.method public a(Loxn;)Lnys;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3956
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3957
    sparse-switch v0, :sswitch_data_0

    .line 3961
    iget-object v2, p0, Lnys;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 3962
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnys;->ah:Ljava/util/List;

    .line 3965
    :cond_1
    iget-object v2, p0, Lnys;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3967
    :sswitch_0
    return-object p0

    .line 3972
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnys;->b:Ljava/lang/String;

    goto :goto_0

    .line 3976
    :sswitch_2
    iget-object v0, p0, Lnys;->c:Lnyz;

    if-nez v0, :cond_2

    .line 3977
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iput-object v0, p0, Lnys;->c:Lnyz;

    .line 3979
    :cond_2
    iget-object v0, p0, Lnys;->c:Lnyz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3983
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3984
    if-eqz v0, :cond_3

    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    .line 3990
    :cond_3
    iput v0, p0, Lnys;->d:I

    goto :goto_0

    .line 3992
    :cond_4
    iput v1, p0, Lnys;->d:I

    goto :goto_0

    .line 3997
    :sswitch_4
    iget-object v0, p0, Lnys;->e:Lnyo;

    if-nez v0, :cond_5

    .line 3998
    new-instance v0, Lnyo;

    invoke-direct {v0}, Lnyo;-><init>()V

    iput-object v0, p0, Lnys;->e:Lnyo;

    .line 4000
    :cond_5
    iget-object v0, p0, Lnys;->e:Lnyo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4004
    :sswitch_5
    iget-object v0, p0, Lnys;->g:Lnyz;

    if-nez v0, :cond_6

    .line 4005
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iput-object v0, p0, Lnys;->g:Lnyz;

    .line 4007
    :cond_6
    iget-object v0, p0, Lnys;->g:Lnyz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4011
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4012
    iget-object v0, p0, Lnys;->n:[Ljava/lang/String;

    array-length v0, v0

    .line 4013
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 4014
    iget-object v3, p0, Lnys;->n:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4015
    iput-object v2, p0, Lnys;->n:[Ljava/lang/String;

    .line 4016
    :goto_1
    iget-object v2, p0, Lnys;->n:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 4017
    iget-object v2, p0, Lnys;->n:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 4018
    invoke-virtual {p1}, Loxn;->a()I

    .line 4016
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4021
    :cond_7
    iget-object v2, p0, Lnys;->n:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 4025
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4026
    iget-object v0, p0, Lnys;->h:[Lnyz;

    if-nez v0, :cond_9

    move v0, v1

    .line 4027
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyz;

    .line 4028
    iget-object v3, p0, Lnys;->h:[Lnyz;

    if-eqz v3, :cond_8

    .line 4029
    iget-object v3, p0, Lnys;->h:[Lnyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4031
    :cond_8
    iput-object v2, p0, Lnys;->h:[Lnyz;

    .line 4032
    :goto_3
    iget-object v2, p0, Lnys;->h:[Lnyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 4033
    iget-object v2, p0, Lnys;->h:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 4034
    iget-object v2, p0, Lnys;->h:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4035
    invoke-virtual {p1}, Loxn;->a()I

    .line 4032
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 4026
    :cond_9
    iget-object v0, p0, Lnys;->h:[Lnyz;

    array-length v0, v0

    goto :goto_2

    .line 4038
    :cond_a
    iget-object v2, p0, Lnys;->h:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 4039
    iget-object v2, p0, Lnys;->h:[Lnyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4043
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnys;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 4047
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnys;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 4051
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4052
    if-eq v0, v4, :cond_b

    if-ne v0, v5, :cond_c

    .line 4054
    :cond_b
    iput v0, p0, Lnys;->k:I

    goto/16 :goto_0

    .line 4056
    :cond_c
    iput v4, p0, Lnys;->k:I

    goto/16 :goto_0

    .line 4061
    :sswitch_b
    iget-object v0, p0, Lnys;->q:Lnzd;

    if-nez v0, :cond_d

    .line 4062
    new-instance v0, Lnzd;

    invoke-direct {v0}, Lnzd;-><init>()V

    iput-object v0, p0, Lnys;->q:Lnzd;

    .line 4064
    :cond_d
    iget-object v0, p0, Lnys;->q:Lnzd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4068
    :sswitch_c
    iget-object v0, p0, Lnys;->f:Lnyp;

    if-nez v0, :cond_e

    .line 4069
    new-instance v0, Lnyp;

    invoke-direct {v0}, Lnyp;-><init>()V

    iput-object v0, p0, Lnys;->f:Lnyp;

    .line 4071
    :cond_e
    iget-object v0, p0, Lnys;->f:Lnyp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4075
    :sswitch_d
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnys;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 4079
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnys;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 4083
    :sswitch_f
    iget-object v0, p0, Lnys;->l:Lnzm;

    if-nez v0, :cond_f

    .line 4084
    new-instance v0, Lnzm;

    invoke-direct {v0}, Lnzm;-><init>()V

    iput-object v0, p0, Lnys;->l:Lnzm;

    .line 4086
    :cond_f
    iget-object v0, p0, Lnys;->l:Lnzm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4090
    :sswitch_10
    iget-object v0, p0, Lnys;->m:Lnzm;

    if-nez v0, :cond_10

    .line 4091
    new-instance v0, Lnzm;

    invoke-direct {v0}, Lnzm;-><init>()V

    iput-object v0, p0, Lnys;->m:Lnzm;

    .line 4093
    :cond_10
    iget-object v0, p0, Lnys;->m:Lnzm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4097
    :sswitch_11
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4098
    iget-object v0, p0, Lnys;->o:[Lnzm;

    if-nez v0, :cond_12

    move v0, v1

    .line 4099
    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzm;

    .line 4100
    iget-object v3, p0, Lnys;->o:[Lnzm;

    if-eqz v3, :cond_11

    .line 4101
    iget-object v3, p0, Lnys;->o:[Lnzm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4103
    :cond_11
    iput-object v2, p0, Lnys;->o:[Lnzm;

    .line 4104
    :goto_5
    iget-object v2, p0, Lnys;->o:[Lnzm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    .line 4105
    iget-object v2, p0, Lnys;->o:[Lnzm;

    new-instance v3, Lnzm;

    invoke-direct {v3}, Lnzm;-><init>()V

    aput-object v3, v2, v0

    .line 4106
    iget-object v2, p0, Lnys;->o:[Lnzm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4107
    invoke-virtual {p1}, Loxn;->a()I

    .line 4104
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 4098
    :cond_12
    iget-object v0, p0, Lnys;->o:[Lnzm;

    array-length v0, v0

    goto :goto_4

    .line 4110
    :cond_13
    iget-object v2, p0, Lnys;->o:[Lnzm;

    new-instance v3, Lnzm;

    invoke-direct {v3}, Lnzm;-><init>()V

    aput-object v3, v2, v0

    .line 4111
    iget-object v2, p0, Lnys;->o:[Lnzm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 3957
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v0, 0x0

    .line 3801
    const/4 v1, 0x1

    iget-object v2, p0, Lnys;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 3802
    iget-object v1, p0, Lnys;->c:Lnyz;

    if-eqz v1, :cond_0

    .line 3803
    const/4 v1, 0x2

    iget-object v2, p0, Lnys;->c:Lnyz;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 3805
    :cond_0
    iget v1, p0, Lnys;->d:I

    if-eq v1, v6, :cond_1

    .line 3806
    const/4 v1, 0x3

    iget v2, p0, Lnys;->d:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 3808
    :cond_1
    iget-object v1, p0, Lnys;->e:Lnyo;

    if-eqz v1, :cond_2

    .line 3809
    const/4 v1, 0x4

    iget-object v2, p0, Lnys;->e:Lnyo;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 3811
    :cond_2
    iget-object v1, p0, Lnys;->g:Lnyz;

    if-eqz v1, :cond_3

    .line 3812
    const/4 v1, 0x5

    iget-object v2, p0, Lnys;->g:Lnyz;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 3814
    :cond_3
    iget-object v1, p0, Lnys;->n:[Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3815
    iget-object v2, p0, Lnys;->n:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 3816
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 3815
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3819
    :cond_4
    iget-object v1, p0, Lnys;->h:[Lnyz;

    if-eqz v1, :cond_6

    .line 3820
    iget-object v2, p0, Lnys;->h:[Lnyz;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 3821
    if-eqz v4, :cond_5

    .line 3822
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 3820
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3826
    :cond_6
    iget-object v1, p0, Lnys;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 3827
    const/16 v1, 0x8

    iget-object v2, p0, Lnys;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 3829
    :cond_7
    iget-object v1, p0, Lnys;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 3830
    const/16 v1, 0x9

    iget-object v2, p0, Lnys;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 3832
    :cond_8
    iget v1, p0, Lnys;->k:I

    if-eq v1, v6, :cond_9

    .line 3833
    const/16 v1, 0xa

    iget v2, p0, Lnys;->k:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 3835
    :cond_9
    iget-object v1, p0, Lnys;->q:Lnzd;

    if-eqz v1, :cond_a

    .line 3836
    const/16 v1, 0xb

    iget-object v2, p0, Lnys;->q:Lnzd;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 3838
    :cond_a
    iget-object v1, p0, Lnys;->f:Lnyp;

    if-eqz v1, :cond_b

    .line 3839
    const/16 v1, 0xc

    iget-object v2, p0, Lnys;->f:Lnyp;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 3841
    :cond_b
    iget-object v1, p0, Lnys;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 3842
    const/16 v1, 0xd

    iget-object v2, p0, Lnys;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 3844
    :cond_c
    iget-object v1, p0, Lnys;->r:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 3845
    const/16 v1, 0xe

    iget-object v2, p0, Lnys;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 3847
    :cond_d
    iget-object v1, p0, Lnys;->l:Lnzm;

    if-eqz v1, :cond_e

    .line 3848
    const/16 v1, 0xf

    iget-object v2, p0, Lnys;->l:Lnzm;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 3850
    :cond_e
    iget-object v1, p0, Lnys;->m:Lnzm;

    if-eqz v1, :cond_f

    .line 3851
    const/16 v1, 0x10

    iget-object v2, p0, Lnys;->m:Lnzm;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 3853
    :cond_f
    iget-object v1, p0, Lnys;->o:[Lnzm;

    if-eqz v1, :cond_11

    .line 3854
    iget-object v1, p0, Lnys;->o:[Lnzm;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_11

    aget-object v3, v1, v0

    .line 3855
    if-eqz v3, :cond_10

    .line 3856
    const/16 v4, 0x11

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 3854
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3860
    :cond_11
    iget-object v0, p0, Lnys;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3862
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3732
    invoke-virtual {p0, p1}, Lnys;->a(Loxn;)Lnys;

    move-result-object v0

    return-object v0
.end method
