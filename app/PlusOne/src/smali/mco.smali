.class public final Lmco;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlc;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1537
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1540
    iput-object v0, p0, Lmco;->apiHeader:Llyq;

    .line 1543
    iput-object v0, p0, Lmco;->a:Lnlc;

    .line 1537
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1560
    const/4 v0, 0x0

    .line 1561
    iget-object v1, p0, Lmco;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 1562
    const/4 v0, 0x1

    iget-object v1, p0, Lmco;->apiHeader:Llyq;

    .line 1563
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1565
    :cond_0
    iget-object v1, p0, Lmco;->a:Lnlc;

    if-eqz v1, :cond_1

    .line 1566
    const/4 v1, 0x2

    iget-object v2, p0, Lmco;->a:Lnlc;

    .line 1567
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1569
    :cond_1
    iget-object v1, p0, Lmco;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1570
    iput v0, p0, Lmco;->ai:I

    .line 1571
    return v0
.end method

.method public a(Loxn;)Lmco;
    .locals 2

    .prologue
    .line 1579
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1580
    sparse-switch v0, :sswitch_data_0

    .line 1584
    iget-object v1, p0, Lmco;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1585
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmco;->ah:Ljava/util/List;

    .line 1588
    :cond_1
    iget-object v1, p0, Lmco;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1590
    :sswitch_0
    return-object p0

    .line 1595
    :sswitch_1
    iget-object v0, p0, Lmco;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 1596
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmco;->apiHeader:Llyq;

    .line 1598
    :cond_2
    iget-object v0, p0, Lmco;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1602
    :sswitch_2
    iget-object v0, p0, Lmco;->a:Lnlc;

    if-nez v0, :cond_3

    .line 1603
    new-instance v0, Lnlc;

    invoke-direct {v0}, Lnlc;-><init>()V

    iput-object v0, p0, Lmco;->a:Lnlc;

    .line 1605
    :cond_3
    iget-object v0, p0, Lmco;->a:Lnlc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1580
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1548
    iget-object v0, p0, Lmco;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 1549
    const/4 v0, 0x1

    iget-object v1, p0, Lmco;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1551
    :cond_0
    iget-object v0, p0, Lmco;->a:Lnlc;

    if-eqz v0, :cond_1

    .line 1552
    const/4 v0, 0x2

    iget-object v1, p0, Lmco;->a:Lnlc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1554
    :cond_1
    iget-object v0, p0, Lmco;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1556
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1533
    invoke-virtual {p0, p1}, Lmco;->a(Loxn;)Lmco;

    move-result-object v0

    return-object v0
.end method
